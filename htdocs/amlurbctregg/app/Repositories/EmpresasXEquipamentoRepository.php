<?php

namespace Amlurb\Repositories;

use Amlurb\Models\EmpresasXEquipamento;
use Illuminate\Support\Facades\Auth;

class EmpresasXEquipamentoRepository extends BaseRepository
{

    protected $modelClass = EmpresasXEquipamento::class;

    public function getEquipamentoById($id)
    {
        $query = $this->newQuery();
        return $query->whereId($id)->with(['equipamento' => function($query) {
            $query->with(['tipo_equipamento']);
        }])->first();
    }

    // traz todos os equipamentos pertencentes a uma empresa
    public function getEquipamentosByIdEmpresa($id, $take = null)
    {
        // $query = $this->newQuery();
        return EmpresasXEquipamento::where('empresa_id', $id)
            ->with(['equipamento' => function($query) {
                $query->with(['tipo_equipamento']);
            }])
            ->with(['equipamento_alocacao' => function($query) {
                $query->with(['empresa']);
            }])
            ->with('status');
        // if ($take) {
        //     return $this->doQuery($query, true, $take);
        // }
        // return $this->doQuery($query);
    }

    // traz equipamentos pertencentes a uma empresa que ainda nao foram alocados a outras empresas
    public function getEquipamentosNaoAlocadosByIdEmpresa($id)
    {
        $query = $this->newQuery();
        $query->where('empresa_id', $id)
            ->with(['equipamento' => function($query) {
                $query->with(['tipo_equipamento']);
            }])
            ->whereHas('equipamento_alocacao', function($query) {
                $query->with('empresa');
            }, '=', 0)
            ->where('status_id', \config('enums.status.ativo'));
        return $this->doQuery($query);
    }

    // equipamentos alocados a uma empresa
    public function getEquipamentosAlocadosToIdEmpresa($empresa_id)
    {
        $query = $this->newQuery();
        $query->where('empresa_id', Auth::user()->empresa_id) // empresa logada, dona dos equipamentos
            ->with(['equipamento' => function($query) {
                $query->with(['tipo_equipamento']);
            }])
            ->whereHas('equipamento_alocacao', function($query) use ($empresa_id) {
                $query->where('empresa_id', $empresa_id); // empresa a quem os equipamentos estao vinculados
            });
        return $this->doQuery($query);
    }

    // traz equipamentos a serem pagos
    public function getEquipamentosNaoAtivadosByIdEmpresa($id)
    {
        $query = $this->newQuery();
        $query->where('empresa_id', $id)
            ->with(['equipamento' => function($query) {
                $query->with(['tipo_equipamento']);
            }])
            ->with('status')
            ->whereIn('status_id', [\config('enums.status.inativo'), \config('enums.status.vencido')]);
        return $this->doQuery($query);
    }

}