<?php

namespace Amlurb\Repositories;

use Illuminate\Database\Eloquent\Builder as EloquentQueryBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Pagination\AbstractPaginator as Paginator;

abstract class BaseRepository
{
    /**
     * @var string
     */
    protected $modelClass;

    /**
     * @return EloquentQueryBuilder|QueryBuilder
     */
    protected function newQuery()
    {
        return app($this->modelClass)->newQuery();
    }

    /**
     * @param EloquentQueryBuilder|QueryBuilder $query
     * @param bool $paginate
     * @param int $take
     *
     * @return EloquentCollection|Paginator
     */
    protected function doQuery($query = null, $paginate = false, $take = 20)
    {
        if (is_null($query)) {
            $query = $this->newQuery();
        }

        if ($paginate == true) {
            return $query->paginate($take);
        }

        return $query->get();
    }

}