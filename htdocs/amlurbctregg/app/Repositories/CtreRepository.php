<?php

namespace Amlurb\Repositories;

use Amlurb\Models\Ctre;
use Illuminate\Support\Facades\DB;

class CtreRepository extends BaseRepository
{

    protected $modelClass = Ctre::class;

    /**
     * metodo para buscar CTRE com todos os vinculos
     */
    public function getCtreCompleto($ctre_id)
    {
        $query = $this->constructCtreEmpresas();
        $query->with([
            'ctre_equipamentos' => function ($query) {
                $query->with([
                    'empresas_x_equipamentos' => function ($query) {
                        $query->with([
                            'equipamento' => function ($query) {
                                $query->with([
                                    'tipo_equipamento' => function ($query) {
                                        $query->select('id', 'nome');
                                    }
                                ]);
                                $query->select('id', 'tipo_equipamento_id', 'capacidade');
                            }
                        ]);
                        $query->select('id', 'equipamento_id', 'codigo_amlurb')->withTrashed();
                    }
                ]);
                $query->select('ctre_id', 'equipamento_id');
            }
        ]);
        $query->with([
            'ctre_residuos' => function ($query) {
                $query->with([
                    'residuo' => function ($query) {
                        $query->select('id', 'nome');
                    }
                ]);
                $query->select('ctre_residuos.ctre_id', 'ctre_residuos.residuo_id');
            }
        ]);
        $query->whereId($ctre_id);

        return $query->first();
    }

    public function getCtreEmitidos($empresa_id = null, $limit = null)
    {
        $query = $this->constructCtreEmpresas();

        if ($empresa_id) {
            $query->where('empresa_id', $empresa_id);
        }
        if ($limit) {
            $query->limit($limit);
        }

        return $this->doQuery($query);
    }

    public function getCtreRecebidos($empresa_id, $tipo_empresa, $limit = null)
    {
        $query = $this->constructCtreEmpresas();

        if ($tipo_empresa == \config('enums.empresas_tipo.transportador')) {
            $query->where('empresa_id', '!=', $empresa_id)
                ->where('transportador_id', '=', $empresa_id);
        } elseif ($tipo_empresa == \config('enums.empresas_tipo.destino_final_reciclado')) {
            $query->where('destino_id', $empresa_id);
        } else { // gerador
            $query->where('empresa_id', '!=', $empresa_id)
                ->where('gerador_id', '=', $empresa_id);
        }

        if ($limit) {
            $query->limit($limit);
        }

        return $this->doQuery($query);
    }

    private function constructCtreEmpresas()
    {
        $query = $this->newQuery();
        $query
            ->with([
                'empresa' => function ($query) {
                    $query->select('id', 'razao_social');
                }
            ])
            ->with([
                'gerador' => function ($query) {
                    $query->select('id', 'razao_social', 'id_limpurb');
                }
            ])
            ->with([
                'transportador' => function ($query) {
                    $query->select('id', 'razao_social', 'id_limpurb');
                }
            ])
            ->with([
                'destino_final' => function ($query) {
                    $query->select('id', 'razao_social', 'id_limpurb');
                }
            ])
            ->with([
                'status' => function ($query) {
                    $query->select('id', 'descricao');
                }
            ])
            ->with([
                'empresas_veiculos' => function ($query) {
                    $query->select('id', 'placa', 'tipo')->withTrashed();
                }
            ])
            ->with([
                'ctre_equipamentos' => function ($query) {
                    $query->select('id', 'ctre_id', 'equipamento_id');
                }
            ])
            ->with([
                'ctre_residuos' => function ($query) {
                    $query->select('id', 'ctre_id', 'residuo_id');
                }
            ])
            ->select('ctre.id', 'ctre.empresa_id', 'ctre.gerador_id', 'ctre.transportador_id', 'ctre.destino_id',
                'ctre.veiculo_id', 'ctre.status_id', 'ctre.codigo', 'ctre.data_emissao',
                'ctre.data_expiracao', 'ctre.data_validacao', 'ctre.data_validacao_final')
            ->orderBy('id', 'DESC');
        return $query;
    }

    public function getCTREsEmAberto()
    {
        $query = $this->newQuery();
        $query->where('status_id', \config('enums.status.em_aberto'));
        return $this->doQuery($query);
    }

    /**
     * conta CTREs de acordo com parametros informados
     *
     * @param int $empresa_id
     * @param int $status_id
     *
     * @return int
     */
    public function countCTREs($empresa_id = null, $status_id = null)
    {
        $query = $this->newQuery();
        if ($empresa_id) {
            $query->where('empresa_id', $empresa_id);
        }
        if ($status_id) {
            $query->where('status_id', $status_id);
        }

        return $query->count();
    }


    public function countCtreByDate($status_id = null, array $params)
    {
        $query = $this->newQuery();

        if (!is_null($status_id)) { // usa status do ctre se tiver sido informado
            $query->where('status_id', $status_id);
        }
        // verifica campos que foram informados
        if (!is_null($params['date_format']) && !is_null($params['period'])) {
            if (!is_null($params['comparator'])) { // usa comparador de datas
                $query->where(DB::raw($params['date_format']), $params['comparator'], $params['period']);
            } else {
                $query->where(DB::raw($params['date_format']), $params['period']);
            }
        }

        if (!is_null($params['empresa_id']) && !is_null($params['empresa_tipo'])) {
            if ($params['empresa_tipo'] == \config('enums.empresas_tipo.transportador')) {
                $query->where('transportador_id', $params['empresa_id'])
                    ->where('empresa_id', '!=', $params['empresa_id']); // transportador != emissor
            } elseif ($params['empresa_tipo'] == \config('enums.empresas_tipo.destino_final_reciclado')) {
                $query->where('destino_id', $params['empresa_id']);
            } else { // gerador
                $query->where('gerador_id', $params['empresa_id']);
            }
        }

        return $query->count();
    }

    // CTREs por ano
    public function countCtreByYear($status_id = null, $date_format)
    {
        $query = $this->newQuery();

        if (!is_null($status_id)) { // usa status do ctre se tiver sido informado
            $query->where('status_id', $status_id);
        }

        return $query->whereYear('created_at', $date_format)->count();
    }

    // CTREs entre datas especificas
    public function countCtreBetweenDates($status_id = null, $date_format, $dataInicio, $dataFinal)
    {
        $query = $this->newQuery();

        if (!is_null($status_id)) { // usa status do ctre se tiver sido informado
            $query->where('status_id', $status_id);
        }

        return $query->whereBetween(DB::raw($date_format), [$dataInicio, $dataFinal])->count();
    }

    // FiscalController - rastreabilidade
    public function ultimosCTREs()
    {
        $query = $this->newQuery();
        return $query->orderBy('ctre.created_at', 'desc')
            ->with('empresa:id,id_limpurb,razao_social')
            ->with('status:id,descricao');
    }


    public function ctreIndicadores($idEmpresa = 0, $tipo = '')
    {
        if ($idEmpresa != 0) {

            if ($tipo == 'administrador') {
                $ctreOpcoes = [
                    'totalEmitido' => Ctre::whereNotNull('id')->count(),
                    'totalAberto' => $this->countCTREs(null, \config('enums.status.em_aberto')),
                    'totalExpirado' => $this->countCTREs(null, \config('enums.status.expirado')),
                    'totalFinalizado' => $this->countCTREs(null, \config('enums.status.finalizado')),
                ];
            } else {
                $ctreOpcoes = [
                    'totalEmitido' => $this->countCTREs($idEmpresa),
                    'totalAberto' => $this->countCTREs($idEmpresa, \config('enums.status.em_aberto')),
                    'totalExpirado' => $this->countCTREs($idEmpresa, \config('enums.status.expirado')),
                    'totalFinalizado' => $this->countCTREs($idEmpresa, \config('enums.status.finalizado')),
                ];
            }

            return $ctreOpcoes;

        } else {
            return false;
        }
    }

}