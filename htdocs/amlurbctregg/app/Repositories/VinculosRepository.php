<?php

namespace Amlurb\Repositories;

use Amlurb\Models\Vinculos;
use Illuminate\Support\Facades\Auth;

class VinculosRepository extends BaseRepository
{

    protected $modelClass = Vinculos::class;

    public function countVinculosPorPeriodo(array $params)
    {
        $query = $this->newQuery();

        $empresa_tipo = $params['empresa_tipo'];

        $query
            ->whereHas('empresa_vinculada', function($query) use ($empresa_tipo) {
                $query->whereHas('empresas_x_empresas_tipos', function($query) use ($empresa_tipo) {
                    $query->where('empresa_tipo_id', $empresa_tipo);
                });
            })
            ->where('vinculador_id', Auth::user()->empresa_id);
        if (!is_null($params['periodo'])) {
            $query->whereBetween('created_at', $params['periodo']);
        }
        return $query->count();
    }

}