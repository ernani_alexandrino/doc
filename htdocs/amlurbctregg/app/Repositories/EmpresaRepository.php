<?php

namespace Amlurb\Repositories;

use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Illuminate\Support\Facades\Auth;

class EmpresaRepository extends BaseRepository
{
    protected $modelClass = Empresa::class;

    public function getEmpresaTipoById($id)
    {
        return EmpresasXEmpresasTipo::where('empresa_id', $id)
        ->select('empresa_tipo_id')->first()->empresa_tipo_id;
    }

    public function buscaEmpresaPorCnpj($cnpj)
    {
        $query = $this->newQuery();
        return $query->where('cnpj', $cnpj)->select('id', 'razao_social', 'id_limpurb')->first();
    }

    public function getDadosCadastraisBasicos($empresaId)
    {
        $query = $this->newQuery();
        return $query->whereId($empresaId)
            ->with(['empresa_informacao_complementar','empresas_x_empresas_tipos'])
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->first();
    }

    // recebe um id de gerador ou destino final
    // retorna dados para formulario de edicao
    public function getVinculoByEmpresaVinculadaId($empresa_id)
    {
        $query = $this->newQuery();
        $query->whereHas('empresa_vinculada', function($query) {
            $query->where('vinculador_id', '=', Auth::user()->empresa_id);
        });
        $query->with(['empresa_vinculada' => function($query) {
            $query->with('frequencia_coleta')
                ->with('empresa_responsavel')
                ->where('vinculador_id', Auth::user()->empresa_id);
        }]);
        $query->with(['logistica_residuos' => function($query) {
            $query->with('residuo')
                ->where('vinculador_id', Auth::user()->empresa_id);
        }]);
        if ($this->getEmpresaTipoById($empresa_id) != \config('enums.empresas_tipo.destino_final_reciclado')) {
            $query->with(['equipamento_alocacao' => function($query) {
                $query->whereHas('empresas_x_equipamento', function($query) {
                    $query->where('empresa_id', '=', Auth::user()->empresa_id);
                })->with(['empresas_x_equipamento' => function($query) {
                    $query->with(['equipamento' => function($query) {
                        $query->with('tipo_equipamento');
                    }]);
                }]);
            }]);
        }
        return $query->with('status')
            ->whereId($empresa_id)->first();
    }

    public function getGeradoresVinculados()
    {
        $query = $this->queryVinculo(Auth::user()->empresa_id, \config('enums.empresas_tipo.grande_gerador'));
        return $this->doQuery($query);
    }

    public function getGeradoresVinculadosCompleto($empresa_id)
    {
        $query = $this->newQuery();
        $query->whereHas('empresa_vinculada', function ($query) use ($empresa_id) {
            $query->where('vinculador_id', '=', $empresa_id);
            $query->whereIn('empresa_vinculada_tipo_id', [
                config('enums.empresas_tipo.grande_gerador'),
                config('enums.empresas_tipo.pequeno_gerador'),
                config('enums.empresas_tipo.orgao_publico'),
                config('enums.empresas_tipo.servico_saude'),
                config('enums.empresas_tipo.condominio_misto')
            ]);
        })->with('status')
            ->with([
                'logistica_residuos' => function ($query) use ($empresa_id) {
                    $query->whereHas('vinculador', function ($query) use ($empresa_id) {
                        $query->where('vinculador_id', '=', $empresa_id);
                    });
                }
            ])
            ->with([
                'equipamento_alocacao' => function ($query) use ($empresa_id) {
                    $query->whereHas('empresas_x_equipamento', function ($query) use ($empresa_id) {
                        $query->where('empresa_id', '=', $empresa_id);
                    });
                }
            ])
            ->with([
                'empresa_vinculada' => function ($query) {
                    $query->with('frequencia_coleta');
                }
            ]);
        return $this->doQuery($query);
    }

    public function getTransportadoresVinculados()
    {
        $query = $this->queryVinculo(Auth::user()->empresa_id, \config('enums.empresas_tipo.transportador'));
        return $this->doQuery($query);
    }

    public function getTransportadoresVinculadosCompleto($empresa_id)
    {
        $query = $this->newQuery();
        $query->whereHas('empresa_vinculada', function ($query) use ($empresa_id) {
            $query->where('vinculador_id', '=', $empresa_id);
            $query->where('empresa_vinculada_tipo_id', '=', \config('enums.empresas_tipo.transportador'));
        })->with([
            'empresa_vinculada' => function ($query) {
                $query->with('frequencia_coleta');
            }
        ])->with([
            'logistica_residuos' => function ($query) use ($empresa_id) {
                $query->where('vinculador_id', $empresa_id);
            }
        ])->with([
            'equipamento_alocacao' => function ($query) use ($empresa_id) {
                $query->whereHas('empresas_x_equipamento', function ($query) use ($empresa_id) {
                    $query->where('empresa_id', '=', $empresa_id);
                });
            }
        ])->with('status');
        return $this->doQuery($query);
    }

    public function getCooperativasVinculadasCompleto($empresa_id)
    {
        $query = $this->newQuery();
        $query->whereHas('empresa_vinculada', function ($query) use ($empresa_id) {
            $query->where('vinculador_id', '=', $empresa_id);
            $query->where('empresa_vinculada_tipo_id', '=', \config('enums.empresas_tipo.cooperativa_residuos'));
        })->with([
            'empresa_vinculada' => function ($query) {
                $query->with('frequencia_coleta');
            }
        ])->with([
            'logistica_residuos' => function ($query) use ($empresa_id) {
                $query->where('vinculador_id', $empresa_id);
            }
        ])->with([
            'equipamento_alocacao' => function ($query) use ($empresa_id) {
                $query->whereHas('empresas_x_equipamento', function ($query) use ($empresa_id) {
                    $query->where('empresa_id', '=', $empresa_id);
                });
            }
        ])->with('status');
        return $this->doQuery($query);
    }

    public function getDestinosFinaisVinculados()
    {
        $query = $this->queryVinculo(Auth::user()->empresa_id, \config('enums.empresas_tipo.destino_final_reciclado'));
        return $this->doQuery($query);
    }

    public function getDestinosFinaisVinculadosCompleto($empresa_id)
    {
        $query = $this->newQuery();
        $query->whereHas('empresa_vinculada', function ($query) use ($empresa_id) {
            $query->where('vinculador_id', '=', $empresa_id);
            $query->where('empresa_vinculada_tipo_id', '=', \config('enums.empresas_tipo.destino_final_reciclado'));
        })->with('status')
            ->with('empresa_vinculada')
            ->with(['logistica_residuos' => function ($query) use ($empresa_id) {
                $query->where('vinculador_id', $empresa_id);
            }]);
        return $this->doQuery($query);
    }

    public function getCondominioMistoVinculado($empresa_id)
    {
        $query = $this->newQuery();
        $query->whereHas('empresa_vinculada', function ($query) use ($empresa_id) {
            $query->where('vinculador_id', '=', $empresa_id);
            $query->where('empresa_vinculada_tipo_id', '=', \config('enums.empresas_tipo.condominio_misto'));
        })->with(['empresa_vinculada' => function ($query) {
            $query->with('empresa_responsavel');
        }])->with('status');
        return $query->first();
    }

    private function queryVinculo($empresa_id, $tipo_empresa)
    {
        $query = $this->newQuery();
        $query->whereHas('empresa_vinculada', function ($query) use ($empresa_id, $tipo_empresa) {
            $query->where('vinculador_id', '=', $empresa_id);
            $query->where('empresa_vinculada_tipo_id', '=', $tipo_empresa);
        });
        return $query;
    }
}