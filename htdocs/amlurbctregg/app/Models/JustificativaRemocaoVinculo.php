<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class JustificativaRemocaoVinculo extends Model
{
    protected $table = 'justificativa_remocao_vinculo';

    protected $fillable = ['justificativa_id', 'vinculo_id'];

}