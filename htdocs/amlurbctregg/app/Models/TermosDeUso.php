<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 02 May 2018 11:59:55 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TermosDeUso
 * 
 * @property int $id
 * @property string $termo_boleto
 * @property string $termo_qr_code
 * @property string $termo_uso
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $empresas_tipos
 * @property \Illuminate\Database\Eloquent\Collection $empresas
 *
 * @package Amlurb\Models
 */
class TermosDeUso extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'termos_de_uso';

	protected $fillable = [
		'termo_boleto',
		'termo_qr_code',
		'termo_uso'
	];

	public function empresas_tipos()
	{
		return $this->belongsToMany(\Amlurb\Models\EmpresasTipo::class, 'empresas_tipo_x_termos_de_uso', 'termos_id', 'empresa_tipo_id')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}

	public function empresas()
	{
		return $this->belongsToMany(\Amlurb\Models\Empresa::class, 'empresas_x_termos_de_uso', 'termo_id')
					->withPivot('id', 'termo_boleto', 'termo_qr_code', 'termo_uso', 'deleted_at')
					->withTimestamps();
	}
}
