<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 21 May 2019 12:46:03 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresaAlteracaoJson
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $responsible_user_id
 * @property int $reviewer_user_id
 * @property string $json_data
 *
 * @property \App\Models\Empresa $empresa
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class EmpresaAlteracaoJson extends Eloquent
{
    protected $table = 'empresa_alteracao_json';
    public $timestamps = false;

    protected $casts = [
        'empresa_id' => 'int',
        'responsible_user_id' => 'int',
        'reviewer_user_id' => 'int'
    ];

    protected $fillable = [
        'empresa_id',
        'responsible_user_id',
        'reviewer_user_id',
        'json_data'
    ];

    public function empresa()
    {
        return $this->belongsTo(\App\Models\Empresa::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'reviewer_user_id');
    }
}
