<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresaEndereco
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $estado_id
 * @property int $cidade_id
 * @property string $endereco
 * @property string $cep
 * @property string $bairro
 * @property string $complemento
 * @property string $ponto_referencia
 * @property int $numero
 * @property string $latitude
 * @property string $longitude
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Amlurb\Models\Cidade $cidade
 * @property \Amlurb\Models\Empresa $empresa
 *
 * @package Amlurb\Models
 */
class EmpresaEndereco extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'empresa_endereco';

    protected $casts = [
        'empresa_id' => 'int',
        'estado_id' => 'int',
        'cidade_id' => 'int',
        'numero' => 'int'
    ];

    protected $fillable = [
        'empresa_id',
        'estado_id',
        'cidade_id',
        'endereco',
        'cep',
        'bairro',
        'complemento',
        'numero'
    ];

    public function cidade()
    {
        return $this->belongsTo(\Amlurb\Models\Cidade::class);
    }

    public function estado()
    {
        return $this->belongsTo(\Amlurb\Models\Estado::class);
    }

    public function empresa()
    {
        return $this->belongsTo(\Amlurb\Models\Empresa::class);
    }

    public static function buscaEndereco($id = 0)
    {
        return EmpresaEndereco::where('empresa_id', $id)->first();
    }

}
