<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Equipamento
 * 
 * @property int $id
 * @property int $status_id
 * @property string $capacidade
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Status $status
 * @property \Illuminate\Database\Eloquent\Collection $empresas
 * @property \Illuminate\Database\Eloquent\Collection $equipamentos_qrcodes
 *
 * @package Amlurb\Models
 */
class Equipamento extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'status_id' => 'int'
	];

	protected $fillable = [
		'status_id',
        'tipo_equipamento',
		'capacidade'
	];

	protected $dates = ['data_validade'];

	public function status()
	{
		return $this->belongsTo(\Amlurb\Models\Status::class);
	}

	public function tipo_equipamento()
    {
        return $this->belongsTo(\Amlurb\Models\TipoEquipamento::class);
    }

	public function empresas()
	{
		return $this->belongsToMany(\Amlurb\Models\Empresa::class, 'empresas_x_equipamentos')
					->withPivot('id', 'status_id', 'deleted_at')
					->withTimestamps();
	}

    public static function boot()
    {
        parent::boot();
        self::observe(LogsObserver::class);
    }

}
