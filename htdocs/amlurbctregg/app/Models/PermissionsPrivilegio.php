<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PermissionsPrivilegio
 * 
 * @property int $id
 * @property int $empresa_id
 * @property int $privilegio_id
 * @property string $permission
 * @property int $excluido
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Empresa $empresa
 * @property \App\Models\Privilegio $privilegio
 *
 * @package App\Models
 */
class PermissionsPrivilegio extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'permissions_privilegio';

	protected $casts = [
		'empresa_id' => 'int',
		'privilegio_id' => 'int',
		'excluido' => 'int'
	];

	protected $fillable = [
		'empresa_id',
		'privilegio_id',
		'permission',
		'excluido'
	];

	public function empresa()
	{
		return $this->belongsTo(\App\Models\Empresa::class);
	}

	public function privilegio()
	{
		return $this->belongsTo(\App\Models\Privilegio::class);
	}
}
