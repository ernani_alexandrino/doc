<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 10 May 2018 17:05:57 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Boleto
 * 
 * @property int $id
 * @property int $empresa_id
 * @property string $anoEmissao
 * @property int $numeroGuia
 * @property float $valor
 * @property int $status_id
 * @property \Carbon\Carbon $dataDeValidade
 * @property \Carbon\Carbon $dataDeVencimento
 * @property \Carbon\Carbon $dataDePagamento
 * @property int $codTipoServGuia
 * @property string $arquivoBoleto
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Empresa $empresa
 * @property \Amlurb\Models\Status $status
 * @property \Illuminate\Database\Eloquent\Collection $empresa_boleto_baixas
 *
 * @package Amlurb\Models
 */
class Boleto extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'boletos';

	protected $casts = [
		'empresa_id' => 'int',
		'numeroGuia' => 'int',
		'valor' => 'float',
		'status_id' => 'int',
		'codTipoServGuia' => 'int'
	];

	protected $dates = [
		'dataDeValidade',
		'dataDeVencimento',
		'dataDePagamento'
	];

	protected $fillable = [
		'empresa_id',
        'tipo',
		'anoEmissao',
		'numeroGuia',
		'valor',
		'status_id',
		'dataDeValidade',
		'dataDeVencimento',
		'dataDePagamento',
		'codTipoServGuia',
		'arquivoBoleto'
	];

	public function empresa()
	{
		return $this->belongsTo(Empresa::class);
	}

	public function status()
	{
		return $this->belongsTo(Status::class);
	}

	public function empresa_boleto_baixas()
	{
		return $this->hasMany(EmpresaBoletoBaixa::class);
	}

    public function boleto_veiculos()
    {
        return $this->hasOne(BoletoVeiculos::class, 'boleto_id', 'id');
    }

    public function getTipoAttribute($value)
    {
        if ($value == 'qrcode') {
            return 'Adesivo QR Code';
        }
        return ucfirst($value);
    }

}
