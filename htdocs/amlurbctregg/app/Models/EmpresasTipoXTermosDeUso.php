<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 02 May 2018 12:00:48 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresasTipoXTermosDeUso
 * 
 * @property int $id
 * @property int $termos_id
 * @property int $empresa_tipo_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\EmpresasTipo $empresas_tipo
 * @property \Amlurb\Models\TermosDeUso $termos_de_uso
 *
 * @package Amlurb\Models
 */
class EmpresasTipoXTermosDeUso extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'empresas_tipo_x_termos_de_uso';

	protected $casts = [
		'termos_id' => 'int',
		'empresa_tipo_id' => 'int'
	];

	protected $fillable = [
		'termos_id',
		'empresa_tipo_id'
	];

	public function empresas_tipo()
	{
		return $this->belongsTo(\Amlurb\Models\EmpresasTipo::class, 'empresa_tipo_id');
	}

	public function termos_de_uso()
	{
		return $this->belongsTo(\Amlurb\Models\TermosDeUso::class, 'termos_id');
	}
}
