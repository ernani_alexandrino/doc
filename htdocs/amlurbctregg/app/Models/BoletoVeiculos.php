<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class BoletoVeiculos extends Model
{
    use SoftDeletes;

    protected $table = 'boleto_veiculos';

    protected $fillable = ['boleto_id', 'veiculo_id'];

    public function veiculo()
    {
        return $this->belongsTo(EmpresasVeiculo::class, 'veiculo_id', 'id');
    }
}