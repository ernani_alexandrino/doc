<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 10 May 2018 15:23:31 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresaAprovacao
 *
 * @property int                    $id
 * @property int                    $empresa_id
 * @property int                    $user_id
 * @property \Carbon\Carbon         $created_at
 * @property \Carbon\Carbon         $updated_at
 * @property string                 $deleted_at
 *
 * @property \Amlurb\Models\Empresa $empresa
 * @property \Amlurb\Models\User    $user
 *
 * @package Amlurb\Models
 */
class EmpresaAprovacao extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'empresa_aprovacao';

    protected $casts = [
        'empresa_id' => 'int',
        'user_id' => 'int'
    ];

    protected $fillable = [
        'empresa_id',
        'user_id',
        'data_limte_vinculo'
    ];

    public static function boot()
    {
        parent::boot();
        self::observe(LogsObserver::class);
    }
    
    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
