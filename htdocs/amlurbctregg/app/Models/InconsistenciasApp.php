<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InconsistenciasApp
 * 
 * @property int $id
 * @property int $user_id
 * @property string $razao_social
 * @property string $grcode_empresa
 * @property string $cnpj
 * @property string $cep
 * @property string $estado
 * @property string $descricao
 * @property string $imagem_fachada
 * @property string $imagem_residuo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Amlurb\Models\User $user
 *
 * @package Amlurb\Models
 */
class InconsistenciasApp extends Eloquent
{
	protected $table = 'inconsistencias_app';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'razao_social',
		'grcode_empresa',
		'cnpj',
		'cep',
		'estado',
		'descricao',
		'imagem_fachada',
		'imagem_residuo'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
