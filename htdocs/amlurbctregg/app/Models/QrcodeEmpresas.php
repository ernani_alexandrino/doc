<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class QrcodeEmpresas
 * 
 * @property int $id
 * @property int $empresa_id
 * @property string $qrcode
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Empresa $empresa
 *
 * @package Amlurb\Models
 */
class QrcodeEmpresas extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'qrcode_empresas';

	protected $casts = [
		'empresa_id' => 'int'
	];

	protected $fillable = ['qrcode_id', 'empresa_id'];

	public function qrcode()
    {
        return $this->belongsTo(Qrcode::class);
    }

	public function empresa()
	{
		return $this->belongsTo(\Amlurb\Models\Empresa::class);
	}
}
