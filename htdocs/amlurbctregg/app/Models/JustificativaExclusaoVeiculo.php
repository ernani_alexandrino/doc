<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class JustificativaExclusaoVeiculo extends Model
{
    protected $table = 'justificativa_exclusao_veiculo';

    protected $fillable = [
        'justificativa_id', // texto da justificativa
        'veiculo_id', // veiculo que foi removido
        'empresa_id' // empresa que possuia o veiculo
    ];

}