<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class TipoEquipamento extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'tipo_equipamento';

    protected $casts = [
        'status_id' => 'int'
    ];

    protected $fillable = [
        'nome',
        'status_id'
    ];

    public function status()
    {
        return $this->belongsTo(\Amlurb\Models\Status::class);
    }

    public function equipamento()
    {
        return $this->hasMany(\Amlurb\Models\Equipamento::class);
    }

}
