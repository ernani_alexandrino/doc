<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:08 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ColetasExcluidasApp
 * 
 * @property int $id
 * @property int $user_id
 * @property \Carbon\Carbon $data_coleta
 * @property string $destino
 * @property string $gerador
 * @property string $veiculo
 * @property string $motivo
 * @property string $equipamento
 * @property string $qrcode_equipamento
 * @property string $data_hora_coleta_iso
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Amlurb\Models\User $user
 *
 * @package Amlurb\Models
 */
class ColetasExcluidasApp extends Eloquent
{
	protected $table = 'coletas_excluidas_app';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $dates = [
		'data_coleta'
	];

	protected $fillable = [
		'user_id',
		'data_coleta',
		'destino',
		'gerador',
		'veiculo',
		'motivo',
		'equipamento',
		'qrcode_equipamento',
		'data_hora_coleta_iso'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
