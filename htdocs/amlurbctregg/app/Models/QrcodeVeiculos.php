<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class QrcodeVeiculos extends Model
{
    protected $table = 'qrcode_veiculos';

    protected $fillable = ['qrcode_id', 'veiculo_id'];

    public function veiculo()
    {
        return $this->belongsTo(EmpresasVeiculo::class, 'veiculo_id', 'id');
    }
}