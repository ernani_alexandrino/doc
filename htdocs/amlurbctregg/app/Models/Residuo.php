<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Residuo
 * 
 * @property int $id
 * @property int $status_id
 * @property string $codigo
 * @property string $nome
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Status $status
 *
 * @package Amlurb\Models
 */
class Residuo extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'status_id' => 'int'
	];

	protected $fillable = [
		'status_id',
		'codigo',
		'nome'
	];

	public function status()
	{
		return $this->belongsTo(\Amlurb\Models\Status::class);
	}

	public function ctre_residuos()
    {
        return $this->hasMany(CtreResiduos::class);
    }

    public function logistica_residuos()
    {
        return $this->hasMany(LogisticaResiduos::class, 'residuo_id', 'id');
    }

}
