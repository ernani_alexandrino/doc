<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Justificativa extends Model
{
    use SoftDeletes;

    protected $table = 'justificativas';

    protected $fillable = ['justificativa'];

    protected $dates = ['deleted_at'];
}