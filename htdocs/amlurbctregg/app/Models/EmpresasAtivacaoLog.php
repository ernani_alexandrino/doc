<?php

namespace Amlurb;

use Illuminate\Database\Eloquent\Model;

/**
 * Grava os logs de ativação ou desativação da empresa no módulo fiscal
 *
 * Class EmpresaAtivacaoLog
 *
 * @property integer user_id
 * @property integer status_id
 * @property integer empresa_id
 * @property string  descricao
 *
 * @package Amlurb
 */
class EmpresasAtivacaoLog extends Model
{

    protected $fillable = [
        'user_id',
        'status_id',
        'empresa_id',
        'descricao',
    ];
}
