<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class JustificativaInconsistenciaVeiculos extends Model
{
    protected $table = 'justificativa_inconsistencia_veiculos';

    protected $fillable = [
        'justificativa_id', // texto da justificativa
        'fiscal_id', // usuario que digitou justificativa
        'empresa_id', // empresa que teve dados reprovados
        'empresas_veiculo_id' // veiculo que teve dados reprovados
        
    ];

    public function justificativa()
    {
        return $this->hasMany(Justificativa::class, 'id', 'justificativa_id');
    }

    public function justificativaLast()
    {
        return $this->hasOne(Justificativa::class, 'id', 'justificativa_id')->latest();
    }
}
