<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Status
 * 
 * @property int $id
 * @property string $descricao
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $cidades
 * @property \Illuminate\Database\Eloquent\Collection $colaboradores_numeros
 * @property \Illuminate\Database\Eloquent\Collection $empresas
 * @property \Illuminate\Database\Eloquent\Collection $empresas_tipos
 * @property \Illuminate\Database\Eloquent\Collection $empresas_veiculos
 * @property \Illuminate\Database\Eloquent\Collection $empresas_x_empresas_tipos
 * @property \Illuminate\Database\Eloquent\Collection $empresas_x_equipamentos
 * @property \Illuminate\Database\Eloquent\Collection $energia_consumos
 * @property \Illuminate\Database\Eloquent\Collection $equipamentos
 * @property \Illuminate\Database\Eloquent\Collection $frequencia_coleta
 * @property \Illuminate\Database\Eloquent\Collection $frequencia_geracaos
 * @property \Illuminate\Database\Eloquent\Collection $ramo_atividade
 * @property \Illuminate\Database\Eloquent\Collection $tipo_ramo_atividade
 * @property \Illuminate\Database\Eloquent\Collection $ctre
 *
 * @package Amlurb\Models
 */
class Status extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'status';

	protected $fillable = [
		'descricao'
	];

    public function estados()
    {
        return $this->hasMany(\Amlurb\Models\Estado::class);
    }

	public function cidades()
	{
		return $this->hasMany(\Amlurb\Models\Cidade::class);
	}

	public function colaboradores_numeros()
	{
		return $this->hasMany(\Amlurb\Models\ColaboradoresNumero::class);
	}

	public function empresas()
	{
		return $this->hasMany(\Amlurb\Models\Empresa::class);
	}

	public function empresas_tipos()
	{
		return $this->hasMany(\Amlurb\Models\EmpresasTipo::class);
	}

	public function empresas_veiculos()
	{
		return $this->hasMany(\Amlurb\Models\EmpresasVeiculo::class);
	}

	public function empresas_x_empresas_tipos()
	{
		return $this->hasMany(\Amlurb\Models\EmpresasXEmpresasTipo::class);
	}

	public function empresas_x_equipamentos()
	{
		return $this->hasMany(\Amlurb\Models\EmpresasXEquipamento::class);
	}

	public function energia_consumos()
	{
		return $this->hasMany(\Amlurb\Models\EnergiaConsumo::class);
	}

	public function equipamentos()
	{
		return $this->hasMany(\Amlurb\Models\Equipamento::class);
	}

	public function frequencia_coleta()
	{
		return $this->hasMany(\Amlurb\Models\FrequenciaColeta::class);
	}

	public function frequencia_geracaos()
	{
		return $this->hasMany(\Amlurb\Models\FrequenciaGeracao::class);
	}

	public function ramo_atividade()
	{
		return $this->hasMany(\Amlurb\Models\RamoAtividade::class);
	}

	public function tipo_equipamentos()
    {
        return $this->hasMany(\Amlurb\Models\TipoEquipamento::class);
    }

	public function tipo_ramo_atividade()
	{
		return $this->hasMany(\Amlurb\Models\TipoRamoAtividade::class);
	}

    public function ctre()
    {
        return $this->hasMany(Ctre::class);
    }

    public function vinculos()
    {
        return $this->hasMany(Vinculos::class);
    }

}
