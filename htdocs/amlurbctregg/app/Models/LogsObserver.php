<?php

namespace Amlurb\Models;

use Amlurb\Models\ActivityLog;
use Illuminate\Support\Facades\Auth;

class LogsObserver
{

    public function creating($model)
    {
        if(Auth::check()){
            $ip = \Request::ip();
            $geolocation = \Location::get($ip);
            $tableName = $model->getTable();
            $original = $model->getOriginal();
            $new = $model->toJson();
            $action = 'create';
            $idEmpresa = Auth::user()->empresa_id;

            $create = [
                'ip' => $ip,
                'country_code' => $geolocation->countryCode,
                'region_name' => $geolocation->regionName,
                'city_name' => $geolocation->cityName,
                'zip_code' => $geolocation->zipCode,
                'geolocation' => $geolocation->latitude . " " . $geolocation->longitude,
                'empresa_id'=>$idEmpresa,
                'tabela'=>$tableName,
                'new'=>$new,
                'action'=>$action
            ];
            ActivityLog::create($create);
        }
    }

    public function updating($model)
    {
        if(Auth::check()){
            $ip = \Request::ip();
            $geolocation = \Location::get($ip);
            $tableName = $model->getTable();
            $original = $model->getOriginal();
            $old = [];
            $new = [];
            $action = 'update';
            $idEmpresa = Auth::user()->empresa_id;

            if($model->status_id == 3){
                $action = 'delete';
            }
            if(empty($original)){
                $new = $model->toArray();
                $old = [0];
                $action = 'create';
            }else{

                foreach($model->toArray() as $key => $value){
                    if(isset($original[$key]) && $value != $original[$key] && $key != 'updated_at'){
                        $old[$key] = $original[$key];
                        $new[$key] = $value;
                    }
                }
            }
            if(!empty($old)){

                $create = [
                    'ip' => $ip,
                    'country_code' => $geolocation->countryCode,
                    'region_name' => $geolocation->regionName,
                    'city_name' => $geolocation->cityName,
                    'zip_code' => $geolocation->zipCode,
                    'geolocation' => $geolocation->latitude . " " . $geolocation->longitude,
                    'empresa_id'=>$idEmpresa,
                    'tabela'=>$tableName,
                    'id_tabela'=>$model->id,
                    'old'=>json_encode($old),
                    'new'=>json_encode($new),
                    'action'=>$action
                ];
                ActivityLog::create($create);
            }
        }
    }

    /*
    public function saving($model)
    {

    }
    */

    public function deleting($model)
    {
        if(Auth::check()){
            $ip = \Request::ip();
            $geolocation = \Location::get($ip);
            $tableName = $model->getTable();
            $action = 'delete';
            $idEmpresa = Auth::user()->empresa_id;

            $create = [
                'ip' => $ip,
                'country_code' => $geolocation->countryCode,
                'region_name' => $geolocation->regionName,
                'city_name' => $geolocation->cityName,
                'zip_code' => $geolocation->zipCode,
                'geolocation' => $geolocation->latitude . " " . $geolocation->longitude,
                'empresa_id'=>$idEmpresa,
                'tabela'=>$tableName,
                'id_tabela'=>$model->id,
                'old'=>$model->toJson(),
                'new'=>'[]',
                'action'=>$action
            ];
            ActivityLog::create($create);
        }
    }
}