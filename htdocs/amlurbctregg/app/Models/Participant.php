<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Participant
 * 
 * @property int $id
 * @property int $thread_id
 * @property int $user_id
 * @property \Carbon\Carbon $last_read
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Participant extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'thread_id' => 'int',
		'user_id' => 'int'
	];

	protected $dates = [
		'last_read'
	];

	protected $fillable = [
		'thread_id',
		'user_id',
		'last_read'
	];
}
