<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 07 Nov 2017 17:52:18 +0000.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Perfi
 * 
 * @property int $id
 * @property string $nome
 * @property int $empresa_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Amlurb\Models\Empresa $empresa
 * @property \Illuminate\Database\Eloquent\Collection $perfil_x_acessos
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package Amlurb\Models
 */
class Perfil extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'empresa_id' => 'int',
		'acesso' 	 => 'text'
	];

	protected $table = 'perfis';

	protected $fillable = [
		'nome',
		'acesso',
		'empresa_id'
	];

	public function empresa()
	{
		return $this->belongsTo(Empresa::class);
	}


	public function users()
	{
		return $this->hasMany(User::class, 'perfil_id');
	}

	public function emails()
	{
		return $this->belongsToMany(TiposEmail::class, 'tipos_email_x_perfil', 'perfil_id', 'tipos_emails_id');
	}
}