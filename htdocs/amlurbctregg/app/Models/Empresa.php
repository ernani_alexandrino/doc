<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use App\Models\PermissionsPrivilegio;
use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Empresa
 *
 * @property int                                      $id
 * @property string                                   $id_limpurb
 * @property int                                      $status_id
 * @property int                                      $cnpj
 * @property string                                   $razao_social
 * @property string                                   $nome_comercial
 * @property int                                      $ie
 * @property int                                      $im
 * @property string                                   $segmento
 * @property string                                   $telefone
 * @property string                                   $nome_responsavel
 * @property string                                   $email
 * @property string                                   $telefone_responsavel
 * @property string                                   $ramal
 * @property string                                   $celular
 * @property string                                   $cargo
 * @property string                                   $id_base_amlurb
 * @property string                                   $id_limpurb_base_amlurb
 * @property \Carbon\Carbon                           $data_cadastro_base_amlurb
 * @property \Carbon\Carbon                           $data_publicacao_dom_base_amlurb
 * @property \Carbon\Carbon                           $created_at
 * @property \Carbon\Carbon                           $updated_at
 * @property string                                   $deleted_at
 *
 * @property \Amlurb\Models\Status                    $status
 * @property \Illuminate\Database\Eloquent\Collection $empresa_endereco
 * @property \Illuminate\Database\Eloquent\Collection $empresa_informacao_complementar
 * @property \Illuminate\Database\Eloquent\Collection $empresa_qrcodes
 * @property \Illuminate\Database\Eloquent\Collection $empresa_responsavels
 * @property \Illuminate\Database\Eloquent\Collection $empresas_veiculos
 * @property \Illuminate\Database\Eloquent\Collection $empresas_x_empresas_tipos
 * @property \Illuminate\Database\Eloquent\Collection $equipamentos
 * @property \Illuminate\Database\Eloquent\Collection $perfis
 * @property \Illuminate\Database\Eloquent\Collection $permissions_privilegios
 * @property \Illuminate\Database\Eloquent\Collection $protocolos
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package Amlurb\Models
 */
class Empresa extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'status_id' => 'int',
        'cnpj' => 'string',
        'ie' => 'int',
        'im' => 'string',
        'updated_at' => 'date',
        'id_matriz' => 'int'
    ];

    protected $fillable = [
        'id_matriz',
        'status_id',
        'cnpj',
        'razao_social',
        'nome_comercial',
        'id_limpurb',
        'ie',
        'im',
        'segmento',
        'telefone',
        'ramal',
        'celular',
        'email',
        'nome_responsavel',
        'telefone_responsavel',
        'cargo',
    ];

    protected $dates = ['validade_cadastro'];

    public function empresa_responsavel(){
        return $this->hasOne(EmpresaResponsavel::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function empresa_endereco()
    {
        return $this->hasOne(EmpresaEndereco::class);
    }

    public function empresa_informacao_complementar()
    {
        return $this->hasOne(EmpresaInformacaoComplementars::class);
    }

    public function empresa_qrcodes()
    {
        return $this->hasOne(QrcodeEmpresas::class);
    }

    public function empresas_veiculos()
    {
        return $this->hasMany(EmpresasVeiculo::class);
    }

    public function empresa_documentos()
    {
        return $this->hasMany(EmpresaDocumentos::class);
    }

    public function empresas_x_empresas_tipos()
    {
        return $this->hasOne(EmpresasXEmpresasTipo::class);
    }
  
    public function empresas_x_equipamentos()
    {
        return $this->hasMany(EmpresasXEquipamento::class, 'empresa_id', 'id');
    }

    public function equipamentos()
    {
        return $this->belongsToMany(Equipamento::class, 'empresas_x_equipamentos')
            ->withPivot('id', 'quantidade', 'status_id', 'deleted_at')
            ->withTimestamps();
    }

    public function equipamento_alocacao()
    {
        return $this->hasMany(EquipamentoAlocacao::class, 'empresa_id', 'id');
    }

    public function logistica_residuos()
    {
        return $this->hasMany(LogisticaResiduos::class, 'empresa_id', 'id');
    }

    public function logistica_residuos_vinculador()
    {
        return $this->hasMany(LogisticaResiduos::class, 'vinculador_id', 'id');
    }

    public function permissions_privilegios()
    {
        return $this->hasOne(PermissionsPrivilegio::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function protocolos()
    {
        return $this->hasMany(Protocolo::class);
    }

    // vinculos registrados pela empresa
    public function vinculos()
    {
        return $this->hasMany(Vinculos::class, 'vinculador_id', 'id');
    }

    // vinculos associados a uma empresa
    public function empresa_vinculada()
    {
        return $this->belongsTo(Vinculos::class, 'id', 'empresa_vinculada_id');
    }

    public function activity_log()
    {
        return $this->hasMany(ActivityLog::class);
    }

    public function termos_de_uso_empresa()
    {
        return $this->belongsTo(EmpresasXTermosDeUso::class);
    }

    public function empresa_aprovacao()
    {
        return $this->hasOne(EmpresaAprovacao::class)->latest('id');
    }

    /**
     * Busca informações de empresas que vieram da Base Amlurb e estão inativas no sistema
     *
     * @param $cnpj
     *
     * @return object
     */
    public static function findCnpjBaseAmlurb($cnpj)
    {
        $empresa = Empresa::where('cnpj', $cnpj)->with('users')->first();

        return $empresa;
    }

    /**
     * Busca informações de empresas já cadastradas no sistema e ativas
     *
     * @param $cnpj
     *
     * @return \Illuminate\Support\Collection
     */
    public static function findCnpjAtivo($cnpj)
    {
        $empresa = Empresa::where('cnpj', $cnpj)->where('status_id', config('enums.status.ativo'))->get();

        return $empresa;
    }

    public function getValidadeCadastroAttribute($value)
    {
        if (!is_null($value)) {
            $dt = new Carbon($value);
            return $dt->format('d/m/Y');
        }
        return '--/--/----';
    }

}
