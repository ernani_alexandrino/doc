<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class QrcodeEquipamentos extends Model
{
    protected $table  = 'qrcode_equipamentos';

    protected $fillable = ['qrcode_id', 'equipamento_id'];

    public function equipamento()
    {
        return $this->belongsTo(EmpresasXEquipamento::class, 'equipamento_id', 'id');
    }
}