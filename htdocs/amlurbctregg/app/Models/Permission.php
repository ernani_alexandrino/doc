<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Permission
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property int $parent_id
 * @property int $level
 * @property string $type
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $permission_x_actions
 *
 * @package Amlurb\Models
 */
class Permission extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $casts = [
        'parent_id' => 'int',
        'level' => 'int'
    ];

    protected $fillable = [
        'name',
        'label',
        'parent_id',
        'level',
        'type'
    ];

    public function actions()
    {
        return $this->hasMany(\Amlurb\Models\PermissionXAction::class);
    }

    public function childrens()
    {
        return $this->hasMany(\Amlurb\Models\Permission::class, 'parent_id');
    }
}
