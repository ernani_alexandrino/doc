<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vinculos extends Model
{
    use SoftDeletes;

    // se vinculador_id for de transportador, representa geradores e destinos finais vinculados
    // caso contrario, representa transportadores vinculados
    // feito dessa maneira para que vinculos sejam unilaterais, de modo que a mesma empresa possa
    ///// criar, editar e remover seus vinculos, e as empresas vinculadas nao acessem esses dados

    protected $table = 'vinculos';

    protected $fillable = [
        'vinculador_id', 'empresa_vinculada_id', 'empresa_vinculada_tipo_id', 'empresa_responsavel_id',
        'frequencia_coleta_id', 'status_id', 'volume_residuo'
    ];

    protected $dates = ['deleted_at'];

    public function vinculador()
    {
        return $this->belongsTo(Empresa::class, 'vinculador_id', 'id');
    }

    public function empresa_vinculada()
    {
        return $this->belongsTo(Empresa::class, 'empresa_vinculada_id', 'id');
    }

    public function empresa_responsavel()
    {
        return $this->belongsTo(EmpresaResponsavel::class, 'empresa_responsavel_id', 'id');
    }
    public function empresas_tipo()
    {
        return $this->belongsTo(EmpresasTipo::class, 'empresa_vinculada_tipo_id', 'id');
    }

    public function frequencia_coleta()
    {
        return $this->belongsTo(FrequenciaColeta::class, 'frequencia_coleta_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

}