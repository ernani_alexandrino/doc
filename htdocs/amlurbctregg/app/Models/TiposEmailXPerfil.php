<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TiposEmailXPerfil
 * 
 * @property int $tipos_emails_id
 * @property int $perfil_id
 * 
 * @property \App\Models\Perfi $perfi
 * @property \App\Models\TiposEmail $tipos_email
 *
 * @package App\Models
 */
class TiposEmailXPerfil extends Eloquent
{
	protected $table = 'tipos_email_x_perfil';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'tipos_emails_id' => 'int',
		'perfil_id' => 'int'
	];

	protected $fillable = [
		'tipos_emails_id',
		'perfil_id'
	];

	public function perfi()
	{
		return $this->belongsTo(\App\Models\Perfi::class, 'perfil_id');
	}

	public function tipos_email()
	{
		return $this->belongsTo(\App\Models\TiposEmail::class, 'tipos_emails_id');
	}
}
