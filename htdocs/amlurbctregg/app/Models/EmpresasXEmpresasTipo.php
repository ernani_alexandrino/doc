<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresasXEmpresasTipo
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $empresa_tipo_id
 * @property int $status_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Amlurb\Models\Status $status
 * @property \Amlurb\Models\Empresa $empresa
 * @property \Amlurb\Models\EmpresasTipo $empresas_tipo
 *
 * @package Amlurb\Models
 */
class EmpresasXEmpresasTipo extends Eloquent
{
   // use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'empresas_x_empresas_tipo';

    protected $casts = [
        'empresa_id' => 'int',
        'empresa_tipo_id' => 'int',
        'status_id' => 'int'
    ];

    protected $fillable = [
        'empresa_id',
        'empresa_tipo_id',
        'status_id'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function empresas_tipo()
    {
        return $this->belongsTo(EmpresasTipo::class, 'empresa_tipo_id', 'id');
    }

    public static function boot()
    {
        parent::boot();
        self::observe(LogsObserver::class);
    }
}
