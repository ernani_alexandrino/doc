<?php

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresaInformacaoComplementars
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $frequencia_geracao_id
 * @property int $frequencia_coleta_id
 * @property int $ramo_atividade_id
 * @property int $tipo_ramo_atividade_id
 * @property int $colaboradores_numero_id
 * @property int $energia_consumo_id
 * @property int $estabelecimento_tipo_id
 * @property int $area_total
 * @property int $area_construida
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package Amlurb\Models
 */
class EmpresaInformacaoComplementars extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'empresa_informacao_complementar';

    protected $casts = [
        'empresa_id' => 'int',
        'frequencia_geracao_id' => 'int',
        'frequencia_coleta_id' => 'int',
        'ramo_atividade_id' => 'int',
        'tipo_ramo_atividade_id' => 'int',
        'colaboradores_numero_id' => 'int',
        'energia_consumo_id' => 'int',
        'estabelecimento_tipo_id' => 'int',
        'area_total' => 'int',
        'area_construida' => 'int'
    ];

    protected $fillable = [
        'empresa_id',
        'frequencia_geracao_id',
        'frequencia_coleta_id',
        'ramo_atividade_id',
        'tipo_ramo_atividade_id',
        'colaboradores_numero_id',
        'energia_consumo_id',
        'estabelecimento_tipo_id',
        'area_total',
        'area_construida'
    ];

    public function energia_consumo()
    {
        return $this->belongsTo(EnergiaConsumo::class);
    }

    public function colaboradores_numero()
    {
        return $this->belongsTo(ColaboradoresNumero::class);
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function frequencia_coleta()
    {
        return $this->belongsTo(FrequenciaColeta::class, 'frequencia_coleta_id');
    }

    public function frequencia_geracao()
    {
        return $this->belongsTo(FrequenciaGeracao::class);
    }

    public function ramo_atividade()
    {
        return $this->belongsTo(RamoAtividade::class, 'ramo_atividade_id', 'id');
    }

    public function tipo_ramo_atividade()
    {
        return $this->belongsTo(TipoRamoAtividade::class, 'tipo_ramo_atividade_id', 'id');
    }

    public function estabelecimento_tipo()
    {
        return $this->belongsTo(EstabelecimentoTipo::class);
    }
}
