<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon;

/**
 * Class Ctre
 *
 * @property int                                      id
 * @property string                                   codigo
 * @property int                                      empresa_id
 * @property int                                      status_id
 * @property \Carbon\Carbon                           data_emissao
 * @property \Carbon\Carbon                           data_expiracao
 * @property \Carbon\Carbon                           data_validacao_final
 * @property \Carbon\Carbon                           created_at
 * @property \Carbon\Carbon                           updated_at
 * @property string                                   deleted_at
 * @property Empresa                                  empresa
 * @property Status                                   status
 *
 * @package Amlurb\Models
 */
class Ctre extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'ctre';

    protected $casts = [
        'empresa_id' => 'int',
        'status_id' => 'int',
        'data_emissao' => 'timestamp',
        'data_expiracao' => 'timestamp',
        'data_validacao' => 'timestamp',
        'data_validacao_final' => 'timestamp',
    ];

    protected $dates = [
        'data_emissao',
        'data_expiracao',
        'data_validacao',
        'data_validacao_final'
    ];

    protected $fillable = [
        'codigo',
        'empresa_id',
        'gerador_id',
        'transportador_id',
        'destino_id',
        'veiculo_id',
        'status_id',
        'placa_veiculo',
        'data_emissao',
        'data_expiracao',
        'data_validacao',
        'data_validacao_final',
        'volume_total'
    ];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function gerador()
    {
        return $this->belongsTo(Empresa::class, 'gerador_id', 'id');
    }

    public function transportador()
    {
        return $this->belongsTo(Empresa::class, 'transportador_id', 'id');
    }

    public function destino_final()
    {
        return $this->belongsTo(Empresa::class, 'destino_id', 'id');
    }

    public function empresas_veiculos()
    {
        return $this->belongsTo(EmpresasVeiculo::class, 'veiculo_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function ctre_equipamentos()
    {
        return $this->hasMany(CtreEquipamentos::class);
    }

    public function ctre_residuos()
    {
        return $this->hasMany(CtreResiduos::class);
    }

    public static function boot()
    {
        parent::boot();
        self::observe(LogsObserver::class);
    }

    public function getDataEmissaoAttribute($value)
    {
        return $this->checkNullDate($value);
    }

    public function getDataExpiracaoAttribute($value)
    {
        return $this->checkNullDate($value);
    }

    public function getDataValidacaoAttribute($value)
    {
        return $this->checkNullDate($value);
    }

    public function getDataValidacaoFinalAttribute($value)
    {
        return $this->checkNullDate($value);
    }

    /**
     * @param $value
     * @return null|string
     */
    private function checkNullDate($value)
    {
        if (is_null($value)) {
            return null;
        }
        return Carbon::parse($value);
    }

}
