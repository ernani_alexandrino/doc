<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 02 May 2018 12:02:08 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresasXTermosDeUso
 * 
 * @property int $id
 * @property int $empresa_id
 * @property int $termo_id
 * @property bool $termo_boleto
 * @property bool $termo_qr_code
 * @property bool $termo_uso
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Empresa $empresa
 * @property \Amlurb\Models\TermosDeUso $termos_de_uso
 *
 * @package Amlurb\Models
 */
class EmpresasXTermosDeUso extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'empresas_x_termos_de_uso';

	protected $casts = [
		'empresa_id' => 'int',
		'termo_id' => 'int',
		'termo_boleto' => 'bool',
		'termo_qr_code' => 'bool',
		'termo_uso' => 'bool'
	];

	protected $fillable = [
		'empresa_id',
		'termo_id',
		'termo_boleto',
		'termo_qr_code',
		'termo_uso'
	];

	public function empresa()
	{
		return $this->belongsTo(\Amlurb\Models\Empresa::class);
	}

	public function termos_de_uso()
	{
		return $this->belongsTo(\Amlurb\Models\TermosDeUso::class);
	}
}
