<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresaResponsavel
 * 
 * @property int $id
 * @property int $empresa_id
 * @property string $nome
 * @property string $email
 * @property string $telefone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Empresa $empresa
 *
 * @package Amlurb\Models
 */
class EmpresaResponsavel extends Eloquent
{
	use SoftDeletes;
	protected $table = 'empresa_responsavel';

	protected $casts = [
		'empresa_id' => 'int'
	];

	protected $fillable = [
		'empresa_id',
		'nome',
		'email',
		'telefone',
        'responsavel',
        'rg',
        'cpf'
	];

	protected $dates = ['deleted_at'];

	public function empresa()
	{
		return $this->belongsTo(Empresa::class);
	}

}
