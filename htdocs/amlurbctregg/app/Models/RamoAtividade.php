<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class RamoAtividade
 * 
 * @property int $id
 * @property int $status_id
 * @property string $nome
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Status $status
 * @property \Illuminate\Database\Eloquent\Collection $empresa_informacao_complementar
 * @property \Illuminate\Database\Eloquent\Collection $tipo_ramo_atividade
 *
 * @package Amlurb\Models
 */
class RamoAtividade extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'ramo_atividade';

	protected $casts = [
		'status_id' => 'int'
	];

	protected $fillable = [
		'status_id',
		'nome'
	];

	public function status()
	{
		return $this->belongsTo(Status::class);
	}

	public function empresa_informacao_complementar()
	{
		return $this->hasMany(EmpresaInformacaoComplementars::class, 'ramo_atividade_id', 'id');
	}

	public function tipo_ramo_atividade()
	{
		return $this->hasMany(TipoRamoAtividade::class);
	}
}
