<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class JustificativaInvalidacaoCtre extends Model
{
    protected $table = 'justificativa_invalidacao_ctre';

    protected $fillable = [
        'justificativa_id', // texto da justificativa
        'ctre_id', // CTRE que foi invalidado
        'empresa_id' // usuario que invalidou CTRE
    ];
}