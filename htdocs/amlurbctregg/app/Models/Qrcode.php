<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Gera o QRCode
 *
 * Class Qrcode
 *
 * @property integer id
 * @property string  tipo
 * @property string  codigo
 * @property integer status_id
 *
 * @package Amlurb\Models
 */
class Qrcode extends Model
{
    protected $table = 'qrcodes';

    protected $fillable = ['tipo', 'codigo', 'status_id'];

    public function qrcode_empresas()
    {
        return $this->hasOne(QrcodeEmpresas::class, 'qrcode_id', 'id');
    }

    public function qrcode_equipamentos()
    {
        return $this->hasOne(QrcodeEquipamentos::class, 'qrcode_id', 'id');
    }

    public function qrcode_veiculos()
    {
        return $this->hasOne(QrcodeVeiculos::class, 'qrcode_id', 'id');
    }

}