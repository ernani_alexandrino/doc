<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class EquipamentoAlocacao extends Model
{
    protected $table = 'equipamento_alocacao';

    protected $fillable = [
        'equipamento_id', 'empresa_id'
    ];

    public function empresas_x_equipamento()
    {
        return $this->belongsTo(\Amlurb\Models\EmpresasXEquipamento::class, 'equipamento_id', 'id');
    }

    public function empresa()
    {
        return $this->belongsTo(\Amlurb\Models\Empresa::class, 'empresa_id', 'id');
    }

}