<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PermissionXAction
 * 
 * @property int $id
 * @property string $name
 * @property string $label
 * @property int $permission_id
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Amlurb\Models\Permission $permission
 *
 * @package Amlurb\Models
 */
class PermissionXAction extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'permission_id' => 'int'
	];

	protected $fillable = [
		'name',
		'label',
		'permission_id'
	];

	public function permission()
	{
		return $this->belongsTo(\Amlurb\Models\Permission::class);
	}
}
