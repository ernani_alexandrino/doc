<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TiposEmail
 * 
 * @property int $id
 * @property string $nome
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $perfis
 *
 * @package Amlurb\Models
 */
class TiposEmail extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'nome'
	];

	public function perfis()
	{
		return $this->belongsToMany(\Amlurb\Models\Perfil::class, 'tipos_email_x_perfil', 'tipos_emails_id', 'perfil_id');
	}
}
