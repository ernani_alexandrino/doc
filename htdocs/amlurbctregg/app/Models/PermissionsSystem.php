<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PermissionsSystem
 * 
 * @property int $id
 * @property string $display_name
 * @property string $name
 * @property int $level
 * @property int $parent_id
 * @property int $excluido
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @package Amlurb\Models
 */
class PermissionsSystem extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'permissions_system';

	protected $casts = [
		'level' => 'int',
		'parent_id' => 'int',
		'excluido' => 'int'
	];

	protected $fillable = [
		'display_name',
		'name',
		'level',
		'parent_id',
		'excluido'
	];
}
