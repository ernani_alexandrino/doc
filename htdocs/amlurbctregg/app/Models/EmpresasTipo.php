<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresasTipo
 *
 * @property int $id
 * @property int $status_id
 * @property string $nome
 * @property string $sigla
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Amlurb\Models\Status $status
 * @property \Illuminate\Database\Eloquent\Collection $empresas
 *
 * @package Amlurb\Models
 */
class EmpresasTipo extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'empresas_tipo';

    protected $casts = [
        'status_id' => 'int'
    ];

    protected $fillable = [
        'status_id',
        'nome',
        'sigla'
    ];

    public function status()
    {
        return $this->belongsTo(\Amlurb\Models\Status::class);
    }

    public function empresas()
    {
        return $this->belongsToMany(\Amlurb\Models\Empresa::class, 'empresas_x_empresas_tipo', 'empresa_tipo_id')
            ->withPivot('id', 'status_id', 'deleted_at')
            ->withTimestamps();
    }
}
