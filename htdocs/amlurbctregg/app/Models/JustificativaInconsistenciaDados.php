<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class JustificativaInconsistenciaDados extends Model
{
    protected $table = 'justificativa_inconsistencia_dados';

    protected $fillable = [
        'justificativa_id', // texto da justificativa
        'fiscal_id', // usuario que digitou justificativa
        'empresa_id' // empresa que teve dados reprovados
        
    ];
}