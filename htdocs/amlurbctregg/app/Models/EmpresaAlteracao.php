<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresaAlteracao
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $responsible_user_id
 * @property int $reviewer_user_id
 * @property int $ramo_atividade_id
 * @property int $tipo_ramo_atividade_id
 * @property string $razao_social
 * @property string $nome_comercial
 * @property string $endereco
 * @property int $numero
 * @property string $bairro
 * @property string $cep
 * @property int $estado_id
 * @property int $cidade_id
 * @property string $complemento
 * @property string $ponto_referencia
 * @property int $ie
 * @property int $im
 * @property string $telefone
 * @property string $cartao_cnpj
 * @property string $numero_iptu
 * @property string $copia_iptu
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Amlurb\Models\Cidade $cidade
 * @property \Amlurb\Models\Estado $estado
 * @property \Amlurb\Models\Empresa $empresa
 * @property \Amlurb\Models\RamoAtividade $ramo_atividade
 * @property \Amlurb\Models\TipoRamoAtividade $tipo_ramo_atividade
 *
 * @package Amlurb\Models
 */
class EmpresaAlteracao extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'empresa_alteracao';

    protected $casts = [
        'empresa_id' => 'int',
        'responsible_user_id' => 'int',
        'reviewer_user_id' => 'int',
        'ramo_atividade_id' => 'int',
        'tipo_ramo_atividade_id' => 'int',
        'estado_id' => 'int',
        'cidade_id' => 'int',
        'numero' => 'int',
        'ie' => 'int',
        'im' => 'string',
    ];

    protected $fillable = [
        'empresa_id',
        'responsible_user_id',
        'reviewer_user_id',
        'ramo_atividade_id',
        'tipo_ramo_atividade_id',
        'razao_social',
        'nome_fantasia',
        'endereco',
        'numero',
        'bairro',
        'cep',
        'estado_id',
        'cidade_id',
        'complemento',
        'ponto_referencia',
        'ie',
        'im',
        'telefone',
        'empresa_cartao_cnpj',
        'empresa_num_iptu',
        'empresa_iptu',
    ];

    protected $dates = ['deleted_at'];

    public function cidade()
    {
        return $this->belongsTo(\Amlurb\Models\Cidade::class);
    }

    public function estado()
    {
        return $this->belongsTo(\Amlurb\Models\Estado::class);
    }

    public function empresa()
    {
        return $this->belongsTo(\Amlurb\Models\Empresa::class);
    }

    public function ramo_atividade()
    {
        return $this->belongsTo(\Amlurb\Models\RamoAtividade::class);
    }

    public function tipo_ramo_atividade()
    {
        return $this->belongsTo(\Amlurb\Models\TipoRamoAtividade::class);
    }

    public static function buscaAlteracao($id = 0)
    {
        return EmpresaAlteracao::where('empresa_id', $id)->first();
    }

}
