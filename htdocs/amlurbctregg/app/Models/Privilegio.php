<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Privilegio
 * 
 * @property int $id
 * @property string $privilegio
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $permissions_privilegios
 *
 * @package Amlurb\Models
 */
class Privilegio extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = [
		'privilegio'
	];

	public function permissions_privilegios()
	{
		return $this->hasMany(\Amlurb\Models\PermissionsPrivilegio::class);
	}
}
