<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class CtreEquipamentos extends Model
{
    protected $table = 'ctre_equipamentos';

    protected $fillable = [
        'ctre_id', 'equipamento_id'
    ];

    public function ctre()
    {
        return $this->belongsTo(Ctre::class);
    }

    public function empresas_x_equipamentos()
    {
        return $this->belongsTo(EmpresasXEquipamento::class, 'equipamento_id', 'id');
    }

}