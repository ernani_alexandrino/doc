<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 10 May 2018 17:07:12 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresaBoletoBaixa
 * 
 * @property int $id
 * @property int $empresa_boleto_id
 * @property string $anoExerEmiGuia
 * @property int $codTipoServGuia
 * @property int $codNumGuia
 * @property \Carbon\Carbon $dataPgtoGuia
 * @property float $valPgtoGuia
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Boleto $empresa_boleto
 *
 * @package Amlurb\Models
 */
class EmpresaBoletoBaixa extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'empresa_boleto_baixa';

	protected $casts = [
		'empresa_boleto_id' => 'int',
		'codTipoServGuia' => 'int',
		'codNumGuia' => 'int',
		'valPgtoGuia' => 'float'
	];

	protected $dates = [
		'dataPgtoGuia'
	];

	protected $fillable = [
		'empresa_boleto_id',
		'anoExerEmiGuia',
		'codTipoServGuia',
		'codNumGuia',
		'dataPgtoGuia',
		'valPgtoGuia'
	];

	public function empresa_boleto()
	{
		return $this->belongsTo(Boleto::class);
	}
}
