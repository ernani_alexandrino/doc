<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Preco extends Model
{
    use SoftDeletes;

    protected $table = 'precos';

    protected $fillable = ['tipo', 'valor'];

}