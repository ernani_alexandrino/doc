<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:08 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ActivityLog
 * 
 * @property int $id
 * @property string $ip
 * @property string $country_code
 * @property string $region_name
 * @property string $city_name
 * @property string $zip_code
 * @property string $geolocation
 * @property int $empresa_id
 * @property string $tabela
 * @property string $old
 * @property string $new
 * @property string $action
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Amlurb\Models\Empresa $empresa
 *
 * @package Amlurb\Models
 */
class ActivityLog extends Eloquent
{
	protected $casts = [
		'empresa_id' => 'int'
	];

	protected $fillable = [
		'ip',
		'country_code',
		'region_name',
		'city_name',
		'zip_code',
		'geolocation',
		'empresa_id',
		'tabela',
		'old',
		'new',
		'action'
	];

    public function empresa()
    {
        return $this->belongsTo(\Amlurb\Models\Empresa::class, 'empresa_id');
    }
}
