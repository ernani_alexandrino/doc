<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class JustificativaEmpresaAlteracao extends Model
{
    protected $table = 'justificativa_empresa_alteracao';

    protected $fillable = ['justificativa_id', 'empresa_alteracao_id'];

    public function empresa_alteracao()
    {
        return $this->belongsTo(EmpresaAlteracaoJson::class);
    }    

    public function justificativas()
    {
        return $this->hasOne(Justificativa::class);
    }    
}