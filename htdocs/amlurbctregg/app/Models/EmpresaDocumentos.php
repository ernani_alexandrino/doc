<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresaDocumentos extends Model
{
    protected $table = 'empresa_documentos';

    protected $fillable = [
        'empresa_id', 'documento_id', 'caminho_arquivo', 'data_emissao', 'data_vencimento'
    ];

    protected $dates = ['data_emissao', 'data_vencimento'];

	public function documentos()
	{
		return $this->belongsTo(Documento::class, 'documento_id', 'id');
	}
}