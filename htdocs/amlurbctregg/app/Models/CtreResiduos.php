<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class CtreResiduos extends Model
{
    protected $table = 'ctre_residuos';

    protected $fillable = [
        'ctre_id', 'residuo_id'
    ];

    public function ctre()
    {
        return $this->belongsTo(Ctre::class);
    }

    public function residuo()
    {
        return $this->belongsTo(Residuo::class);
    }

}