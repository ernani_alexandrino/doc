<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TipoRamoAtividade
 * 
 * @property int $id
 * @property int $ramo_atividade_id
 * @property int $status_id
 * @property string $codigo
 * @property string $nome
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Status $status
 * @property \Amlurb\Models\RamoAtividade $ramo_atividade
 * @property \Illuminate\Database\Eloquent\Collection $empresa_informacao_complementar
 *
 * @package Amlurb\Models
 */
class TipoRamoAtividade extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'tipo_ramo_atividade';

	protected $casts = [
		'ramo_atividade_id' => 'int',
		'status_id' => 'int'
	];

	protected $fillable = [
		'ramo_atividade_id',
		'status_id',
		'codigo',
		'nome'
	];

	public function status()
	{
		return $this->belongsTo(\Amlurb\Models\Status::class);
	}

	public function ramo_atividade()
	{
		return $this->belongsTo(\Amlurb\Models\RamoAtividade::class);
	}

    public function empresa_informacao_complementar()
    {
        return $this->hasMany(EmpresaInformacaoComplementars::class, 'tipo_ramo_atividade_id', 'id');
    }

}
