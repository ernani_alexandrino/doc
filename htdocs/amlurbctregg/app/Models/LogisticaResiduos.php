<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * representa os residuos que sao produzidos por uma empresa (gerador)
 *
 * Class LogisticaResiduos
 * @package App\Models
 */
class LogisticaResiduos extends Model
{
    protected $table = 'logistica_residuos';

    /*
     * se vinculador for transporador, empresa_id sera do gerador ou destino final
     * se vinculador for gerador ou destino final, empresa_id sera do transportador
     */

    protected $fillable = [
        'vinculador_id', 'empresa_id', 'residuo_id'
    ];

    public function vinculador()
    {
        return $this->belongsTo(Empresa::class, 'vinculador_id', 'id');
    }

    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresa_id', 'id');
    }

    public function residuo()
    {
        return $this->belongsTo(Residuo::class, 'residuo_id', 'id');
    }

}