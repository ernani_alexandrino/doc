<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EnergiaConsumo
 *
 * @property int $id
 * @property string $nome
 * @property int $quantidade
 * @property int $status_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Amlurb\Models\Status $status
 * @property \Illuminate\Database\Eloquent\Collection $empresa_informacao_complementar
 *
 * @package Amlurb\Models
 */
class EnergiaConsumo extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'energia_consumo';

    protected $casts = [
        'quantidade' => 'int',
        'status_id' => 'int'
    ];

    protected $fillable = [
        'nome',
        'quantidade',
        'status_id'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function empresa_informacao_complementar()
    {
        return $this->hasMany(EmpresaInformacaoComplementars::class);
    }
}
