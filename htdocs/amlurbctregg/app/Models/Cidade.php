<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:08 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cidade
 * 
 * @property int $id
 * @property int $estados_id
 * @property \Amlurb\Models\Estado $estado
 * @property \Illuminate\Database\Eloquent\Collection $empresa_endereco
 *
 * @package Amlurb\Models
 */
class Cidade extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'estados_id' => 'int',
		'status_id' => 'int'
	];

	protected $fillable = [
		'estados_id',
		'status_id',
		'nome'
	];

	public function status()
	{
		return $this->belongsTo(\Amlurb\Models\Status::class);
	}

	public function estado()
	{
		return $this->belongsTo(\Amlurb\Models\Estado::class, 'estados_id');
	}

	public function empresa_endereco()
	{
		return $this->hasMany(\Amlurb\Models\EmpresaEndereco::class);
	}

}
