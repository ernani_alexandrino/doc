<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Amlurb\Traits\PermissionsUserTrait;
use Amlurb\Notifications\enviaEmaildeDefinicaodeSenha;
use Cmgmyr\Messenger\Traits\Messagable;


/**
 * Class User
 * 
 * @property int $id
 * @property int $empresa_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property int $status_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $perfil_id
 * @property \Amlurb\Models\Empresa $empresa
 * @property \Illuminate\Database\Eloquent\Collection $user_perfils
 *
 * @package Amlurb\Models
 */
class User extends Authenticatable
{
	use PermissionsUserTrait;
	use Notifiable;
	use \Illuminate\Database\Eloquent\SoftDeletes;
    use Messagable;
	protected $casts = [
		'empresa_id' => 'int'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'empresa_id',
		'name',
		'sobrenome',
		'email',
		'password',
		'remember_token',
		'perfil_id',
        'status_id'
	];

	protected $relationPerfil = 'perfil';
    protected $columnAcesso = 'acesso';

	public function empresa()
	{
		return $this->belongsTo(Empresa::class);
	}

	public function user_perfils()
	{
		return $this->hasOne(UserPerfil::class);
	}

	public function empresa_tipo()
	{
		return $this->hasOne(EmpresasXEmpresasTipo::class);
	}
	public function perfil()
	{
		return $this->belongsTo(Perfil::class, 'perfil_id');
	}

	public function checkEmpresaTipo($tipo){
	    $tipoEmpresa = $this->empresa_tipo->empresas_tipo->sigla;

	    if($tipo == $tipoEmpresa){
	        return true;
        }

        return false;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new enviaEmaildeDefinicaodeSenha($token));
    }
}
