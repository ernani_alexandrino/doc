<?php

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

class JustificativaExclusaoEquipamento extends Model
{
    protected $table = 'justificativa_exclusao_equipamento';

    protected $fillable = [
        'justificativa_id', // texto da justificativa
        'equipamento_id', // equipamento que foi removido
        'empresa_id' // empresa que possuia o equipamento
    ];

}