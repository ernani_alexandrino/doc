<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class EmpresasVeiculo
 * 
 * @property int $id
 * @property int $empresa_id
 * @property int $status_id
 * @property string $placa
 * @property int $ano
 * @property int $renavam
 * @property string $tipo
 * @property int $capacidade
 * @property int $tara
 * @property string $marca
 * @property \Carbon\Carbon $vencimento_ipva
 * @property \Carbon\Carbon $data_validade
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Status $status
 * @property \Amlurb\Models\Empresa $empresa
 *
 * @package Amlurb\Models
 */
class EmpresasVeiculo extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'empresa_id' => 'int',
		'status_id' => 'int',
		'ano_veiculo' => 'int',
		'renavam' => 'string',
		'capacidade' => 'int',
		'tara' => 'int'
	];

	protected $dates = [
		'vencimento_ipva', 'data_validade'
	];

	protected $fillable = [
		'empresa_id',
		'status_id',
        'codigo_amlurb',
		'placa',
		'ano_veiculo',
		'renavam',
		'tipo',
		'capacidade',
		'tara',
		'marca',
		'vencimento_ipva',
		'data_validade',
		'anexo_documento_veiculo',
		'anexo_documento_inmetro',
		'anexo_comodato_veiculo',
		'numero_inmetro'
	];

	public function status()
	{
		return $this->belongsTo(\Amlurb\Models\Status::class);
	}

	public function empresa()
	{
		return $this->belongsTo(\Amlurb\Models\Empresa::class);
	}

	public function ctre()
    {
        return $this->hasMany(Ctre::class);
    }

    public static function boot()
    {
        parent::boot();
        self::observe(LogsObserver::class);
    }
}
