<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserPerfil
 *
 * @property int $id
 * @property int $user_id
 * @property \Carbon\Carbon $data_nascimento
 * @property string $cargo
 * @property string $telefone
 * @property string $ramal
 * @property string $celular
 * @property string $imagem_perfil
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \Amlurb\Models\User $user
 *
 * @package Amlurb\Models
 */
class UserPerfil extends Eloquent
{
   use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'user_perfil';

    protected $casts = [
        'user_id' => 'int'
    ];

    protected $dates = [
        'data_nascimento'
    ];

    protected $fillable = [
        'user_id',
        'data_nascimento',
        'cargo',
        'telefone',
        'ramal',
        'celular',
        'imagem_perfil'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDataNascimentoAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }

    public function setDataNascimentoAttribute($value)
    {
        if (isset($value) && $value != '') {
            $this->attributes['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $value);
        }
    }
}
