<?php

namespace Amlurb\Models;

use Amlurb\Models\Empresa;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Protocolo
 *
 * @property int $empresa_id
 * @property string $numero_protocolo
 * @property string $nome
 * @property string $descricao
 * @property \Carbon\Carbon $created_at
 *
 * @package Amlurb\Models
 */
class Protocolo extends Model
{
    protected $casts = [
        'empresa_id' => 'int',
        'nome' => 'string',
    ];

    protected $fillable = [
        'empresa_id',
        'numero_protocolo',
        'nome',
        'descricao'
    ];


    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }
}
