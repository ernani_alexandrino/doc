<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Estado
 * 
 * @property int $id
 * @property string $nome
 * @property string $sigla
 * 
 * @property \Illuminate\Database\Eloquent\Collection $cidades
 *
 * @package Amlurb\Models
 */
class Estado extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'nome',
		'sigla'
	];

	public function cidades()
	{
		return $this->hasMany(\Amlurb\Models\Cidade::class, 'estados_id');
	}

    public function empresa_endereco()
    {
        return $this->hasMany(\Amlurb\Models\EmpresaEndereco::class, 'estado_id');
    }

    public function status()
    {
        return $this->belongsTo(\Amlurb\Models\Status::class)->where('status_id', 1);
    }


}
