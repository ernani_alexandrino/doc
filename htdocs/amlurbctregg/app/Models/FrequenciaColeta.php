<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 13 Sep 2017 17:37:47 +0000.
 */

namespace Amlurb\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FrequenciaColeta
 * 
 * @property int $id
 * @property string $nome
 * @property int $quantidade
 * @property int $status_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Status $status
 * @property \Illuminate\Database\Eloquent\Collection $empresa_informacao_complementar
 *
 * @package Amlurb\Models
 */
class FrequenciaColeta extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'frequencia_coleta';

	protected $casts = [
		'quantidade' => 'int',
		'status_id' => 'int'
	];

	protected $fillable = [
		'nome',
		'quantidade',
		'status_id'
	];

	public function status()
	{
		return $this->belongsTo(\Amlurb\Models\Status::class);
	}

	public function empresa_informacao_complementar()
	{
		return $this->hasMany(\Amlurb\Models\EmpresaInformacaoComplementars::class, 'frequencia_coleta_id');
	}
}
