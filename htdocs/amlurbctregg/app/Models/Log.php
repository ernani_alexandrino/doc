<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Log
 * 
 * @property int $id
 * @property \Carbon\Carbon $hora
 * @property string $ip
 * @property string $user
 * @property string $mensagem
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Log extends Eloquent
{
	protected $dates = [
		'hora'
	];

	protected $fillable = [
		'hora',
		'ip',
		'user',
		'mensagem'
	];
}
