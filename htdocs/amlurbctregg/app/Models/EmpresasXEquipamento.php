<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 19 Feb 2018 20:19:09 -0300.
 */

namespace Amlurb\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class EmpresasXEquipamento
 *
 * esta classe representa o pertencimento de um equipamento a uma empresa
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $equipamento_id
 * @property int $status_id
 * @property \Carbon\Carbon $data_validade
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Amlurb\Models\Status $status
 * @property \Amlurb\Models\Empresa $empresa
 * @property \Amlurb\Models\Equipamento $equipamento
 *
 * @package Amlurb\Models
 */
class EmpresasXEquipamento extends Model
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'empresa_id' => 'int',
		'equipamento_id' => 'int',
		'status_id' => 'int'
	];

	protected $dates = ['data_validade'];

	protected $fillable = [
		'empresa_id',
		'equipamento_id',
		'status_id',
		'quantidade',
        'codigo_amlurb',
        'data_validade'
	];

	public function status()
	{
		return $this->belongsTo(\Amlurb\Models\Status::class);
	}

	public function empresa()
	{
		return $this->belongsTo(\Amlurb\Models\Empresa::class);
	}

	public function equipamento()
	{
		return $this->belongsTo(Equipamento::class, 'equipamento_id', 'id');
	}

    public function equipamento_alocacao()
    {
        return $this->hasOne(EquipamentoAlocacao::class, 'equipamento_id', 'id');
    }

    public function ctre_equipamento()
    {
        return $this->hasMany(CtreEquipamentos::class, 'equipamento_id', 'id');
    }

    public static function boot()
    {
        parent::boot();
        self::observe(LogsObserver::class);
    }

}
