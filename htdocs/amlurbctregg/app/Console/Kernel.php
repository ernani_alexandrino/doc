<?php

namespace Amlurb\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $schedule->command('firebase:empresas')
                  ->everyTenMinutes();

        $schedule->command('firebase:inconsistencia')
                  ->hourly();

        $schedule->command('firebase:coletas')
                  ->everyThirtyMinutes();

        $schedule->command('firebase:coletas_excluidas')
                  ->dailyAt('00:30');

        $schedule->command('firebase:drop')
                  ->dailyAt('00:00');

        $schedule->command('dispatch:ctre:abertos')
                  ->hourly();

        $schedule->command('dispatch:boletos')
                  ->dailyAt('01:00');
          
        $schedule->command('geolocate:address')
                  ->dailyAt('02:00');

        $schedule->command('email:vinculo')
                  ->dailyAt('07:00');

        // verifica se servico esta rodando, e inicia de novo se nao estiver
        // tarefa delegada para SUPERVISOR // 2018-10-05
        //if (!strstr(shell_exec('ps xf'), 'php artisan queue:work')) {
        //    $schedule->command('queue:work')
        //        ->everyTenMinutes();
        //}
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
