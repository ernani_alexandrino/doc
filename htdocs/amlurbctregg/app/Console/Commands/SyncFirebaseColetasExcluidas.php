<?php

namespace Amlurb\Console\Commands;

use Illuminate\Console\Command;
use Amlurb\Models\ColetasExcluidasApp;

use Firebase,File,Hash;

class SyncFirebaseColetasExcluidas extends Command
{
    /**
     * The name and signature of the console command.
     * 
     * @var string
     */
    protected $signature = 'firebase:coletas_excluidas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coletasFB =  json_decode(Firebase::get('/coletas_excluidas/'),true);
        if (!is_iterable($coletasFB)) {
            return; // nao faz nada
        }

        foreach($coletasFB AS $userId => $coletas){

            foreach($coletas AS $chaveFirebase => $coleta){ 

                ColetasExcluidasApp::create([
                    'user_id' => $userId,
                    'data_coleta' =>$coleta['dataColeta'],
                    'destino' => $coleta['destino'],
                    'gerador' => $coleta['gerador'],
                    'veiculo' => $coleta['veiculo'],
                    'motivo' => $coleta['motivo'],
                    'equipamento' => $coleta['nome'],
                    'qrcode_equipamento' => $coleta['qrcode'],
                    'data_hora_coleta_iso' => $coleta['horaDataColeta'],
                    ]);

                Firebase::delete('/coletas_excluidas/'.$userId.'/'.$chaveFirebase);

            }
        }
    }
}
