<?php

namespace Amlurb\Console\Commands;

use Illuminate\Console\Command;
use Amlurb\Models\BoletoVeiculos;
use Amlurb\Models\Boleto;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;
use Excel;
use DB;

class active_veiculos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'read:veiculos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $dados = $this->readExcel();
        foreach ($dados as $dado) {
                   $veiculo = EmpresasVeiculo::where(DB::raw('LOWER (placa) '), mb_strtolower($dado['nr_placa_veiculo']))->first();
            if (!empty($veiculo)) {
                    //cria dados falsos para o boleto
                    $dadosBoleto['dadosGuia'] = [
                        'empresa_id' => $veiculo->empresa_id,
                        'tipo' => 'veiculo',
                        'anoEmissao' => $dado['dt_validade']->format('Y'),
                        'numeroGuia' => $veiculo->empresa_id,
                        'valor' => 0,
                        'status_id' => \config('enums.status.pago'),
                        'codTipoServGuia' => 0,
                        'arquivoBoleto' => 'fake',
                        'dataDeValidade' => $dado['dt_validade']->format('Y-m-d'),
                        'dataDeVencimento' => $dado['dt_validade']->format('Y-m-d'),
                        'dataDePagamento' => $dado['dt_validade']->format('Y-m-d')
                    ];
                    $boletoFake =  $this->salvarBoleto($dadosBoleto, $veiculo->empresa_id, config('enums.codigo_tipo_servico.veiculo'));
                    
                    BoletoVeiculos::firstOrCreate([
                        'boleto_id' => $boletoFake->id,
                        'veiculo_id' => $veiculo->id
                    ]);
                    $status = \config('enums.status.ativo');
                    $veiculo = EmpresasVeiculo::find($veiculo->id)->update(
                        [
                             'data_validade' => $dado['dt_validade']->format('Y-m-d'),
                             'status_id' => $status,
                         ]
                    );
                    $handle = fopen('logSalvos.txt', 'a') or die('Cannot open file:  log.txt');
                    $new_data = "\n" . 'Ativado: ' . $dado['nr_placa_veiculo'];
                    fwrite($handle, $new_data);
            } else {
                    $handle = fopen('log.txt', 'a') or die('Cannot open file:  log.txt');
                    $new_data = "\n" . 'Placa não encontrada: ' . $dado['nr_placa_veiculo'];
                    fwrite($handle, $new_data);
            }
        }
    }

    private function readExcel()
    {
        return Excel::selectSheets('Veículos Transportadores')->load(storage_path('xls/veiculos_transportadores_gg.xlsx'))
        ->get();
    }

    public function salvarBoleto($dadosBoleto, $empresa_id, $tipo_servico)
    {
        
        $dadosSalvarBoleto = [
            'empresa_id' => $empresa_id,
            'anoEmissao' => $dadosBoleto['dadosGuia']['anoEmissao'],
            'dataDeValidade' => $dadosBoleto['dadosGuia']['dataDeValidade'],
            'dataDeVencimento' => $dadosBoleto['dadosGuia']['dataDeVencimento'],
            'valor' => $dadosBoleto['dadosGuia']['valor'],
            'status_id' => $dadosBoleto['dadosGuia']['status_id'],
            'codTipoServGuia' => $dadosBoleto['dadosGuia']['codTipoServGuia'],
            'numeroGuia' => $dadosBoleto['dadosGuia']['numeroGuia'],
            'arquivoBoleto' => $dadosBoleto['dadosGuia']['arquivoBoleto'],
            'tipo' => $dadosBoleto['dadosGuia']['tipo'],
            'dataDePagamento' => $dadosBoleto['dadosGuia']['dataDePagamento'],
        ];
        return Boleto::firstOrCreate($dadosSalvarBoleto);
    }
}
