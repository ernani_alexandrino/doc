<?php

namespace Amlurb\Console\Commands;

use Illuminate\Console\Command;
use Amlurb\Models\Empresa;
use Amlurb\Models\LogisticaResiduos;
use Firebase;

class SyncFirebaseEmpresas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firebase:empresas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincroniza os Geradores,Destinos Finais e Veiculos com o Firebase do Aplicativo CTRE-T';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() 
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $empresas = Empresa::has('empresas_veiculos')->get();
        $dataveiculos = [];
        $dataDestinoFinal = [];
        $dataGeradores= [];
        $dataResiduos= [];
        
        foreach ($empresas as $k => $empresa) {
            foreach ($empresa->empresas_veiculos as $veiculo) {
                $dataveiculos[$empresa->id][]['placa'] = $veiculo->placa;
            }
           

            foreach ($empresa->vinculos as $vinculo) {
                $empresa_vinculada = $vinculo->empresa_vinculada;
                if (isset($empresa_vinculada->empresas_x_empresas_tipos->empresa_tipo_id)) {
                    $tipo_id = $vinculo->empresa_vinculada_tipo_id;
                
                    $residuos = LogisticaResiduos::where('vinculador_id', $empresa->id) // empresa logada, que cadastrou vinculos
                    ->where('empresa_id', $empresa_vinculada->id)
                        ->with('residuo')->get();
                    
                    foreach ($residuos as $k => $residuo) {
                        $dataResiduos[$empresa->id][$empresa_vinculada->id][$k]['resiudo_nome'] = $residuo->residuo->nome;
                        $dataResiduos[$empresa->id][$empresa_vinculada->id][$k]['residuo_id'] = $residuo->residuo_id;
                    }
                    
                    if ($tipo_id == \config('enums.empresas_tipo.destino_final_reciclado')) {
                        $dataDestinoFinal[$empresa->id][$vinculo->id]['razao_social'] = str_replace("-", " ", $empresa_vinculada->razao_social) ;
                        $dataDestinoFinal[$empresa->id][$vinculo->id]['id_empresa'] = $empresa_vinculada->id;
                        $dataDestinoFinal[$empresa->id][$vinculo->id]['cnpj'] = formatar('cnpj', $empresa_vinculada->cnpj);
                    } else {
                        $dataGeradores[$empresa->id][$vinculo->id]['razao_social'] =  str_replace("-", "", $empresa_vinculada->razao_social);
                        $dataGeradores[$empresa->id][$vinculo->id]['id_empresa'] = $empresa_vinculada->id;
                        $dataGeradores[$empresa->id][$vinculo->id]['cnpj'] = formatar('cnpj', $empresa_vinculada->cnpj);
                    }
                }
            }
        }
        Firebase::set('/veiculos/', $dataveiculos);
        Firebase::set('/destinos_finais/', $dataDestinoFinal);
        Firebase::set('/geradores/', $dataGeradores);
        Firebase::set('/residuos/', $dataResiduos);
    }
}
