<?php

namespace Amlurb\Console\Commands;

use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasTipo;
use Illuminate\Console\Command;

class UpdateIdLimpurb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:id_limpurb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $empresas = Empresa::with('empresas_x_empresas_tipos', 'empresa_informacao_complementar')
        ->get();
        
        foreach ($empresas as $empresa) {
            if (!empty($empresa->id_limpurb)) {
                $siglaAtual = preg_replace("/[^A-Z]+/", "", $empresa->id_limpurb);
                if (isset($empresa->empresas_x_empresas_tipos->empresas_tipo->sigla)) {
                    if ($siglaAtual != $empresa->empresas_x_empresas_tipos->empresas_tipo->sigla) {
                        $empresa->update(
                            [
                                'id_limpurb' => str_replace($siglaAtual, $empresa->empresas_x_empresas_tipos->empresas_tipo->sigla, $empresa->id_limpurb)
                            ]
                        );
                    }
                }
            }
        }
    }
}
