<?php

namespace Amlurb\Console\Commands;

use Amlurb\Models\Empresa;
use Amlurb\Models\User;
use Illuminate\Console\Command;

class setAvaliableCompanies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'up:companies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $empresas = Empresa::whereIn('cnpj', 
        [
           '69234003000196',
           '49808223000179',
           '05649153000188',
           '02991559000156',
           '11832260000140',
           '23103227000168',
           '06353329000201',
           '08770436000171',
           '12258033000115',
           '97529595000109',
           '26272373000105',
           '08624842000126',
           '08329217000151',
           '11005389000184',
           '16938754000127',
           '03464149000110',
            '03592043000100',
            '02281959000410',
            '22770554000100',
            '11939512000215',
            '10416338000182',
            '49783350000160',
            '69234003000196',
        ]
       )->get();
       
       foreach ($empresas as $empresa) {
           
           $empresaFind = Empresa::find($empresa->id);
          
           if($empresaFind){
               $empresaFind->status_id = 1;
               $empresaFind->save();
           }

           $usuarios = User::where('empresa_id', $empresa->id)->get();
           if(!$usuarios->isEmpty()){

            foreach ($usuarios as $usuario) {
                $usuarioFind =   User::find($usuario->id);  

                if($usuarioFind){
                    $usuarioFind->password = bcrypt('amlurb#@');
                    $usuarioFind->save();
                }
            }

           }else{
               if(!empty($empresaFind->email)){
                $usuario = new User;
                $usuario->name = 'Amlurb';
                $usuario->status_id = 1;
                $usuario->empresa_id = $empresaFind->id;
                $usuario->email = $empresaFind->email;
                $usuario->password =  bcrypt('amlurb#@');
                $usuario->save();
               }
            
           }
         
       }
    }
}
