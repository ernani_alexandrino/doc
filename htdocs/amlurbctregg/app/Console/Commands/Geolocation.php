<?php

namespace Amlurb\Console\Commands;

use Illuminate\Console\Command;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresaEndereco;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Geocoder;
use File;

class Geolocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'geolocate:address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pega a Geolocalização e atualiza o campo latitude e longitude das empresas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->coordinates();
            $idTiposEmpresas = config('enums.empresas_tipo');
            foreach ($idTiposEmpresas as $idTipoEmpresa) {
                $this->downloadJsonGeolocation($idTipoEmpresa);
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }

    private function coordinates()
    {
        $empresas = Empresa::take(1000)
        ->where('id', '>', 16041)
        ->with('empresa_endereco:id,endereco,numero,bairro,cep')
        ->with('empresa_endereco.cidade:id,nome')
        ->with('empresa_endereco.estado:id,nome,sigla')
        ->whereHas('empresa_endereco', function ($q) {
            $q->where('latitude', null);
            $q->where('longitude', null);
        })
        ->get();
        foreach ($empresas as $indice => $empresa) {
                $endereco = $empresa->empresa_endereco->endereco . ", " . $empresa->empresa_endereco->numero . ", " . $empresa->empresa_endereco->cidade->nome . ", " . $empresa->empresa_endereco->estado->nome;
                $coordinates = Geocoder::getCoordinatesForAddress($endereco);
            if (!empty($coordinates)) {
                $empresaEndereco = EmpresaEndereco::where('empresa_id', $empresa->id)->first();
                if (!empty($empresaEndereco)) {
                    $empresaEndereco->latitude = $coordinates['lat'];
                    $empresaEndereco->longitude = $coordinates['lng'];
                    $empresaEndereco->save();
                }
            }
        }
    }
    private function downloadJsonGeolocation($tipo = 0)
    {
        ini_set('memory_limit', '-1');

        switch ($tipo) {
            case 1:
                $fileName = 'geoLocationGG.json'; //grande gerador
                break;

            case 2:
                $fileName = 'geoLocationPG.json'; //pequeno gerador
                break;

            case 3:
                $fileName = 'geoLocationTR.json'; //transportador
                break;

            case 4:
                $fileName = 'geoLocationDF.json'; //destino final
                break;

            case 5:
                $fileName = 'geoLocationCO.json'; //cooperativa de residuos
                break;
            case 6:
                $fileName = 'geoLocationFS.json'; //fiscal
                break;
            case 7:
                $fileName = 'geoLocationAD.json'; //Administrador
                break;
            case 8:
                $fileName = 'geoLocationOP.json'; //orgão publico
                break;
            case 8:
                $fileName = 'geoLocationSS.json'; //serviço de saude
                break;
            case 9:
                $fileName = 'geoLocationSS.json'; //serviço de saude
                break;
            case 10:
                $fileName = 'geoLocationCM.json'; //condominio misto
                break;
        }

        $dados = DB::table('empresa_endereco')
            ->join('empresas', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->join('cidades', 'empresa_endereco.cidade_id', '=', 'cidades.id')
            ->join('estados', 'empresa_endereco.estado_id', '=', 'estados.id')
            ->join('empresas_x_empresas_tipo', 'empresas.id', '=', 'empresas_x_empresas_tipo.empresa_id')
            ->select(
                'empresas.id as id_empresas',
                'empresas.razao_social',
                'empresas.nome_comercial',
                'empresas.id_limpurb',
                'empresas.cnpj',
                'cidades.nome as nome_cidades',
                'estados.sigla as sigla_estados',
                'empresa_endereco.endereco',
                'empresa_endereco.numero',
                'empresa_endereco.bairro',
                'empresa_endereco.cep',
                'empresa_endereco.latitude',
                'empresa_endereco.longitude'
            )
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', $tipo)
            ->get();
            
        
        //Linux
        $path = storage_path('geolocation/jsonfile/');
        if (!File::exists($path)) {
            File::makeDirectory($path, 0755, true);
        }
        File::put($path . $fileName, json_encode($dados));

        return "Arquivo Json gerado com sucesso!";
    }
}
