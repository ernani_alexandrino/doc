<?php

namespace Amlurb\Console\Commands;

use Illuminate\Console\Command;
use Amlurb\Models\InconsistenciasApp;
use Firebase,File,Hash;

class SyncFirebaseInconsistencias extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firebase:inconsistencia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inconsistenciasFB =  json_decode(Firebase::get('/incosistencia/'),true);
        if (is_null($inconsistenciasFB)) {
            return; // nao faz nada
        }

        foreach($inconsistenciasFB AS $userId => $inconsistencias){

            foreach($inconsistencias AS $chaveFirebase => $inconsistencia){ 

                $teste = str_replace(array('$','-','.','/'),'',Hash::make(date('Y-m-d h:s'),[ 'rounds' => 5]));
                $fileFachada = '';
                if(isset($inconsistencia['imagemFachada'])){
                    $fileFachada = $this->base64($inconsistencia['imagemFachada']);
                 }
                 $fileResiduo = '';
                 if(isset($inconsistencia['imagemResiduo'])){
                    $fileResiduo = $this->base64($inconsistencia['imagemResiduo']);
                 }
                 $cep = '';
                 if(isset($inconsistencia['cep'])){
                    $cep = $inconsistencia['cep'];
                 }

               
                    $create = InconsistenciasApp::create([
                        'user_id' => $inconsistencia['usuario'],
                        'razao_social' => $inconsistencia['razao_social'],
                        'grcode_empresa' => $inconsistencia['id'],
                        'cnpj' => $inconsistencia['cnpj'],
                        'cep' => $cep,
                        'estado' => $inconsistencia['estado'],
                        'descricao' => $inconsistencia['descricao'],
                        'imagem_fachada' => $fileFachada,
                        'imagem_residuo' => $fileResiduo,
                        ]);

                Firebase::delete('/incosistencia/'.$userId.'/'.$chaveFirebase);

            }
        }
    }


    public function base64($base64)
    {
        $image_parts = explode(";base64,", $base64);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.png';
        $file = public_path('/uploads/incosistencias/'). $fileName;
        file_put_contents($file, $image_base64);

        return $fileName;
    }

    
}
