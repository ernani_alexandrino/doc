<?php

namespace Amlurb\Console\Commands;

use Illuminate\Console\Command;
use Firebase;

class DropFirebaseColetas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firebase:drop';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $coletasFB =  json_decode(Firebase::get('/coletas/'),true);
        if (!is_iterable($coletasFB)) {
            return; // nao faz nada
        }

        foreach($coletasFB AS $userId => $coletas){
            
            foreach($coletas AS $chaveFirebase => $coleta){ 
            if(isset($coleta['read'])){
             
            
                Firebase::delete('/coletas/'.$userId.'/'.$coleta['id']);
               
            }
        }
           
        }
    }
}
