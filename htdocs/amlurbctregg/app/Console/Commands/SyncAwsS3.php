<?php

namespace Amlurb\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Amlurb\Models\Empresa;

class SyncAwsS3 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:s3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $empresas  = Empresa::all();
        foreach ($empresas as $empresa) {
            $pastaLocal = storage_path("licencas/{$empresa->id}");
            $pastaS3 = "licencas/{$empresa->id}/";
            if (file_exists($pastaLocal)) {
                $aux = scandir($pastaLocal);
                $files = array_diff($aux, array('.', '..'));
             
                //Copia os arquivos para o s3
                foreach ($files as $file) {
                    $pastaS3 = $pastaS3.$file;
                    if (!Storage::disk('s3')->exists($pastaS3.$file)) {
                        Storage::disk('s3')->put($pastaS3, file_get_contents($pastaLocal.'/'.$file));
                    }
                }
            }
        }
    }
}
