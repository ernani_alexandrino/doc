<?php

namespace Amlurb\Console\Commands;

use Illuminate\Console\Command;
use Amlurb\Models\EmpresaAprovacao;
use Amlurb\Models\Empresa;
use Illuminate\Support\Carbon;
use Amlurb\Mail\VinculosFaltantes;
use Illuminate\Support\Facades\Mail;

class EmailVinculo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:vinculo';
    const TIPOS = ["GG", "OP", "SS", "CM"];
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia emails para GG OP SS e CM que ainda não fizeram os vinculos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tipos = self::TIPOS;
        $now = Carbon::now()->startOfDay();
        $empresas = Empresa::where(function ($query) use ($tipos) {
            foreach ($tipos as $tipo) {
                $query->orWhere('id_limpurb', 'LIKE', '%'.$tipo.'%');
            }
        })
        ->with('empresa_aprovacao')
        ->whereHas('empresa_aprovacao', function ($q) use ($now) {
            $q->where('data_limte_vinculo', '>=', $now->format('Y-m-d'));
        })
        ->where('status_id', config('enums.status.vinculos_pendentes'))
        ->get();
       
        $this->processaDados($empresas);
    }

    private function processaDados($empresas)
    {
        foreach ($empresas as $empresa) {
            $empresasValida = [
                'transporte' => false,
                'condominio' => false,
                'cooperativa' => false,
                'grandeGerador' => false,
            ];
            $idTipoEmpresa = $empresa->empresas_x_empresas_tipos->empresas_tipo->id;

            foreach ($empresa->vinculos as $vinculo) {
                 //foreach para pegar os tipos vinculados
                 $empresasValida = $this->validaVinculos($empresasValida, $idTipoEmpresa, $vinculo);
            }
            /**
             * se a empresa for GG/OP/SS, valida se ela tem um transportador ou condominio privado cadastrado na AMLURB
             */
            if ($idTipoEmpresa == config('enums.empresas_tipo.grande_gerador')
                || $idTipoEmpresa == config('enums.empresas_tipo.orgao_publico')
                || $idTipoEmpresa == config('enums.tipo_atividade.servico_saude')
                ) {
                if (!$empresasValida['transporte'] && !$empresasValida['condominio']) {
                    if (isset($empresa->empresa_aprovacao->data_limte_vinculo)) {
                        $this->enviaEmail($empresa);
                    }
                } else {
                    $empresa->status_id = config('enums.status.ativo');
                    $empresa->save();
                }
            }
            /**
             * se a empresa for CM,valida se ela tem um transportador,cooperativa ambiental de resíduos,
             * e grandes geradores privado cadastrado na AMLURB
             */
            if ($idTipoEmpresa == config('enums.empresas_tipo.condominio_misto')) {
                if ($empresasValida['transporte'] && $empresasValida['cooperativa'] && $empresasValida['grandeGerador']) {
                    $empresa->status_id = config('enums.status.ativo');
                    $empresa->save();
                } else {
                    if (isset($empresa->empresa_aprovacao->data_limte_vinculo)) {
                        $this->enviaEmail($empresa);
                    }
                }
            }
        }
    }

    private function validaVinculos($empresasValida, $idTipoEmpresa, $vinculo)
    {
        if ($idTipoEmpresa == config('enums.empresas_tipo.grande_gerador')
        || $idTipoEmpresa == config('enums.empresas_tipo.orgao_publico')
        || $idTipoEmpresa == config('enums.tipo_atividade.servico_saude')
        ) {
            if ($vinculo->empresa_vinculada_tipo_id == config('enums.empresas_tipo.transportador')) {
                $empresasValida['transporte'] = true;
            }
            if ($vinculo->empresa_vinculada_tipo_id == config('enums.empresas_tipo.condominio_misto')) {
                $empresasValida['condominio'] = true;
            }
        }

        if ($idTipoEmpresa == config('enums.empresas_tipo.condominio_misto')) {
            if ($vinculo->empresa_vinculada_tipo_id == config('enums.empresas_tipo.transportador')) {
                $empresasValida['transporte'] = true;
            }
            if ($vinculo->empresa_vinculada_tipo_id == config('enums.empresas_tipo.cooperativa_residuos')) {
                $empresasValida['cooperativa'] = true;
            }
            if ($vinculo->empresa_vinculada_tipo_id == config('enums.empresas_tipo.grande_gerador')) {
                $empresasValida['grandeGerador'] = true;
            }
        }
        return $empresasValida;
    }
    private function enviaEmail($empresa)
    {
        Mail::to($empresa->email)->send(new VinculosFaltantes($empresa));
    }
}
