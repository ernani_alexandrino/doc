<?php

namespace Amlurb\Console\Commands;

use Amlurb\Jobs\StatusBoletos;
use Illuminate\Console\Command;
use Amlurb\Models\Boleto;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Services\ServiceBoletos;
use Amlurb\Mail\VinculosFaltantes;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Log;

class DispatchBoletos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch:boletos';
    protected $serviceBoletos;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->serviceBoletos = new ServiceBoletos();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $boletosVeiculo = Boleto::where('status_id', \config('enums.status.pagamento_pendente'))
        ->where('created_at', '>', '2018-12-31')
        ->where('tipo', 'veiculo')
        ->get();
        foreach ($boletosVeiculo as $boleto) {
            $this->processaBoleto($boleto);
        }
        
        $boletosEmpresa = Boleto::where('status_id', \config('enums.status.pagamento_pendente'))
        ->where('created_at', '>', '2018-12-31')
        ->where('tipo', null)
        ->orderBy('created_at', 'ASC')
        ->get();
        foreach ($boletosEmpresa as $boleto) {
            $this->processaBoleto($boleto);
        }
    }

    private function processaBoleto(Boleto $boleto)
    {
        try {
            $params_pesquisa = $this->getParametrosConsulta($boleto->numeroGuia, $boleto->codTipoServGuia);
           
            $retorno = $this->serviceBoletos->retornoGuia($params_pesquisa);
            if (is_object($retorno) && property_exists($retorno, 'pagamentos')) {
                if (property_exists($retorno->pagamentos, 'PagamentoDTO')) {
                    if ($retorno->pagamentos->PagamentoDTO->codNumGuia == $boleto->numeroGuia) {
                        $dataPagamento = Carbon::parse($retorno->pagamentos->PagamentoDTO->dtPgtoGuia);
                        $this->atualizaBoleto($boleto, $dataPagamento);
                    }
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
        }
    }

    private function atualizaBoleto(Boleto $boleto, $dataPagamento)
    {
      
        $boleto->status_id = \config('enums.status.pago');
        $boleto->dataDePagamento = $dataPagamento->format('Y-m-d');
        $boleto->save();

        /** colocando mb_strtolower para Veiculos ficar veiculo */
        if (mb_strtolower($boleto->tipo) == \config('enums.tipos_boleto.veiculo')) {
            $veiculo = EmpresasVeiculo::whereId($boleto->boleto_veiculos->veiculo_id)->first();
            if ($veiculo) {
                $this->ativaVeiculo($veiculo);
            }
        } else { // empresa
            $empresa = Empresa::whereId($boleto->empresa_id)->first();
            if ($empresa) {
                $this->ativaEmpresa($empresa);
            }
        }
    }

    private function ativaVeiculo($veiculo)
    {
        $veiculo->status_id = \config('enums.status.ativo');
        $veiculo->data_validade = Carbon::now()->addYear();
        $veiculo->save();
    }

    private function ativaEmpresa($empresa)
    {
        $idTipoEmpresa = $empresa->empresas_x_empresas_tipos->empresas_tipo->id;

        if ($idTipoEmpresa == config('enums.empresas_tipo.grande_gerador')
                    || $idTipoEmpresa == config('enums.empresas_tipo.orgao_publico')
                    || $idTipoEmpresa == config('enums.tipo_atividade.servico_saude')
                    || $idTipoEmpresa == config('enums.tipo_atividade.condominio_misto')
            ) {
                $empresa->status_id = \config('enums.status.vinculos_pendentes');
                $empresa->validade_cadastro = Carbon::now()->addYear();
            if ($empresa->save()) {
                    $empresa->empresa_aprovacao->update(
                        [
                            'data_limte_vinculo' => Carbon::now()->addDays(5)
                        ]
                    );
                Mail::to($empresa->email)->send(new VinculosFaltantes($empresa));
            }
        } else {
            $empresa->status_id = \config('enums.status.ativo');
            $empresa->validade_cadastro = Carbon::now()->addYear();
            $empresa->save();
        }
    }

    private function getParametrosConsulta($num_guia, $cod_servico)
    {
        $now = Carbon::now();
        $now->subDays(30);
        $future = Carbon::now();
        // $future->endOfMonth();
        return [
            'InputMessagePagamento' => [
                'inputPagamento' => [
                    'dtIniPagamento' => $now->format('d/m/Y'),
                    'dtFimPagamento' => $future->format('d/m/Y'),
                    'numGuia' => $num_guia,
                    'anoEmissaoGuia' => $now->format('Y'),
                    'codSiglaUnidade' => 'AMLURB',
                    'codTipoServico' => $cod_servico,
                ]
            ]
        ];
    }
}
