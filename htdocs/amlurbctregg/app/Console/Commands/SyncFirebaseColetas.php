<?php

namespace Amlurb\Console\Commands;

use Amlurb\Models\CtreEquipamentos;
use Amlurb\Models\EmpresasXEquipamento;
use Amlurb\Models\CtreResiduos;
use Illuminate\Console\Command;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;
use Carbon\Carbon;
use Amlurb\Models\User;
use Amlurb\Models\Ctre;
use Amlurb\Services\GeradorCodigos;
use Firebase;
use Log;
use Illuminate\Support\Facades\DB;

class SyncFirebaseColetas extends Command
{

    protected $user;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'firebase:coletas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincroniza as coletas realizadas pelo Aplicativo CTRE-T';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $coletasFirebase = json_decode(Firebase::get('/coletas/'), true);
        if (is_null($coletasFirebase)) {
            return; // nao faz nada
        }

        $coletasArr = []; // Array final sera [id_usuario][cnpj_empresa][cnpj_destino][cnpj_gerador][placa_veiculo]

        foreach ($coletasFirebase as $userId => $coletas) { // separa por usuario

            foreach ($coletas as $chaveFirebase => $coleta) {

                if (!isset($coleta['read'])) { // verifica se ja foi acessado ou nao

                    $geradorDados = explode('-', $coleta['gerador'], 3);
                    $destinoDados = explode('-', $coleta['destino'], 3);
                  
                    $destinoCnpj = trim($destinoDados[2]);
                    $geradorCnpj = trim($geradorDados[2]);

                    $coletasArr[$userId]
                        [cleanNumber($coleta['cnpj'])]
                            [cleanNumber($destinoCnpj)]
                                [cleanNumber($geradorCnpj)]
                                    [$coleta['veiculo']]['dados'] = $coleta;
                    $coletasArr[$userId]
                        [cleanNumber($coleta['cnpj'])]
                            [cleanNumber($destinoCnpj)]
                                [cleanNumber($geradorCnpj)]
                                    [$coleta['veiculo']]['equipamentos'][] = $coleta['qrcode'];

                    $coleta['read'] = true;
                    Firebase::set('/coletas/' . $userId . '/' . $coleta['id'], $coleta);
                }

            }

        }

        //realiza o loop no array agrupado e lança o CTRE-T
        foreach ($coletasArr as $userId => $coletasArrays) {

            $this->user = User::find($userId);

            foreach ($coletasArrays as $cnpjEmpresa => $coletasDestino) { // acessa coletas do transportador

                foreach ($coletasDestino as $cnpjDestino => $coletasGerador) { // acessa coletas a enviar para cada destino

                    foreach ($coletasGerador as $cnpjGerador => $coletasVeiculos) { // dados de cada gerador
                        $this->geraCTRE($coletasVeiculos, $cnpjDestino, $cnpjGerador);
                    }

                }

            }

            $this->info('DONE');

        }

    }

    // gera CTRE a partir dos dados informados
    private function geraCTRE($coletasVeiculos, $cnpjDestino, $cnpjGerador)
    {
        $destino = Empresa::where('cnpj', $cnpjDestino)->first();
        $gerador = Empresa::where('cnpj', $cnpjGerador)->first();

        foreach ($coletasVeiculos as $veiculo => $coleta) {

            try {
                DB::beginTransaction();

                $veiculo = EmpresasVeiculo::where('placa', $veiculo)->first();
                $dataEmissao = new Carbon($coleta['dados']['horaDataColeta']);
                
                $ctre = Ctre::firstOrCreate([
                    'codigo' => GeradorCodigos::geraCodigoCTRE($this->user->empresa_id),
                    'empresa_id' => $this->user->empresa_id,
                    'gerador_id' => $gerador->id,
                    'transportador_id' => $this->user->empresa_id,
                    'destino_id' => $destino->id,
                    'veiculo_id' => $veiculo->id,
                    'data_emissao' => $dataEmissao->toDateTimeString(),
                    'data_expiracao' => $dataEmissao->addDays(3)->toDateTimeString(),
                    'status_id' => \config('enums.status.em_aberto'),
                ]);
                
                foreach ($coleta['equipamentos'] as $codigo_equip) {
                    $this->vinculaEquipamentosComCtre($ctre->id, $codigo_equip);
                }
               
                foreach ($coleta['dados']['residuo'] as $id_residuo) {
                  
                    $this->vinculaResiduosComCtre($ctre->id, $id_residuo);
                }
             
                DB::commit();
            } catch (\Exception $e) {
                Log::error($e);
                DB::rollBack();
                echo 'problema em salvar dados' . $e->getMessage();
            }

        }
    }

    private function vinculaEquipamentosComCtre($ctre_id, $codigo_equip)
    {
        $equipamento =  EmpresasXEquipamento::where('codigo_amlurb', $codigo_equip)->first();
        if ($equipamento) {
            CtreEquipamentos::create([
                'ctre_id' => $ctre_id,
                'equipamento_id' => $equipamento->id
            ]);
        }
    }

    private function vinculaResiduosComCtre($ctre_id, $id_residuo)
    {
        if ($id_residuo) {
            CtreResiduos::create([
                'ctre_id' => $ctre_id,
                'residuo_id' => $id_residuo
            ]);
        }
    }

}
