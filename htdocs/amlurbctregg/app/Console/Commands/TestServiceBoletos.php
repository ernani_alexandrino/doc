<?php

namespace Amlurb\Console\Commands;

use Amlurb\Services\ServiceBoletos;
use Carbon\Carbon;
use Illuminate\Console\Command;

class testServiceBoletos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:service:boletos {--baixa}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'envia requisicao para ver retorno de geracao de boletos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $service = new ServiceBoletos();

        if ($this->option('baixa')) {
            $request = $this->consultarRequest();
            $result = $service->retornoGuia($request);
            dd($result);
        } else {
            $request = $this->gerarRequest();
            $result = $service->gerarBoleto($request);
            dd($result['dadosGuia'], $result['file']);
        }

    }


    private function gerarRequest()
    {
        $now = Carbon::now();
        return [
            'InputMessage' => [
                'Input' => [
                    'SiglaUnidade' => 'AMLURB',
                    'PessoaFisicaJuridica' => '1',
                    'CpfCnpj' => '56897447000160',
                    'NomeContribuinte' => 'Transportador Teste',
                    'NomeEndereco' => 'Rua Jesuíno Arruda',
                    'NumeroImovel' => '797',
                    'ComplementoEndereco' => 'Teste',
                    'BairroContribuinte' => 'Bairro',
                    'CepContribuinte' => '04532--08',
                    'Municipio' => 'SAO PAULO',
                    'Email' => 'tr@plataformaverde.com.br',
                    'dataVencimento' => $now->addDays(4)->format('d/m/Y'),
                    'Variavel1' => 1,
                    'CodigoTipoServico' => '9776',
                    'OutrasInformacoes' => 'Veiculos, Equipamentos, QR Codes'
                ]
            ]
        ];
    }

    private function consultarRequest()
    {
        $now = Carbon::now();
        $now->startOfMonth();
        $future = Carbon::now();
        $future->endOfMonth();
        return [
            'InputMessagePagamento' => [
                'inputPagamento' => [
                    'dtIniPagamento' => $now->format('d/m/Y'), // opcional
                    'dtFimPagamento' => $future->format('d/m/Y'), // opcional
                    //'numGuia' => 3, // opcional
                    'anoEmissaoGuia' => $now->format('Y'),
                    'codSiglaUnidade' => 'AMLURB',
                    'codTipoServico' => '9776',
                ]
            ]
        ];
    }

}
