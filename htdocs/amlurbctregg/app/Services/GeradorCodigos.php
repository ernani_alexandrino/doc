<?php

namespace Amlurb\Services;

use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\EmpresasXEquipamento;
use Amlurb\Repositories\EmpresasXEquipamentoRepository;
use Illuminate\Support\Facades\Auth;
use QrCode;

/**
 * classe usada para gerar codigos utilizados no sistema
 * codigo Amlurb e QR Code
 *
 * Class GeradorCodigos
 *
 * @package Amlurb\Services
 */
class GeradorCodigos
{
    const SIGLA_VEICULO = 'V';
    const SIGLA_EQUIPAMENTO = 'E';


    public static function geraCodigoFilial($id_empresa, $id_limpurb)
    {
        $contaFiliais = Empresa::where('id_matriz', $id_empresa)->count();
        $numFilialAtual = $contaFiliais + 1;
        $cod_Filial = str_pad($numFilialAtual, 3, '0', STR_PAD_LEFT);

        return $id_limpurb . '-' . $cod_Filial;
    }

    public static function geraCodigoEmpresa($sigla)
    {
        $codigo_ctre = GeradorCodigos::numeroSeteDigitosComAno();
        $verificaRepeticaoCodigo = true;

        // nao deixa repetir codigo
        while ($verificaRepeticaoCodigo) {
            $emp = Empresa::where('id_limpurb', 'like', '%' . $codigo_ctre)->first();
            if (is_null($emp)) {
                $verificaRepeticaoCodigo = false;
            } else {
                $codigo_ctre = GeradorCodigos::numeroSeteDigitosComAno();
            }
        }
        return $sigla . $codigo_ctre;
    }

    /**
     * gera codigo unico para veiculos
     *
     * @param integer $id_empresa
     *
     * @return string
     */
    public static function geraCodigoAmlurbVeiculos($id_empresa)
    {
        $tipoEmpresa = EmpresasXEmpresasTipo::where('empresa_id', $id_empresa)
            ->with('empresas_tipo:id,sigla')
            ->first();

        $codigoAmlurbVeiculo = GeradorCodigos::geraCodigoAmlurb($tipoEmpresa->empresas_tipo->sigla, GeradorCodigos::SIGLA_VEICULO);

        $existeCodigoAmlurbVeiculo = EmpresasVeiculo::where('codigo_amlurb', $codigoAmlurbVeiculo)->count();
        if ($existeCodigoAmlurbVeiculo) {
            $codigoAmlurbVeiculo = GeradorCodigos::geraCodigoAmlurbVeiculos($id_empresa);
        }
        return $codigoAmlurbVeiculo;
    }

    /**
     * gera codigo unico para equipamentos
     *
     * @param $id_empresa
     *
     * @return string
     */
    public static function geraCodigoAmlurbEquipamento($id_empresa)
    {
        $tipoEmpresa = EmpresasXEmpresasTipo::where('empresa_id', $id_empresa)
            ->with('empresas_tipo:id,sigla')
            ->first();

        $codigoAmlurbEquipamento = GeradorCodigos::geraCodigoAmlurb($tipoEmpresa->empresas_tipo->sigla, GeradorCodigos::SIGLA_EQUIPAMENTO);

        $existeCodigoAmlurbEquipamento = EmpresasXEquipamento::where('codigo_amlurb',
            $codigoAmlurbEquipamento)->count();
        if ($existeCodigoAmlurbEquipamento) {
            $codigoAmlurbEquipamento = GeradorCodigos::geraCodigoAmlurbEquipamento($id_empresa);
        }
        return $codigoAmlurbEquipamento;
    }

    // sigla complementar usada para equipamentos e veiculo
    public static function geraCodigoAmlurb($sigla_empresa, $sigla_complementar = '')
    {
        return $sigla_empresa . $sigla_complementar . GeradorCodigos::numeroSeteDigitosComAno();
    }

    public static function geraCodigoCTRE($id_empresa)
    {
        return date('ymdHis') . GeradorCodigos::numeroSeteDigitos($id_empresa);
    }

    public static function numeroSeteDigitosComAno()
    {
        return GeradorCodigos::numeroSeteDigitos(rand(0, 9999999)) . '/' . date('Y');
    }

    public static function numeroSeteDigitos($input)
    {
        return str_pad($input, 7, '0', STR_PAD_LEFT);
    }


    /// QR CODES

    public static function geraQrCodeEmpresa($id_empresa)
    {
        $empresa = Empresa::whereId($id_empresa)->first();
        $qrcode = [
            'razao_social' => $empresa->razao_social,
            'id' => $empresa->id_limpurb,
            'cnpj' => $empresa->cnpj,
            'endereco' => $empresa->empresa_endereco->endereco . ',' . $empresa->empresa_endereco->numero . ',' . $empresa->empresa_endereco->bairro,
            'estado' => $empresa->empresa_endereco->estado->sigla,
            'cep' => $empresa->empresa_endereco->cep
        ];

        return QrCode::size(1000)->generate(json_encode($qrcode));
    }

    public static function geraQrCodeVeiculo($id_veiculo)
    {
        $empresa = Empresa::whereId(Auth::user()->empresa_id)->first();
        $veiculo = EmpresasVeiculo::whereId($id_veiculo)->first();
        $qrcode = [
            'razao_social' => $empresa->razao_social,
            'id' => $veiculo->codigo_amlurb,
            'cnpj' => $empresa->cnpj,
            'estado' => $empresa->empresa_endereco->estado->sigla,
            'placa' => $veiculo->placa,
            'ano_veiculo' => $veiculo->ano_veiculo,
            'tipo' => $veiculo->tipo,
            'marca' => $veiculo->marca,
            'inmetro' => ''
        ];

        return QrCode::size(1000)->generate(json_encode($qrcode));
    }

    public static function geraQrCodeEquipamento($id_equipamento)
    {
        $empresa = Empresa::whereId(Auth::user()->empresa_id)->first();
        $exe = new EmpresasXEquipamentoRepository();
        $equip = $exe->getEquipamentoById($id_equipamento);

        $qrcode = [
            'nome' => $equip->equipamento->tipo_equipamento->nome,
            'id' => $equip->codigo_amlurb,
            'empresa' => $empresa->razao_social,
            'numero_amlurb' => $empresa->id_limpurb,
            'tipo_empresa' => $empresa->empresas_x_empresas_tipos->empresas_tipo->nome,
            'cnpj' => $empresa->cnpj,
            'endereco' => $empresa->empresa_endereco->endereco . ',' . $empresa->empresa_endereco->numero . ',' . $empresa->empresa_endereco->bairro,
            'cep' => $empresa->empresa_endereco->cep,
            'estado' => $empresa->empresa_endereco->estado->sigla,
            'cidade' => $empresa->empresa_endereco->cidade->nome
        ];

        return QrCode::size(1000)->generate(json_encode($qrcode));
    }

}