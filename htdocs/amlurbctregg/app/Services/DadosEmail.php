<?php

namespace Amlurb\Services;


use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;

class DadosEmail
{
    public static function cadastroAprovado(Empresa $empresa)
    {
        return [
            'empresa' => $empresa,
            'idTipoEmpresa' => $empresa->empresas_x_empresas_tipos->empresas_tipo->id,
            'tipoEmpresa' => $empresa->empresas_x_empresas_tipos->empresas_tipo->nome,
            'dataEnvio' => date('d/m/Y'),
            'emailReceptor' => $empresa->email,
            'subject' => 'CTRE - Cadastro aprovado com Sucesso',
            'tipoRamoAtividadeId' => $empresa->empresa_informacao_complementar->tipo_ramo_atividade_id,
            'view' => 'emails.cadastros-empresas.cadastro-aprovado'
        ];
    }

    public static function segundaViaBoleto(Empresa $empresa)
    {
        return [
            'empresa' => $empresa,
            'idTipoEmpresa' => $empresa->empresas_x_empresas_tipos->empresas_tipo->id,
            'tipoEmpresa' => $empresa->empresas_x_empresas_tipos->empresas_tipo->nome,
            'dataEnvio' => date('d/m/Y'),
            'emailReceptor' => $empresa->email,
            'subject' => 'CTRE - Segunda Via de Boleto',
            'tipoRamoAtividadeId' => $empresa->empresa_informacao_complementar->tipo_ramo_atividade_id,
            'view' => 'emails.boletos.segunda-via'
        ];
    }

    public static function inconsistenciasCadasto(Empresa $empresa, $msg)
    {
        return [
            'empresa' => $empresa,
            'dataEnvio' => date('d/m/Y'),
            'emailReceptor' => $empresa->email,
            'msgInconsistencia' => $msg,
            'subject' => 'CTRE - Dados inconsistentes',
            'view' => 'emails.cadastros-empresas.reportar-inconsistencia'
        ];
    }

    public static function boletoVeiculo(Empresa $empresa, EmpresasVeiculo $veiculo)
    {
        return [
            'empresa' => $empresa,
            'veiculo' => $veiculo,
            'dataEnvio' => date('d/m/Y'),
            'emailReceptor' => $empresa->email,
            'subject' => 'CTRE - Boleto Veículo - Placa ' . $veiculo->placa,
            'view' => 'emails.boletos.veiculo'
        ];
    }

}