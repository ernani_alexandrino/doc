<?php

namespace Amlurb\Services;

use Illuminate\Support\Facades\Mail;

class MailSend
{

    // recebe view e repassa email adiante
    public static function sendMail($view, $dadosEmail)
    {
        Mail::send($view, ['dadosEmail' => $dadosEmail], function ($message) use ($dadosEmail) {
            $message->to($dadosEmail['emailReceptor'])->subject($dadosEmail['subject']);
        });
    }

}
