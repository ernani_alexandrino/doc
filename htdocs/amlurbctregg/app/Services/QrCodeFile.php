<?php

namespace Amlurb\Services;

use Amlurb\Models\Qrcode;
use Amlurb\Repositories\EmpresaRepository;
use Illuminate\Support\Facades\File;

use setasign\Fpdi\Fpdi;
use Spatie\Browsershot\Browsershot;
use Spatie\Image\Manipulations;
use Auth;

class QrCodeFile
{
    const COD = 'cod';

    public static function getPDF($id, $cnpj)
    {
        $qrcode = Qrcode::whereId($id)->first();

        // cria diretorios se nao existirem
        $path = storage_path('qrcodes'. DIRECTORY_SEPARATOR . $cnpj);
        if (!File::exists($path)) {
            File::makeDirectory($path, 0755, true);
        }

        $img_file = $path . DIRECTORY_SEPARATOR . QrCodeFile::COD . $qrcode->id . '.png';
        $pdf_file = $path . DIRECTORY_SEPARATOR . QrCodeFile::COD . $qrcode->id . '.pdf';

       

        // carrega moldura do QR Code
        $pdf_template = QrCodeFile::getPdfTemplate($qrcode);
        // salva imagem do QR Code
        QrCodeFile::generateQrCodeImage($qrcode, $img_file);
        QrCodeFile::generateQrCodePDF($pdf_template, $img_file, $pdf_file, $qrcode);
        // remove imagem baixada
        unlink($img_file);

        return $pdf_file;
    }

    private static function getPdfTemplate(Qrcode $qrcode)
    {
        $template_path = base_path() . DIRECTORY_SEPARATOR . 'resources' .
            DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'qrcodes' .
            DIRECTORY_SEPARATOR;

        // verifica tipo de qr code for de veiculo ou equipamento
        if ($qrcode->qrcode_equipamentos()->exists()) {
            return $template_path . 'equip.pdf';
        }
        if ($qrcode->qrcode_veiculos()->exists()) {
            return $template_path . 'veiculo.pdf';
        }

        // verifica tipo da empresa
        $empresasRepository = new EmpresaRepository();
        $empresa_tipo_id = $empresasRepository->getEmpresaTipoById($qrcode->qrcode_empresas->empresa_id);
        if ($empresa_tipo_id == \config('enums.empresas_tipo.grande_gerador')) {
            $template_path .= 'gg.pdf';
        }
        if ($empresa_tipo_id == \config('enums.empresas_tipo.pequeno_gerador')) {
            $template_path .= 'pg.pdf';
        }
        if ($empresa_tipo_id == \config('enums.empresas_tipo.transportador')) {
            $template_path .= 'tr.pdf';
        }
        if ($empresa_tipo_id == \config('enums.empresas_tipo.destino_final_reciclado')) {
            $template_path .= 'df.pdf';
        }
        if ($empresa_tipo_id == \config('enums.empresas_tipo.cooperativa_residuos')) {
            $template_path .= 'co.pdf';
        }
        if ($empresa_tipo_id == \config('enums.empresas_tipo.orgao_publico')) {
            $template_path .= 'op.pdf';
        }
        if ($empresa_tipo_id == \config('enums.empresas_tipo.servico_saude')) {
            $template_path .= 'ss.pdf';
        }
        if ($empresa_tipo_id == \config('enums.empresas_tipo.condominio_misto')) {
            $template_path .= 'cm.pdf';
        }

        return $template_path;
    }

    private static function generateQrCodeImage(Qrcode $qrcode, $img_file)
    {
        Browsershot::html($qrcode->codigo)
            ->windowSize(1000, 1000)
            ->fit(Manipulations::FIT_CROP, 800, 800)
            ->save($img_file);
    }

    private static function generateQrCodePDF($pdf_template, $img_file, $pdf_file ,Qrcode $qrcode)
    {
        $pdf = new Fpdi();
        $pdf->AddPage();
        $pdf->setSourceFile($pdf_template);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);
        
        if ($qrcode->qrcode_equipamentos()->exists()) {
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetFontSize('12'); // set font size
            $pdf->SetXY(-281, 46); // set the position of the box
            $pdf->SetTextColor(255,255,255);
            $pdf->Cell(0, 10, 'Empresa: '.Auth::user()->empresa->id_limpurb, 0, 0, 'C');

            $pdf->SetFont('Arial', 'B', 10);
            $pdf->SetFontSize('12'); // set font size
            $pdf->SetXY(-281, 128); // set the position of the box
            $pdf->SetTextColor(255,255,255);
            $pdf->Cell(0, 10,$qrcode->qrcode_equipamentos->equipamento->codigo_amlurb, 0, 0, 'C');
        }
        if ($qrcode->qrcode_veiculos()->exists()) {
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetFontSize('12'); // set font size
            $pdf->SetXY(-235, 65); // set the position of the box
            $pdf->SetTextColor(255,255,255);
            $pdf->Cell(0, 10, 'Empresa: '.Auth::user()->empresa->id_limpurb, 0, 0, 'C');

            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetFontSize('12'); // set font size
            $pdf->SetXY(-235, 180); // set the position of the box
            $pdf->SetTextColor(255,255,255);
            $pdf->Cell(0, 10, 'Placa: '.formataPlacadeCarro(mb_strtoupper($qrcode->qrcode_veiculos->veiculo->placa)), 0, 0, 'C');
        }

        if ($qrcode->qrcode_empresas()->exists()) {
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->SetFontSize('15'); // set font size
            $pdf->SetXY(-240, 65); // set the position of the box
            $pdf->SetTextColor(255,255,255);
            $pdf->Cell(0, 10, 'Empresa: '.Auth::user()->empresa->id_limpurb, 0, 0, 'C');
        }
    

        // ajusta tamanho qr code
        if (strpos($pdf_template, 'equip') != 0) {
            // medidas equipamento
            $pdf->Image($img_file, 37.5, 58, 52, 52);
        } else {
            $pdf->Image($img_file, 49, 78, 72, 72);
        }

        $pdf->Output('F', $pdf_file);
    }

}