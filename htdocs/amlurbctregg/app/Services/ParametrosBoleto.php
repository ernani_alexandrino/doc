<?php

namespace Amlurb\Services;

use Amlurb\Models\Empresa;
use Carbon\Carbon;

class ParametrosBoleto
{

    /**
     * Comunicação com API Amlurb
     *
     * 1 - Pessoa física
     * 2 - Pessoa Jurídica
     *
     */
    const PARAMETROS_BASICOS = [
        'InputMessage' => [
            'Input' => [
                'SiglaUnidade' => 'AMLURB',
                'PessoaFisicaJuridica' => '1',
            ]
        ]
    ];

    public static function preencheDadosEmpresa(Empresa $empresa)
    {
        $parametrosBoleto = ParametrosBoleto::PARAMETROS_BASICOS;

        $carbonVencBoleto = new Carbon();

        $parametrosBoleto['InputMessage']['Input']['CpfCnpj'] = $empresa->cnpj;
        $parametrosBoleto['InputMessage']['Input']['NomeContribuinte'] = $empresa->razao_social;
        $parametrosBoleto['InputMessage']['Input']['NomeEndereco'] = $empresa->empresa_endereco->endereco;
        $parametrosBoleto['InputMessage']['Input']['NumeroImovel'] = $empresa->empresa_endereco->numero;
        $parametrosBoleto['InputMessage']['Input']['ComplementoEndereco'] = ($empresa->empresa_endereco->complemento) ? $empresa->empresa_endereco->complemento : 'Nenhum';
        $parametrosBoleto['InputMessage']['Input']['BairroContribuinte'] = $empresa->empresa_endereco->bairro;
        $parametrosBoleto['InputMessage']['Input']['CepContribuinte'] = $empresa->empresa_endereco->cep;
        $parametrosBoleto['InputMessage']['Input']['Municipio'] = $empresa->empresa_endereco->cidade->nome;
        $parametrosBoleto['InputMessage']['Input']['Email'] = $empresa->email;
        $parametrosBoleto['InputMessage']['Input']['dataVencimento'] = $carbonVencBoleto->now()->addDays(5)->format('Y-m-d');
        $parametrosBoleto['InputMessage']['Input']['Variavel1'] = 1;

        return $parametrosBoleto;
    }

    public static function preencheTipoEmpresa($parametrosBoleto, $tipo_servico)
    {
        $parametrosBoleto['InputMessage']['Input']['CodigoTipoServico'] = $tipo_servico;
        $parametrosBoleto['InputMessage']['Input']['OutrasInformacoes'] = 'Registro Novo ou Renovação - por registro.';
        return $parametrosBoleto;
    }

    // DEV
    public static function dadosGuiaRandomico()
    {
        $dados_guia = new \stdClass();
        $dados_guia->Valor = rand(1, 100);
        $dados_guia->Numero = rand(1, 10000);
        $dados_guia->AnoEmissao = date('Y');
        $dados_guia->DataDeValidade = Carbon::now()->addDays(5);
        $dados_guia->DataDeVencimento = Carbon::now();
        return $dados_guia;
    }

}