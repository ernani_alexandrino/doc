<?php

namespace Amlurb\Services;

use Illuminate\Support\Facades\File;
use SoapHeader;
use SoapClient;
use Illuminate\Support\Facades\Log;


/**
 * Service de boleto para comunicação com a Amlurb e Prodam
 *
 * Class ServiceBoletos
 *
 * @package Amlurb\Services
 */
class ServiceBoletos
{

    private $identificacao;
    private $chave;
    private $wsdl;
    private $wsdlConsulta;

    const OPERACAO = 'EmitirDAMSP';
    const OPERACAO_PAGAMENTOS = 'ConsultarPagamentosBaixados';
    const DEFAULT_PATH = 'boletos' . DIRECTORY_SEPARATOR;

    private $path;

    public function __construct()
    {
        $this->identificacao = config('boleto.identificacao_soap');
        $this->chave = config('boleto.chave_soap');
        $this->wsdl = config('boleto.url_wsdl_prefeitura');
        $this->wsdlConsulta = config('boleto.url_wsdl_prefeitura_consulta');
        $this->path = storage_path(self::DEFAULT_PATH);
    }

    /**
     * Envia os dados para a API para gerar o boleto.
     *
     * @param $data
     *
     * @return SoapFault|array|\Exception
     */
    public function gerarBoleto($data)
    {
        $retorno = [];
        $clientSoap = new SoapClient($this->wsdl);
        $header = [];
        $header[] = new SoapHeader('http://tempuri.org/', 'Identificacao', $this->identificacao);
        $header[] = new SoapHeader('http://tempuri.org/', 'Chave', $this->chave);

        $clientSoap->__setSoapHeaders($header);

        // Gera a DAMSP atraves de envio dos dados ao sistema de Precos Publicos
        try {
            $name = rand(1, 1000);
            $name = 'boleto_' . $name . time() . '.pdf';

            $result = $clientSoap->__soapCall(self::OPERACAO, $data, null, $header);

            if (!File::exists($this->path)) {
                File::makeDirectory($this->path, 0755, true);
            }
            if (file_put_contents($this->path . $name, $result->PDF)) {
                $retorno = ['file' => $name, 'dadosGuia' => $result->RetornoGuia];
            }

            return $retorno;

        } catch (\Exception $e) {
            Log::error($e);
            return $e->getMessage();
        }
    }

    public function retornoGuia($dataRetorno)
    {
        try {
            $clientSoap = new SoapClient($this->wsdlConsulta);
            $header = [];
            $header[] = new SoapHeader('http://tempuri.org/', 'Identificacao', $this->identificacao);
            $header[] = new SoapHeader('http://tempuri.org/', 'Chave', $this->chave);

            $clientSoap->__setSoapHeaders($header);

            return $clientSoap->__soapCall(self::OPERACAO_PAGAMENTOS, $dataRetorno, null, $header);

        } catch (\Exception $e) {
             Log::error($e);
            return $e->getMessage();
        }
    }

    // CAMINHO PARA SALVAR ARQUIVOS DE BOLETOS

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = storage_path($path);
    }

}
