<?php

namespace Amlurb\Services\DataTables;

use Yajra\Datatables\Datatables;

class CtresRenderer
{
    public static function renderGeradorEmitidos($ctres)
    {
        return Datatables::of($ctres)
            ->setRowId(function ($ctre) {
                return $ctre->id;
            })
            ->addColumn('data_emissao', function ($ctre) {
                return $ctre->data_emissao->format('d/m/Y');
            })
            ->addColumn('codigo', function ($ctre) {
                return $ctre->codigo;
            })
            ->addColumn('transportador', function ($ctre) {
                return $ctre->transportador->razao_social;
            })
            ->addColumn('total_residuos', function ($ctre) {
                return count($ctre->ctre_residuos);
            })
            ->addColumn('status', function ($ctre) {
                return  $ctre->status->descricao;
            })
            ->addColumn('validacao', function ($ctre) {
                $row = CtresRenderer::getValidacaoIcon('G', true);
                $row .= CtresRenderer::getValidacaoIcon('T', !is_null($ctre->data_validacao));
                $row .= CtresRenderer::getValidacaoIcon('D', !is_null($ctre->data_validacao_final));
                return  $row;
            })
            ->rawColumns(['data_emissao', 'codigo', 'transportador', 'total_residuos', 'status', 'validacao'])
            ->make(true);
    }

    public static function renderGeradorRecebidos($ctres)
    {
        return Datatables::of($ctres)
            ->setRowId(function ($ctre) {
                return $ctre->id;
            })
            ->addColumn('data_emissao', function ($ctre) {
                return $ctre->data_emissao->format('d/m/Y');
            })
            ->addColumn('codigo', function ($ctre) {
                return $ctre->codigo;
            })
            ->addColumn('transportador', function ($ctre) {
                return $ctre->transportador->razao_social;
            })
            ->addColumn('total_equipamentos', function ($ctre) {
                return count($ctre->ctre_equipamentos);
            })
            ->addColumn('status', function ($ctre) {
                return  $ctre->status->descricao;
            })
            ->addColumn('validacao', function ($ctre) {
                if (!is_null($ctre->data_validacao) || $ctre->status_id == \config('enums.status.invalidado')) {
                    $row = CtresRenderer::getValidacaoIcon('T', true);
                    $row .= CtresRenderer::getValidacaoIcon('G', true);
                    $row .= CtresRenderer::getValidacaoIcon('D', !is_null($ctre->data_validacao_final));
                } else {
                    $row = "<a class='table-status-validar'>Validar</a>";
                }
                return  $row;
            })
            ->rawColumns(['data_emissao', 'codigo', 'transportador', 'total_equipamentos', 'status', 'validacao'])
            ->make(true);
    }

    public static function renderTransportadorEmitidos($ctres)
    {
        return Datatables::of($ctres)
            ->setRowId(function ($ctre) {
                return $ctre->id;
            })
            ->addColumn('data_emissao', function ($ctre) {
                return $ctre->data_emissao->format('d/m/Y');
            })
            ->addColumn('codigo', function ($ctre) {
                return $ctre->codigo;
            })
            ->addColumn('gerador', function ($ctre) {
                if (empty($ctre->gerador->razao_social))
                    return '';
                return $ctre->gerador->razao_social;
            })
            ->addColumn('destino', function ($ctre) {
                if (empty($ctre->destino_final->razao_social))
                    return '';
                return $ctre->destino_final->razao_social;
            })
            ->addColumn('status', function ($ctre) {
                return  $ctre->status->descricao;
            })
            ->addColumn('validacao', function ($ctre) {
                $row = CtresRenderer::getValidacaoIcon('T', true);
                $row .= CtresRenderer::getValidacaoIcon('G', !is_null($ctre->data_validacao));
                $row .= CtresRenderer::getValidacaoIcon('D', !is_null($ctre->data_validacao_final));
                return  $row;
            })
            ->rawColumns(['data_emissao', 'codigo', 'gerador', 'destino', 'status', 'validacao'])
            ->make(true);
    }

    public static function renderTransportadorRecebidos($ctres)
    {
        return Datatables::of($ctres)
            ->setRowId(function ($ctre) {
                return $ctre->id;
            })
            ->addColumn('data_emissao', function ($ctre) {
                return $ctre->data_emissao->format('d/m/Y');
            })
            ->addColumn('codigo', function ($ctre) {
                return $ctre->codigo;
            })
            ->addColumn('gerador', function ($ctre) {
                return $ctre->gerador->razao_social;
            })
            ->addColumn('total_residuos', function ($ctre) {
                return count($ctre->ctre_residuos);
            })
            ->addColumn('status', function ($ctre) {
                return  $ctre->status->descricao;
            })
            ->addColumn('validacao', function ($ctre) {
                if (!is_null($ctre->data_validacao) || $ctre->status_id == \config('enums.status.invalidado')) {
                    $row = CtresRenderer::getValidacaoIcon('G', true);
                    $row .= CtresRenderer::getValidacaoIcon('T', true);
                    $row .= CtresRenderer::getValidacaoIcon('D', !is_null($ctre->data_validacao_final));
                } else {
                    $row = "<a class='table-status-validar'>Validar</a>";
                }
                return  $row;
            })
            ->rawColumns(['data_emissao', 'codigo', 'gerador', 'total_residuos', 'status', 'validacao'])
            ->make(true);
    }

    public static function renderDestinoFinalRecebidos($ctres)
    {
        return Datatables::of($ctres)
            ->setRowId(function ($ctre) {
                return $ctre->id;
            })
            ->addColumn('data_emissao', function ($ctre) {
                return $ctre->data_emissao->format('d/m/Y');
            })
            ->addColumn('codigo', function ($ctre) {
                return $ctre->codigo;
            })
            ->addColumn('placa', function ($ctre) {
                return $ctre->empresas_veiculos->placa;
            })
            ->addColumn('transportador', function ($ctre) {
                return $ctre->transportador->razao_social;
            })
            ->addColumn('status', function ($ctre) {
                return $ctre->status->descricao;
            })
            ->addColumn('validacao', function ($ctre) {
                if (!is_null($ctre->data_validacao_final) || $ctre->status_id == \config('enums.status.invalidado')) {
                    $row = '';
                    if ($ctre->empresa_id == $ctre->gerador_id) { // se foi emitido por gerador, tudo estara validado
                        $row .= CtresRenderer::getValidacaoIcon('G', true);
                        $row .= CtresRenderer::getValidacaoIcon('T', true);
                    } else {
                        $row .= CtresRenderer::getValidacaoIcon('T', true);
                        $row .= CtresRenderer::getValidacaoIcon('G', !is_null($ctre->data_validacao));
                    }
                    $row .= CtresRenderer::getValidacaoIcon('D', true);
                } else {
                    $row = "<a class='table-status-validar'>Validar</a>";
                }
                return  $row;
            })
            ->rawColumns(['data_emissao', 'codigo', 'placa', 'transportador', 'status', 'validacao'])
            ->make(true);
    }

    // define icone colorido ou descolorido de acordo com status
    private static function getValidacaoIcon($letra, $status_validacao)
    {
        $class_validacao = ($status_validacao) ? 'table-status-validacao__' . strtolower($letra) : '';
        return "<span class='table-status-validacao {$class_validacao}'>{$letra}</span>";
    }

}