<?php

namespace Amlurb\Services\DataTables;

// clientes/fornecedores
use Yajra\Datatables\Datatables;

class VinculosRenderer
{
    public static function renderVinculosGeradores($geradores)
    {
        return DataTables::of($geradores)
            ->setRowId(function ($gerador) {
                return $gerador->id;
            })
            ->addColumn('id_limpurb', function ($gerador) {
                return $gerador->id_limpurb;
            })
            ->addColumn('cnpj', function ($gerador) {
                return $gerador->cnpj;
            })
            ->addColumn('razao_social', function ($gerador) {
                return $gerador->razao_social;
            })
            ->addColumn('total_residuos', function ($gerador) {
                return count($gerador->logistica_residuos);
            })
            ->addColumn('total_equipamentos', function ($gerador) {
                return count($gerador->equipamento_alocacao);
            })
            ->addColumn('frequencia_coleta', function ($gerador) {
                return (!is_null($gerador->empresa_vinculada->frequencia_coleta)) ?
                    $gerador->empresa_vinculada->frequencia_coleta->nome : '--';
            })
            ->addColumn('status', function ($gerador) {
                return $gerador->status->descricao;
            })
            ->addColumn('acao', function ($gerador) {
                $row = "<button class='btn btn-delete delete-vinculo' value='{$gerador->empresa_vinculada->id}' title='Remover Vínculo'>";
                $row .= "<img src='";
                $row .= asset('images/remover.png');
                $row .= "' alt='Remover vínculo'>";
                $row .= "</button>";
                return $row;
            })
            ->rawColumns(['id_limpurb', 'cnpj', 'razao_social', 'total_residuos', 'total_equipamentos',
                'frequencia_coleta', 'status', 'acao'])
            ->make(true);
    }

    public static function renderVinculosTransportadores($transportadores)
    {
        return Datatables::of($transportadores)
            ->setRowId(function ($transportador) {
                return $transportador->id;
            })
            ->addColumn('id_limpurb', function ($transportador) {
                return $transportador->id_limpurb;
            })
            ->addColumn('razao_social', function ($transportador) {
                return $transportador->razao_social;
            })
            ->addColumn('total_residuos', function ($transportador) {
                return count($transportador->logistica_residuos);
            })
            ->addColumn('total_equipamentos', function ($transportador) {
                return count($transportador->equipamento_alocacao);
            })
            ->addColumn('frequencia_coleta', function ($transportador) {
                return (!is_null($transportador->empresa_vinculada->frequencia_coleta)) ?
                    $transportador->empresa_vinculada->frequencia_coleta->nome : '--';
            })
            ->addColumn('status', function ($transportador) {
                return $transportador->status->descricao;
            })
            ->addColumn('acao', function ($transportador) {
                $row = "<button class='btn btn-delete delete-vinculo' value='{$transportador->empresa_vinculada->id}' title='Remover Vínculo'>";
                $row .= "<img src='";
                $row .= asset('images/remover.png');
                $row .= "' alt='Remover vínculo'>";
                $row .= "</button>";
                return $row;
            })
            ->rawColumns(['id_limpurb', 'razao_social', 'total_residuos', 'total_equipamentos', 'frequencia_coleta', 'status', 'acao'])
            ->make(true);
    }

    public static function renderVinculosCooperativas($transportadores)
    {
        return Datatables::of($transportadores)
            ->setRowId(function ($transportador) {
                return $transportador->id;
            })
            ->addColumn('id_limpurb', function ($transportador) {
                return $transportador->id_limpurb;
            })
            ->addColumn('razao_social', function ($transportador) {
                return $transportador->razao_social;
            })
            ->addColumn('total_residuos', function ($transportador) {
                return count($transportador->logistica_residuos);
            })
            ->addColumn('total_equipamentos', function ($transportador) {
                return count($transportador->equipamento_alocacao);
            })
            ->addColumn('frequencia_coleta', function ($transportador) {
                return (!is_null($transportador->empresa_vinculada->frequencia_coleta)) ?
                    $transportador->empresa_vinculada->frequencia_coleta->nome : '--';
            })
            ->addColumn('status', function ($transportador) {
                return $transportador->status->descricao;
            })
            ->addColumn('acao', function ($transportador) {
                $row = "<button class='btn btn-delete delete-vinculo' value='{$transportador->empresa_vinculada->id}' title='Remover Vínculo'>";
                $row .= "<img src='";
                $row .= asset('images/remover.png');
                $row .= "' alt='Remover vínculo'>";
                $row .= "</button>";
                return $row;
            })
            ->rawColumns(['id_limpurb', 'razao_social', 'total_residuos', 'total_equipamentos', 'frequencia_coleta', 'status', 'acao'])
            ->make(true);
    }

    public static function renderVinculosDestinosFinais($destinos)
    {
        return DataTables::of($destinos)
            ->setRowId(function ($destino) {
                return $destino->id;
            })
            ->addColumn('id_limpurb', function ($destino) {
                return $destino->id_limpurb;
            })
            ->addColumn('razao_social', function ($destino) {
                return $destino->razao_social;
            })
            ->addColumn('total_residuos', function ($destino) {
                return count($destino->logistica_residuos);
            })
            ->addColumn('volume_residuos', function ($destino) {
                return (!is_null($destino->empresa_vinculada->volume_residuo)) ?
                    str_replace('.', ',', $destino->empresa_vinculada->volume_residuo) . ' ton' : '--';
            })
            ->addColumn('status', function ($destino) {
                return $destino->status->descricao;
            })
            ->addColumn('acao', function ($destino) {
                $row = "<button class='btn btn-delete delete-vinculo' value='{$destino->empresa_vinculada->id}' title='Remover Vínculo'>";
                $row .= "<img src='";
                $row .= asset('images/remover.png');
                $row .= "' alt='Remover vínculo'>";
                $row .= "</button>";
                return $row;
            })
            ->rawColumns(['id_limpurb', 'cnpj', 'razao_social', 'total_residuos', 'total_equipamentos',
                'frequencia_coleta', 'status', 'acao'])
            ->make(true);
    }

    public static function renderVinculosRemovidos($vinculos)
    {
        return Datatables::of($vinculos)
            ->setRowId(function ($vinculo) {
                return $vinculo->id;
            })
            ->addColumn('id_limpurb', function ($vinculo) {
                return $vinculo->empresa_vinculada->id_limpurb;
            })
            ->addColumn('razao_social', function ($vinculo) {
                return $vinculo->empresa_vinculada->razao_social;
            })
            ->addColumn('status', function ($vinculo) {
                return $vinculo->empresa_vinculada->status->descricao;
            })
            ->addColumn('restore', function ($vinculo) {
                return "<button class='btn btn-restore' value='{$vinculo->id}'>Restaurar</button>";
            })
            ->rawColumns(['id_limpurb', 'razao_social', 'status', 'restore'])
            ->make(true);
    }

}