<?php

namespace Amlurb\Services;

use Yajra\Datatables\Datatables;

/**
 * classe para lidar com construcao dos DataTables do sistema
 * Class DataTablesRenderer
 * @package Amlurb\Repositories
 */
class FiscalRenderer
{

    public static function renderFiscalRastreabilidade($ctreList)
    {
        return Datatables::of($ctreList)
            ->addColumn('id', function ($results) {
                $ctreId = "<div class='ctre' data-id='$results->id'>$results->codigo</div>";
                return $ctreId;
            })
            ->addColumn('razaosocial', function ($results) {
                $razaoSocial = "<div class='empresa'>{$results->empresa->razao_social}</div>";
                return $razaoSocial;
            })
            ->addColumn('numeroamlurb', function ($results) {
                $limpurbId = "<div>{$results->empresa->id_amlurb}</div>";
                return $limpurbId;
            })
            ->addColumn('alerta', function () {
                $alerta = "<div></div>";
                return $alerta;
            })
            ->addColumn('rastreio', function ($results) {

                $rastreio = '<div class=\'rastreio-neutro btn rastreio-gg\' title=\'Gerador\'>G</div>';
                if ($results->data_validacao != null) {
                    $rastreio .= "<div class='rastreio-neutro btn rastreio-tr' title='Transportador'>T</div>";
                } else {
                    $rastreio .= "<div class='rastreio-neutro btn' title='Transportador'>T</div>";
                }

                if ($results->data_validacao_final != null) {
                    $rastreio .= "<div class='rastreio-neutro btn rastreio-df' title='Destino Final'>D</div>";
                } else {
                    $rastreio .= "<div class='rastreio-neutro btn' title='Destino Final'>D</div>";
                }

                return $rastreio;
            })
            ->addColumn('status', function ($results) {
                $status = '';
                switch ($results->status->id) {
                    case \config('enums.status.em_aberto'):
                        $status = "<div class='status status-aberto'>{$results->status->descricao}</div>";
                        break;
                    case \config('enums.status.expirado'):
                        $status = "<div class='status status-expirado'>{$results->status->descricao}</div>";
                        break;
                    case \config('enums.status.finalizado'):
                        $status = "<div class='status status-finalizado'>{$results->status->descricao}</div>";
                        break;
                }
                return $status;
            })
            ->rawColumns(['id', 'razaosocial', 'nramlurb', 'alerta', 'rastreio', 'status'])
            ->make(true);
    }

    public static function renderFiscalEmpresasSearch($empresas)
    {
        return Datatables::of($empresas)
            ->addColumn('id', function ($results) {
                $ctreId = "<div class='amlurb_id' data-id='{$results->id}'>{$results->id_limpurb}</div>";
                return $ctreId;
            })
            ->addColumn('nome_comercial', function ($results) {
                $nomeComercial = "<div class='nome_comercial'>{$results->nome_comercial}</div>";
                return $nomeComercial;
            })
            ->addColumn('razao_social', function ($results) {
                $razaoSocial = "<div class='razao_social'>{$results->razao_social}</div>";
                return $razaoSocial;
            })
            ->addColumn('cnpj', function ($results) {
                $cnpj = "<div class='cnpj'>{$results->cnpj}</div>";
                return $cnpj;
            })
            ->addColumn('bairro', function ($results) {
                $bairro = "<div class='endereco'>{$results->bairro}</div>";
                return $bairro;
            })
            ->addColumn('status', function ($results) {
                $status = "<div class='endereco'>{$results->descricao}</div>";
                return $status;
            })
            ->addColumn('acao', function ($results) {
                $acao = "<div class='acao text-center'>";
                if ($results->status_id == config('enums.status.ativo')) {
                    $acao.= '<a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="' . $results->id . '" title="Inativar empresa">';
                    $acao.= '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                    
                }
                else {
                    $acao.= '<a href="#" class="text-info" id="lnkAtivarDesativarEmpresa" data-id="' . $results->id . '" title="Ativar empresa">';
                    $acao.= '<i class="fa fa-power-off" aria-hidden="true"></i>';                    
                }
                $acao.= "</a></div>";
                return $acao;
            })
            ->rawColumns(['id', 'nome_comercial', 'razao_social', 'cnpj', 'bairro', 'status', 'acao'])
            ->make(true);
    }

    public static function renderFiscalGeradoresCondominios($ggs)
    {
        return Datatables::of($ggs)
            ->addColumn('id', function ($results) {
                if (!empty($results->empresa)) {
                    $ctreId = "<div class='amlurb_id' data-id='{$results->empresa->id}'>{$results->empresa->id_limpurb}</div>";
                    return $ctreId;
                }
            })
            ->addColumn('nome_comercial', function ($results) {
                if (!empty($results->empresa)) {
                    $nomeComercial = "<div class='nome_comercial'>{$results->empresa->nome_comercial}</div>";
                    return $nomeComercial;
                }
            })
            ->addColumn('razao_social', function ($results) {
                if (!empty($results->empresa)) {
                    $razaoSocial = "<div class='razao_social'>{$results->empresa->razao_social}</div>";
                    return $razaoSocial;
                }
            })
            ->addColumn('cnpj', function ($results) {
                if (!empty($results->empresa)) {
                    $cnpj = "<div class='cnpj'>{$results->empresa->cnpj}</div>";
                    return $cnpj;
                }
            })
            ->addColumn('bairro', function ($results) {
                if (!empty($results->empresa)) {
                    if (isset($results->empresa->empresa_endereco->bairro)){
                        $bairro = "<div class='endereco'>{$results->empresa->empresa_endereco->bairro}</div>";
                        return $bairro;
                    }
                }
            })
            ->addColumn('status', function ($results) {
                if (!empty($results->empresa)) {
                    if (isset($results->empresa->status->descricao)) {
                        $status = "<div class='status_id'>{$results->empresa->status->descricao}</div>";
                    } else {
                        $status = "<div class='status_id'></div>";
                    }
                    return $status;
                }
            })
            ->addColumn('acao', function ($results) {
                if (!empty($results->empresa)) {
                    $acao = "<div class='acao text-center'>";
                    if ($results->empresa->status_id == config('enums.status.ativo')) {
                        $acao.= '<a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="' . $results->empresa->id . '" title="Inativar empresa">';
                        $acao.= '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                    }
                    else {
                        $acao.= '<a href="#" class="text-info" id="lnkAtivarDesativarEmpresa" data-id="' . $results->empresa->id . '" title="Ativar empresa">';
                        $acao.= '<i class="fa fa-power-off" aria-hidden="true"></i>';                    
                    }
                    $acao.= "</a></div>";
                    return $acao;
                }
            }) 
            ->rawColumns(['id', 'nome_comercial', 'razao_social', 'cnpj', 'bairro', 'status', 'acao'])
            ->make(true);
    }

    public static function renderFiscalEmpresasFiliais($empresas)
    {   
        // print_r($empresas); exit();
        return Datatables::of($empresas)
            ->addColumn('id', function ($results) {
                if (!empty($results->id_limpurb)) {
                    return "<div class='amlurb_id' data-id='{$results->id}'>{$results->id_limpurb}</div>";
                }
            })
            ->addColumn('nome_comercial', function ($results) {
                if (!empty($results->nome_comercial)) {
                    return "<div class='nome_comercial'>{$results->nome_comercial}</div>";
                }
            })
            ->addColumn('nome_responsavel', function ($results) {
                if (!empty($results->nome_responsavel)) {
                    return "<div class='nome_responsavel'>{$results->nome_responsavel}</div>";
                }
            })
            ->addColumn('email', function ($results) {
                if (!empty($results->email)) {
                    return "<div class='email'>{$results->email}</div>";
                }
            })
            ->addColumn('status', function ($results) {
                if (!empty($results->status->descricao)) {
                    if (isset($results->status->descricao)) {
                        $status = "<div class='status_id'>{$results->status->descricao}</div>";
                    } else {
                        $status = "<div class='status_id'></div>";
                    }
                    return $status;
                }
            })
            ->addColumn('acao', function ($results) {
                if (!empty($results)) {
                    $acao = "<div class='acao text-center'>";
                    if ($results->status_id == config('enums.status.ativo')) {
                        $acao.= '<a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="' . $results->id . '" title="Inativar empresa">';
                        $acao.= '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                    }
                    else {
                        $acao.= '<a href="#" class="text-info" id="lnkAtivarDesativarEmpresa" data-id="' . $results->id . '" title="Ativar empresa">';
                        $acao.= '<i class="fa fa-power-off" aria-hidden="true"></i>';                    
                    }
                    $acao.= "</a></div>";
                    return $acao;
                }
            })
            ->rawColumns(['id', 'nome_comercial', 'nome_responsavel', 'email', 'status', 'acao'])
            ->make(true);
    }

    public static function renderFiscalCadastroEmpresas($empresas)
    {   
        return Datatables::of($empresas)
            ->addColumn('id', function ($results) {
                if (!empty($results->empresa)) {
                    return "<div class='amlurb_id' data-id='{$results->empresa->id}'>{$results->empresa->id_limpurb}</div>";
                }
            })
            ->addColumn('nome_comercial', function ($results) {
                if (!empty($results->empresa)) {
                return "<div class='nome_comercial'>{$results->empresa->nome_comercial}</div>";
                }
            })
            ->addColumn('razao_social', function ($results) {
                if (!empty($results->empresa)) {
                return "<div class='razao_social'>{$results->empresa->razao_social}</div>";
                }
            })
            ->addColumn('cnpj', function ($results) {
                if (!empty($results->empresa)) {
                return "<div class='cnpj'>{$results->empresa->cnpj}</div>";
                }
            })
            ->addColumn('bairro', function ($results) {
                if (!empty($results->empresa)) {
                    if (isset($results->empresa->empresa_endereco->bairro)) {
                        $bairro = "<div class='endereco'>{$results->empresa->empresa_endereco->bairro}</div>";
                    } else {
                        $bairro = "<div class='endereco'></div>";
                    }
                    return $bairro;
                }
            })
            ->addColumn('status', function ($results) {
                if (!empty($results->empresa)) {
                    if (isset($results->empresa->status->descricao)) {
                        $status = "<div class='status_id'>{$results->empresa->status->descricao}</div>";
                    } else {
                        $status = "<div class='status_id'></div>";
                    }
                    return $status;
                }
            })
            ->addColumn('acao', function ($results) {
                if (!empty($results->empresa)) {
                    $acao = "<div class='acao text-center'>";
                    if ($results->empresa->status_id == config('enums.status.ativo')) {
                        $acao.= '<a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="' . $results->empresa->id . '" title="Inativar empresa">';
                        $acao.= '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                    }
                    else {
                        $acao.= '<a href="#" class="text-info" id="lnkAtivarDesativarEmpresa" data-id="' . $results->empresa->id . '" title="Ativar empresa">';
                        $acao.= '<i class="fa fa-power-off" aria-hidden="true"></i>';                    
                    }
                    $acao.= "</a></div>";
                    return $acao;
                }
            })
            ->rawColumns(['id', 'nome_comercial', 'razao_social', 'cnpj', 'bairro', 'status', 'acao'])
            ->make(true);
    }

    public static function renderFiscalTransportadoresVeiculos($vinculos)
    {
        return Datatables::of($vinculos)
            ->addColumn('placa', function ($results) {
                return "<div class='placa' data-id='{$results->empresa->id}' data-id_veiculo='{$results->id}'>{$results->placa}</div>";
            })
            ->addColumn('tipo', function ($results) {
                return "<div class='tipo'>{$results->tipo}</div>";
            })
            ->addColumn('marca', function ($results) {
                return "<div class='marca'>{$results->marca}</div>";
            })
            ->addColumn('ano', function ($results) {
                return "<div class='razao_social'>{$results->ano_veiculo}</div>";
            })
            ->addColumn('renavam', function ($results) {
                return "<div class='renavam'>{$results->renavam}</div>";
            })
            ->addColumn('venc_ipva', function ($results) {
                $data = date2data($results->vencimento_ipva);
                return "<div class='venc_ipva'>{$data}</div>";
            })
            ->addColumn('capacidade', function ($results) {
                return "<div class='capacidade'>{$results->capacidade}</div>";
            })
            ->addColumn('tara', function ($results) {
                return "<div class='tara'>{$results->tara}</div>";
            })
            ->addColumn('status', function ($results) {
                return "<div class='status_veiculo'>{$results->status->descricao}</div>";
            })
            ->rawColumns(['placa', 'tipo', 'marca', 'ano', 'renavam', 'venc_ipva', 'capacidade', 'tara', 'status'])
            ->make(true);
    }

    public static function renderFiscalTransportadoresEquipamentos($vinculos)
    {
        return Datatables::of($vinculos)
            ->addColumn('equipamento_id', function ($results) {
                return "<div class='equipamento_id' data-id='{$results->id}'>{$results->codigo_amlurb}</div>";
            })
            ->addColumn('equipamento_nome', function ($results) {
                return "<div class='equipamento_nome'>{$results->equipamento->tipo_equipamento->nome}</div>";
            })
            ->addColumn('status', function ($results) {
                return "<div class='status_veiculo'>{$results->status->descricao}</div>";
            })
            ->rawColumns(['equipamento_id', 'equipamento_nome', 'status'])
            ->make(true);
    }

    public static function renderFiscalEmpresaVinculos($vinculos)
    {
        return Datatables::of($vinculos)
            ->addColumn('id', function ($results) {
                $id_ctre = (!is_null($results->empresa_vinculada->id_limpurb)) ?
                    $results->empresa_vinculada->id_limpurb : '';
                return "<div class='amlurb_id' data-id='{$results->empresa_vinculada->id}'>{$id_ctre}</div>";
            })
            ->addColumn('razao_social', function ($results) {
                return "<div class='razao_social'>{$results->empresa_vinculada->razao_social}</div>";
            })
            ->addColumn('cnpj', function ($results) {
                return "<div class='razao_social'>{$results->empresa_vinculada->cnpj}</div>";
            })
            ->addColumn('data_vencimento', function () {
                return "<div class='data_vencimento'></div>";
            })
            ->addColumn('status', function ($results) {
                return (isset($results->status->descricao)) ?
                    "<div class='status_id'>{$results->status->descricao}</div>" :
                    "<div class='status_id'></div>";
            })
            ->rawColumns(['id', 'razao_social', 'cnpj', 'data_vencimento', 'status'])
            ->make(true);
    }
}