<?php

namespace Amlurb\Services\DataTables;

use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class EquipamentosVeiculosRenderer
{

    public static function renderEquipamentos($equipamentos)
    {
        return Datatables::of($equipamentos)
            ->setRowId(function ($equipamento) {
                return $equipamento->id;
            })
            ->addColumn('codigo_amlurb', function ($equipamento) {
                return $equipamento->codigo_amlurb;
            })
            ->addColumn('nome', function ($equipamento) {
                return $equipamento->equipamento->tipo_equipamento->nome . ' - ' . $equipamento->equipamento->capacidade;
            })
            ->addColumn('vinculo', function ($equipamento) {
                // se nao houver vinculo, mostra empresa em posse do equipamento
                return isset($equipamento->equipamento_alocacao->empresa) ?
                    $equipamento->equipamento_alocacao->empresa->nome_comercial :
                    Auth::user()->empresa->nome_comercial;
            })
            ->addColumn('status', function ($equipamento) {
                return $equipamento->status->descricao;
            })
            ->addColumn('validade', function ($equipamento) {
                return  '-';//$equipamento->data_validade;
            })
            ->addColumn('acao', function ($equipamento) {
                return EquipamentosVeiculosRenderer::montaAcoesEquipamento($equipamento);
            })
            ->rawColumns(['codigo_amlurb', 'nome', 'vinculo', 'status', 'validade', 'acao'])
            ->make(true);
    }

    public static function renderVeiculos($veiculos)
    {
        return Datatables::of($veiculos)
            ->setRowId(function ($veiculo) {
                return $veiculo->id;
            })
            ->addColumn('codigo_amlurb', function ($veiculo) {
                return $veiculo->codigo_amlurb;
            })
            ->addColumn('placa', function ($veiculo) {
                return $veiculo->placa;
            })
            ->addColumn('ano', function ($veiculo) {
                return $veiculo->ano_veiculo;
            })
            ->addColumn('renavam', function ($veiculo) {
                return $veiculo->renavam;
            })
            ->addColumn('tipo', function ($veiculo) {
                return $veiculo->tipo;
            })
            ->addColumn('marca', function ($veiculo) {
                return $veiculo->marca;
            })
            ->addColumn('ipva', function ($veiculo) {
                return $veiculo->ipva;
            })
            ->addColumn('capacidade', function ($veiculo) {
                return $veiculo->capacidade;
            })
            ->addColumn('status', function ($veiculo) {
                return $veiculo->status->descricao;
            })
            ->addColumn('validade', function ($veiculo) {
                return  '-';//$veiculo->data_validade;
            })
            ->addColumn('acao', function ($veiculo) {
                return EquipamentosVeiculosRenderer::montaAcoesVeiculo($veiculo);
            })
            ->rawColumns(['codigo_amlurb', 'placa', 'ano', 'renavam', 'tipo', 'marca',
                'ipva', 'capacidade', 'status', 'validade', 'acao'
            ])
            ->make(true);

    }

    public static function renderBoletos($boletos)
    {
        return Datatables::of($boletos)
            ->setRowId(function ($boleto) {
                return $boleto->id;
            })
            ->addColumn('data_emissao', function ($boleto) {
                return $boleto->created_at;
            })
            ->addColumn('serv_guia', function ($boleto) {
                return $boleto->codTipoServGuia;
            })
            ->addColumn('tipo', function ($boleto) {
                return $boleto->tipo;
            })
            ->addColumn('codigo_item', function ($boleto) {
                return $boleto->codigo_item;
            })
            ->addColumn('data_vencimento', function ($boleto) {
                return $boleto->dataDeVencimento;
            })
            ->addColumn('status', function ($boleto) {
                return $boleto->status->descricao;
            })
            ->addColumn('acao', function ($boleto) {
                return EquipamentosVeiculosRenderer::montaAcoesBoleto($boleto);
            })
            ->rawColumns(['data_emissao', 'serv_guia', 'tipo', 'codigo_item',
                'data_vencimento', 'status', 'acao'
            ])
            ->make(true);

    }


    private static function montaAcoesEquipamento($equipamento)
    {
        $row = "";

        if ($equipamento->status->id == \config('enums.status.inativo') ||
            $equipamento->status->id == \config('enums.status.vencido')) {

            $btn_class = ($equipamento->status->id == \config('enums.status.inativo')) ?
                'ativar-equipamento' : 'renovar-equipamento';

            $row .= EquipamentosVeiculosRenderer::getRenovarIcon($btn_class);

        } else {
            $row .= EquipamentosVeiculosRenderer::getRenovarGreyIcon();
        }

        if ($equipamento->status->id == \config('enums.status.ativo')) {
            $row .= EquipamentosVeiculosRenderer::getRenovarQrCodeIcon();
        } else {
            $row .= EquipamentosVeiculosRenderer::getRenovarQrCodeGreyIcon();
        }

        $btn_class = ($equipamento->status->id == \config('enums.status.inativo') && (!$equipamento->data_validade))
            ? 'delete-equipamento-force' : 'delete-equipamento';
        $row .= EquipamentosVeiculosRenderer::getDeleteIcon($btn_class, $equipamento->id);

        return $row;
    }

    private static function montaAcoesVeiculo($veiculo)
    {
        $row = "";

        if ($veiculo->status->id == \config('enums.status.inativo') ||
            $veiculo->status->id == \config('enums.status.vencido') ||
            $veiculo->status->id == \config('enums.status.pagamento_pendente')) {

            $btn_class = ($veiculo->status->id == \config('enums.status.inativo') || $veiculo->status->id == \config('enums.status.pagamento_pendente')) ?
                'ativar-veiculo' : 'renovar-veiculo';

            $row .= EquipamentosVeiculosRenderer::getRenovarIcon($btn_class);

        } else {
            $row .= EquipamentosVeiculosRenderer::getRenovarGreyIcon();
        }
       
        if ($veiculo->status->id == \config('enums.status.ativo')) {
            $row .= EquipamentosVeiculosRenderer::getRenovarQrCodeIcon();
        } else {
            $row .= EquipamentosVeiculosRenderer::getRenovarQrCodeGreyIcon();
        }

        $btn_class = ($veiculo->status->id == \config('enums.status.inativo') && (!$veiculo->data_validade))
            ? 'delete-veiculo-force' : 'delete-veiculo';
        $row .= EquipamentosVeiculosRenderer::getDeleteIcon($btn_class, $veiculo->id);
        if ($veiculo->status->id == \config('enums.status.inconsistente')) {
            $row .= EquipamentosVeiculosRenderer::getEditarIcon('editar-veiculo', $veiculo->id);
        }
        return $row;
    }

    private static function montaAcoesBoleto($boleto)
    {
        $class = 'link-acoes__imprimir'; $text = 'Imprimir';
        if (strtolower($boleto->status->descricao) == 'pago') {
            $class = 'link-acoes__ver'; $text = 'Ver';
        } elseif (strtolower($boleto->status->descricao) == 'vencido') {
            $class = 'link-acoes__2via'; $text = '2a via';
        }
        return "<a href='#' class='btn link-acoes {$class}'>{$text}</a>";
    }

    private static function getRenovarIcon($btn_class)
    {
        $img = asset('images/renovar.png');
        $row = "<button class='btn btn-delete {$btn_class}' title='Ativar/Renovar'>";
        $row .= "<img src='{$img}' alt='Ativar/Renovar'>";
        $row .= "</button>";
        return $row;
    }

    private static function getRenovarGreyIcon()
    {
        $img = asset('images/renovar-grey.png');
        $row = "<button class='btn btn-delete' title='Ativar/Renovar'>";
        $row .= "<img src='{$img}' alt='Ativar/Renovar'>";
        $row .= "</button>";
        return $row;
    }

    private static function getRenovarQrCodeIcon()
    {
        $img = asset('images/qr-renova.png');
        $row = "<button class='btn btn-delete solicitar-qrcode' title='QR Code'>";
        $row .= "<img src='{$img}' alt='QR Code'>";
        $row .= "</button>";
        return $row;
    }

    private static function getEditarIcon($btn_class, $id)
    {
        $img = asset('images/edit.png');
        $row = "<button class='btn btn-delete {$btn_class}' value='{$id}' title='Editar'>";
        $row .= "<img src='{$img}' alt='Editar'>";
        $row .= "</button>";
        return $row;
    }

    private static function getRenovarQrCodeGreyIcon()
    {
        $img = asset('images/qr-renova-grey.png');
        $row = "<button class='btn btn-delete' title='QR Code'>";
        $row .= "<img src='{$img}' alt='QR Code'>";
        $row .= "</button>";
        return $row;
    }

    private static function getDeleteIcon($btn_class, $id)
    {
        $img = asset('images/remover.png');
        $row = "<button class='btn btn-delete {$btn_class}' value='{$id}' title='Excluir'>";
        $row .= "<img src='{$img}' alt='Excluir'>";
        $row .= "</button>";
        return $row;
    }

}