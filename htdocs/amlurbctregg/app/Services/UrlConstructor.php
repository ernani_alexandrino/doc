<?php

namespace Amlurb\Services;

class UrlConstructor
{
    public static function urlBoletoTaxaAmlurb($filename)
    {
        if (config('app.env') != 'local') { // se nao for ambiente DEV
            return storage_path('boletos' . DIRECTORY_SEPARATOR . $filename);
        }
        return public_path('templates' . DIRECTORY_SEPARATOR . 'boleto' . DIRECTORY_SEPARATOR . $filename);
    }

    public static function meuCadastro($empresa_tipo_id)
    {
        $url = 'painel' . DIRECTORY_SEPARATOR;

        switch($empresa_tipo_id) {
            case config('enums.empresas_tipo.grande_gerador'):
                $url .= 'gerador';
                break;
            case config('enums.empresas_tipo.transportador'):
                $url .= 'transportador';
                break;
            case config('enums.empresas_tipo.destino_final_reciclado'):
                $url .= 'destino-final';
                break;
            case config('enums.empresas_tipo.orgao_publico'):
                $url .= 'orgao-publico';
                break;
            case config('enums.empresas_tipo.servico_saude'):
                $url .= 'servico-saude';
                break;
            case config('enums.empresas_tipo.condominio_misto'):
                $url .= 'condominio-misto';
                break;
        }

        return $url .= DIRECTORY_SEPARATOR . 'meu-cadastro';
    }
}