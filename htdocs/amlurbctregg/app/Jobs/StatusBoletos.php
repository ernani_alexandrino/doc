<?php

namespace Amlurb\Jobs;

use Amlurb\Models\Boleto;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Services\ServiceBoletos;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;

class StatusBoletos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $serviceBoletos;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->serviceBoletos = new ServiceBoletos();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $boletos = Boleto::where('status_id', \config('enums.status.pagamento_pendente'))->get();
        foreach ($boletos as $boleto) {
            $this->processaBoleto($boleto);
        }
    }

    private function processaBoleto(Boleto $boleto)
    {

        $params_pesquisa = $this->getParametrosConsulta($boleto->numeroGuia, $boleto->codTipoServGuia);
        $retorno = $this->serviceBoletos->retornoGuia($params_pesquisa);
        if (is_object($retorno) && property_exists($retorno, 'pagamentos')) {
            if (property_exists($retorno->pagamentos, 'PagamentoDTO')) {
                if ($retorno->pagamentos->PagamentoDTO->codNumGuia == $boleto->numeroGuia) {
                    $dataPagamento = Carbon::parse($retorno->pagamentos->PagamentoDTO->dtPgtoGuia);
                    $this->atualizaBoleto($boleto, $dataPagamento);
                }
            }
        }

    }

    private function atualizaBoleto(Boleto $boleto, $dataPagamento)
    {
      
        $boleto->status_id = \config('enums.status.pago');
        $boleto->dataDePagamento = $dataPagamento->format('Y-m-d');
        $boleto->save();

        if ($boleto->tipo == \config('enums.tipos_boleto.veiculo')) {
            $veiculo = EmpresasVeiculo::whereId($boleto->boleto_veiculos->veiculo_id)->first();
            $this->ativaVeiculo($veiculo);
        } else { // empresa
            $empresa = Empresa::whereId($boleto->empresa_id);
            $this->ativaEmpresa($empresa);
        }

    }

    private function ativaVeiculo(EmpresasVeiculo $veiculo)
    {
        $veiculo->status_id = \config('enums.status.ativo');
        $veiculo->data_validade = Carbon::now()->addYear();
        $veiculo->save();
    }

    private function ativaEmpresa(Empresa $empresa)
    {
        $empresa->status_id = \config('enums.status.ativo');
        $empresa->validade_cadastro = Carbon::now()->addYear();
        $empresa->save();
    }

    private function getParametrosConsulta($num_guia, $cod_servico)
    {
        $now = Carbon::now();
        $now->startOfMonth();
        $future = Carbon::now();
        $future->endOfMonth();
        return [
            'InputMessagePagamento' => [
                'inputPagamento' => [
                    'dtIniPagamento' => $now->format('d/m/Y'),
                    'dtFimPagamento' => $future->format('d/m/Y'),
                    'numGuia' => $num_guia,
                    'anoEmissaoGuia' => $now->format('Y'),
                    'codSiglaUnidade' => 'AMLURB',
                    'codTipoServico' => $cod_servico,
                ]
            ]
        ];
    }


}
