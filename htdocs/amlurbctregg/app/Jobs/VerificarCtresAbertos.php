<?php

namespace Amlurb\Jobs;

use Amlurb\Models\Ctre;
use Amlurb\Repositories\CtreRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

/**
 *
 * muda status de CTREs em aberto que ultrapassarem a data de expiracao
 *
 * Class VerificarCtresAbertos
 * @package Amlurb\Jobs
 */

class VerificarCtresAbertos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $ctreRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->ctreRepository = new CtreRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ctres = $this->ctreRepository->getCTREsEmAberto();
        foreach ($ctres as $ctre) {
            $this->processaCtre($ctre);
        }
    }

    // atualiza status do CTRE se data de expiracao tiver sido ultrapassada
    protected function processaCtre(Ctre $ctre)
    {
        $now = new Carbon();
        if ($now->gt($ctre->data_expiracao)) {
            $ctre->status_id = \config('enums.status.expirado');
            $ctre->save();
        }
    }

}
