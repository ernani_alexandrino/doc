<?php

namespace Amlurb\Traits;

trait PermissionsUserTrait
{

    /**
     * Verifica as permissões
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        $relation = $this->getRelationPermission();
        $column = $this->getColumnAccess();
        $this->load($relation);

        if ($this->{$relation}) {
            $acessos = json_decode($this->{$relation}->{$column}, true);

            if (is_array($acessos)) {
                $permission = explode(':', $permission);
                return $this->check_permissions($permission, $acessos);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    protected function getRelationPermission()
    {
        if ($this->relationPerfil) {
            $relation = $this->relationPerfil;
        } else {
            $relation = 'perfil';
        }

        return $relation;
    }

    protected function getColumnAccess()
    {
        if ($this->columnAcesso) {
            $column = $this->columnAcesso;
        } else {
            $column = 'acesso';
        }

        return $column;
    }


    /**
     * Verifica se o destino é aterro.
     *
     * @param $needle
     * @param $arrays
     * @return bool (int) 0 1.
     */
    protected function check_permissions($needle, $arrays)
    {
        $checked = $this->array_search_deep($needle[0], $arrays);
        if ($checked) {
            $toCheck = $checked;
            if (count($needle) > 1) {
                for ($i = 1; $i < count($needle); $i++) {
                    if (array_key_exists($needle[$i], $toCheck) || in_array($needle[$i], $toCheck)) {
                        if (isset($toCheck[$needle[$i]]) && $i != count($needle) - 1) {
                            $toCheck = $toCheck[$needle[$i]];
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Verifica se o destino é aterro.
     *
     * @param $action
     * @param $arrays
     * @return bool (int) 0 1.
     */
    protected function has_action($action, $arrays)
    {

        if (in_array($action, $arrays)) {
            return true;
        } else {
            foreach ($arrays as $array) {
                if (is_array($array) && in_array($action, $array)) {
                    return true;
                } elseif (is_array($array)) {
                    $checked = $this->has_action($action, $array);
                    if ($checked) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /**
     * Verifica se o destino é aterro.
     *
     * @param $permission
     * @param $action
     * @param $arrays
     * @return bool (int) 0 1.
     */
    protected function permission_has_action($permission, $action, $arrays)
    {
        $checked = $this->array_search_deep($permission, $arrays);
        if ($checked && $this->has_action($action, $checked)) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se o destino é aterro.
     *
     * @param $needle
     * @param $arrays
     * @return int|mixed (int) 0 1.
     */
    protected function array_search_deep($needle, $arrays)
    {
        if (is_array($arrays) && array_key_exists($needle, $arrays)) {
            return $arrays[$needle];
        } elseif (is_array($arrays)) {
            foreach ($arrays as $array) {
                if (is_array($array) && array_key_exists($needle, $array)) {
                    return $array[$needle];
                } elseif (is_array($array)) {
                    $checked = $this->array_search_deep($needle, $array);
                    if ($checked) {
                        return $checked;
                    }
                } else {
                    return 0;
                }
            }
            return 0;
        } else {
            return 0;
        }
    }
}
