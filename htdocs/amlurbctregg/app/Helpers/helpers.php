<?php

use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Carbon\Carbon;

if (!function_exists('getFileS3')) {
    /**
     * Retorna  o Arquivo (Documento) em um Download Automático.
     *
     * @param  string $pasta a pasta onde se encontram as pastas dos arquivos, por Default = 'licencas'
     * @param  string $arquivo o nome do arquivo será baixado
     * @param  string $id_empresa o id da empresa dona do arquivo.
     *
     * @return headers Baixando arquivo automaticamente .
     */
    function getFileS3($pasta = 'licencas', $arquivo, $id_empresa, $placa = null)
    {
        if (!empty($pasta) && !empty($arquivo) && !empty($id_empresa)) {
            $ext = pathinfo($arquivo, PATHINFO_EXTENSION);
            $content_type = '';
            switch ($ext) {
                case 'pdf':
                    $content_type = 'application/pdf';
                break;
                
                case 'jpeg':
                    $content_type = 'image/jpeg';
                break;
            
                case 'png':
                    $content_type = 'image/png';
                break;
        
                case 'mpeg':
                    $content_type = 'audio/mpeg';
                break;
    
                case 'ogg':
                    $content_type = 'audio/ogg';
                break;

                case 'mp4':
                    $content_type = 'video/mp4';
                break;
            }
            $headers = [
                'Content-Type'        => $content_type,
                'Content-Disposition' => 'attachment; filename="'. $arquivo .'"',
            ];
            if (is_null($placa))
                $file = $pasta.'/'.$id_empresa.'/'.$arquivo;
            else
            $file = $pasta.'/'.$id_empresa.'/'.mb_strtolower($placa).'/'.$arquivo;
            return \Response::make(Storage::disk('s3')->get($file), 200, $headers);
        }
    }
}

if (!function_exists('pd')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  dynamic  mixed
     *
     * @return void
     */
    function pd()
    {
        $backtrace = debug_backtrace();
        echo '<span style="color:red">' . $backtrace[0]['file'] . '(' . $backtrace[0]['line'] . ') </span>';
        echo '<pre>';
        array_map(function ($x) {
            print_r($x);
        }, func_get_args());
        echo '</pre>';
        die;
    }
}

if (!function_exists('printr')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed $args
     *
     * @return void
     */
    function printr(...$args)
    {
        http_response_code(500);

        $backtrace = debug_backtrace();
        echo '<span style="color:red">' . $backtrace[0]['file'] . '(' . $backtrace[0]['line'] . ') </span>';
        echo '<pre>';

        array_map(function ($x) {
            print_r($x);
        }, func_get_args());

        echo '</pre>';
    }
}

if (!function_exists('formataPlacadeCarro')) {
    function formataPlacadeCarro($placa){
        $tam	= strlen($placa);
        $primeiraParte = substr($placa,0,3);
        $segundaParte = substr($placa,3);
       
        $PLACA = $primeiraParte ."-". $segundaParte;
        return $PLACA;
      }
}

if (!function_exists('formatarVolume')) {
    /**
     * Retorna o volume tradado para exibir na tela.
     *
     * @param         $number
     * @param  string $unidade default 'kg' a unidade que deseja que o volume seja tradao
     *
     * @return integer o volume tratado
     */
    function formatarVolume($number, $unidade = 'kg')
    {
        $unidade = strtolower($unidade);
        if ($unidade == 'kg') {

            return number_format($number / 1000, 2, ',', '.');

        } else {
            if ($unidade == 'ton' || $unidade == 'm³') {

                return number_format($number / 1000000, 3, ',', '.');

            }
        }

        return number_format($number, 0, ',', '.');


    }
}

if (!function_exists('formatarVolumeCalc')) {
    /**
     * Retorna o volume tradado para exibir na tela.
     *
     * @param  strint $volume  o volume a ser trado
     * @param  string $unidade default 'kg' a unidade que deseja que o volume seja tradao
     *
     * @return integer o volume tratado
     */
    function formatarVolumeCalc($number, $unidade = 'kg')
    {
        $unidade = strtolower($unidade);
        if ($unidade == 'kg') {

            return number_format($number / 1000, 2, '.', '');

        } else {
            if ($unidade == 'ton' || $unidade == 'm³') {

                return number_format($number / 1000000, 3, '.', '');
            }
        }

        return (int)$number;


    }
}

if (!function_exists('formatarVolumeDB')) {
    /**
     * Retorna o volume tradado para inserir no banco.
     *
     * @param  strint $volume  o volume a ser trado
     * @param  string $unidade default 'kg' a unidade que deseja que o volume seja tradao
     *
     * @return integer o volume tratado
     */
    function formatarVolumeDB($number, $unidade = 'kg')
    {
        $unidade = strtolower($unidade);

        $number = str_replace('_', '', $number);

        if (strpos($number, ',')) {
            $number = str_replace('.', '', $number);
        }

        $number = str_replace(',', '.', $number);
        if ($unidade == 'kg') {
            return $number * 1000;
        } elseif ($unidade == 'ton' || $unidade == 'm³') {
            return $number * 1000000;
        }
        return str_replace('.', '', $number);


    }
}


if (!function_exists('dataExibe')) {
    /**
     * Retorna  a data no formato dd/mm/yyyy.
     *
     * @param  string $data a data que deseja converter para formato a exibir
     *
     * @return string .
     */
    function dataExibe($date)
    {
        if ($date) {
            return date('d/m/Y', strtotime(str_replace('-', '/', $date)));
        } else {

            return '';
        }

    }
}

if (!function_exists('setValorBalanco')) {
    /**
     * Retorna  o valor pronto para gravar no de acordo com o balanco.
     *
     * @param  string $valor   o valor a  que vai inserir no banco ex: 100.50 , 1.100,50, 100
     * @param  bolean $balanco um bolean ou 0 ou 1 onde false ou 0 = um retorno negativo e true ou 1 um retorno positivo
     *
     * @return integer  .
     */
    function setValorBalanco($valor, $balanco)
    {
        $valor = str_replace('_', '', $valor);
        if ($balanco) {
            return $valor * 100;
        }
        return $valor * -100;

    }
}

if (!function_exists('formataValor')) {
    /**
     * Retorna  o valor pronto para exibir na view.
     *
     * @param  integer $valor    a ser exbido na view.
     * @param  integer $decimals quantidade de casas decimais default: 2.
     *
     * @return string  1.100,50.
     */
    function formataValor($valor, $decimals = 2)
    {
        return number_format($valor / 100, $decimals, ',', '.');
    }
}

if (!function_exists('formataValorNegative')) {
    /**
     * Retorna  o valor pronto para exibir na view.
     *
     * @param  integer $valor    a ser exbido na view.
     * @param  integer $decimals quantidade de casas decimais default: 2.
     *
     * @return string  1.100,50.
     */
    function formataValorNegative($valor, $decimals = 2)
    {
        $isNegative = $valor < 0;
        $valor = str_replace('-', '', $valor);
        if ($isNegative) {
            return '(' . number_format($valor / 100, $decimals, ',', '.') . ')';
        }
        return number_format($valor / 100, $decimals, ',', '.');
    }
}
if (!function_exists('formataValorDB')) {
    /**
     * Retorna  o valor pronto para exibir na view.
     *
     * @param  integer $valor a ser inserido no DB..
     *
     * @return Integer  100000.
     */
    function formataValorDB($valor)
    {
        if (strpos($valor, ',')) {
            $valor = str_replace('.', '', $valor);
        }
        $valor = str_replace(',', '.', $valor);

        return $valor * 100;
    }
}

if (!function_exists('formataValorCalc')) {
    /**
     * Retorna  o valor pronto para exibir na view.
     *
     * @param  integer $valor    a ser exbido na view.
     * @param  integer $decimals quantidade de casas decimais default: 2.
     *
     * @return string  1.100,50.
     */
    function formataValorCalc($valor, $decimals = 2)
    {
        return number_format($valor / 100, $decimals, '.', '');
    }
}

if (!function_exists('cleanNumber')) {
    /**
     * Retorna  o numero sem caracteres especiais.
     *
     * @param  string .
     *
     * @return string  1.100,50.
     */
    function cleanNumber($number)
    {
        $simbols = [
            '\'',
            '-',
            '\\',
            '/',
            '_',
            ',',
            '.',
            ' '
        ];
        return str_replace($simbols, '', $number);
    }
}

if (!function_exists('showDateReports')) {
    /**
     * Retorna a data no formato com o mês em PT.
     *
     * @param  string a data que deseja converter .
     * @param  string o separador da data.
     * @param  string se quer retornar só mês/ano ou dias/mês/ano default days .
     *
     * @return string  Jan/2017 ou 01/01/2017.
     */
    function showDateReports($date, $separator = '-', $by_month_or_day = 'days')
    {
        $mes_extenso = [
            '01' => 'Jan',
            '02' => 'Fev',
            '03' => 'Mar',
            '04' => 'Abr',
            '05' => 'Mai',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Ago',
            '09' => 'Set',
            '10' => 'Out',
            '11' => 'Nov',
            '12' => 'Dez'
        ];
        $date = str_replace('/', '-', $date);

        if ($by_month_or_day == 'month') {
            $date = explode('-', $date);
            return $mes_extenso[$date[1]] . $separator . $date[0];
        }

        return date('d' . $separator . 'm' . $separator . 'Y', strtotime($date));

    }

}
if (!function_exists('showNumeroDoc')) {
    /**
     * Retorna a data no formato com o mês em PT.
     *
     * @param  string o separador da data.
     * @param  string se quer retornar só mês/ano ou dias/mês/ano default days .
     *
     * @return string  Jan/2017 ou 01/01/2017.
     */
    function showNumeroDoc($numero, $tipo_doc)
    {
        $tipo_doc = $tipo_doc{0};
        $numero = str_pad($numero, 8, 0, STR_PAD_LEFT);
        return strtoupper($tipo_doc) . '-' . $numero;

    }
}
if (!function_exists('encodeDate')) {
    /**
     * Retorna a data no formato com o mês em PT.
     *
     * @param  string o separador da data.
     * @param  string se quer retornar só mês/ano ou dias/mês/ano default days .
     *
     * @return string  Jan/2017 ou 01/01/2017.
     */
    function encodeDate($date, $returnFalse = null)
    {
        if ($date) {
            return date('Y-m-d', strtotime(str_replace('/', '-', $date)));
        } else {
            return $returnFalse;
        }
    }
}
if (!function_exists('parseVerbatim')) {
    /**
     * Retorna strign para se utilizar no javascript como variavel ex: se passar id retorna {{id}} .
     *
     * @param  string o nome da variavel que deseja passar.
     *
     * @return string  {{string}}.
     */
    function parseVerbatim($varName)
    {
        return '{' . '{' . $varName . '}' . '}';
    }
}
if (!function_exists('mask')) {
    /**
     * Retorna strign para se utilizar no javascript como variavel ex: se passar id retorna {{id}} .
     *
     * @param  string o nome da variavel que deseja passar.
     *
     * @return string  {{string}}.
     */
    function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;

        for ($i = 0; $i <= strlen($mask) - 1; ++$i) {

            if ($mask[$i] == '#') {

                if (isset($val[$k])) {

                    $maskared .= $val[$k++];

                }

            } else {

                if (isset($mask[$i])) {

                    $maskared .= $mask[$i];

                }
            }
        }

        return $maskared;
    }
}

if (!function_exists('isAterro')) {
    /**
     * Verifica se o destino é aterro.
     *
     * @param  string o nome da variavel que deseja passar.
     *
     * @return int 0 ou 1.
     */
    function isAterro($destino)
    {
        return strpos(strtolower($destino), 'aterro') ? 1 : 0;
    }
}

if (!function_exists('isReciclavel')) {
    /**
     * Verifica se o destino é aterro.
     *
     * @param  string o nome da variavel que deseja passar.
     *
     * @return int 0 1.
     */
    function isReciclavel($categoria)
    {
        return strtolower($categoria) == 'reciclável' ? 1 : 0;
    }
}
if (!function_exists('array_search_deep')) {
    /**
     * Verifica se o destino é aterro.
     *
     * @param  string o nome da variavel que deseja passar.
     *
     * @return int 0 1.
     */
    function array_search_deep($needle, $arrays)
    {
        if (is_array($arrays) && array_key_exists($needle, $arrays)) {

            return $arrays[$needle];

        } elseif (is_array($arrays)) {

            foreach ($arrays as $array) {

                if (is_array($array) && array_key_exists($needle, $array)) {

                    return $array[$needle];

                } else {

                    if (is_array($array)) {

                        $checked = array_search_deep($needle, $array);

                        if ($checked) {

                            return $checked;

                        }

                    } else {

                        return 0;

                    }
                }
            }
            return 0;
        } else {
            return 0;
        }

    }
}
if (!function_exists('check_permissions')) {
    /**
     * Verifica se o destino é aterro.
     *
     * @param  string o nome da variavel que deseja passar.
     *
     * @return int 0 1.
     */
    function check_permissions($needle, $arrays)
    {
        $checked = array_search_deep($needle[0], $arrays);
        if ($checked) {
            $toCheck = $checked;
            if (count($needle) > 1) {
                for ($i = 1; $i < count($needle); $i++) {
                    if (array_key_exists($needle[$i], $toCheck) || in_array($needle[$i], $toCheck)) {
                        if (isset($toCheck[$needle[$i]]) && $i != count($needle) - 1) {
                            $toCheck = $toCheck[$needle[$i]];
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                return true;
            }
        } else {
            return false;
        }

    }
}

if (!function_exists('has_action')) {
    /**
     * Verifica se o destino é aterro.
     *
     * @param  string o nome da variavel que deseja passar.
     *
     * @return (int) 0 1.
     */
    function has_action($action, $arrays)
    {

        if (in_array($action, $arrays)) {
            return true;
        } else {
            foreach ($arrays as $array) {
                if (is_array($array) && in_array($action, $array)) {
                    return true;
                } else {
                    if (is_array($array)) {
                        $checked = has_action($action, $array);
                        if ($checked) {
                            return true;
                        }
                    }
                }

            }
            return false;
        }
        return false;

    }
}
if (!function_exists('permission_has_action')) {
    /**
     * Verifica se o destino é aterro.
     *
     * @param  string o nome da variavel que deseja passar.
     *
     * @return (int) 0 1.
     */
    function permission_has_action($permission, $action, $arrays)
    {
        $checked = array_search_deep($permission, $arrays);
        if ($checked && has_action($action, $checked)) {
            return true;
        }
        return false;

    }
}
if (!function_exists('formatar')) {
    /**
     * Função para formatar cnpj, CEP, FONE,CPF,RG
     *
     * @param $string
     *
     * @return string
     */

    function formatar($tipo = "", $string, $size = 10)
    {
        $string = preg_replace("[^0-9]", "", $string);


        switch ($tipo) {
            case 'fone':
                if ($size === 10) {
                    $string = '(' . substr($tipo, 0, 2) . ') ' . substr($tipo, 2, 4)
                        . '-' . substr($tipo, 6);
                } else {
                    if ($size === 11) {
                        $string = '(' . substr($tipo, 0, 2) . ') ' . substr($tipo, 2, 5)
                            . '-' . substr($tipo, 7);
                    }
                }
                break;
            case 'cep':
                $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
                break;
            case 'cpf':
                $string = substr($string, 0, 3) . '.' . substr($string, 3, 3) .
                    '.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
                break;
            case 'cnpj':
                $string = str_replace("-", "", str_replace("/", "", str_replace(".", "", $string)));
                break;
            case 'rg':
                $string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
                    '.' . substr($string, 5, 3);
                break;
            default:
                $string = 'É ncessário definir um tipo(fone, cep, cpg, cnpj, rg)';
                break;
        }
        return $string;
    }

}
if (!function_exists('url_amigavel')) {
    /**
     * Função para remover acentos
     *
     * @param $string
     *
     * @return string
     */
    function url_amigavel($string)
    {
        $table = array(
            'Š' => 'S',
            'š' => 's',
            'Đ' => 'Dj',
            'đ' => 'dj',
            'Ž' => 'Z',
            'ž' => 'z',
            'Č' => 'C',
            'č' => 'c',
            'Ć' => 'C',
            'ć' => 'c',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
            'Ŕ' => 'R',
            'ŕ' => 'r',
        );
        // Traduz os caracteres em $string, baseado no vetor $table
        $string = strtr($string, $table);
        // converte para minúsculo
        $string = strtolower($string);
        // remove caracteres indesejáveis (que não estão no padrão)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        // Remove múltiplas ocorrências de hífens ou espaços
        $string = preg_replace("/[\s-]+/", " ", $string);
        // Transforma espaços e underscores em hífens
        $string = preg_replace("/[\s_]/", "-", $string);
        // retorna a string
        return $string;
    }

}

if (!function_exists('decodeDate')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  dynamic  mixed
     *
     * @return void
     */
    function decodeDate($date)
    {
        if ($date) {
            return date('d/m/Y', strtotime(str_replace('-', '/', $date)));
        } else {
            return '';
        }
    }
}
if (!function_exists('valida_cnpj')) {
    /**
     * Valida CNPJ.
     *
     * @author Luiz Otávio Miranda <contato@todoespacoonline.com/w>
     *
     * @param string $cnpj
     *
     * @return bool true para CNPJ correto
     */
    function valida_cnpj($cnpj)
    {
        // Deixa o CNPJ com apenas números
        $cnpj = preg_replace('/[^0-9]/', '', $cnpj);

        // Garante que o CNPJ é uma string
        $cnpj = (string)$cnpj;

        // O valor original
        $cnpj_original = $cnpj;

        // Captura os primeiros 12 números do CNPJ
        $primeiros_numeros_cnpj = substr($cnpj, 0, 12);

        /*
         * Multiplicação do CNPJ
         *
         * @param string $cnpj Os digitos do CNPJ
         * @param int $posicoes A posição que vai iniciar a regressão
         * @return int O
         *
         */
        if (!function_exists('multiplica_cnpj')) {
            function multiplica_cnpj($cnpj, $posicao = 5)
            {
                // Variável para o cálculo
                $calculo = 0;

                // Laço para percorrer os item do cnpj
                for ($i = 0; $i < strlen($cnpj); ++$i) {
                    // Cálculo mais posição do CNPJ * a posição
                    $calculo = $calculo + ($cnpj[$i] * $posicao);

                    // Decrementa a posição a cada volta do laço
                    --$posicao;

                    // Se a posição for menor que 2, ela se torna 9
                    if ($posicao < 2) {
                        $posicao = 9;
                    }
                }
                // Retorna o cálculo
                return $calculo;
            }
        }

        // Faz o primeiro cálculo
        $primeiro_calculo = multiplica_cnpj($primeiros_numeros_cnpj);

        // Se o resto da divisão entre o primeiro cálculo e 11 for menor que 2, o primeiro
        // Dígito é zero (0), caso contrário é 11 - o resto da divisão entre o cálculo e 11
        $primeiro_digito = ($primeiro_calculo % 11) < 2 ? 0 : 11 - ($primeiro_calculo % 11);

        // Concatena o primeiro dígito nos 12 primeiros números do CNPJ
        // Agora temos 13 números aqui
        $primeiros_numeros_cnpj .= $primeiro_digito;

        // O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
        $segundo_calculo = multiplica_cnpj($primeiros_numeros_cnpj, 6);
        $segundo_digito = ($segundo_calculo % 11) < 2 ? 0 : 11 - ($segundo_calculo % 11);

        // Concatena o segundo dígito ao CNPJ
        $cnpj = $primeiros_numeros_cnpj . $segundo_digito;

        // Verifica se o CNPJ gerado é idêntico ao enviado
        if ($cnpj === $cnpj_original) {
            return true;
        }
    }
}
if (!function_exists('gera_codigo_limpurb')) {
    /**
     * Função para criar o código Limpurb alphanumérico EX: RRG(7 digitos aleatórios únicos)/(4 digitos do ano);
     *
     * @param int $idRamoAtividade
     * @param int $idTipoRamoAtividade
     * @param int $idGeracaoDiaria
     *
     * @return string
     */
    function gera_codigo_limpurb($idRamoAtividade = 0, $idTipoRamoAtividade = 0, $idGeracaoDiaria = 0)
    {

        $tipoEmpresa = '';

        if ($idRamoAtividade == \Illuminate\Support\Facades\Config::get('enums.ramo_atividade.tratamento_residuos')) {
            if ($idTipoRamoAtividade == 39) {
                $tipoEmpresa = 'TR';
            } elseif ($idTipoRamoAtividade == 41) {
                $tipoEmpresa = 'CR';
            } elseif ($idTipoRamoAtividade == 42) {
                $tipoEmpresa = 'DR';
            }
        } else {
            if ($idGeracaoDiaria == 1 || $idGeracaoDiaria == 2) {
                $tipoEmpresa = 'PG';
            } else {
                $tipoEmpresa = 'GG';
            }
        }

        $numeroSeteDigitos = rand(0, 9999999);

        $numeroSeteDigitosCompleto = str_pad($numeroSeteDigitos, 7, '0', STR_PAD_LEFT);

        $digitosAno = date('Y');

        $codigoLimpurb = $tipoEmpresa . $numeroSeteDigitosCompleto . '/' . $digitosAno;

        $existeCodigoLimpurb = Empresa::where('id_limpurb', $codigoLimpurb)->count();

//        echo $codigoLimpurb.' ';
//        echo $existeCodigoLimpurb;

        if ($existeCodigoLimpurb) {
            if ($idRamoAtividade == \Illuminate\Support\Facades\Config::get('enums.ramo_atividade.tratamento_residuos')) {
                $codigoLimpurb = gera_codigo_limpurb($idRamoAtividade, $idRamoAtividade);
            } else {
                $codigoLimpurb = gera_codigo_limpurb($idRamoAtividade, $idRamoAtividade, $idGeracaoDiaria);
            }
        }

        return $codigoLimpurb;
    }
}
if (!function_exists('data2date')) {
    /**
     * Formata a data para formato BD
     *
     * @param $data
     *
     * @return string
     */
    function data2date($data)
    {
        $data = substr($data, 6, 4) . '-' . substr($data, 3, 2) . '-' . substr($data, 0, 2);

        // retorna a string
        return $data;
    }

}
if (!function_exists('date2data')) {
    /**
     * Formata a data para formato Brasileiro
     *
     * @param $data
     *
     * @return string
     */
    function date2data($data)
    {
        $data = substr($data, 8, 2) . '/' . substr($data, 5, 2) . '/' . substr($data, 0, 4);

        // retorna a string
        return $data;
    }
}

if (!function_exists('datetime2data')) {
    /**
     * Formata a data para formato Brasileiro
     *
     * @param $data
     *
     * @return string
     */
    function datetime2data($data)
    {
        if ($data) {
            $data = substr($data, 8, 2) . '/' . substr($data, 5, 2) . '/' . substr($data, 0, 4);
        }

        // retorna a string
        return $data;
    }
}

if (!function_exists('datetime2datatempo')) {
    /**
     * Formata a data para formato Brasileiro
     *
     * @param $data
     *
     * @return string
     */
    function datetime2datatempo($data)
    {
        $data = substr($data, 8, 2) . '/' . substr($data, 5, 2) . '/' . substr($data, 0, 4) . " " . substr($data, 11,
                8);

        // retorna a string
        return $data;
    }
}
if (!function_exists('cnpjToInteger')) {

    function cnpjToInteger($cnpj = '')
    {
        return preg_replace("/[^0-9]/", "", $cnpj);

    }

}

if (!function_exists('tipoEmpresa')) {

    function tipoEmpresa($idRamoAtividade = 0, $idTipoRamoAtividade = 0, $idGeracaoDiaria = 0, $tipoEstabelecimento = 0)
    {
        if ($idRamoAtividade == config('enums.ramo_atividade.tratamento_residuos')) {
            switch($idTipoRamoAtividade) {
                case config('enums.tipo_atividade.transportador'):
                    $tipoEmpresa = 'TR';
                    break;
                case config('enums.tipo_atividade.gerenciador_residuo'):
                    $tipoEmpresa = 'GR';
                    break;
                case config('enums.tipo_atividade.cooperativa_residuo'):
                    $tipoEmpresa = 'CR';
                    break;
                case config('enums.tipo_atividade.destino_final'):
                    $tipoEmpresa = 'DR';
                    break;
            }
        } else {
            if ($idGeracaoDiaria == 1) {
                $tipoEmpresa = 'PG';
            } else {
                if (($tipoEstabelecimento == config('enums.estabelecimento_tipo.predio'))
                    || $tipoEstabelecimento == config('enums.estabelecimento_tipo.shopping')) {
                    $tipoEmpresa = 'GO';
                } elseif ($idTipoRamoAtividade == config('enums.tipo_atividade.predio_comercial_industria') || $idTipoRamoAtividade == config('enums.tipo_atividade.predio_misto_industria') || $idTipoRamoAtividade == config('enums.tipo_atividade.shopping_industria') || $idTipoRamoAtividade == config('enums.tipo_atividade.predio_comercial_comercio') || $idTipoRamoAtividade == config('enums.tipo_atividade.predio_misto_comercio') || $idTipoRamoAtividade == config('enums.tipo_atividade.shopping_comercio')) {
                    $tipoEmpresa = 'GC';
                } else {
                    $tipoEmpresa = 'GG';
                }
            }
        }

        return $tipoEmpresa;
    }
}
if (!function_exists('insereTipoEmpresa')) {

    function insereTipoEmpresa($ramoAtividade = 0, $tipoRamoAtividade = 0, $empresaId = 0, $geracaoDiariaId = 0)
    {

        if ($ramoAtividade != 0 && $tipoRamoAtividade != 0) {
            $tipoEmpresa = new EmpresasXEmpresasTipo;

            $siglaEmpresa = tipoEmpresa($ramoAtividade, $tipoRamoAtividade, $geracaoDiariaId);

            switch ($siglaEmpresa) {
                case 'GG':
                    $tipoEmpresa->empresa_tipo_id = \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.grande_gerador');
                    break;
                case 'PG':
                    $tipoEmpresa->empresa_tipo_id = \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.pequeno_gerador');
                    break;
                case 'TR':
                    $tipoEmpresa->empresa_tipo_id = \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.transportador');
                    break;
                case 'DR':
                    $tipoEmpresa->empresa_tipo_id = \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.destino_final_reciclado');;
                    break;
                case 'CR':
                    $tipoEmpresa->empresa_tipo_id = \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.cooperativa_residuos');
                    break;
            }

            $tipoEmpresa->status_id = \Illuminate\Support\Facades\Config::get('enums.status.ativo');
            $tipoEmpresa->empresa_id = $empresaId;
            $tipoEmpresa->created_at = now();
            $tipoEmpresa->save();

        } else {
            return false;
        }

    }
}
if (!function_exists('geraCodigoProtocolo')) {
    function geraCodigoProtocolo($empresaId = 0, $tipoProtocolo = '')
    {
        $data = date('Ymd');

        switch ($tipoProtocolo) {
            case "cadastro":
                $numeroProtocolo = "C" . $data . $empresaId;
                break;
            default:
                $numeroProtocolo = "INDEF" . $data . $empresaId;
                break;
        }

        return $numeroProtocolo;
    }
}
if (!function_exists('datesPeriodForIndicator')) {
    /**
     * Calcula as datas de Início e Final para cada período dos indicadores: Dia, Semanal, Mensal, Trimestral, Semestral, Anual
     *
     * @return stdClass
     */
    function datesPeriodForIndicator()
    {
        //Inicializando Carbon;
        $carbon = new Carbon();

        //Inicializando objeto que receberá todas as datas
        $datesIndicator = new stdClass();

        //Datas de Hoje e amanhã
        $datesIndicator->now = $carbon->now()->toDateString();
        $datesIndicator->tomorrow = $carbon->now()->addDay()->toDateString();

        //Datas de segunda-feira e domingo da semana atual
        $datesIndicator->monday = $carbon->startOfWeek()->toDateString();
        $datesIndicator->sunday = $carbon->endOfWeek()->toDateString();

        //Datas de primeiro e último dia do mês atual
        $datesIndicator->firstDayOfMonth = $carbon->startOfMonth()->toDateString();
        $datesIndicator->endDayOfMonth = $carbon->endOfMonth()->toDateString();

        //Datas de primeiro e último dia do trimestre
        $datesIndicator->firstDayOfQuarter = $carbon->startOfQuarter()->toDateString();
        $datesIndicator->endDayOfQuarter = $carbon->endOfQuarter()->toDateString();

        // Datas de primeiro e último dia do ano
        $datesIndicator->firstDayOfYear = $carbon->startOfYear()->toDateString();
        $datesIndicator->endDayOfYear = $carbon->endOfYear()->toDateString();

        //Datas de primeiro e último dia do semestre
        $carbonSemester = new Carbon();
        $datesIndicator->firstDayOfSemester = $carbonSemester->startOfQuarter()->subQuarters(1)->toDateString();
        $datesIndicator->endDayOfSemester = $datesIndicator->endDayOfMonth;

        return $datesIndicator;
    }
}