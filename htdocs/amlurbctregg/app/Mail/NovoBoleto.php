<?php

namespace Amlurb\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NovoBoleto extends Mailable
{
    use Queueable, SerializesModels;

    private $dadosEmail = [];

    public $tries = 5;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dados)
    {
        $this->dadosEmail = $dados;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dadosEmail = $this->dadosEmail;

        if (isset($this->dadosEmail['boleto'])) {
            $this->attach($this->dadosEmail['boleto']);
        }

        return $this->view($dadosEmail['view'], compact('dadosEmail'))
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject($this->dadosEmail['subject']);
    }
}
