<?php

namespace Amlurb\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VinculosFaltantes extends Mailable
{
    use Queueable, SerializesModels;
    private $empresa = [];

    public $tries = 5;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $empresa = $this->empresa;

        return $this->view('emails.vinculos.vinculos-faltantes', compact('empresa'))
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('CTRE – Vinculos pendentes');
    }
}
