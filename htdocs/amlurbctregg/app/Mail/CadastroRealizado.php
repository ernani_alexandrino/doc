<?php

namespace Amlurb\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class CadastroRealizado
 *
 * @property array $dadosEmail
 * @package Amlurb\Mail
 */
class CadastroRealizado extends Mailable
{
    use Queueable, SerializesModels;

    private $dadosEmail = [];

    public $tries = 5;

    /**
     * Create a new message instance.
     *
     * @param $dados
     */
    public function __construct($dados)
    {
        $this->dadosEmail = $dados;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dadosEmail = $this->dadosEmail;

        return $this->view($this->dadosEmail['view'], compact('dadosEmail'))
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->subject($this->dadosEmail['subject']);
    }
}
