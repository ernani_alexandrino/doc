<?php

namespace Amlurb\Http\Middleware;

use Amlurb\Services\UrlConstructor;
use Closure;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthGerador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $alloweds = array(config('enums.status.ativo'), config('enums.status.vinculos_pendentes'));
        $user = Auth::user();
        $userTipo = EmpresasXEmpresasTipo::where('empresa_id', $user->empresa_id)->where('status_id', 1)->first();

        if ($userTipo->empresa_tipo_id == config('enums.empresas_tipo.grande_gerador')) {

            // prossegue se for url de cadastro
            if ($request->is(UrlConstructor::meuCadastro($userTipo->empresa_tipo_id))) {
                return $next($request);
            }
            if (!in_array($user->empresa->status_id, $alloweds)) {
                return Redirect::route('mc_gg');
            }
            return $next($request);
        }

        Auth::logout();
        Session::flush();
        return Redirect::to('/login');
    }
}
