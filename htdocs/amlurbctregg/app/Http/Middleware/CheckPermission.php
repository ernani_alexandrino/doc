<?php

namespace Amlurb\Http\Middleware;

use Closure;

class CheckPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        $user = $request->user();

        if ($user->is_master == 1) {
            return $next($request);
        } elseif ($user->hasPermission($permission)) {
            return $next($request);
        }

        return redirect()->back()->withErrors(['Você não tem permissão.']);
    }
}