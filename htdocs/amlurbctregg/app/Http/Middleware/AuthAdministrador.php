<?php

namespace Amlurb\Http\Middleware;

use Closure;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthAdministrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $userTipo = EmpresasXEmpresasTipo::where('empresa_id', $user->empresa_id)->where('status_id', 1)->first();

        // Se for diferente de GG
        if($userTipo->empresa_tipo_id != 7){
            Auth::logout();
            Session::flush();
            return Redirect::to('/login');
        }

        return $next($request);
    }
}
