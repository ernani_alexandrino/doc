<?php

namespace Amlurb\Http\Middleware;

use Closure;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthPequenoGerador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $userTipo = EmpresasXEmpresasTipo::where('empresa_id', $user->empresa_id)->where('status_id', 1)->first();

        if ($userTipo->empresa_tipo_id == config('enums.empresas_tipo.pequeno_gerador')) {

            return $next($request);

        } else {
            Auth::logout();
            Session::flush();
            return Redirect::to('/login');
        }
    }
}
