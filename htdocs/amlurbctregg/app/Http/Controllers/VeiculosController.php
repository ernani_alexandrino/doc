<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Models\Justificativa;
use Amlurb\Models\JustificativaExclusaoVeiculo;
use Amlurb\Services\GeradorCodigos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Redirect;

class VeiculosController extends Controller
{

    public function veiculoSalvar(Request $request)
    {

      
        $validator = validator($request->all(), [
            'placa' => 'required|min:8|max:8|regex:/^[a-zA-Z]{3}-\d{4}$/',
            'ano_veiculo' => 'required|digits:4|date_format:Y|after:1964|before:' . ((int)date('Y') + 1),
            'renavam' => 'required|max:11',
            'tipo' => 'required|min:3|max:30',
            'vencimento_ipva' => 'required|date_format:d/m/Y',
            'capacidade' => 'required|numeric',
            'tara' => 'required|max:15',
            'marca' => 'required|min:3|max:30',
            'numero_inmetro' => 'required',
            'anexo_documento_inmetro_enviado' => 'required',
            'anexo_documento_veiculo_enviado' => 'required'
        ]);

        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[] = [$k => $validator->errors()->first($k)];
            }

            return response()->json(['error' => $form_error]);
        }

        $vencimento_ipva = data2date($request->input('vencimento_ipva'));
        $codigo_amlurb = GeradorCodigos::geraCodigoAmlurbVeiculos(Auth::user()->empresa_id);

        // Vinculando Emrpesa ao veículo no BD
        // salva veiculo como inativo; necessario ativacao via pagamento de boleto

        $empresaVeiculo = EmpresasVeiculo::firstOrNew(
            [
                'placa' => str_replace('-', '', $request->input('placa')),
                'renavam' => $request->input('renavam'),
                'empresa_id' => Auth::user()->empresa_id,
                'status_id' => \config('enums.status.aguardando_revisao'),
            ]
        );
        $empresaVeiculo->codigo_amlurb = $codigo_amlurb;
        $empresaVeiculo->ano_veiculo = $request->input('ano_veiculo');
        $empresaVeiculo->tipo = $request->input('tipo');
        $empresaVeiculo->capacidade = $request->input('capacidade');
        $empresaVeiculo->tara = $request->input('tara');
        $empresaVeiculo->marca = $request->input('marca');
        $empresaVeiculo->vencimento_ipva = $vencimento_ipva;
        $empresaVeiculo->numero_inmetro = $request->input('numero_inmetro');
        $empresaVeiculo->save();
       
        return response()->json([
            'success' => 'Veículo salvo'
        ]);
    }

    public function veiculoDeletar(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $justificativa = Justificativa::create([
                'justificativa' => $request->input('justificativa')
            ]);

            $veiculo = EmpresasVeiculo::find($id);
            $veiculo->status_id = \config('enums.status.excluido');
            $veiculo->save();
            $veiculo->delete(); // softdelete

            JustificativaExclusaoVeiculo::create([
                'justificativa_id' => $justificativa->id,
                'empresa_id' => Auth::user()->empresa_id,
                'veiculo_id' => $veiculo->id
            ]);

            DB::commit();

        } catch (\Exception $e) {
            return response()->json(['success' => 'Ocorreu um problema na exclusão deste veículo - ' . $e->getMessage()]);
        }

        return response()->json(['success' => 'Veículo de código ' . $veiculo->codigo_amlurb . ' foi excluído.']);
    }

    // metodo para deletar veiculos que ainda não foram registrados por boleto e qrcode
    public function veiculoForceDeletar($id)
    {
        EmpresasVeiculo::whereId($id)->first()->forceDelete();
        return response()->json(['success' => 'Veículo excluído']);
    }

    /**
     * @param string $placa
     * @return string
     */
    public function buscarTipoVeiculo($placa)
    {
        return EmpresasVeiculo::wherePlaca($placa)->first()->tipo;
    }

    public function uploadDocumentos(Request $request, $tipo)
    {
        $retornoAjax['status'] = false;
        $validator = validator($request->all(), [
            'placa' => 'required',
            'renavam' => 'required',
        ]);

        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[] = [$k => $validator->errors()->first($k)];
            }

            return response()->json(['error' => $form_error]);
        }
        $arquivo = $request->file($tipo);
        $path = 'veiculos/' . Auth::user()->empresa_id.'/'.strtolower(str_replace('-', '', $request->input('placa'))).'/';

        if ($request->hasFile($tipo) && $arquivo->isValid()) {
                $nameFile = $tipo . '.' . $arquivo->getClientOriginalExtension();
                $nameFile = str_replace('_editar', '', $nameFile);
                $tipo = str_replace('_editar', '', $tipo);
                
                $path = $path.'/'.$nameFile;
                Storage::disk('s3')->put($path, file_get_contents($arquivo));
                $empresaVeiculo = EmpresasVeiculo::firstOrNew(
                    [
                        'placa' => str_replace('-', '', $request->input('placa')),
                        'renavam' => $request->input('renavam'),
                        'empresa_id' => Auth::user()->empresa_id,
                        'status_id' => \config('enums.status.aguardando_revisao'),
                    ]
                );
                $empresaVeiculo->$tipo = $nameFile;
                $empresaVeiculo->save();
                $retornoAjax['status'] = true;
        }
        return response()->json($retornoAjax);
    }


    public function uploadDocumentosEditar(Request $request, $tipo)
    {
        $retornoAjax['status'] = false;
        $validator = validator($request->all(), [
            'placa' => 'required',
            'renavam' => 'required',
        ]);

        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[] = [$k => $validator->errors()->first($k)];
            }

            return response()->json(['error' => $form_error]);
        }
        $arquivo = $request->file($tipo);
        $path = 'veiculos/' . Auth::user()->empresa_id.'/'.strtolower(str_replace('-', '', $request->input('placa'))).'/';

        if ($request->hasFile($tipo) && $arquivo->isValid()) {
                $nameFile = $tipo . '.' . $arquivo->getClientOriginalExtension();
                $nameFile = str_replace('_editar', '', $nameFile);
                $tipo = str_replace('_editar', '', $tipo);
                
                $path = $path.'/'.$nameFile;
                Storage::disk('s3')->put($path, file_get_contents($arquivo));
                $empresaVeiculo = EmpresasVeiculo::find($request->input('id_veiculo')); 
                $empresaVeiculo->placa = str_replace('-', '', $request->input('placa'));
                $empresaVeiculo->renavam = $request->input('renavam');
                $empresaVeiculo->empresa_id = Auth::user()->empresa_id;
                $empresaVeiculo->$tipo = $nameFile;
                $empresaVeiculo->save();
                $retornoAjax['status'] = true;
        }
        return response()->json($retornoAjax);
    }

    public function veiculoEditarSalvar(Request $request)
    {

        try {
            $validator = validator($request->all(), [
                'placa' => 'required|min:7|max:7|regex:/^[a-zA-Z]{3}\d{4}$/',
                'ano_veiculo' => 'required|digits:4|date_format:Y|after:1964|before:' . ((int)date('Y') + 1),
                'renavam' => 'required|max:11',
                'tipo' => 'required|min:3|max:30',
                'vencimento_ipva' => 'required|date_format:d/m/Y',
                'capacidade' => 'required|numeric',
                'tara' => 'required|max:15',
                'marca' => 'required|min:3|max:30',
                'numero_inmetro' => 'required'
            ]);
    
            if ($validator->fails()) {
                $form_error = [];
                foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                    return Redirect::back()->with([
                    'context' => 'danger',
                    'message' => $validator->errors()->first($k)
                    ]);
                }
            }
    
            $vencimento_ipva = data2date($request->input('vencimento_ipva'));
            // Vinculando Emrpesa ao veículo no BD
            // salva veiculo como inativo; necessario ativacao via pagamento de boleto
            $empresaVeiculo = EmpresasVeiculo::find($request->input('id_veiculo')); 
            
            $empresaVeiculo->placa = str_replace('-', '', $request->input('placa'));
            $empresaVeiculo->renavam = $request->input('renavam');
            $empresaVeiculo->empresa_id = Auth::user()->empresa_id;
            $empresaVeiculo->ano_veiculo = $request->input('ano_veiculo');
            $empresaVeiculo->tipo = $request->input('tipo');
            $empresaVeiculo->capacidade = $request->input('capacidade');
            $empresaVeiculo->tara = $request->input('tara');
            $empresaVeiculo->marca = $request->input('marca');
            $empresaVeiculo->vencimento_ipva = $vencimento_ipva;
            $empresaVeiculo->numero_inmetro = $request->input('numero_inmetro');
            $empresaVeiculo->status_id = \config('enums.status.revisado');
            $empresaVeiculo->save();
            
            return Redirect::back()->with([
                'context' => 'success',
                'message' => 'Veículo salvo com sucesso!'
            ]);
        } catch (\Exception $e) {
            Log::error($e);
            return Redirect::back()->with([
                'context' => 'error',
                'message' => 'Ocorreu um erro ao salvar!'
            ]);
        }
    }
}
