<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\Empresa;
use Illuminate\Http\Request;
use Amlurb\Models\Cidade;
use Amlurb\Models\User;
use Amlurb\Models\TipoRamoAtividade;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{

    /**
     * Realiza a busca do CNPJ na base local, se não encontrar, utiliza a API da receitaws.com.br
     * @param int $cnpj
     * @return \Illuminate\Http\JsonResponse
     */
    public function cnpjEmpresa(Request $request, $cnpj = 0)
    {
        
        if (!empty($cnpj) && $cnpj != 0) {
            $fixaCnpj = array('cnpj_cad' => $cnpj);
            session($fixaCnpj); # linha inserida By Mr.Goose em 08 abr 19 às 2227h - afim de utilizar no upload de arquivo no Cadastro de empresa. - alterado by Mr.Goose em 24 abr 19 em Prod
            $dadosEmpresa['inf'] = Empresa::where('cnpj', $cnpj)
                                    ->with(['empresa_endereco','empresa_informacao_complementar'])->first();
            
            // verifica se possui dados no sistema e se possui algum usuario vinculado
           
            if (!empty($dadosEmpresa['inf'])) {
                if ($dadosEmpresa['inf']->cnpj == $cnpj) {
                    $user = User::where('empresa_id', $dadosEmpresa['inf']->id)->first();

                    if (Auth::check() || empty($user)) {
                        $dadosEmpresa['endereco'] = isset($dadosEmpresa['inf']->empresa_endereco) ? $dadosEmpresa['inf']->empresa_endereco : [];
                        $dadosEmpresa['ramo'] = isset($dadosEmpresa['inf']->empresa_informacao_complementar) ? $dadosEmpresa['inf']->empresa_informacao_complementar : [];
                        $dadosEmpresa['cidade'] = isset($dadosEmpresa['inf']->empresa_endereco->cidade_id) ? Cidade::where('id', $dadosEmpresa['inf']->empresa_endereco->cidade_id)->first() : [];
                        $dadosEmpresa['estado'] = isset($dadosEmpresa['inf']->empresa_endereco->estado_id)? : [];
                        $dadosEmpresa['tipo'] = isset($dadosEmpresa['ramo']->tipo_ramo_atividade_id)? TipoRamoAtividade::where('id', $dadosEmpresa['ramo']->tipo_ramo_atividade_id)->first() : []; ;
                        return response()->json(['obj' => '', 'dadosEmpresa' => $dadosEmpresa]);
                    }
                    if (!empty($user)) {
                        $obj['mensagem'] = 'Esta empresa ja consta em nossa base de dados, <a href="https://www.ctre.com.br/login">clique aqui para se logar.</a>';
                        return response()->json(['obj' => $obj]);
                    }
                } else {
                    // $request->session()->put('cnpj_cad', $cnpj); # linha inserida By Mr.Goose em 08 abr 19 às 2227h - afim de utilizar no upload de arquivo no Cadastro de empresa.
                    if (valida_cnpj($cnpj)) {
                        $service_url = 'https://receitaws.com.br/v1/cnpj/' . $cnpj;
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
                        curl_setopt($ch, CURLOPT_URL, $service_url);
                        curl_setopt($ch, CURLOPT_FAILONERROR, false);
                        curl_setopt($ch, CURLOPT_HTTP200ALIASES, (array)400);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    
                        $result = curl_exec($ch);
    
                        if (curl_error($ch)) {
                            $obj = '';
                        } else {
                            $obj = json_decode($result);
                            if ($obj) {
                                if ($obj->status == 'ERROR') {
                                    $obj = '';
                                }
                            }
                        }
    
                        curl_close($ch);
                    } else {
                        $obj = '';
                    }
                    return response()->json(['obj' => $obj, 'dadosEmpresa' => $dadosEmpresa]);
                }
                // passa dados de empresa para as paginas de vincular empresas
            } else {
                    // $request->session()->put('cnpj_cad', $cnpj); # linha inserida By Mr.Goose em 08 abr 19 às 2227h - afim de utilizar no upload de arquivo no Cadastro de empresa.
                if (valida_cnpj($cnpj)) {
                        $service_url = 'https://receitaws.com.br/v1/cnpj/' . $cnpj;
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
                        curl_setopt($ch, CURLOPT_URL, $service_url);
                        curl_setopt($ch, CURLOPT_FAILONERROR, false);
                        curl_setopt($ch, CURLOPT_HTTP200ALIASES, (array)400);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    
                        $result = curl_exec($ch);
    
                    if (curl_error($ch)) {
                            $obj = '';
                    } else {
                            $obj = json_decode($result);
                        if ($obj) {
                            if ($obj->status == 'ERROR') {
                                    $obj = '';
                            }
                        }
                    }
                     curl_close($ch);
                } else {
                        $obj = '';
                }
            }
        }

        return response()->json(['obj' => $obj]);
    }
}
