<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Http\Controllers\Gerenciador\PainelController;

//Models
use Amlurb\Models\Cidade;
use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Models\EmpresaDocumentos;
use Amlurb\Models\Estado;
use Amlurb\Models\RamoAtividade;
use Amlurb\Models\Residuo;
use Amlurb\Models\Equipamento;
use Amlurb\Models\FrequenciaColeta;
use Amlurb\Models\TipoRamoAtividade;
use Amlurb\Models\EmpresaResponsavel;
use Amlurb\Services\DataTables\CtresRenderer;
use Amlurb\Services\DataTables\EquipamentosVeiculosRenderer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Amlurb\Models\JustificativaInconsistenciaVeiculos;
use Amlurb\Models\Justificativa;

class TransportadorController extends PainelController
{
    public $titulo = [
        'principal' => ' Transportador',
        'subtitulo' => 'Painel de Controle'
    ];

    public function index()
    {
        $titulo = (object)$this->titulo;

        // CTRE
        $geradores = $this->empresaRepository->getGeradoresVinculados();
        $destinos = $this->empresaRepository->getDestinosFinaisVinculados();
        $veiculos = $this->user->empresa->empresas_veiculos
            ->where('status_id', \config('enums.status.ativo'))->pluck('placa', 'placa');

        return view('painel.transportador.home', compact(
            'titulo','geradores', 'destinos', 'veiculos'
        ));
    }

    // veiculos e equipamentos
    public function meusEquipamentos()
    {
        $this->titulo['subtitulo'] = 'Meus Equipamentos/Veículos';
        $titulo = (object)$this->titulo;

        $equipamentos = Equipamento::with('tipo_equipamento')->get();

        return view('painel.transportador.equipamentos-veiculos', compact(
            'titulo', 'equipamentos'));
    }

    // geradores e destinos finais
    public function clientesFornecedores()
    {
        $this->titulo['subtitulo'] = 'Clientes/Fornecedores';
        $titulo = (object)$this->titulo;

        $frequenciaColeta = FrequenciaColeta::all();
        // so traz equipamentos que ainda nao foram vinculados a nenhum gerador
        $equipamentos = $this->empresasXequipamentoRepository->getEquipamentosNaoAlocadosByIdEmpresa($this->user->empresa_id);
        $residuos = Residuo::all();

        return view('painel.transportador.clientes-fornecedores', compact(
            'titulo', 'frequenciaColeta', 'equipamentos', 'residuos'));
    }

    // boletos de equipamentos e veiculos
    public function pagamentos()
    {
        $this->titulo['subtitulo'] = 'Pagamentos';
        $titulo = (object)$this->titulo;

        return view('painel.transportador.pagamentos', compact('titulo'));
    }

    public function listCtreEmitidos()
    {
        return CtresRenderer::renderTransportadorEmitidos($this->ctreRepository->getCtreEmitidos($this->user->empresa_id));
    }

    public function listCtreRecebidos()
    {
        return CtresRenderer::renderTransportadorRecebidos($this->ctreRepository->getCtreRecebidos(
            $this->user->empresa_id, \config('enums.empresas_tipo.transportador')));
    }

    public function listVeiculos()
    {
        $veiculos = EmpresasVeiculo::where('empresa_id', $this->user->empresa_id)
            ->with('status')->get();
        if (count($veiculos)) {
            foreach ($veiculos as $veiculo) {
                $veiculo->ipva = date2data($veiculo->vencimento_ipva);
            }
        }

        return EquipamentosVeiculosRenderer::renderVeiculos($veiculos);
    }

    public function listVeiculosAtivar()
    {
        $veiculos = EmpresasVeiculo::where('empresa_id', $this->user->empresa_id)
            ->whereIn('status_id', [\config('enums.status.inativo'), \config('enums.status.vencido')])->get();
        $html = View::make('painel.transportador.equipamentos-veiculos.modais.ativar-renovar-veiculos', compact(
            'veiculos'))->render();

        return response()->json(['html' => $html]);
    }

    public function meuCadastro()
    {
        $this->titulo['subtitulo'] = 'Meu Cadastro';
        $titulo = (object)$this->titulo;

        $empresa = Auth::user()->empresa;
        
        $documentos = array();
        $getDocumentos = EmpresaDocumentos::where('empresa_id', $empresa->id)->get();
        foreach ($getDocumentos as $value) {
            $documentos[$value->documentos->nome] = $value->caminho_arquivo;

            if (!empty($value->data_emissao))
                $documentos[$value->documentos->nome.'_emissao'] = $value->data_emissao;
            if (!empty($value->data_vencimento))
                $documentos[$value->documentos->nome.'_vencimento'] = $value->data_vencimento;

            if (strpos($value->documentos->nome, 'licenca_')){
                $documentos['licencadefault'] = $value->documentos->id;
                $documentos['licenca'] = $value->caminho_arquivo;
                $documentos['licenca_emissao'] = $value->data_emissao;
                $documentos['licenca_vencimento'] = $value->data_vencimento;
            }
        }
       
        $empreendimentoSocioPrincial  = EmpresaResponsavel::where('empresa_id', $empresa->id)->where('responsavel', 'socio_principal')->get()->last();
        $empreendimentoSocios  = EmpresaResponsavel::where('empresa_id', $empresa->id)->where('responsavel', 'socio')->get();
        $socios = [];
        $i =2;
        foreach ($empreendimentoSocios as $socio) {
            $socios['id_socio_'.$i] = $socio->id;
            $socios['nome_socio_'.$i] = $socio->nome;
            $socios['numero_rg_socio_'.$i] = $socio->rg;
            $socios['numero_cpf_socio_'.$i] = $socio->cpf;

            $i++;
        }
        $ramoAtividade = RamoAtividade::all()->pluck('nome', 'id');
        $tipoRamoAtividade = TipoRamoAtividade::all()->pluck('nome', 'id');
        $estados = Estado::all()->pluck('nome', 'id');
        $cidades = Cidade::all()->pluck('nome', 'id');

        return view('painel.transportador.meu-cadastro',
            compact('titulo', 'empresa', 'ramoAtividade', 'tipoRamoAtividade', 'estados', 'cidades', 'documentos', 'empreendimentoSocioPrincial', 'socios')
        );
    }

    public function faq()
    {
        $this->titulo['subtitulo'] = 'FAQ';
        $titulo = (object)$this->titulo;

        return view('painel.transportador.faq', compact('titulo'));
    }

    public function getVeiculo(Request $request, $idVeiculo)
    {
        if ($request->ajax()) {
            $veiculo = EmpresasVeiculo::find($idVeiculo);

            $justificativa = JustificativaInconsistenciaVeiculos::where('empresas_veiculo_id', $idVeiculo)
            ->with('justificativaLast')->get()->last();
           
            $anexo_documento_veiculo = null;
            $anexo_documento_inmetro = null;
            $anexo_comodato_veiculo = null;
            if (!empty($veiculo->anexo_documento_veiculo)) {
                $anexo_documento_veiculo = route('download_veiculo', ['arquivo' => $veiculo->anexo_documento_veiculo, 'empresa' => $veiculo->empresa_id]);
            }
            if (!empty($veiculo->anexo_documento_inmetro)) {
                $anexo_documento_inmetro = route('download_veiculo', ['arquivo' => $veiculo->anexo_documento_inmetro, 'empresa' => $veiculo->empresa_id]);
            }
            if (!empty($veiculo->anexo_comodato_veiculo)) {
                $anexo_comodato_veiculo = route('download_veiculo', ['arquivo' => $veiculo->anexo_comodato_veiculo, 'empresa' => $veiculo->empresa_id]);
            }
            
            $result  = null ;
            
            if ($veiculo) {
                $result['id'] = $veiculo->id;
                $result['empresa_id'] = $veiculo->empresa_id;
                $result['status_id'] = $veiculo->status_id;
                $result['codigo_amlurb'] = $veiculo->codigo_amlurb;
                $result['placa'] = $veiculo->placa;
                $result['ano_veiculo'] = $veiculo->ano_veiculo;
                $result['renavam'] = $veiculo->renavam;
                $result['tipo'] = $veiculo->tipo;
                $result['capacidade'] = $veiculo->capacidade;
                $result['tara'] = $veiculo->tara;
                $result['marca'] = $veiculo->marca;
                $result['numero_inmetro'] = $veiculo->numero_inmetro;
                $result['vencimento_ipva'] = Carbon::parse($veiculo->vencimento_ipva)->format('d/m/Y');
                $result['anexo_documento_veiculo'] = $anexo_documento_veiculo;
                $result['anexo_documento_inmetro'] = $anexo_documento_inmetro;
                $result['anexo_comodato_veiculo'] = $anexo_comodato_veiculo;
                $result['justificativa'] = $justificativa->justificativaLast->justificativa;
            }
            return response()->json(['dados_veiculo' => $result]);
        }
    }

}
