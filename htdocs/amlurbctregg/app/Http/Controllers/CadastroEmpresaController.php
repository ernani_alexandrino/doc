<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Http\Requests\CadastroCondominioResponsavelRequest;
use Amlurb\Http\Requests\CadastroEmpresaRequest;
use Amlurb\Http\Requests\CadastroEmpresaUploadRequest;
use Amlurb\Http\Requests\CadastroInformacoesComplementaresRequest;
use Amlurb\Http\Requests\CadastroResponsavelRequest;
use Amlurb\Http\Requests\CadastroResponsavelEmpreendimentoRequest;
use Amlurb\Http\Requests\CadastroResponsavelEmpreendimentoCooRequest;
use Amlurb\Mail\CadastroRealizado;
use Amlurb\Models\Cidade;
use Amlurb\Models\ColaboradoresNumero;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresaEndereco;
use Amlurb\Models\EmpresaInformacaoComplementars;
use Amlurb\Models\EmpresaDocumentos;
use Amlurb\Models\EmpresaResponsavel;
use Amlurb\Models\EmpresasTipoXTermosDeUso;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\EnergiaConsumo;
use Amlurb\Models\EstabelecimentoTipo;
use Amlurb\Models\Estado;
use Amlurb\Models\FrequenciaColeta;
use Amlurb\Models\FrequenciaGeracao;
use Amlurb\Models\Protocolo;
use Amlurb\Models\Qrcode;
use Amlurb\Models\QrcodeEmpresas;
use Amlurb\Models\RamoAtividade;
use Amlurb\Models\TipoRamoAtividade;
use Amlurb\Models\User;
use Amlurb\Services\GeradorCodigos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use Session;

class CadastroEmpresaController extends Controller
{
    public $titulo = [
        'principal' => 'Geradores',
        'subtitulo' => 'Cadastro de Filial'
    ];

    private static $new_password = 'amlurb#@';

    /**
     * Carrega o formulário para o cadastro da empresa.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $cnpj = '';
        if(!empty($user)){
            $userPG =   Empresa::where('id', $user->empresa_id)->first();
            $cnpj = $userPG->cnpj;
            $empresa_id = $user->empresa_id;
        }    
        $estados = Estado::all()->pluck('sigla', 'id');
        $ramoAtividades = RamoAtividade::all()->pluck('nome', 'id');
        $ramoAtividades->prepend('Selecione uma opção', 0);
        $tipoAtividades[0] = 'Selecione uma opção';
        $tipoRamoAtividade = TipoRamoAtividade::all()->pluck('nome', 'id');
        $cidades = Cidade::all()->pluck('nome', 'id');
        $colaboradorNumero = ColaboradoresNumero::all('id', 'nome');
        $energiaConsumo = EnergiaConsumo::all('id', 'nome');
        $estabelecimentoTipo = EstabelecimentoTipo::all('id', 'nome');
        $frequenciaGeracao = FrequenciaGeracao::all('nome', 'id');
        $frequenciaColeta = FrequenciaColeta::all('id', 'nome');

        return view('cadastro.index', compact(
            'estados',
            'cidades',
            'ramoAtividades',
            'tipoAtividades',
            'tipoRamoAtividade',
            'colaboradorNumero',
            'energiaConsumo',
            'estabelecimentoTipo',
            'frequenciaGeracao',
            'frequenciaColeta',
            'cnpj',
            'empresa_id'
        ));
    }    

    /**
     * Valida os dados principais da empresa
     *
     * @param CadastroEmpresaRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function validaEmpresa(CadastroEmpresaRequest $request)
    {
        try {
            $retornoAjax['status'] = true;

            $dados['empresa']['cnpj'] = cnpjToInteger($request->cnpj);
            $dados['empresa']['razao_social'] = $request->razao_social;
            $dados['empresa']['nome_comercial'] = $request->nome_fantasia;
            $dados['empresa']['ie'] = $request->inscricao_estadual;
            $dados['empresa']['im'] = $request->licenca_municipal;
            $dados['empresa']['cep'] = $request->cep;
            $dados['empresa']['endereco'] = $request->endereco;
            $dados['empresa']['numero'] = $request->numero;
            $dados['empresa']['bairro'] = $request->bairro;
            $dados['empresa']['estado'] = $request->estado;
            $dados['empresa']['cidade'] = $request->cidade;
            $dados['empresa']['complemento'] = $request->complemento;
            $dados['empresa']['ponto_referencia'] = $request->ponto_referencia;
            $dados['empresa']['telefone'] = $request->telefone;
            $dados['empresa']['ramo_atividade'] = $request->ramo_atividade;
            $dados['empresa']['tipo_atividade'] = $request->tipo_atividade;
           

            # abaixo, inserido by Mr.Goose em 09 abr 19 afim de salvar o Número do IPTU Digitado pelo Usuário na primeira tela do Cadastro
            $sessionDocumentos = session('documentos');
            if (!empty($sessionDocumentos)) {
                foreach ($sessionDocumentos as $campo => $valor) {
                    $dados['documentos'][$campo] = $valor;
                }
            }
            $dados['documentos']['empresa_num_iptu'] = $request->empresa_num_iptu; 
            # acima, inserido by Mr.Goose em 09 abr 19 afim de salvar o Número do IPTU Digitado pelo Usuário na primeira tela do Cadastro


            if (session()->has('empresa')) {
                session()->forget('empresa');
            }
            if (session()->has('cnpj')) {
                session()->forget('cnpj');
            }
            $dados['cnpj'] = cnpjToInteger($request->cnpj);
            session($dados);


            $retornoAjax['idDestino'] = '#tela-2';
            $user = Auth::user();
            //$retornoAjax['idDestino'] = '#step-2';

            $retornoAjax['view'] = view('cadastro.responsavel')->render();

            return response()->json($retornoAjax);
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Validar a Aba Empresa");
        }
    }

    /**
     * Valida os dados do responsável pela empresa
     * Estes dados são usados para o login da empresa
     *
     * @param CadastroResponsavelRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function validaResponsavel(CadastroResponsavelRequest $request)
    {
        try {
            $retornoAjax['status'] = true;
            $user = Auth::user();

            $dados['responsavel']['responsavel'] = $request->responsavel;        
            $dados['responsavel']['email'] = $request->email;        
            $dados['responsavel']['cargo'] = $request->cargo;
            $dados['responsavel']['celular_responsavel'] = $request->celular_responsavel;
            $dados['responsavel']['telefone_responsavel'] = $request->telefone_responsavel;
            $dados['responsavel']['ramal'] = $request->ramal;

            if (session()->has('responsavel')) {
                session()->forget('responsavel');
            }

            session($dados);

            $dados['informacaoComplementar']['frequencia_geracao_id'] = session('informacaoComplementar') ? session('informacaoComplementar')['frequencia_geracao_id'] : 0;

            switch(session('empresa')['ramo_atividade']) {
                case config('enums.ramo_atividade.condominio_misto'):
                    $retornoAjax['idDestino'] = '#tela-3cm';
                    break;
                case config('enums.ramo_atividade.tratamento_residuos'):
                case config('enums.ramo_atividade.destino_final'):
                    $retornoAjax['idDestino'] = '#tela-3tr';
                    break;
                case config('enums.ramo_atividade.cooperativa'):

                    $retornoAjax['idDestino'] = '#tela-3co';
                    break;
                default: // FLUXO GERADOR GG / PG -- OP - SS
                    $retornoAjax['idDestino'] = '#tela-4g';
            }

            $retornoAjax['idAnterior'] = '#tela-2';

            return response()->json($retornoAjax);
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Validar a Aba Responsável");
        }
    }

    /**
     * Valida as informações complementares.
     *
     * @param CadastroInformacoesComplementaresRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function validaInformacoesComplementares(CadastroInformacoesComplementaresRequest $request)
    {
        try {
            $retornoAjax['status'] = true;

            $dados['informacaoComplementar']['frequencia_geracao_id'] = $request->geracao_diaria;
            $dados['informacaoComplementar']['frequencia_coleta_id'] = $request->coleta_diaria;
            $dados['informacaoComplementar']['colaboradores_numero_id'] = $request->numero_colaboradores;
            $dados['informacaoComplementar']['energia_consumo_id'] = $request->consumo_mensal;
            $dados['informacaoComplementar']['estabelecimento_tipo_id'] = $request->estabelecimento_tipo;
            $dados['informacaoComplementar']['area_total'] = str_replace('.', '', str_replace(',', '', $request->area_total));
            $dados['informacaoComplementar']['area_construida'] = str_replace('.', '', str_replace(',', '', $request->area_construida));

            if (session()->has('informacaoComplementar')) {
                session()->forget('informacaoComplementar');
            }

            session($dados);

            $empresa['empresa'] = session('empresa');
            $responsavel['responsavel'] = session('responsavel');

            $empresa['empresa']['empresa_tipo'] = $this->tipoEmpresa($empresa['empresa']['ramo_atividade'],
                $dados['informacaoComplementar']['frequencia_geracao_id']);
            
            $retornoAjax['idTesteTipo'] = $empresa['empresa']['empresa_tipo'];

            $retornoAjax['idAnterior'] = '#tela-4g';
            // termos-de-uso
            $retornoAjax['idDestino'] = '#tela-5';

            $termo = EmpresasTipoXTermosDeUso::with('termos_de_uso')->where('empresa_tipo_id',
                $empresa['empresa']['empresa_tipo'])->first();

            $tipo = $this->nomeTipoEmpresa($empresa['empresa']['empresa_tipo']);
          
            if(!empty($termo)){
                
                $termo = $termo->termos_de_uso->termo_uso;
                $retornoAjax['termoDeUso'] = $termo;
                $retornoAjax['termoDeUso'] =  str_replace("[Linha de Código puxando a razão social cadastrada]", $empresa['empresa']['razao_social'], $retornoAjax['termoDeUso']);
                $retornoAjax['termoDeUso'] =  str_replace("[Linha de código puxando a data do momento do cadastro]", date('d/m/Y'), $retornoAjax['termoDeUso']);
                $retornoAjax['termoDeUso'] =  str_replace("[Linha d código para definir o módulo de cadastro : grande gerador, pequeno gerador, gerador misto, etc]", $tipo, $retornoAjax['termoDeUso']);
            }

            return response()->json($retornoAjax);

        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao validar a Aba Informações Complementares");
        }
    }

    /**
     * Valida os dados do condominio misto e pega o termo de uso correto para o tipo da empresa
     *
     * @param Request $request
     * @param         $nomeCampo
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */

    // condominio misto

    public function validaCondominioArquivos (Request $request, $nomeCampo = ''){
        try {
            //Persiste a sessão para não perder os dados na próxima requisição
            $request->session()->regenerate();

            $retornoAjax['status'] = false;

            $messages = [
                "*$nomeCampo.required" => 'O :input é obrigatório',
                "*$nomeCampo.max" => 'O tamanho do arquivo ultrapassa o limite.',
                "*$nomeCampo.mimes" => 'O tipo do arquivo não é válido.'
            ];


            $validator = Validator::make($request->all(), [
                "*$nomeCampo" => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf'
            ], $messages);


            if ($validator->fails()) {

                $retornoAjax['erro'] = $validator->errors();

                return response()->json($retornoAjax);
            }

            $auth_empresa = Auth::user(); // Busca os dados da Empresa que está Logada
            if (!empty($auth_empresa)) {
                $empresa = $auth_empresa->empresa->cnpj; // CNPJ da Empresa Logada
            } else{
                $empresa = session('cnpj_cad'); // Busca o CNPJ no Cadastro de Empresa
                if (empty($empresa)) {
                    $retornoAjax['erro'] = 'Falta CNPJ';
                    return response()->json($retornoAjax); // Erro! sai aqui por não ter CNPJ de jeito nenhum.
                }
            }

            $nomeDoArquivo = $this->gravaArquivoDeUpload($request, $empresa, $nomeCampo);

            $sessionCondominio = session('condominio');

            if ($sessionCondominio) {
                foreach ($sessionCondominio as $campo => $valor) {
                    $dadosTemporariosEmpresa['condominio'][$campo] = $valor;
                }
            }

            $dadosTemporariosEmpresa['condominio'][$nomeCampo] = $nomeDoArquivo;

            session($dadosTemporariosEmpresa);

            $retornoAjax['status'] = true;

            return response()->json($retornoAjax);

        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao validar arquivos do Condomínio");
        }
    }


    // condominio misto
    public function validaCondominioResponsavel(CadastroCondominioResponsavelRequest $request)
    {
        try {
            $retornoAjax['status'] = true;

            $sessionCondominio = session('condominio');
            $responsavel['responsavel'] = session('responsavel');

            if ($sessionCondominio) {
                foreach ($sessionCondominio as $campo => $valor) {
                    $dadosEmpresa['condominio'][$campo] = $valor;
                }
            }

            $dadosEmpresa['condominio']['sindico'] = $request->condominio_sindico;
            $dadosEmpresa['condominio']['sindico_rg']  = $request->condominio_sindico_rg;
            $dadosEmpresa['condominio']['sindico_cpf'] = $request->condominio_sindico_cpf;

            session($dadosEmpresa);
            $empresa['empresa'] = session('empresa');

            $termoDeUso = EmpresasTipoXTermosDeUso::with('termos_de_uso')->where('empresa_tipo_id',
                config('enums.empresas_tipo.condominio_misto'))->first();
            $tipo = $this->nomeTipoEmpresa(config('enums.empresas_tipo.condominio_misto'));

            if($termoDeUso){
                $termoDeUso = $termoDeUso->termos_de_uso->termos_de_uso;
                $retornoAjax['status'] = true;
                $retornoAjax['termoDeUso'] = $termoDeUso;
                $retornoAjax['termoDeUso'] =  str_replace("[Linha de Código puxando a razão social cadastrada]",  $empresa['empresa']['razao_social'], $retornoAjax['termoDeUso']);
                $retornoAjax['termoDeUso'] =  str_replace("[Linha de código puxando a data do momento do cadastro]", date('d/m/Y'), $retornoAjax['termoDeUso']);
                $retornoAjax['termoDeUso'] =  str_replace("[Linha d código para definir o módulo de cadastro : grande gerador, pequeno gerador, gerador misto, etc]", $tipo, $retornoAjax['termoDeUso']);
           
            } else {
                $retornoAjax['termoDeUso'] = 1;
            }

            //Termos de uso
            $retornoAjax['idAnterior'] = '#tela-3cm';
            $retornoAjax['idDestino'] = '#tela-4g';

            return response()->json($retornoAjax);

        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao validar Responsável do Condomínio");
        }
    }


    // cooperativa
    public function validaResponsavelEmpreendimentoCoo(CadastroResponsavelEmpreendimentoCooRequest $request)
    {
        try {
            $retornoAjax['status'] = true;

            $sessionResponsavel = session('responsavelEmpreendimento');

            if ($sessionResponsavel) {
                foreach ($sessionResponsavel as $campo => $valor) {
                    $dadosEmpresa['responsavelEmpreendimento'][$campo] = $valor;
                }
            }

            if(isset($request->presidente)){
                $dadosEmpresa['responsavelEmpreendimento']['presidente'] = 'presidente';
            }

            $dadosEmpresa['responsavelEmpreendimento']['nome_socio'] = $request->nome_socio;
            $dadosEmpresa['responsavelEmpreendimento']['numero_rg_socio']  = $request->numero_rg_socio;
            $dadosEmpresa['responsavelEmpreendimento']['numero_cpf_socio'] = $request->numero_cpf_socio;
            $dadosEmpresa['responsavelEmpreendimento']['nome_socio_2'] = 
            $dadosEmpresa['responsavelEmpreendimento']['numero_rg_socio_2'] = 
            $dadosEmpresa['responsavelEmpreendimento']['numero_cpf_socio_2'] = 
            $dadosEmpresa['responsavelEmpreendimento']['nome_socio_3'] = 
            $dadosEmpresa['responsavelEmpreendimento']['numero_rg_socio_3'] = 
            $dadosEmpresa['responsavelEmpreendimento']['numero_cpf_socio_3'] = '';

            session($dadosEmpresa);

            switch(session('empresa')['ramo_atividade']) {
                case config('enums.ramo_atividade.cooperativa'):
                    $retornoAjax['idAnterior'] = '#tela-3co';
                    break;
                default: // FLUXO GERADOR GG / PG -- OP - SS
                    $retornoAjax['idAnterior'] = '#tela-3tr';
            }

            $retornoAjax['idDestino'] = '#tela-4tr';

            return response()->json($retornoAjax);

        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Responsável pelo Empreendimento Cooperativa");
        }
    }


    // transportador / destino final
    public function validaResponsavelEmpreendimento(CadastroResponsavelEmpreendimentoRequest $request)
    {
        try {
            $retornoAjax['status'] = true;

            $sessionResponsavel = session('responsavelEmpreendimento');

            if ($sessionResponsavel) {
                foreach ($sessionResponsavel as $campo => $valor) {
                    $dadosEmpresa['responsavelEmpreendimento'][$campo] = $valor;
                }
            }

            if(isset($request->presidente)){
                $dadosEmpresa['responsavelEmpreendimento']['presidente'] = 'presidente';
            }

            $dadosEmpresa['responsavelEmpreendimento']['nome_socio'] = $request->nome_socio;
            $dadosEmpresa['responsavelEmpreendimento']['numero_rg_socio']  = $request->numero_rg_socio;
            $dadosEmpresa['responsavelEmpreendimento']['numero_cpf_socio'] = $request->numero_cpf_socio;
            $dadosEmpresa['responsavelEmpreendimento']['nome_socio_2'] = $request->nome_socio_2;
            $dadosEmpresa['responsavelEmpreendimento']['numero_rg_socio_2'] = $request->numero_rg_socio_2;
            $dadosEmpresa['responsavelEmpreendimento']['numero_cpf_socio_2'] = $request->numero_cpf_socio_2;
            $dadosEmpresa['responsavelEmpreendimento']['nome_socio_3'] = $request->nome_socio_3;
            $dadosEmpresa['responsavelEmpreendimento']['numero_rg_socio_3'] = $request->numero_rg_socio_3;
            $dadosEmpresa['responsavelEmpreendimento']['numero_cpf_socio_3'] = $request->numero_cpf_socio_3;

            session($dadosEmpresa);

            switch(session('empresa')['ramo_atividade']) {
                case config('enums.ramo_atividade.cooperativa'):
                    $retornoAjax['idAnterior'] = '#tela-3co';
                    break;
                default: // FLUXO GERADOR GG / PG -- OP - SS
                    $retornoAjax['idAnterior'] = '#tela-3tr';
            }

            $retornoAjax['idDestino'] = '#tela-4tr';

            return response()->json($retornoAjax);

        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Responsável pelo Empreendimento");
        }
    }


    /**
     * Valida os Arquivos da primeira tela de Cadastro
     *
     * @param Request $request
     * @param         $nomeCampo
     * @param         $empresa
     *+ by Mr.Goose em 08 abr 19
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function validaEmpresaArquivos(Request $request, $nomeCampo = '')
    {
        try {
            //Persiste a sessão para não perder os dados na próxima requisiçãonumero_cpf_presidente_file
            $request->session()->regenerate();

            $retornoAjax['status'] = false;
         
            $messages = [
                "*$nomeCampo.required" => 'O :input é obrigatório',
                "*$nomeCampo.max" => 'O tamanho do arquivo ultrapassa o limite.',
                "*$nomeCampo.mimes" => 'O tipo do arquivo não é válido.'
            ];
          
            if ($nomeCampo == 'condominio_ata' || $nomeCampo == 'condominio_contribuicao') {
                $validator = Validator::make($request->all(), [
                    "*$nomeCampo" => 'required|max:10240|mimes:jpeg,jpg,bmp,png,pdf'
                ], $messages);
            } else {
                $validator = Validator::make($request->all(), [
                    "*$nomeCampo" => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf'
                ], $messages);
            }

            if ($validator->fails()) {

                $retornoAjax['erro'] = $validator->errors();

                return response()->json($retornoAjax);
            }

            $auth_empresa = Auth::user(); // Busca os dados da Empresa que está Logada
            if (!empty($auth_empresa)) {
                $empresa = $auth_empresa->empresa->cnpj; // CNPJ da Empresa Logada
            } else {
                $empresa = session('cnpj_cad'); // Busca o CNPJ no Cadastro de Empresa
                if (empty($empresa)) {
                    $retornoAjax['erro'] = 'Falta CNPJ';
                    return response()->json($retornoAjax); // Erro! sai aqui por não ter CNPJ de jeito nenhum.
                }
            }

            if (strpos($nomeCampo, 'presidente')) {
                $nomeCampo = str_replace('presidente', 'socio', $nomeCampo);
                # linha acima inserida by Mr.Goose em 30 abr 19 afim de tratar o Salvamento do Doc de Presidente da Coo que não estava salvando.
            }
             
            if ($request->hasFile($nomeCampo.'_alterado')) {
                $nomeCampo = $nomeCampo.'_alterado';
            }
        
            $nomeDoArquivo = $this->gravaArquivoDeUpload($request, $empresa, $nomeCampo);
            $sessionDocumentos = session('documentos');

            if (!empty($sessionDocumentos)) {
                foreach ($sessionDocumentos as $campo => $valor) {
                    $dadosTemporariosEmpresa['documentos'][$campo] = $valor;
                }
            }

            $dadosTemporariosEmpresa['documentos'][$nomeCampo] = $nomeDoArquivo;
           
            session($dadosTemporariosEmpresa);

            $retornoAjax['status'] = true;

            return response()->json($retornoAjax);
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Validar o Arquivo da Empresa");
        }
    }

    /**
     * Valida os dados do transportador e pega o termo de uso correto para o tipo da empresa
     *
     * @param Request $request
     * @param         $nomeCampo
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function validaTransportadorArquivos(Request $request, $nomeCampo = '')
    {
        try {
            //Persiste a sessão para não perder os dados na próxima requisição
            $request->session()->regenerate();

            $retornoAjax['status'] = false;

            $messages = [
                "*$nomeCampo.required" => 'O :input é obrigatório',
                "*$nomeCampo.max" => 'O tamanho do arquivo ultrapassa o limite.',
                "*$nomeCampo.mimes" => 'O tipo do arquivo não é válido.'
            ];

            if ($nomeCampo == 'transportador_contrato_social' || $nomeCampo == 'modal_contrato_social') {

                $validator = Validator::make($request->all(), [
                    "*$nomeCampo" => 'required|max:10240|mimes:jpeg,jpg,bmp,png,pdf'
                ], $messages);
            } else {
                $validator = Validator::make($request->all(), [
                    "*$nomeCampo" => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf'
                ], $messages);
            }

            if ($validator->fails()) {

                $retornoAjax['erro'] = $validator->errors();

                return response()->json($retornoAjax);
            }

            $auth_empresa = Auth::user(); // Busca os dados da Empresa que está Logada
            if (!empty($auth_empresa)) {
                $empresa = $auth_empresa->empresa->cnpj; // CNPJ da Empresa Logada
            } else{
                $empresa = session('cnpj_cad'); // Busca o CNPJ no Cadastro de Empresa
                if (empty($empresa)) {
                    $retornoAjax['erro'] = 'Falta CNPJ';
                    return response()->json($retornoAjax); // Erro! sai aqui por não ter CNPJ de jeito nenhum.
                }
            }
            if ($request->hasFile($nomeCampo.'_alterado')) {
                $nomeCampo = $nomeCampo.'_alterado';
            }
           
            $nomeDoArquivo = $this->gravaArquivoDeUpload($request, $empresa, $nomeCampo);

            $sessionDocumentos = session('documentos');

            if (!empty($sessionDocumentos)) {
                foreach ($sessionDocumentos as $campo => $valor) {
                    $dadosTemporariosEmpresa['documentos'][$campo] = $valor;
                }
            }

            $dadosTemporariosEmpresa['documentos'][$nomeCampo] = $nomeDoArquivo;
        
            session($dadosTemporariosEmpresa);

            $retornoAjax['status'] = true;

            return response()->json($retornoAjax);
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Validar os Documentos na Aba Transportador");
        }
    }

    private function validaDocumentos(Request $request)
    {

        try{
            $sessionDocumentos = session('documentos');
            $responsavel['responsavel'] = session('responsavel');
            $empresa['empresa'] = session('empresa');
            
            if (!empty($sessionDocumentos)) {
                foreach ($sessionDocumentos as $campo => $valor) {
                    $dadosTemporariosEmpresa['documentos'][$campo] = $valor;
                }
            }
            if (isset($sessionDocumentos['licenka']))
                $dadosTemporariosEmpresa['documentos']['licenca'] = $sessionDocumentos['licenka'];
            $dadosTemporariosEmpresa['documentos']['transportador_rg_num_responsavel_tecnico'] = $request->transportador_rg_num_responsavel_tecnico;
            $dadosTemporariosEmpresa['documentos']['transportador_nome_responsavel_tecnico'] = $request->transportador_nome_responsavel_tecnico;
            $dadosTemporariosEmpresa['documentos']['transportador_cpf_num_responsavel_tecnico'] = $request->transportador_cpf_num_responsavel_tecnico;
            $dadosTemporariosEmpresa['documentos']['transportador_crea_num_responsavel_tecnico'] = $request->transportador_crea_num_responsavel_tecnico;
            $dadosTemporariosEmpresa['documentos']['emissaolicenca'] = encodeDate($request->emissaolicenca);
            $dadosTemporariosEmpresa['documentos']['licencavencimento'] = encodeDate($request->licencavencimento);
            $dadosTemporariosEmpresa['documentos']['licencadefault'] = $request->licencadefault;
            
            unset($dadosTemporariosEmpresa['documentos']['licenka']);

            $dadosTemporariosEmpresa['documentos']['transportador_anotacao_responsabilidade_tecnica_emissao'] = encodeDate($request->transportador_anotacao_responsabilidade_tecnica_emissao);
            $dadosTemporariosEmpresa['documentos']['transportador_anotacao_responsabilidade_tecnica_vencimento'] = encodeDate($request->transportador_anotacao_responsabilidade_tecnica_vencimento);

            $dadosTemporariosEmpresa['documentos']['transportador_certificado_dispensa_licenca_emissao'] = encodeDate($request->transportador_certificado_dispensa_licenca_emissao);
            $dadosTemporariosEmpresa['documentos']['transportador_certificado_dispensa_licenca_vencimento'] = encodeDate($request->transportador_certificado_dispensa_licenca_vencimento);

            $dadosTemporariosEmpresa['documentos']['emissaolicenca'] = encodeDate($request->emissaolicenca);
            $dadosTemporariosEmpresa['documentos']['licencavencimento'] = encodeDate($request->licencavencimento);

            $dadosTemporariosEmpresa['documentos']['transportador_alvara_prefeitura_emissao'] = encodeDate($request->transportador_alvara_prefeitura_emissao);
            $dadosTemporariosEmpresa['documentos']['transportador_alvara_prefeitura_vencimento'] = encodeDate($request->transportador_alvara_prefeitura_vencimento);

            $dadosTemporariosEmpresa['documentos']['transportador_ctf_ibama_emissao'] = encodeDate($request->transportador_ctf_ibama_emissao);
            $dadosTemporariosEmpresa['documentos']['transportador_ctf_ibama_vencimento'] = encodeDate($request->transportador_ctf_ibama_vencimento);

            $dadosTemporariosEmpresa['documentos']['transportador_avcb_emissao'] = encodeDate($request->transportador_avcb_emissao);
            $dadosTemporariosEmpresa['documentos']['transportador_avcb_vencimento'] = encodeDate($request->transportador_avcb_vencimento);

            session($dadosTemporariosEmpresa);

            $empresa['empresa']['empresa_tipo'] = $this->tipoEmpresa($empresa['empresa']['ramo_atividade']);

            $termoDeUso = EmpresasTipoXTermosDeUso::with('termos_de_uso')->where('empresa_tipo_id',
                config('enums.empresas_tipo.transportador'))->first();
            $termoDeUso = $termoDeUso->termos_de_uso->termo_uso;

            // transportador , destino ou cooperativa
            $tipo = $this->nomeTipoEmpresa($empresa['empresa']['empresa_tipo']);
          
            $retornoAjax['status'] = true;
            $retornoAjax['termoDeUso'] = $termoDeUso;
            $retornoAjax['termoDeUso'] =  str_replace("[Linha de Código puxando a razão social cadastrada]", $empresa['empresa']['razao_social'], $retornoAjax['termoDeUso']);
            $retornoAjax['termoDeUso'] =  str_replace("[Linha de código puxando a data do momento do cadastro]", date('d/m/Y'), $retornoAjax['termoDeUso']);
            $retornoAjax['termoDeUso'] =  str_replace("[Linha d código para definir o módulo de cadastro : grande gerador, pequeno gerador, gerador misto, etc]", $tipo, $retornoAjax['termoDeUso']);
       
            $retornoAjax['idAnterior'] = '#tela-4tr';
            // termos-de-uso
            $retornoAjax['idDestino'] = '#tela-5';

            return $retornoAjax;
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Validar a Aba de Documentos");
        }
    }

    public function validaDocsCoo(Request $request)
    {
        return $this->validaDocumentos($request);
    }

    public function validaTransportador(CadastroEmpresaUploadRequest $request)
    {
        return $this->validaDocumentos($request);
    }

    public function validaCooperativa(Request $request)
    {
        try {
            $retornoAjax['status'] = true;
            $responsavel['responsavel'] = session('responsavel');
            $empresa['empresa'] = session('empresa');
            $termoDeUso = EmpresasTipoXTermosDeUso::with('termos_de_uso')->where('empresa_tipo_id',
                config('enums.empresas_tipo.cooperativa_residuos'))->first();
            $tipo = $this->nomeTipoEmpresa(config('enums.empresas_tipo.cooperativa_residuos'));

            $termoDeUso = $termoDeUso->termos_de_uso->termo_uso;
            $retornoAjax['termoDeUso'] = $termoDeUso;
            $retornoAjax['termoDeUso'] =  str_replace("[Linha de Código puxando a razão social cadastrada]",  $empresa['empresa']['razao_social'], $retornoAjax['termoDeUso']);
            $retornoAjax['termoDeUso'] =  str_replace("[Linha de código puxando a data do momento do cadastro]", date('d/m/Y'), $retornoAjax['termoDeUso']);
            $retornoAjax['termoDeUso'] =  str_replace("[Linha d código para definir o módulo de cadastro : grande gerador, pequeno gerador, gerador misto, etc]", $tipo, $retornoAjax['termoDeUso']);

            $retornoAjax['idAnterior'] = '#tela-4co';
            // termos-de-uso
            $retornoAjax['idDestino'] = '#tela-5';

            return $retornoAjax;

        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao validar Aba Cooperativa");
        }
    }

    public function validaTermosDeUso()
    {
        try {
            $empresa['empresa'] = session('empresa');

            $responsavel['responsavel'] = session('responsavel');
            $dados['informacaoComplementar']['frequencia_geracao_id'] = session('informacaoComplementar') ? session('informacaoComplementar')['frequencia_geracao_id'] : 0;

            $empresa['empresa']['empresa_tipo'] = $this->tipoEmpresa($empresa['empresa']['ramo_atividade'],
                $dados['informacaoComplementar']['frequencia_geracao_id']);
            $tipo = $this->nomeTipoEmpresa($empresa['empresa']['empresa_tipo']);

            session($empresa);

            $termo = EmpresasTipoXTermosDeUso::with('termos_de_uso')->where('empresa_tipo_id',
                $empresa['empresa']['empresa_tipo'])->first();

            if($termo) {
                
                $termo = $termo->termos_de_uso->termo_uso;
                $retornoAjax['termoDeUso'] = $termo;
                $retornoAjax['termoDeUso'] =  str_replace("[Linha de Código puxando a razão social cadastrada]",  $empresa['empresa']['razao_social'], $retornoAjax['termoDeUso']);
                $retornoAjax['termoDeUso'] =  str_replace("[Linha de código puxando a data do momento do cadastro]", date('d/m/Y'), $retornoAjax['termoDeUso']);
                $retornoAjax['termoDeUso'] =  str_replace("[Linha d código para definir o módulo de cadastro : grande gerador, pequeno gerador, gerador misto, etc]", $tipo, $retornoAjax['termoDeUso']);
           
        
            } else {
                $retornoAjax['termoDeUso'] = '';
            }

            return response()->json($retornoAjax);

        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao validar a Aba Termo de Uso");
        }
    }

    /**
     * Salva os dados da empresa e suas dependências no banco.
     *
     * @param Request $request, $filial
     * - var $filial by Mr.Goose em 25 jun 19 afim de aproveitar este Método para Criação de Filial.
     *
     * @return \Illuminate\Http\Response|string
     * @throws \Exception
     */
    public static function store(Request $request, $filial = false)
    {
        $dadosEmpresa = session('empresa');
        $dadosResponsavel = session('responsavel');
        $dadosComplementares = session('informacaoComplementar');
        $dadosCondominio = session('condominio');
        $dadosDocumentos = session('documentos');
        $dadosResponsavelEmpreendimento = session('responsavelEmpreendimento');
        $retorno = [];

        DB::beginTransaction();

        try {

            if (empty($filial)){
                // entra aqui se NÃO for uma Filial, ou seja, é uma Empresa normal.
                $sigla = self::siglaEmpresa(
                    $dadosEmpresa['ramo_atividade'], $dadosComplementares['frequencia_geracao_id']
                );

                $codigo_ctre = GeradorCodigos::geraCodigoEmpresa($sigla);
            } // close if (empty($filial))
            else{
                // entra aqui se FOR uma Filial
                $dadosMatriz = session('matriz');
                $sigla = substr($dadosMatriz['id_limpurb'], 0, 2);
                $codigo_ctre = GeradorCodigos::geraCodigoFilial($dadosMatriz['id'], $dadosMatriz['id_limpurb']);
            } // close else if (empty($filial))
            $empresa = self::salvaEmpresa($dadosEmpresa, $dadosResponsavel, $codigo_ctre, $filial);
            self::salvaTipoEmpresa($sigla, $empresa->id, $filial);

            // if (empty($filial))
            self::salvaEnderecoEmpresa($dadosEmpresa, $empresa->id);

            $salvaComp = self::salvaInformacoesComplementares($empresa, $dadosEmpresa, $dadosComplementares, $dadosDocumentos);

            self::salvaUsuario($dadosResponsavel, $empresa->id); // usuario que tera acesso ao sistema

            $protocolo = self::salvaProtocolo($empresa->id); // log


            if (isset($dadosCondominio['sindico'])) { // faz vinculo com condominio
               $salvaCondo = self::salvaDadosCondominio($dadosCondominio, $empresa);
            }

            // dados responsavel : Destino Final, Transportador e Cooperativa

            if ($dadosResponsavelEmpreendimento) {
                self::salvaResposavelEmpreendimento($dadosResponsavelEmpreendimento, $empresa);
            }

            if ((isset($salvaCondo) && !empty($salvaCondo)) && (isset($salvaComp) && !empty($salvaComp))) {

                Storage::disk('s3')->deleteDirectory("licencas_temp/{$empresa->cnpj}/");
            }

            if (empty($filial)) {
                // entra aqui se NÃO for cadastro de uma Filial, ou seja, é um Cadastro Normal

                /// GERA QR CODE
                self::qrcodeEmpresa($empresa->id);

                //*** Pega os dados para retornar para a view de sucesso ***//
                $empresa = Empresa::with('empresas_x_empresas_tipos')
                    ->with('empresa_responsavel')
                    ->find($empresa->id);
                $empresaTipo = $empresa->empresas_x_empresas_tipos;
                $empresa_tipo_id = $empresaTipo->empresa_tipo_id;

                $tipoCadastro = $empresaTipo->empresas_tipo->nome;
                if (in_array($empresaTipo->empresas_tipo->id, [
                    config('enums.empresas_tipo.orgao_publico'),
                    config('enums.empresas_tipo.servico_saude'),
                    config('enums.empresas_tipo.condominio_misto'),
                ])) {
                    $tipoCadastro .= '/Grande Gerador';
                }

                $retorno['tipoCadastro'] = $tipoCadastro;
                $retorno['tipoEmpresa'] = $empresaTipo->empresas_tipo->id;
                $retorno['protocolo'] = $protocolo->id;
                $retorno['id_limpurb'] = $empresa->id_limpurb;
                $retorno['inscricaoMunicipal'] = $empresa->im;
                $retorno['razaoSocial'] = $empresa->razao_social;
                $retorno['email'] = $empresa['email'];

                $retorno['mensagem'] = self::mensagemRetorno($empresa_tipo_id);

                //*** Dados para envio de email ***//
                $subject = self::emailSubject($empresa->status_id);
                $dadosEmail = [
                    'tipoCadastro' => $tipoCadastro,
                    'protocolo' => $protocolo->id,
                    'id_limpurb' =>  $empresa->id_limpurb,
                    'razaoSocial' => $empresa->razao_social,
                    'inscricaoMunicipal' => $empresa->im,
                    'dataEnvio' => date('d/m/Y'),
                    'emailReceptor' => $empresa->email,
                    'subject' => $subject,
                    'email' => $empresa['email'],
                    'senha' => self::$new_password
                ];

                if (!empty($dadosCondominio)) {
                    $dadosEmail['responsavel'] = $empresa->nome_responsavel;
                    $dadosEmail['telefone'] = $empresa->telefone_responsavel;
                    $dadosEmail['nomeComercial'] = $empresa->nome_comercial;
                    $dadosEmail['cnpj'] = $empresa->cnpj;
                }

            } // if (empty($filial))
            else{
                // -> Entra aqui se for uma Filial.
                $dadosEmail                      = session('dadosEmail');
                $dadosEmail['password']          = self::$new_password;
                $dadosEmail['subject']           = 'CTRE - Cadastramento de Filial';
                $dadosEmail['emailReceptor']     = $dadosEmail['login'];
                $empresa_tipo_id                 = $dadosEmail['empresas_tipo_id'];
                $dadosEmail['id_limpurb_filial'] = $codigo_ctre;

            } // else if (empty($filial))
            
            self::emailCadastroRealizado($dadosEmail, $empresa_tipo_id, $filial);

            DB::commit();

            //Limpa a sessão dos dados do cadastro.
            session()->forget([
                'empresa',
                'responsavel',
                'informacaoComplementar',
                'condominio',
                'documentos'
            ]);

            if (!empty($filial))
                return $empresa->id;

            return redirect()->route('cadastro.sucesso')->with($retorno);

        } catch (\Exception $e) {
            DB::rollback();

            return $e->getMessage();
        }
    }

    /*-
    * var $idMatriz AND $filial criadas by Mr.Goose em 26 jun 19, afim de Registrar a Empresa Filial ligada à sua Matriz de mesmo CNPJ.
    -*/
    private static function salvaEmpresa($dadosEmpresa, $dadosResponsavel, $codigo_ctre, $filial = false)
    {
        try {
            $empresa = Empresa::where('cnpj', $dadosEmpresa['cnpj'])->first();
            if (!empty($filial) || is_null($empresa)) {
                if (!empty($filial))
                    $idMatriz = $empresa->id;

                $empresa = new Empresa();
                $empresa->cnpj = $dadosEmpresa['cnpj'];
            }

            if (!empty($idMatriz)){
                // entra aqui se FOR uma Filial.
                $empresa->id_matriz = $idMatriz;
                $empresa->status_id = config('enums.status.pre_cadastro'); // Novo Status. by Mr.Goose em 10jul19 conforme solicitação do Antoine.
            }else{
                // Entra aqui se form uma empresa normal.
                $empresa->status_id = config('enums.status.autodeclarado');
            }
            
            $empresa->id_limpurb = $codigo_ctre;
            $empresa->razao_social = $dadosEmpresa['razao_social'];
            $empresa->nome_comercial = $dadosEmpresa['nome_comercial'];
            $empresa->ie = $dadosEmpresa['ie'];
            $empresa->im = $dadosEmpresa['im'];
            $empresa->telefone = $dadosEmpresa['telefone'];
            $empresa->nome_responsavel = $dadosResponsavel['responsavel'];
            $empresa->email = $dadosResponsavel['email'];
            $empresa->telefone_responsavel = $dadosResponsavel['telefone_responsavel'];
            $empresa->ramal = $dadosResponsavel['ramal'];
            $empresa->celular = $dadosResponsavel['celular_responsavel'];
            $empresa->cargo = $dadosResponsavel['cargo'];
            $empresa->save();
            return $empresa;
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados da Empresa!");
        }
    }

    private static function salvaTipoEmpresa($sigla, $empresaId, $filial = false)
    {
        try {
            $tipoEmpresa = EmpresasXEmpresasTipo::where('empresa_id', $empresaId)->first();
            if (is_null($tipoEmpresa)) {
                $tipoEmpresa = new EmpresasXEmpresasTipo();
                $tipoEmpresa->empresa_id = $empresaId;
            }
            
            $status = config('enums.status.autodeclarado');
            
            switch ($sigla) {
                case config('enums.empresas_sigla.grande_gerador'):
                    $tipoEmpresa->empresa_tipo_id = config('enums.empresas_tipo.grande_gerador');
                    break;
                case config('enums.empresas_sigla.pequeno_gerador'):
                    $tipoEmpresa->empresa_tipo_id = config('enums.empresas_tipo.pequeno_gerador');
                    break;
                case config('enums.empresas_sigla.transportador'):
                    $tipoEmpresa->empresa_tipo_id = config('enums.empresas_tipo.transportador');
                    $status = config('enums.status.em_analise_amlurb');
                    break;
                case config('enums.empresas_sigla.destino_final_reciclado'):
                    $tipoEmpresa->empresa_tipo_id = config('enums.empresas_tipo.destino_final_reciclado');
                    $status = config('enums.status.em_analise_amlurb');
                    break;
                case config('enums.empresas_sigla.cooperativa_residuos'):
                    $tipoEmpresa->empresa_tipo_id = config('enums.empresas_tipo.cooperativa_residuos');
                    $status = config('enums.status.em_analise_amlurb');
                    break;
                case config('enums.empresas_sigla.orgao_publico'):
                    $tipoEmpresa->empresa_tipo_id = config('enums.empresas_tipo.orgao_publico');
                    break;
                case config('enums.empresas_sigla.servico_saude'):
                    $tipoEmpresa->empresa_tipo_id = config('enums.empresas_tipo.servico_saude');
                    break;
                case config('enums.empresas_sigla.condominio_misto'):
                    $tipoEmpresa->empresa_tipo_id = config('enums.empresas_tipo.condominio_misto');
                    break;
            }

            if (empty($filial))
                Empresa::find($empresaId)->update(
                    [
                        'status_id' => $status
                    ]
                );

            $tipoEmpresa->status_id = config('enums.status.ativo');
            $tipoEmpresa->save();
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados de Tipo da Empresa");
        }
    }

    private static function salvaEnderecoEmpresa($dadosEmpresa, $empresa_id)
    {
        try {
            $endereco = EmpresaEndereco::where('empresa_id', $empresa_id)->first();
            if (is_null($endereco)) {
                $endereco = new EmpresaEndereco();
            }

            $endereco->empresa_id = $empresa_id;
            $endereco->estado_id = $dadosEmpresa['estado'];
            $endereco->cidade_id = $dadosEmpresa['cidade'];
            $endereco->endereco = $dadosEmpresa['endereco'];
            $endereco->cep = strtr($dadosEmpresa['cep'], ['.' => '', '-' => '']);
            $endereco->bairro = $dadosEmpresa['bairro'];
            $endereco->complemento = $dadosEmpresa['complemento'];
            $endereco->ponto_referencia = $dadosEmpresa['ponto_referencia'];
            $endereco->numero = $dadosEmpresa['numero'];

            // Pega a geolocalização do endereço da empresa
            if (!empty($endereco->endereco) && !empty($endereco->numero) && !empty($endereco->cidade_id) && !empty($endereco->estado_id))
                $geoLocation = (new GeoLocationController)->geoLocal(
                    $endereco->endereco . ", " . $endereco->numero . ", " .
                    Cidade::find($endereco->cidade_id) . ", " . Estado::find($endereco->estado_id)
                );

            if (!empty($geoLocation)) {
                $endereco->latitude = $geoLocation->lat;
                $endereco->longitude = $geoLocation->lng;
            }

            $endereco->save();
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados de Endereço da empresa");
        }
    }

    private static function salvaInformacoesComplementares(Empresa $empresa, $dadosEmpresa, $dadosComplementares, $dadosDocumentos = null)
    {
        try {
            $informacaoComplementar = new EmpresaInformacaoComplementars;
            $informacaoComplementar->empresa_id = $empresa->id;
            $informacaoComplementar->ramo_atividade_id = $dadosEmpresa['ramo_atividade'];
            $informacaoComplementar->tipo_ramo_atividade_id = $dadosEmpresa['tipo_atividade'];

            // nao salva estes dados se for transportador, destino final ou cooperativa
            if (!is_null($dadosComplementares) && !empty($dadosComplementares)) {
                $informacaoComplementar->frequencia_geracao_id = $dadosComplementares['frequencia_geracao_id'];
                $informacaoComplementar->frequencia_coleta_id = $dadosComplementares['frequencia_coleta_id'];
                $informacaoComplementar->colaboradores_numero_id = $dadosComplementares['colaboradores_numero_id'];
                $informacaoComplementar->energia_consumo_id = $dadosComplementares['energia_consumo_id'];
                $informacaoComplementar->estabelecimento_tipo_id = $dadosComplementares['estabelecimento_tipo_id'];
                $informacaoComplementar->area_total = str_replace('.', '', str_replace(',', '', $dadosComplementares['area_total']));
                $informacaoComplementar->area_construida = str_replace('.', '', str_replace(',', '', $dadosComplementares['area_construida']));
            }
            $informacaoComplementar->save();

            if (!is_null($dadosDocumentos) && !empty($dadosDocumentos)) {

                $documentos = config('enums.documentos');
                foreach ($dadosDocumentos as $campo => $valor) {
                    $campo = str_replace('condominio_file_', 'condominio_sindico_', $campo);
                    if (array_key_exists($campo, $documentos) && !empty($valor)) {
                        $empresaDocumento = new EmpresaDocumentos;
                        $empresaDocumento->empresa_id = $empresa->id;
                        $empresaDocumento->documento_id = $documentos[$campo];
                        $empresaDocumento->caminho_arquivo = $valor;
                        if(!empty($dadosDocumentos[$campo.'_emissao']))
                            $empresaDocumento->data_emissao = $dadosDocumentos[$campo.'_emissao'];
                        if(!empty($dadosDocumentos[$campo.'_vencimento']))
                            $empresaDocumento->data_vencimento = $dadosDocumentos[$campo.'_vencimento'];
                        $empresaDocumento->save();
                    } // close if (array_key_exists($campo, $documentos))
                } // close foreach ($dadosDocumentos as $campo => $valor)

                if (!empty($dadosDocumentos['licenca'])) {
                    $empresaDocumento = new EmpresaDocumentos;
                    $empresaDocumento->empresa_id = $empresa->id;
                    $empresaDocumento->documento_id = $dadosDocumentos['licencadefault'];
                    $empresaDocumento->caminho_arquivo = $dadosDocumentos['licenca'];
                    if(!empty($dadosDocumentos['emissaolicenca']))
                        $empresaDocumento->data_emissao = $dadosDocumentos['emissaolicenca'];
                    if(!empty($dadosDocumentos['licencavencimento']))
                        $empresaDocumento->data_vencimento = $dadosDocumentos['licencavencimento'];
                    $empresaDocumento->save();
                } // close if(!empty($dadosDocumentos['licenca']))

                $newPatch = "licencas/{$empresa->id}/";
                $oldPath = "licencas_temp/{$empresa->cnpj}/";
                //Pega os arquivos da pasta temporária
                $pastaTemporaria = Storage::disk('s3')->allFiles("licencas_temp/{$empresa->cnpj}/");

                foreach ($pastaTemporaria as $file) {
                    $copied_file = str_replace($oldPath, $newPatch, $file);
                    if (!Storage::disk('s3')->exists($copied_file)) {
                        Storage::disk('s3')->copy($file, $copied_file);
                    }

                    /*- abaixo, inserido by Mr.Goose em 03 maio 19, conforme dica do DeJota afim de garantir o salvamento do Arquivo no BD -*/
                    $breakPathFile = explode('/', $file);
                    $nomeDoc = explode('.', $breakPathFile[2]);
                    $campo = $nomeDoc[0];
                    if (!empty($documentos[$campo])) {
                        $arrayDados = array(
                            'empresa_id' => $empresa->id,
                            'documento_id' => $documentos[$campo],
                            'caminho_arquivo' => $breakPathFile[2]
                        );
                        if(!empty($dadosDocumentos[$campo.'_emissao']))
                            $arrayDados['data_emissao'] = $dadosDocumentos[$campo.'_emissao'];
                        if(!empty($dadosDocumentos[$campo.'_vencimento']))
                            $arrayDados['data_vencimento'] = $dadosDocumentos[$campo.'_vencimento'];
                    } else
                    if ($campo == 'licenka') {
                        $arrayDados = array(
                            'empresa_id' => $empresa->id,
                            'documento_id' => $dadosDocumentos['licencadefault'],
                            'caminho_arquivo' => $breakPathFile[2]
                        );
                        if(!empty($dadosDocumentos['emissaolicenca']))
                            $arrayDados['data_emissao'] = $dadosDocumentos['emissaolicenca'];
                        if(!empty($dadosDocumentos['licencavencimento']))
                            $arrayDados['data_vencimento'] = $dadosDocumentos['licencavencimento'];
                    }
                    if (!empty($arrayDados))
                        EmpresaDocumentos::firstOrCreate($arrayDados);
                    /*- acima, inserido by Mr.Goose em 03 maio 19, conforme dica do DeJota afim de garantir o salvamento do Arquivo no BD -*/
                }
            }
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados de Informações Complementares da empresa");
        }
    }

    private static function salvaUsuario($dadosResponsavel, $empresa_id)
    {
        try {
            $user = User::where('email',$dadosResponsavel['email'])->first();
            if (!is_null($user)) {
                self::$new_password = str_random('12');
                $user->update(
                    [
                        'email' => $dadosResponsavel['email'],
                        'name' => $dadosResponsavel['responsavel'],
                        'empresa_id' => $empresa_id,
                        'password' => bcrypt(self::$new_password)
                    ]
                );

                return;
            }
            $usuario = new User;
            $usuario->email = $dadosResponsavel['email'];
            $usuario->name = $dadosResponsavel['responsavel'];
            $usuario->empresa_id = $empresa_id;
            self::$new_password = str_random('12');
            $usuario->password = bcrypt(self::$new_password);
            $usuario->save();
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados de Usuário");
        }
    }

    private static function salvaProtocolo($empresa_id)
    {
        try {
            $protocolo = new Protocolo;
            $protocolo->empresa_id = $empresa_id;
            $protocolo->numero_protocolo = geraCodigoProtocolo($empresa_id, 'cadastro');
            $protocolo->nome = 'REGISTRO EFETUADO';
            $protocolo->descricao = "Foi realizado o cadastro de licenças para a empresa com id = {$empresa_id}";
            $protocolo->created_at = now();
            $protocolo->save();
            return $protocolo;
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados de Protocolo");
        }
    }

    private static function salvaResposavelEmpreendimento($dadosResp, $empresa)
    {
        try {
            $empresaCondominio['cnpj'] = cnpjToInteger($empresa->cnpj);
            $empresaExiste = Empresa::where('cnpj', $empresaCondominio['cnpj'])->first();

            if(isset($dadosResp['presidente'])){
               $principal = 'presidente';
            } else {
                $principal = 'socio_principal';
            }

            if ($empresaExiste) {
                $dadosResponsavel = [
                    'empresa_id' => $empresaExiste->id,
                    'nome' => $dadosResp['nome_socio'],
                    'rg' => $dadosResp['numero_rg_socio'],
                    'cpf' => $dadosResp['numero_cpf_socio'],
                    'responsavel' => $principal,
                ];

                EmpresaResponsavel::create($dadosResponsavel);

                if(!empty($dadosResp['nome_socio_2'])){
                    $dadosResponsavel2 = [
                        'empresa_id' => $empresaExiste->id,
                        'nome' => $dadosResp['nome_socio_2'],
                        'rg' => $dadosResp['numero_rg_socio_2'],
                        'cpf' => $dadosResp['numero_cpf_socio_2'],
                        'responsavel' => 'socio',
                    ];
                    EmpresaResponsavel::create($dadosResponsavel2 );
                }

                if(!empty($dadosResp['nome_socio_3'])){
                    $dadosResponsavel3 = [
                        'empresa_id' => $empresaExiste->id,
                        'nome' => $dadosResp['nome_socio_3'],
                        'rg' => $dadosResp['numero_rg_socio_3'],
                        'cpf' => $dadosResp['numero_cpf_socio_3'],
                        'responsavel' => 'socio',
                    ];
                    EmpresaResponsavel::create($dadosResponsavel3);
                }

            }
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados de Responsável Empreendimento");
        }

    }
    private static function salvaDadosCondominio($dadosCondominio, $empresa)
    {
        try {
            $empresaCondominio['cnpj'] = cnpjToInteger($empresa->cnpj);
            $empresaExiste = Empresa::where('cnpj', $empresaCondominio['cnpj'])->first();

            if ($empresaExiste) {

                $dadosResponsavel = [
                    'empresa_id' => $empresaExiste->id,
                    'nome' => $dadosCondominio['sindico'],
                    'rg' => $dadosCondominio['sindico_rg'],
                    'cpf' => $dadosCondominio['sindico_cpf'],
                    'responsavel' => 'sindico',
                ];

                //Se a empresa já existe, adicionamos um novo responsável na empresa_responsavel
                EmpresaResponsavel::create($dadosResponsavel);

                $newPatch = "licencas/{$empresa->id}/";
                $oldPath = "licencas_temp/{$empresa->cnpj}/";
                //Pega os arquivos da pasta temporária
                $pastaTemporaria = Storage::disk('s3')->allFiles("licencas_temp/{$empresa->cnpj}/");

                foreach ($pastaTemporaria as $file) {
                    $copied_file = str_replace($oldPath, $newPatch, $file);
                    if (!Storage::disk('s3')->exists($copied_file)) {
                        Storage::disk('s3')->copy($file, $copied_file);
                    }
                }
            } else {
                $dado['status_id'] = \config('enums.status.inativo');
                $dado['cnpj'] = $empresaCondominio['cnpj'];
                $dado['nome_responsavel'] = $dadosCondominio['nome'];
                $dado['email'] = $dadosCondominio['email'];
            }
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados do Condomínio");
        }
    }

    private static function qrcodeEmpresa($empresa_id)
    {
        try {
            $qrcode_empresa = GeradorCodigos::geraQrCodeEmpresa($empresa_id);
            $qrcode = Qrcode::create([
                'tipo' => \config('enums.tipos_qrcode.empresa'),
                'codigo' => $qrcode_empresa,
                'status_id' => \config('enums.status.ativo')
            ]);
            QrcodeEmpresas::create([
                'qrcode_id' => $qrcode->id,
                'empresa_id' => $empresa_id
            ]);
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados do QrCode da empresa");
        }
    }

    private static function mensagemRetorno($empresa_tipo_id)
    {
        $mensagem = 'Mensagem padrão';
        switch ($empresa_tipo_id) {
            case config('enums.empresas_tipo.grande_gerador'):
                $mensagem = 'Mensagem do Grande gerador';
                break;

            case config('enums.empresas_tipo.pequeno_gerador'):
                $mensagem = 'Mensagem do pequeno gerador';
                break;

            case config('enums.empresas_tipo.transportador'):
                $mensagem = 'Mensagem do transportador';
                break;

            default:
                break;
        }
        return $mensagem;
    }

    private static function emailSubject($empresa_status_id)
    {
        if ($empresa_status_id == config('enums.status.ativo')) {
            return 'CTRE - Cadastro criado com sucesso';
        }
        if ($empresa_status_id == config('enums.status.autodeclarado')) {
            return 'CTRE - Cadastro aguardando validação';
        }
        return '';
    }

    private static function emailCadastroRealizado($dadosEmail, $empresa_tipo_id, $filial = false)
    {
        if (!empty($filial)) {
            $dadosEmail['view'] = 'emails.cadastros-filiais.cadastrar-filial';
        } else
        if (config('enums.empresas_tipo.pequeno_gerador') === $empresa_tipo_id) {
            $dadosEmail['view'] = 'emails.cadastros-empresas.cadastro-realizado-pg';
        } else {
            $dadosEmail['view'] = 'emails.cadastros-empresas.cadastro-realizado';
        }

        $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($dadosEmail['emailReceptor']));
        
        $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
            "email" => 'mailgun_email:mailbox'
        ]);
        if ($validator->fails()) {
         
            $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
            throw new \Exception($retornoAjax['erro']);
        }

        Mail::to($dadosEmail['emailReceptor'])->queue(new CadastroRealizado($dadosEmail));
    }

    /**
     * Método responsável por mostrar a view de sucesso após os termos de uso serem validados.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function empresaCadastroSucesso()
    {

        if (session('cnpj') != null) {
            return view("cadastro.sucesso");
        } else {
            Auth::logout();
            Session::flush();
            return redirect("/login");
        }
    }

    private static function siglaEmpresa($idRamoAtividade = 0, $idGeracaoDiaria = 0)
    {
        $siglaEmpresa = null;
        $idGeracaoDiaria = (int)$idGeracaoDiaria;
        if ($idGeracaoDiaria === 1) {
            return config('enums.empresas_sigla.pequeno_gerador');
        }

        switch ($idRamoAtividade) {
            case config('enums.ramo_atividade.tratamento_residuos'):
                $siglaEmpresa = config('enums.empresas_sigla.transportador');
                break;
            case config('enums.ramo_atividade.cooperativa'):
                $siglaEmpresa = config('enums.empresas_sigla.cooperativa_residuos');
                break;
            case config('enums.ramo_atividade.destino_final'):
                $siglaEmpresa = config('enums.empresas_sigla.destino_final_reciclado');
                break;
            case config('enums.ramo_atividade.orgao_publico'):
                $siglaEmpresa = config('enums.empresas_sigla.orgao_publico');
                break;
            case config('enums.ramo_atividade.servico_saude'):
                $siglaEmpresa = config('enums.empresas_sigla.servico_saude');
                break;
            case config('enums.ramo_atividade.condominio_misto'):
                $siglaEmpresa = config('enums.empresas_sigla.condominio_misto');
                break;
            default:
                $siglaEmpresa = config('enums.empresas_sigla.grande_gerador');
                break;
        }
        return $siglaEmpresa;
    }

    private function nomeTipoEmpresa($empresa_tipo_id)
    {
        $tipo = '';
        switch($empresa_tipo_id) {
            case config('enums.empresas_tipo.grande_gerador'):
                $tipo = 'Grande Gerador';
                break;
            case config('enums.empresas_tipo.pequeno_gerador'):
                $tipo = 'Pequeno Gerador';
                break;
            case config('enums.empresas_tipo.transportador'):
                $tipo = 'Transportador';
                break;
            case config('enums.empresas_tipo.destino_final_reciclado'):
                $tipo = 'Destino Final';
                break;
            case config('enums.empresas_tipo.cooperativa_residuos'):
                $tipo = 'Cooperativa';
                break;
            case config('enums.empresas_tipo.orgao_publico'):
                $tipo = 'Órgão Público';
                break;
            case config('enums.empresas_tipo.servico_saude'):
                $tipo = 'Serviço de saúde';
                break;
            case config('enums.empresas_tipo.condominio_misto'):
                $tipo = 'Condomínio Misto';
                break;
        }
        return $tipo;
    }

    private function tipoEmpresa($ramoAtividade = 0, $idGeracaoDiaria = 0)
    {

        if ($ramoAtividade != 0) {
            $tipoEmpresa = 0;

            $siglaEmpresa = $this->siglaEmpresa($ramoAtividade, $idGeracaoDiaria);

            switch ($siglaEmpresa) {
                case config('enums.empresas_sigla.grande_gerador'):
                    $tipoEmpresa = config('enums.empresas_tipo.grande_gerador');
                    break;

                case config('enums.empresas_sigla.pequeno_gerador'):
                    $tipoEmpresa = config('enums.empresas_tipo.pequeno_gerador');
                    break;

                case config('enums.empresas_sigla.transportador'):
                    $tipoEmpresa = config('enums.empresas_tipo.transportador');
                    break;

                case config('enums.empresas_sigla.destino_final_reciclado'):
                    $tipoEmpresa = config('enums.empresas_tipo.destino_final_reciclado');
                    break;

                case config('enums.empresas_sigla.cooperativa_residuos'):
                    $tipoEmpresa = config('enums.empresas_tipo.cooperativa_residuos');
                    break;

                case config('enums.empresas_sigla.orgao_publico'):
                    $tipoEmpresa = config('enums.empresas_tipo.orgao_publico');
                    break;

                case config('enums.empresas_sigla.servico_saude'):
                    $tipoEmpresa = config('enums.empresas_tipo.servico_saude');
                    break;

                case config('enums.empresas_sigla.condominio_misto'):
                    $tipoEmpresa = config('enums.empresas_tipo.condominio_misto');
                    break;
            }

            return $tipoEmpresa;

        } else {
            return false;
        }

    }

    public function uploadDocumentos(Request $request, $empresaId, $nomeCampo)
    {
        $retornoAjax['status'] = false;

        $messages = [
            "*$nomeCampo.required" => 'O :input é obrigatório',
            "*$nomeCampo.max" => 'O tamanho do arquivo ultrapassa o limite.',
            "*$nomeCampo.mimes" => 'O tipo do arquivo não é válido.'
        ];

        if ($nomeCampo == 'transportador_contrato_social' || $nomeCampo == 'modal_contrato_social') {

            $validator = Validator::make($request->all(), [
                "*$nomeCampo" => 'required|max:10240|mimes:jpeg,jpg,bmp,png,pdf'
            ], $messages);
        } else {
            $validator = Validator::make($request->all(), [
                "*$nomeCampo" => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf'
            ], $messages);
        }

        if ($validator->fails()) {

            $retornoAjax['erro'] = $validator->errors();

            return response()->json($retornoAjax);
        }

        DB::beginTransaction();
        try{

            if ($empresaId) {
                $retornoAjax['status'] = $this->gravaArquivoDeUpload($request, $empresaId, $nomeCampo);
            }

            DB::commit();

            return response()->json($retornoAjax);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollback();
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    private function gravaArquivoDeUpload($request, $cnpj, $nomeCampo)
    {
        try {
            // Caminho que salvará os arquivos de licenças, criando uma pasta com o id da empresa
            $path = 'licencas_temp/' . $cnpj.'/';
           
            $arquivo = $request->file($nomeCampo);
            if ($request->hasFile($nomeCampo) && $arquivo->isValid()) {
                $nameFile = $nomeCampo . '.' . $arquivo->getClientOriginalExtension();
                $path = $path.'/'.$nameFile;
                Storage::disk('s3')->put($path, file_get_contents($arquivo));
                return $nameFile;
            }
            return false;
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Gravar Arquivo no s3");
        }
    }

    public function cadastrarFilial()
    {
        return view('painel.fiscal.cadastro-filiais-gg.cadastro-filial-gg');        
    }  

    public function cadastroDeFilial()
    {    
        $titulo = (object) $this->titulo;
        
        return view('painel.fiscal.cadastro-filiais-gg.includes.dados-cadastrais-filiais-gg', compact('titulo')); 
            
    }  
}