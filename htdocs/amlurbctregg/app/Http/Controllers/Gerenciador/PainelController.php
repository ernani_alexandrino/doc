<?php

namespace Amlurb\Http\Controllers\Gerenciador;

use Amlurb\Http\Controllers\Controller;

// Models
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\LogisticaResiduos;
use Amlurb\Models\Perfil;
use Amlurb\Models\TiposEmail;
use Amlurb\Models\Permission;
use Amlurb\Repositories\CtreRepository;
use Amlurb\Repositories\EmpresaRepository;
use Amlurb\Repositories\EmpresasXEquipamentoRepository;
use Amlurb\Repositories\VinculosRepository;

// Dependências Laravel
use Amlurb\Services\UrlConstructor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

// Geração de Boleto e Envio de Segunda Via
use Amlurb\Models\Boleto;
use Amlurb\Http\Controllers\AprovacaoCadastroController;
use Amlurb\Mail\CadastroRealizado;
use Amlurb\Services\ParametrosBoleto;
use Amlurb\Services\DadosEmail;
use Amlurb\Services\ServiceBoletos;
use Amlurb\Models\Empresa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Validator;

class PainelController extends Controller
{
    const DATE_YMD = "DATE_FORMAT(created_at, '%Y-%m-%d')";
    const DATE_YM = "DATE_FORMAT(created_at, '%Y-%m')";

    // Variável $user que usaremos no sistema
    protected $user;

    /**
     * @var CtreRepository
     */
    protected $ctreRepository;

    /**
     * @var EmpresaRepository
     */
    protected $empresaRepository;

    /**
     * @var EmpresasXEquipamentoRepository
     */
    protected $empresasXequipamentoRepository;

    /**
     * @var VinculosRepository
     */
    protected $vinculosRepository;

    public $titulo = array(
        'principal' => 'Amlurb',
        'subtitulo' => 'CTR-E Eletrônico'
    );

    public function __construct()
    {
        $this->ctreRepository = new CtreRepository();
        $this->empresaRepository = new EmpresaRepository();
        $this->empresasXequipamentoRepository = new EmpresasXEquipamentoRepository();
        $this->vinculosRepository = new VinculosRepository();
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index()
    {
        $userTipo = EmpresasXEmpresasTipo::where('empresa_id', $this->user->empresa_id)->where('status_id', 1)->first();

        $redirect = '';

        // Verifica qual é o tipo da empresa que está logada e redireciona para o local correto
        switch ($userTipo->empresa_tipo_id) {
            case config('enums.empresas_tipo.grande_gerador'):
                $redirect = 'painel/gerador';
                break;

            case config('enums.empresas_tipo.pequeno_gerador'):
                $redirect = 'painel/pequeno-gerador';
                break;

            case config('enums.empresas_tipo.transportador'):
                $redirect = 'painel/transportador';
                break;

            case config('enums.empresas_tipo.destino_final_reciclado'):
                $redirect = 'painel/destino-final';
                break;

            case config('enums.empresas_tipo.cooperativa_residuos'):
                $redirect = 'painel/cooperativa';
                break;

            case config('enums.empresas_tipo.orgao_publico'):
                $redirect = 'painel/orgao-publico';
                break;

            case config('enums.empresas_tipo.servico_saude'):
                $redirect = 'painel/servico-saude';
                break;

            case config('enums.empresas_tipo.fiscal'):
            case config('enums.empresas_tipo.administrador'):
                $redirect = 'painel/fiscal';
                break;

            case config('enums.empresas_tipo.condominio_misto'):
                $redirect = 'painel/condominio-misto';
                break;
        }

        session(['userTipo' => $userTipo->empresa_tipo_id]);
        return Redirect::to($redirect);
    }

    public function configuracoes()
    {
        $userTipo = EmpresasXEmpresasTipo::where('empresa_id', $this->user->empresa_id)->first();

        switch ($userTipo->empresa_tipo_id) {
            case 1:
            case 2:
                $type = 'gerador';
                break;

            case 3:
                $type = 'transportador';
                break;

            case 4:
                $type = 'destinofinal';
                break;

            case 5:
                $type = 'cooperativa';
                break;

            case 6:
            case 7:
                $type = 'fiscal';
                break;

            case 10:
                $type = 'condominiomisto';
                break;

            default:
                $type = '';
                break;
        }

        $permissions = Permission::with('actions')->where('type', $type)->get();
        $tiposEmail = TiposEmail::all();
        $perfis = Perfil::with('emails')->where('empresa_id', Auth::user()->empresa_id);

        $permissoes = $perfis->pluck('nome', 'id');
        $perfis = $perfis->get();

        $perfis->each(function ($perfil) {

            $perfil->acesso = json_decode($perfil->acesso, true);

            if (!$perfil->emails->isEmpty()) {
                $perfil->emails = $perfil->emails->pluck('nome');
                $perfil->email = implode(', ', $perfil->emails->toArray());
            } else {
                $perfil->email = '';
            }
        });

        $cols = 0;

        foreach ($permissions as $permission) {
            if ($permission->actions->count() > $cols) {
                $cols = $permission->actions->count();
            }
        }

        $this->titulo['subtitulo'] = 'Configurações';
        $titulo = (object)$this->titulo;
        $veiculos = '';

        return view(
            'painel.fiscal.configuracoes.home',
            compact(
                'titulo',
                'veiculos',
                'permissions',
                'cols',
                'tiposEmail',
                'perfis',
                'permissoes'
            )
        );
    }

    public function getResiduosVinculadosTo($id_empresa)
    {
        $residuos = LogisticaResiduos::where('vinculador_id', Auth::user()->empresa_id) // empresa logada, que cadastrou vinculos
        ->where('empresa_id', $id_empresa)
            ->with('residuo')->get();
        return response()->json($residuos);
    }

    /**
     * Método que retorna o total de vínculos por período, utilizado nos indicadores da home de transportadores e destinos finais
     *
     * @param string $periodo
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function vinculosPorPeriodo($periodo = '')
    {
        //Get todas as datas para os períodos de cada indicador, calculadas pelo helper datesPeriodForIndicator()
        $datesPeriod = datesPeriodForIndicator();
        $period = null;

        if ($periodo != '') {
            switch ($periodo) {
                case 'diario':
                    $period = [$datesPeriod->now, $datesPeriod->tomorrow];
                    break;
                case 'semanal':
                    $period = [$datesPeriod->monday, $datesPeriod->sunday];
                    break;
                case 'mensal':
                    $period = [$datesPeriod->firstDayOfMonth, $datesPeriod->endDayOfMonth];
                    break;
                case 'trimestral':
                    $period = [$datesPeriod->firstDayOfQuarter, $datesPeriod->endDayOfQuarter];
                    break;
                case 'semestreal':
                    $period = [$datesPeriod->firstDayOfSemester, $datesPeriod->endDayOfSemester];
                    break;
                case 'anual':
                    $period = [$datesPeriod->firstDayOfYear, $datesPeriod->endDayOfYear];
                    break;
                case 'total':
                    $period = null;

            }

            $totalVinculos['geradores'] = $this->vinculosRepository->countVinculosPorPeriodo([
                'empresa_tipo' => \config('enums.empresas_tipo.grande_gerador'), 'periodo' => $period
            ]);
            $totalVinculos['pequenosGeradores'] = $this->vinculosRepository->countVinculosPorPeriodo([
                'empresa_tipo' => \config('enums.empresas_tipo.pequeno_gerador'), 'periodo' => $period
            ]);
            $totalVinculos['destinos'] = $this->vinculosRepository->countVinculosPorPeriodo([
                'empresa_tipo' => \config('enums.empresas_tipo.destino_final_reciclado'), 'periodo' => $period
            ]);
            $totalVinculos['transportadores'] = $this->vinculosRepository->countVinculosPorPeriodo([
                'empresa_tipo' => \config('enums.empresas_tipo.transportador'), 'periodo' => $period
            ]);

            return response()->json(['vinculos' => $totalVinculos]);
        } else {
            return response()->json(['error' => 'Erro durante a consulta!']);
        }
    }

    /**
     * Método que retorna o total de CTRE pela empresa selecionada na listagem de cadastro, seja Gerador, Transportador e etc... Alimenta os indicadores da home de cadastro do ente
     *
     * @param string $periodo
     * @param int    $empresaId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ctrePorPeriodoFiscalAjax($periodo = '', $empresaId = 0)
    {
        $ctre = [];
        $params = $this->getFilterParams($periodo);
        if ($empresaId) {
            $params['empresa_id'] = $empresaId;
            $params['empresa_tipo'] = $this->empresaRepository->getEmpresaTipoById($empresaId);
        } else {
            $params['empresa_id'] = null;
            $params['empresa_tipo'] = null;
        }

        $ctre['emitidos'] = $this->ctreRepository->countCtreByDate(null, $params);
        $ctre['abertos'] = $this->ctreRepository->countCtreByDate(\config('enums.status.em_aberto'), $params);
        $ctre['expirados'] = $this->ctreRepository->countCtreByDate(\config('enums.status.expirado'), $params);
        $ctre['finalizados'] = $this->ctreRepository->countCtreByDate(\config('enums.status.finalizado'), $params);
        $ctre['expirados_finalizados'] = $this->ctreRepository->countCtreByDate(\config('enums.status.expirado_finalizado'), $params);

        if ($periodo != '') {
            return response()->json(['ctre' => $ctre]);
        } else {
            return $ctre;
        }
    }

    public function ctrePorPeriodo($periodo = '')
    {
        $ctre = [];

        $params = $this->getFilterParams($periodo);
        $params['empresa_id'] = Auth::user()->empresa_id;
        $params['empresa_tipo'] = Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id;

        $ctre['emitidos'] = $this->ctreRepository->countCtreByDate(null, $params);
        $ctre['abertos'] = $this->ctreRepository->countCtreByDate(\config('enums.status.em_aberto'), $params);
        $ctre['expirados'] = $this->ctreRepository->countCtreByDate(\config('enums.status.expirado'), $params);
        $ctre['finalizados'] = $this->ctreRepository->countCtreByDate(\config('enums.status.finalizado'), $params);
        $ctre['expirados_finalizados'] = $this->ctreRepository->countCtreByDate(\config('enums.status.expirado_finalizado'), $params);

        return response()->json(['ctre' => $ctre]);
    }

    /**
     * Método que retorna o total de CTRE lançado por período. Alimenta os indicadores da home do painel fiscal
     *
     * @param string $periodo
     * @param string $dataInicio
     * @param string $dataFinal
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ctrePorPeriodoHomeFiscalAjax($periodo = '', $dataInicio = '', $dataFinal = '')
    {
        $ctre = [];

        if ($periodo == 'custom') {

            $ctre['emitidos'] = $this->ctreRepository->countCtreBetweenDates(null, self::DATE_YMD, $dataInicio,
                $dataFinal);
            $ctre['abertos'] = $this->ctreRepository->countCtreBetweenDates(\config('enums.status.em_aberto'),
                self::DATE_YMD, $dataInicio, $dataFinal);
            $ctre['expirados'] = $this->ctreRepository->countCtreBetweenDates(\config('enums.status.expirado'),
                self::DATE_YMD, $dataInicio, $dataFinal);
            $ctre['finalizados'] = $this->ctreRepository->countCtreBetweenDates(\config('enums.status.finalizado'),
                self::DATE_YMD, $dataInicio, $dataFinal);

        } else {

            $params = $this->getFilterParams($periodo);
            $params['empresa_id'] = null;
            $params['empresa_tipo'] = null;

            $ctre['emitidos'] = $this->ctreRepository->countCtreByDate(null, $params);
            $ctre['abertos'] = $this->ctreRepository->countCtreByDate(\config('enums.status.em_aberto'), $params);
            $ctre['expirados'] = $this->ctreRepository->countCtreByDate(\config('enums.status.expirado'), $params);
            $ctre['finalizados'] = $this->ctreRepository->countCtreByDate(\config('enums.status.finalizado'), $params);

        }

        return response()->json(['ctre' => $ctre]);
    }

    // parametros de contagem de CTREs
    // home fiscal e transportador
    protected function getFilterParams($periodo)
    {
        $now = new Carbon();
        $params = ['date_format' => null, 'period' => null, 'comparator' => null];

        switch ($periodo) {

            case 'diario':
                $params['date_format'] = self::DATE_YMD;
                $params['period'] = $now->format('Y-m-d');
                break;
            case 'semanal':
                $params['date_format'] = self::DATE_YMD;
                $params['period'] = $now->now()->startOfWeek();
                $params['comparator'] = '>=';
                break;
            case 'mensal':
                $params['date_format'] = self::DATE_YM;
                $params['period'] = $now->format('Y-m');
                break;
            case 'trimestral':
                $params['date_format'] = self::DATE_YMD;
                $params['period'] = $now->subMonth(3)->format('Y-m-d');
                $params['comparator'] = '>=';
                break;
            case 'semestral':
                $params['date_format'] = self::DATE_YMD;
                $params['period'] = $now->subMonth(6)->format('Y-m-d');
                $params['comparator'] = '>=';
                break;
            case 'anual':
                $params['period'] = $now->format('Y');
                $params['period'] = $now->subYear(1)->format('Y');
                break;
            default:
                break;

        }
        return $params;
    }

    /**
     * Faz o download dos arquivos enviados no cadastro das empresas
     *
     * @param     $filename
     * @param int $empresaId
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($empresaId = 0, $filename)
    {
        $path = storage_path("licencas/" . $empresaId . '/' . $filename);

        if (file_exists($path)) {
            return response()->file($path);
        }
    }

    /**
     * Faz o download do boleto gerado pela API da Amlurb
     *
     * @param $filename
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadBoleto($filename)
    {
        $path = UrlConstructor::urlBoletoTaxaAmlurb($filename);

        if (File::exists($path)) {
            return response()->file($path, ['Content-Type' => 'application/pdf']);
        } else {
            $info = 'Ocorreu um problema para localizar seu boleto. Tente novamente mais tarde, e caso o problema persista, entre em contato';
            return response()->json(['info' => $info], 500);
        }
    }

    /**
     * Emite Nova Via do Boleto, Deleta os Anteriores e muda o Status da Empresa para "Aguardando Pagamento"
     *
     * @param $empresaId
     * @param ServiceBoletos $serviceBoletos
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function boletoSegundaVia($empresaId, ServiceBoletos $serviceBoletos)
    {
        $retornoAjax['status'] = false;
        $retornoAjax['error'] = '';

        $empresa = Empresa::with('empresas_x_empresas_tipos', 'empresa_informacao_complementar')->find($empresaId);

        if ($empresaId) {

            try {
                // DB::beginTransaction();
                
                $empresa->status_id = config('enums.status.pagamento_pendente');

                $dadosBoleto = [];

                $idTipoEmpresa = $empresa->empresas_x_empresas_tipos->empresas_tipo->id;
                $idTipoRamo = $empresa->empresa_informacao_complementar->tipo_ramo_atividade_id;

                if ($idTipoEmpresa != config('enums.empresas_tipo.pequeno_gerador')
                    && $idTipoEmpresa != config('enums.empresas_tipo.destino_final_reciclado')
                    && $idTipoEmpresa != config('enums.empresas_tipo.cooperativa_residuos')
                    && $idTipoRamo != config('enums.tipo_atividade.servico_de_administracao_municipal_direta')
                    && $idTipoRamo != config('enums.tipo_atividade.servico_de_administracao_municipal_indireta')) {
                    $parametrosBoleto = ParametrosBoleto::preencheDadosEmpresa($empresa);
                    $dadosEmail = DadosEmail::segundaViaBoleto($empresa);

                    $tipo_servico = config('enums.codigo_tipo_servico.gerador');
                    if ($idTipoEmpresa == config('enums.empresas_tipo.transportador')) {
                        $tipo_servico = config('enums.codigo_tipo_servico.transbordo');
                        if ($idTipoRamo == config('enums.tipo_atividade.transportador_residuos'))
                            $tipo_servico = config('enums.codigo_tipo_servico.transportador');
                    } // close if ()

                    $parametrosBoleto = ParametrosBoleto::preencheTipoEmpresa($parametrosBoleto, $tipo_servico);


                    /*-+ A Funcionalidade de Exclusão dos Boletos anteriores foi apagada em comtentário abaixo, by Mr.Goose em 27 maio 19 por suspeitar que possa acarretar em problemas futuros. então, a princípio, deixarei os Boletos antigos ainda pendentes. +-*/
                    // $boletos = Boleto::where('empresa_id', $empresa->id)->get();
                    // foreach ($boletos as $boleto) {
                    //     $boleto->status_id = \config('enums.status.excluido');

                    //     $boleto->save();

                    //     $boleto->delete(); // softdelete no boleto antigo
                    // } // close foreach($boletos) - deleta os Boletos antigos.

                    // Se for no ambiente PROD gera o boleto através da API da PRODAM
                    $app_env = config('app.env');
                    if (!is_null($app_env) && in_array($app_env, ['production', 'homolog'])) {
                        $dadosBoleto = $serviceBoletos->gerarBoleto($parametrosBoleto);
                        if (empty($dadosBoleto['file'])) {
                            /*- entra aqui se tive havido problema na Geração do Boleto. -*/
                            $retornoAjax['status'] = false;
                            $retornoAjax['mensagem'] = "Problema ao Geraro Boleto. <br/> Código de Gẽração do Boleto: ".$tipo_servico."<br/> Entrar em contato com a Prodam. <br/> A página irá recarregar, aguarde...";
                            return response()->json($retornoAjax);
                        } // close if (empty($dadosBoleto['file']))
                        $boletopdffile = $dadosBoleto['file'];
                    } else { // Se for no ambiente DESENV usa um modelo fake de boleto
                        $boletopdffile = 'boleto-facil-exemplo.pdf';
                    }

                    
                    $dadosEmail['boleto'] = route('download.boleto', $boletopdffile);

                    $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($dadosEmail['emailReceptor']));

                    $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
                        "email" => 'mailgun_email:mailbox'
                    ]);
                    if ($validator->fails()) {
                        $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
                        throw new \Exception($retornoAjax['erro']);
                    }

                    Mail::to($dadosEmail['emailReceptor'])->send(new CadastroRealizado($dadosEmail));

                    $empresa->save();

                } // fecha if de verificação do Ramo e Tipo de Ramo.
                       


                if (isset($dadosBoleto) && !empty($dadosBoleto))
                    $newBoleto = AprovacaoCadastroController::salvarBoleto($dadosBoleto, $empresa->id, $parametrosBoleto['InputMessage']['Input']['CodigoTipoServico']);
                
                if (isset($newBoleto) && !empty($newBoleto->arquivoBoleto))
                    return redirect()->route('download.boleto', ['id' => $newBoleto->arquivoBoleto]);
               
                // DB::commit();

                $retornoAjax['status'] = true;
                $retornoAjax['mensagem'] = "Segunda Via enviada com sucesso. \n\n Verifique seu e-mail!";

            } catch (\Exception $e) {
                Log::error($e);
                // DB::rollback();
                $retornoAjax['mensagem'] = $e->getMessage();
            }
        } else {
            $retornoAjax['mensagem'] = "Não conseguimos encontrar esta empresa!";
        }

        return response()->json($retornoAjax);
    }
}
