<?php

namespace Amlurb\Http\Controllers;

class HomeController extends Controller
{

    public $dados_header = array(
        'titulo' => 'Amlurb'
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}
