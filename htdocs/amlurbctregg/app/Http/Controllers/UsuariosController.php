<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Http\Requests\CriarUsuarioRequest;
use Amlurb\Http\Requests\PerfilRequest;
use Amlurb\Models\Empresa;
use Amlurb\Models\User;
use Amlurb\Models\UserPerfil;
use Amlurb\Models\Perfil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Criar um novo usuário
     *
     * @param CriarUsuarioRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(CriarUsuarioRequest $request)
    {
        $retornoAjax['status'] = false;

        DB::beginTransaction();
        try {
            $data_user['name'] = $request->input('nome');
            $data_user['email'] = $request->input('email');
            $data_user['sobrenome'] = $request->input('sobrenome');
            $data_user['perfil_id'] = $request->input('permissao');
            $data_user['password'] = Hash::make($request->input('password'));
            $data_user['empresa_id'] = Auth::user()->empresa_id;
            $data_user['perfil_id'] = $request->input('permissao');
            $save_user = User::create($data_user);

            $save_user_perfil = UserPerfil::firstOrNew(array('user_id' => $save_user->id));

            if ($request->hasFile('imagem_perfil')) {
                $file = $request->file('imagem_perfil');
                $filename = substr(md5(microtime()), rand(0, 26), 3);
                $ext = $file->getClientOriginalExtension();
                $destinationPath = 'uploads/imagens_perfil/';
                $move = $file->move($destinationPath, $filename . '.' . $ext);

                if ($move) {
                    $save_user_perfil->imagem_perfil = $filename . '.' . $ext;
                }
            }

            $save_user_perfil->data_nascimento = $request->input('data_nascimento');
            $save_user_perfil->cargo = $request->input('cargo');
            $save_user_perfil->telefone = $request->input('telefone');
            $save_user_perfil->ramal = $request->input('ramal');
            $save_user_perfil->celular = $request->input('celular');
            $save_user_perfil->save();

            DB::commit();

            $retornoAjax['status'] = true;
            $retornoAjax['redirect'] = route('painel.configuracoes');

            return response()->json($retornoAjax);
        } catch (Exception $e) {
            DB::rollback();

            $retornoAjax['error'] = $e->getMessage();

            return response()->json($retornoAjax);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resou rce.
     *
     * @param User $user
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, $id)
    {
        $perfils = Perfil::with('emails')->where('empresa_id', Auth::user()->empresa_id)->pluck('nome', 'id');
        $usuario = $user->where('id', $id)->first();

        return View::make("painel.fiscal.configuracoes.modais.editarUsuarios", compact('usuario', 'perfils'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PerfilRequest $request
     * @param  int $id
     * @param $rota
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function update(PerfilRequest $request, $id, $rota)
    {
        DB::beginTransaction();
        try {
           
            $data_user['name'] = $request->input('nome');
            $data_user['sobrenome'] = $request->input('sobrenome');
            $data_user['perfil_id'] = $request->input('permissao');
            $data_user['email'] = $request->input('email');

            if (!empty($request->input('password'))) {
                $data_user['password'] = Hash::make($request->input('password'));
            }
            $user = User::find($id);
            $user->update($data_user);

            $perfil = UserPerfil::firstOrCreate(['user_id' => $user->id]);
            $perfil->data_nascimento = $request->input('data_nascimento');
            $perfil->cargo = $request->input('cargo');
            $perfil->telefone = $request->input('telefone');
            $perfil->ramal = $request->input('ramal');
            $perfil->celular = $request->input('celular');

            if ($request->hasFile('imagem_perfil')) {
                $file = $request->file('imagem_perfil');
                $filename = substr(md5(microtime()), rand(0, 26), 3);
                $ext = $file->getClientOriginalExtension();

                $destinationPath = 'uploads/imagens_perfil/';
                $move = $file->move($destinationPath, $filename . '.' . $ext);

                if ($move) {
                    $perfil->imagem_perfil = $filename . '.' . $ext;
                }
            }

            $perfil->save();

            DB::commit();

            $return = [
                'messages' => [
                    'type' => 'success',
                    'message' => 'Dados Atualizados com sucesso!'
                ]
            ];
        } catch (Exception $e) {
            DB::rollback();
            $return = [
                'messages' => [
                    'type' => 'danger',
                    'message' => 'Ocorreu um erro ao editar os dados. Tente novamente.' .
                    ' Se o erro persistir, entre em contato com os administradores do sistema.'
                ]
            ];
        }

        return redirect()->route($rota)->with($return);
    }

    public function updateFromConfiguracoesEmpresa(Request $request)
    {
        $messages = [
            'nome.required' => 'Nome do responsável não pode ser vazio',
            'email.required' => 'Você não pode deixar o email de login vazio.',
            'email.email' => 'Email inválido'
        ];
        $validator = validator($request->all(), [
            'nome' => 'required',
            'email' => 'required|email'
        ], $messages);

        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[$k] = $validator->errors()->first($k);
            }

            return response()->json(['error' => $form_error]);
        }

        try {

            DB::beginTransaction();

            // update user
            $user = User::find(Auth::user()->id);
            $user->name = $request->input('nome');
            if (!empty($request->input('password'))) {
                $user->password = Hash::make($request->input('password'));
            }
            $user->save();

            $empresa = Empresa::find(Auth::user()->empresa_id);
            $empresa->cargo = $request->input('cargo');
            $empresa->celular = $request->input('celular');
            $empresa->telefone_responsavel = $request->input('telefone_responsavel');
            $empresa->ramal = $request->input('ramal');
            $empresa->save();

            DB::commit();
            return response()->json([
                'success' => 'Dados atualizados',
                'error' => []
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return redirect()->back()->with([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.'
            ]);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            User::where('id', $id)->where('empresa_id', Auth::user()->empresa_id)->delete();

            DB::commit();

            return redirect()->route('painelconfiguracoes')->with(array(
                'context' => 'success',
                'message' => 'Usuário deletado com sucesso!'
            ));
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->route('painel.configuracoes')->with(array(
                'context' => 'danger',
                'message' => 'Ocorreu um erro ao deletar o usuário, tente novamente',
            ));
        }
    }
}
