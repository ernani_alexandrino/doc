<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Http\Controllers\Gerenciador\PainelController;

use Amlurb\Models\Cidade;
use Amlurb\Models\Estado;
use Amlurb\Models\RamoAtividade;
use Amlurb\Models\Residuo;
use Amlurb\Models\EmpresaDocumentos;
use Amlurb\Models\EmpresaResponsavel;
use Illuminate\Support\Facades\DB;
use Amlurb\Models\TipoRamoAtividade;
use Amlurb\Services\DataTables\CtresRenderer;
use Illuminate\Support\Facades\Auth;

class DestinoFinalController extends PainelController
{
    public $titulo = [
        'principal' => ' Destino Final',
        'subtitulo' => 'Painel de Controle'
    ];

    public function index()
    {
        $titulo = (object) $this->titulo;
        return view('painel.destino-final.home', compact('titulo'));
    }

    // transportadores
    public function clientesFornecedores()
    {
        $this->titulo['subtitulo'] = 'Clientes/Fornecedores';
        $titulo = (object)$this->titulo;

        $residuos = Residuo::all();

        return view('painel.destino-final.clientes-fornecedores', compact(
            'titulo', 'residuos'));
    }

    public function listCtreRecebidos()
    {
        return CtresRenderer::renderDestinoFinalRecebidos($this->ctreRepository->getCtreRecebidos(
            $this->user->empresa_id, \config('enums.empresas_tipo.destino_final_reciclado')));
    }

    public function meuCadastro()
    {
        $this->titulo['subtitulo'] = 'Meu Cadastro';
        $titulo = (object)$this->titulo;

        $empresa = Auth::user()->empresa;
        
        $documentos = array();
        $getDocumentos = EmpresaDocumentos::where('empresa_id', $empresa->id)->get();
        foreach ($getDocumentos as $value) {
            $documentos[$value->documentos->nome] = $value->caminho_arquivo;

            if (!empty($value->data_emissao))
                $documentos[$value->documentos->nome.'_emissao'] = $value->data_emissao;
            if (!empty($value->data_vencimento))
                $documentos[$value->documentos->nome.'_vencimento'] = $value->data_vencimento;

            if (strpos($value->documentos->nome, 'licenca_')){
                $documentos['licencadefault'] = $value->documentos->id;
                $documentos['licenca'] = $value->caminho_arquivo;
                $documentos['licenca_emissao'] = $value->data_emissao;
                $documentos['licenca_vencimento'] = $value->data_vencimento;
            }
        }
       
        $empreendimentoSocioPrincial  = EmpresaResponsavel::where('empresa_id', $empresa->id)->where('responsavel', 'socio_principal')->get()->last();
        $empreendimentoSocios  = EmpresaResponsavel::where('empresa_id', $empresa->id)->where('responsavel', 'socio')->get();
        $socios = [];
        $i =2;
        foreach ($empreendimentoSocios as $socio) {
            $socios['id_socio_'.$i] = $socio->id;
            $socios['nome_socio_'.$i] = $socio->nome;
            $socios['numero_rg_socio_'.$i] = $socio->rg;
            $socios['numero_cpf_socio_'.$i] = $socio->cpf;

            $i++;
        }
        $ramoAtividade = RamoAtividade::all()->pluck('nome', 'id');
        $tipoRamoAtividade = TipoRamoAtividade::all()->pluck('nome', 'id');
        $estados = Estado::all()->pluck('nome', 'id');
        $cidades = Cidade::all()->pluck('nome', 'id');

        return view('painel.destino-final.meu-cadastro',
            compact('titulo', 'empresa', 'ramoAtividade', 'tipoRamoAtividade', 'estados', 'cidades', 'documentos', 'empreendimentoSocioPrincial', 'socios')
        );
    }
}
