<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Models\Qrcode;
use Amlurb\Models\QrcodeEmpresas;
use Amlurb\Models\QrcodeEquipamentos;
use Amlurb\Models\QrcodeVeiculos;
use Amlurb\Repositories\EmpresaRepository;
use Amlurb\Repositories\EmpresasXEquipamentoRepository;
use Amlurb\Services\GeradorCodigos;
use Amlurb\Services\QrCodeFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class QrcodeController extends Controller
{

    const MSG_PERMISSAO = 'Você não tem permissão de acessar este QR Code';

    /**
     * @var EmpresaRepository
     */
    private $empresasRepository;
    /**
     * @var EmpresasXEquipamentoRepository
     */
    private $empresasXequipamentoRepository;

    public function __construct()
    {
        $this->empresasRepository = new EmpresaRepository();
        $this->empresasXequipamentoRepository = new EmpresasXEquipamentoRepository();
    }

    /**
     * Método teste para os QrCodes
     * retorna qr code por id informado
     */
    public function qrCode($id)
    {
        $qrcode = Qrcode::whereId($id)->first();
        if (!$qrcode) {
            return 'este id não tem QR Code';
        }
        return $qrcode->codigo;
    }

    // metodo teste para gerar QR Code de empresas
    public function testeGerarQrCodeEmpresa($empresa_id)
    {
        $codigo = GeradorCodigos::geraQrCodeEmpresa($empresa_id);
        $qrcode = Qrcode::firstOrCreate([
            'tipo' => 'empresa',
            'codigo' => $codigo,
            'status_id' => \config('enums.status.ativo')
        ]);
        QrcodeEmpresas::firstOrCreate([
            'qrcode_id' => $qrcode->id,
            'empresa_id' => $empresa_id
        ]);

        return redirect()->route('qrcode_pdf', ['id' => $qrcode->id]);
    }

    public function novoQRCodeEquipamento($id)
    {
        $empresa_equip = $this->empresasXequipamentoRepository->getEquipamentoById($id);
        if ($empresa_equip->empresa_id != Auth::user()->empresa_id) {
            return response()->json(['info' => self::MSG_PERMISSAO], 402);
        }
        $qrcode = QrcodeEquipamentos::where('equipamento_id', $id)->first();
        if ($qrcode) {
            return response()->json(['qrcode_id' => $qrcode->qrcode_id]);
        }

        try {
            DB::beginTransaction();

            $qrcode_equipamento = GeradorCodigos::geraQrCodeEquipamento($id);

            $qrcode = Qrcode::create([
                'tipo' => \config('enums.tipos_qrcode.equipamento'),
                'codigo' => $qrcode_equipamento,
                'status_id' => \config('enums.status.ativo')
            ]);
            QrcodeEquipamentos::create([
                'qrcode_id' => $qrcode->id,
                'equipamento_id' => $id
            ]);

            DB::commit();
            return response()->json([
                'qrcode_id' => $qrcode->id
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['error' => 'Ocorreu um erro interno, tente novamente.']);
        }
    }

    public function novoQRCodeVeiculo($id)
    {
        $empresa_veic = EmpresasVeiculo::whereId($id)->first();
        if ($empresa_veic->empresa_id != Auth::user()->empresa_id) {
            return response()->json(['info' => self::MSG_PERMISSAO], 402);
        }
        $qrcode = QrcodeVeiculos::where('veiculo_id', $id)->first();
        if ($qrcode) {
            return response()->json(['qrcode_id' => $qrcode->qrcode_id]);
        }

        try {
            DB::beginTransaction();

            $qrcode_veiculo = GeradorCodigos::geraQrCodeVeiculo($id);

            $qrcode = Qrcode::create([
                'tipo' => \config('enums.tipos_qrcode.veiculo'),
                'codigo' => $qrcode_veiculo,
                'status_id' => \config('enums.status.ativo')
            ]);
            QrcodeVeiculos::create([
                'qrcode_id' => $qrcode->id,
                'veiculo_id' => $id
            ]);

            DB::commit();
            return response()->json([
                'qrcode_id' => $qrcode->id
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['error' => 'Ocorreu um erro interno, tente novamente.']);
        }
    }

    /**
     * monta pdf (ou pega pdf existente) e devolve arquivo para o usuario
     * @param $id
     * @return string
     */
    public function qrcodePDF($id)
    {
        // pega QR Code e verifica se usuario esta liberado para acessar
        $qrcode = Qrcode::whereId($id)->first();
        if (!$qrcode) {
            return response()->json(['info' => self::MSG_PERMISSAO], 402);
        }
        if (!$this->verificaPermissaoQrCode($qrcode)) {
            return response()->json(['info' => self::MSG_PERMISSAO], 402);
        }

        $cnpj = '';
        if ($qrcode->qrcode_equipamentos()->exists()) {
            $cnpj = $qrcode->qrcode_equipamentos->equipamento->empresa->cnpj;
        } else {
            if ($qrcode->qrcode_veiculos()->exists()) {
                $cnpj = $qrcode->qrcode_veiculos->veiculo->empresa->cnpj;
            } else {
                $cnpj = $qrcode->qrcode_empresas->empresa->cnpj;
            }
        }
       
        $pdf_file = QrCodeFile::getPDF($id, $cnpj);
       
        return response()->file($pdf_file, ['Content-Type' => 'application/pdf']);
    }

    public function guia()
    {
        $pdf_guia = public_path('/images/faqs/guia-qrcode.pdf');
        return response()->file($pdf_guia, ['Content-Type' => 'application/pdf']);
    }

    /**
     * @param Qrcode $qrcode
     * @return bool
     */
    private function verificaPermissaoQrCode(Qrcode $qrcode)
    {
        $tipo_empresa = Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id;
        if ($tipo_empresa == config('enums.empresas_tipo.administrador')) {
            return true;
        }

        if ($qrcode->qrcode_equipamentos()->exists()) {
            $empresa_equip = $this->empresasXequipamentoRepository->getEquipamentoById($qrcode->qrcode_equipamentos->equipamento_id);
            if ($empresa_equip->empresa_id != Auth::user()->empresa_id) {
                return false;
            }
        }
        if ($qrcode->qrcode_veiculos()->exists()) {
            $empresa_veic = EmpresasVeiculo::whereId($qrcode->qrcode_veiculos->veiculo_id)->first();
            if ($empresa_veic->empresa_id != Auth::user()->empresa_id) {
                return false;
            }
        }
        if ($qrcode->qrcode_empresas()->exists()) {
            $qrcode_empresas = QrcodeEmpresas::where('qrcode_id', $qrcode->id)->first();
            if ($qrcode_empresas->empresa_id != Auth::user()->empresa_id) {
                return false;
            }
        }
        return true;
    }

}