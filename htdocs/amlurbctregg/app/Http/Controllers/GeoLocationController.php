<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresaEndereco;

use Amlurb\Services\GeradorCodigos;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GeoLocationController extends Controller
{
    // Armazena a chave API do projeto gerada no Google Developers
    private $mapsKey = 'AIzaSyCY6m_DC7HDEpQj1wRAs8RYWozAaE39guI';

    //Chave Konrrado
    //private $mapsKey = 'AIzaSyD4us0mv0g5i_wyfEVOqlu4e16lZXCiCBQ';

    public function __construct($key = null)
    {

        if (!is_null($key)) {
            $this->mapsKey = $key;
        }

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();

            return $next($request);
        });

    }

    /**
     * Processa o retorno da URL que é passada por parâmetro. Retorno que vem da API Google Maps Geocoding do google.
     * @param $url
     * @return mixed|string
     */
    public function carregaUrl($url)
    {
        if ($url) {
            $service_url = $url;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
            curl_setopt($ch, CURLOPT_URL, $service_url);
            curl_setopt($ch, CURLOPT_FAILONERROR, false);
            curl_setopt($ch, CURLOPT_HTTP200ALIASES, (array)400);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);

            $result = curl_exec($ch);

            if (curl_error($ch)) {
                $obj = '';
            } else {
                $obj = json_decode($result);
                if ($obj->status == 'ERROR') {
                    $obj = '';
                }
            }

            curl_close($ch);

        } else {
            $obj = '';
        }

        // return response()->json(['obj' => $obj]);
        return $obj;
    }

    /**
     * Realiza a comunição com a API passando como parâmetros o Endereço que desejamos para obter a Geolocalização e a Key para acessarmos a API.
     * @param $endereco
     * @return bool
     */
    public function geoLocal($endereco)
    {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($endereco) . "&key=" . $this->mapsKey;
        $data = $this->carregaUrl($url);

        if (isset($data->status) && $data->status === 'OK') {
            return $data->results[0]->geometry->location;
        } else {
            return false;
        }
    }

    /**
     * Busca endereços das empresas, realiza todo o processo de comunicação e retorno de geolocalização, salva essas informações no banco
     * @return string
     */
    public function getEnderecoEmpresas()
    {

        ini_set('max_execution_time', -1);

        $empresas = Empresa::take(1000)
            ->where('id', '>', 16041)
            ->with('empresa_endereco:id,endereco,numero,bairro,cep')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,nome,sigla')
            ->whereHas('empresa_endereco', function ($q) {
                $q->where('latitude', null);
                $q->where('longitude', null);
            })
            ->get();
         
        $geoLocation = new GeoLocationController($this->mapsKey);

        foreach ($empresas as $indice => $empresa) {

            $endereco = $empresa->empresa_endereco->endereco . ", " . $empresa->empresa_endereco->numero . ", " . $empresa->empresa_endereco->cidade->nome . ", " . $empresa->empresa_endereco->estado->nome;

            if (!empty($geoLocation->geoLocal($endereco))) {
                $dados[$empresa->id] = $geoLocation->geoLocal($endereco);
            }
        }

        if (!empty($dados)) {
            foreach ($dados as $indice => $valor) {
                $empresaEndereco = EmpresaEndereco::where('empresa_id', $indice)->first();

                if (!empty($valor->lat)) {
                    $empresaEndereco->latitude = $valor->lat;
                }

                if (!empty($valor->lng)) {
                    $empresaEndereco->longitude = $valor->lng;
                }

                $empresaEndereco->save();
            }
        }

        return "Updates de geolocalização realizados!";
    }

    /**
     * Executa o download do arquivo Json que tem todas as informações da localização das empresas e salva no diretório storage
     * @param int $tipo
     * @return string
     */
    public function downloadJsonGeolocation($tipo = 0)
    {
        ini_set('memory_limit', '-1');

        switch ($tipo) {
            case 1:
                $fileName = 'geoLocationGG.json'; //grande gerador
                break;

            case 2:
                $fileName = 'geoLocationPG.json'; //pequeno gerador
                break;

            case 3:
                $fileName = 'geoLocationTR.json'; //transportador
                break;

            case 4:
                $fileName = 'geoLocationDF.json'; //destino final
                break;

            case 5:
                $fileName = 'geoLocationCO.json'; //cooperativa de residuos
                break;
            case 6:
                $fileName = 'geoLocationFS.json'; //fiscal
                break;
            case 7:
                $fileName = 'geoLocationAD.json'; //Administrador
                break;
            case 8:
                $fileName = 'geoLocationOP.json'; //orgão publico
                break;
            case 8:
                $fileName = 'geoLocationSS.json'; //serviço de saude
                break;
            case 9:
                $fileName = 'geoLocationSS.json'; //serviço de saude
                break;
            case 10:
                $fileName = 'geoLocationCM.json'; //condominio misto
                break;
        }

        $dados = DB::table('empresa_endereco')
            ->join('empresas', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->join('cidades', 'empresa_endereco.cidade_id', '=', 'cidades.id')
            ->join('estados', 'empresa_endereco.estado_id', '=', 'estados.id')
            ->join('empresas_x_empresas_tipo', 'empresas.id', '=', 'empresas_x_empresas_tipo.empresa_id')
            ->select('empresas.id as id_empresas', 'empresas.razao_social', 'empresas.nome_comercial',
                'empresas.id_limpurb', 'empresas.cnpj',
                'cidades.nome as nome_cidades',
                'estados.sigla as sigla_estados',
                'empresa_endereco.endereco', 'empresa_endereco.numero', 'empresa_endereco.bairro',
                'empresa_endereco.cep', 'empresa_endereco.latitude', 'empresa_endereco.longitude')
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', $tipo)
            ->get();

        //Linux
        $path = storage_path('geolocation/jsonfile/');
        if (!File::exists($path)) {
            File::makeDirectory($path, 0755, true);
        }
        File::put($path . $fileName, json_encode($dados));

        return "Arquivo Json gerado com sucesso!";
    }

    /**
     * Realiza a leitura do arquivo Json com todas as localizações e retorna para o javascript montar os marcadores no mapa
     * mapaMarkerclusterer.js function initMap() ajax
     * @return \Illuminate\Http\JsonResponse
     */
    public function readJsonGeolocation($tipoEmpresa = 0, $filter = '', $termoPesquisado = '')
    {
        
        $termoPesquisado = str_replace("*", "/", $termoPesquisado);

        switch ($tipoEmpresa) {
            case 1:
                $fileJsonTipoEmpresa = 'geoLocationGG.json'; //grande gerador
                break;

            case 2:
                $fileJsonTipoEmpresa = 'geoLocationPG.json'; //pequeno gerador
                break;

            case 3:
                $fileJsonTipoEmpresa = 'geoLocationTR.json'; //transportador
                break;

            case 4:
                $fileJsonTipoEmpresa = 'geoLocationDF.json'; //destino final
                break;

            case 5:
                $fileJsonTipoEmpresa = 'geoLocationCO.json'; //cooperativa de residuos
                break;
            case 6:
                $fileJsonTipoEmpresa = 'geoLocationFS.json'; //fiscal
                break;
            case 7:
                $fileJsonTipoEmpresa = 'geoLocationAD.json'; //Administrador
                break;
            case 8:
                $fileJsonTipoEmpresa = 'geoLocationOP.json'; //orgão publico
                break;
            case 8:
                $fileJsonTipoEmpresa = 'geoLocationSS.json'; //serviço de saude
                break;
            case 9:
                $fileJsonTipoEmpresa = 'geoLocationSS.json'; //serviço de saude
                break;
            case 10:
                $fileJsonTipoEmpresa = 'geoLocationCM.json'; //condominio misto
                break;
        }

        $infEmpresas = json_decode(file_get_contents(storage_path('geolocation/jsonfile/' . $fileJsonTipoEmpresa)));

        if ($filter != '') {
            foreach ($infEmpresas as $key => $dado) {

                switch ($tipoEmpresa) {
                    case 1:
                        $dado->link = '/painel/fiscal/cadastro-grande-gerador/' . $dado->id_empresas;
                        break;
                    case 2:
                        $dado->link = '/painel/fiscal/cadastro-pequeno-gerador/' . $dado->id_empresas;
                        break;
                    case 3:
                        $dado->link = '/painel/fiscal/cadastro-transportadores/' . $dado->id_empresas;
                        break;
                    case 4:
                        $dado->link = '/painel/fiscal/cadastro-destinos/' . $dado->id_empresas;
                        break;
                    case 5:
                        $dado->link = '/painel/fiscal/cadastro-cooperativa/' . $dado->id_empresas;
                        break;
                    case 8:
                        $dado->link = '/painel/fiscal/cadastro-orgao-publico/' . $dado->id_empresas;
                        break;
                    case 9:
                        $dado->link = '/painel/fiscal/cadastro-servico-saude/' . $dado->id_empresas;
                        break;
                    case 11:
                        $dado->link = '/painel/fiscal/cadastro-grande-gerador-condominio/' . $dado->id_empresas;
                        break;
                }

                if ($filter == 'id-limpurb') {
                    $position = stripos($dado->id_limpurb, $termoPesquisado);

                    if ($position === false) {
                        unset($infEmpresas[$key]);
                    }
                } else {
                    if ($filter == 'nome-comercial') {
                        $position = stripos($dado->nome_comercial, $termoPesquisado);

                        if ($position === false) {
                            unset($infEmpresas[$key]);
                        }
                    } else {
                        if ($filter == 'razao-social') {
                            $position = stripos($dado->razao_social, $termoPesquisado);

                            if ($position === false) {
                                unset($infEmpresas[$key]);
                            }
                        } else {
                            if ($filter == 'cnpj') {
                                $termoPesquisado = cnpjToInteger($termoPesquisado);
                                $position = stripos($dado->cnpj, $termoPesquisado);

                                if ($position === false) {
                                    unset($infEmpresas[$key]);
                                }
                            } else {
                                if ($filter == 'bairro') {
                                    $position = stripos($dado->bairro, $termoPesquisado);

                                    if ($position === false) {
                                        unset($infEmpresas[$key]);
                                    }
                                }
                            }
                        }
                    }
                }

            }

            // Retirando todas as keys para que o open do markercluesterer possa funcionar no mapa filtrado
            $infEmpresas = array_values($infEmpresas);
        } else {
            foreach ($infEmpresas as $key => $dado) {

                switch ($tipoEmpresa) {
                    case 1:
                        $dado->link = '/painel/fiscal/cadastro-grande-gerador/' . $dado->id_empresas;
                        break;
                    case 2:
                        $dado->link = '/painel/fiscal/cadastro-pequeno-gerador/' . $dado->id_empresas;
                        break;
                    case 3:
                        $dado->link = '/painel/fiscal/cadastro-transportadores/' . $dado->id_empresas;
                        break;
                    case 4:
                        $dado->link = '/painel/fiscal/cadastro-destinos/' . $dado->id_empresas;
                        break;
                    case 5:
                        $dado->link = '/painel/fiscal/cadastro-cooperativa/' . $dado->id_empresas;
                        break;
                    case 8:
                        $dado->link = '/painel/fiscal/cadastro-orgao-publico/' . $dado->id_empresas;
                        break;
                    case 9:
                        $dado->link = '/painel/fiscal/cadastro-servico-saude/' . $dado->id_empresas;
                        break;
                    case 11:
                        $dado->link = '/painel/fiscal/cadastro-grande-gerador-condominio/' . $dado->id_empresas;
                        break;
                }
            }
        }

        $infEmpresas = json_encode($infEmpresas);

        return response()->json($infEmpresas);
    }

    /**
     * Gera código Amlurb em massa para cadastros que não contém o código Amlurb
     */
    public function codAmlurb()
    {

        ini_set('max_execution_time', -1);

        $empresas = EmpresasXEmpresasTipo::skip(0)->take(400)->with('empresa:id,id_limpurb')->select('id', 'empresa_id',
            'empresa_tipo_id')->where('empresa_tipo_id', 3)->get();

        foreach ($empresas as $empresa) {

            if (empty($empresa->empresa->id_limpurb)) {
                $codigo_ctre = GeradorCodigos::geraCodigoEmpresa(config('enums.empresas_sigla.transportador'));

                $empresaSalvar = Empresa::find($empresa->empresa_id);
                $empresaSalvar->id_limpurb = $codigo_ctre;
                $empresaSalvar->save();
            }

        }

        pd($empresas->toArray());
    }

}
