<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Http\Controllers\Gerenciador\PainelController;
use Amlurb\Models\Cidade;
use Amlurb\Models\ColaboradoresNumero;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresaDocumentos;
use Amlurb\Models\EnergiaConsumo;
use Amlurb\Models\EstabelecimentoTipo;
use Amlurb\Models\Estado;
use Amlurb\Models\FrequenciaColeta;
use Amlurb\Models\FrequenciaGeracao;
use Amlurb\Models\RamoAtividade;
use Amlurb\Models\TipoRamoAtividade;
use Illuminate\Support\Facades\Auth;

class PequenoGeradorController extends PainelController
{
    public $titulo = [
        'principal' => 'Pequeno Gerador',
        'subtitulo' => 'Painel de Controle'
    ];

    public function index()
    {
        $titulo = (object)$this->titulo;
        $empresa = Empresa::whereId(Auth::user()->empresa_id)->first();

        $documentos = array();
        $getDocumentos = EmpresaDocumentos::where('empresa_id', $empresa->id)->get();
        foreach ($getDocumentos as $value) {
            $documentos[$value->documentos->nome] = $value->caminho_arquivo;

            if (!empty($value->data_emissao))
                $documentos[$value->documentos->nome.'_emissao'] = $value->data_emissao;
            if (!empty($value->data_vencimento))
                $documentos[$value->documentos->nome.'_vencimento'] = $value->data_vencimento;

            if (strpos($value->documentos->nome, 'licenca_')){
                $documentos['licencadefault'] = $value->documentos->id;
                $documentos['licenca'] = $value->caminho_arquivo;
                $documentos['licenca_emissao'] = $value->data_emissao;
                $documentos['licenca_vencimento'] = $value->data_vencimento;
            }
        }

        $ramoAtividade = RamoAtividade::all()->pluck('nome', 'id');
        $tipoRamoAtividade = TipoRamoAtividade::all()->pluck('nome', 'id');
        $estados = Estado::all()->pluck('nome', 'id');
        $cidades = Cidade::all()->pluck('nome', 'id');

        $frequencia_geracao = FrequenciaGeracao::all()->pluck('nome', 'id');
        $frequencia_coleta = FrequenciaColeta::all()->pluck('nome', 'id');
        $colaboradores_numero = ColaboradoresNumero::all()->pluck('nome', 'id');
        $energia_consumo = EnergiaConsumo::all()->pluck('nome', 'id');
        $estabelecimento_tipo = EstabelecimentoTipo::all()->pluck('nome', 'id');

        return view('painel.pequeno-gerador.home', compact('titulo', 'empresa',
            'ramoAtividade', 'tipoRamoAtividade', 'estados', 'cidades', 'frequencia_geracao', 'frequencia_coleta',
            'colaboradores_numero', 'energia_consumo', 'estabelecimento_tipo', 'documentos'
        ));
    }
}
