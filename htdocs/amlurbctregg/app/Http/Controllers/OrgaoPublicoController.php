<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Http\Controllers\Gerenciador\PainelController;
use Amlurb\Models\Cidade;
use Amlurb\Models\ColaboradoresNumero;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresaDocumentos;
use Amlurb\Models\EnergiaConsumo;
use Amlurb\Models\Equipamento;
use Amlurb\Models\EstabelecimentoTipo;
use Amlurb\Models\Estado;
use Amlurb\Models\FrequenciaColeta;
use Amlurb\Models\FrequenciaGeracao;
use Amlurb\Models\RamoAtividade;
use Amlurb\Models\Residuo;
use Amlurb\Models\TipoRamoAtividade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrgaoPublicoController extends PainelController
{
    public $titulo = [
        'principal' => 'Órgão Público',
        'subtitulo' => 'Painel de Controle'
    ];

    public function index()
    {
        $titulo = (object)$this->titulo;
        $transportadores = $this->empresaRepository->getTransportadoresVinculados();

        return view('painel.orgao-publico.home', compact(
            'titulo','transportadores'));
    }

    public function meusEquipamentos()
    {
        $this->titulo['subtitulo'] = 'Meus Equipamentos';
        $titulo = (object)$this->titulo;

        $equipamentos = Equipamento::with('tipo_equipamento')->get();
        return view('painel.orgao-publico.equipamentos', compact(
            'titulo', 'equipamentos'));
    }

    public function clientesFornecedores()
    {
        $this->titulo['subtitulo'] = 'Clientes/Fornecedores';
        $titulo = (object)$this->titulo;

        $frequenciaColeta = FrequenciaColeta::all();
        // so traz equipamentos que ainda nao foram vinculados a nenhum gerador
        $equipamentos = $this->empresasXequipamentoRepository->getEquipamentosNaoAlocadosByIdEmpresa(Auth::user()->empresa_id);
        $residuos = Residuo::all();

        $vinculo_condominio = $this->empresaRepository->getCondominioMistoVinculado(Auth::user()->empresa_id);

        return view('painel.orgao-publico.clientes-fornecedores', compact(
            'titulo', 'frequenciaColeta', 'equipamentos', 'residuos', 'vinculo_condominio'
        ));
    }

    public function meuCadastro()
    {
        $this->titulo['subtitulo'] = 'Meu Cadastro';
        $titulo = (object)$this->titulo;

        $empresa = Empresa::whereId(Auth::user()->empresa_id)->first();

        $documentos = array();
        $getDocumentos = EmpresaDocumentos::where('empresa_id', $empresa->id)->get();
        foreach ($getDocumentos as $value) {
            $documentos[$value->documentos->nome] = $value->caminho_arquivo;
        }

        $ramoAtividade = RamoAtividade::all()->pluck('nome', 'id');
        $tipoRamoAtividade = TipoRamoAtividade::all()->pluck('nome', 'id');
        $estados = Estado::all()->pluck('nome', 'id');
        $cidades = Cidade::all()->pluck('nome', 'id');

        $frequencia_geracao = FrequenciaGeracao::all()->pluck('nome', 'id');
        $frequencia_coleta = FrequenciaColeta::all()->pluck('nome', 'id');
        $colaboradores_numero = ColaboradoresNumero::all()->pluck('nome', 'id');
        $energia_consumo = EnergiaConsumo::all()->pluck('nome', 'id');
        $estabelecimento_tipo = EstabelecimentoTipo::all()->pluck('nome', 'id');

        return view('painel.orgao-publico.meu-cadastro', compact('titulo', 'empresa',
            'ramoAtividade', 'tipoRamoAtividade', 'estados', 'cidades', 'frequencia_geracao', 'frequencia_coleta',
            'colaboradores_numero', 'energia_consumo', 'estabelecimento_tipo', 'documentos'
        ));
    }
}
