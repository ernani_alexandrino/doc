<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Http\Controllers\Gerenciador\PainelController;

use Amlurb\Http\Requests\CadastroFilialRequest;

use Amlurb\Models\EmpresaDocumentos;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\EmpresasXEquipamento;
use Amlurb\Models\Vinculos;
use Amlurb\Repositories\CtreRepository;
use Amlurb\Services\FiscalRenderer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Amlurb\Models\JustificativaInconsistenciaVeiculos;
use Amlurb\Models\Justificativa;
use Amlurb\Models\BoletoVeiculos;
use Amlurb\Models\Boleto;
use Log;
use Auth;
use Carbon\Carbon;

class FiscalController extends PainelController
{
    public $titulo = array(
        'principal' => 'Amlurb',
        'subtitulo' => 'Painel de Controle'
    );

    public $tables = [];

    public function index()
    {
        $titulo = (object)$this->titulo;

        $ctreEmitidos = $this->ctreRepository->getCtreEmitidos(null, 10);

        $ctreIndicadores = json_decode(
            json_encode(
                $this->ctrePorPeriodoHomeFiscalAjax(
                    '',
                    null,
                    null
                ),
                true
            )
        )->original->ctre;
        $cadastrosIndicadores = json_decode(json_encode($this->cadastrosPorPeriodo(''), true))->original->cadastros;

        return view('painel.fiscal.home', compact('titulo', 'ctreEmitidos', 'ctreIndicadores', 'cadastrosIndicadores'));
    }

    public function rastreabilidade()
    {
        $this->titulo['subtitulo'] = 'Rastreabilidade';
        $titulo = (object)$this->titulo;

        return view('painel.fiscal.rastreabilidade', compact('titulo'));
    }

    public function rastreabilidadeAjax()
    {
        $ctreRepository = new CtreRepository();
        $ctres = $ctreRepository->ultimosCTREs();

        return FiscalRenderer::renderFiscalRastreabilidade($ctres);
    }

    /**
     * Faz a busca de cadastros por período.
     * Por padrão ele busca todos os dadaos, sem aplicar filtro de período.
     *
     * @param string $periodo
     * @param string $dataInicial
     * @param string $dataFinal
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cadastrosPorPeriodo($periodo = '', $dataInicial = '', $dataFinal = '')
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        $now = new Carbon();

        $geradores = Empresa::join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', config('enums.empresas_tipo.grande_gerador'));

        $peqGeradores = Empresa::join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', config('enums.empresas_tipo.pequeno_gerador'));

        $transportadores = Empresa::join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', config('enums.empresas_tipo.transportador'));

        $destinos = Empresa::join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', config('enums.empresas_tipo.destino_final_reciclado'));

        $cooperativas = Empresa::join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', config('enums.empresas_tipo.cooperativa_residuos'));

        $servicos = Empresa::join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', config('enums.empresas_tipo.servico_saude'));

        $orgaos = Empresa::join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->where('empresas_x_empresas_tipo.empresa_tipo_id', config('enums.empresas_tipo.orgao_publico'));

        $equipamentos = EmpresasXEquipamento::where('status_id', config('enums.status.ativo'));
        $veiculos = EmpresasVeiculo::where('status_id', config('enums.status.ativo'));

        switch ($periodo) {
            case 'diario':
                $geradores->where(DB::raw("DATE_FORMAT(empresas.created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                $peqGeradores->where(DB::raw("DATE_FORMAT(empresas.created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                $transportadores->where(DB::raw("DATE_FORMAT(empresas.created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                $destinos->where(DB::raw("DATE_FORMAT(empresas.created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                $cooperativas->where(DB::raw("DATE_FORMAT(empresas.created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                $servicos->where(DB::raw("DATE_FORMAT(empresas.created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                $orgaos->where(DB::raw("DATE_FORMAT(empresas.created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                $equipamentos->where(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                $veiculos->where(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), $now->format('Y-m-d'));
                break;

            case 'semanal':
                $geradores->where('empresas.created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                $peqGeradores->where('empresas.created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                $transportadores->where('empresas.created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                $destinos->where('empresas.created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                $cooperativas->where('empresas.created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                $servicos->where('empresas.created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                $orgaos->where('empresas.created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                $equipamentos->where('created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                $veiculos->where('created_at', '>=', $now->now()->startOfWeek()->format('Y-m-d'));
                break;

            case 'mensal':
                $geradores->whereMonth('empresas.created_at', $now->format('m'))
                    ->whereYear('empresas.created_at', $now->format('Y'));
                $peqGeradores->whereMonth('empresas.created_at', $now->format('m'))
                    ->whereYear('empresas.created_at', $now->format('Y'));
                $transportadores->whereMonth('empresas.created_at', $now->format('m'))
                    ->whereYear('empresas.created_at', $now->format('Y'));
                $destinos->whereMonth('empresas.created_at', $now->format('m'))
                    ->whereYear('empresas.created_at', $now->format('Y'));
                $cooperativas->whereMonth('empresas.created_at', $now->format('m'))
                    ->whereYear('empresas.created_at', $now->format('Y'));
                $servicos->whereMonth('empresas.created_at', $now->format('m'))
                    ->whereYear('empresas.created_at', $now->format('Y'));
                $orgaos->whereMonth('empresas.created_at', $now->format('m'))
                    ->whereYear('empresas.created_at', $now->format('Y'));
                $equipamentos->whereMonth('created_at', $now->format('m'))
                    ->whereYear('created_at', $now->format('Y'));
                $veiculos->whereMonth('created_at', $now->format('m'))
                    ->whereYear('created_at', $now->format('Y'));
                break;

            case 'trimestral':
                $geradores->where('empresas.created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                $peqGeradores->where('empresas.created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                $transportadores->where('empresas.created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                $destinos->where('empresas.created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                $cooperativas->where('empresas.created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                $servicos->where('empresas.created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                $orgaos->where('empresas.created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                $equipamentos->where('created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                $veiculos->where('created_at', '>=', $now->subMonth(3)->format('Y-m-d'));
                break;

            case 'semestral':
                $geradores->where('empresas.created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                $peqGeradores->where('empresas.created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                $transportadores->where('empresas.created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                $destinos->where('empresas.created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                $cooperativas->where('empresas.created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                $servicos->where('empresas.created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                $orgaos->where('empresas.created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                $equipamentos->where('created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                $veiculos->where('created_at', '>=', $now->subMonth(6)->format('Y-m-d'));
                break;

            case 'anual':
                $geradores->where('empresas.created_at', '>=', $now->subYear()->format('Y-m-d'));
                $peqGeradores->where('empresas.created_at', '>=', $now->subYear()->format('Y-m-d'));
                $transportadores->where('empresas.created_at', '>=', $now->subYear()->format('Y-m-d'));
                $destinos->where('empresas.created_at', '>=', $now->subYear()->format('Y-m-d'));
                $cooperativas->where('empresas.created_at', '>=', $now->subYear()->format('Y-m-d'));
                $servicos->where('empresas.created_at', '>=', $now->subYear()->format('Y-m-d'));
                $orgaos->where('empresas.created_at', '>=', $now->subYear()->format('Y-m-d'));
                $equipamentos->where('created_at', '>=', $now->subYear()->format('Y-m-d'));
                $veiculos->where('created_at', '>=', $now->subYear()->format('Y-m-d'));
                break;

            case 'custom':
                $geradores->whereBetween('empresas.created_at', [$dataInicial, $dataFinal]);
                $peqGeradores->whereBetween('empresas.created_at', [$dataInicial, $dataFinal]);
                $transportadores->whereBetween('empresas.created_at', [$dataInicial, $dataFinal]);
                $destinos->whereBetween('empresas.created_at', [$dataInicial, $dataFinal]);
                $cooperativas->whereBetween('empresas.created_at', [$dataInicial, $dataFinal]);
                $servicos->whereBetween('empresas.created_at', [$dataInicial, $dataFinal]);
                $orgaos->whereBetween('empresas.created_at', [$dataInicial, $dataFinal]);
                $equipamentos->whereBetween('created_at', [$dataInicial, $dataFinal]);
                $veiculos->whereBetween('created_at', [$dataInicial, $dataFinal]);
                break;
        }

        $totalCadastros = [
            'geradores' => $geradores->count(),
            'transportadores' => $transportadores->count(),
            'destinosFinais' => $destinos->count(),
            'pequenosGeradores' => $peqGeradores->count(),
            'cooperativas' => $cooperativas->count(),
            'servicosSaude' => $servicos->count(),
            'orgaosPublicos' => $orgaos->count(),
            'equipamentos' => $equipamentos->count(),
            'veiculos' => $veiculos->count(),
        ];

        return response()->json(['cadastros' => $totalCadastros]);
    }

    public function rastreabilidadeLogsAjax($ctreId = 0)
    {
        $ctreId = (int)$ctreId;

        if ($ctreId != 0) {

            $ctre = $this->ctreRepository->getCtreCompleto($ctreId);
            $ctre->empresa->razao_social = str_limit($ctre->empresa->razao_social, 27, '...');
            $ctre->tempoCtre = $ctre->data_expiracao->diffInHours();
            $ctre->hora_emissao = $ctre->data_emissao->format('d/m/Y - H:i:s');
            if (!is_null($ctre->data_validacao)) {
                $ctre->hora_validacao = $ctre->data_validacao->format('d/m/Y - H:i:s');
            }

            switch ($ctre->status_id) {

                case \config('enums.status.em_aberto'):

                    $ctre->class = 'validacao pendente';
                    $ctre->situacao = 'Pendente';
                    $ctre->classTempo = 'em-aberto';
                    $ctre->classStatusCtre = 'status status-aberto';
                    $ctre->textoStatusCtre = 'Em Aberto';
                    break;

                case \config('enums.status.expirado'):

                    $ctre->class = 'validacao aguardando';
                    $ctre->situacao = 'Aguardando';
                    $ctre->classTempo = 'expirado';
                    $ctre->classStatusCtre = 'status status-expirado';
                    $ctre->textoStatusCtre = 'Expirado';
                    break;

                case \config('enums.status.finalizado'):

                    $ctre->class = 'validacao validado';
                    $ctre->situacao = 'Validado';
                    $ctre->classTempo = 'finalizado';
                    $ctre->classStatusCtre = 'status status-finalizado';
                    $ctre->textoStatusCtre = 'Finalizado';
                    break;

            }

            return response()->json(['ctre' => $ctre]);
        } else {
            return response()->json(['error' => true]);
        }
    }


    /**
     * Cadastra os dados do responsável pela Filial e Cria a Filial.
     * Estes dados são usados para o login da Empresa Filial
     *
     * @param CadastroFilialRequest $request
     * @param int $empresaId
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     * by Mr.Goose
     * in 24 junho 2019
     */
    public function cadastroFilial(CadastroFilialRequest $request)
    {
        try {
            $retornoAjax['status'] = false;
            if (empty($request->empresa_id))
                return response()->json($retornoAjax);
                
            $empresaMatriz = Empresa::where('id', $request->empresa_id)
                ->with(['empresas_x_empresas_tipos' => function($empresaMatriz){
                    $empresaMatriz->where('status_id', config('enums.status.ativo'));
                }])
                ->with(['empresa_documentos' => function($empresaMatriz){
                    $empresaMatriz->where('documento_id', config('enums.documentos.empresa_cartao_cnpj'));
                }])
                ->first();

            session()->forget([
                'empresa',
                'responsavel',
                'informacaoComplementar',
                'condominio',
                'documentos',
                'responsavelEmpreendimento',
                'dadosEmail',
                'matriz'
            ]); // Limpa as Sessions para criá-las novamente logo abaixo.

            if (empty($empresaMatriz->empresa_informacao_complementar->ramo_atividade_id) || empty($empresaMatriz->empresa_informacao_complementar->tipo_ramo_atividade_id) || empty($empresaMatriz->empresa_informacao_complementar->frequencia_geracao_id)) {
                $retornoAjax['status'] = false;
                $retornoAjax['mensagem'] = "Esta Empresa tem Dados Faltando e por isto não é possível criar uma Filial.";
                return response()->json($retornoAjax);
            } // close if() --- Empresa faltando Dados. -- Validação by Mr.Goose em 05 jul 19


            $dados['matriz']['id']                                      = $empresaMatriz->id;
            $dados['matriz']['id_limpurb']                              = $empresaMatriz->id_limpurb;

            $dados['empresa']['cnpj']                                   = $empresaMatriz->cnpj;
            $dados['empresa']['razao_social']                           = $empresaMatriz->razao_social;
            $dados['empresa']['nome_comercial']                         = $request->nome_filial;
            $dados['empresa']['ramo_atividade']                         = $empresaMatriz->empresa_informacao_complementar->ramo_atividade_id;
            $dados['empresa']['tipo_atividade']                         = $empresaMatriz->empresa_informacao_complementar->tipo_ramo_atividade_id;
            $dados['informacaoComplementar']['frequencia_geracao_id']   = $empresaMatriz->empresa_informacao_complementar->frequencia_geracao_id;
            $dados['responsavel']['responsavel']                        = $request->nome_responsavel_filial;
            $dados['responsavel']['email']                              = $request->email;

            $dados['empresa']['estado']                                 = $empresaMatriz->empresa_endereco->estado_id;
            $dados['empresa']['cidade']                                 = $empresaMatriz->empresa_endereco->cidade_id;

            // Abaixo as Variáveis que não serão utilizadas neste momento da Criação da Filial, mas que devem ser passadas para Salvamento correto dos Dados.
            $dados['informacaoComplementar']['frequencia_coleta_id']    = 
            $dados['informacaoComplementar']['colaboradores_numero_id'] = 
            $dados['informacaoComplementar']['energia_consumo_id']      = 
            $dados['informacaoComplementar']['estabelecimento_tipo_id'] = 
            $dados['empresa']['ie']                                     =
            $dados['empresa']['im']                                     =
            $dados['empresa']['telefone']                               =
            $dados['responsavel']['telefone_responsavel']               =
            $dados['responsavel']['ramal']                              =
            $dados['responsavel']['celular_responsavel']                =
            $dados['responsavel']['cargo']                              = null;
            $dados['informacaoComplementar']['area_total']              = 
            $dados['empresa']['numero']                                 =
            $dados['informacaoComplementar']['area_construida']         = 0;
            $dados['empresa']['endereco']                               =
            $dados['empresa']['cep']                                    =
            $dados['empresa']['bairro']                                 =
            $dados['empresa']['complemento']                            =
            $dados['empresa']['ponto_referencia']                       = '';

            // Abaixo Dados que serão enviados no E-mail::
            // $dados['dadosEmail']['tipo_empresa_vinculada']  = 
            // $dados['dadosEmail']['tipo_empresa']            = $empresaMatriz->empresas_x_empresas_tipos->first()->empresas_tipo->nome;
            $dados['dadosEmail']['vinculador_razao_social'] = $empresaMatriz->razao_social;
            $dados['dadosEmail']['responsavel']             = $request->nome_responsavel_filial;
            $dados['dadosEmail']['login']                   = $request->email;
            $dados['dadosEmail']['vinculador_cnpj']         = $empresaMatriz->cnpj;
            $dados['dadosEmail']['vinculador_email']        = $empresaMatriz->email;
            $dados['dadosEmail']['id_limpurb']              = $empresaMatriz->id_limpurb;
            // $dados['dadosEmail']['vinculador_telefone']     = $empresaMatriz->telefone;
            // $dados['dadosEmail']['vinculador_responsavel']  = $empresaMatriz->nome_responsavel;
            $dados['dadosEmail']['empresas_tipo_id']        = $empresaMatriz->empresas_x_empresas_tipos->first()->empresas_tipo->id;

            session($dados);

            // $dadosEmpresa = session('empresa');
            // $dadosResponsavel = session('responsavel');
            // $dadosComplementares = session('informacaoComplementar');

            $filial = CadastroEmpresaController::store($request, true); # Cadastra a Filial Propriamente dita.
            if (!empty($filial) && !empty(($empresaMatriz->empresa_documentos[0]->caminho_arquivo))) {
                // Aqui copia o Arquivo de CNPJ e tmb salva no BD.
                $filePath = 'licencas/'.$request->empresa_id.'/'.$empresaMatriz->empresa_documentos[0]->caminho_arquivo;
                $filePathMove = 'licencas/'.$filial.'/'.$empresaMatriz->empresa_documentos[0]->caminho_arquivo;
                Storage::disk('s3')->copy($filePath, $filePathMove);

                $arrayDados = array(
                    'empresa_id' => $filial,
                    'documento_id' => config('enums.documentos.empresa_cartao_cnpj'),
                    'caminho_arquivo' => $empresaMatriz->empresa_documentos[0]->caminho_arquivo
                );
                EmpresaDocumentos::firstOrCreate($arrayDados);
            } // close if (!empty($filial) && !empty(($empresaMatriz->empresa_documentos[0]->caminho_arquivo)))

            $retornoAjax['status'] = true;
            $retornoAjax['mensagem'] = "A Filial foi criada com sucesso. \n\n A página irá recarregar, aguarde...";
            return response()->json($retornoAjax);
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao Criar uma Filial");
        }
    }


    /**
     * Item Cadastro -> Grandes Geradores do menu fiscal.
     *
     * @param int $empresaId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cadastroGg($empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Grandes Geradores';
        $titulo = (object)$this->titulo;
        $inconsistencias = [];
        $informacoesFinanceiras = [];
        $ctre = ($empresaId) ?
            (object)$this->ctrePorPeriodoFiscalAjax('', $empresaId) : [];
        $dadosCadastrais = ($empresaId) ? $this->empresaRepository->getDadosCadastraisBasicos($empresaId) : [];

        /*- abaixo, inserido by Mr.Goose em 03 jul 19 afim de trazer os Docs da Empresa. -*/
        $documentos = array();
        if (!empty($empresaId)) {
            $getDocumentos = EmpresaDocumentos::where('empresa_id', $empresaId)->get();
            foreach ($getDocumentos as $value) {
                $documentos[$value->documentos->nome] = $value->caminho_arquivo;
            }
        } // close if (!empty($empresaId))
        /*- acima, inserido by Mr.Goose em 03 jul 19 afim de trazer os Docs da Empresa. -*/

        return view('painel.fiscal.cadastro-gg.cadastro-gg', compact(
            'titulo','dadosCadastrais', 'inconsistencias',
            'informacoesFinanceiras', 'ctre', 'documentos'
        ));
    }

    public function cadastroPg($empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Pequenos Geradores';
        $titulo = (object)$this->titulo;
        $dadosCadastrais = ($empresaId) ? $this->empresaRepository->getDadosCadastraisBasicos($empresaId) : [];

        return view('painel.fiscal.cadastro-pg.cadastro-pg',
            compact('titulo', 'dadosCadastrais'));
    }

    public function cadastroGc($empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Condomínios Mistos';
        $titulo = (object)$this->titulo;
        $inconsistencias = [];
        $informacoesFinanceiras = [];
        $ctre = ($empresaId) ?
            (object)$this->ctrePorPeriodoFiscalAjax('', $empresaId) : [];
        $dadosCadastrais = ($empresaId) ? $this->empresaRepository->getDadosCadastraisBasicos($empresaId) : [];

        return view('painel.fiscal.cadastro-gc.cadastro-gc', compact(
            'titulo','dadosCadastrais', 'inconsistencias',
            'informacoesFinanceiras', 'ctre'
        ));
    }

    public function cadastroTr($empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Transportadores';
        $titulo = (object)$this->titulo;
        $inconsistencias = [];
        $informacoesFinanceiras = [];
        $ctre = ($empresaId) ?
            (object)$this->ctrePorPeriodoFiscalAjax('', $empresaId) : [];
        $dadosCadastrais = ($empresaId) ? $this->empresaRepository->getDadosCadastraisBasicos($empresaId) : [];

        if($dadosCadastrais){

            $documentos = array();
            $getDocumentos = EmpresaDocumentos::where('empresa_id',$empresaId)->get();

            foreach ($getDocumentos as $value) {
                $documentos[$value->documentos->nome] = $value->caminho_arquivo;

                if (!empty($value->data_emissao))
                    $documentos[$value->documentos->nome.'_data_emissao'] = $value->data_emissao;
                if (!empty($value->data_vencimento))
                    $documentos[$value->documentos->nome.'_data_vencimento'] = $value->data_vencimento;

                if (strpos($value->documentos->nome, 'licenca_')){
                    switch ($value->documentos->id) {
                        case 17:
                            $documentos['licencadefault'] = 'Licença Prévia';
                            break;

                        case 18:
                            $documentos['licencadefault'] = 'Licença de Instalação';
                            break;

                        case 19:
                            $documentos['licencadefault'] = 'Licença de Operação';
                            break;
                    }
                    $documentos['licenca'] = $value->caminho_arquivo;
                    $documentos['licenca_data_emissao'] = $value->data_emissao;
                    $documentos['licenca_data_vencimento'] = $value->data_vencimento;
                }
            }
        }

//        echo '<pre>';
//        print_r($dadosCadastrais->empresa_informacao_complementar);
//        exit;
        return view('painel.fiscal.cadastro-tr.cadastro-tr', compact(
            'titulo','dadosCadastrais', 'inconsistencias', 'documentos',
            'informacoesFinanceiras', 'ctre'
        ));
    }

    public function cadastroDf($empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Destinos Finais';
        $titulo = (object)$this->titulo;
        $inconsistencias = [];
        $informacoesFinanceiras = [];
        $ctre = ($empresaId) ?
            (object)$this->ctrePorPeriodoFiscalAjax('', $empresaId) : [];
        $dadosCadastrais = ($empresaId) ? $this->empresaRepository->getDadosCadastraisBasicos($empresaId) : [];

        return view('painel.fiscal.cadastro-df.cadastro-df', compact(
            'titulo','dadosCadastrais', 'inconsistencias',
            'informacoesFinanceiras', 'ctre'
        ));
    }

    public function cadastroCr($empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Cooperativas';
        $titulo = (object)$this->titulo;
        $dadosCadastrais = ($empresaId) ? $this->empresaRepository->getDadosCadastraisBasicos($empresaId) : [];

        return view('painel.fiscal.cadastro-cr.cadastro-cr', compact(
            'titulo','dadosCadastrais'
        ));
    }

    public function cadastroSs($empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Serviço de Saúde';
        $titulo = (object)$this->titulo;
        $dadosCadastrais = ($empresaId) ? $this->empresaRepository->getDadosCadastraisBasicos($empresaId) : [];

        return view('painel.fiscal.cadastro-ss.cadastro-ss', compact(
            'titulo','dadosCadastrais'
        ));
    }

    public function cadastroOp($empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Órgão Público';
        $titulo = (object)$this->titulo;
        $dadosCadastrais = ($empresaId) ? $this->empresaRepository->getDadosCadastraisBasicos($empresaId) : [];

        return view('painel.fiscal.cadastro-op.cadastro-op', compact(
            'titulo','dadosCadastrais'
        ));
    }

    public function listaFiliaisAjax($empresaId = null)
    {
        // return $this->queryEmpresaFiliais($empresaId);
        return FiscalRenderer::renderFiscalEmpresasFiliais(
            $this->queryEmpresaFiliais($empresaId)
        );
    }

    public function cadastroGgAjax()
    {
        return FiscalRenderer::renderFiscalCadastroEmpresas(
            $this->queryEmpresaBairroAndStatus(\config('enums.empresas_tipo.grande_gerador')));
    }

    public function cadastroPgAjax()
    {
        return FiscalRenderer::renderFiscalCadastroEmpresas(
            $this->queryEmpresaBairroAndStatus(\config('enums.empresas_tipo.pequeno_gerador')));
    }

    public function cadastroGcAjax()
    {
        return FiscalRenderer::renderFiscalGeradoresCondominios(
            $this->queryEmpresaBairroAndStatus(\config('enums.empresas_tipo.condominio_misto')));
    }

    public function cadastroTransportadorAjax()
    {
        return FiscalRenderer::renderFiscalCadastroEmpresas(
            $this->queryEmpresaBairroAndStatus(\config('enums.empresas_tipo.transportador')));
    }

    public function cadastroDfAjax()
    {
        return FiscalRenderer::renderFiscalCadastroEmpresas(
            $this->queryEmpresaBairroAndStatus(\config('enums.empresas_tipo.destino_final_reciclado')));
    }

    public function cadastroCrAjax()
    {
        return FiscalRenderer::renderFiscalCadastroEmpresas(
            $this->queryEmpresaBairroAndStatus(\config('enums.empresas_tipo.cooperativa_residuos')));
    }

    public function cadastroSsAjax()
    {
        return FiscalRenderer::renderFiscalCadastroEmpresas(
            $this->queryEmpresaBairroAndStatus(\config('enums.empresas_tipo.servico_saude')));
    }

    public function cadastroOpAjax()
    {
        return FiscalRenderer::renderFiscalCadastroEmpresas(
            $this->queryEmpresaBairroAndStatus(\config('enums.empresas_tipo.orgao_publico')));
    }

    private function queryEmpresaFiliais($empresaId)
    {   
        return Empresa::where('id_matriz', $empresaId);
    }

    private function queryEmpresaBairroAndStatus($tipo_empresa)
    {
        return EmpresasXEmpresasTipo::with('empresa.empresa_endereco:empresa_id,bairro')
            ->with('empresa.status:id,descricao')
            ->where('empresa_tipo_id', $tipo_empresa);
    }

    public function vinculosAjax($empresaId = 0)
    {
        if ($empresaId != 0) {
            $vinculos = Vinculos::where('vinculador_id', $empresaId)
                ->with('empresa_vinculada:id,id_limpurb,razao_social,cnpj')
                ->with('empresa_vinculada.status:descricao')
                ->get();

            return FiscalRenderer::renderFiscalEmpresaVinculos($vinculos);
        } else {
            return false;
        }
    }

    public function cadastroGgSearch(Request $request, $empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Grandes Geradores';
        $titulo = (object)$this->titulo;

        $filtro = $request->segment(4);
        $filtroValor = $request->segment(5);
        $dadosCadastrais= $dadosPesquisa = null;

        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        if ($empresaId != 0) {
            $dadosCadastrais = Empresa::where('id', $empresaId)
                ->with('empresa_endereco.cidade:id,nome')
                ->with('empresa_endereco.estado:id,sigla,nome')
                ->with('empresa_informacao_complementar.energia_consumo:id,nome')
                ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
                ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
                ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
                ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
                ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
                ->first();
        }
        elseif (!empty($filtro) && $filtro == 'todos') {

            $dadosPesquisa = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.grande_gerador'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

            if (!empty($filtroValor)) {
                $dadosPesquisa->where(function ($dadosPesquisa) use ($filtroValor) {
                    $dadosPesquisa->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                        ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                });                    
            }
            $dadosPesquisa->get();
        }

        return view('painel.fiscal.cadastro-gg.cadastro-gg-search', compact(
            'titulo',
            'dadosCadastrais',
            'dadosPesquisa', 
            'filtro',
            'filtroValor'
        ));
    }

    public function cadastroGgSearchAjax($filtro = '', $filtroValor = '')
    {
        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        $empresas = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.grande_gerador'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->distinct()
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

        switch ($filtro) {
            case 'id-limpurb':
                $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%");
                break;

            case 'nome-comercial':
                $empresas->where('empresas.nome_comercial', 'like', "%{$filtroValor}%");
                break;

            case 'razao-social':
                $empresas->where('empresas.razao_social', 'like', "%{$filtroValor}%");
                break;

            case 'cnpj':
                $filtroValor = cnpjToInteger($filtroValor);
                $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                break;

            case 'bairro':
                $empresas->where('empresa_endereco.bairro', 'like', "%{$filtroValor}%");
                break;

            case 'status':
                $empresas->where('status.descricao', 'like', "%{$filtroValor}%");
                break;

            default:
                if (!empty($filtro) && $filtro == 'todos' && !empty($filtroValor)) {
                    if (valida_cnpj($filtroValor)) {
                        $filtroValor = cnpjToInteger($filtroValor);
                        $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                    }

                    $empresas->where(function ($empresas) use ($filtroValor) {
                        $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                            ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                    });
                }
                break;
        }

        return FiscalRenderer::renderFiscalEmpresasSearch($empresas);
    }

    public function cadastroPgSearch(Request $request, $empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Pequenos Geradores';
        $titulo = (object)$this->titulo;

        $filtro = $request->segment(4);
        $filtroValor = $request->segment(5);
        $dadosPesquisa = $dadosCadastrais = null;

        if ($empresaId != 0) {
            $dadosCadastrais = Empresa::where('id', $empresaId)
                ->with('empresa_endereco.cidade:id,nome')
                ->with('empresa_endereco.estado:id,sigla,nome')
                ->with('empresa_informacao_complementar.energia_consumo:id,nome')
                ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
                ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
                ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
                ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
                ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
                ->first();
        }
        elseif (!empty($filtro) && $filtro == 'todos') {

            $dadosPesquisa = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.pequeno_gerador'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

            if (!empty($filtroValor)) {
                $dadosPesquisa->where(function ($dadosPesquisa) use ($filtroValor) {
                    $dadosPesquisa->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                        ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                });                    
            }
            $dadosPesquisa->get();
        }

        return view('painel.fiscal.cadastro-pg.cadastro-pg-search', compact(
            'titulo',
            'dadosCadastrais',
            'dadosPesquisa',
            'filtro',
            'filtroValor'
        ));
    }

    public function cadastroPgSearchAjax($filtro = '', $filtroValor = '')
    {
        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        $empresas = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.pequeno_gerador'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->distinct()
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

        switch ($filtro) {
            case 'id-limpurb':
                $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%");
                break;

            case 'nome-comercial':
                $empresas->where('empresas.nome_comercial', 'like', "%{$filtroValor}%");
                break;

            case 'razao-social':
                $empresas->where('empresas.razao_social', 'like', "%{$filtroValor}%");
                break;

            case 'cnpj':
                $filtroValor = cnpjToInteger($filtroValor);
                $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                break;

            case 'bairro':
                $empresas->where('empresa_endereco.bairro', 'like', "%{$filtroValor}%");
                break;

            case 'status':
                $empresas->where('status.descricao', 'like', "%{$filtroValor}%");
                break;

            default:
                if (!empty($filtro) && $filtro == 'todos' && !empty($filtroValor)) {
                    if (valida_cnpj($filtroValor)) {
                        $filtroValor = cnpjToInteger($filtroValor);
                        $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                    }

                    $empresas->where(function ($empresas) use ($filtroValor) {
                        $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                            ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                    });
                }
                break;
        }

        return FiscalRenderer::renderFiscalEmpresasSearch($empresas);
    }

    public function cadastroGcSearch(Request $request, $empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Condomínios Mistos';
        $titulo = (object)$this->titulo;

        $filtro = $request->segment(4);
        $filtroValor = $request->segment(5);
        $dadosCadastrais = $dadosPesquisa = null;

        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        if ($empresaId != 0) {
            $dadosCadastrais = Empresa::where('id', $empresaId)
                ->with('empresa_endereco.cidade:id,nome')
                ->with('empresa_endereco.estado:id,sigla,nome')
                ->with('empresa_informacao_complementar.energia_consumo:id,nome')
                ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
                ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
                ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
                ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
                ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
                ->first();
        }
        elseif (!empty($filtro) && $filtro == 'todos') {

            $dadosPesquisa = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.condominio_misto'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

            if (!empty($filtroValor)) {
                $dadosPesquisa->where(function ($dadosPesquisa) use ($filtroValor) {
                    $dadosPesquisa->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                        ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                });                    
            }
            $dadosPesquisa->get();
        }

        return view('painel.fiscal.cadastro-gc.cadastro-gc-search', compact(
            'titulo',
            'dadosCadastrais',
            'dadosPesquisa',
            'filtro',
            'filtroValor'
        ));
    }

    public function cadastroGcSearchAjax($filtro = '', $filtroValor = '')
    {
        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        $empresas = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.condominio_misto'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->distinct()
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

        switch ($filtro) {
            case 'id-limpurb':
                $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%");
                break;

            case 'nome-comercial':
                $empresas->where('empresas.nome_comercial', 'like', "%{$filtroValor}%");
                break;

            case 'razao-social':
                $empresas->where('empresas.razao_social', 'like', "%{$filtroValor}%");
                break;

            case 'cnpj':
                $filtroValor = cnpjToInteger($filtroValor);
                $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                break;

            case 'bairro':
                $empresas->where('empresa_endereco.bairro', 'like', "%{$filtroValor}%");
                break;

            case 'status':
                $empresas->where('status.descricao', 'like', "%{$filtroValor}%");
                break;

            default:
                if (!empty($filtro) && $filtro == 'todos' && !empty($filtroValor)) {
                    if (valida_cnpj($filtroValor)) {
                        $filtroValor = cnpjToInteger($filtroValor);
                        $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                    }

                    $empresas->where(function ($empresas) use ($filtroValor) {
                        $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                            ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                    });
                }
                break;
        }

        return FiscalRenderer::renderFiscalEmpresasSearch($empresas);
    }

    public function cadastroTransportadorSearch(Request $request, $empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Transportadores';
        $titulo = (object)$this->titulo;
        $dadosCadastrais = $dadosPesquisa = false;

        $filtro = $request->segment(4);
        $filtroValor = $request->segment(5);
        $filtroValor = str_replace("*", "/", $filtroValor);

        if ($empresaId != 0) {
            $dadosCadastrais = Empresa::where('id', $empresaId)
                ->with('empresa_endereco.cidade:id,nome')
                ->with('empresa_endereco.estado:id,sigla,nome')
                ->with('empresa_informacao_complementar.energia_consumo:id,nome')
                ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
                ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
                ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
                ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
                ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
                ->first();
        }
        elseif (!empty($filtro) && $filtro == 'todos') {

            $dadosPesquisa = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.transportador'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

            if (!empty($filtroValor)) {
                $dadosPesquisa->where(function ($dadosPesquisa) use ($filtroValor) {
                    $dadosPesquisa->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                        ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                });                    
            }
            $dadosPesquisa->get();
        }

        return view('painel.fiscal.cadastro-tr.cadastro-tr-search',
            compact('titulo', 'dadosCadastrais', 'dadosPesquisa', 'filtro', 'filtroValor'));
    }

    public function cadastroTransportadorSearchAjax($filtro = '', $filtroValor = '')
    {
        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        $empresas = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.transportador'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->distinct()
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

        switch ($filtro) {
            case 'id-limpurb':
                $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%");
                break;

            case 'nome-comercial':
                $empresas->where('empresas.nome_comercial', 'like', "%{$filtroValor}%");
                break;

            case 'razao-social':
                $empresas->where('empresas.razao_social', 'like', "%{$filtroValor}%");
                break;

            case 'cnpj':
                $filtroValor = cnpjToInteger($filtroValor);
                $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                break;

            case 'bairro':
                $empresas->where('empresa_endereco.bairro', 'like', "%{$filtroValor}%");
                break;

            case 'status':
                $empresas->where('status.descricao', 'like', "%{$filtroValor}%");
                break;

            default:
                if (!empty($filtro) && $filtro == 'todos' && !empty($filtroValor)) {
                    if (valida_cnpj($filtroValor)) {
                        $filtroValor = cnpjToInteger($filtroValor);
                        $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                    }

                    $empresas->where(function ($empresas) use ($filtroValor) {
                        $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                            ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                    });
                }
                break;
        }

        return FiscalRenderer::renderFiscalEmpresasSearch($empresas);
    }

    public function vinculosTrVeiculosAjax($empresaId = 0)
    {
        if ($empresaId != 0) {
            $vinculosGg = EmpresasVeiculo::where('empresa_id', $empresaId)
                ->with('empresa:id,id_limpurb,razao_social,cnpj')
                ->with('status:id,descricao');

            return FiscalRenderer::renderFiscalTransportadoresVeiculos($vinculosGg);
        } else {
            return false;
        }
    }

    public function vinculosTrEquipamentosAjax($empresaId = 0)
    {
        if ($empresaId != 0) {
            $vinculosGg = EmpresasXEquipamento::where('empresa_id', $empresaId)
                ->with(['equipamento' => function($query) {
                    $query->with('tipo_equipamento');
                }])
                ->with('status')
                ->get();

            return FiscalRenderer::renderFiscalTransportadoresEquipamentos($vinculosGg);

        } else {
            return false;
        }
    }

    public function cadastroDfSearch(Request $request, $empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Destinos Finais';
        $titulo = (object)$this->titulo;

        $filtro = $request->segment(4);
        $filtroValor = $request->segment(5);
        $dadosCadastrais = $dadosPesquisa = null;

        if ($empresaId != 0) {
            $dadosCadastrais = Empresa::where('id', $empresaId)
                ->with('empresa_endereco.cidade:id,nome')
                ->with('empresa_endereco.estado:id,sigla,nome')
                ->with('empresa_informacao_complementar.energia_consumo:id,nome')
                ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
                ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
                ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
                ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
                ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
                ->first();
        }
        elseif (!empty($filtro) && $filtro == 'todos') {

            $dadosPesquisa = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.destino_final_reciclado'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

            if (!empty($filtroValor)) {
                $dadosPesquisa->where(function ($dadosPesquisa) use ($filtroValor) {
                    $dadosPesquisa->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                        ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                });                    
            }
            $dadosPesquisa->get();
        }

        return view('painel.fiscal.cadastro-df.cadastro-df-search',
            compact('titulo', 'dadosCadastrais', 'dadosPesquisa', 'filtro', 'filtroValor'));
    }

    public function cadastroDfSearchAjax($filtro = '', $filtroValor = '')
    {
        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        $empresas = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.destino_final_reciclado'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->distinct()
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

        switch ($filtro) {
            case 'id-limpurb':
                $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%");
                break;

            case 'nome-comercial':
                $empresas->where('empresas.nome_comercial', 'like', "%{$filtroValor}%");
                break;

            case 'razao-social':
                $empresas->where('empresas.razao_social', 'like', "%{$filtroValor}%");
                break;

            case 'cnpj':
                $filtroValor = cnpjToInteger($filtroValor);
                $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                break;

            case 'bairro':
                $empresas->where('empresa_endereco.bairro', 'like', "%{$filtroValor}%");
                break;

            case 'status':
                $empresas->where('status.descricao', 'like', "%{$filtroValor}%");
                break;

            default:
                if (!empty($filtro) && $filtro == 'todos' && !empty($filtroValor)) {
                    if (valida_cnpj($filtroValor)) {
                        $filtroValor = cnpjToInteger($filtroValor);
                        $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                    }

                    $empresas->where(function ($empresas) use ($filtroValor) {
                        $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                            ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                    });
                }
                break;
        }

        return FiscalRenderer::renderFiscalEmpresasSearch($empresas);

    }

    public function cadastroCrSearch(Request $request, $empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Cadastro Cooperativas';
        $titulo = (object)$this->titulo;

        $filtro = $request->segment(4);
        $filtroValor = $request->segment(5);
        $dadosCadastrais = $dadosPesquisa = null;

        if ($empresaId != 0) {
            $dadosCadastrais = Empresa::where('id', $empresaId)
                ->with('empresa_endereco.cidade:id,nome')
                ->with('empresa_endereco.estado:id,sigla,nome')
                ->with('empresa_informacao_complementar.energia_consumo:id,nome')
                ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
                ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
                ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
                ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
                ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
                ->first();
        }
        elseif (!empty($filtro) && $filtro == 'todos') {

            $dadosPesquisa = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.cooperativa_residuos'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

            if (!empty($filtroValor)) {
                $dadosPesquisa->where(function ($dadosPesquisa) use ($filtroValor) {
                    $dadosPesquisa->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                        ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                });                    
            }
            $dadosPesquisa->get();
        }

        return view('painel.fiscal.cadastro-cr.cadastro-cr-search',
            compact('titulo', 'dadosCadastrais', 'dadosPesquisa', 'filtro', 'filtroValor'));
    }

    public function cadastroCrSearchAjax($filtro = '', $filtroValor = '')
    {
        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        $empresas = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.cooperativa_residuos'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->distinct()
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

        switch ($filtro) {
            case 'id-limpurb':
                $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%");
                break;

            case 'nome-comercial':
                $empresas->where('empresas.nome_comercial', 'like', "%{$filtroValor}%");
                break;

            case 'razao-social':
                $empresas->where('empresas.razao_social', 'like', "%{$filtroValor}%");
                break;

            case 'cnpj':
                $filtroValor = cnpjToInteger($filtroValor);
                $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                break;

            case 'bairro':
                $empresas->where('empresa_endereco.bairro', 'like', "%{$filtroValor}%");
                break;

            case 'status':
                $empresas->where('status.descricao', 'like', "%{$filtroValor}%");
                break;

            default:
                if (!empty($filtro) && $filtro == 'todos' && !empty($filtroValor)) {
                    if (valida_cnpj($filtroValor)) {
                        $filtroValor = cnpjToInteger($filtroValor);
                        $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                    }

                    $empresas->where(function ($empresas) use ($filtroValor) {
                        $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                            ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                    });
                }
                break;
        }

        return FiscalRenderer::renderFiscalEmpresasSearch($empresas);
    }

    public function cadastroSsSearch(Request $request, $empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Serviço de Saúde';
        $titulo = (object)$this->titulo;

        $filtro = $request->segment(4);
        $filtroValor = $request->segment(5);
        $dadosCadastrais = $dadosPesquisa = null;

        if ($empresaId != 0) {
            $dadosCadastrais = Empresa::where('id', $empresaId)
                ->with('empresa_endereco.cidade:id,nome')
                ->with('empresa_endereco.estado:id,sigla,nome')
                ->with('empresa_informacao_complementar.energia_consumo:id,nome')
                ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
                ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
                ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
                ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
                ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
                ->first();
        }
        elseif (!empty($filtro) && $filtro == 'todos') {

            $dadosPesquisa = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.servico_saude'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

            if (!empty($filtroValor)) {
                $dadosPesquisa->where(function ($dadosPesquisa) use ($filtroValor) {
                    $dadosPesquisa->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                        ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                });                    
            }
            $dadosPesquisa->get();
        }

        return view('painel.fiscal.cadastro-ss.cadastro-ss-search',
            compact('titulo', 'dadosCadastrais', 'dadosPesquisa', 'filtro', 'filtroValor'));
    }

    public function cadastroSsSearchAjax($filtro = '', $filtroValor = '')
    {
        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        $empresas = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.servico_saude'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->distinct()
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

        switch ($filtro) {
            case 'id-limpurb':
                $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%");
                break;

            case 'nome-comercial':
                $empresas->where('empresas.nome_comercial', 'like', "%{$filtroValor}%");
                break;

            case 'razao-social':
                $empresas->where('empresas.razao_social', 'like', "%{$filtroValor}%");
                break;

            case 'cnpj':
                $filtroValor = cnpjToInteger($filtroValor);
                $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                break;

            case 'bairro':
                $empresas->where('empresa_endereco.bairro', 'like', "%{$filtroValor}%");
                break;

            case 'status':
                $empresas->where('status.descricao', 'like', "%{$filtroValor}%");
                break;

            default:
                if (!empty($filtro) && $filtro == 'todos' && !empty($filtroValor)) {
                    if (valida_cnpj($filtroValor)) {
                        $filtroValor = cnpjToInteger($filtroValor);
                        $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                    }

                    $empresas->where(function ($empresas) use ($filtroValor) {
                        $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                            ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                    });
                }
                break;
        }

        return FiscalRenderer::renderFiscalEmpresasSearch($empresas);
    }

    public function cadastroOpSearch(Request $request, $empresaId = 0)
    {
        $this->titulo['subtitulo'] = 'Órgão Público';
        $titulo = (object)$this->titulo;

        $filtro = $request->segment(4);
        $filtroValor = $request->segment(5);
        $dadosCadastrais = $dadosPesquisa = null;

        if ($empresaId != 0) {
            $dadosCadastrais = Empresa::where('id', $empresaId)
                ->with('empresa_endereco.cidade:id,nome')
                ->with('empresa_endereco.estado:id,sigla,nome')
                ->with('empresa_informacao_complementar.energia_consumo:id,nome')
                ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
                ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
                ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
                ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
                ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
                ->first();
        }
        elseif (!empty($filtro) && $filtro == 'todos') {

            $dadosPesquisa = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.orgao_publico'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->with('empresa_endereco.cidade:id,nome')
            ->with('empresa_endereco.estado:id,sigla,nome')
            ->with('empresa_informacao_complementar.energia_consumo:id,nome')
            ->with('empresa_informacao_complementar.colaboradores_numero:id,nome')
            ->with('empresa_informacao_complementar.frequencia_coleta:id,nome')
            ->with('empresa_informacao_complementar.frequencia_geracao:id,nome')
            ->with('empresa_informacao_complementar.ramo_atividade:id,nome')
            ->with('empresa_informacao_complementar.tipo_ramo_atividade:id,nome')
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

            if (!empty($filtroValor)) {
                $dadosPesquisa->where(function ($dadosPesquisa) use ($filtroValor) {
                    $dadosPesquisa->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                        ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                        ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                        ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                });                    
            }
            $dadosPesquisa->get();
        }

        return view('painel.fiscal.cadastro-op.cadastro-op-search',
            compact('titulo', 'dadosCadastrais', 'dadosPesquisa', 'filtro', 'filtroValor'));
    }

    public function cadastroOpSearchAjax($filtro = '', $filtroValor = '')
    {
        // Quando filtroValor tem /, esse caracter é substituído por * no js, aqui voltamos a barra.
        $filtroValor = str_replace("*", "/", $filtroValor);

        $empresas = Empresa::where('empresas_x_empresas_tipo.empresa_tipo_id',
            \config('enums.empresas_tipo.orgao_publico'))
            ->join('empresas_x_empresas_tipo', 'empresas_x_empresas_tipo.empresa_id', '=', 'empresas.id')
            ->join('status', 'status.id', '=', 'empresas.status_id')
            ->leftJoin('empresa_endereco', 'empresa_endereco.empresa_id', '=', 'empresas.id')
            ->distinct()
            ->select(
                'empresas.id',
                'empresas.id_limpurb',
                'empresas.nome_comercial',
                'empresas.razao_social',
                'empresas.cnpj',
                'empresa_endereco.bairro',
                'status.descricao'
            );

        switch ($filtro) {
            case 'id-limpurb':
                $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%");
                break;

            case 'nome-comercial':
                $empresas->where('empresas.nome_comercial', 'like', "%{$filtroValor}%");
                break;

            case 'razao-social':
                $empresas->where('empresas.razao_social', 'like', "%{$filtroValor}%");
                break;

            case 'cnpj':
                $filtroValor = cnpjToInteger($filtroValor);
                $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                break;

            case 'bairro':
                $empresas->where('empresa_endereco.bairro', 'like', "%{$filtroValor}%");
                break;

            case 'status':
                $empresas->where('status.descricao', 'like', "%{$filtroValor}%");
                break;

            default:
                if (!empty($filtro) && $filtro == 'todos' && !empty($filtroValor)) {
                    if (valida_cnpj($filtroValor)) {
                        $filtroValor = cnpjToInteger($filtroValor);
                        $empresas->where('empresas.cnpj', 'like', "%{$filtroValor}%");
                    }

                    $empresas->where(function ($empresas) use ($filtroValor) {
                        $empresas->where('empresas.id_limpurb', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.nome_comercial', 'like', "%{$filtroValor}%")
                            ->orWhere('empresas.razao_social', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.endereco', 'like', "%{$filtroValor}%")
                            ->orWhere('empresa_endereco.bairro', 'like', "%{$filtroValor}%")
                            ->orWhere('status.descricao', 'like', "%{$filtroValor}%");
                    });
                }
                break;
        }

        return FiscalRenderer::renderFiscalEmpresasSearch($empresas);
    }

    /**
     * @param string $dataInicial
     * @param string $dataFinal
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function ctreBalancoPeriodo($dataInicial = '', $dataFinal = '')
    {
        if ($dataInicial != '' || $dataFinal != '') {

            $ctre = DB::table('ctre')
                ->join('status', 'status.id', '=', 'ctre.status_id')
                ->select(DB::raw('count(*) as ctre_cont, ctre.status_id, status.descricao, YEAR(ctre.data_emissao) as ano, MONTH(ctre.data_emissao) as mes'))
                ->whereBetween('ctre.data_emissao', [$dataInicial, $dataFinal])
                ->groupBy('ctre.status_id', 'status.descricao', 'ano', 'mes')
                ->orderByRaw('ano ASC, mes ASC')
                ->get();

            $grafico = array();

            if (count($ctre)) {
                foreach ($ctre as $item) {

                    $indiceAnoMes = $item->ano . "/" . $item->mes;
                    $indiceDescricao = strtolower(str_replace(" ", "", trim($item->descricao)));

                    if (array_key_exists($indiceAnoMes, $grafico)) {
                        if (array_key_exists($indiceDescricao, $grafico[$indiceAnoMes])) {
                            $grafico[$indiceAnoMes][$indiceDescricao]['ctre_cont'] = $grafico[$indiceAnoMes][$indiceDescricao]['ctre_cont'] + $item->ctre_cont;
                        } else {
                            $grafico[$indiceAnoMes][$indiceDescricao] = array(
                                'ctre_cont' => $item->ctre_cont,
                            );
                        }

                    } else {
                        $grafico[$indiceAnoMes][$indiceDescricao] = array(
                            'ctre_cont' => $item->ctre_cont,
                        );
                    }
                }
            }

            if (count($ctre)) {
                return response()->json(['ctre' => $grafico]);
            } else {
                return response()->json(['error' => true]);
            }

        } else {
            return response()->json(['error' => true]);
        }
    }

    public function getVeiculo(Request $request, $idVeiculo)
    {
        if ($request->ajax()) {
            $veiculo = EmpresasVeiculo::find($idVeiculo);
            $anexo_documento_veiculo = null;
            $anexo_documento_inmetro = null;
            $anexo_comodato_veiculo = null;
            if (!empty($veiculo->anexo_documento_veiculo)) {
                $anexo_documento_veiculo = route('download_veiculo', ['arquivo' => $veiculo->anexo_documento_veiculo, 'empresa' => $veiculo->empresa_id]);
            }
            if (!empty($veiculo->anexo_documento_inmetro)) {
                $anexo_documento_inmetro = route('download_veiculo', ['arquivo' => $veiculo->anexo_documento_inmetro, 'empresa' => $veiculo->empresa_id]);
            }
            if (!empty($veiculo->anexo_comodato_veiculo)) {
                $anexo_comodato_veiculo = route('download_veiculo', ['arquivo' => $veiculo->anexo_comodato_veiculo, 'empresa' => $veiculo->empresa_id]);
            }
            $veiculo->anexo_documento_veiculo = $anexo_documento_veiculo;
            $veiculo->anexo_documento_inmetro = $anexo_documento_inmetro;
            $veiculo->anexo_comodato_veiculo = $anexo_comodato_veiculo;
            return response()->json(['dados_veiculo' => $veiculo]);
        }
    }

    public function aprovarVeiculo(Request $request, $idVeiculo, $boleto, $data = null)
    {
        
        if ($request->ajax()) {
            $status = \config('enums.status.inativo');
            if ($boleto == 'false') {
                $veiculo = EmpresasVeiculo::find($idVeiculo);
                    //cria dados falsos para o boleto
                    $data = Carbon::parse($data);
                    
                    $dadosBoleto['dadosGuia'] = [
                        'empresa_id' => $veiculo->empresa_id,
                        'tipo' => 'veiculo',
                        'anoEmissao' => $data->format('Y'),
                        'numeroGuia' => $veiculo->empresa_id,
                        'valor' => 0,
                        'status_id' => \config('enums.status.pago'),
                        'codTipoServGuia' => 0,
                        'arquivoBoleto' => 'fake',
                        'dataDeValidade' => $data->format('Y-m-d'),
                        'dataDeVencimento' => $data->format('Y-m-d'),
                        'dataDePagamento' => $data->format('Y-m-d')
                    ];
                    $boletoFake =  $this->salvarBoleto($dadosBoleto, $veiculo->empresa_id, config('enums.codigo_tipo_servico.veiculo'));
                    
                    BoletoVeiculos::firstOrCreate([
                        'boleto_id' => $boletoFake->id,
                        'veiculo_id' => $veiculo->id
                    ]);
                    $veiculo = EmpresasVeiculo::find($idVeiculo)->update(
                        [
                             'data_validade' => $data->format('Y-m-d'),
                         ]
                    );
                    $status = \config('enums.status.ativo');
            }

            $veiculo = EmpresasVeiculo::find($idVeiculo)->update(
                [
                     'status_id' => $status,
                 ]
            );
            
            return response()->json(['dados_veiculo' => $veiculo]);
        }
    }

    public function reportarInconsistenciaVeiculo(Request $request)
    {
        if ($request->ajax()) {
            $retornoAjax['status'] = false;
            $retornoAjax['error'] = '';
            $msg_inconsistencia = $request->input('mensagem');
            try {
                DB::beginTransaction();
    
                $veiculo = EmpresasVeiculo::find($request->input('id_veiculo'));
                $veiculo->status_id =  \config('enums.status.inconsistente');
                $veiculo->save();
    
                $just = Justificativa::create(['justificativa' => $msg_inconsistencia]);
                JustificativaInconsistenciaVeiculos::create([
                    'justificativa_id' => $just->id,
                    'fiscal_id' => Auth::user()->id,
                    'empresa_id' =>$veiculo->empresa_id ,
                    'empresas_veiculo_id' => $veiculo->id
                ]);
                DB::commit();
                $retornoAjax['status'] = true;
                $retornoAjax['mensagem'] = "Incosistência reportada com sucesso!";
            } catch (\Exception $e) {
                Log::error($e);
                DB::rollback();
                $retornoAjax['mensagem'] = $e->getMessage();
            }
            
            return response()->json($retornoAjax);
        }
    }

    public function getInconsistenciaVeiculo(Request $request, $idVeiculo)
    {
        
        if ($request->ajax()) {
            $retornoAjax['status'] = false;
            $justificativa = JustificativaInconsistenciaVeiculos::where('empresas_veiculo_id', $idVeiculo)
            ->with('justificativaLast')->get()->last();
           
            if ($justificativa) {
                $retornoAjax['status'] = true;
                $retornoAjax['mensagem'] = $justificativa->justificativaLast->justificativa;
            }
            return response()->json($retornoAjax);
        }
    }

    public function salvarBoleto($dadosBoleto, $empresa_id, $tipo_servico)
    {
        
        $dadosSalvarBoleto = [
            'empresa_id' => $empresa_id,
            'anoEmissao' => $dadosBoleto['dadosGuia']['anoEmissao'],
            'dataDeValidade' => $dadosBoleto['dadosGuia']['dataDeValidade'],
            'dataDeVencimento' => $dadosBoleto['dadosGuia']['dataDeVencimento'],
            'valor' => $dadosBoleto['dadosGuia']['valor'],
            'status_id' => $dadosBoleto['dadosGuia']['status_id'],
            'codTipoServGuia' => $dadosBoleto['dadosGuia']['codTipoServGuia'],
            'numeroGuia' => $dadosBoleto['dadosGuia']['numeroGuia'],
            'arquivoBoleto' => $dadosBoleto['dadosGuia']['arquivoBoleto'],
            'tipo' => $dadosBoleto['dadosGuia']['tipo'],
            'dataDePagamento' => $dadosBoleto['dadosGuia']['dataDePagamento'],
        ];
        return Boleto::firstOrCreate($dadosSalvarBoleto);
    }
}
