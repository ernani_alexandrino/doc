<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\Cidade;

class CidadeController extends Controller
{

    public function show($id)
    {
        return Cidade::where('estado_id', $id)->pluck('nome', 'id');
    }

    public function buscarTipoAtividade($idRamoAtividade = 1)
    {
        
        return TipoRamoAtividade::where('ramo_atividade_id', $idRamoAtividade)->pluck('nome', 'id');
        //return response()->json($tipoAtividade);
    }
}
