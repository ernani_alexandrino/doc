<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Http\Controllers\AprovacaoCadastroController;
use Amlurb\Models\Protocolo;

use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresaAlteracao;
use Amlurb\Models\EmpresaDocumentos;
use Amlurb\Models\EmpresaAlteracaoJson;
use Amlurb\Mail\CadastroRealizado;
use Amlurb\Models\Justificativa;
use Amlurb\Models\JustificativaEmpresaAlteracao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Empresa::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function solicitarAlteracaoDados(Request $request)
    {
       
        $dados = $request->all();
        parse_str($dados['empresa_alteracao'], $dadosForm);
       
        $arrJustificativas = $dados['justificativas'];
        $arrEmpresaAlteracao['dados_empresa'] = [];
        $arrEmpresaAlteracao['dados_empresa']['empresa_id'] = Auth::user()->empresa_id;
        $arrEmpresaAlteracao['dados_empresa']['responsible_user_id'] = Auth::user()->id;
       // $arrEmpresaAlteracao['cep'] = str_replace('-', '', $arrEmpresaAlteracao['cep']);

        //  return response()->json(['error' => $form_error]);
        try {
            DB::beginTransaction();

            $empresa_alteracao = EmpresaAlteracaoJson::firstOrCreate(
                [
                    'empresa_id' => $arrEmpresaAlteracao['dados_empresa']['empresa_id'],
                    'responsible_user_id' => $arrEmpresaAlteracao['dados_empresa']['responsible_user_id']
                ]
            );

            if (!empty($empresa_alteracao->json_data)) {
                $arrEmpresaAlteracao = json_decode($empresa_alteracao->json_data, true);
            }

            foreach ($dadosForm as $k => $v) {
                if (strpos($k, 'alterado') !== false) {
                    $arrEmpresaAlteracao['dados_empresa'][str_replace('_alterado', '', preg_replace("/\b(\w+)\s+\\1\b/i", "$1", $k))] = $v;
                }
            }
            
            $empresa_alteracao->json_data = json_encode($arrEmpresaAlteracao);
            $empresa_alteracao->save();
            
            // salva a justificativa de invalidacao
            $justificativa = Justificativa::firstOrCreate($arrJustificativas);

            JustificativaEmpresaAlteracao::create([
                'justificativa_id' => $justificativa->id,
                'empresa_alteracao_id' => $empresa_alteracao->id
            ]);

            $empresa = $this->alterarStatus($arrEmpresaAlteracao['dados_empresa']['empresa_id'], config('enums.status.revisado'));
           
            $salvaDoc = $this->salvaDocumentosTemp($empresa->id, $empresa->cnpj); // Salva o Arquivo by Mr.Goose em 18 abr 19
            
            $dadosEmail = [
                'responsavel' => $empresa->nome_responsavel,
                'razaoSocial' => $empresa->razao_social,
                'inscricaoMunicipal' => $empresa->im,
                'dataEnvio' => date('d/m/Y'),
                'emailReceptor' => $empresa->email,
            ];
            if (!empty($empresa->id_matriz) && ($empresa->status_id == config('enums.status.autodeclarado'))) {
                // Entra Aqui qndo é uma Empresa Filial (além de ter id_matriz o empresa->status_id == autodeclarado).
                $aprovacao = new AprovacaoCadastroController();
                $aprovacao->revisarCadastro($request, $empresa->id);

                $empresaTipo = $empresa->empresas_x_empresas_tipos;
                $tipoCadastro = $empresaTipo->empresas_tipo->nome;
                $protocolo = Protocolo::where('empresa_id', $empresa->id)->first();

                $dadosEmail['tipoCadastro'] = $tipoCadastro;
                $dadosEmail['id_limpurb'] = $empresa->id_limpurb;
                $dadosEmail['nomeFantasia'] = $empresa->nome_comercial;
                $dadosEmail['protocolo'] = $protocolo->numero_protocolo;
                
                $dadosEmail['view'] = 'emails.cadastros-empresas.cadastro-realizado';
                $dadosEmail['subject'] = 'CTRE - Cadastro aguardando validação';
            } // close if() Entra Aqui qndo é uma Empresa Filial (além de ter id_matriz o empresa->status_id == autodeclarado).
            else{
                $dadosEmail['view'] = 'emails.cadastros-empresas.cadastro-solicitacao-revisao';
                $dadosEmail['subject'] = 'CTRE - Solicitação de revisão de cadastro';
            } // close else if() // Entra aqui qndo NÃO for um Pré Cadastro de Filial, ou seja, todos os casos normais.

            
            $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($dadosEmail['emailReceptor']));
            
            $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
                "email" => 'mailgun_email:mailbox'
            ]);
            if ($validator->fails()) {
                $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
                throw new \Exception($retornoAjax['erro']);
            }

            Mail::to($dadosEmail['emailReceptor'])->queue(new CadastroRealizado($dadosEmail));

            DB::commit();
            return response()->json([
                'success' => 'Solicitação de alteração de dados da empresa enviada com sucesso.<br />Aguarde o processo de aprovação e efetivação da(s) mudança(s).',
                'error' => []
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'error' => $e->getMessage()
            ]);
        }
    }
    public function alterarStatus($empresa_id, $status_id) {
        try {

            DB::beginTransaction();
            
            $empresa = Empresa::with('empresas_x_empresas_tipos')
                    ->with('empresa_responsavel')
                    ->find($empresa_id);
            if ($empresa->status_id ==  config('enums.status.pre_cadastro'))
                $empresa->status_id = config('enums.status.autodeclarado');
            else
                $empresa->status_id = $status_id;
            $empresa->save();

            DB::commit();

            return $empresa;

        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return redirect()->back()->with([
                'error' => 'Ocorreu um erro na alteração de status. Por favor, tente novamente.'
            ]);
        }
    }

    /**
     * Salva os Documentos e Altera no Banco
     * by Mr.Goose in 18 abr 2019
     * @param  int $empresa_id
     * @param  str $empresa_cnpj
     * @return boolean
     */
    public function salvaDocumentos($empresa_id, $empresa_cnpj)
    {
        $dadosDocumentos = session('documentos');
    
        if (!empty($dadosDocumentos) && !empty($empresa_id) && !empty($empresa_cnpj)) {

            $documentos = config('enums.documentos');
            foreach ($dadosDocumentos as $campo => $valor) {
                if (array_key_exists($campo, $documentos) && !empty($valor)) {
                    $empresaDocumento = EmpresaDocumentos::where('empresa_id',$empresa_id)->where('documento_id',$documentos[$campo])->first();
                    // $empresaDocumento->empresa_id = $empresa_id;
                    // $empresaDocumento->documento_id = $documentos[$campo];
                    $empresaDocumento->caminho_arquivo = $valor;
                    if(!empty($dadosDocumentos[$campo.'_emissao']))
                        $empresaDocumento->data_emissao = $dadosDocumentos[$campo.'_emissao'];
                    if(!empty($dadosDocumentos[$campo.'_vencimento']))
                        $empresaDocumento->data_vencimento = $dadosDocumentos[$campo.'_vencimento'];
                    $empresaDocumento->save();
                } // close if (array_key_exists($campo, $documentos))
            } // close foreach ($dadosDocumentos as $campo => $valor)

            // if (!empty($dadosDocumentos['licenca'])) {
            //     $empresaDocumento = new EmpresaDocumentos;
            //     $empresaDocumento->empresa_id = $empresa_id;
            //     $empresaDocumento->documento_id = $dadosDocumentos['licencadefault'];
            //     $empresaDocumento->caminho_arquivo = $dadosDocumentos['licenca'];
            //     if(!empty($dadosDocumentos['emissaolicenca']))
            //         $empresaDocumento->data_emissao = $dadosDocumentos['emissaolicenca'];
            //     if(!empty($dadosDocumentos['licencavencimento']))
            //         $empresaDocumento->data_vencimento = $dadosDocumentos['licencavencimento'];
            //     $empresaDocumento->save();
            // } // close if(!empty($dadosDocumentos['licenca']))

            $newPatch = "licencas/{$empresa_id}/";
            $oldPath = "licencas_temp/{$empresa_cnpj}/";
            //Pega os arquivos da pasta temporária
            $pastaTemporaria = Storage::disk('s3')->allFiles("licencas_temp/{$empresa_cnpj}/");

            foreach ($pastaTemporaria as $file) {
                $copied_file = str_replace($oldPath, $newPatch, $file);
                if (!Storage::disk('s3')->exists($copied_file)) {
                    Storage::disk('s3')->copy($file, $copied_file);
                }
            }

            return true;
        }
        return false;
    }


    public function salvaDocumentosTemp($empresa_id, $empresa_cnpj)
    {
        $dadosDocumentos = session('documentos');
        $arrEmpresaAlteracao['documentos'] = [];
        if (!empty($dadosDocumentos) && !empty($empresa_id) && !empty($empresa_cnpj)) {

            $documentos = config('enums.documentos_alterado');
            
            $empresa_alteracao = EmpresaAlteracaoJson::where('empresa_id', $empresa_id)->first();
           
            if (!empty($empresa_alteracao->json_data)) {
                $arrEmpresaAlteracao = json_decode($empresa_alteracao->json_data, true);
            }

            foreach ($dadosDocumentos as $campo => $valor) {
                if (array_key_exists($campo, $documentos) && !empty($valor)) {
                
                    
                     $arrEmpresaAlteracao['documentos'][$campo] = $valor;
                 
                    if (!empty($dadosDocumentos[$campo.'_emissao'])) {
                        $arrEmpresaAlteracao['documentos'][$campo]['emissao'] = $dadosDocumentos[$campo.'_emissao'];
                    }
                        
                    if (!empty($dadosDocumentos[$campo.'_vencimento'])) {
                        $arrEmpresaAlteracao['documentos'][$campo]['vencimento'] = $dadosDocumentos[$campo.'_vencimento'];
                    }
                }
            }
        
            $data = array_merge(json_decode($empresa_alteracao->json_data, true), $arrEmpresaAlteracao);
            $empresa_alteracao->json_data = json_encode($data);
            $empresa_alteracao->save();
            return true;
        }
        return false;
    }
}
