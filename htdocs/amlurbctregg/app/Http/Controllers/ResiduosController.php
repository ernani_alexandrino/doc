<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\Residuo;

use Illuminate\Http\Request;

use Redirect;

class ResiduosController extends Controller
{
    const RESIDUOS_INDEX = 'R';
    const NUM_ZEROES = 3;

    public $titulo = array(
        'principal' => 'Amlurb',
        'subtitulo' => 'Residuos'
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $residuos = Residuo::all();
        $total = Residuo::withTrashed()->count();
        $proximo_codigo = $this->proximoCodigoResiduo($total);

        $this->titulo['subtitulo'] = 'Cadastro Resíduos';
        $titulo = (object)$this->titulo;

        return view('painel.fiscal.residuos.home', compact('residuos', 'titulo', 'proximo_codigo'));
    }

    /**
     * @param int $total
     * @return string
     */
    private function proximoCodigoResiduo($total)
    {
        $zeros = self::NUM_ZEROES - strlen($total + 1);
        return self::RESIDUOS_INDEX . str_repeat('0', $zeros) . ((int)$total + 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data_residuo['codigo'] = $request->input('codigo');
        $data_residuo['nome'] = $request->input('nome');

        Residuo::firstOrCreate($data_residuo);

        return Redirect::route('residuo_fs')->with([
            'context' => 'success',
            'message' => 'Resíduo Cadastrado com sucesso!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $residuo = Residuo::whereId($id)->first();
        return view('painel.fiscal.residuos.modais.editarResiduo', compact('residuo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $residuo = Residuo::find($id);
        $residuo->codigo = $request->input('codigo');
        $residuo->nome = $request->input('nome');

        if ($residuo->save()) {
            return Redirect::route('residuo_fs')->with([
                'context' => 'success',
                'message' => 'Resíduo Editado com sucesso!'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Residuo::whereId($id)->delete();
        return Redirect::route('residuo_fs')->with([
            'context' => 'success',
            'message' => 'Resíduo Deletado com sucesso!'
        ]);
    }
}
