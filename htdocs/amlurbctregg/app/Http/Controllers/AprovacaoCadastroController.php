<?php

namespace Amlurb\Http\Controllers;

use Amlurb\EmpresasAtivacaoLog;
use Amlurb\Http\Requests\AtivarDesativarEmpresaRequest;
use Amlurb\Mail\CadastroRealizado;
use Amlurb\Mail\VinculosFaltantes;
use Amlurb\Models\Boleto;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\EmpresaResponsavel;
use Amlurb\Models\JustificativaInconsistenciaDados;
use Amlurb\Models\Status;
use Amlurb\Models\Cidade;
use Amlurb\Models\Estado;
use Amlurb\Models\FrequenciaGeracao;
use Amlurb\Models\FrequenciaColeta;
use Amlurb\Models\ColaboradoresNumero;
use Amlurb\Models\EnergiaConsumo;
use Amlurb\Models\EstabelecimentoTipo;
use Amlurb\Models\User;
use Amlurb\Models\EmpresaDocumentos;
use Amlurb\Services\DadosEmail;
use Amlurb\Services\ParametrosBoleto;
use Amlurb\Services\ServiceBoletos;
use Amlurb\Models\EmpresaAprovacao;
use Amlurb\Models\EmpresaAlteracaoJson;
use Amlurb\Models\JustificativaEmpresaAlteracao;
use Amlurb\Models\Justificativa;
use Amlurb\Models\EmpresaEndereco;
use Amlurb\Models\EmpresaInformacaoComplementars;
use Illuminate\Support\Facades\Storage;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;
use Validator;

class AprovacaoCadastroController extends Controller
{
    public $titulo = array(
        'principal' => ' Amlurb',
        'subtitulo' => 'Aprovação de cadastro'
    );

    /**
     * Dashboard da lista de empresas que precisam de aprovação
     * Status 10 = Aguardando aprovação
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $titulo = (object)$this->titulo;

        $cadastros = [
            'enviados' => [
                'grande_gerador' => [
                    'total' => 0, 'link' => route('fiscal.aprovacao.lista', ['grande-gerador']),
                    'class' => 'boxIndicador isPg', 'title' => 'Grande Gerador'
                ],
                'pequeno_gerador' => [
                    'total' => 0, 'link' => route('fiscal.aprovacao.lista', ['pequeno-gerador']),
                    'class' => 'boxIndicador isGg', 'title' => 'Pequeno Gerador'
                ],
                'transportador' => [
                    'total' => 0, 'link' => route('fiscal.aprovacao.lista', ['transportador']),
                    'class' => 'boxIndicador isTr', 'title' => 'Transportador'
                ],
                'destino_final' => [
                    'total' => 0, 'link' => route('fiscal.aprovacao.lista', ['destino-final']),
                    'class' => 'boxIndicador isDf', 'title' => 'Destino Final'
                ],
                'cooperativa' => [
                    'total' => 0, 'link' => route('fiscal.aprovacao.lista', ['cooperativa']),
                    'class' => 'boxIndicador isCr', 'title' => 'Cooperativa'
                ],
                'condominio_misto' => [
                    'total' => 0, 'link' => route('fiscal.aprovacao.lista', ['condominio-misto']),
                    'class' => 'boxIndicador isCm', 'title' => 'Condomínio Misto'
                ],
                'servico_saude' => [
                    'total' => 0, 'link' => route('fiscal.aprovacao.lista', ['servico-saude']),
                    'class' => 'boxIndicador isSs', 'title' => 'Serviço Saúde'
                ],
                'orgao_publico' => [
                    'total' => 0, 'link' => route('fiscal.aprovacao.lista', ['orgao-publico']),
                    'class' => 'boxIndicador isOp', 'title' => 'Órgão Público'
                ]
            ],
            'revisar' => [
                'grande_gerador' => [
                    'total' => 0, 'link' => route('fiscal.revisao.lista', ['grande-gerador']),
                    'class' => 'boxIndicador isPg', 'title' => 'Grande Gerador'
                ],
                'pequeno_gerador' => [
                    'total' => 0, 'link' => route('fiscal.revisao.lista', ['pequeno-gerador']),
                    'class' => 'boxIndicador isGg', 'title' => 'Pequeno Gerador'
                ],
                'transportador' => [
                    'total' => 0, 'link' => route('fiscal.revisao.lista', ['transportador']),
                    'class' => 'boxIndicador isTr', 'title' => 'Transportador'
                ],
                'destino_final' => [
                    'total' => 0, 'link' => route('fiscal.revisao.lista', ['destino-final']),
                    'class' => 'boxIndicador isDf', 'title' => 'Destino Final'
                ],
                'cooperativa' => [
                    'total' => 0, 'link' => route('fiscal.revisao.lista', ['cooperativa']),
                    'class' => 'boxIndicador isCr', 'title' => 'Cooperativa'
                ],
                'condominio_misto' => [
                    'total' => 0, 'link' => route('fiscal.revisao.lista', ['condominio-misto']),
                    'class' => 'boxIndicador isCm', 'title' => 'Condomínio Misto'
                ],
                'servico_saude' => [
                    'total' => 0, 'link' => route('fiscal.revisao.lista', ['servico-saude']),
                    'class' => 'boxIndicador isSs', 'title' => 'Serviço Saúde'
                ],
                'orgao_publico' => [
                    'total' => 0, 'link' => route('fiscal.revisao.lista', ['orgao-publico']),
                    'class' => 'boxIndicador isOp', 'title' => 'Órgão Público'
                ]
            ]
        ];



        $empresas = Empresa::whereIn(
            'status_id',
            [config('enums.status.autodeclarado'),
            config('enums.status.em_analise_amlurb')]
        )
        ->with(['empresas_x_empresas_tipos' => function ($query) {
            $query->where('status_id', '=', config('enums.status.ativo'));
        }]);
        $cadastros['enviados_total'] = $empresas->count();
      
        foreach ($empresas->get() as $empresa) {
            switch ($empresa->empresas_x_empresas_tipos->empresa_tipo_id) {
                case config('enums.empresas_tipo.grande_gerador'):
                    $cadastros['enviados']['grande_gerador']['total']++;
                    break;

                case config('enums.empresas_tipo.pequeno_gerador'):
                    $cadastros['enviados']['pequeno_gerador']['total']++;
                    break;

                case config('enums.empresas_tipo.transportador'):
                    $cadastros['enviados']['transportador']['total']++;
                    break;

                case config('enums.empresas_tipo.destino_final_reciclado'):
                    $cadastros['enviados']['destino_final']['total']++;
                    break;

                case config('enums.empresas_tipo.cooperativa_residuos'):
                    $cadastros['enviados']['cooperativa']['total']++;
                    break;

                case config('enums.empresas_tipo.condominio_misto'):
                    $cadastros['enviados']['condominio_misto']['total']++;
                    break;

                case config('enums.empresas_tipo.servico_saude'):
                    $cadastros['enviados']['servico_saude']['total']++;
                    break;

                case config('enums.empresas_tipo.orgao_publico'):
                    $cadastros['enviados']['orgao_publico']['total']++;
                    break;
            }
        }
      
        $empresas = Empresa::whereIn(
            'status_id',
            [config('enums.status.aguardando_revisao'),
             config('enums.status.revisado')
            ]
        )
        ->with('empresas_x_empresas_tipos:empresa_id,empresa_tipo_id')->has('empresas_x_empresas_tipos');
        $cadastros['revisar_total'] = $empresas->count();

        foreach ($empresas->get() as $empresa) {
            switch ($empresa->empresas_x_empresas_tipos->empresa_tipo_id) {
                case config('enums.empresas_tipo.grande_gerador'):
                    $cadastros['revisar']['grande_gerador']['total']++;
                    break;

                case config('enums.empresas_tipo.pequeno_gerador'):
                    $cadastros['revisar']['pequeno_gerador']['total']++;
                    break;

                case config('enums.empresas_tipo.transportador'):
                    $cadastros['revisar']['transportador']['total']++;
                    break;

                case config('enums.empresas_tipo.destino_final_reciclado'):
                    $cadastros['revisar']['destino_final']['total']++;
                    break;

                case config('enums.empresas_tipo.cooperativa_residuos'):
                    $cadastros['revisar']['cooperativa']['total']++;
                    break;
               
                case config('enums.empresas_tipo.condominio_misto'):
                    $cadastros['revisar']['condominio_misto']['total']++;
                    break;

                case config('enums.empresas_tipo.servico_saude'):
                    $cadastros['revisar']['servico_saude']['total']++;
                    break;

                case config('enums.empresas_tipo.orgao_publico'):
                    $cadastros['revisar']['orgao_publico']['total']++;
                    break;
            }
        }

        return view('painel.fiscal.aprovacao.dashboard', compact(
            'titulo',
            'cadastros'
        ));
    }
    /**
     * Carrega a lista de empresas selecionadas pelo tipo passado no parâmetro
     *
     * @param string $tipoEmpresa
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lista($tipoEmpresa = '')
    {
        $titulo = (object)$this->titulo;
        switch ($tipoEmpresa) {
            case 'pequeno-gerador':
                $titulo->subtitulo = 'Aprovação para pequeno gerador';
                break;
            case 'grande-gerador':
                $titulo->subtitulo = 'Aprovação para grande gerador';
                break;
            case 'transportador':
                $titulo->subtitulo = 'Aprovação para transportador';
                break;
            case 'destino-final':
                $titulo->subtitulo = 'Aprovação para destino final';
                break;
            case 'cooperativa':
                $titulo->subtitulo = 'Aprovação para cooperativas';
                break;
            case 'condominio-misto':
                $titulo->subtitulo = 'Aprovação para condomínio misto';
                break;
            case 'servico-saude':
                $titulo->subtitulo = 'Aprovação para serviço de saúde';
                break;
            case 'orgao-publico':
                $titulo->subtitulo = 'Aprovação para órgão público';
                break;
        }
       
        return view('painel.fiscal.aprovacao.lista', compact('titulo', 'tipoEmpresa'));
    }

    public function listaRevisao($tipoEmpresa = '') {
        $titulo = (object)$this->titulo;
        switch ($tipoEmpresa) {
            case 'pequeno-gerador':
                $titulo->subtitulo = 'Revisão para pequeno gerador';
                break;
            case 'grande-gerador':
                $titulo->subtitulo = 'Revisão para grande gerador';
                break;
            case 'transportador':
                $titulo->subtitulo = 'Revisão para transportador';
                break;
            case 'destino-final':
                $titulo->subtitulo = 'Revisão para destino final';
                break;
            case 'cooperativa':
                $titulo->subtitulo = 'Revisão para cooperativas';
                break;

            case 'condominio-misto':
                $titulo->subtitulo = 'Revisão para Condomínio Misto';
                break;
            
            case 'servico-saude':
                $titulo->subtitulo = 'Revisão para Serviço de Saúde';
                break;
            
            case 'orgao-publico':
                $titulo->subtitulo = 'Revisão para Orgão Público';
                break;
        }

        return view('painel.fiscal.aprovacao.lista-revisao', compact('titulo', 'tipoEmpresa'));
    }

    /**
     * Pega a lista de empresas que ainda estão com status de aguardando aprovação
     *
     * @param string $tipoEmpresa
     *
     * @return mixed
     * @throws \Exception
     */
    public function pegaListaEmpresasParaAprovacao($tipoEmpresa = '')
    {

        $idTipoEmpresa = '';

        switch ($tipoEmpresa) {
            case 'pequeno-gerador':
                $idTipoEmpresa = config('enums.empresas_tipo.pequeno_gerador');
                break;

            case 'grande-gerador':
                $idTipoEmpresa = config('enums.empresas_tipo.grande_gerador');
                break;

            case 'transportador':
                $idTipoEmpresa = config('enums.empresas_tipo.transportador');
                break;

            case 'destino-final':
                $idTipoEmpresa = config('enums.empresas_tipo.destino_final_reciclado');
                break;

            case 'cooperativa':
                $idTipoEmpresa = config('enums.empresas_tipo.cooperativa_residuos');
                break;

            case 'condominio-misto':
                $idTipoEmpresa = config('enums.empresas_tipo.condominio_misto');
                break;

            case 'servico-saude':
                $idTipoEmpresa = config('enums.empresas_tipo.servico_saude');
                break;

            case 'orgao-publico':
                $idTipoEmpresa = config('enums.empresas_tipo.orgao_publico');
                break;
        }

       
        $empresas = Empresa::whereIn(
            'status_id',
            [config('enums.status.autodeclarado'),
            config('enums.status.em_analise_amlurb')]
        )
        ->with('empresa_informacao_complementar')
        ->with('empresas_x_empresas_tipos:empresa_id,empresa_tipo_id')
        
        ->whereHas('empresas_x_empresas_tipos', function ($query) use ($idTipoEmpresa) {
            $query->where('empresa_tipo_id', '=', $idTipoEmpresa);
            $query->where('status_id', '=', config('enums.status.ativo'));
        })->get();
           
    
         return Datatables::of($empresas)
            ->addColumn('cnpj', function ($results) {
                return "<div class=\"cnpj\">{$results->cnpj}</div>";
            })
            ->addColumn('razao_social', function ($results) {
                return "<div class=\"razao_social\">{$results->razao_social}</div>";
            })
            ->addColumn('data', function ($results) {
                $data = decodeDate($results->updated_at);
                return "<div class=\"data\">{$data}</div>";
            })
            ->addColumn('status', function ($results) {
                return "<div class=\"statusAprovacao\">{$results->status->descricao}</div>";
            })
            ->rawColumns(['cnpj', 'razao_social', 'data', 'status'])
            ->make(true);
    }

    /**
     * Pega a lista de empresas que ainda estão com status de aguardando revisão
     *
     * @param string $tipoEmpresa
     *
     * @return mixed
     * @throws \Exception
     */
    public function pegaListaEmpresasParaRevisao($tipoEmpresa = '')
    {

        $idTipoEmpresa = [];

        switch ($tipoEmpresa) {
            case 'pequeno-gerador':
                $idTipoEmpresa[] = config('enums.empresas_tipo.pequeno_gerador');
                break;

            case 'grande-gerador':
                $idTipoEmpresa[] = config('enums.empresas_tipo.grande_gerador');
                break;

            case 'transportador':
                $idTipoEmpresa[] = config('enums.empresas_tipo.transportador');
                break;

            case 'destino-final':
                $idTipoEmpresa[] = config('enums.empresas_tipo.destino_final_reciclado');
                break;

            case 'cooperativa':
                $idTipoEmpresa[] = config('enums.empresas_tipo.cooperativa_residuos');
                break;
            case 'condominio-misto':
                $idTipoEmpresa[] = config('enums.empresas_tipo.condominio_misto');
                break;

            case 'servico-saude':
                $idTipoEmpresa[] = config('enums.empresas_tipo.servico_saude');
                break;

            case 'orgao-publico':
                $idTipoEmpresa[] = config('enums.empresas_tipo.orgao_publico');
                break;

        }

        $empresas = Empresa::whereIn(
            'status_id',
            [config('enums.status.aguardando_revisao'),
            config('enums.status.revisado')
            ]
        )
        ->with('empresas_x_empresas_tipos:empresa_id,empresa_tipo_id')
        ->whereHas('empresas_x_empresas_tipos', function ($query) use ($idTipoEmpresa) {
            $query->whereIn('empresa_tipo_id', $idTipoEmpresa);
        })->get();

        return Datatables::of($empresas)
            ->addColumn('cnpj', function ($results) {
                return "<div class=\"cnpj\">{$results->cnpj}</div>";
            })
            ->addColumn('razao_social', function ($results) {
                return "<div class=\"razao_social\">{$results->razao_social}</div>";
            })
            ->addColumn('data', function ($results) {
                $data = decodeDate($results->updated_at);
                return "<div class=\"data\">{$data}</div>";
            })
            ->addColumn('status', function ($results) {
                return "<div class=\"statusAprovacao\">{$results->status->descricao}</div>";
            })
            ->rawColumns(['cnpj', 'razao_social', 'data', 'status'])
            ->make(true);
    }

    public function exibirDetalhesEmpresa()
    {
        $retornoAjax['status'] = false;
        $idEmpresa = (int)Input::get('empresa');

        $empresa = Empresa::where('id', $idEmpresa)
            ->with('empresa_informacao_complementar')
            ->with('empresa_endereco')
            ->with(['empresas_x_empresas_tipos' => function($empresa){
                $empresa->where('status_id', config('enums.status.ativo'));
            }])
            ->first();

        if ($empresa) {
            $documentos = array();
            $getDocumentos = EmpresaDocumentos::where('empresa_id', $empresa->id)->get();
            foreach ($getDocumentos as $value) {
                $documentos[$value->documentos->nome] = $value->caminho_arquivo;
                
                if (!empty($value->data_emissao))
                    $documentos[$value->documentos->nome.'_data_emissao'] = $value->data_emissao;
                if (!empty($value->data_vencimento))
                    $documentos[$value->documentos->nome.'_data_vencimento'] = $value->data_vencimento;

                if (strpos($value->documentos->nome, 'licenca_')){
                    switch ($value->documentos->id) {
                        case 17:
                            $documentos['licencadefault'] = 'Licença Prévia';
                        break;

                        case 18:
                            $documentos['licencadefault'] = 'Licença de Instalação';
                        break;

                        case 19:
                            $documentos['licencadefault'] = 'Licença de Operação';
                        break;
                    }
                    $documentos['licenca'] = $value->caminho_arquivo;
                    $documentos['licenca_data_emissao'] = $value->data_emissao;
                    $documentos['licenca_data_vencimento'] = $value->data_vencimento;
                }
            }

            $responsaveis = array(); $x = 2;
            $getResponsaveis = EmpresaResponsavel::where('empresa_id', $empresa->id)->get();
            foreach ($getResponsaveis as $value) {
                $key = $value->responsavel;
                if ($key == 'socio') {
                    $key = $key.$x;
                    $x++;
                }
                $responsaveis[$key]['nome'] = $value->nome;
                $responsaveis[$key]['email'] = $value->email;
                $responsaveis[$key]['rg'] = $value->rg;
                $responsaveis[$key]['cpf'] = $value->cpf;
                $responsaveis[$key]['telefone'] = $value->telefone;
            }

            $showInfoCompl = $showRespEmp = $showDocs = $showPres = $showSind = false;
            $idTipoEmpresa = $empresa->empresas_x_empresas_tipos->empresa_tipo_id;
            $semInfoCompl = array(
                config('enums.empresas_tipo.transportador'),
                config('enums.empresas_tipo.destino_final_reciclado'),
                config('enums.empresas_tipo.cooperativa_residuos')
            );
            if (!in_array($idTipoEmpresa, $semInfoCompl))
                $showInfoCompl = true;
            $comRespEmp = array(
                config('enums.empresas_tipo.transportador'),
                config('enums.empresas_tipo.destino_final_reciclado')
            );
            if (in_array($idTipoEmpresa, $comRespEmp))
                $showRespEmp = $showDocs = true;
            $comPres = array(
                config('enums.empresas_tipo.cooperativa_residuos')
            );
            if (in_array($idTipoEmpresa, $comPres))
                $showPres = $showDocs = true;
            $comSind = array(
                config('enums.empresas_tipo.condominio_misto')
            );
            if (in_array($idTipoEmpresa, $comSind))
                $showSind = true;
            $retornoAjax['view'] = view('painel.fiscal.aprovacao.detalhes', compact('empresa', 'showInfoCompl', 'documentos', 'showRespEmp', 'showDocs', 'showPres', 'responsaveis', 'showSind'));
            $retornoAjax['view'] = $retornoAjax['view']->render();
            $retornoAjax['status'] = true;
        }

        return response()->json($retornoAjax);
    }

    public function exibirDetalhesEmpresaRevisao()
    {
        $retornoAjax['status'] = false;
        $idEmpresa = (int)Input::get('empresa');

        $empresa = Empresa::where('id', $idEmpresa)
            ->with(['empresa_informacao_complementar', 'empresa_endereco', 'empresas_x_empresas_tipos'])
            ->first();

        if ($empresa) {
            
            $documentos= array();
          
            $getDocumentos = EmpresaDocumentos::where('empresa_id', $idEmpresa)->get();
            foreach ($getDocumentos as $value) {
                $documentos[$value->documentos->nome] = $value->caminho_arquivo;
                
                if (!empty($value->data_emissao)) {
                    $documentos[$value->documentos->nome.'_data_emissao'] = $value->data_emissao;
                }
                  
                if (!empty($value->data_vencimento)) {
                    $documentos[$value->documentos->nome.'_data_vencimento'] = $value->data_vencimento;
                }
                   

                if (strpos($value->documentos->nome, 'licenca_')) {
                    $documentos['licenca'] = $value->caminho_arquivo;
                    $documentos['licenca_data_emissao'] = $value->data_emissao;
                    $documentos['licenca_data_vencimento'] = $value->data_vencimento;
                }
            }
         
            $empresa_alteracao = EmpresaAlteracaoJson::where('empresa_id', $idEmpresa)->get()->last();
            $empresa_alteracao_id = $empresa_alteracao->id;
         
            $justificativa_empresa_alteracao = JustificativaEmpresaAlteracao::where('empresa_alteracao_id', $empresa_alteracao_id)->orderBy('created_at', 'desc')->first();
            $justificativa_id = $justificativa_empresa_alteracao->justificativa_id;

            $justificativas = Justificativa::find($justificativa_id);

            $idTipoEmpresa = $empresa->empresas_x_empresas_tipos->empresa_tipo_id;
            $semInfoCompl = array(
                config('enums.empresas_tipo.transportador'),
                config('enums.empresas_tipo.destino_final_reciclado'),
                config('enums.empresas_tipo.cooperativa_residuos')
            );
           

            $showInfoCompl = $showRespEmp = $showDocs = $showPres = $showSind = false;
            $idTipoEmpresa = $empresa->empresas_x_empresas_tipos->empresa_tipo_id;
            $semInfoCompl = array(
                config('enums.empresas_tipo.transportador'),
                config('enums.empresas_tipo.destino_final_reciclado'),
                config('enums.empresas_tipo.cooperativa_residuos')
            );
            if (!in_array($idTipoEmpresa, $semInfoCompl)) {
                $showInfoCompl = true;
            }
                
            $comRespEmp = array(
                config('enums.empresas_tipo.transportador'),
                config('enums.empresas_tipo.destino_final_reciclado')
            );
            if (in_array($idTipoEmpresa, $comRespEmp)) {
                $showRespEmp = $showDocs = true;
            }
                
            $comPres = array(
                config('enums.empresas_tipo.cooperativa_residuos')
            );
            if (in_array($idTipoEmpresa, $comPres)) {
                $showPres = $showDocs = true;
            }
                
            $comSind = array(
                config('enums.empresas_tipo.condominio_misto')
            );
            if (in_array($idTipoEmpresa, $comSind)) {
                $showSind = true;
            }
                
            $empresa_alteracao  = json_decode($empresa_alteracao->json_data, true);
            if (isset($empresa_alteracao['dados_empresa']['licencadefault'])) {
                switch ($empresa_alteracao['dados_empresa']['licencadefault']) {
                    case 17:
                        $empresa_alteracao['dados_empresa']['licencadefault'] = 'Licença Prévia';
                    break ;
                    case 18:
                        $empresa_alteracao['dados_empresa']['licencadefault'] = 'Licença de Instalação';
                    break;
                    case 19:
                        $empresa_alteracao['dados_empresa']['licencadefault'] = 'Licença de Operação';
                    break;
                }
            }
           
            //se tiver frequencia de geracao
            if (isset($empresa_alteracao['dados_empresa']['frequencia_geracao'])) {
                $empresa_alteracao['dados_empresa']['frequencia_geracao']= FrequenciaGeracao::where('id', $empresa_alteracao['dados_empresa']['frequencia_geracao'])->first()->nome;
            }

            //se tiver frequencia de geracao
            if (isset($empresa_alteracao['dados_empresa']['frequencia_coleta'])) {
                $empresa_alteracao['dados_empresa']['frequencia_coleta']= FrequenciaColeta::where('id', $empresa_alteracao['dados_empresa']['frequencia_coleta'])->first()->nome;
            }

            //se tiver numero de colaboradores
            if (isset($empresa_alteracao['dados_empresa']['colaboradores_numero'])) {
                $empresa_alteracao['dados_empresa']['colaboradores_numero']= ColaboradoresNumero::where('id', $empresa_alteracao['dados_empresa']['colaboradores_numero'])->first()->nome;
            }
          
            //se tiver numero de energia_consumo
            if (isset($empresa_alteracao['dados_empresa']['energia_consumo'])) {
                $empresa_alteracao['dados_empresa']['energia_consumo']= EnergiaConsumo::where('id', $empresa_alteracao['dados_empresa']['energia_consumo'])->first()->nome;
            }

            //se tiver numero de estabelecimento_tipo
            if (isset($empresa_alteracao['dados_empresa']['estabelecimento_tipo'])) {
                $empresa_alteracao['dados_empresa']['estabelecimento_tipo']= EstabelecimentoTipo::where('id', $empresa_alteracao['dados_empresa']['estabelecimento_tipo'])->first()->nome;
            }
            $responsaveis = array(); $x = 2;
            $getResponsaveis = EmpresaResponsavel::where('empresa_id', $empresa->id)->get();
            foreach ($getResponsaveis as $value) {
                $key = $value->responsavel;
                if ($key == 'socio') {
                    $key = $key.$x;
                    $x++;
                }
                $responsaveis[$key]['nome'] = $value->nome;
                $responsaveis[$key]['email'] = $value->email;
                $responsaveis[$key]['rg'] = $value->rg;
                $responsaveis[$key]['cpf'] = $value->cpf;
                $responsaveis[$key]['telefone'] = $value->telefone;
            }
           
            $retornoAjax['view'] = view('painel.fiscal.aprovacao.detalhes-revisao',
                compact('empresa', 'empresa_alteracao', 'justificativas', 'showInfoCompl', 'showSind', 'showPres', 'showDocs', 'showRespEmp', 'documentos', 'responsaveis')
            );
            $retornoAjax['view'] = $retornoAjax['view']->render();
            $retornoAjax['status'] = true;
        }

        return response()->json($retornoAjax);
    }

    /**
     * Faz a aprovação da empresa pelo funcionário Amlurb
     *
     * @param Request        $request
     * @param ServiceBoletos $serviceBoletos
     * @param $tipo  - aprovacao | revisao
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function aprovarCadastro(Request $request, ServiceBoletos $serviceBoletos, $tipo = 'aprovacao')
    {
       
        $retornoAjax['status'] = false;
        $retornoAjax['error'] = '';
        $empresaId = $request->input('empresa');

        $empresa = Empresa::with('empresas_x_empresas_tipos', 'empresa_informacao_complementar')->find($empresaId);

        if ($empresaId) {
          
            try {
                DB::beginTransaction();
                if ($tipo == 'revisao') {
                    $this->revisarCadastro($request);
                }
             
               
                $dataValidade = encodeDate($request->input('validade')); # var $dataValidade by Mr.Goose em 04 maio 19 (sábado) afim de NÃO Gerar o Boleto.
             
                if (empty($dataValidade))
                    $dadosBoleto = [];

                $idTipoEmpresa = $empresa->empresas_x_empresas_tipos->empresas_tipo->id;
                $idTipoRamo = $empresa->empresa_informacao_complementar->tipo_ramo_atividade_id;

                $new_password = null;
                if ($idTipoEmpresa != config('enums.empresas_tipo.pequeno_gerador')) {
                   
                    $jid = JustificativaInconsistenciaDados::where('empresa_id', $empresaId)->count();
                    if ($jid == 0) { // gera senha nova para usuario
                        $user = User::where('empresa_id', $empresaId)->first();
                        if (empty($user)) {
                            /*- Se entrar aqui o Cadastro está com Erro, pois não possui Usuário criado. -*/
                            $retornoAjax['mensagem'] = "O Usuário não foi encontrado! \n\n A página irá recarregar, aguarde...";
                            DB::commit();
                            DB::rollback();
                            return response()->json($retornoAjax);
                        } // close if (empty($user)) -- by Mr.Goose in 30 abr 19
                        if ($tipo != 'revisao') {
                            $new_password = str_random(12);
                            $user->password = bcrypt($new_password);
                            $user->save();
                        }
                    }
                }
            
                if ($idTipoEmpresa != config('enums.empresas_tipo.pequeno_gerador')
                    && $idTipoEmpresa != config('enums.empresas_tipo.destino_final_reciclado')
                    && $idTipoEmpresa != config('enums.empresas_tipo.cooperativa_residuos')
                    && $idTipoRamo != config('enums.tipo_atividade.servico_de_administracao_municipal_direta')
                    && $idTipoRamo != config('enums.tipo_atividade.servico_de_administracao_municipal_indireta')) {

                    $parametrosBoleto = ParametrosBoleto::preencheDadosEmpresa($empresa);

                    $empresa->status_id = config('enums.status.pagamento_pendente');
                } else {
                    if ($idTipoEmpresa == config('enums.empresas_tipo.grande_gerador')
                        || $idTipoEmpresa == config('enums.empresas_tipo.orgao_publico')
                        || $idTipoEmpresa == config('enums.empresas_tipo.servico_saude')
                        || $idTipoEmpresa == config('enums.empresas_tipo.condominio_misto')
                    ) {
                        $empresa->status_id = config('enums.status.vinculos_pendentes');
                    } else {
                        $empresa->status_id = config('enums.status.ativo');
                    }
                }

                $dadosEmail = DadosEmail::cadastroAprovado($empresa);
                $dadosEmail['senha'] = $new_password;

                $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($dadosEmail['emailReceptor']));

                $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
                    "email" => 'mailgun_email:mailbox'
                ]);
                if ($validator->fails()) {
                    DB::rollback();
                    $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
                    return response()->json($retornoAjax);
                }
                switch ($idTipoEmpresa) {
                    // casos em que boleto deve ser enviado
                    case config('enums.empresas_tipo.transportador'):
                    case config('enums.empresas_tipo.grande_gerador'):
                    case config('enums.empresas_tipo.condominio_misto'):
                    case config('enums.empresas_tipo.orgao_publico'):
                    case config('enums.empresas_tipo.servico_saude'):
                        if ($idTipoRamo != config('enums.tipo_atividade.servico_de_administracao_municipal_direta')
                            && $idTipoRamo != config('enums.tipo_atividade.servico_de_administracao_municipal_indireta')) {

                            $tipo_servico = config('enums.codigo_tipo_servico.gerador');
                            if ($idTipoEmpresa == config('enums.empresas_tipo.transportador')) {
                                $tipo_servico = config('enums.codigo_tipo_servico.transbordo');
                                if ($idTipoRamo == config('enums.tipo_atividade.transportador_residuos'))
                                    $tipo_servico = config('enums.codigo_tipo_servico.transportador');
                            } // close if ()

                            if (empty($dataValidade)){
                                // Entra aqui para Gerar o Boleto. segue o fluxo que já tinha.
                               
                                $parametrosBoleto = ParametrosBoleto::preencheTipoEmpresa($parametrosBoleto, $tipo_servico);

                                // Se for no ambiente PROD gera o boleto através da API da PRODAM
                                $app_env = config('app.env');
                                
                                if (!is_null($app_env) && in_array($app_env, ['production', 'homolog'])) {
                                    $dadosBoleto = $serviceBoletos->gerarBoleto($parametrosBoleto);
                                    if (empty($dadosBoleto['file'])) {
                                        /*- entra aqui se tive havido problema na Geração do Boleto. -*/
                                        $retornoAjax['status'] = false;
                                        $retornoAjax['mensagem'] = "Problema ao Geraro Boleto. <br/> Código de Gẽração do Boleto: ".$tipo_servico."<br/> Entrar em contato com a Prodam. <br/> A página irá recarregar, aguarde...";
                                        return response()->json($retornoAjax);
                                    } // close if (empty($dadosBoleto['file']))
                                    $boletopdffile = $dadosBoleto['file'];
                                } else { // Se for no ambiente DESENV usa um modelo fake de boleto
                                    $boletopdffile = 'boleto-facil-exemplo.pdf';
                                }
                                
                                $dadosEmail['boleto'] = route('download.boleto', $boletopdffile);
                            } // close if (empty($dataValidade)) - se não tiver Data de Validade, ou seja, deve Gerar o Boleto -
                            else{
                                // entra aqui se NÃO Gerou Boleto.
                                if ($idTipoEmpresa == config('enums.empresas_tipo.grande_gerador')
                                    || $idTipoEmpresa == config('enums.empresas_tipo.orgao_publico')
                                    || $idTipoEmpresa == config('enums.empresas_tipo.servico_saude')
                                    || $idTipoEmpresa == config('enums.empresas_tipo.condominio_misto')
                                ) {
                                    $empresa->status_id = config('enums.status.vinculos_pendentes');
                                } else {
                                    $empresa->status_id = config('enums.status.ativo'); # altera o Status para Ativar a Empresa.
                                }
                              
                                $empresa->validade_cadastro = $dataValidade.' 00:00:00'; # insere a Data de Validade da Empresa para ser exibida.
                                
                                // Salvar Data de Validade da Conta que marca a próxima geração de boleto.
                                $dadosBoleto['dadosGuia'] = (object)array(
                                                                'DataDeValidade'    =>  $dataValidade.' 00:00:00',
                                                                'DataDeVencimento'  =>  date('Y-m-d').' 00:00:00',
                                                                'AnoEmissao'        =>  date('Y'),
                                                                'Valor'             =>  0,
                                                                'Numero'            =>  Auth::user()->id
                                                            );
                                $dadosBoleto['file'] = 'fake';
                                $dadosBoleto['status_id'] = config('enums.status.pago');

                                $dadosEmail['dataValidade'] = $dataValidade;
                            } // close else if (empty($dataValidade)) - se não Gerar o Boleto, ou seja EXISTE Data de Valide -

                        } // entra acima se NÃO for Órgão Público Municipal
                        
                        Mail::to($dadosEmail['emailReceptor'])->send(new CadastroRealizado($dadosEmail));
                       
                        break;

                    case config('enums.empresas_tipo.destino_final_reciclado'):

                        $dadosEmail['view'] = 'emails.cadastros-empresas.cadastro-aprovado-df';
                        Mail::to($dadosEmail['emailReceptor'])->send(new CadastroRealizado($dadosEmail));
                        break;

                    case config('enums.empresas_tipo.cooperativa_residuos'):

                        $dadosEmail['view'] = 'emails.cadastros-empresas.cadastro-aprovado-cr';
                        Mail::to($dadosEmail['emailReceptor'])->send(new CadastroRealizado($dadosEmail));
                        break;
                }

                // Salva o usuário que realizou a operação
                EmpresaAprovacao::firstOrcreate([
                    'empresa_id' => $empresa->id,
                    'user_id' => Auth::user()->id,
                  //  'data_limte_vinculo' => Carbon::now()->addDays(5)
                ]);

                $empresa->save();

                if (isset($dadosBoleto) && !empty($dadosBoleto)) {
                    if (empty($parametrosBoleto['InputMessage']['Input']['CodigoTipoServico']))
                        $parametrosBoleto['InputMessage']['Input']['CodigoTipoServico'] = $tipo_servico;
                    # condição acima, inserido by Mr.Goose in 06 maio 19 afim de tratar qndo NÃO for gerado o Boleto, ou seja, Boleto Fake por Cadastro com Data de Validade.
                    $this->salvarBoleto($dadosBoleto, $empresa->id, $parametrosBoleto['InputMessage']['Input']['CodigoTipoServico']);
                }
                //envia email sobre vinculos pendentes caso esse seja o status
                if ($empresa->status_id == config('enums.status.vinculos_pendentes')) {
                    EmpresaAprovacao::firstOrcreate([
                        'empresa_id' => $empresa->id,
                        'user_id' => Auth::user()->id,
                        'data_limte_vinculo' => Carbon::now()->addDays(5)
                    ]);
                    Mail::to($empresa->email)->send(new VinculosFaltantes($empresa));
                }
                DB::commit();

                $retornoAjax['status'] = true;
                $retornoAjax['mensagem'] = "A empresa foi aprovada com sucesso. \n\n A página irá recarregar, aguarde...";

            } catch (\Exception $e) {
                Log::error($e);
                DB::rollback();
                $retornoAjax['mensagem'] = $e->getMessage();
            }
        } else {
            $retornoAjax['mensagem'] = "Não conseguimos encontrar esta empresa, atualize a página e tente novamente";
        }

        return response()->json($retornoAjax);
    }

    /**
     * Faz a revisão da solicitação de alteração dos dados da empresa pelo funcionário Amlurb
     *
     * @param Request        $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function revisarCadastro(Request $request, $id_empresa = null)
    {
        $retornoAjax['status'] = false;
        $retornoAjax['error'] = '';
        $empresaId = is_null($id_empresa) ? $request->input('empresa') : $id_empresa;

        if (!empty($empresaId)) {
            DB::beginTransaction();

            try {
                $empresa = Empresa::with('empresas_x_empresas_tipos')->find($empresaId);

                $dadosEmail = [
                    'responsavel' => $empresa->nome_responsavel,
                    'razaoSocial' => $empresa->razao_social,
                    'inscricaoMunicipal' => $empresa->im,
                    'dataEnvio' => date('d/m/Y'),
                    'emailReceptor' => $empresa->email,
                    'subject' => 'CTRE - Cadastro revisado com Sucesso'
                ];

               
                $empresaEndereco = EmpresaEndereco::where('empresa_id', $empresaId)->first();
                $empresaInformacoesComplementares = EmpresaInformacaoComplementars::where('empresa_id', $empresaId)->first();
                
                $empresaAlteracao = EmpresaAlteracaoJson::where('empresa_id', $empresaId)->get()->last();
                $empresa_alteracao  = json_decode($empresaAlteracao->json_data, true);
             
                $this->salvaEnderecoDadosEmpresa($empresa_alteracao, $empresa, $empresaEndereco, $empresaInformacoesComplementares);
                $this->salvaResposavelEmpreendimento($empresa_alteracao, $empresa);
                if (isset($empresa_alteracao['documentos']['numero_rg_socio_file_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['numero_rg_socio_file_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'numero_rg_socio_file_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['numero_rg_socio_file_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['numero_cpf_socio_file_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['numero_cpf_socio_file_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'numero_cpf_socio_file_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['numero_cpf_socio_file_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['numero_cpf_socio_2_file_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['numero_cpf_socio_2_file_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'numero_cpf_socio_2_file_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['numero_cpf_socio_2_file_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['numero_rg_socio_2_file_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['numero_rg_socio_2_file_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'numero_rg_socio_2_file_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['numero_rg_socio_2_file_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }
                if (isset($empresa_alteracao['documentos']['numero_rg_socio_3_file_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['numero_rg_socio_3_file_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'numero_rg_socio_3_file_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['numero_rg_socio_3_file_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }
                if (isset($empresa_alteracao['documentos']['numero_cpf_socio_3_file_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['numero_cpf_socio_3_file_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'numero_cpf_socio_3_file_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['numero_cpf_socio_3_file_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_contrato_social_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_contrato_social_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_contrato_social_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_contrato_social_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }
                if (isset($empresa_alteracao['documentos']['transportador_doc_ccm_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_doc_ccm_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_doc_ccm_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_doc_ccm_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_certidao_falencia_concordata_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_certidao_falencia_concordata_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_certidao_falencia_concordata_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_certidao_falencia_concordata_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_balanco_financeiro_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_balanco_financeiro_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_balanco_financeiro_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_balanco_financeiro_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }
                if (isset($empresa_alteracao['documentos']['transportador_demonstrativo_contabil_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_demonstrativo_contabil_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_demonstrativo_contabil_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_demonstrativo_contabil_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_certidao_negativa_municipal_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_certidao_negativa_municipal_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_certidao_negativa_municipal_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_certidao_negativa_municipal_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_certidao_negativa_estadual_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_certidao_negativa_estadual_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_certidao_negativa_estadual_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_certidao_negativa_estadual_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }
                if (isset($empresa_alteracao['documentos']['transportador_certidao_negativa_federal_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_certidao_negativa_federal_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_certidao_negativa_federal_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                                $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_certidao_negativa_federal_alterado'];
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_anotacao_responsabilidade_tecnica_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_anotacao_responsabilidade_tecnica_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_anotacao_responsabilidade_tecnica_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_anotacao_responsabilidade_tecnica_alterado'];
                            if (isset($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_emissao']) && !empty($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_emissao'])) {
                                    $empresaDocumento->data_emissao = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_emissao'])->format('Y-m-d');
                            }
                                
                            if (isset($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_vencimento']) && !empty($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_vencimento'])) {
                                    $empresaDocumento->data_vencimento = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_vencimento'])->format('Y-m-d');
                            }
                               
                                $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_rg_responsavel_tecnico_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_rg_responsavel_tecnico_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_rg_responsavel_tecnico_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_rg_responsavel_tecnico_alterado'];
                            $empresaDocumento->save();
                        }
                    }
                }
                if (isset($empresa_alteracao['dados_empresa']['rg_reponsavel_tecnico'])) {
                
                    $nomeArquivo = 'transportador_rg_num_responsavel_tecnico_alterado';
                    $documentos = config('enums.documentos_alterado');
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['dados_empresa']['rg_reponsavel_tecnico'];
                            $empresaDocumento->save();
                    }
                }

                if (isset($empresa_alteracao['dados_empresa']['nome_reponsavel_tecnico'])) {
                    $nomeArquivo = 'transportador_nome_responsavel_tecnico_alterado';
                    $documentos = config('enums.documentos_alterado');
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                 $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                     [
                                         'empresa_id' => $empresa->id,
                                         'documento_id' => $documentos[$nomeArquivo],
                                     ]
                                 );
                             $empresaDocumento->caminho_arquivo = $empresa_alteracao['dados_empresa']['rg_reponsavel_tecnico'];
                             $empresaDocumento->save();
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_cpf_responsavel_tecnico_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_cpf_responsavel_tecnico_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_cpf_responsavel_tecnico_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_cpf_responsavel_tecnico_alterado'];
                            $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['dados_empresa']['cpf_responsavel_tecnico'])) {
                
                    $nomeArquivo = 'transportador_cpf_num_responsavel_tecnico_alterado';
                    $documentos = config('enums.documentos_alterado');
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['dados_empresa']['cpf_responsavel_tecnico'];
                            $empresaDocumento->save();
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_certificado_dispensa_licenca_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_certificado_dispensa_licenca_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_certificado_dispensa_licenca_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_certificado_dispensa_licenca_alterado'];
                            
                            if (isset($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_emissao']) && !empty($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_emissao'])) {
                                $empresaDocumento->data_emissao = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_emissao'])->format('Y-m-d');
                            }
                            
                            if (isset($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_vencimento']) && !empty($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_vencimento'])) {
                                $empresaDocumento->data_vencimento = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_vencimento'])->format('Y-m-d');
                            }
                            $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_alvara_prefeitura_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_alvara_prefeitura_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_alvara_prefeitura_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_alvara_prefeitura_alterado'];
                            if (isset($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_emissao']) && !empty($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_emissao'])) {
                                $empresaDocumento->data_emissao = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_emissao'])->format('Y-m-d');
                            }
                            
                            if (isset($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_vencimento']) && !empty($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_vencimento'])) {
                                $empresaDocumento->data_vencimento = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_vencimento'])->format('Y-m-d');
                            }
                            $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_ctf_ibama_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_ctf_ibama_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_ctf_ibama_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_ctf_ibama_alterado'];
                            if (isset($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_emissao']) && !empty($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_emissao'])) {
                                $empresaDocumento->data_emissao = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_ctf_ibama_emissao'])->format('Y-m-d');
                            }
                            
                            if (isset($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_vencimento']) && !empty($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_vencimento'])) {
                                $empresaDocumento->data_vencimento = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_ctf_ibama_vencimento'])->format('Y-m-d');
                            }
                            $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_avcb_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_avcb_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'transportador_avcb_alterado';
                        $documentos = config('enums.documentos_alterado');
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_avcb_alterado'];
                            if (isset($empresa_alteracao['dados_empresa']['transportador_avcb_emissao']) && !empty($empresa_alteracao['dados_empresa']['transportador_avcb_emissao'])) {
                                $empresaDocumento->data_emissao = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_avcb_emissao'])->format('Y-m-d');
                            }
                            
                            if (isset($empresa_alteracao['dados_empresa']['transportador_avcb_vencimento']) && !empty($empresa_alteracao['dados_empresa']['transportador_avcb_vencimento'])) {
                                $empresaDocumento->data_vencimento = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['transportador_avcb_vencimento'])->format('Y-m-d');
                            }
                            $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['empresa_cartao_cnpj_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['empresa_cartao_cnpj_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'empresa_cartao_cnpj_alterado';
                        $documentos = config('enums.documentos_alterado');
                            $nomeArquivo = str_replace('condominio_file_', 'condominio_sindico_', $nomeArquivo);
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['empresa_cartao_cnpj_alterado'];
                            $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['empresa_iptu_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['empresa_iptu_alterado'], $empresa);
                    if ($nomeArquivo) {
                        $nomeArquivo = 'empresa_iptu_alterado';
                        $documentos = config('enums.documentos_alterado');
                            $nomeArquivo = str_replace('condominio_file_', 'condominio_sindico_', $nomeArquivo);
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['empresa_iptu_alterado'];
                            $empresaDocumento->save();
                        }
                    }
                }
                if (isset($empresa_alteracao['dados_empresa']['empresa_num_iptu'])) {
                    $nomeArquivo = 'empresa_num_iptu_alterado';
                    $documentos = config('enums.documentos_alterado');
                            $nomeArquivo = str_replace('condominio_file_', 'condominio_sindico_', $nomeArquivo);
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['dados_empresa']['empresa_num_iptu'];
                            $empresaDocumento->save();
                    }
                }
               
                if (isset($empresa_alteracao['documentos']['condominio_sindico_rg_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['condominio_sindico_rg_alterado'], $empresa);
                    $nomeArquivo = 'condominio_sindico_rg_alterado';
                    $documentos = config('enums.documentos_alterado');
                    if (array_key_exists($nomeArquivo, $documentos)) {
                       
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['condominio_sindico_rg_alterado'];
                            $empresaDocumento->save();
                    }
                }
          
             
                if (isset($empresa_alteracao['documentos']['condominio_sindico_cpf_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['condominio_sindico_cpf_alterado'], $empresa);
                    $nomeArquivo = 'condominio_sindico_cpf_alterado';
                    $documentos = config('enums.documentos_alterado');
                            $nomeArquivo = str_replace('condominio_file_', 'condominio_sindico_', $nomeArquivo);
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['condominio_sindico_cpf_alterado'];
                            $empresaDocumento->save();
                    }
                }
              
                if (isset($empresa_alteracao['documentos']['condominio_ata_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['condominio_ata_alterado'], $empresa);
                    $documentos = config('enums.documentos_alterado');
                    $nomeArquivo = 'condominio_ata_alterado';
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['condominio_ata_alterado'];
                            $empresaDocumento->save();
                    }
                }
                if (isset($empresa_alteracao['documentos']['condominio_contribuicao_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['condominio_contribuicao_alterado'], $empresa);
                    $documentos = config('enums.documentos_alterado');
                    $nomeArquivo = 'condominio_contribuicao_alterado';
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['condominio_contribuicao_alterado'];
                            $empresaDocumento->save();
                    }
                }
                if (isset($empresa_alteracao['documentos']['condominio_procuracao_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['condominio_procuracao_alterado'], $empresa);
                    $documentos = config('enums.documentos_alterado');
                    $nomeArquivo = 'condominio_procuracao_alterado';
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['condominio_procuracao_alterado'];
                            $empresaDocumento->save();
                    }
                }
                

                if (isset($empresa_alteracao['documentos']['licenka_alterado']) && isset($empresa_alteracao['dados_empresa']['licencadefault'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['licenka_alterado'], $empresa);
                    if ($nomeArquivo) {
                        switch ($empresa_alteracao['dados_empresa']['licencadefault']) {
                            case 17:
                                $nomeArquivo = 'transportador_licenca_previa_alterado';
                                break;
                            case 18:
                                $nomeArquivo = 'transportador_licenca_instalacao_alterado';
                                break;
                            case 19:
                                $nomeArquivo = 'transportador_licenca_operacao_alterado';
                                break;
                        }
                        $documentos = config('enums.documentos_alterado');
                            $nomeArquivo = str_replace('condominio_file_', 'condominio_sindico_', $nomeArquivo);
                        if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['licenka_alterado'];
                            if (isset($empresa_alteracao['dados_empresa']['emissaolicenca']) && !empty($empresa_alteracao['dados_empresa']['emissaolicenca'])) {
                                $empresaDocumento->data_emissao = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['emissaolicenca'])->format('Y-m-d');
                            }
                            
                            if (isset($empresa_alteracao['dados_empresa']['licencavencimento']) && !empty($empresa_alteracao['dados_empresa']['licencavencimento'])) {
                                $empresaDocumento->data_vencimento = Carbon::createFromFormat('d/m/Y', $empresa_alteracao['dados_empresa']['licencavencimento'])->format('Y-m-d');
                            }
                            $empresaDocumento->save();
                        }
                    }
                }

                if (isset($empresa_alteracao['documentos']['transportador_crf_caixa_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_crf_caixa_alterado'], $empresa);
                    $documentos = config('enums.documentos_alterado');
                    $nomeArquivo = 'transportador_crf_caixa_alterado';
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_crf_caixa_alterado'];
                            $empresaDocumento->save();
                    }
                }
                if (isset($empresa_alteracao['documentos']['transportador_crea_responsavel_tecnico_alterado'])) {
                    $nomeArquivo =  $this->salvaArquivo($empresa_alteracao['documentos']['transportador_crea_responsavel_tecnico_alterado'], $empresa);
                    $documentos = config('enums.documentos_alterado');
                    $nomeArquivo = 'transportador_crea_responsavel_tecnico_alterado';
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['documentos']['transportador_crea_responsavel_tecnico_alterado'];
                            $empresaDocumento->save();
                    }
                }
                if (isset($empresa_alteracao['dados_empresa']['transportador_crea_num_responsavel_tecnico'])) {
                    $nomeArquivo = 'transportador_crea_num_responsavel_tecnico';
                    $documentos = config('enums.documentos');
                    if (array_key_exists($nomeArquivo, $documentos)) {
                                $empresaDocumento = EmpresaDocumentos::firstOrNew(
                                    [
                                        'empresa_id' => $empresa->id,
                                        'documento_id' => $documentos[$nomeArquivo],
                                    ]
                                );
                            $empresaDocumento->caminho_arquivo = $empresa_alteracao['dados_empresa']['transportador_crea_num_responsavel_tecnico'];
                            $empresaDocumento->save();
                    }
                }

                if (is_null($id_empresa)) {
                    // Salva o usuário que realizou a operação
                    EmpresaAprovacao::firstOrcreate([
                        'empresa_id' => $empresa->id,
                        'user_id' => Auth::user()->id,
                        'data_limte_vinculo' => Carbon::now()->addDays(5)
                    ]);
                } // close if (is_null($id_empresa)) - entra aqui se NÃO for uma Filial Pré Cadastrada

                $empresaAlteracao->reviewer_user_id = Auth::user()->id;
                $empresaAlteracao->save();
                $empresaAlteracao->delete(); // softdelete

                if (is_null($id_empresa)) {
                    $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($dadosEmail['emailReceptor']));

                    $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
                        "email" => 'mailgun_email:mailbox'
                    ]);
                    if ($validator->fails()) {
                        DB::rollback();
                        $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
                        return response()->json($retornoAjax);
                    }
                    $dadosEmail['view'] = 'emails.cadastros-empresas.cadastro-revisado';

                    Mail::to($dadosEmail['emailReceptor'])->send(new CadastroRealizado($dadosEmail));
                } // close if (is_null($id_empresa))

                DB::commit();

                $retornoAjax['status'] = true;
                $retornoAjax['mensagem'] = "A empresa teve seus dados revisados com sucesso. \n\n A página irá recarregar, aguarde...";
            } catch (\Exception $e) {
                DB::rollback();
                Log::error($e);
                throw new \Exception($e->getMessage());
            }
        } else {
            $retornoAjax['mensagem'] = "Não conseguimos encontrar esta empresa, atualize a página e tente novamente";
        }

        return response()->json($retornoAjax);
    }

    private function salvaArquivo($arquivo_nome, $empresa)
    {
        try {
            // Caminho que salvará os arquivos de licenças, criando uma pasta com o id da empresa
            $path_old = 'licencas_temp/' . $empresa->cnpj.'/';
            $path_new = 'licencas/' . $empresa->id.'/';
            $path_old = $path_old.$arquivo_nome;
            $path_new = $path_new.$arquivo_nome;
            $arquivo  = Storage::disk('s3')->get($path_old);
            Storage::disk('s3')->put($path_new, $arquivo);
            return $arquivo_nome;
        } catch (\Exception $e) {
                Log::error($e);
                throw new \Exception("Ocorreu um erro ao Gravar Arquivo no s3");
        }
    }
    private function salvaEnderecoDadosEmpresa($empresa_alteracao, $empresa, $empresaEndereco, $empresaInformacoesComplementares)
    {
        try {

            $blnEmpresaEndereco = $blnEmpresaInformacoesComplementares = false;
            if (isset($empresa_alteracao['dados_empresa']['razao_social'])) {
                $empresa->razao_social = $empresa_alteracao['dados_empresa']['razao_social'];
            }
            if (isset($empresa_alteracao['dados_empresa']['nome_fantasia'])) {
                $empresa->nome_comercial = $empresa_alteracao['dados_empresa']['nome_fantasia'];
            }
            if (isset($empresa_alteracao['dados_empresa']['endereco'])) {
                $empresaEndereco->endereco = $empresa_alteracao['dados_empresa']['endereco'];
                if (!$blnEmpresaEndereco) {
                    $blnEmpresaEndereco = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['numero'])) {
                $empresaEndereco->numero = $empresa_alteracao['dados_empresa']['numero'];
                if (!$blnEmpresaEndereco) {
                    $blnEmpresaEndereco = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['bairro'])) {
                $empresaEndereco->bairro = $empresa_alteracao['dados_empresa']['bairro'];
                if (!$blnEmpresaEndereco) {
                    $blnEmpresaEndereco = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['cep'])) {
                $empresaEndereco->cep = preg_replace('/\D/', '', $empresa_alteracao['dados_empresa']['cep']);
                if (!$blnEmpresaEndereco) {
                    $blnEmpresaEndereco = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['estado'])) {
                $empresaEndereco->estado_id = $empresa_alteracao['dados_empresa']['estado'];
                if (!$blnEmpresaEndereco) {
                    $blnEmpresaEndereco = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['cidade'])) {
                $empresaEndereco->cidade_id = $empresa_alteracao['dados_empresa']['cidade'];
                if (!$blnEmpresaEndereco) {
                    $blnEmpresaEndereco = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['complemento'])) {
                $empresaEndereco->complemento = $empresa_alteracao['dados_empresa']['complemento'];
                if (!$blnEmpresaEndereco) {
                    $blnEmpresaEndereco = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['ponto_referencia'])) {
                $empresaEndereco->ponto_referencia = $empresa_alteracao['dados_empresa']['ponto_referencia'];
                if (!$blnEmpresaEndereco) {
                    $blnEmpresaEndereco = true;
                }
            }
        
            if ($blnEmpresaEndereco) {
                // Pega a geolocalização do endereço da empresa
                $geoLocation = (new GeoLocationController)->geoLocal(
                    $empresaEndereco->endereco . ", " . $empresaEndereco->numero . ", " .
                    Cidade::find($empresaEndereco->cidade_id) . ", " . Estado::find($empresaEndereco->estado_id)
                );

                if ($geoLocation) {
                    $empresaEndereco->latitude = $geoLocation->lat;
                    $empresaEndereco->longitude = $geoLocation->lng;
                }
                $empresaEndereco->save();
            }
            
            if (isset($empresa_alteracao['dados_empresa']['ramo_atividade'])) {
                $empresaEndereco->ramo_atividade_id = $empresa_alteracao['dados_empresa']['ramo_atividade'];
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['tipo_ramo_atividade'])) {
                $empresaEndereco->tipo_ramo_atividade_id = $empresa_alteracao['dados_empresa']['tipo_ramo_atividade'];
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['frequencia_geracao'])) {
                $empresaInformacoesComplementares->frequencia_geracao_id = $empresa_alteracao['dados_empresa']['frequencia_geracao'];
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['frequencia_coleta'])) {
                $empresaInformacoesComplementares->frequencia_coleta_id = $empresa_alteracao['dados_empresa']['frequencia_coleta'];
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }

            if (isset($empresa_alteracao['dados_empresa']['colaboradores_numero'])) {
                $empresaInformacoesComplementares->colaboradores_numero_id = $empresa_alteracao['dados_empresa']['colaboradores_numero'];
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }

            if (isset($empresa_alteracao['dados_empresa']['energia_consumo'])) {
                $empresaInformacoesComplementares->energia_consumo_id = $empresa_alteracao['dados_empresa']['energia_consumo'];
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }
            if (isset($empresa_alteracao['dados_empresa']['estabelecimento_tipo'])) {
                $empresaInformacoesComplementares->estabelecimento_tipo_id = $empresa_alteracao['dados_empresa']['estabelecimento_tipo'];
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }

            if (isset($empresa_alteracao['dados_empresa']['area_total'])) {
                $empresaInformacoesComplementares->area_total = str_replace('.', '', str_replace(',', '', $empresa_alteracao['dados_empresa']['area_total']));
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }

            if (isset($empresa_alteracao['dados_empresa']['area_construida'])) {
                $empresaInformacoesComplementares->area_construida = str_replace('.', '', str_replace(',', '', $empresa_alteracao['dados_empresa']['area_construida']));
                if (!$blnEmpresaInformacoesComplementares) {
                    $blnEmpresaInformacoesComplementares = true;
                }
            }
            
            if ($blnEmpresaInformacoesComplementares) {
                $empresaInformacoesComplementares->save();
            }

            if (isset($empresa_alteracao['dados_empresa']['ie'])) {
                $empresa->ie = $empresa_alteracao['dados_empresa']['ie'];
            }
            if (isset($empresa_alteracao['dados_empresa']['im'])) {
                $empresa->im = $empresa_alteracao['dados_empresa']['im'];
            }
            if (isset($empresa_alteracao['dados_empresa']['telefone_empresa'])) {
                $empresa->telefone = preg_replace('/\D/', '', $empresa_alteracao['dados_empresa']['telefone_empresa']);
            }
            $empresa->save();
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados da Empresa");
        }
    }
    private function salvaResposavelEmpreendimento($empresa_alteracao, $empresa)
    {
        try {
            if (isset($empresa_alteracao['dados_empresa']['nome_socio'])) {
                if (!empty($empresa_alteracao['dados_empresa']['id_socio_1'])) {
                    $responsavel = EmpresaResponsavel::find($empresa_alteracao['dados_empresa']['id_socio_1']);
                } else {
                    $responsavel = EmpresaResponsavel::create(
                        [
                            'empresa_id' => $empresa->id,
                            'responsavel' => 'socio_principal'
                        ]
                    );
                }
                
                $responsavel->nome = $empresa_alteracao['dados_empresa']['nome_socio'];
                if (isset($empresa_alteracao['dados_empresa']['numero_rg_socio'])) {
                    $responsavel->rg = $empresa_alteracao['dados_empresa']['numero_rg_socio'];
                }
                if (isset($empresa_alteracao['dados_empresa']['numero_cpf_socio'])) {
                    $responsavel->cpf = $empresa_alteracao['dados_empresa']['numero_cpf_socio'];
                }
                $responsavel->save();
            }
            if (isset($empresa_alteracao['dados_empresa']['nome_socio_2'])) {
                if (!empty($empresa_alteracao['dados_empresa']['id_socio_2'])) {
                    $responsavel = EmpresaResponsavel::find($empresa_alteracao['dados_empresa']['id_socio_2']);
                } else {
                    $responsavel = EmpresaResponsavel::firstOrNew(
                        [
                            'empresa_id' => $empresa->id,
                            'responsavel' => 'socio'
                        ]
                    );
                }
                
                $responsavel->nome = $empresa_alteracao['dados_empresa']['nome_socio_2'];
                if (isset($empresa_alteracao['dados_empresa']['numero_rg_socio_2'])) {
                    $responsavel->rg = $empresa_alteracao['dados_empresa']['numero_rg_socio_2'];
                }
                if (isset($empresa_alteracao['dados_empresa']['numero_cpf_socio_2'])) {
                    $responsavel->cpf = $empresa_alteracao['dados_empresa']['numero_cpf_socio_2'];
                }
                $responsavel->save();
            }
            if (isset($empresa_alteracao['dados_empresa']['nome_socio_3'])) {
                if (!empty($empresa_alteracao['dados_empresa']['id_socio_3'])) {
                    $responsavel = EmpresaResponsavel::find($empresa_alteracao['dados_empresa']['id_socio_3']);
                } else {
                    $responsavel = EmpresaResponsavel::create(
                        [
                            'empresa_id' => $empresa->id,
                            'responsavel' => 'socio'
                        ]
                    );
                }
                
                $responsavel->nome = $empresa_alteracao['dados_empresa']['nome_socio_3'];
                if (isset($empresa_alteracao['dados_empresa']['numero_rg_socio_3'])) {
                    $responsavel->rg = $empresa_alteracao['dados_empresa']['numero_rg_socio_3'];
                }
                if (isset($empresa_alteracao['dados_empresa']['numero_cpf_socio_3'])) {
                    $responsavel->cpf = $empresa_alteracao['dados_empresa']['numero_cpf_socio_3'];
                }
                $responsavel->save();
            }

            if (isset($empresa_alteracao['dados_empresa']['condominio_sindico'])) {
                if (!empty($empresa_alteracao['dados_empresa']['id_sindico'])) {
                    $responsavel = EmpresaResponsavel::find($empresa_alteracao['dados_empresa']['id_sindico']);
                } else {
                    $responsavel = EmpresaResponsavel::create(
                        [
                            'empresa_id' => $empresa->id,
                            'responsavel' => 'sindico'
                        ]
                    );
                }
                
                $responsavel->nome = $empresa_alteracao['dados_empresa']['condominio_sindico'];
                if (isset($empresa_alteracao['dados_empresa']['condominio_sindico_rg_text'])) {
                    $responsavel->rg = $empresa_alteracao['dados_empresa']['condominio_sindico_rg_text'];
                }
                if (isset($empresa_alteracao['dados_empresa']['condominio_sindico_cpf_text'])) {
                    $responsavel->cpf = $empresa_alteracao['dados_empresa']['condominio_sindico_cpf_text'];
                }
                $responsavel->save();
            }
        } catch (\Exception $e) {
            Log::error($e);
            throw new \Exception("Ocorreu um erro ao salvar os dados de Responsável Empreendimento");
        }
    }

    /**
     * Faz a recusa dos dados da empresa pelo funcionário Amlurb e muda tipo da empresa para Grande Gerador
     *
     * @param Request        $request

     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function recusarCadastro(Request $request, ServiceBoletos $serviceBoletos)
    {
     
     
        $retornoAjax['status'] = false;
        $retornoAjax['error'] = '';
        $empresaId = $request->input('empresa');

        $empresa = Empresa::with('empresas_x_empresas_tipos', 'empresa_informacao_complementar')->find($empresaId);

        if ($empresaId) {
            try {
                DB::beginTransaction();

                $dadosBoleto = [];

                
                $empresa->status_id = config('enums.status.pagamento_pendente');
                $nova_sigla = config('enums.empresas_sigla.grande_gerador');
                $novo_tipo_id = config('enums.empresas_tipo.grande_gerador');
                if ($empresa->empresa_informacao_complementar->ramo_atividade_id == config('enums.ramo_atividade.condominio_misto')) {
                    $nova_sigla = config('enums.empresas_sigla.condominio_misto');
                    $novo_tipo_id = config('enums.empresas_tipo.condominio_misto');
                }
                if ($empresa->empresa_informacao_complementar->ramo_atividade_id == config('enums.ramo_atividade.orgao_publico')) {
                    $nova_sigla = config('enums.empresas_sigla.orgao_publico');
                    $novo_tipo_id = config('enums.empresas_tipo.orgao_publico');
                }
                if ($empresa->empresa_informacao_complementar->ramo_atividade_id == config('enums.ramo_atividade.servico_saude')) {
                    $nova_sigla = config('enums.empresas_sigla.servico_saude');
                    $novo_tipo_id = config('enums.empresas_tipo.servico_saude');
                }

                // salva novo tipo e sigla da empresa
                $empresa->empresas_x_empresas_tipos->update([
                    'empresa_tipo_id' => $novo_tipo_id
                ]);
                $empresa->id_limpurb = str_replace(config('enums.empresas_sigla.pequeno_gerador'), $nova_sigla, $empresa->id_limpurb);
                $empresa->save();

                $parametrosBoleto = ParametrosBoleto::preencheDadosEmpresa($empresa);
                $parametrosBoleto = ParametrosBoleto::preencheTipoEmpresa($parametrosBoleto, config('enums.codigo_tipo_servico.gerador'));

                  
                $dadosEmail = DadosEmail::cadastroAprovado($empresa);

                // Se for no ambiente PROD gera o boleto através da API da PRODAM
                $app_env = config('app.env');
                if (!is_null($app_env) && in_array($app_env, ['production', 'homolog'])) {
                    $dadosBoleto = $serviceBoletos->gerarBoleto($parametrosBoleto);
                    $boletopdffile = $dadosBoleto['file'];
                } else { // Se for no ambiente DESENV usa um modelo fake de boleto
                    $boletopdffile = 'boleto-facil-exemplo.pdf';
                }

                $dadosEmail['boleto'] = route('download.boleto', $boletopdffile);

                $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($dadosEmail['emailReceptor']));

                $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
                    "email" => 'mailgun_email:mailbox'
                ]);
                if ($validator->fails()) {
                    DB::rollback();
                    $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
                    return response()->json($retornoAjax);
                }

                Mail::to($dadosEmail['emailReceptor'])->send(new CadastroRealizado($dadosEmail));

                // Salva o usuário que realizou a operação
                EmpresaAprovacao::create([
                    'empresa_id' => $empresa->id,
                    'user_id' => Auth::user()->id
                ]);

                if ($dadosBoleto) {
                    $this->salvarBoleto($dadosBoleto, $empresa->id, $parametrosBoleto['InputMessage']['Input']['CodigoTipoServico']);
                }

                $msg = "A empresa foi redefinida como {$empresa->empresas_x_empresas_tipos->empresas_tipo->nome}. <br> A página irá recarregar, aguarde...";
                $retornoAjax['status'] = true;
                $retornoAjax['mensagem'] = $msg;

                DB::commit();

            } catch (\Exception $e){
                DB::rollback();
                $retornoAjax['mensagem'] = $e->getMessage();
            }

        } else {
            $retornoAjax['mensagem'] = "Não conseguimos encontrar esta empresa, atualize a página e tente novamente";
        }
        return response()->json($retornoAjax);
    }

    public function reportarInconsistencia(Request $request)
    {
        $retornoAjax['status'] = false;
        $retornoAjax['error'] = '';
        $empresaId = $request->input('empresa');
        $msg_inconsistencia = $request->input('mensagem');

        try{
            $empresa = Empresa::find($empresaId);
            $empresa_tipo_id = $empresa->empresas_x_empresas_tipos->empresa_tipo_id;

            if ($empresaId) {
                DB::beginTransaction();

                try{

                    $new_password = null;
                    $jid = JustificativaInconsistenciaDados::where('empresa_id', $empresaId)->count();
                    if ($jid == 0 &&
                        $empresa_tipo_id != config('enums.empresas_tipo.pequeno_gerador')
                    ) { // gera senha nova para usuario
                        $user = User::where('empresa_id', $empresaId)->first();
                        $new_password = str_random(12);
                        $user->password = bcrypt($new_password);
                        $user->save();
                    }

                    $just = Justificativa::create(['justificativa' => $msg_inconsistencia]);
                    JustificativaInconsistenciaDados::create([
                        'justificativa_id' => $just->id,
                        'fiscal_id' => Auth::user()->id,
                        'empresa_id' => $empresaId
                    ]);

                    // muda status empresa
                    $empresa->status_id = config('enums.status.inconsistente');
                    $empresa->save();

                    $dadosEmail = DadosEmail::inconsistenciasCadasto($empresa, $msg_inconsistencia);

                    $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($dadosEmail['emailReceptor']));
                    
                    $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
                        "email" => 'mailgun_email:mailbox'
                    ]);
                    if ($validator->fails()) {
                        DB::rollback();
                        $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
                        return response()->json($retornoAjax);
                    }

                    $dadosEmail['senha'] = $new_password;
                    Mail::to($dadosEmail['emailReceptor'])->send(new CadastroRealizado($dadosEmail));

                    $retornoAjax['status'] = true;
                    $retornoAjax['mensagem'] = "As informações foram reportadas...";

                    DB::commit();

                } catch (\Exception $e) {
                    DB::rollback();
                    $retornoAjax['mensagem'] = $e->getMessage();
                }

            } else {
                $retornoAjax['mensagem'] = "Não conseguimos encontrar esta empresa, atualize a página e tente novamente";
            }
        } catch (\Exception $e) {
            DB::rollback();
            $retornoAjax['mensagem'] = $e->getMessage();
        }


        return response()->json($retornoAjax);
    }

    static function salvarBoleto($dadosBoleto, $empresa_id, $tipo_servico)
    {
        $data_validade = str_replace('T', ' ', $dadosBoleto['dadosGuia']->DataDeValidade);
        $data_vencimento = str_replace('T', ' ', $dadosBoleto['dadosGuia']->DataDeVencimento);
        $validade = Carbon::createFromFormat('Y-m-d H:i:s', $data_validade);
        $vencimento = Carbon::createFromFormat('Y-m-d H:i:s', $data_vencimento);

        /**
         * status_id = 8 - Aguardando pagamento
         * arquivoBoleto - Nome do arquivo, montar a url - route('download.boleto',$nomeDoArquivo)
         */
        $dadosSalvarBoleto = [
            'empresa_id' => $empresa_id,
            'anoEmissao' => $dadosBoleto['dadosGuia']->AnoEmissao,
            'dataDeValidade' => $validade,
            'dataDeVencimento' => $vencimento,
            'valor' => $dadosBoleto['dadosGuia']->Valor,
            'status_id' => (empty($dadosBoleto['status_id'])) ? config('enums.status.pagamento_pendente') : $dadosBoleto['status_id'],
            'codTipoServGuia' => $tipo_servico,
            'numeroGuia' => $dadosBoleto['dadosGuia']->Numero,
            'arquivoBoleto' => $dadosBoleto['file'],
        ];
        return Boleto::create($dadosSalvarBoleto);
    }

    public function ativarDesativarEmpresa(AtivarDesativarEmpresaRequest $request, $empresa)
    {
        $retornoAjax['status'] = false;
        $idEmpresa = (int)$empresa;

        $empresa = Empresa::find($idEmpresa);

        if ($empresa) {

            try {
                DB::beginTransaction();

                if ($empresa->status_id == config('enums.status.ativo')) {
                    $novoStatus = config('enums.status.inativo');
                } else {
                    $novoStatus = config('enums.status.ativo');
                }

                $empresa->status_id = $novoStatus;
                $empresa->save();

                $nomeStatus = Status::find($novoStatus);

                $log = new EmpresasAtivacaoLog;
                $log->user_id = Auth::user()->id;
                $log->status_id = $novoStatus;
                $log->empresa_id = $empresa->id;
                $log->descricao = $request->descricao;
                $log->save();

                DB::commit();

                $retornoAjax['status'] = true;
                $retornoAjax['mensagem'] = "O status foi atualizado para $nomeStatus->descricao.";

            } catch (\Exception $e) {

                DB::rollback();
                $retornoAjax['mensagem'] = $e->getMessage();

            }

        } else {
            $retornoAjax['mensagem'] = 'Esta empresa não foi encontrada, feche o modal e tente novamente.';
        }

        return response()->json($retornoAjax);
    }

}
