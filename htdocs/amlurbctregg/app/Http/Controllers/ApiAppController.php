<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;
use Illuminate\Http\Request;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ApiAppController extends Controller
{
    public function loginApp(Request $request)
    {

        $credentials = $request->only('email', 'password');

        $credentials['email'] = mb_strtolower($credentials['email']);

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        Auth::attempt($credentials);
       
        $user_id = Auth::user()->id;
        $empresa_id = Auth::user()->empresa_id;
        $usuario_nome = Auth::user()->name;
        // all good so return the token
        $empresa = Empresa::find(Auth::user()->empresa_id);
        if (strpos($empresa->id_limpurb, 'TR') !== false) {
            return response()->json(compact('token', 'user_id', 'empresa_id', 'usuario_nome'), 200);
        } else {
            return response()->json(['error' => 'invalid_credentials'], 401);
        }
    }

    public function getEmpresa(Request $request)
    {
        $empresas = Empresa::where('id_limpurb',
        $request->input('id_limpurb'))->with('empresa_endereco.estado')->get();

        $data_empresa = [];
        foreach ($empresas as $k => $empresa) {
            $data_empresa[$k]['razao_social'] = $empresa->razao_social;
            $data_empresa[$k]['cnpj'] = formatar('cnpj', $empresa->cnpj);
            $data_empresa[$k]['endereco'] = $empresa->empresa_endereco->endereco . ',' . $empresa->empresa_endereco->numero . ',' . $empresa->empresa_endereco->bairro;
            $data_empresa[$k]['estado'] = $empresa->empresa_endereco->estado->sigla;
            $data_empresa[$k]['cep'] = $empresa->empresa_endereco->cep;
        }

        return $data_empresa;
        //return JWTAuth::parseToken()->authenticate();
    }

    public function getVeiculo(Request $request)
    {


        $veiculos = EmpresasVeiculo::where(
            'amlurb_id',
            $request->input('amlurb_id')
        )->with('empresa.empresa_endereco.estado')->get();
        $data_veiculos = [];
        foreach ($veiculos as $k => $veiculo) {
            $data_veiculos[$k]['razao_social'] = $veiculo->empresa->razao_social;
            $data_veiculos[$k]['cnpj'] = formatar('cnpj', $veiculo->empresa->cnpj);
            $data_veiculos[$k]['estado'] = $veiculo->empresa->empresa_endereco->estado->sigla;
            $data_veiculos[$k]['placa'] = $veiculo->placa;
            $data_veiculos[$k]['ano_veiculo'] = $veiculo->ano_veiculo;
            $data_veiculos[$k]['tipo'] = $veiculo->tipo;
            $data_veiculos[$k]['marca'] = $veiculo->marca;
        }

        return $data_veiculos;
        //return JWTAuth::parseToken()->authenticate();
    }
}
