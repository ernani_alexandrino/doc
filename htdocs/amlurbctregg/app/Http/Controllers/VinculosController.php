<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Mail\CadastroRealizado;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresaResponsavel;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\EquipamentoAlocacao;
use Amlurb\Models\FrequenciaColeta;
use Amlurb\Models\Justificativa;
use Amlurb\Models\JustificativaRemocaoVinculo;
use Amlurb\Models\LogisticaResiduos;
use Amlurb\Models\Protocolo;
use Amlurb\Models\Residuo;
use Amlurb\Models\Vinculos;
use Amlurb\Repositories\EmpresaRepository;
use Amlurb\Repositories\EmpresasXEquipamentoRepository;

use Amlurb\Services\DataTables\VinculosRenderer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request as RequestFacade;
use Illuminate\Support\Facades\View;
use Validator;

class VinculosController extends Controller
{
    private $validations = [
        'cnpj' => 'required',
        'razao_social' => 'required',
        'nome_fantasia' => 'required',
        'responsavel' => 'required|min:3|max:50',
        'email' => 'required|email',
    ];

    private $validator_messages = [
        'cnpj.required' => 'CNPJ é obrigatório',
        'razao_social.required' => 'Razão Social é obrigatório',
        'nome_fantasia.required' => 'Nome Fantasia é obrigatório',
        'responsavel.required' => 'Contato Responsável é obrigatório',
        'email.required' => 'Email obrigatório',
        'email.email' => 'Formato de email inválido'
    ];

    /**
     * @var EmpresaRepository
     */
    protected $empresasRepository;

    /**
     * @var EmpresasXEquipamentoRepository
     */
    protected $empresasXequipamentoRepository;

    public function __construct()
    {
        $this->empresasRepository = new EmpresaRepository();
        $this->empresasXequipamentoRepository = new EmpresasXEquipamentoRepository();
    }

    public function vincularEmpresa(Request $request)
    {
        $validator = validator($request->all(), $this->validations, $this->validator_messages);
        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[$k] = $validator->errors()->first($k);
            }

            return response()->json(['error' => $form_error]);
        }

        try {
            $cnpj = formatar('cnpj', $request->input('cnpj'));
            $empresa = $this->empresasRepository->buscaEmpresaPorCnpj($cnpj);
            $tipo_empresa = (
                (!RequestFacade::exists('tipo_empresa')) ? 'transportador' :
                    (($request->input('tipo_empresa') == 'gerador') ? 'gerador' : 'destino final')
            );

            DB::beginTransaction();

            if (!$empresa) { // empresa nova; adiciona no sistema
                // cria empresa e contato responsavel
                $empresa = $this->adicionaNovaEmpresaVinculada($cnpj, $request);

                // verifica tipo da nova empresa
                $tipo_id = (
                (!RequestFacade::exists('tipo_empresa')) ? \config('enums.empresas_tipo.transportador') :
                    (($request->input('tipo_empresa') == 'gerador') ? \config('enums.empresas_tipo.grande_gerador') :
                        \config('enums.empresas_tipo.destino_final_reciclado'))
                );
                $this->adicionaTipoEmpresa($empresa->id, $tipo_id);
            } else { // empresa ja existe; verifica se ja esta vinculada
                $vinculo = Vinculos::where('vinculador_id', Auth::user()->empresa_id)
                    ->where('empresa_vinculada_id', $empresa->id)
                    ->first();

                // vinculo ja existe, retorna sem fazer nenhuma insercao no banco
                if ($vinculo) {
                    $msg = 'O ' . $tipo_empresa . ' de CNPJ ' . $request->input('cnpj') . ' já está vinculado à sua empresa. Por favor verifique sua listagem de cadastros externos.';
                    return response()->json([
                        'error' => ['warning' => $msg]
                    ]);
                }
            } // se nao entrou no IF, empresa ja existe; segue fluxo normal
            if (empty($tipo_id)) {
                 $tipo_id =  \config('enums.empresas_tipo.destino_final_reciclado');
                if ($tipo_empresa == 'transportador') {
                    $tipo_id = \config('enums.empresas_tipo.transportador');
                }
                if ($tipo_empresa == 'gerador') {
                    $tipo_id = \config('enums.empresas_tipo.grande_gerador');
                }
            }
            $er = $this->empresaResponsavel($empresa->id, $request);
            $this->adicionaVinculo($empresa->id, $er->id, $request, $tipo_id);

            // registra coleta ou envio de residuos
            if (!is_null($request->input('residuos_vinculo'))) {
                $residuos = explode(",", $request->input('residuos_vinculo'));
                $this->vinculaResiduos($empresa->id, $residuos);
            }

            // aloca equipamentos selecionados com empresa
            // equipamento nao pode estar alocado com outra empresa
            if (RequestFacade::exists('equipamentos_vinculo')) {
                if (($tipo_empresa != 'destino_final') && (!is_null($request->input('equipamentos_vinculo')))) {
                    $equipamentos = explode(",", $request->input('equipamentos_vinculo'));
                    $this->vinculaEquipamentos($empresa->id, $equipamentos);
                }
            }

            $this->salvaProtocolo(ucfirst($tipo_empresa), Auth::user()->empresa_id, $empresa->id);

            DB::commit();

            $this->emailVinculo($empresa, $er); // verifica possibilidade de enviar email

            return response()->json([
                'success' => 'Vínculo com  ' . $tipo_empresa . ' registrado com sucesso',
                'error' => []
            ]);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.'
            ]);
        }
    }

    //Novo Vinculo de Cooperativa com Condomino Misto
    public function vincularCooperativa(Request $request)
    {

        $validator = validator($request->all(), $this->validations, $this->validator_messages);
        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[$k] = $validator->errors()->first($k);
            }

            return response()->json(['error' => $form_error]);
        }

        try {
            $cnpj = formatar('cnpj', $request->input('cnpj'));
            $cooperativa = $this->empresasRepository->buscaEmpresaPorCnpj($cnpj);

            DB::beginTransaction();

            if (!$cooperativa) { // empresa nova; adiciona no sistema
                // cria empresa
                $cooperativa = $this->adicionaNovaEmpresaVinculada($cnpj, $request);
                $this->adicionaTipoEmpresa($cooperativa->id, config('enums.empresas_tipo.cooperativa_residuos'));
            } else { // empresa ja existe; verifica se ja esta vinculada
                $vinculo = Vinculos::where('vinculador_id', Auth::user()->empresa_id)
                    ->where('empresa_vinculada_id', $cooperativa->id)
                    ->first();

                // vinculo ja existe, retorna sem fazer nenhuma insercao no banco
                if ($vinculo) {
                    $msg = 'A Cooperativa de CNPJ ' . $request->input('cnpj') . ' já está vinculado à sua empresa. Por favor verifique sua listagem de cadastros externos.';
                    return response()->json([
                        'error' => ['warning' => $msg]
                    ]);
                }
            }

            $er = $this->empresaResponsavel($cooperativa->id, $request);
            $this->adicionaVinculo($cooperativa->id, $er->id, $request,  config('enums.empresas_tipo.cooperativa_residuos'));

            // registra coleta ou envio de residuos
            if (!is_null($request->input('residuos_vinculo'))) {
                $residuos = explode(",", $request->input('residuos_vinculo'));
                $this->vinculaResiduos($cooperativa->id, $residuos);
            }

            // aloca equipamentos selecionados com empresa
            // equipamento nao pode estar alocado com outra empresa
            if (RequestFacade::exists('equipamentos_vinculo')) {
                if (!is_null($request->input('equipamentos_vinculo'))) {
                    $equipamentos = explode(",", $request->input('equipamentos_vinculo'));
                    $this->vinculaEquipamentos($cooperativa->id, $equipamentos);
                }
            }

            $nome_empresa = Auth::user()->empresa->empresas_x_empresas_tipos->empresas_tipo->nome;
            $this->salvaProtocolo($nome_empresa, Auth::user()->empresa_id, $cooperativa->id);

            DB::commit();

            return response()->json([
                'success' => 'Vínculo com Cooperativa registrado com sucesso.',
                'error' => []
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.'
            ]);
        }
    }


    public function vincularTransportador(Request $request)
    {
        $validator = validator($request->all(), $this->validations, $this->validator_messages);
        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[$k] = $validator->errors()->first($k);
            }

            return response()->json(['error' => $form_error]);
        }

        try {
            $cnpj = formatar('cnpj', $request->input('cnpj'));
            $transportador = $this->empresasRepository->buscaEmpresaPorCnpj($cnpj);

            DB::beginTransaction();

            if (!$transportador) { // empresa nova; adiciona no sistema
                // cria empresa
                $transportador = $this->adicionaNovaEmpresaVinculada($cnpj, $request);
                $this->adicionaTipoEmpresa($transportador->id, config('enums.empresas_tipo.transportador'));
            } else { // empresa ja existe; verifica se ja esta vinculada
                $vinculo = Vinculos::where('vinculador_id', Auth::user()->empresa_id)
                    ->where('empresa_vinculada_id', $transportador->id)
                    ->first();

                // vinculo ja existe, retorna sem fazer nenhuma insercao no banco
                if ($vinculo) {
                    $msg = 'O Transportador de CNPJ ' . $request->input('cnpj') . ' já está vinculado à sua empresa. Por favor verifique sua listagem de cadastros externos.';
                    return response()->json([
                        'error' => ['warning' => $msg]
                    ]);
                }
            }

            $er = $this->empresaResponsavel($transportador->id, $request);
            $this->adicionaVinculo($transportador->id, $er->id, $request, config('enums.empresas_tipo.transportador'));

            // registra coleta ou envio de residuos
            if (!is_null($request->input('residuos_vinculo'))) {
                $residuos = explode(",", $request->input('residuos_vinculo'));
                $this->vinculaResiduos($transportador->id, $residuos);
            }

            // aloca equipamentos selecionados com empresa
            // equipamento nao pode estar alocado com outra empresa
            if (RequestFacade::exists('equipamentos_vinculo')) {
                if (!is_null($request->input('equipamentos_vinculo'))) {
                    $equipamentos = explode(",", $request->input('equipamentos_vinculo'));
                    $this->vinculaEquipamentos($transportador->id, $equipamentos);
                }
            }

            $nome_empresa = Auth::user()->empresa->empresas_x_empresas_tipos->empresas_tipo->nome;
            $this->salvaProtocolo($nome_empresa, Auth::user()->empresa_id, $transportador->id);

            DB::commit();

            return response()->json([
                'success' => 'Vínculo com Transportador registrado com sucesso.',
                'error' => []
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.'
            ]);
        }
    }

    public function vincularDestinoFinal(Request $request)
    {
        $validator = validator($request->all(), $this->validations, $this->validator_messages);
        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[$k] = $validator->errors()->first($k);
            }

            return response()->json(['error' => $form_error]);
        }

        try {
            $cnpj = formatar('cnpj', $request->input('cnpj'));
            $destino = $this->empresasRepository->buscaEmpresaPorCnpj($cnpj);

            DB::beginTransaction();

            if (!$destino) { // empresa nova; adiciona no sistema
                // cria empresa
                $destino = $this->adicionaNovaEmpresaVinculada($cnpj, $request);
                $this->adicionaTipoEmpresa($destino->id, config('enums.empresas_tipo.destino_final_reciclado'));
            } else { // empresa ja existe; verifica se ja esta vinculada
                $vinculo = Vinculos::where('vinculador_id', Auth::user()->empresa_id)
                    ->where('empresa_vinculada_id', $destino->id)
                    ->first();

                // vinculo ja existe, retorna sem fazer nenhuma insercao no banco
                if ($vinculo) {
                    $msg = 'O Destino Final de CNPJ ' . $request->input('cnpj') . ' já está vinculado à sua empresa. Por favor verifique sua listagem de cadastros externos.';
                    return response()->json([
                        'error' => ['warning' => $msg]
                    ]);
                }
            }

            $er = $this->empresaResponsavel($destino->id, $request);
            $this->adicionaVinculo($destino->id, $er->id, $request, config('enums.empresas_tipo.destino_final_reciclado'));

            // registra coleta ou envio de residuos
            if (!is_null($request->input('residuos_vinculo'))) {
                $residuos = explode(",", $request->input('residuos_vinculo'));
                $this->vinculaResiduos($destino->id, $residuos);
            }

            $nome_empresa = Auth::user()->empresa->empresas_x_empresas_tipos->empresas_tipo->nome;
            $this->salvaProtocolo($nome_empresa, Auth::user()->empresa_id, $destino->id);

            DB::commit();

            return response()->json([
                'success' => 'Vínculo com Destino Final registrado com sucesso.',
                'error' => []
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.'
            ]);
        }
    }

    public function vincularCondominio(Request $request)
    {
        $validator = validator($request->all(), $this->validations, $this->validator_messages);
        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[$k] = $validator->errors()->first($k);
            }

            return response()->json(['error' => $form_error]);
        }

        try {
            $cnpj = formatar('cnpj', $request->input('cnpj'));
            $condominio = $this->empresasRepository->buscaEmpresaPorCnpj($cnpj);

            DB::beginTransaction();

            if (!$condominio) { // empresa nova; adiciona no sistema
                // cria empresa
                $condominio = $this->adicionaNovaEmpresaVinculada($cnpj, $request);
                $this->adicionaTipoEmpresa($condominio->id, config('enums.empresas_tipo.condominio_misto'));
            }

            $er = $this->empresaResponsavel($condominio->id, $request);
            $this->adicionaVinculo($condominio->id, $er->id, $request, config('enums.empresas_tipo.condominio_misto'));
            $nome_empresa = Auth::user()->empresa->empresas_x_empresas_tipos->empresas_tipo->nome;
            $this->salvaProtocolo($nome_empresa, Auth::user()->empresa_id, $condominio->id);

            DB::commit();

            return response()->json([
                'success' => 'Vínculo com Condominio registrado com sucesso. A página irá recarregar, aguarde...',
                'error' => []
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.'
            ]);
        }
    }

    private function adicionaNovaEmpresaVinculada($cnpj, $request)
    {
        // cria empresa
        $empresa = Empresa::create([
            'cnpj' => $cnpj,
            'razao_social' => $request->input('razao_social'),
            'nome_comercial' => $request->input('nome_fantasia'),
            'status_id' => \config('enums.status.inativo')
        ]);

        return $empresa;
    }

    private function adicionaTipoEmpresa($empresa_id, $tipo_id)
    {
        EmpresasXEmpresasTipo::create([
            'empresa_id' => $empresa_id,
            'empresa_tipo_id' => $tipo_id,
            'status_id' => \config('enums.status.ativo')
        ]);
    }

    private function empresaResponsavel($empresa_id, $request)
    {
        $er = EmpresaResponsavel::where('email', $request->input('email'))->first();
        if (!$er) {
            $er = EmpresaResponsavel::create([
                'empresa_id' => $empresa_id,
                'nome' => $request->input('responsavel'),
                'email' => $request->input('email'),
                'telefone' => $request->input('telefone') ?: null
            ]);
        }
        return $er;
    }

    private function adicionaVinculo($empresa_id, $empresa_responsavel_id, $request, $tipo)
    {
        $tipo_id = (
            (!RequestFacade::exists('tipo_empresa')) ? \config('enums.empresas_tipo.transportador') :
                (($request->input('tipo_empresa') == 'gerador') ? \config('enums.empresas_tipo.grande_gerador') :
                        (($request->input('tipo_empresa') == 'cooperativa') ? \config('enums.empresas_tipo.cooperativa_residuos') :
                            \config('enums.empresas_tipo.destino_final_reciclado')))
            );
        $dadosVinculo = [
            'vinculador_id' => Auth::user()->empresa_id,
            'empresa_vinculada_id' => $empresa_id,
            'empresa_responsavel_id' => $empresa_responsavel_id,
            'empresa_vinculada_tipo_id' => $tipo,
            'status_id' => \config('enums.status.ativo')
        ];

        if (RequestFacade::exists('coleta_diaria')) { // geradores x transportador
            $dadosVinculo['frequencia_coleta_id'] = $request->input('coleta_diaria');
        }

        $volume_formatado =  $request->input('volume_residuos') ? explode(",", $request->input('volume_residuos')) : null;


        if (RequestFacade::exists('volume_residuos')) { // transportador x destino final
            $dadosVinculo['volume_residuo'] = $volume_formatado[0];
        }

        // vincula gerador com transportador
        Vinculos::create($dadosVinculo);
    }

    private function vinculaResiduos($empresa_id, $residuos)
    {
        // registra coleta ou envio de residuos
        foreach ($residuos as $residuo_id) {
            $lr = LogisticaResiduos::where('empresa_id', $empresa_id)
                ->where('vinculador_id', Auth::user()->empresa_id)
                ->where('residuo_id', $residuo_id)->first();

            if (!$lr) { // cria vinculo se nao existir
                LogisticaResiduos::create([
                    'vinculador_id' => Auth::user()->empresa_id,
                    'empresa_id' => $empresa_id,
                    'residuo_id' => $residuo_id
                ]);
            }
        }
    }

    private function vinculaEquipamentos($empresa_id, $equipamentos)
    {
        foreach ($equipamentos as $equipamento_id) {
            EquipamentoAlocacao::create([
                'equipamento_id' => $equipamento_id,
                'empresa_id' => $empresa_id
            ]);
        }
    }

    // registro de vinculo
    private function salvaProtocolo($nome_empresa, $vinculador_id, $vinculado_id)
    {
        Protocolo::create([
            'empresa_id' => $vinculador_id,
            'nome' => 'CADASTRO DE VINCULO - ' . strtoupper($nome_empresa),
            'descricao' => $nome_empresa . ' ' . $vinculador_id . ' cadastrou vinculo com empresa ' . $vinculado_id
        ]);
    }

    private function emailVinculo(Empresa $empresa, $empresa_responsavel)
    {
        // verifica se empresa vinculada esta cadastrada no CTRE
        if (is_null($empresa->id_limpurb)) {
            $tipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
            ->first()->empresa_tipo_id;

            $empresa_tipo_id = $this->empresasRepository->getEmpresaTipoById(Auth::user()->empresa_id);
            $tipo_vinculador = (
            ($empresa_tipo_id == \config('enums.empresas_tipo.transportador')) ? 'transportador' :
                (($empresa_tipo_id == \config('enums.empresas_tipo.destino_final_reciclado')) ?
                    'destino final' : 'gerador')
            );
            $tipo_vinculado = (
            ($tipo == \config('enums.empresas_tipo.transportador')) ? 'transportador' :
                (($tipo == \config('enums.empresas_tipo.destino_final_reciclado')) ?
                    'destino final' : 'gerador')
            );

            // envia email informando sobre o cadastro
            $dadosEmail = [
                'subject' => 'CTRE - Sua empresa foi adicionada ao sistema',
                'view' => 'emails.vinculos.cadastro-externo',
                'dataEnvio' => date('d/m/Y'),
                'responsavel' => $empresa_responsavel->nome,
                'tipo_empresa' => strtoupper($tipo_vinculador),
                'tipo_empresa_vinculada' => strtoupper($tipo_vinculado),
                'vinculador_razao_social' => Auth::user()->empresa->razao_social,
                'vinculador_cnpj' => Auth::user()->empresa->cnpj,
                'vinculador_email' => Auth::user()->email,
                'vinculador_telefone' => Auth::user()->empresa->telefone,
                'vinculador_responsavel' => Auth::user()->name
            ];

            $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($empresa_responsavel->email));
            
            $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
                "email" => 'mailgun_email:mailbox'
            ]);
            if ($validator->fails()) {
                $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
                throw new \Exception($retornoAjax['erro']);
            }
            Mail::to($dadosEmail['emailReceptor'])->queue(new CadastroRealizado($dadosEmail));
        }
    }


    // retorna formulario de edicao da empresa que foi vinculada
    public function show($empresa_id)
    {
        $vinculo = $this->empresasRepository->getVinculoByEmpresaVinculadaId($empresa_id);
        $empresa_tipo = $this->empresasRepository->getEmpresaTipoById($empresa_id);

        // dados comuns a geradores e a destinos finais
        $residuos = Residuo::all();

        if ($empresa_tipo == config('enums.empresas_tipo.destino_final_reciclado')) {
            // vinculo com destino final
            $html = View::make('painel.vinculos.modais.destino-final-edit', compact(
                'vinculo',
                'residuos'
            ))->render();
        } else {
            $frequenciaColeta = FrequenciaColeta::all();
            // so traz equipamentos que ainda nao foram vinculados a nenhum gerador
            $equipamentos = $this->empresasXequipamentoRepository->getEquipamentosNaoAlocadosByIdEmpresa(Auth::user()->empresa_id);
            $equipamentos_alocados = $this->empresasXequipamentoRepository->getEquipamentosAlocadosToIdEmpresa($empresa_id);

            /*$blade_template = ($empresa_tipo == \config('enums.empresas_tipo.transportador')) ?
                'painel.vinculos.modais.transportador-edit' : 'painel.transportador.clientes-fornecedores.modais.editGerador';*/

            $blade_template = '';
            if($empresa_tipo == \config('enums.empresas_tipo.transportador')){

                $blade_template = 'painel.vinculos.modais.transportador-edit';
            } elseif ($empresa_tipo == \config('enums.empresas_tipo.cooperativa_residuos')) {

                $blade_template = 'painel.vinculos.modais.cooperativa-edit';
            } else {

                $blade_template = 'painel.transportador.clientes-fornecedores.modais.editGerador';
            }

            $html = View::make($blade_template, compact(
                'vinculo',
                'frequenciaColeta',
                'equipamentos',
                'equipamentos_alocados',
                'residuos'
            ))->render();
        }

        return response()->json([
            'html' => $html
        ]);
    }

    // usa o id da empresa, NAO do vinculo
    public function update(Request $request, $empresa_id)
    {
        $messages = [
            'responsavel.required' => 'Contato Responsável é obrigatório',
            'email.email' => 'Email inválido'
        ];
        $validator = validator($request->all(), [
            'responsavel' => 'required|min:3|max:50',
            'email' => 'email'
        ], $messages);

        if ($validator->fails()) {
            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[$k] = $validator->errors()->first($k);
            }

            return response()->json(['error' => $form_error]);
        }

        try {
            DB::beginTransaction();

            $vinculo = Vinculos::where('empresa_vinculada_id', $empresa_id)
                ->where('vinculador_id', Auth::user()->empresa_id)->first();

            // atualiza frequencia de coleta e/ou volume de residuos
            if (RequestFacade::exists('coleta_diaria')) {
                $vinculo->frequencia_coleta_id = $request->input('coleta_diaria');
            }
            if (RequestFacade::exists('volume_residuos')) {
                $vinculo->volume_residuo = (!is_null($request->input('volume_residuos'))) ?
                    $vinculo->volume_residuo = str_replace(',', '.', ($request->input('volume_residuos'))) : null;
            }
            $vinculo->save();

            // atualiza equipamentos e residuos
            if (RequestFacade::exists('equipamentos_vinculo')) {
                $this->removeAlocacoesEquipamentos($empresa_id);

                if (!is_null($request->input('equipamentos_vinculo'))) {
                    $equipamentos = array_unique(explode(",", $request->input('equipamentos_vinculo')));
                    // vincula novos equipamentos
                    foreach ($equipamentos as $equipamento_id) {
                        EquipamentoAlocacao::create([
                            'equipamento_id' => $equipamento_id,
                            'empresa_id' => $empresa_id
                        ]);
                    }
                }
            }
            if (RequestFacade::exists('residuos_vinculo')) {
                $this->removeVinculosResiduos($empresa_id);

                if (!is_null($request->input('residuos_vinculo'))) {
                    $residuos = array_unique(explode(",", $request->input('residuos_vinculo')));
                    foreach ($residuos as $residuo_id) {
                        LogisticaResiduos::create([
                            'vinculador_id' => Auth::user()->empresa_id,
                            'empresa_id' => $empresa_id,
                            'residuo_id' => $residuo_id
                        ]);
                    }
                }
            }

            // atualiza responsavel
            $er = EmpresaResponsavel::whereId($vinculo->empresa_responsavel_id)->first();
            if ($er->email != $request->input('email')) {
                $er = EmpresaResponsavel::create([
                    'empresa_id' => $empresa_id,
                    'nome' => $request->input('responsavel'),
                    'email' => $request->input('email'),
                    'telefone' => $request->input('telefone') ?: null
                ]);
                $vinculo->empresa_responsavel_id = $er->id;
                $vinculo->save();
            } else {
                $er->nome = $request->input('responsavel');
                $er->telefone = $request->input('telefone');
                $er->save();
            }

            $nome_empresa = Auth::user()->empresa->empresas_x_empresas_tipos->empresas_tipo->nome;

            // registro da edição
            Protocolo::create([
                'empresa_id' => Auth::user()->empresa_id,
                'nome' => 'EDIÇÃO DE VINCULO - ' . strtoupper($nome_empresa),
                'descricao' => $nome_empresa . ' ' . Auth::user()->empresa_id . ' editou vinculo com empresa ' . $empresa_id
            ]);

            DB::commit();
            return response()->json([
                'success' => 'Vínculo atualizado',
                'error' => []
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.' . $e->getMessage()
            ]);
        }
    }

    // remove vinculo entre duas empresas
    public function delete(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $vinculo = Vinculos::where('empresa_vinculada_id', $id)->where('vinculador_id', Auth::user()->empresa_id)->first();

            $this->removeAlocacoesEquipamentos($vinculo->empresa_vinculada_id);

            // remove vinculo com empresa
            $vinculo->delete(); // soft delete

            // registra justificativa e associa ao vinculo
            $justificativa = Justificativa::create([
                'justificativa' => $request->input('justificativa')
            ]);
            JustificativaRemocaoVinculo::create([
                'justificativa_id' => $justificativa->id,
                'vinculo_id' => $vinculo->id
            ]);

            DB::commit();

            $vinculo = Vinculos::select('empresa_vinculada_tipo_id')->where('vinculador_id', Auth::user()->empresa_id)->get();

            foreach($vinculo AS $total){
    
                if($total->empresa_vinculada_tipo_id == 5){
                    $coop = 'ok';
                } elseif($total->empresa_vinculada_tipo_id == 1){
                    $ger = 'ok';
                } elseif($total->empresa_vinculada_tipo_id == 3) {
                    $transp = 'ok';
                }
            }
    
            if($coop != 'ok' || $ger != 'ok' || $transp != 'ok'){
            
                $emp = Empresa::where('id', Auth::user()->empresa_id)->first();
                $emp->status_id = 18;
                $emp->save();            
            }    

            return response()->json([
                'success' => 'Vínculo com Empresa foi removido',
                'error' => []
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.' . $e->getMessage()
            ]);
        }
    }

    // restaura vinculo entre duas empresas
    public function restore($id)
    {
        try {
            DB::beginTransaction();

            $vinculo = Vinculos::withTrashed()->find($id);
            $vinculo->deleted_at = null;

            $empresa_tipo = $vinculo->empresa_vinculada->empresas_x_empresas_tipos->empresas_tipo;
            if ($empresa_tipo->id == config('enums.empresas_tipo.condominio_misto')) {
                // verifica se ja existe outro condominio misto vinculado // so pode haver um
                $cm = $this->empresasRepository->getCondominioMistoVinculado(Auth::user()->empresa_id);
                if (!is_null($cm)) {
                    return response()->json([
                        'warning' => 'Você já possui um Condomínio Misto associado à sua empresa. Remova este vínculo para poder restaurar o outro.'
                    ]);
                }
            }

            $vinculo->save();

            DB::commit();
            return response()->json([
                'success' => 'Vínculo com ' . $empresa_tipo->nome . ' foi restaurado',
                'empresa' => $empresa_tipo->nome,
                'error' => []
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json([
                'erro' => 'Ocorreu um erro. Por favor, tente novamente.' . $e->getMessage()
            ]);
        }
    }

    public function listGeradoresVinculados()
    {
        $empresa_id = Auth::user()->empresa_id;        

        return VinculosRenderer::renderVinculosGeradores(
            $this->empresasRepository->getGeradoresVinculadosCompleto($empresa_id)
        );
    }

    public function listTransportadoresVinculados()
    {
        $empresa_id = Auth::user()->empresa_id;
        return VinculosRenderer::renderVinculosTransportadores(
            $this->empresasRepository->getTransportadoresVinculadosCompleto($empresa_id)
        );
    }

    public function listCooperativasVinculadas()
    {
        $empresa_id = Auth::user()->empresa_id;
        return VinculosRenderer::renderVinculosCooperativas(
            $this->empresasRepository->getCooperativasVinculadasCompleto($empresa_id)
        );
    }

    public function listDestinosVinculados()
    {
        $empresa_id = Auth::user()->empresa_id;
        return VinculosRenderer::renderVinculosDestinosFinais(
            $this->empresasRepository->getDestinosFinaisVinculadosCompleto($empresa_id)
        );
    }

    public function listVinculosRemovidos()
    {
        $vinculos = Vinculos::whereHas('vinculador', function ($query) {
            $query->where('vinculador_id', '=', Auth::user()->empresa_id);
        })->with(['empresa_vinculada' => function ($query) {
            $query->with('status');
        }])->onlyTrashed()->get();

        return VinculosRenderer::renderVinculosRemovidos($vinculos);
    }

    private function removeAlocacoesEquipamentos($empresa_id)
    {
        EquipamentoAlocacao::whereHas('empresas_x_equipamento', function ($query) {
            $query->where('empresa_id', Auth::user()->empresa_id);
        })->where('empresa_id', $empresa_id)->delete();
    }

    private function removeVinculosResiduos($empresa_id)
    {
        LogisticaResiduos::where('empresa_id', $empresa_id)
            ->where('vinculador_id', Auth::user()->empresa_id)->delete();
    }
}
