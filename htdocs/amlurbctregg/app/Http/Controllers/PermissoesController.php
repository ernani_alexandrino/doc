<?php

namespace Amlurb\Http\Controllers;

use Illuminate\Contracts\Logging\Log;
use Amlurb\Models\Perfil;
use Amlurb\Models\TiposEmail;
use Amlurb\Models\Permission;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class PermissoesController extends Controller
{

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $perfil = [
                'nome' => $request->nome,
                'empresa_id' => $request->user()->empresa_id,
                'acesso' => '[]'
            ];

            $permissions = [];

            if ($request->permissions) {
                foreach ($request->permissions as $permission) {
                    if ($request->{$permission}) {
                        $permissions[$permission] = $request->{$permission};
                    }
                }
            }

            $perfil['acesso'] = json_encode($permissions);
            $perfilCreated = Perfil::create($perfil);

            if ($request->emails) {
                $perfilCreated->emails()->attach($request->emails);
            }

            DB::commit();

            return Redirect::back()->with([
                'messages' => [
                    'type' => 'success',
                    'message' => 'Perfil cadastrado com sucesso!'
                ]
            ]);

        } catch (Exception $e) {

            DB::rollback();
            Log::error($e);
            return Redirect::back()->with([
                'messages' => [
                    'type' => 'danger',
                    'message' => 'Erro ao tentar cadastrar o perfil!'
                ]
            ]);
        }
    }

    public function edit($id)
    {
        $userTipo = EmpresasXEmpresasTipo::where('empresa_id', Auth::user()->empresa_id)->get();

        switch ($userTipo[0]->empresa_tipo_id) {
            case 1:
            case 2:
                $type = 'gerador';
                break;

            case 3:
                $type = 'transportador';
                break;

            case 4:
                $type = 'destinofinal';
                break;

            case 5:
                $type = 'cooperativa';
                break;

            case 6:
            case 7:
                $type = 'fiscal';
                break;

            default:
                $type = '';
                break;
        }

        $permissions = Permission::with('actions')->where('type', $type)->get();
        $tiposEmail = TiposEmail::all();
        $empresaID = Auth::user()->empresa_id;
        $perfil = Perfil::with('emails')->where('empresa_id', $empresaID)->find($id);
        $perfil->acesso = json_decode($perfil->acesso, true);
        $perfil->emails = $perfil->emails->pluck('id', 'id');
        $cols = 0;

        foreach ($permissions as $permission) {
            if ($permission->actions->count() > $cols) {
                $cols = $permission->actions->count();
            }
        }

        return view("painel.fiscal.configuracoes.modais.editarPerfil", compact('permissions', 'tiposEmail', 'perfil', 'cols'));
    }

    public function update($id, Request $request)
    {
        DB::beginTransaction();

        try {

            $empresaID = $request->user()->empresa_id;
            $perfilUpdated = Perfil::where('empresa_id', $empresaID)->find($id);

            $perfil = [
                'nome' => $request->nome,
                'acesso' => '[]'
            ];

            $permissions = [];

            if ($request->permissions) {
                foreach ($request->permissions as $permission) {
                    if ($request->{$permission}) {
                        $permissions[$permission] = $request->{$permission};
                    }
                }
            }

            $perfil['acesso'] = json_encode($permissions);
            $perfilUpdated->update($perfil);

            if ($request->emails) {
                $perfilUpdated->emails()->sync($request->emails);
            }

            DB::commit();

            return Redirect::back()->with([
                'messages' => [
                    'type' => 'success',
                    'message' => 'Perfil atualizado com sucesso!'
                ]
            ]);
        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return Redirect::back()->with([
                'messages' => [
                    'type' => 'danger',
                    'message' => 'Erro ao tentar cadastrar o perfil!'
                ]
            ]);
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            $empresaID = Auth::user()->empresa_id;
            $perfil = Perfil::where('empresa_id', $empresaID)->find($id);

            if ($perfil) {
                $perfil->delete();
            } else {
                return Redirect::back()->with([
                    'messages' => [
                        'type' => 'danger',
                        'message' => 'Perfil não encontrado'
                    ]
                ]);
            }

            DB::commit();

            return Redirect::back()->with([
                'messages' => [
                    'type' => 'success',
                    'message' => 'Perfil deletado com sucesso!'
                ]
            ]);

        } catch (Exception $e) {
            DB::rollback();
            Log::error($e);
            return Redirect::back()->with([
                'messages' => [
                    'type' => 'danger',
                    'message' => 'Ocorreu um erro ao deletar o perfil, tente novamente'
                ]
            ]);
        }
    }
}
