<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\EmpresasXEquipamento;
use Amlurb\Models\Justificativa;
use Amlurb\Models\JustificativaExclusaoEquipamento;
use Amlurb\Repositories\EmpresasXEquipamentoRepository;
use Amlurb\Services\DataTables\EquipamentosVeiculosRenderer;
use Amlurb\Services\GeradorCodigos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class EquipamentosController extends Controller
{
    /**
     * @var EmpresasXEquipamentoRepository
     */
    protected $empresasXequipamentoRepository;

    public function __construct()
    {
        $this->empresasXequipamentoRepository = new EmpresasXEquipamentoRepository();
    }

    /**
     * Método que salva o equipamento para o ente que realiza a operação no painel
     * @return \Illuminate\Http\JsonResponse
     */
    public function equipamentoSalvar(Request $request)
    {
        $equipamentoId = $request->input('equipamento');
        $quantidade = $request->input('quantidade');

        // salva equipamento como inativo; necessario ativacao via pagamento de boleto
        $dados = [
            'equipamento_id' => $equipamentoId,
            'empresa_id' => Auth::user()->empresa_id,
            'status_id' => \config('enums.status.inativo')
        ];

        DB::beginTransaction();
        try {
            // salva equipamentos de acordo com quantidade selecionada
            for ($i = 0; $i < $quantidade; $i++) {
                $dados['codigo_amlurb'] = GeradorCodigos::geraCodigoAmlurbEquipamento(Auth::user()->empresa_id);
                EmpresasXEquipamento::create($dados);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()]);
        }

        return response()->json(['success' => 'Equipamento(s) salvo(s)']);
    }

    /**
     * Método que deleta um equipamento quando o ente realiza a operação no painel. É acessado via Jquery.
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function equipamentoDeletar(Request $request, $id)
    {
        try {
            DB::beginTransaction();

            $justificativa = Justificativa::create([
                'justificativa' => $request->input('justificativa')
            ]);

            $equip = EmpresasXEquipamento::whereId($id)->first();
            $equip->status_id = \config('enums.status.excluido');
            $equip->save();
            $equip->delete(); // softdelete // equipamento_alocacao cascade

            JustificativaExclusaoEquipamento::create([
                'justificativa_id' => $justificativa->id,
                'empresa_id' => Auth::user()->empresa_id,
                'equipamento_id' => $equip->id
            ]);

            DB::commit();

        } catch (\Exception $e) {
            return response()->json(['success' => 'Ocorreu um problema na exclusão deste equipamento - ' . $e->getMessage()]);
        }

        return response()->json(['success' => 'Equipamento de código ' . $equip->codigo_amlurb . ' foi excluído.']);
    }

    // metodo para deletar equipamentos que ainda não foram registrados por boleto e qrcode
    public function equipamentoForceDeletar($id)
    {
        $this->empresasXequipamentoRepository->getEquipamentoById($id)->forceDelete();
        return response()->json(['success' => 'Equipamento excluído']);
    }

    // gerador e transportador
    public function listEquipamentos()
    {
        return EquipamentosVeiculosRenderer::renderEquipamentos(
            $this->empresasXequipamentoRepository->getEquipamentosByIdEmpresa(Auth::user()->empresa_id)
        );
    }

    public function listEquipamentosAtivar()
    {
        $equipamentos = $this->empresasXequipamentoRepository->getEquipamentosNaoAtivadosByIdEmpresa(Auth::user()->empresa_id);
        $html = View::make('painel.equipamentos.modais.ativar-renovar-equipamentos', compact(
            'equipamentos'))->render();

        return response()->json(['html' => $html]);
    }

    public function getEquipamentosAlocadosTo($id_empresa)
    {
        $equipamentos = $this->empresasXequipamentoRepository->getEquipamentosAlocadosToIdEmpresa($id_empresa);
        return response()->json($equipamentos);
    }

    public function ativarEquipamento($id_equipamento)
    {
        try {
            DB::beginTransaction();

            $equipamento = EmpresasXEquipamento::find($id_equipamento);
            $equipamento->status_id = \config('enums.status.ativo');
            $equipamento->save();

            DB::commit();

        } catch (\Exception $e) {
            return response()->json(['success' => 'Ocorreu um problema na ativação deste equipamento']);
        }

        return response()->json(['success' => 'Equipamento de código ' . $equipamento->codigo_amlurb . ' ativado.']);
    }

    public function ativarVariosEquipamentos(Request $request)
    {
        try {
            DB::beginTransaction();

            foreach ($request->get('equipamentos') as $equipamento_id) {
                $equipamento = EmpresasXEquipamento::find($equipamento_id);
                $equipamento->status_id = \config('enums.status.ativo');
                $equipamento->save();
            }

            DB::commit();

        } catch (\Exception $e) {
            return response()->json(['success' => 'Ocorreu um problema na ativação dos equipamentos']);
        }

        return response()->json(['success' => 'Equipamentos selecionados ativados.']);
    }

}