<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Mail\NovoBoleto;
use Amlurb\Models\Boleto;
use Amlurb\Models\BoletoVeiculos;
use Amlurb\Models\EmpresasVeiculo;

use Amlurb\Services\DadosEmail;
use Amlurb\Services\DataTables\EquipamentosVeiculosRenderer;
use Amlurb\Services\ParametrosBoleto;
use Amlurb\Services\ServiceBoletos;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Validator;

class BoletoController extends Controller
{

    /**
     * @var ServiceBoletos
     */
    private $serviceBoletos;
    private $empresa;

    private $codigo_tipo_servico = '';

    public function __construct()
    {
        $this->serviceBoletos = new ServiceBoletos();
    }

    /// VEICULOS

    public function novoBoletoVeiculo(Request $request, $veiculo_id)
    {
        $this->codigo_tipo_servico = \config('enums.codigo_tipo_servico.veiculo');
        try {
            DB::beginTransaction();

            $boleto = $this->gerarBoleto(\config('enums.tipos_boleto.veiculo'));
            $veiculo = $this->vincularVeiculoBoleto($boleto->id, $veiculo_id);

            // envia boleto por email se usuario tiver solicitado
            if ($request->get('modo') == 'email') {
                $dadosEmail = DadosEmail::boletoVeiculo(Auth::user()->empresa, $veiculo);
                $dadosEmail['boleto'] = $boleto->arquivoBoleto;

                $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower($dadosEmail['emailReceptor']));

                $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
                    "email" => 'mailgun_email:mailbox'
                ]);
                if ($validator->fails()) {
                    DB::rollback();
                    $retornoAjax['erro'] = 'O email: ' .$dadosEmail['emailReceptor'].' é invalido por favor entre em contato com cliente para atualizar o e-mail';
                    return response()->json($retornoAjax);
                }
                
                Mail::to($dadosEmail['emailReceptor'])->queue(new NovoBoleto($dadosEmail));
            }

            DB::commit();
            return response()->json([
                'success' => 'Boleto emitido',
                'boleto_id' => $boleto->id
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['error' => 'Ocorreu um erro interno, tente novamente.' . $e->getMessage()]);
        }
    }

    public function novoBoletoVariosVeiculos(Request $request)
    {
        $this->codigo_tipo_servico = \config('enums.codigo_tipo_servico.veiculo');
        try {
            DB::beginTransaction();

            foreach ($request->input('veiculos') as $veiculo_id) {
                $boleto = $this->gerarBoleto(\config('enums.tipos_boleto.veiculo'));
                $this->vincularVeiculoBoleto($boleto->id, $veiculo_id);
            }

            DB::commit();
            return response()->json([
                'success' => 'Boleto emitido',
                'boleto_id' => $boleto->id
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['error' => 'Ocorreu um erro interno, tente novamente.']);
        }
    }

    public function boletoSegundaVia($id)
    {
        $boleto = Boleto::whereId($id)->first();

        try {
            DB::beginTransaction();

            if (strtolower($boleto->tipo) == \config('enums.tipos_boleto.veiculo')) {

                $boleto_veiculo = BoletoVeiculos::where('boleto_id', $id)->first();
                $new_boleto = $this->gerarBoleto(\config('enums.tipos_boleto.veiculo'));
                BoletoVeiculos::create([
                    'boleto_id' => $new_boleto->id,
                    'veiculo_id' => $boleto_veiculo->veiculo_id
                ]);

            }

            $boleto->status_id = \config('enums.status.excluido');
            $boleto->save();
            $boleto->delete(); // softdelete no boleto antigo para nao aparecer na listagem

            DB::commit();
            return response()->json([
                'success' => 'Foi emitido um novo boleto para o ítem relacionado ao boleto antigo',
                'boleto_id' => $new_boleto->id
            ]);

        } catch (\Exception $e) {
            DB::rollBack();
            Log::error($e);

            return response()->json(['error' => 'Ocorreu um erro interno, tente novamente.']);
        }
    }

    // return PDF file
    public function showBoletoPDF($id)
    {
        $file = Boleto::whereId($id)->first()->arquivoBoleto;
        return response()->file($file, ['Content-Type' => 'application/pdf']);
    }

    /// TELA PAGAMENTOS TRANSPORTADOR / GERADOR

    public function showBoleto($boleto_id)
    {
        $boleto = Boleto::whereId($boleto_id)->with('status')->first();

        // verifica tipo de boleto
        if (strtolower($boleto->tipo) == \config('enums.tipos_boleto.veiculo')) {
            $veiculo = EmpresasVeiculo::withTrashed()->whereId($boleto->boleto_veiculos->veiculo_id)->first();
            $html = View::make('painel.pagamentos.modais.veiculos-list', compact(
                'boleto','veiculo'))->render();
        }

        return response()->json(['html' => $html]);
    }

    public function listBoletos()
    {
        $boletos = Boleto::where('empresa_id', Auth::user()->empresa_id)
            ->with('status')->get();

        foreach ($boletos as $i => $b) {
            if (strtolower($b->tipo) == 'veiculo') {
                $bv = BoletoVeiculos::where('boleto_id', $b->id)->first();
                $boletos[$i]->codigo_item = EmpresasVeiculo::whereId($bv->veiculo_id)->withTrashed()->first()->codigo_amlurb;;
            }
        }

        return EquipamentosVeiculosRenderer::renderBoletos($boletos);
    }


    /// FUNCOES AUXILIARES

    // gerar registro de boleto no BD
    private function gerarBoleto($tipo)
    {
        $info_boleto = $this->gerarArquivoBoleto();

        $data_validade = str_replace('T', ' ', $info_boleto['dadosGuia']->DataDeValidade);
        $data_vencimento = str_replace('T', ' ', $info_boleto['dadosGuia']->DataDeVencimento);
        $validade = Carbon::createFromFormat('Y-m-d H:i:s', $data_validade);
        $vencimento = Carbon::createFromFormat('Y-m-d H:i:s', $data_vencimento);

        $dadosBoleto = [
            'empresa_id' => Auth::user()->empresa_id,
            'tipo' => $tipo,
            'anoEmissao' => $info_boleto['dadosGuia']->AnoEmissao,
            'numeroGuia' => $info_boleto['dadosGuia']->Numero,
            'valor' => $info_boleto['dadosGuia']->Valor,
            'status_id' => \config('enums.status.pagamento_pendente'),
            'codTipoServGuia' => $this->codigo_tipo_servico,
            'arquivoBoleto' => $this->serviceBoletos->getPath() . $info_boleto['file'],
            'dataDeValidade' => $validade,
            'dataDeVencimento' => $vencimento
        ];

        return Boleto::create($dadosBoleto);
    }

    // solicita geracao de boleto para API se ambiente for de producao
    private function gerarArquivoBoleto()
    {
        $this->empresa = Auth::user()->empresa;
        $this->serviceBoletos->setPath('boletos' . DIRECTORY_SEPARATOR
            . $this->empresa->cnpj . DIRECTORY_SEPARATOR);

        $app_env = config('app.env');
        if (!is_null($app_env) && in_array($app_env, ['production', 'homolog'])) {
            $request_data = $this->constructRequestBoleto();
            $boleto_gerado = $this->serviceBoletos->gerarBoleto($request_data);
            return $boleto_gerado;
        } else { // AMBIENTE DEV
            return [
                'file' => 'boleto-facil-exemplo.pdf', 'dadosGuia' => ParametrosBoleto::dadosGuiaRandomico()
            ];
        }
    }

    private function constructRequestBoleto()
    {
        $params = ParametrosBoleto::preencheDadosEmpresa($this->empresa);
        $params['InputMessage']['Input']['CodigoTipoServico'] = $this->codigo_tipo_servico;
        $params['InputMessage']['Input']['OutrasInformacoes'] = 'Taxa de Veiculo';
        return $params;
    }

    private function vincularVeiculoBoleto($boleto_id, $veiculo_id)
    {
        BoletoVeiculos::create([
            'boleto_id' => $boleto_id,
            'veiculo_id' => $veiculo_id
        ]);

        // muda status veiculo
        $veiculo = EmpresasVeiculo::whereId($veiculo_id)->first();
        $veiculo->status_id = \config('enums.status.pagamento_pendente');
        $veiculo->save();
        return $veiculo;
    }

}