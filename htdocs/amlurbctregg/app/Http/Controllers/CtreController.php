<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\Ctre;
use Amlurb\Models\CtreEquipamentos;
use Amlurb\Models\CtreResiduos;
use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Models\Justificativa;
use Amlurb\Models\JustificativaInvalidacaoCtre;
use Amlurb\Repositories\CtreRepository;
use Amlurb\Repositories\EmpresaRepository;
use Amlurb\Repositories\EmpresasXEquipamentoRepository;
use Amlurb\Services\GeradorCodigos;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class CtreController extends Controller
{
    /**
     * @var CtreRepository
     */
    protected $ctreRepository;
    /**
     * @var EmpresaRepository
     */
    protected $empresaRepository;
    /**
     * @var EmpresasXEquipamentoRepository
     */
    protected $empresasXequipamentosRepository;

    public function __construct()
    {
        $this->ctreRepository = new CtreRepository();
        $this->empresaRepository = new EmpresaRepository();
        $this->empresasXequipamentosRepository = new EmpresasXEquipamentoRepository();
    }

    // salva novo CTRE emitido por transportador ou gerador
    public function emissaoCtre(Request $request)
    {
        $tipo_empresa = $this->empresaRepository->getEmpresaTipoById(Auth::user()->empresa_id);

        $messages = [
            'residuos_vinculo.required' => 'Resíduos é obrigatório'
        ];
        $validation_params = [
            'residuos_vinculo' => 'required'
        ];

        // constroi validacao de acordo com tipo da empresa logada
        if ($tipo_empresa == \config('enums.empresas_tipo.transportador')) { // transportador
            $messages['gerador.required'] = 'Gerador é obrigatório';
            $messages['destino.required'] = 'Destino é obrigatório';
            $messages['placa_veiculo.required'] = 'Placa do Veículo é obrigatória';

            $validation_params['gerador'] = 'required';
            $validation_params['destino'] = 'required';
            $validation_params['placa_veiculo'] = 'required';
        } else { // gerador
            $messages['transportador.required'] = 'Transportador é obrigatório';
            $validation_params['transportador'] = 'required';
        }

        $validator = validator($request->all(), $validation_params, $messages);
        if ($validator->fails()) {

            $form_error = [];
            foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                $form_error[$k] = $validator->errors()->first($k);
            }

            return response()->json(['error' => $form_error]);
        }

        try {
            DB::beginTransaction();

            $dt = new Carbon();

            // Recebendo os dados do ctre e preparando para inserir nas tabelas
            $dadosCtre = [
                'empresa_id' => Auth::user()->empresa_id, // emissor do CTRE - empresa logada
                'data_emissao' => $dt->now(),
                'data_expiracao' => $dt->addDays(3),
                'status_id' => \config('enums.status.em_aberto'),
                'codigo' => GeradorCodigos::geraCodigoCTRE(Auth::user()->empresa_id),
            ];

            // verifica restante dos dados a serem preenchidos de acordo com a empresa logada
            if ($tipo_empresa == \config('enums.empresas_tipo.transportador')) { // transportador
                $dadosCtre['gerador_id'] = $request->input('gerador');
                $dadosCtre['transportador_id'] = Auth::user()->empresa_id;
                $dadosCtre['destino_id'] = $request->input('destino');
                $dadosCtre['veiculo_id'] = $this->getIdVeiculoFromPlaca($request->input('placa_veiculo'));
            } else { // gerador
                $dadosCtre['gerador_id'] = Auth::user()->empresa_id;
                $dadosCtre['transportador_id'] = $request->input('transportador');
            }

            // recebendo os equipamentos e residuos escolhidos
            $equipamentos = (!is_null($request->input('equipamentos_vinculo'))) ?
                explode(',', $request->input('equipamentos_vinculo')) : [];
            $residuos = explode(',', $request->input('residuos_vinculo'));

            // insere Ctre
            $ctre = Ctre::create($dadosCtre);
            $dadosCtre['ctre_id'] = $ctre->id;

            // vinculo de equipamento e residuos com CTRE
            foreach ($equipamentos as $equipamento) {
                $dadosCtre['equipamento_id'] = $equipamento;
                CtreEquipamentos::create($dadosCtre);
            }
            foreach ($residuos as $residuo) {
                $dadosCtre['residuo_id'] = $residuo;
                CtreResiduos::create($dadosCtre);
            }

            DB::commit();
            return response()->json([
                'success' => 'Emissão de CTR-E realizada com sucesso!',
                'error' => []
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);

            return response()->json(['error' => 'Ocorreu um erro interno' . $e->getMessage()]);
        }
    }

    // ver CTRE emitida por transportador ou gerador
    public function showCtre($id)
    {
        $ctre = $this->ctreRepository->getCtreCompleto($id);
        $tipo_empresa = $this->empresaRepository->getEmpresaTipoById($ctre->empresa->id);
        $emissor = ($tipo_empresa == \config('enums.empresas_tipo.transportador')) ?
            'Transportador' : 'Gerador';
        $html = View::make('painel.home.modais.visualizarCTRE', compact(
            'ctre', 'emissor'))->render();
        return response()->json(['html' => $html]);
    }

    // CTRE de gerador para transportador validar
    public function showCtreGeradorValidar($id)
    {
        $ctre = $this->ctreRepository->getCtreCompleto($id);
        $veiculos = Auth::user()->empresa->empresas_veiculos
            ->where('status_id', \config('enums.status.ativo'))->pluck('placa', 'placa');
        $destinos = $this->empresaRepository->getDestinosFinaisVinculados();
        $equipamentos = $this->empresasXequipamentosRepository->getEquipamentosAlocadosToIdEmpresa($ctre->gerador->id);

        $html = View::make('painel.transportador.home.modais.validarCTRE', compact(
            'ctre', 'veiculos', 'destinos', 'equipamentos'))->render();
        return response()->json(['html' => $html]);
    }

    // CTRE de transportador para gerador ou destino final validar
    public function showCtreValidar($id)
    {
        $ctre = $this->ctreRepository->getCtreCompleto($id);
        $tipo_empresa = $this->empresaRepository->getEmpresaTipoById($ctre->empresa->id);
        $emissor = ($tipo_empresa == \config('enums.empresas_tipo.transportador')) ?
            'Transportador' : 'Gerador';
        $html = View::make('painel.home.modais.validarCTRE', compact(
            'ctre', 'emissor'))->render();
        return response()->json(['html' => $html]);
    }

    // validacao de CTRE por gerador e transportador
    public function validarCtre(Request $request, $id)
    {
        $tipo_empresa = $this->empresaRepository->getEmpresaTipoById(Auth::user()->empresa_id);

        if ($tipo_empresa == \config('enums.empresas_tipo.transportador')) {

            $validator = validator($request->all(), [
                'destino' => 'required',
                'placa_veiculo' => 'required'
            ], [
                'destino.required' => 'Destino Final é obrigatório',
                'placa_veiculo.required' => 'Tipo de Veículo é obrigatório'
            ]);
            if ($validator->fails()) {
                $form_error = [];
                foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                    $form_error[$k] = $validator->errors()->first($k);
                }

                return response()->json(['error' => $form_error]);
            }

        }

        if ($tipo_empresa == \config('enums.empresas_tipo.destino_final_reciclado')) {
            $validator = validator($request->all(), [
                'volume_total' => 'required'
            ], [
                'volume_total.required' => 'É necessário informar o volume total do CTRE'
            ]);
            if ($validator->fails()) {
                $form_error = [];
                foreach ($validator->getMessageBag()->getMessages() as $k => $v) {
                    $form_error[$k] = $validator->errors()->first($k);
                }

                return response()->json(['error' => $form_error]);
            }
        }

        $ctre = Ctre::find($id); // pega CTRE no banco

        try {

            DB::beginTransaction();

            $dt = new Carbon();

            // atualizando CTRE
            if ($tipo_empresa == \config('enums.empresas_tipo.transportador')) {
                $ctre->destino_id = $request->input('destino');
                $ctre->veiculo_id = $this->getIdVeiculoFromPlaca($request->input('placa_veiculo'));

                // vinculando equipamentos
                $equipamentos = (!is_null($request->input('equipamentos_vinculo'))) ?
                    explode(',', $request->input('equipamentos_vinculo')) : [];
                $dadosCtre = ['ctre_id' => $ctre->id];
                foreach ($equipamentos as $equipamento) {
                    $dadosCtre['equipamento_id'] = $equipamento;
                    CtreEquipamentos::create($dadosCtre);
                }
            }

            // confirmando validacao
            if ($tipo_empresa == \config('enums.empresas_tipo.destino_final_reciclado')) {
                // destino final adiciona volume de residuos no CTRE
                $ctre['volume_total'] = str_replace(',', '.', ($request->input('volume_total')));
                $ctre->data_validacao_final = $dt->now();
                $ctre->status_id = ($ctre->status_id == \config('enums.status.em_aberto')) ?
                    \config('enums.status.finalizado') : \config('enums.status.expirado_finalizado');

            } else { // gerador ou transportador
                $ctre->data_validacao = $dt->now();
            }

            $ctre->save();

            DB::commit();
            return response()->json([
                'success' => 'CTRE validada',
                'error' => []
            ]);

        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json(['error' => 'Ocorreu um erro interno, tente novamente.']);
        }

    }

    // invalidacao de CTRE por gerador e transportador
    public function invalidarCtre(Request $request, $id)
    {
        $ctre = Ctre::find($id); // pega CTRE no banco

        try {

            // atualiza status do CTRE
            $ctre->status_id = \config('enums.status.invalidado');
            $ctre->save();

            // salva a justificativa de invalidacao
            $justificativa = Justificativa::create([
                'justificativa' => $request->input('justificativa')
            ]);
            JustificativaInvalidacaoCtre::create([
                'justificativa_id' => $justificativa->id,
                'ctre_id' => $ctre->id,
                'empresa_id' => Auth::user()->empresa_id
            ]);

            DB::commit();
            return response()->json([
                'success' => 'CTRE invalidada',
                'error' => []
            ]);

        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json(['error' => 'Ocorreu um erro interno, tente novamente.']);
        }
    }

    public function getIdVeiculoFromPlaca($placa)
    {
        return EmpresasVeiculo::wherePlaca($placa)->first()->id;
    }

}