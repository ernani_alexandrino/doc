<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\Cidade;
use Amlurb\Models\Empresa;
use Amlurb\Models\TipoRamoAtividade;
use Firebase;

class SiteController extends Controller
{
    const DEFAULT_PASSWORD = 'amlurb#@';

    /**
     * Método responsável por buscar os tipos de atividades do Ramo de atividade selecionado e retornar ao scripts.js para montar o select de cadastro
     * @param int $idRamoAtividade
     * @return mixed
     */
    public function buscarTipoAtividade($idRamoAtividade)
    {
        
        $tipoAtividade = TipoRamoAtividade::where('ramo_atividade_id', $idRamoAtividade)->orderBy('nome',
            'asc')->pluck('nome', 'id');

        return response()->json($tipoAtividade);
    }

    /**
     * Método responsável por buscar as cidades após selecionar o estado no cadastro e retornar ao scripts.js todas as cidades.
     * @param int $idEstado
     * @return mixed
     */
    public function buscarCidade($idEstado = 0)
    {
        return Cidade::where('estado_id', $idEstado)->select('id', 'nome')->get();
    }

    /*/
    Funcoes para o Firebase
    /*/
    public function firebasePutVeiculos()
    {
        $empresas = Empresa::has('empresas_veiculos')->get();

        $dataveiculos = [];
        $dataDestinoFinal = [];
        $dataGeradores = [];

        foreach ($empresas AS $k => $empresa) {

            foreach ($empresa->empresas_veiculos AS $veiculo) {

                $dataveiculos[$empresa->id][]['placa'] = $veiculo->placa;
            }

            foreach ($empresa->vinculos as $vinculo) {
                // verifica o vinculo
                if ($vinculo->empresa_vinculada->empresas_x_empresas_tipos->empresa_tipo_id == \config('enums.empresas_tipo.destino_final_reciclado')) {
                    $dataDestinoFinal[$empresa->id][$vinculo->empresa_vinculada->id]['razao_social'] = $vinculo->empresa_vinculada->razao_social;
                    $dataDestinoFinal[$empresa->id][$vinculo->empresa_vinculada->id]['cnpj'] = formatar('cnpj', $vinculo->empresa_vinculada->cnpj);
                } else {
                    $dataGeradores[$empresa->id][$vinculo->empresa_vinculada->id]['razao_social'] = $vinculo->empresa_vinculada->razao_social;
                    $dataGeradores[$empresa->id][$vinculo->empresa_vinculada->id]['cnpj'] = formatar('cnpj', $vinculo->empresa_vinculada->cnpj);
                }
            }
        }

        Firebase::set('/veiculos/', $dataveiculos);
        Firebase::set('/destinos_finais/', $dataDestinoFinal);
        Firebase::set('/geradores/', $dataGeradores);

        // print_r($dataveiculos);
        die('here');

        // $coletas = json_decode(Firebase::get('/coletas/',['print'=> 'pretty']),true);
    }

}
