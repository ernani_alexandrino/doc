<?php

namespace Amlurb\Http\Controllers;

use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class MessagesController extends Controller
{
    public $idLayoutPainel = '';
    public $menuLateral = '';
    public $titulo = array(
        'principal' => '',
        'subtitulo' => ''
    );

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();

            return $next($request);
        });
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {

        $dados = $this->tipoEmpresa();

        $titulo = $dados->titulo;
        $idLayoutPainel = $dados->idLayout;
        $menuLateral = $dados->menuLateral;
        $titulo = (object) $titulo;

        // All threads, ignore deleted/archived participants
//        $threads = Thread::getAllLatest()->get();
        // All threads that user is participating in
         $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();
        // All threads that user is participating in, with new messages
//         $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();
        return view('messenger.index', compact('threads', 'idLayoutPainel', 'menuLateral', 'titulo'));
    }

    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $dados = $this->tipoEmpresa();

        $titulo = $dados->titulo;
        $idLayoutPainel = $dados->idLayout;
        $menuLateral = $dados->menuLateral;
        $titulo = (object) $titulo;

        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect()->route('messages');
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::id();
        $users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get();
        $thread->markAsRead($userId);
        return view('messenger.show', compact('thread', 'users', 'idLayoutPainel', 'menuLateral', 'titulo'));
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $dados = $this->tipoEmpresa();

        $titulo = $dados->titulo;
        $idLayoutPainel = $dados->idLayout;
        $menuLateral = $dados->menuLateral;
        $titulo = (object) $titulo;

        $users = User::where('id', '!=', Auth::id())->get();
        return view('messenger.create', compact('users', 'idLayoutPainel', 'menuLateral', 'titulo'));
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store()
    {
        $input = Input::all();
        $thread = Thread::create([
            'subject' => $input['subject'],
        ]);
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $input['message'],
        ]);
        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'last_read' => new Carbon,
        ]);
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant($input['recipients']);
        }
        return redirect()->route('messages');
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            return redirect()->route('messages');
        }
        $thread->activateAllParticipants();
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => Input::get('message'),
        ]);
        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
        ]);
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipant(Input::get('recipients'));
        }
        return redirect()->route('messages.show', $id);
    }

    public function tipoEmpresa()
    {
        $this->titulo['subtitulo'] = 'Mensagens';

        $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $this->user->empresa_id)->first();

        switch ($empresaTipo->empresa_tipo_id) {
            case 1:
                $this->idLayoutPainel = 'painel-gerador';
                $this->menuLateral = 'painel.gerador.menu-lateral';
                $this->titulo['principal'] = 'Gerador';
                break;
            case 3:
                $this->idLayoutPainel = 'painel-transportador';
                $this->menuLateral = 'painel.transportador.menu-lateral';
                $this->titulo['principal'] = 'Transportador';
                break;
            case 4:
                $this->idLayoutPainel = 'painel-destino-final';
                $this->menuLateral = 'painel.destino-final.menu-lateral';
                $this->titulo['principal'] = 'Destino Final';
                break;
            case 6:
                $this->idLayoutPainel = 'painel-fiscal';
                $this->menuLateral = 'painel.fiscal.menu-lateral';
                $this->titulo['principal'] = 'Amlurb';
                break;
            case 7:
                $this->idLayoutPainel = 'painel-fiscal';
                $this->menuLateral = 'painel.fiscal.menu-lateral';
                $this->titulo['principal'] = 'Amlurb';
                break;
        }

        $dados = (object) array(
            'titulo' => $this->titulo,
            'idLayout' => $this->idLayoutPainel,
            'menuLateral' => $this->menuLateral
        );

        return $dados;
    }

}
