<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroResponsavelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'responsavel' => 'required',
            'email' => 'required|email|unique:users',
            'cargo' => 'required',
            'celular_responsavel' => 'required',
            'telefone_responsavel' => 'required',
            'ramal' => 'max:15',
        ];
    }

    public function messages()
    {
        return [
            'responsavel.required' => 'O responsável é obrigatório.',
            'email.required' => 'O email é obrigatório.',
            'cargo.required' => 'O cargo é obrigatório.',
            'telefone_responsavel.required' => 'O telefone é obrigatório.',
            'ramal.required' => 'O ramal é obrigatório.',
            'celular_responsavel.max' => 'No máximo 15 caracteres.',
        ];
    }
}
