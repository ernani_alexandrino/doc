<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroCondominioResponsavelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'condominio_sindico_rg' => 'required',
            'condominio_file_rg_enviado' => 'required',
            'condominio_file_rg' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',

            'condominio_sindico_cpf' => 'required',
            'condominio_file_cpf_enviado' => 'required',
            'condominio_file_cpf' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',

            'condominio_ata_enviado' => 'required',
            'condominio_ata' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',
        ];
    }

    public function message()
    {
        $max = '3mb';
        $maxContrato = '10mb';

        return [
            'condominio_sindico_rg.required' => 'O número do RG é de preenchimento obrigatório',
            'condominio_file_rg_enviado.required' => 'O envio do RG é obrigatório',
            'condominio_file_rg.max' => "O arquivo deve ter até $max",
            'condominio_file_rg.mimes' => 'Formato de arquivo inválido.',

            'condominio_sindico_cpf.required' => 'O número do CPF é de preenchimento obrigatório',
            'condominio_file_cpf_enviado.required' => 'O envio do CPF é obrigatório',
            'condominio_file_cpf.max' => "O arquivo deve ter até $max",
            'condominio_file_cpf.mimes' => 'Formato de arquivo inválido.',

            'condominio_ata_enviado.required' => 'O envio da ATA do condomínio é obrigatório',
            'condominio_ata.max' => "O arquivo deve ter até $max",
            'condominio_ata.mimes' => 'Formato de arquivo inválido.',
        ];
    }
}
