<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroEmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ramo_atividade' => 'required|integer|min:1',
            'tipo_atividade' => 'required|integer|min:1',
            'cnpj' => 'required',
            'razao_social' => 'required',
            'nome_fantasia' => 'required',
            'licenca_municipal' => 'required|max:15',
            'inscricao_estadual' => 'max:15',
            'telefone' => 'required',
            'cep' => 'required',
            'estado' => 'required',
            'cidade' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'empresa_cartao_cnpj' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',
            'empresa_iptu' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',
            'empresa_iptu_enviado' => 'required',
            'empresa_num_iptu' => 'required',
            'empresa_cartao_cnpj_enviado' => 'required'
//            'responsavel' => 'required',
//            'email' => 'required|email|unique:empresas',
//            'cargo' => 'required',
//            'telefone_responsavel' => 'required',
//            'ramal' => 'required|max:15',
//            'celular_responsavel' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cnpj.required' => 'O CNPJ é obrigatório.',
            'razao_social.required' => 'O razão social é obrigatório.',
            'nome_fantasia.required' => 'O nome fantasia é obrigatório.',
            'cep.required' => 'O CEP é obrigatório.',
            'estado.required' => 'O estado é obrigatório.',
            'cidade.required' => 'A cidade é obrigatório.',
            'endereco.required' => 'O endereço é obrigatório.',
            'numero.required' => 'O número é obrigatório.',
            'bairro.required' => 'O bairro é obrigatório.',
            'licenca_municipal.required' => 'O CCM é obrigatório.',
            'inscricao_estadual.max' => 'No máximo 15 caracteres',
            'telefone.required' => 'O telefone é obrigatório.',
            'ramo_atividade.required' => 'O ramo de atividade é obrigatório.',
            'ramo_atividade.integer' => 'O ramo de atividade é obrigatório.',
            'ramo_atividade.min' => 'O ramo de atividade é obrigatório.',
            'tipo_atividade.required' => 'O tipo de atividade é obrigatório.',
            'tipo_atividade.integer' => 'O tipo de atividade é obrigatório.',
            'tipo_atividade.min' => 'O tipo de atividade é obrigatório.',
            'responsavel.required' => 'O campo Responsável é obrigatório.',
            'email.required' => 'O campo E-mail é obrigatório.',
            'cargo.required' => 'O campo Cargo é obrigatório.',
            'telefone_responsavel.required' => 'O campo Telefone do Responsável é obrigatório.',
            'ramal.required' => 'O campo Ramal é obrigatório.',
            'celular_responsavel.required' => 'O campo Celular do Responsável é obrigatório',
            'empresa_cartao_cnpj_enviado.required' => 'O campo cartão cnpj é obrigatório',
            'empresa_num_iptu.required' => 'O campo número de  iptu é obrigatório',
            'empresa_iptu_enviado.required' => 'O campo cópia do iptu é obrigatório'
        ];
    }
}
