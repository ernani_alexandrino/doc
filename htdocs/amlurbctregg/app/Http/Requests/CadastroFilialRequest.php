<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroFilialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'nome_filial' => 'required',
            'nome_responsavel_filial' => 'required',
            'email' => 'required|email|unique:users',
        ];
    }

    public function messages()
    {
        return [
            'nome_filial.required' => 'O Nome da Filial é obrigatório.',
            'nome_responsavel_filial.required' => 'O Nome do Responsável pela Filial é obrigatório.',
            'email.required' => 'O E-mail do Responsável pela Filial é obrigatório.',
        ];
    }
}
