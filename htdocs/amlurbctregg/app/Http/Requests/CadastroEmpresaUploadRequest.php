<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroEmpresaUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * Os campos que estão com "_enviado" são apenas para validar o envio ou não na função que faz o upload por ajax dos arquivos.
     *
     * @return array
     */
    public function rules()
    {
        $retorno = [
            // 'cartao_cnpj' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            // 'cartao_cnpj_enviado' => 'required',

            'transportador_contrato_social' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_contrato_social_enviado' => 'required',

            'transportador_rg_num_responsavel_tecnico' => 'required',
            'transportador_nome_responsavel_tecnico' => 'required',
            'transportador_cpf_num_responsavel_tecnico' => 'required',
            'transportador_crea_num_responsavel_tecnico' => 'required',
            // 'emissaolicenca' => 'required',
            // 'licencavencimento' => 'required',
            // 'licenka_enviado' => 'required',
            'licencadefault' => 'required',

            'transportador_certidao_falencia_concordata' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_certidao_falencia_concordata_enviado' => 'required',

            'transportador_balanco_financeiro' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_demonstrativo_contabil' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_demonstrativo_contabil_enviado' => 'required',

            'transportador_certidao_negativa_municipal' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_certidao_negativa_municipal_enviado' => 'required',

            'transportador_certidao_negativa_estadual' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_certidao_negativa_estadual_enviado' => 'required',

            'transportador_certidao_negativa_federal' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_certidao_negativa_federal_enviado' => 'required',

            'transportador_doc_ccm' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_doc_ccm_enviado' => 'required',

            'transportador_balanco_financeiro' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_balanco_financeiro_enviado' => 'required',

            'transportador_anotacao_responsabilidade_tecnica' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_anotacao_responsabilidade_tecnica_enviado' => 'required',

            'transportador_rg_responsavel_tecnico' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_rg_responsavel_tecnico_enviado' => 'required',

            'transportador_cpf_responsavel_tecnico' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_cpf_responsavel_tecnico_enviado' => 'required',
            'transportador_crea_responsavel_tecnico' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_crea_responsavel_tecnico_enviado' => 'required',
            'transportador_crf_caixa' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_crf_caixa_enviado' => 'required',

            'licenka' => 'required',

            
            'transportador_anotacao_responsabilidade_tecnica_emissao' => 'required|date_format:"d/m/Y"|before_or_equal:transportador_anotacao_responsabilidade_tecnica_vencimento',
            'transportador_anotacao_responsabilidade_tecnica_vencimento' => 'required|date_format:"d/m/Y"|after_or_equal:transportador_anotacao_responsabilidade_tecnica_emissao',

            'transportador_certificado_dispensa_licenca' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            //'transportador_certificado_dispensa_licenca_emissao' => 'date_format:"d/m/Y"|before_or_equal:transportador_certificado_dispensa_licenca_vencimento',
            //'transportador_certificado_dispensa_licenca_vencimento' => 'date_format:"d/m/Y"|after_or_equal:transportador_certificado_dispensa_licenca_emissao',

            'licenka' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            // 'transportador_licenca_previa' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            //'transportador_licenca_previa_emissao' => 'date_format:"d/m/Y"|before_or_equal:transportador_licenca_previa_vencimento',
            //'transportador_licenca_previa_vencimento' => 'date_format:"d/m/Y"|after_or_equal:transportador_licenca_previa_emissao',

            // 'transportador_licenca_instalacao' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            //'transportador_licenca_instalacao_emissao' => 'date_format:"d/m/Y"|before_or_equal:transportador_licenca_instalacao_vencimento',
            //'transportador_licenca_instalacao_vencimento' => 'date_format:"d/m/Y"|after_or_equal:transportador_licenca_instalacao_emissao',

            // 'transportador_licenca_operacao' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            //'transportador_licenca_operacao_emissao' => 'date_format:"d/m/Y"|before_or_equal:transportador_licenca_operacao_vencimento',
            //'transportador_licenca_operacao_vencimento' => 'date_format:"d/m/Y"|after_or_equal:transportador_licenca_operacao_emissao',

            'transportador_alvara_prefeitura' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_alvara_prefeitura_enviado' => 'required',

            'transportador_alvara_prefeitura_emissao' => 'required|date_format:"d/m/Y"|before_or_equal:transportador_alvara_prefeitura_vencimento',
            'transportador_alvara_prefeitura_vencimento' => 'date_format:"d/m/Y"|after_or_equal:transportador_alvara_prefeitura_emissao',

            
            'transportador_avcb' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'transportador_avcb_enviado' => 'required',
            'transportador_avcb_emissao' => 'required|date_format:"d/m/Y"|before_or_equal:transportador_avcb_vencimento',
            'transportador_avcb_vencimento' => 'required|date_format:"d/m/Y"|after_or_equal:transportador_avcb_emissao'
        ];

        if ($this->transportador_anotacao_responsabilidade_tecnica_vencimento_indeterminada) {
            $retorno['transportador_anotacao_responsabilidade_tecnica_emissao'] = 'required|date_format:"d/m/Y"';
            $retorno['transportador_anotacao_responsabilidade_tecnica_vencimento'] = '';
        }

        //CDL
        if ($this->transportador_certificado_dispensa_licenca_enviado) {

            $retorno['transportador_certificado_dispensa_licenca_emissao'] = 'required|date_format:"d/m/Y"|before_or_equal:transportador_certificado_dispensa_licenca_vencimento';

            if ($this->transportador_certificado_dispensa_licenca_vencimento) {
                $retorno['transportador_certificado_dispensa_licenca_vencimento'] = 'date_format:"d/m/Y"|after_or_equal:transportador_certificado_dispensa_licenca_emissao';
            }

            if ($this->transportador_certificado_dispensa_licenca_vencimento_indeterminada) {
                $retorno['transportador_certificado_dispensa_licenca_emissao'] = 'required|date_format:"d/m/Y"';
                $retorno['transportador_certificado_dispensa_licenca_vencimento'] = '';
            }
        }

        //Licença
        if ($this->licenka_enviado) {

            $retorno['emissaolicenca'] = 'required|date_format:"d/m/Y"|before_or_equal:licencavencimento';

            if ($this->licencavencimento) {
                $retorno['licencavencimento'] = 'date_format:"d/m/Y"|after_or_equal:emissaolicenca';
            }

            if ($this->licencavencimento_indeterminada) {
                $retorno['emissaolicenca'] = 'required|date_format:"d/m/Y"';
                $retorno['licencavencimento'] = '';
            }
        }

        //Licença prévia
        // if ($this->transportador_licenca_previa_enviado) {

        //     $retorno['transportador_licenca_previa_emissao'] = 'required|date_format:"d/m/Y"|before_or_equal:transportador_licenca_previa_vencimento';

        //     if ($this->transportador_licenca_previa_vencimento) {
        //         $retorno['transportador_licenca_previa_vencimento'] = 'date_format:"d/m/Y"|after_or_equal:transportador_licenca_previa_emissao';
        //     }

        //     if ($this->transportador_licenca_previa_vencimento_indeterminada) {
        //         $retorno['transportador_licenca_previa_emissao'] = 'required|date_format:"d/m/Y"';
        //         $retorno['transportador_licenca_previa_vencimento'] = '';
        //     }
        // }

        // if ($this->transportador_licenca_instalacao_enviado) {

        //     $retorno['transportador_licenca_instalacao_emissao'] = 'required|date_format:"d/m/Y"|before_or_equal:transportador_licenca_instalacao_vencimento';

        //     if ($this->transportador_licenca_instalacao_vencimento) {
        //         $retorno['transportador_licenca_instalacao_vencimento'] = 'date_format:"d/m/Y"|after_or_equal:transportador_licenca_instalacao_emissao';
        //     }

        //     if ($this->transportador_licenca_instalacao_vencimento_indeterminada) {
        //         $retorno['transportador_licenca_instalacao_emissao'] = 'date_format:"d/m/Y"';
        //         $retorno['transportador_licenca_instalacao_vencimento'] = '';
        //     }
        // }

        // if ($this->transportador_licenca_operacao_enviado) {

        //     $retorno['transportador_licenca_operacao_emissao'] = 'required|date_format:"d/m/Y"|before_or_equal:transportador_licenca_operacao_vencimento';
        //     //'transportador_licenca_operacao_vencimento' => 'date_format:"d/m/Y"|after_or_equal:transportador_licenca_operacao_emissao',

        //     if ($this->transportador_licenca_operacao_vencimento) {
        //         $retorno['transportador_licenca_operacao_vencimento'] = 'date_format:"d/m/Y"|after_or_equal:transportador_licenca_operacao_vencimento';
        //     }

        //     if ($this->transportador_licenca_operacao_vencimento_indeterminada) {
        //         $retorno['transportador_licenca_operacao_emissao'] = 'date_format:"d/m/Y"';
        //         $retorno['transportador_licenca_operacao_vencimento'] = '';
        //     }
        // }

        if ($this->transportador_alvara_prefeitura_vencimento_indeterminada) {
            $retorno['transportador_alvara_prefeitura_emissao'] = 'required|date_format:"d/m/Y"';
            $retorno['transportador_alvara_prefeitura_vencimento'] = '';
        }

        if ($this->transportador_ctf_ibama_vencimento_indeterminada) {
            $retorno['transportador_ctf_ibama_emissao'] = 'required|date_format:"d/m/Y"';
            $retorno['transportador_ctf_ibama_vencimento'] = '';
        }

        if ($this->transportador_avcb_vencimento_indeterminada) {
            $retorno['transportador_avcb_emissao'] = 'required|date_format:"d/m/Y"';
            $retorno['transportador_avcb_vencimento'] = '';
        }

        if ($this->licencavencimento_indeterminada) {
            $retorno['emissaolicenca'] = 'required|date_format:"d/m/Y"';
            $retorno['licencavencimento'] = '';
        }

        return $retorno;
    }

    public function messages()
    {
        $max = '3mb';
        $maxContrato = '10mb';

        return [
            'cartao_cnpj.required' => 'O cartão CNPJ é obrigatório.',
            'cartao_cnpj_enviado.required' => "O cartão CNPJ é obrigatório.",
            'cartao_cnpj.max' => "O arquivo deve ter até $max",
            'cartao_cnpj.mimes' => 'Formato de arquivo inválido.',
            'licenka.required' => 'A licença é obrigatório',
            'licenka_enviado.mimes' => 'Formato de arquivo inválido.',
            // 'licenka_enviado.required' => 'A licença é obrigatório',
            // 'emissaolicenca.required' => 'A Data de Emissão é obrigatório',
            // 'licencavencimento.required' => 'A Data de Vencimento é obrigatório',
            'licencadefault.required' => 'Selecione uma licença',

            'transportador_contrato_social.required' => 'O contrato social é obrigatório.',
            'transportador_contrato_social_enviado.required' => 'O contrato social é obrigatório.',
            'transportador_contrato_social.max' => "O arquivo deve ter até $maxContrato",
            'transportador_contrato_social.mimes' => 'Formato de arquivo inválido.',

            'transportador_certidao_falencia_concordata.required' => 'A certidão de falência ou concordata é obrigatória.',
            'transportador_certidao_falencia_concordata_enviado.required' => 'A certidão de falência ou concordata é obrigatória.',
            'transportador_certidao_falencia_concordata.max' => "O arquivo deve ter até $max",
            'transportador_certidao_falencia_concordata.mimes' => 'Formato de arquivo inválido.',

            'transportador_balanco_financeiro.max' => "O arquivo deve ter até $max",
            'transportador_balanco_financeiro.mimes' => 'Formato de arquivo inválido.',

            'transportador_demonstrativo_contabil.max' => "O arquivo deve ter até $max",
            'transportador_demonstrativo_contabil.mimes' => 'Formato de arquivo inválido.',

            'transportador_certidao_negativa_municipal.required' => 'A certidão negativa municipal é obrigatória.',
            'transportador_certidao_negativa_municipal_enviado.required' => 'A certidão negativa municipal é obrigatória.',
            'transportador_certidao_negativa_municipal.max' => "O arquivo deve ter até $max",
            'transportador_certidao_negativa_municipal.mimes' => 'Formato de arquivo inválido.',

            'transportador_certidao_negativa_estadual.required' => 'A certidão negativa estadual é obrigatória.',
            'transportador_certidao_negativa_estadual_enviado.required' => 'A certidão negativa estadual é obrigatória.',
            'transportador_certidao_negativa_estadual.max' => "O arquivo deve ter até $max",
            'transportador_certidao_negativa_estadual.mimes' => 'Formato de arquivo inválido.',

            'transportador_certidao_negativa_federal.required' => 'A certidão negativa federal é obrigatória.',
            'transportador_certidao_negativa_federal_enviado.required' => 'A certidão negativa federal é obrigatória.',
            'transportador_certidao_negativa_federal.max' => "O arquivo deve ter até $max",
            'transportador_certidao_negativa_federal.mimes' => 'Formato de arquivo inválido.',

            'transportador_anotacao_responsabilidade_tecnica.required' => 'A ART é obrigatória.',
            'transportador_anotacao_responsabilidade_tecnica_enviado.required' => 'A ART é obrigatória.',
            'transportador_anotacao_responsabilidade_tecnica.max' => "O arquivo deve ter até $max",
            'transportador_anotacao_responsabilidade_tecnica.mimes' => 'Formato de arquivo inválido.',
            'transportador_anotacao_responsabilidade_tecnica_emissao.required' => 'A data de emissão da ART é obrigatória.',
            'transportador_anotacao_responsabilidade_tecnica_emissao.date_format' => 'Insira uma data válida.',
            'transportador_anotacao_responsabilidade_tecnica_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'transportador_anotacao_responsabilidade_tecnica_vencimento.required' => 'A data de vencimento da ART é obrigatória.',
            'transportador_anotacao_responsabilidade_tecnica_vencimento.date_format' => 'Insira uma data válida.',
            'transportador_anotacao_responsabilidade_tecnica_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            //'transportador_certificado_dispensa_licenca' => '',
            'transportador_certificado_dispensa_licenca.mimes' => 'Formato de arquivo inválido.',
            'transportador_certificado_dispensa_licenca.max' => "O arquivo deve ter até $max",
            'transportador_certificado_dispensa_licenca_emissao.date_format' => 'Insira uma data válida.',
            'transportador_certificado_dispensa_licenca_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'transportador_certificado_dispensa_licenca_vencimento.date_format' => 'Insira uma data válida.',
            'transportador_certificado_dispensa_licenca_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            //'licenka' => '',
            'licenka.mimes' => 'Formato de arquivo inválido.',
            'licenka.max' => "O arquivo deve ter até $max",

            //'transportador_licenca_previa' => '',
            'transportador_licenca_previa.mimes' => 'Formato de arquivo inválido.',
            'transportador_licenca_previa.max' => "O arquivo deve ter até $max",
            'transportador_licenca_previa_emissao.date_format' => 'Insira uma data válida.',
            'transportador_licenca_previa_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'transportador_licenca_previa_vencimento.date_format' => 'Insira uma data válida.',
            'transportador_licenca_previa_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            //'transportador_licenca_instalacao' => '',
            'transportador_licenca_instalacao.mimes' => 'Formato de arquivo inválido.',
            'transportador_licenca_instalacao.max' => "O arquivo deve ter até $max",
            'transportador_licenca_instalacao_emissao.date_format' => 'Insira uma data válida.',
            'transportador_licenca_instalacao_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'transportador_licenca_instalacao_vencimento.date_format' => 'Insira uma data válida.',
            'transportador_licenca_instalacao_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            //'transportador_licenca_operacao' => '',
            'transportador_licenca_operacao.mimes' => 'Formato de arquivo inválido.',
            'transportador_licenca_operacao.max' => "O arquivo deve ter até $max",
            'transportador_licenca_operacao_emissao.date_format' => 'Insira uma data válida.',
            'transportador_licenca_operacao_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'transportador_licenca_operacao_vencimento.date_format' => 'Insira uma data válida.',
            'transportador_licenca_operacao_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            'transportador_alvara_prefeitura.required' => 'O alvará da prefeitura é obrigatório.',
            'transportador_alvara_prefeitura_enviado.required' => 'O alvará da prefeitura é obrigatório.',
            'transportador_alvara_prefeitura.mimes' => 'Formato de arquivo inválido.',
            'transportador_alvara_prefeitura.max' => "O arquivo deve ter até $max",
            'transportador_alvara_prefeitura_emissao.required' => 'A data de emissão do alvará da prefeitura é obrigatória.',
            'transportador_alvara_prefeitura_emissao.date_format' => 'Insira uma data válida.',
            'transportador_alvara_prefeitura_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'transportador_alvara_prefeitura_vencimento.required' => 'A data de vencimento do alvará da prefeitura é obrigatória.',
            'transportador_alvara_prefeitura_vencimento.date_format' => 'Insira uma data válida',
            'transportador_alvara_prefeitura_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            'transportador_ctf_ibama.required' => 'O CTF Ibama é obrigatório.',
            'transportador_ctf_ibama_enviado.required' => 'O CTF Ibama é obrigatório.',
            'transportador_ctf_ibama.mimes' => 'Formato de arquivo inválido.',
            'transportador_ctf_ibama.max' => "O arquivo deve ter até $max",
            'transportador_ctf_ibama_emissao.required' => 'A data de emissão do CTF Ibama é obrigatória.',
            'transportador_ctf_ibama_emissao.date_format' => 'Insira uma data válida.',
            'transportador_ctf_ibama_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'transportador_ctf_ibama_vencimento.required' => 'A data de vencimento do CTF Ibama é obrigatória.',
            'transportador_ctf_ibama_vencimento.date_format' => 'Insira uma data válida',
            'transportador_ctf_ibama_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            'transportador_avcb.required' => 'O AVCB é obrigatório.',
            'transportador_avcb_enviado.required' => 'O AVCB é obrigatório.',
            'transportador_avcb.mimes' => 'Formato de arquivo inválido.',
            'transportador_avcb.max' => "O arquivo deve ter até $max",
            'transportador_avcb_emissao.required' => 'A data de emissão do AVCB é obrigatória.',
            'transportador_avcb_emissao.date_format' => 'Insira uma data válida.',
            'transportador_avcb_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'transportador_avcb_vencimento.required' => 'A data de vencimento do AVCB é obrigatória.',
            'transportador_avcb_vencimento.date_format' => 'Insira uma data válida.',
            'transportador_avcb_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',
        ];
    }
}
