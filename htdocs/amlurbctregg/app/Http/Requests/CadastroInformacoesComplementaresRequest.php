<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroInformacoesComplementaresRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'geracao_diaria' => 'required',
            'coleta_diaria' => 'required',
            'numero_colaboradores' => 'required',
            'consumo_mensal' => 'required',
            'estabelecimento_tipo' => 'required',
            'area_total' => 'max:10000000',
            'area_construida' => 'max:10000000',
            'area_construida' => 'required',
            'area_total' => 'required',
            // 'area_total' => 'numeric|max:10000000',
            // 'area_construida' => 'numeric|max:10000000',
        ];
    }

    public function messages()
    {
        return [
            'geracao_diaria.required' => 'Informe a Geração Diária de Resíduos',
            'coleta_diaria.required' => 'Informe os dados da Coleta Diária',
            'numero_colaboradores.required' => 'Selecione o número de colaboradores.',
            'consumo_mensal.required' => 'Selecione o consumo mensal.',
            'estabelecimento_tipo.required' => 'Selecione o local do empreendimento',
            // 'area_total.numeric' => 'Valor precisa ser numérico.',
            'area_total.max' => 'Valor não pode ser maior que dez milhões.',
            // 'area_construida.numeric' => 'Valor precisa ser numérico.',
            'area_construida.max' => 'Valor não pode ser maior que dez milhões.'
        ];
    }
}
