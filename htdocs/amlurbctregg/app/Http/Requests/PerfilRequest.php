<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PerfilRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regras = [
            'nome' => 'required',
            'sobrenome' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($this->id),
            ],
            'data_nascimento' => 'required|date_format:"d/m/Y"',
            'cargo' => '',
            'telefone' => '',
            'ramal' => '',
            'celular' => ''
        ];

        if ($this->permissao) {
            $regras['permissao'] = 'required';
        }

        if ($this->imagem_perfil) {
            $regras['imagem_perfil'] = 'image|max:3073|mimes:jpeg,jpg,png';
        }

        if ($this->senha) {
            $regras['senha'] = 'sometimes|min:8|confirmed';
        }

        return $regras;
    }

    public function messages()
    {
        return [
            'nome.required' => 'O nome é obrigatório.',
            'email.required' => 'O email é obrigatório',
            'email.email' => 'O formato do email não é válido.',
            'imagem_perfil.image' => 'Este arquivo não é uma imagem.',
            'imagem_perfil.max' => 'O arquivo é muito grande.',
            'imagem_perfil.mimes' => 'A extenção deste arquivo não é permitida, somente :mimes',
            'data_nascimento.required' => 'A data de nascimento é obrigatória.',
            'data_nascimento.date_format' => 'A data de nascimento não parece ser válida.',
            'senha.min' => 'A senha deve ter no mínimo :min caracteres.',
            'senha.confirmed' => 'A confirmação da senha não confere com a senha.'
        ];
    }
}