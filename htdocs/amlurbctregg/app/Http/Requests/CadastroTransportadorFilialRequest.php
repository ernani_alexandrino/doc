<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroTransportadorFilialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $retorno = [
            'cnpj' => 'required',
            'razao_social' => 'required',
            'nome_fantasia' => 'required',
            'cep' => 'required',
            'estados' => 'required',
            'cidades' => 'required',
            'endereco' => 'required',
            'numero' => 'required',
            'bairro' => 'required',
            'responsavel' => 'required',
            'telefone_responsavel' => 'required',
            'modal_cartao_cnpj' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_contrato_social' => 'required|max:10240|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_certidao_falencia_concordata' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_balanco_financeiro' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_demonstrativo_contabil' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_certidao_negativa_municipal' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_certidao_negativa_estadual' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_certidao_negativa_federal' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_anotacao_responsabilidade_tecnica' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_anotacao_responsabilidade_tecnica_emissao' => 'required|date_format:"d/m/Y"|before_or_equal:modal_anotacao_responsabilidade_tecnica_vencimento',
            'modal_anotacao_responsabilidade_tecnica_vencimento' => 'required|date_format:"d/m/Y"|after_or_equal:modal_anotacao_responsabilidade_tecnica_emissao',
            'modal_cdl' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_licenca_previa' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_licenca_instalacao' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_licenca_operacao' => 'max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_alvara_prefeitura' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_alvara_prefeitura_emissao' => 'required|date_format:"d/m/Y"|before_or_equal:modal_alvara_prefeitura_vencimento',
            'modal_alvara_prefeitura_vencimento' => 'required|date_format:"d/m/Y"|after_or_equal:modal_alvara_prefeitura_emissao',
            'modal_ctf_ibama' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_ctf_ibama_emissao' => 'required|date_format:"d/m/Y"|before_or_equal:modal_ctf_ibama_vencimento',
            'modal_ctf_ibama_vencimento' => 'required|date_format:"d/m/Y"|after_or_equal:modal_ctf_ibama_emissao',
            'modal_avcb' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'modal_avcb_emissao' => 'required|date_format:"d/m/Y"|before_or_equal:modal_avcb_vencimento',
            'modal_avcb_vencimento' => 'required|date_format:"d/m/Y"|after_or_equal:modal_avcb_emissao',
        ];

        if ($this->email != null) {
            $retorno['email'] = 'email';
        }

        if ($this->modal_cdl_emissao != null) {
            $retorno['modal_cdl_emissao'] = 'date_format:"d/m/Y"|before_or_equal:modal_cdl_vencimento';
        }

        if ($this->modal_cdl_vencimento != null) {
            $retorno['modal_cdl_vencimento'] = 'date_format:"d/m/Y"|after_or_equal:modal_cdl_emissao';
        }

        if ($this->modal_licenca_previa_emissao != null) {
            $retorno['modal_licenca_previa_emissao'] = 'date_format:"d/m/Y"|before_or_equal:modal_licenca_previa_vencimento';
        }

        if ($this->modal_licenca_previa_vencimento != null) {
            $retorno['modal_licenca_previa_vencimento'] = 'date_format:"d/m/Y"|after_or_equal:modal_licenca_previa_emissao';
        }

        if ($this->modal_licenca_instalacao_emissao != null) {
            $retorno['modal_licenca_instalacao_emissao'] = 'date_format:"d/m/Y"|before_or_equal:modal_licenca_instalacao_vencimento';
        }

        if ($this->modal_licenca_instalacao_vencimento != null) {
            $retorno['modal_licenca_instalacao_vencimento'] = 'date_format:"d/m/Y"|after_or_equal:modal_licenca_instalacao_emissao';
        }

        if ($this->modal_licenca_operacao_emissao != null) {
            $retorno['modal_licenca_operacao_emissao'] = 'date_format:"d/m/Y"|before_or_equal:modal_licenca_operacao_vencimento';
        }

        if ($this->modal_licenca_operacao_vencimento != null) {
            $retorno['modal_licenca_operacao_vencimento'] = 'date_format:"d/m/Y"|after_or_equal:modal_licenca_operacao_emissao';
        }

        return $retorno;
    }

    public function messages()
    {
        $max = '3mb';
        $maxContrato = '10mb';

        return [
            'cnpj.required' => 'O CNPJ é obrigatório.',
            'razao_social.required' => 'A razão social é obrigatória.',
            'nome_fantasia.required' => 'O nome fantasia é obrigatório.',
            'cep.required' => 'O CEP é obrigatório.',
            'estados.required' => 'Selecione um estado.',
            'cidades.required' => 'Selecione uma cidade.',
            'endereco.required' => 'O endereço é obrigatório.',
            'numero.required' => 'O número é obrigatório.',
            'bairro.required' => 'O bairro é obrigatório',
            'responsavel.required' => 'O nome de contato é obrigatório.',
            'email.email' => 'O email do contato não parece ser válido.',
            'telefone_responsavel.required' => 'O telefone do contato é obrigatório.',

            'modal_cartao_cnpj.required' => 'O cartão CNPJ é obrigatório.',
            'modal_cartao_cnpj.max' => "O arquivo deve ter até $max",
            'modal_cartao_cnpj.mimes' => 'Formato de arquivo inválido.',

            'modal_contrato_social.required' => 'O contrato social é obrigatório.',
            'modal_contrato_social.max' => "O arquivo deve ter até $maxContrato",
            'modal_contrato_social.mimes' => 'Formato de arquivo inválido.',

            'modal_certidao_falencia_concordata.required' => 'A certidão de falência ou concordata é obrigatória.',
            'modal_certidao_falencia_concordata.max' => "O arquivo deve ter até $max",
            'modal_certidao_falencia_concordata.mimes' => 'Formato de arquivo inválido.',

            //'modal_balanco_financeiro' => '',
            'modal_balanco_financeiro.max' => "O arquivo deve ter até $max",
            'modal_balanco_financeiro.mimes' => 'Formato de arquivo inválido.',

            //'modal_demonstrativo_contabil' => '',
            'modal_demonstrativo_contabil.max' => "O arquivo deve ter até $max",
            'modal_demonstrativo_contabil.mimes' => 'Formato de arquivo inválido.',

            'modal_certidao_negativa_municipal.required' => 'A certidão negativa municipal é obrigatória.',
            'modal_certidao_negativa_municipal.max' => "O arquivo deve ter até $max",
            'modal_certidao_negativa_municipal.mimes' => 'Formato de arquivo inválido.',

            'modal_certidao_negativa_estadual.required' => 'A certidão negativa estadual é obrigatória.',
            'modal_certidao_negativa_estadual.max' => "O arquivo deve ter até $max",
            'modal_certidao_negativa_estadual.mimes' => 'Formato de arquivo inválido.',

            'modal_certidao_negativa_federal.required' => 'A certidão negativa federal é obrigatória.',
            'modal_certidao_negativa_federal.max' => "O arquivo deve ter até $max",
            'modal_certidao_negativa_federal.mimes' => 'Formato de arquivo inválido.',

            'modal_anotacao_responsabilidade_tecnica.required' => 'A ART é obrigatória.',
            'modal_anotacao_responsabilidade_tecnica.max' => "O arquivo deve ter até $max",
            'modal_anotacao_responsabilidade_tecnica.mimes' => 'Formato de arquivo inválido.',
            'modal_anotacao_responsabilidade_tecnica_emissao.required' => 'A data de emissão da ART é obrigatória.',
            'modal_anotacao_responsabilidade_tecnica_emissao.date' => 'Insira uma data válida.',
            'modal_anotacao_responsabilidade_tecnica_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'modal_anotacao_responsabilidade_tecnica_vencimento.required' => 'A data de vencimento da ART é obrigatória.',
            'modal_anotacao_responsabilidade_tecnica_vencimento.data' => 'Insira uma data válida.',
            'modal_anotacao_responsabilidade_tecnica_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            //'modal_cdl' => '',
            'modal_cdl.max' => "O arquivo deve ter até $max",
            'modal_cdl.mimes' => 'Formato de arquivo inválido.',
            'modal_cdl_emissao.date' => 'Insira uma data válida.',
            'modal_cdl_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'modal_cdl_vencimento.date' => 'Insira uma data válida.',
            'modal_cdl_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            //'modal_licenca_previa' => '',
            'modal_licenca_previa.max' => "O arquivo deve ter até $max",
            'modal_licenca_previa.mimes' => 'Formato de arquivo inválido.',
            'modal_licenca_previa_emissao.date' => 'Insira uma data válida.',
            'modal_licenca_previa_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'modal_licenca_previa_vencimento.date' => 'Insira uma data válida.',
            'modal_licenca_previa_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            //'modal_licenca_instalacao' => '',
            'modal_licenca_instalacao.max' => "O arquivo deve ter até $max",
            'modal_licenca_instalacao.mimes' => 'Formato de arquivo inválido.',
            'modal_licenca_instalacao_emissao.date' => 'Insira uma data válida.',
            'modal_licenca_instalacao_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'modal_licenca_instalacao_vencimento.date' => 'Insira uma data válida.',
            'modal_licenca_instalacao_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            //'modal_licenca_operacao' => '',
            'modal_licenca_operacao.max' => "O arquivo deve ter até $max",
            'modal_licenca_operacao.mimes' => 'Formato de arquivo inválido.',
            'modal_licenca_operacao_emissao.date' => 'Insira uma data válida.',
            'modal_licenca_operacao_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'modal_licenca_operacao_vencimento.date' => 'Insira uma data válida.',
            'modal_licenca_operacao_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            'modal_alvara_prefeitura.required' => 'O alvará da prefeitura é obrigatório.',
            'modal_alvara_prefeitura.max' => "O arquivo deve ter até $max",
            'modal_alvara_prefeitura.mimes' => 'Formato de arquivo inválido.',
            'modal_alvara_prefeitura_emissao.required' => 'A data de emissão do alvará da prefeitura é obrigatória.',
            'modal_alvara_prefeitura_emissao.date' => 'Insira uma data válida.',
            'modal_alvara_prefeitura_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'modal_alvara_prefeitura_vencimento.required' => 'A data de vencimento do alvará da prefeitura é obrigatória.',
            'modal_alvara_prefeitura_vencimento.date' => 'Insira uma data válida',
            'modal_alvara_prefeitura_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            'modal_ctf_ibama.required' => 'O CTF Ibama é obrigatório.',
            'modal_ctf_ibama.max' => "O arquivo deve ter até $max",
            'modal_ctf_ibama.mimes' => 'Formato de arquivo inválido.',
            'modal_ctf_ibama_emissao.required' => 'A data de emissão do CTF Ibama é obrigatória.',
            'modal_ctf_ibama_emissao.date' => 'Insira uma data válida.',
            'modal_ctf_ibama_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'modal_ctf_ibama_vencimento.required' => 'A data de vencimento do CTF Ibama é obrigatória.',
            'modal_ctf_ibama_vencimento.date' => 'Insira uma data válida',
            'modal_ctf_ibama_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',

            'modal_avcb.required' => 'O AVCB é obrigatório.',
            'modal_avcb.max' => "O arquivo deve ter até $max",
            'modal_avcb.mimes' => 'Formato de arquivo inválido.',
            'modal_avcb_emissao.required' => 'A data de emissão do AVCB é obrigatória.',
            'modal_avcb_emissao.date' => 'Insira uma data válida.',
            'modal_avcb_emissao.before_or_equal' => 'Deve ser uma data menor ou igual ao vencimento.',
            'modal_avcb_vencimento.required' => 'A data de vencimento do AVCB é obrigatória.',
            'modal_avcb_vencimento.date' => 'Insira uma data válida.',
            'modal_avcb_vencimento.after_or_equal' => 'Deve ser uma data maior ou igual a emissão.',
        ];
    }
}
