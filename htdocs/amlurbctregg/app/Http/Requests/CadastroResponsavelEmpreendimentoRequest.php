<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroResponsavelEmpreendimentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'nome_socio' => 'required',
            'numero_rg_socio' => 'required',
            'numero_cpf_socio' => 'required',

            'numero_rg_socio_file' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',
            'numero_cpf_socio_file' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',

            'numero_rg_socio_file_enviado' => 'required',
            'numero_cpf_socio_file_enviado' => 'required',

//            'numero_rg_socio_file_2_enviado' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',
//            'numero_cpf_socio_2_file_enviado' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',
//
//            'numero_rg_socio_3_file_enviado' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',
//            'numero_cpf_socio_3_file_enviado' => 'max:10240|mimes:jpeg,jpg,bmp,png,pdf',


        ];
    }

    public function message()
    {
        $max = '3mb';

        return [
            'nome_socio.required' => 'Nome do Sócio obrigatório',
            'numero_rg_socio.required' => 'O número do RG é de preenchimento obrigatório',
            'numero_cpf_socio.required' => 'O número do CPF é de preenchimento obrigatório',
            'numero_rg_socio_file_enviado.required' => 'O envio do RG do Sócio é obrigatório',
            'numero_cpf_socio_file_enviado.required' => 'O envio do CPF do Sócio é obrigatório',
//
            'numero_rg_socio_file.max' => "O arquivo deve ter até $max",
            'numero_cpf_socio_file.max' => "O arquivo deve ter até $max",
            'numero_rg_socio_file.mimes' => 'Formato de arquivo inválido.',
            'numero_cpf_socio_file.mimes' => 'Formato de arquivo inválido.',

        ];
    }
}
