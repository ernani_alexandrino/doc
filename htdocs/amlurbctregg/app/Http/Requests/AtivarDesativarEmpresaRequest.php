<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Faz a validação dos dados enviados no form de ativação/desativação da empresa no módulo fiscal
 *
 * Class AtivarDesativarEmpresaRequest
 *
 * @property string descricao
 *
 * @package Amlurb\Http\Requests
 */
class AtivarDesativarEmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descricao' => 'required|min:5'
        ];
    }

    public function messages()
    {
        return [
            'descricao.required' => 'É necessário fornecer uma descrição.',
            'descricao.min' => 'É necessário um mínimo de :min caracteres.',
        ];
    }
}
