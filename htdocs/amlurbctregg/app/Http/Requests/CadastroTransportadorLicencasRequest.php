<?php

namespace Amlurb\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroTransportadorLicencasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cartao_cnpj' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'contrato_social' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'certidao_falencia_concordata' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'balanco_financeiro' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'demonstrativo_contabil' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'certidao_negativa_municipal' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'certidao_negativa_estadual' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
            'certidao_negativa_federal' => 'required|max:3073|mimes:jpeg,jpg,bmp,png,pdf',
        ];
    }

    public function messages()
    {
        return [
            'cartao_cnpj.required' => 'O Cartão CNPJ é obrigatório',
            'contrato_social.required' => 'O Contrato Social é obrigatório',
            'certidao_falencia_concordata.required' => 'A Certidão de Falência ou Concordata é obrigatória',
            'balanco_financeiro.required' => 'O Balanço Financeiro é obrigatório',
            'demonstrativo_contabil.required' => 'O Demonstrativo Contábil é obrigatório',
            'certidao_negativa_municipal.required' => 'A Certidão Negativa Municipal é obrigatória',
            'certidao_negativa_estadual.required' => 'A Certidão Negativa Estadual é obrigatória',
            'certidao_negativa_federal.required' => 'A Certidão Negativa Federal é obrigatória',
            'transportador_rg_num_responsavel_tecnico' => 'O N° do RG do Responsável Técnico é Obrigatório',
            'transportador_nome_responsavel_tecnico' => 'O Nome do Responsável Técnico é Obrigatório',
            'transportador_cpf_num_responsavel_tecnico' => 'O N° do CPF do Responsável Técnico é Obrigatório',
            'transportador_cpf_responsavel_tecnico' => 'A Cópia do CPF do Responsável Técnico é Obrigatório',
            'transportador_rg_responsavel_tecnico' => 'A Cópia do RG do Responsável Técnico é Obrigatório'
        ];
    }
}
