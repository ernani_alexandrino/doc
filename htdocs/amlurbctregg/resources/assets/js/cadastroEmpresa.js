$(function () {

    funcionamentoAbasCadastroEmpresa();
    buscaEmpresaPorCnpj();
    vincularEmpresaPorCnpj();

    //Faz o cadastro ajax das licenças da empresa
    var $formTerceiraEtapa = $('#formTerceiraEtapa');

    $formTerceiraEtapa.on('submit', function (e) {
        e.preventDefault();

        var $this = $(this),
            $dados = $(this).serialize(),
            $loading = $('#loading-cadastro'),
            $modalErros = $('#modalDisplayErros'),
            $containerMsg = $('.msg');

        $.ajax({
            url: window.baseUrl + '/empresa/cadastro-documentos',
            data: $dados,
            type: "POST",
            beforeSend: function () {

                $modalErros.modal('hide');
                $loading.addClass('ativo');
                $('.form-group').removeClass('error');
                $('p.text-danger', $this).remove();
                $containerMsg.hide().text('');

            },
            success: function ($retorno) {

                $loading.removeClass('ativo');

                if ($retorno.status) {

                    if ($retorno.redirect) {
                        window.location = $retorno.redirect;
                    }
                }
            },
            error: function ($retorno) {

                $loading.removeClass('ativo');
                $modalErros.modal('show');

                // Erro retornado pelo laravel quando o post não for válido.
                if ($retorno.status === 422) {

                    $.each($retorno.responseJSON.errors, function (key, value) {

                        var $elemento = $('#' + key);

                        $elemento.parents('.form-group').addClass('error').append("<p class=\"text-danger\">" + value + "</p>");
                    });
                }
            }
        });

        return false;
    });

    // if ($formTerceiraEtapa.length > 0) {

    //Faz o envio individual dos arquivos de licenças
    var $empresaId = parseInt($('#formTerceiraEtapa').find('#empresa_id').val()),
        $cartaoCNPJ = $('#empresa_cartao_cnpj'),
        $iptuEmpresa = $('#empresa_iptu'),
        $contratoSocial = $('#transportador_contrato_social'),
        $docCCM = $('#transportador_doc_ccm'),
        $certidaoFalenciaConcordata = $('#transportador_certidao_falencia_concordata'),
        $balancoFinanceiro = $('#transportador_balanco_financeiro'),
        $demonstrativoContabil = $('#transportador_demonstrativo_contabil'),
        $certidaoNegativaMunicipal = $('#transportador_certidao_negativa_municipal'),
        $certidaoNegativaEstadual = $('#transportador_certidao_negativa_estadual'),
        $certidaoNegativaFederal = $('#transportador_certidao_negativa_federal'),
        $art = $('#transportador_anotacao_responsabilidade_tecnica'),
        $rgResponsavelTecnico = $('#transportador_rg_responsavel_tecnico'),
        $cpfResponsavelTecnico = $('#transportador_cpf_responsavel_tecnico'),
        $creaResponsavelTecnico = $('#transportador_crea_responsavel_tecnico'),
        $crf_caixa = $('#transportador_crf_caixa'),
        $cdl = $('#transportador_certificado_dispensa_licenca'),
        $licenca = $('#licenka'),
        // $licencaPrevia = $('#licenca_previa'),
        // $licencaInstalacao = $('#licenca_instalacao'),
        // $licencaOperacao = $('#licenca_operacao'),
        $alvaraPrefeitura = $('#transportador_alvara_prefeitura'),
        $ctfIbama = $('#transportador_ctf_ibama'),
        $transportador_avcb = $('#transportador_avcb'),

        $avcb = $('#avcb'),
        $file_rg = $('#condominio_file_rg'),
        $file_cpg = $('#condominio_file_cpf'),
        $ata = $('#condominio_ata'),
        $condominio_sindico_rg = $('#condominio_sindico_rg'),
        $condominio_sindico_cpf = $('#condominio_sindico_cpf'),
        $contribuicao = $('#condominio_contribuicao'),
        $procuracao = $('#condominio_procuracao'),
        $iptu = $('#iptu');

    //cadastro Responsaveis do Destino Final Residuos e Cooperativa Ambiental

    var $rgSocio = $('#numero_rg_socio_file'),
        $cpSocio = $('#numero_cpf_socio_file'),
        $rgSocio2 = $('#numero_rg_socio_2_file'),
        $cpfSocio2 = $('#numero_cpf_socio_2_file'),
        $rgSocio3 = $('#numero_rg_socio_3_file'),
        $cpfSocio3 = $('#numero_cpf_socio_3_file');

    var $rgPresidente = $('#numero_rg_presidente_file'),
        $cpfPresidente = $('#numero_cpf_presidente_file');


    $rgPresidente.on("change", enviarArquivo($rgPresidente, '', 'empresa'));
    $cpfPresidente.on("change", enviarArquivo($cpfPresidente, '', 'empresa'));

    $cartaoCNPJ.on("change", enviarArquivo($cartaoCNPJ, $empresaId, 'empresa'));
    $iptuEmpresa.on("change", enviarArquivo($iptuEmpresa, $empresaId, 'empresa'));
    $contratoSocial.on("change", enviarArquivo($contratoSocial, $empresaId));
    $docCCM.on("change", enviarArquivo($docCCM, $empresaId));
    $certidaoFalenciaConcordata.on("change", enviarArquivo($certidaoFalenciaConcordata, $empresaId));
    $balancoFinanceiro.on("change", enviarArquivo($balancoFinanceiro, $empresaId));
    $demonstrativoContabil.on("change", enviarArquivo($demonstrativoContabil, $empresaId));
    $certidaoNegativaMunicipal.on("change", enviarArquivo($certidaoNegativaMunicipal, $empresaId));
    $certidaoNegativaEstadual.on("change", enviarArquivo($certidaoNegativaEstadual, $empresaId));
    $certidaoNegativaFederal.on("change", enviarArquivo($certidaoNegativaFederal, $empresaId));
    $art.on("change", enviarArquivo($art, $empresaId));
    $rgResponsavelTecnico.on("change", enviarArquivo($rgResponsavelTecnico, $empresaId));
    $cpfResponsavelTecnico.on("change", enviarArquivo($cpfResponsavelTecnico, $empresaId));
    $creaResponsavelTecnico.on("change", enviarArquivo($creaResponsavelTecnico, $empresaId));
    $crf_caixa.on("change", enviarArquivo($crf_caixa, $empresaId));
    $cdl.on("change", enviarArquivo($cdl, $empresaId));
    $licenca.on("change", enviarArquivo($licenca, $empresaId));
    // $licencaPrevia.on("change", enviarArquivo($licencaPrevia, $empresaId));
    // $licencaInstalacao.on("change", enviarArquivo($licencaInstalacao, $empresaId));
    // $licencaOperacao.on("change", enviarArquivo($licencaOperacao, $empresaId));
    $alvaraPrefeitura.on("change", enviarArquivo($alvaraPrefeitura, $empresaId));
    $ctfIbama.on("change", enviarArquivo($ctfIbama, $empresaId));
    $avcb.on("change", enviarArquivo($avcb, $empresaId));
    $transportador_avcb.on("change", enviarArquivo($transportador_avcb, $empresaId));
    $iptu.on("change", enviarArquivo($iptu, $('#cnpj').val().replace('/','').replace('.','').replace('-','')));
    // }


    // inputs file emp-resa tipo Condominio Misto

    $file_rg.on("change", enviarArquivo($file_rg, '', 'empresa'));
    $file_cpg.on("change", enviarArquivo($file_cpg, '', 'empresa'));
    $ata.on("change",enviarArquivo($ata, '', 'empresa'));
    $contribuicao.on("change",enviarArquivo($contribuicao, '', 'empresa'));
    $condominio_sindico_rg.on("change",enviarArquivo($condominio_sindico_rg, '', 'empresa'));
    $condominio_sindico_cpf.on("change",enviarArquivo($condominio_sindico_cpf, '', 'empresa'));
    $procuracao.on("change",enviarArquivo($procuracao, '', 'empresa'));

    // onchange Responsaveis do Destino Final Residuos e Cooperativa Ambiental

    $rgSocio.on("change", enviarArquivo($rgSocio, '', 'empresa'));
    $cpSocio.on("change", enviarArquivo($cpSocio, '', 'empresa'));
    $rgSocio2.on("change", enviarArquivo($rgSocio2, '', 'empresa'));
    $cpfSocio2.on("change", enviarArquivo($cpfSocio2, '', 'empresa'));
    $rgSocio3.on("change", enviarArquivo($rgSocio3, '', 'empresa'));
    $cpfSocio3.on("change", enviarArquivo($cpfSocio3, '', 'empresa'));

    //Desativa a data de vencimento no cadastro de licenças
    $('.checkVencimento').on("change", function () {

        var $this = $(this),
            $idVencimento = $this.val(),
            $checado = $this.prop('checked');

        if ($checado) {
            $('#' + $idVencimento).val('').prop('disabled', true);
        } else {
            $('#' + $idVencimento).prop('disabled', false);
        }
    });

    //Mostrando a primeira parte do cadastro da empresa
    $('.cnpjContinua').on('click', function () {


    });

    //Form de cadastro da empresa
    let $formCadastroEmpresa = $('#formCadastroEmpresa');

    /// nao deixa enviar formulario se apertarem enter no cnpj
    /// verificar problema no resto do form
    $('input', $formCadastroEmpresa).keypress(function (event) {

        // enter
        if (event.keyCode === 13) {
            return false;
        }
    });
    function modalAvisoServicoSaude(){

        var $selectRamoSelecionado = $('#ramo_atividade :selected').text();        
        
        if($selectRamoSelecionado == "Serviço de Saúde"){

            $("#modalAvisoCadastroServicoSaude").modal({
                backdrop: false,
                keyboard:false,
                show:true
            }); 
                            
            $("#confirmarModalAvisoServicoSaude").click(function(){               
                $("#modalAvisoCadastroServicoSaude").modal('hide');  
            });
    
            $("#cancelarModalAvisoServicoSaude").click(function(){                
                window.location = 'login';
            });                    
        };

       
    }
    /**
     * Principais dados da empresa
     */
    $('.btnValidaPrimeiraEtapa', $formCadastroEmpresa).on('click', function (e) {
        e.preventDefault();          

        let $this = $(this);
        let $containerInput = $('.containerPrimeiraEtapa');
        let $campos = $containerInput.find(':input').serialize();
        let $dados = {
            'url': '/valida-empresa',
            'campos': $campos,
            'this': $this
        };

        modalAvisoServicoSaude();  
        
        enviaFormulario($dados);               
    });

    /**
     * Responsável pela empresa, também usado para login
     */
    $('.btnValidaResponsavel', $formCadastroEmpresa).on('click', function (e) {
        e.preventDefault();

        let $this = $(this);
        let $containerInput = $('.containerResponsavel');
        let $campos = $containerInput.find(':input').serialize();        
        let $dados = {
            'url': '/valida-responsavel',
            'campos': $campos,
            'this': $this
        };

        enviaFormulario($dados);
    });

    /**
     * Informações complementares
     */
    $('.btnValidaInformacoesComplementares', $formCadastroEmpresa).on('click', function (e) {
        e.preventDefault();

        let $this = $(this);
        let $containerInput = $('.containerInformacoesComplementares');
        let $campos = $containerInput.find(':input').serialize();
        let $dados = {
            'url': '/valida-informacoes-complementares',
            'campos': $campos,
            'this': $this
        };

        enviaFormulario($dados);
    });

    $('.btnValidaSocio', $formCadastroEmpresa).on('click', function (e) {
        e.preventDefault();

        let $this = $(this);
        let $containerInput = $('.containerSocio');
        let $campos = $containerInput.find(':input').serialize();
        let $dados = {
            'url': '/valida-responsavel-empreendimento',
            'campos': $campos,
            'this': $this
        };

        enviaFormulario($dados);
    });

    $('.btnValidaSocio2', $formCadastroEmpresa).on('click', function (e) {
        e.preventDefault();

        let $this = $(this);
        let $containerInput = $('.containerSocio2');
        let $campos = $containerInput.find(':input').serialize();
        let $dados = {
            'url': '/valida-responsavel-empreendimento-coo',
            'campos': $campos,
            'this': $this
        };

        enviaFormulario($dados);
    });

    /**
     * Transportador
     */
    $('.btnValidaTransportador', $formCadastroEmpresa).on('click', function (e) {
        e.preventDefault();

        let $this = $(this);
        let $containerInput = $('.containerTransportador');
        let $campos = $containerInput.find(':input').serialize();
        /*-/ Abaixo, var $url criada aqui e inserido condição if By Mr.Goose em 11 abr 19, afim de tratar o Caso Especial de Cooperativas. /-*/
        if ($('.btnValidaTransportador').hasClass('btnValidaDocsCoo'))
            $url = '/valida-docs-coo';
        else
            $url = '/valida-transportador';
        /*-/ Acima, var $url criada aqui e inserido condição if By Mr.Goose em 11 abr 19, afim de tratar o Caso Especial de Cooperativas. /-*/
        let $dados = {
            'url': $url,
            'campos': $campos,
            'this': $this
        };

        enviaFormulario($dados);
    });

    $('.btnValidaCooperativa', $formCadastroEmpresa).on('click', function (e) {
        e.preventDefault();

        let $this = $(this);
        let $containerInput = $('.containerCooperativa');
        let $campos = $containerInput.find(':input').serialize();
        let $dados = {
            'url': '/valida-cooperativa',
            'campos': $campos,
            'this': $this
        };

        enviaFormulario($dados);
    });

    /**
     * Termos de uso
     */
    $('.btnValidaTermosDeUso', $formCadastroEmpresa).on('click', function () {


        let $this = $(this);
        let $containerInput = $('.containerTermosUso');
        let $campos = $containerInput.find(':input').serialize();
        let $dados = {
            'url': '/valida-termos-de-uso',
            'campos': $campos,
            'this': $this
        };

        enviaFormulario($dados);
    });

    /**
     * Cadastro de condomínio responsável
     */
    $('.btnValidaCondominio', $formCadastroEmpresa).on('click', function () {

        let $this = $(this);
        let $containerInput = $('.containerCondominioResponsavel');
        let $campos = $containerInput.find(':input').serialize();
        let $dados = {
            'url': '/valida-condominio-responsavel',
            'campos': $campos,
            'this': $this
        };

        enviaFormulario($dados);
    });

    /**
     * CCM.
     - by Mr.Goose em 06 abr 19
     */
    $('#licenca_municipal').blur(function(){
        $('#ccm_digitado').html($(this).val());
    });

    $formCadastroEmpresa.on("submit", function () {

        loading(1);

        $('button[type=submit]', $(this)).prop('disabled', true);
        $('a.prevBtn', $(this)).prop('disabled', true);

    });

    $('.termoDeUso', $formCadastroEmpresa).on('click', function (e) {
        e.preventDefault();
        $('.termo-privacidade').css({
            "overflow" : "scroll",
            'height'  : '300px',
            'font-size' : '10px',
            'display' : 'block',
            'margin-bottom' : '20px'
        });
    });
});

function enviarArquivo($input, $empresaId, url) {

    if ($empresaId == undefined)
        var $empresaId = '';

    if (url == undefined)
        var url = 'transportador';

    var classBtn = ".btn-upload";

    switch (url){
        case 'empresa':
            url =  "/valida-empresa-arquivos/";
        break;

        case 'condominio':
            url =  "/valida-condominio-arquivos/";
        break;
        default:
            url =  "/valida-transportador-arquivos/";
        break;
    }


    if ($input.length > 0) {
        return $input.fileupload({
            url: url + $input[0].id,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf)$/i,
            maxFileSize: 3073,
            progressServerRate: 0.3,
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]', $("#formCadastroEmpresa")).val()
            },
            add: function (e, data) {
                $(classBtn, $(this).parent().parent()).removeClass('error success');
                $('.progress .bar', $(this).parent()).css('width', '0%').attr('data-content', '0');
                data.submit();
            },
            progressall: function (e, data) {

                let progress = parseInt(data.loaded / data.total * 100, 10);

                $('.progress p', $(this).parent()).html('Enviando...');
                $('.progress .bar', $(this).parent()).css('width', progress + '%').attr('data-content', progress + '%');
            },
            error: function (e, data) {
                console.log('*** Error ***');
                console.log(data);
            },
            done: function (e, data) {
                $(this).parents('.form-group').find('p.text-danger').remove();

                if (data.result.status) {
                    $('.progress p', $(this).parent()).html("Enviado com sucesso");
                    $(classBtn, $(this).parent().parent()).addClass('success');
                    $('.form-group', $(this).parent().parent().parent()).removeClass('error');

                    //Grava em um input hidden somente para validar se o arquivo ja foi enviado para o servidor, já que o input file fica vazio após o envio por ajax.
                    $('#' + this.id + '_enviado').val($input[0].files[0].name);
                } else {

                    let $mensagem = '';

                    $.each(data.result.erro, function (key, value) {
                        $mensagem = value[0];
                    });

                    $(this).parents('.form-group').addClass('error').append('<p class="text-danger">' + $mensagem + '</p>');

                    $('.progress p', $(this).parent()).html('Falhou, tente novamente');
                    $(classBtn, $(this).parent().parent()).addClass('error');
                }
            }
        });
    }
}

function enviaFormulario($dados) {

    let $formCadastroEmpresa = $('#formCadastroEmpresa');

    return $.ajax({
        url: $dados.url,
        type: 'POST',
        data: $dados.campos,
        headers: {
            'X-CSRF-TOKEN': $('[name="_token"]', $formCadastroEmpresa).val()
        },
        beforeSend: function () {
            $('.form-group').removeClass('error').find('p.text-danger').remove();
            loading(1);
        },
        success: function ($retorno) {
            if ($retorno.status) {

                let $containerAtual = $('.setup-content' + $retorno.idDestino);

                if ($retorno.termoDeUso) {
                    // let pathPdfTermoDeUso = '/templates/termo-de-uso/modelo_termo_de_uso_' + str_pad($retorno.termoDeUso, 2, '0', STR_PAD_LEFT) + '.pdf';
                    // $('a.termoDeUso', $containerAtual).attr('href', pathPdfTermoDeUso);
                    $('.termo-privacidade').html($retorno.termoDeUso);

                }

                $dados.this.addClass("nextBtn").attr("href", $retorno.idDestino);
                console.log($retorno);
                if ($retorno.idAnterior) {
                    $('.prevBtn', $containerAtual).attr('href', $retorno.idAnterior);
                }

                $('.setup-content').hide();
                $containerAtual.show();

                loading(0);
                window.scrollTo(0, 0);
            }
        },
        error: function ($retorno) {

            // Erro retornado pelo laravel quando o post não for válido.
            if ($retorno.status === 422) {

                $.each($retorno.responseJSON.errors, function (campo, mensagem) {

                    let $elemento = $('[name="' + campo + '"]');

                    $elemento.closest(".form-group").addClass("error").append("<p class=\"text-danger\">" + mensagem + "</p>");
                });
            }

            loading(0);
            window.scrollTo(0, 0);
        }
    });
}

function funcionamentoAbasCadastroEmpresa() {

    let navListItems = $('div.setup-panel div a');
    let $body = $('html, body');
    let allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();

        let $target = $($(this).attr('href'));
        let $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
        }
    });

    $('.prevBtn').on('click', function (e) {
        e.preventDefault();

        let $target = $($(this).attr('href'));
        let $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.find('.nextBtn').removeClass('nextBtn');
            $target.show();
        }
        window.scrollTo(0, 0);
    });

    $('.nextBtn').on('click', function () {

        let curStep = $(this).closest(".setup-content");
        let curStepBtn = curStep.attr("id");
        let nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");
        let curInputs = curStep.find("input:required, select:required");
        let isValid = true;

        $(".form-group").removeClass("error");

        for (let i = 0; i < curInputs.length; i++) {

            if (!curInputs[i].validity.valid || (curInputs[i].value == '')) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("error");
            }
            window.scrollTo(0, 0);
        }

        if (isValid) {
            // nextStepWizard.removeAttr('disabled').trigger('click');

            $body.animate({
                scrollTop: $body.offset().top
            }, 800);

        } else {

            $body.animate({
                scrollTop: $('#' + curInputs[0].id).offset().top - 100
            }, 800);

        }
    });

    $('div.setup-panel div a.btn-success').trigger('click');
}

function buscaEmpresaPorCnpj() {

    $('#cnpj').focusout(function () {
        $('.containerPrimeiraEtapaContinuacao').hide();
    });
    $('.cnpjClica').on('click', function () {  

        let $cnpjValido = valida_cnpj($('#cnpj').val());
        
        if ($cnpjValido) {

            $(this).parent().find(".text-danger").text("");

            var $cnpj = $('#cnpj').val().replace(/[^\d]+/g, ''),
                url = "/api/cnpj-empresa/" + $cnpj,
                $razaoSocial = $('#razao_social'),
                $nomeFantasia = $('#nome_fantasia'),
                $licencaMunicipal = $('#licenca_municipal'),
                $inscricaoEstadual = $('#inscricao_estadual'),
                $ramoAtividade = $('#ramo_atividade'),
                $ramoAtividadeOption = $('#ramo_atividade option'),
                $tipoAtividadeOption = $('#tipo_atividade option'),
                $tipoAtividade = $('#tipo_atividade'),
                $telefone = $('#telefone'),
                $endereco = $('#endereco'),
                $numero = $('#numero'),
                $complemento = $('#complemento'),
                $bairro = $('#bairro'),
                $cep = $('#cep'),
                $estado = $('#estado option'),
                $cidade = $('#cidade option'),
                $pontoReferencia = $('#ponto_referencia'),
                $nomeResponsavel = $('#responsavel'),
                $emailResponsavel = $('#email'),
                $ramalResponsavel = $('#ramal'),
                $areaTotal = $('#area_total');
                $areaConstruida = $('#area_construida');
                $geracao_diaria = $('.geracao_diaria');
                $cargo = $('#cargo'),
                $telefoneResponsavel = $('#telefone_responsavel'),
                $celular = $('#celular_responsavel'),
                estadoId = 0,
                estadoNome = '',
                cidadeId = 0,
                cidadeNome = '';

            let $containerMensagemCnpj = $('.mensagemCnpj');
            let $btnCnpjContinua = $('.cnpjContinua');

            $('.cnpj_digitado').html($('#cnpj').val()); // linha inserida by Mr.Goose em 06 abr 19 afim de mostrar o CNPJ no Campo para Anexar o CNPJ.

            $.ajax({
                url: url,
                beforeSend: function () {
                    loadingAjax(1);
                    $('.containerPrimeiraEtapaContinuacao').hide();
                    $containerMensagemCnpj.addClass('hide').find('.alert').removeClass('alert-danger').html('');
                },
                success: function (data) {

                    loadingAjax(0);

                    if (data.obj.mensagem) {

                        $containerMensagemCnpj.removeClass('hide').find('.alert').addClass('alert-danger').html(data.obj.mensagem);

                        return false;
                    }

                    if ($.isEmptyObject(data.error)) {

                        cepTriggered = false;
                        
                        // Se houver cadastro na base, traz todas essas informações
                        if (data.dadosEmpresa) {
                            $('.containerPrimeiraEtapaContinuacao').show();
                     
                            $razaoSocial.val(data.dadosEmpresa.inf.razao_social);
                            $nomeFantasia.val(data.dadosEmpresa.inf.nome_comercial);

                            $licencaMunicipal.val(data.dadosEmpresa.inf.im);
                            $inscricaoEstadual.val(data.dadosEmpresa.inf.ie);
                            $telefone.val(data.dadosEmpresa.inf.telefone);

                            $nomeResponsavel.val(data.dadosEmpresa.inf.nome_responsavel);
                            $emailResponsavel.val(data.dadosEmpresa.inf.email);
                            $ramalResponsavel.val(data.dadosEmpresa.inf.ramal);
                            $cargo.val(data.dadosEmpresa.inf.cargo);
                            $telefoneResponsavel.val(data.dadosEmpresa.inf.telefone_responsavel);
                            $celular.val(data.dadosEmpresa.inf.celular);
                    
                            if (data.dadosEmpresa.endereco) {

                                $cep.val(data.dadosEmpresa.endereco.cep);
                                $endereco.val(data.dadosEmpresa.endereco.endereco);
                                $numero.val(data.dadosEmpresa.endereco.numero);
                                $bairro.val(data.dadosEmpresa.endereco.bairro);
                                $complemento.val(data.dadosEmpresa.endereco.complemento);
                                $pontoReferencia.val(data.dadosEmpresa.endereco.ponto_referencia);
                                //$('#estado').val(data.dadosEmpresa.endereco.estado_id);

                                //estadoId = data.dadosEmpresa.endereco.estado_id;
                                cidadeId = data.dadosEmpresa.endereco.cidade_id;
                                
                                estadoNome = data.dadosEmpresa.estado.sigla;
                                cidadeNome = data.dadosEmpresa.cidade.nome;
                                
                            }

                            $ramoAtividade.val(data.dadosEmpresa.ramo.ramo_atividade_id);
                            $tipoAtividade.val(data.dadosEmpresa.ramo.tipo_ramo_atividade_id);
                            
                            
                            var $radios = $('input:radio[name=geracao_diaria]');
                            if(1 === data.dadosEmpresa.ramo.frequencia_geracao_id) {
                                $radios.filter('[value=1]').prop('checked', true);
                            }else if(2 === data.dadosEmpresa.ramo.frequencia_geracao_id){
                                $radios.filter('[value=2]').prop('checked', true);
                            }else if(3 === data.dadosEmpresa.ramo.frequencia_geracao_id){
                                $radios.filter('[value=3]').prop('checked', true);
                            }else if(4 === data.dadosEmpresa.ramo.frequencia_geracao_id){
                                $radios.filter('[value=3]').prop('checked', true);
                            }
                            
                            var $radioColeta = $('input:radio[name=coleta_diaria]');
                            if(1 === data.dadosEmpresa.ramo.frequencia_coleta_id) {
                                $radioColeta.filter('[value=1]').prop('checked', true);
                            }else if(2 == data.dadosEmpresa.ramo.frequencia_coleta_id){
                                $radioColeta.filter('[value=2]').prop('checked', true);
                            }else if(3 == data.dadosEmpresa.ramo.frequencia_coleta_id){
                                $radioColeta.filter('[value=3]').prop('checked', true);
                            }else if(4 == data.dadosEmpresa.ramo.frequencia_coleta_id){
                                $radioColeta.filter('[value=4]').prop('checked', true);
                            }else if(5 == data.dadosEmpresa.ramo.frequencia_coleta_id){
                                $radioColeta.filter('[value=5]').prop('checked', true);
                            }

                            var $radioColaboradores = $('input:radio[name=numero_colaboradores]');
                            if(1 === data.dadosEmpresa.ramo.colaboradores_numero_id) {
                                $radioColaboradores.filter('[value=1]').prop('checked', true);
                            }else if(2 == data.dadosEmpresa.ramo.colaboradores_numero_id){
                                $radioColaboradores.filter('[value=2]').prop('checked', true);
                            }else if(3 == data.dadosEmpresa.ramo.colaboradores_numero_id){
                                $radioColaboradores.filter('[value=3]').prop('checked', true);
                            }else if(4 == data.dadosEmpresa.ramo.colaboradores_numero_id){
                                $radioColaboradores.filter('[value=4]').prop('checked', true);
                            }else if(5 == data.dadosEmpresa.ramo.colaboradores_numero_id){
                                $radioColaboradores.filter('[value=5]').prop('checked', true);
                            }

                            var $radioConsumo = $('input:radio[name=consumo_mensal]');
                            if(1 === data.dadosEmpresa.ramo.energia_consumo_id) {
                                $radioConsumo.filter('[value=1]').prop('checked', true);
                            }else if(2 == data.dadosEmpresa.ramo.energia_consumo_id){
                                $radioConsumo.filter('[value=2]').prop('checked', true);
                            }else if(3 == data.dadosEmpresa.ramo.energia_consumo_id){
                                $radioConsumo.filter('[value=3]').prop('checked', true);
                            }else if(4 == data.dadosEmpresa.ramo.energia_consumo_id){
                                $radioConsumo.filter('[value=4]').prop('checked', true);
                            }else if(5 == data.dadosEmpresa.ramo.energia_consumo_id){
                                $radioConsumo.filter('[value=5]').prop('checked', true);
                            }

                            var $radioEstabelecimento = $('input:radio[name=estabelecimento_tipo]');
                            if(1 === data.dadosEmpresa.ramo.estabelecimento_tipo_id) {
                                $radioEstabelecimento.filter('[value=1]').prop('checked', true);
                            }else if(2 == data.dadosEmpresa.ramo.estabelecimento_tipo_id){
                                $radioEstabelecimento.filter('[value=2]').prop('checked', true);
                            }else if(3 == data.dadosEmpresa.ramo.estabelecimento_tipo_id){
                                $radioEstabelecimento.filter('[value=3]').prop('checked', true);
                            }else if(4 == data.dadosEmpresa.ramo.estabelecimento_tipo_id){
                                $radioEstabelecimento.filter('[value=4]').prop('checked', true);
                            }else if(5 == data.dadosEmpresa.ramo.estabelecimento_tipo_id){
                                $radioEstabelecimento.filter('[value=5]').prop('checked', true);
                            }

                            $areaTotal.val(data.dadosEmpresa.ramo.area_total);
                            $areaConstruida.val(data.dadosEmpresa.ramo.area_construida);

                            $ramoAtividadeOption.each(function() {

                                $('#ramo_atividade').removeAttr("selected");
                                if (data.dadosEmpresa.ramo.ramo_atividade_id) {
                                    if ($('#ramo_atividade').val() == data.dadosEmpresa.ramo.ramo_atividade_id) {
                                        
                                        $('#ramo_atividade').attr("selected", "selected");                                    
                                        $('#ramo_atividade').trigger("change");                                    
                                    }
                                    montarTipoAtividade(data.dadosEmpresa.ramo.tipo_ramo_atividade_id);
                                }
                            });
                            // $ramoAtividadeOption = $("#ramo_atividade option:selected").val();

                    
                        } else {

                            // Se não houver cadastro na base, vai consumir apenas o retorno da API da receita
                            
                            $razaoSocial.val(data.obj.nome);
                            $nomeFantasia.val(data.obj.fantasia);
                            $licencaMunicipal.val("");
                            $inscricaoEstadual.val("");
                            $telefone.val(data.obj.telefone);
                            $endereco.val(data.obj.logradouro);
                            $numero.val(data.obj.numero);
                            $bairro.val(data.obj.bairro);
                            $cep.val(data.obj.cep);
                            $complemento.val("");
                            $pontoReferencia.val("");
                            $nomeResponsavel.val("");
                            $cargo.val("");
                            $telefoneResponsavel.val("");
                            $celular.val("");

                            estadoNome = data.obj.uf;
                            cidadeNome = data.obj.municipio;


                            $('.containerPrimeiraEtapaContinuacao').show();

                        }                        
                        if (estadoNome == null) {
                            var estadoVazio = "<option selected disabled>Selecione um estado</option>"
                            $(".estados").append(estadoVazio);
                        }else {
                            $estado.each(function () {

                                $(this).removeAttr("selected");

                                if (estadoId) {
                                    if ($(this).val() === estadoId) {
                                        $(this).attr("selected", "selected");
                                        $(this).trigger("change");
                                    }
                                } else {
                                    if ($(this).text() === estadoNome) {
                                        $(this).attr("selected", "selected");
                                        $(this).trigger("change");
                                    }
                                }
                            });
                        }

                        $estado = $("#estado option:selected").val();
                        
                        //$btnCnpjContinua.prop('disabled', false);

                        cepTriggered = true;                        
                        montarCidades($estado, cidadeNome, cidadeId);
                        //montarCidades(estadoId, cidadeNome, cidadeId);
                        

                    } else if (data.error = "cnpj_existente") {
                        $('#modalCnpjExistente').modal({backdrop: "static", keyboard: false});
                        $('#modalCnpjExistente').modal("show");
                    }
                }
            });
        } else {
            $(this).parent().find('.text-danger').text("");
            $(this).after('<p class="text-danger">CNPJ Inválido. Tente Novamente!</p>');
            $(this).focus();            
        }
    });

}

function vincularEmpresaPorCnpj() {
    $('#cnpj_vincular').focusout(function () {

        let $cnpjValido = valida_cnpj($(this).val());

        if ($cnpjValido) {

            $(this).parent().find('.text-danger').text("");

            let $cnpj = $(this).val().replace(/[^\d]+/g, ''),
                url = '/api/cnpj-empresa/' + $cnpj,
                $razaoSocial = $('#razao_social'),
                $nomeFantasia = $('#nome_fantasia'),
                $cep = $('#cep'),
                $estado = $('#estado option'),
                $cidade = $('#cidade option'),
                $endereco = $('#endereco'),
                $numero = $('#numero'),
                $bairro = $('#bairro'),
                estadoId = 0,
                estadoNome = '',
                cidadeId = 0,
                cidadeNome = '';

            $.ajax({
                url: url,
                beforeSend: function () {
                    loading(1);
                },
                success: function (data) {

                    loading(0);

                    if ($.isEmptyObject(data.error)) {

                        // Se houver cadastro na base, traz todas essas informações
                        if (data.dadosEmpresa) {
                            $razaoSocial.val(data.dadosEmpresa.inf.razao_social).attr('readonly', true);
                            $nomeFantasia.val(data.dadosEmpresa.inf.nome_comercial).attr('readonly', true);

                            // pega dados de endereço se houverem
                            if (data.dadosEmpresa.endereco) {
                                $cep.val(data.dadosEmpresa.endereco.cep).attr('readonly', true);
                                $endereco.val(data.dadosEmpresa.endereco.endereco).attr('readonly', true);
                                $numero.val(data.dadosEmpresa.endereco.numero).attr('readonly', true);
                                $bairro.val(data.dadosEmpresa.endereco.bairro).attr('readonly', true);

                                $estado.attr('readonly', true);
                                $cidade.attr('readonly', true);

                                estadoId = data.dadosEmpresa.endereco.estado_id;
                                cidadeId = data.dadosEmpresa.endereco.cidade_id;
                            }

                        } else {

                            // Se não houver cadastro na base, vai consumir apenas o retorno da API da receita
                            $razaoSocial.val(data.obj.nome).removeAttr('readonly');
                            $nomeFantasia.val(data.obj.fantasia).removeAttr('readonly');
                            $cep.val(data.obj.cep).removeAttr('readonly');
                            $endereco.val(data.obj.logradouro).removeAttr('readonly');
                            $numero.val(data.obj.numero).removeAttr('readonly');
                            $bairro.val(data.obj.bairro).removeAttr('readonly');

                            $estado.attr('readonly', false);
                            $cidade.attr('readonly', false);

                            estadoNome = data.obj.uf;
                            cidadeNome = data.obj.municipio;
                        }

                        cepTriggered = false;
                        $estado.each(function () {

                            $(this).removeAttr("selected");

                            if (estadoId) {

                                if ($(this).val() === estadoId) {

                                    $(this).attr("selected", "selected");
                                    $(this).trigger("change");
                                }
                            } else if (estadoNome) {

                                if ($(this).text() === estadoNome) {
                                    $(this).attr("selected", "selected");
                                    $(this).trigger("change");
                                }
                            }

                        });

                        $estado = $("#estado option:selected").val();

                        cepTriggered = false;
                        montarCidades($estado, cidadeNome, cidadeId);

                    } else if (data.error = 'cnpj_existente') {
                        $("#modalCnpjExistente").modal({backdrop: 'static', keyboard: false});
                        $("#modalCnpjExistente").modal('show');
                    }
                }
            });
        } else {
            $(this).parent().find(".text-danger").text("");
            $(this).after("<p class=\"text-danger\">CNPJ Inválido. Tente Novamente!</p>");
            $(this).focus();
        }

    });
}
