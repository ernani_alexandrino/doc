$(function () {

    // verificar listagens a serem carregadas na pagina
    if (window.location.href.includes('painel/transportador/clientes-fornecedores') || window.location.href.includes('painel/condominio-misto/clientes-fornecedores')) {
        atualizaListaVinculosGeradorTransportador();
    }

    if (window.location.href.endsWith('clientes-fornecedores')) {
        // vinculos com transportadores
        atualizaListaVinculosTransportador();
        vinculosTransportador();

        // vinculos com Cooperativas
        if (!window.location.href.includes('cooperativas')) {
            vinculosCooperativas();
            atualizaListaVinculosCooperativas();
        }

        // vinculos com destinos finais
        if (!window.location.href.includes('destino-final')) {
            vinculosDestino();
            atualizaListaVinculosDestino();
        }

        atualizaListaVinculosRemovidos();
        remocaoVinculos();
    }

});

let vinculosCooperativas = () => {
    let id_cooperativa = '';

    const resetarFormCooperativa = () => {
        $('#frmVincularCooperativa')[0].reset();
        $('#coleta_diaria').val('').trigger('change');
        $('#equipamentos_vinculo').val('').trigger('change');
        $('#residuos_vinculo').val('').trigger('change');
        $('#equipamentos-escolhidos').empty();
        $('#residuos-escolhidos').empty();
        $('#razao_social_cooperativa').attr('readonly', false);
        $('#nome_fantasia_cooperativa').attr('readonly', false);
        $('#volume_residuos').empty();
    };

    let apontarErrosFormCooperativa = (data) => {
        $('#razaosocial-cooperativa p.text-danger').empty();
        if ('razao_social' in data.error) {
            $('#razaosocial-cooperativa p.text-danger').append(data.error['razao_social']);
        }

        $('#nomefantasia-cooperativa p.text-danger').empty();
        if ('nome_fantasia' in data.error) {
            $('#nomefantasia-cooperativa p.text-danger').append(data.error['nome_fantasia']);
        }

        $('#responsavel-cooperativa p.text-danger').empty();
        if ('responsavel' in data.error) {
            $('#responsavel-cooperativa p.text-danger').append(data.error['responsavel']);
        }

        $('#email-cooperativa p.text-danger').empty();
        if ('email' in data.error) {
            $('#email-cooperativa p.text-danger').append(data.error['email']);
        }

        $('#cnpj-cooperativa p.text-danger').empty();
        if ('cnpj' in data.error) {
            $('#cnpj-transportador p.text-danger').append(data.error['cnpj']);
        }

        $('#warnings-cooperativa p.text-warning').empty();
        if ('warning' in data.error) {
            $('#warnings-cooperativa p.text-warning').append(data.error['warning']);
            resetarFormCooperativa();
        }

    };

    //mostra a modal de Cooperativa
    $('#btn_add_cooperativa').click(function () {
        $('#modalNewVinculoCooperativa').modal('show');
        $('#vinculo-success').empty().parent().parent().hide();
    });

    // reseta modal de Cooperativa
    $('#btn-reset-new-cooperativa').click(function () {
        $('#modalNewVinculoCooperativa').modal('hide');
        apontarErrosFormCooperativa({'error': {}});
        resetarFormCooperativa();
    });

    // mostra modal de editar Cooperativa
    $('#cooperativa-list').on('click', 'tr', function () {
        id_cooperativa = $(this).attr('id');
        let url = '/painel/vinculos/' + id_cooperativa;

        $.ajax({
            url: url,
            success: function (modal) {

                $('#modalEditCooperativa').empty().append(modal.html);
                $('#modalEditCooperativa').modal('show');

            }
        });
    });

    // Editar cooperativa
    $('#modalEditCooperativa').on('click', '#btn-save-edit-cooperativa', function (e) {
        e.preventDefault();

        let formDataCooperativa = {
            _token: $("#frmEditVincularCooperativa input[name='_token']").val(),
            responsavel: $("#frmEditVincularCooperativa #responsavel_cooperativa").val(),
            email: $("#frmEditVincularCooperativa #email_cooperativa").val(),
            telefone: $("#frmEditVincularCooperativa #telefone_cooperativa").val(),
            coleta_diaria: $("#frmEditVincularCooperativa #coleta_diaria").val(),
            equipamentos_vinculo: $('#frmEditVincularCooperativa input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            residuos_vinculo: $('#frmEditVincularCooperativa input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            volume_residuos: $('#frmEditVincularCooperativa #volume_residuos').val()
        };

        let url = '/painel/vinculos/' + id_cooperativa;

        $.ajax({
            type: 'PATCH',
            headers: getAjaxHeaders(),
            url: url,
            data: formDataCooperativa,

            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    $('#vinculo-success').empty().append(data['success']).parent().parent().show();
                    $("#table-cooperativas-vinculados").DataTable().ajax.reload();
                    $('#modalEditCooperativa').modal('hide');

                }

                apontarErrosFormCooperativa(data);
            }

        })

    });

    // fecha modal de editar Cooperativa
    $('#modalEditCooperativa').on('click', '#btn-reset-edit-cooperativa', function () {
        $('#modalEditCooperativa').modal('hide');
        $('#modalEditCooperativa').empty();
    });

    // salvar vinculo com Cooperativa
    $('#btn-save-new-cooperativa').click(function (e) {
        e.preventDefault();
        let formId = "#" + $(this).closest("form").attr('id');

        let formDataCooperativa = {
            _token: $("input[name='_token']").val(),
            tipo_empresa: 'cooperativa',
            cnpj: $("#cnpj_vincular_cooperativa").val(),
            razao_social: $("#razao_social_cooperativa").val(),
            nome_fantasia: $("#nome_fantasia_cooperativa").val(),
            responsavel: $("#responsavel_cooperativa").val(),
            email: $("#email_cooperativa").val(),
            telefone: $("#telefone_cooperativa").val(),
            coleta_diaria: $("#coleta_diaria").val(),
            equipamentos_vinculo: $(formId + ' input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            residuos_vinculo: $(formId + ' input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            volume_residuos: $('#volume_residuos').val()
        };

        let url = '/painel/vincular-cooperativa';

        $.ajax({
            type: 'POST',
            headers: getAjaxHeaders(),
            url: url,
            data: formDataCooperativa,

            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    // resseta formulario, informa mensagem de sucesso
                    // e atualiza listagem
                    $('#vinculo-success').append(data['success']).parent().parent().show();
                    $('#modalNewVinculoCooperativa').modal('hide');

                    $("#table-cooperativas-vinculados").DataTable().ajax.reload();
                    resetarFormCooperativa();

                }

                apontarErrosFormCooperativa(data);

            }

        })

    });

    // traz dados sobre cooperativa para popular form
    $('#cnpj_vincular_cooperativa').focusout(function () {

        if (valida_cnpj($(this).val())) {

            let $cnpj = $(this).val().replace(/[^\d]+/g, '');
            let url = '/api/cnpj-empresa/' + $cnpj;
            let razao_social = '', nome_fantasia = '';

            $.ajax({
                url: url,
                beforeSend: function () {
                    loadingAjax(1);
                    apontarErrosFormCooperativa({'error': {}});
                },
                success: function (data) {

                    loadingAjax(0);

                    // verifica fonte dos dados da empresa
                    if (data.dadosEmpresa) {
                        razao_social = data.dadosEmpresa.inf.razao_social;
                        nome_fantasia = data.dadosEmpresa.inf.nome_comercial;
                    } else {
                        razao_social = data.obj.nome;
                        nome_fantasia = data.obj.fantasia;
                    }

                    if (razao_social.length > 0) {
                        $('#razao_social_cooperativa').val(razao_social).attr('readonly', true);
                    }
                    if (nome_fantasia.length > 0) {
                        $('#nome_fantasia_cooperativa').val(nome_fantasia).attr('readonly', true);
                    }

                }

            })

        } else {
            apontarErrosFormCooperativa({'error': {'cnpj': 'CNPJ Inválido. Tente Novamente!'}});
        }

    });

};

let vinculosTransportador = () => {

    let id_transportador = '';

    const resetarFormTransportadorGerador = () => {
        $('#frmVincularTransportador')[0].reset();
        $('#coleta_diaria').val('').trigger('change');
        $('#equipamentos_vinculo').val('').trigger('change');
        $('#residuos_vinculo').val('').trigger('change');
        $('#equipamentos-escolhidos').empty();
        $('#residuos-escolhidos').empty();
        $('#razao_social_transportador').attr('readonly', false);
        $('#nome_fantasia_transportador').attr('readonly', false);
        $('#volume_residuos').empty();
    };

    let apontarErrosFormTransportadorGerador = (data) => {
        $('#razaosocial-transportador p.text-danger').empty();
        if ('razao_social' in data.error) {
            $('#razaosocial-transportador p.text-danger').append(data.error['razao_social']);
        }

        $('#nomefantasia-transportador p.text-danger').empty();
        if ('nome_fantasia' in data.error) {
            $('#nomefantasia-transportador p.text-danger').append(data.error['nome_fantasia']);
        }

        $('#responsavel-transportador p.text-danger').empty();
        if ('responsavel' in data.error) {
            $('#responsavel-transportador p.text-danger').append(data.error['responsavel']);
        }

        $('#email-transportador p.text-danger').empty();
        if ('email' in data.error) {
            $('#email-transportador p.text-danger').append(data.error['email']);
        }

        $('#cnpj-transportador p.text-danger').empty();
        if ('cnpj' in data.error) {
            $('#cnpj-transportador p.text-danger').append(data.error['cnpj']);
        }

        $('#warnings-transportador p.text-warning').empty();
        if ('warning' in data.error) {
            $('#warnings-transportador p.text-warning').append(data.error['warning']);
            resetarFormTransportadorGerador();
        }

    };

    let apontarErrosFormEditTransportadorGerador = (data) => {
        $('#frmEditVincularTransportador #responsavel-transportador p.text-danger').empty();
        if ('responsavel' in data.error) {
            $('#frmEditVincularTransportador #responsavel-transportador p.text-danger').append(data.error['responsavel']);
        }

        $('#frmEditVincularTransportador #email-transportador p.text-danger').empty();
        if ('email' in data.error) {
            $('#frmEditVincularTransportador #email-transportador p.text-danger').append(data.error['email']);
        }
    };

    // mostra o modal de Transportadores
    $('#btn_add_transportador').click(function () {
        $('#modalNewVinculoTransportador').modal('show');
        $('#vinculo-success').empty().parent().parent().hide();
    });

    // salvar vinculo com transportador
    $('#btn-save-new-transportador').click(function (e) {
        e.preventDefault();
        let formId = "#" + $(this).closest("form").attr('id');

        let formDataGerador = {
            _token: $("input[name='_token']").val(),
            cnpj: $("#cnpj_vincular_transportador").val(),
            razao_social: $("#razao_social_transportador").val(),
            nome_fantasia: $("#nome_fantasia_transportador").val(),
            responsavel: $("#responsavel_transportador").val(),
            email: $("#email_transportador").val(),
            telefone: $("#telefone_transportador").val(),
            coleta_diaria: $("#coleta_diaria").val(),
            equipamentos_vinculo: $(formId + ' input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            residuos_vinculo: $(formId + ' input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            volume_residuos: $('#volume_residuos').val()
        };

        let url = '/painel/vincular-transportador';

        $.ajax({
            type: 'POST',
            headers: getAjaxHeaders(),
            url: url,
            data: formDataGerador,

            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    // resseta formulario, informa mensagem de sucesso
                    // e atualiza listagem
                    $('#vinculo-success').append(data['success']).parent().parent().show();
                    $('#modalNewVinculoTransportador').modal('hide');

                    $("#table-transportadores-vinculados").DataTable().ajax.reload();
                    resetarFormTransportadorGerador();

                }

                apontarErrosFormTransportadorGerador(data);

            }

        })

    });

    // reseta modal de Geradores
    $('#btn-reset-new-transportador').click(function () {
        $('#modalNewVinculoTransportador').modal('hide');
        apontarErrosFormTransportadorGerador({'error': {}});
        resetarFormTransportadorGerador();
    });

    // mostra modal de editar Transportador
    $('#transportador-list').on('click', 'tr', function () {
        id_transportador = $(this).attr('id');
        let url = '/painel/vinculos/' + id_transportador;

        $.ajax({
            url: url,
            success: function (modal) {

                $('#modalEditTransportador').empty().append(modal.html);
                $('#modalEditTransportador').modal('show');

            }
        });
    });

    // salvar vinculo com transportador
    $('#modalEditTransportador').on('click', '#btn-save-edit-transportador', function (e) {
        e.preventDefault();

        let formDataTransportador = {
            _token: $("#frmEditVincularTransportador input[name='_token']").val(),
            responsavel: $("#frmEditVincularTransportador #responsavel_transportador").val(),
            email: $("#frmEditVincularTransportador #email_transportador").val(),
            telefone: $("#frmEditVincularTransportador #telefone_transportador").val(),
            coleta_diaria: $("#frmEditVincularTransportador #coleta_diaria").val(),
            equipamentos_vinculo: $('#frmEditVincularTransportador input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            residuos_vinculo: $('#frmEditVincularTransportador input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            volume_residuos: $('#frmEditVincularTransportador #volume_residuos').val()
        };

        let url = '/painel/vinculos/' + id_transportador;

        $.ajax({
            type: 'PATCH',
            headers: getAjaxHeaders(),
            url: url,
            data: formDataTransportador,

            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    $('#vinculo-success').empty().append(data['success']).parent().parent().show();
                    $("#table-transportadores-vinculados").DataTable().ajax.reload();
                    $('#modalEditTransportador').modal('hide');

                }

                apontarErrosFormEditTransportadorGerador(data);
            }

        })

    });

    // fecha modal de editar Transportadores
    $('#modalEditTransportador').on('click', '#btn-reset-edit-transportador', function () {
        $('#modalEditTransportador').modal('hide');
        $('#modalEditTransportador').empty();
    });

    // traz dados sobre transportador para popular form
    $('#cnpj_vincular_transportador').focusout(function () {

        if (valida_cnpj($(this).val())) {

            let $cnpj = $(this).val().replace(/[^\d]+/g, '');
            let url = '/api/cnpj-empresa/' + $cnpj;
            let razao_social = '', nome_fantasia = '';

            $.ajax({
                url: url,
                beforeSend: function () {
                    loadingAjax(1);
                    apontarErrosFormTransportadorGerador({'error': {}});
                },
                success: function (data) {

                    loadingAjax(0);

                    // verifica fonte dos dados da empresa
                    if (data.dadosEmpresa) {
                        razao_social = data.dadosEmpresa.inf.razao_social;
                        nome_fantasia = data.dadosEmpresa.inf.nome_comercial;
                    } else {
                        razao_social = data.obj.nome;
                        nome_fantasia = data.obj.fantasia;
                    }

                    if (razao_social.length > 0) {
                        $('#razao_social_transportador').val(razao_social).attr('readonly', true);
                    }
                    if (nome_fantasia.length > 0) {
                        $('#nome_fantasia_transportador').val(nome_fantasia).attr('readonly', true);
                    }

                }

            })

        } else {
            apontarErrosFormTransportadorGerador({'error': {'cnpj': 'CNPJ Inválido. Tente Novamente!'}});
        }

    });

};

let vinculosDestino = () => {

    let id_destino = '';

    const resetarFormDestino = () => {
        $('#frmVincularDestino')[0].reset();
        $('#frmVincularDestino #residuos_vinculo').val('').trigger('change');
        $('#frmVincularDestino #residuos-escolhidos').empty();
        $('#razao_social_destino').attr('readonly', false);
        $('#nome_fantasia_destino').attr('readonly', false);
    };

    let apontarErrosFormDestino = (data) => {

        $('#razaosocial-destino p.text-danger').empty();
        if ('razao_social' in data.error) {
            $('#razaosocial-destino p.text-danger').append(data.error['razao_social']);
        }

        $('#nomefantasia-destino p.text-danger').empty();
        if ('nome_fantasia' in data.error) {
            $('#nomefantasia-destino p.text-danger').append(data.error['nome_fantasia']);
        }

        $('#responsavel-destino p.text-danger').empty();
        if ('responsavel' in data.error) {
            $('#responsavel-destino p.text-danger').append(data.error['responsavel']);
        }

        $('#email-destino p.text-danger').empty();
        if ('email' in data.error) {
            $('#email-destino p.text-danger').append(data.error['email']);
        }

        $('#cnpj-destino p.text-danger').empty();
        if ('cnpj' in data.error) {
            $('#cnpj-destino p.text-danger').append(data.error['cnpj']);
        }

        $('#warnings-destino p.text-warning').empty();
        if ('warning' in data.error) {
            $('#warnings-destino p.text-danger').append(data.error['warning']);
            resetarFormDestino();
        }

    };

    let apontarErrosFormEditDestino = (data) => {

        $('#frmEditVincularDestino #responsavel-destino p.text-danger').empty();
        if ('responsavel' in data.error) {
            $('#frmEditVincularDestino #responsavel-destino p.text-danger').append(data.error['responsavel']);
        }

        $('#frmEditVincularDestino #email-destino p.text-danger').empty();
        if ('email' in data.error) {
            $('#frmEditVincularDestino #email-destino p.text-danger').append(data.error['email']);
        }
    };

    // mostra o modal de Destinos Finais
    $('#btn_add_destino').click(function () {
        $("#modalNewVinculoDestino").modal("show");
        $("#vinculo-success").empty().parent().parent().hide();
    });

    // mostra modal de editar Destino Final
    $("#destino-list").on("click", "tr", function () {

        id_destino = $(this).attr('id');

        $.ajax({
            url: "/painel/vinculos/" + id_destino,
            success: function (modal) {
                $("#modalEditDestino").empty().append(modal.html);
                $("#modalEditDestino").modal("show");
            }
        });
    });

    // salvar vinculo com destino final
    $('#modalEditDestino').on('click', '#btn-save-edit-destino', function (e) {
        e.preventDefault();

        let formDataDestino = {
            _token: $("#frmEditVincularDestino input[name='_token']").val(),
            responsavel: $("#frmEditVincularDestino #responsavel_destino").val(),
            email: $("#frmEditVincularDestino #email_destino").val(),
            telefone: $("#frmEditVincularDestino #telefone_destino").val(),
            volume_residuos: $("#frmEditVincularDestino #volume_residuos").val(),
            residuos_vinculo: $('#frmEditVincularDestino input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join()
        };

        let url = "/painel/vinculos/" + id_destino;

        $.ajax({
            url: url,
            type: 'PATCH',
            headers: getAjaxHeaders(),
            data: formDataDestino,
            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    $('#vinculo-success').empty().append(data['success']).parent().parent().show();
                    $("#table-destinos-vinculados").DataTable().ajax.reload();
                    $('#modalEditDestino').modal('hide');
                    $(document).scrollTop(0);

                }

                apontarErrosFormEditDestino(data);
            }

        })

    });

    // reseta modal de Destino
    $('#btn-reset-new-destino').click(function () {
        $('#modalNewVinculoDestino').modal("hide");
        apontarErrosFormDestino({"error": {}});
        resetarFormDestino();
    });

    // salvar vinculo com destino
    $('#btn-save-new-destino').click(function (e) {
        e.preventDefault();
        let formId = "#" + $(this).closest("form").attr('id');

        let formDataDestino = {
            _token: $("input[name='_token']").val(),
            tipo_empresa: 'destino_final',
            cnpj: $("#cnpj_vincular_destino").val(),
            razao_social: $("#razao_social_destino").val(),
            nome_fantasia: $("#nome_fantasia_destino").val(),
            responsavel: $("#responsavel_destino").val(),
            email: $("#email_destino").val(),
            telefone: $("#telefone_destino").val(),
            residuos_vinculo: $(formId + ' input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            volume_residuos: $('#volume_residuos').val()
        };

        let url = '/painel/vincular-destino-final';

        $.ajax({
            type: 'POST',
            headers: getAjaxHeaders(),
            url: url,
            data: formDataDestino,
            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    // resseta formulario, informa mensagem de sucesso
                    // e atualiza listagem
                    $('#vinculo-success').append(data["success"]).parent().parent().show();
                    $('#modalNewVinculoDestino').modal("hide");
                    $(document).scrollTop(0); // vai para topo da pagina para mostrar mensagem de sucesso

                    $("#table-destinos-vinculados").DataTable().ajax.reload();
                    resetarFormDestino();

                }

                apontarErrosFormDestino(data);

            }

        })

    });

    // fecha modal de editar Destinos
    $("#modalEditDestino").on("click", "#btn-reset-edit-destino", function () {
        $('#modalEditDestino').modal('hide');
        $('#modalEditDestino').empty();
    });

    // traz dados sobre destino para popular form
    $('#cnpj_vincular_destino').focusout(function () {

        if (valida_cnpj($(this).val())) {

            let $cnpj = $(this).val().replace(/[^\d]+/g, '');
            let razao_social = '', nome_fantasia = '';

            $.ajax({
                url: "/api/cnpj-empresa/" + $cnpj,
                beforeSend: function () {
                    loadingAjax(1);
                    apontarErrosFormDestino({'error': {}});
                },
                success: function (data) {

                    loadingAjax(0);

                    // verifica fonte dos dados da empresa
                    if (data.dadosEmpresa) {
                        razao_social = data.dadosEmpresa.inf.razao_social;
                        nome_fantasia = data.dadosEmpresa.inf.nome_comercial;
                    } else {
                        razao_social = data.obj.nome;
                        nome_fantasia = data.obj.fantasia;
                    }

                    if (razao_social.length > 0) {
                        $("#razao_social_destino").val(razao_social).attr("readonly", true);
                    }

                    if (nome_fantasia.length > 0) {
                        $("#nome_fantasia_destino").val(nome_fantasia).attr("readonly", true);
                    }

                }

            })

        } else {
            apontarErrosFormDestino({'error': {'cnpj': 'CNPJ Inválido. Tente Novamente!'}});
        }

    })

};

let remocaoVinculos = () => {

    let id_vinculo = "";
    let id_tbody = ""; // controle da tabela que recebeu o click

    // mostra modal de deletar vinculos
    $('.vinculos-list').on('click', '.delete-vinculo', function (e) {
        e.stopPropagation(); // protege evento de vazar para elementos filhos
        id_vinculo = $(this).val();
        id_tbody = $(this).closest("tbody").attr("id");
        empresa_vinculada_id = $(this).closest("tr").attr("id");
        $('#modalDesvincularEmpresa').modal('show');
    });

    // deletar vinculos entre empresas
    $("#btn-delete-vinculo").click(() => {

        let justificativa = $("#justificativa-remocao-vinculo").val().trim();

        if (allEqual(justificativa)) {
            $('#warnings-remocao-vinculo p.text-warning').empty().append("O sistema rejeitou seu texto.");
            return;
        }
        if (justificativa.length < 30) {
            $('#warnings-remocao-vinculo p.text-warning').empty().append("O sistema rejeitou seu texto.");
            return;
        }

        $.ajax({
            url: "/painel/vinculos/" + empresa_vinculada_id,
            type: 'DELETE',
            headers: getAjaxHeaders(),
            data: {justificativa: justificativa},
            success: (data) => {
                $("#vinculo-success").empty().append(data['success']).parent().parent().show();
                $("#justificativa-remocao-vinculo").val('');

                // verifica qual listagem deve ser atualizada
                if (id_tbody == "transportador-list") {
                    $("#table-transportadores-vinculados").DataTable().ajax.reload();
                }
                if (id_tbody == "gerador-list") {
                    $("#table-geradores-vinculados").DataTable().ajax.reload();
                }
                if (id_tbody == "destino-list") {
                    $("#table-destinos-vinculados").DataTable().ajax.reload();
                }

                $('#modalDesvincularEmpresa').modal('hide');
                $("#table-vinculos-removidos").DataTable().ajax.reload();
                $(document).scrollTop(0); // vai para topo da pagina para mostrar mensagem de sucesso

                if (id_tbody == 'vinculo-condominio') {
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        });
    });

    $("#btn-cancel-delete-vinculo").click(() => {
        $("#modalDesvincularEmpresa").modal("hide");
    });

    $("#vinculos-removidos-list").on("click", ".btn-restore", function(e) {
        e.stopPropagation(); // protege evento de vazar para elementos filhos
        id_vinculo = $(this).val();
        $("#modalRestaurarVinculo").modal("show");
    });

    // restaura um vinculo deletado
    $("#btn-restore-vinculo").click(() => {
        $.ajax({
            url: "/painel/vinculo-restaurar/" + id_vinculo,
            type: 'PATCH',
            headers: getAjaxHeaders(),
            success: (data) => {
                if (data['warning']) {
                    $("#vinculo-success").empty().append(data['warning']).parent().parent().show();
                }
                if (data['success']) {
                    $("#vinculo-success").empty().append(data['success']).parent().parent().show();
                }

                // verifica qual listagem de vinculos tem que ser atualizada
                if (data['empresa'] == 'Grande Gerador') {
                    $("#table-geradores-vinculados").DataTable().ajax.reload();
                }
                if (data['empresa'] == 'Transportador') {
                    $("#table-transportadores-vinculados").DataTable().ajax.reload();
                }
                if (data['empresa'] == 'Destino Final') {
                    $("#table-destinos-vinculados").DataTable().ajax.reload();
                }

                $("#table-vinculos-removidos").DataTable().ajax.reload();
                $('#modalRestaurarVinculo').modal('hide');
                $(document).scrollTop(0); // vai para topo da pagina para mostrar mensagem de sucesso

                if (data['empresa'] == 'Condomínio Misto') {
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                }
            }
        });
    });

    $("#btn-cancel-restore-vinculo").click(() => {
        $("#modalRestaurarVinculo").modal("hide");
    });

};

let atualizaListaVinculosGeradorTransportador = () => {

    let msg = "Não há Geradores vinculados à sua empresa ainda. Clique no botão abaixo para adicioná-los.";

    $("#table-geradores-vinculados").DataTable({
        "language": datatableLanguageParams(msg),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/geradores-vinculados-list",
        columns: [
            {data: 'id_limpurb', name: 'id_limpurb'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'total_equipamentos', name: 'total_equipamentos'},
            {data: 'total_residuos', name: 'total_residuos'},
            {data: 'frequencia_coleta', name: 'frequencia_coleta'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'}
        ]
    });

};

let atualizaListaVinculosDestino = () => {

    let msg = "Não há Destinos Finais vinculados à sua empresa ainda. Clique no botão abaixo para adicioná-los.";

    $("#table-destinos-vinculados").DataTable({
        "language": datatableLanguageParams(msg),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/destinos-vinculados-list",
        columns: [
            {data: 'id_limpurb', name: 'id_limpurb'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'total_residuos', name: 'total_residuos'},
            {data: 'volume_residuos', name: 'volume_residuos'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'}
        ]
    });

};

let atualizaListaVinculosCooperativas = () => {

    let msg = "Não há Cooperativas vinculadas à sua empresa ainda. Clique no botão abaixo para adicioná-los.";

    let table = $("#table-cooperativas-vinculados");

    let columns = [
        {data: 'id_limpurb', name: 'id_limpurb'},
        {data: 'razao_social', name: 'razao_social'},
        {data: 'total_residuos', name: 'total_residuos'}
    ];

    // geradores
    if (!window.location.href.includes('destino-final')
        && !window.location.href.includes('transportador')) {

        columns.push({data: 'total_equipamentos', name: 'total_equipamentos'});
        columns.push({data: 'frequencia_coleta', name: 'frequencia_coleta'});
    }

    columns.push({data: 'status', name: 'status'});
    columns.push({data: 'acao', name: 'acao'});

    table.DataTable({
        "language": datatableLanguageParams(msg),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/cooperativas-vinculadas-list",
        columns: columns
    });

};

let atualizaListaVinculosTransportador = () => {

    let msg = "Não há Transportadores vinculados à sua empresa ainda. Clique no botão abaixo para adicioná-los.";
    let table = $("#table-transportadores-vinculados");

    let columns = [
        {data: 'id_limpurb', name: 'id_limpurb'},
        {data: 'razao_social', name: 'razao_social'},
        {data: 'total_residuos', name: 'total_residuos'}
    ];

    // geradores
    if (!window.location.href.includes('destino-final')
        && !window.location.href.includes('transportador')) {

        columns.push({data: 'total_equipamentos', name: 'total_equipamentos'});
        columns.push({data: 'frequencia_coleta', name: 'frequencia_coleta'});
    }

    columns.push({data: 'status', name: 'status'});
    columns.push({data: 'acao', name: 'acao'});

    table.DataTable({
        "language": datatableLanguageParams(msg),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/transportadores-vinculados-list",
        columns: columns
    });

};

let atualizaListaVinculosRemovidos = () => {

    let msg = "Não há nenhum vínculo removido";

    $("#table-vinculos-removidos").DataTable({
        "language": datatableLanguageParams(msg),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/vinculos-removidos-list",
        columns: [
            {data: 'id_limpurb', name: 'id_limpurb'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'status', name: 'status'},
            {data: 'restore', name: 'vinculo.restore'}
        ]
    });

};