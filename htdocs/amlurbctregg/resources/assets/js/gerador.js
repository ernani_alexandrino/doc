$(function () {

    vinculosCondominio();

});

function vinculosCondominio() {

    const resetarFormCondominio = () => {
        $('#frmVincularCondominio')[0].reset();
        $('#razao_social_condominio').attr('readonly', false);
        $('#nome_fantasia_condominio').attr('readonly', false);
    };

    let apontarErrosFormCondominio = (data) => {
        $('#razaosocial-condominio p.text-danger').empty();
        if ('razao_social' in data.error) {
            $('#razaosocial-condominio p.text-danger').append(data.error['razao_social']);
        }

        $('#nomefantasia-condominio p.text-danger').empty();
        if ('nome_fantasia' in data.error) {
            $('#nomefantasia-condominio p.text-danger').append(data.error['nome_fantasia']);
        }

        $('#responsavel-condominio p.text-danger').empty();
        if ('responsavel' in data.error) {
            $('#responsavel-condominio p.text-danger').append(data.error['responsavel']);
        }

        $('#email-condominio p.text-danger').empty();
        if ('email' in data.error) {
            $('#email-condominio p.text-danger').append(data.error['email']);
        }

        $('#cnpj-condominio p.text-danger').empty();
        if ('cnpj' in data.error) {
            $('#cnpj-condominio p.text-danger').append(data.error['cnpj']);
        }
    };

    // mostra o modal de Condominio Misto
    $('#btn_add_condominio').click(function () {
        $('#modalNewVinculoCondominio').modal('show');
        $('#vinculo-success').empty().parent().parent().hide();
    });

    // salvar vinculo com condominio
    $('#btn-save-new-condominio').click(function (e) {
        e.preventDefault();

        let formDataCondominio = {
            _token: $("input[name='_token']").val(),
            cnpj: $("#cnpj_vincular_condominio").val(),
            razao_social: $("#razao_social_condominio").val(),
            nome_fantasia: $("#nome_fantasia_condominio").val(),
            responsavel: $("#responsavel_condominio").val(),
            email: $("#email_condominio").val()
        };

        let url = '/painel/vincular-condominio';

        $.ajax({
            type: 'POST',
            headers: getAjaxHeaders(),
            url: url,
            data: formDataCondominio,

            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    // resseta formulario, informa mensagem de sucesso
                    // e atualiza listagem
                    $('#vinculo-success').append(data['success']).parent().parent().show();
                    $('#modalNewVinculoCondominio').modal('hide');

                    document.getElementById('vinculo-success').scrollIntoView();

                    setTimeout(function () {
                        location.reload();
                    }, 3000);

                    resetarFormCondominio();

                }

                apontarErrosFormCondominio(data);

            }

        })

    });

    // reseta modal de Condominio
    $('#btn-reset-new-condominio').click(function () {
        $('#modalNewVinculoCondominio').modal('hide');
        apontarErrosFormCondominio({'error': {}});
        resetarFormCondominio();
    });

    // traz dados sobre condominio para popular form
    $('#cnpj_vincular_condominio').focusout(function () {

        if (valida_cnpj($(this).val())) {

            let $cnpj = $(this).val().replace(/[^\d]+/g, '');
            let url = '/api/cnpj-empresa/' + $cnpj;
            let razao_social = '', nome_fantasia = '';

            $.ajax({
                url: url,
                beforeSend: function () {
                    loadingAjax(1);
                    apontarErrosFormCondominio({'error': {}});
                },
                success: function (data) {

                    loadingAjax(0);

                    // verifica fonte dos dados da empresa
                    if (data.dadosEmpresa) {
                        razao_social = data.dadosEmpresa.inf.razao_social;
                        nome_fantasia = data.dadosEmpresa.inf.nome_comercial;
                    } else {
                        razao_social = data.obj.nome;
                        nome_fantasia = data.obj.fantasia;
                    }

                    if (razao_social.length > 0) {
                        $('#razao_social_condominio').val(razao_social).attr('readonly', true);
                    }
                    if (nome_fantasia.length > 0) {
                        $('#nome_fantasia_condominio').val(nome_fantasia).attr('readonly', true);
                    }

                }

            })

        } else {
            apontarErrosFormCondominio({'error': {'cnpj': 'CNPJ Inválido. Tente Novamente!'}});
        }

    });

}
