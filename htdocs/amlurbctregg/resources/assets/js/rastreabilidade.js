$(function () {

    if (window.location.href.includes("rastreabilidade")) {
        dataTableRastreabilidadeFiscal();
    }

});

function dataTableRastreabilidadeFiscal() {

    let preencheTabelaRastreio = (ctre) => {

        let $containerAcondicionamento = $('.linha.containerAcondicionamento'),
            $containerAcondicionamentoQtde = $('.linha.containerAcondicionamentoQtde'),
            $containerPlacaVeiculo = $('.linha.containerPlacaVeiculo'),

            $boxRastreioGg = $(".box-rastreio-gg"),
            $boxRastreioTr = $(".box-rastreio-tr"),
            $boxRastreioDf = $(".box-rastreio-df"),
            $tempoCtre = $("#tempo"),
            $tempoTextoCtre = $("#tempo-texto"),
            $statusRastreabilidade = $("#status-rastreabilidade-fiscal"),
            tempoTexto = '',

            $imagemGg = $boxRastreioGg.find('.imagem'),
            $tituloGg = $boxRastreioGg.find('.titulo'),
            $nomeGg = $boxRastreioGg.find('.nome'),
            $nrAmlurbGg = $boxRastreioGg.find('.nr-amlurb'),
            $validacaoGg = $boxRastreioGg.find('.validacao'),
            $residuoGg = $boxRastreioGg.find('.residuo'),
            $acondicionamentoGg = $boxRastreioGg.find('.acondicionamento'),
            $horaGg = $boxRastreioGg.find('.hora-ctre'),
            $placaVeiculoGg = $boxRastreioGg.find('.placa-veiculo'),

            $imagemTr = $boxRastreioTr.find('.imagem'),
            $tituloTr = $boxRastreioTr.find('.titulo'),
            $nomeTr = $boxRastreioTr.find('.nome'),
            $nrAmlurbTr = $boxRastreioTr.find('.nr-amlurb'),
            $validacaoTr = $boxRastreioTr.find('.validacao'),
            $residuoTr = $boxRastreioTr.find('.residuo'),
            $acondicionamentoTr = $boxRastreioTr.find('.acondicionamento'),
            $horaTr = $boxRastreioTr.find('.hora-ctre'),
            $placaVeiculoTr = $boxRastreioTr.find('.placa-veiculo'),

            $imagemDf = $boxRastreioDf.find('.imagem'),
            $tituloDf = $boxRastreioDf.find('.titulo'),
            $nomeDf = $boxRastreioDf.find('.nome'),
            $nrAmlurbDf = $boxRastreioDf.find('.nr-amlurb'),
            $validacaoDf = $boxRastreioDf.find('.validacao'),
            $residuoDf = $boxRastreioDf.find('.residuo'),
            $acondicionamentoDf = $boxRastreioDf.find('.acondicionamento'),
            $horaDf = $boxRastreioDf.find('.hora-ctre'),
            $placaVeiculoDf = $boxRastreioDf.find('.placa-veiculo'),

            $classeInconsistencia = 'inconsistencia',

            equipamentosGg = '',
            equipamentosTr = '',
            equipamentosDf = '';

        $containerAcondicionamento.removeClass($classeInconsistencia);
        $containerAcondicionamentoQtde.removeClass($classeInconsistencia);
        $containerPlacaVeiculo.removeClass($classeInconsistencia);

        if (ctre.classTempo == 'expirado') {
            ctre.tempoCtre = "0h00min";
            tempoTexto = "Tempo Esgotado";
        } else if (ctre.classTempo == 'finalizado') {

            if (ctre.tempoCtre == 0) {
                ctre.tempoCtre = "0h00min";
            } else {
                ctre.tempoCtre = ctre.tempoCtre + " horas";
            }

            tempoTexto = "Finalizado";

        } else {
            ctre.tempoCtre = ctre.tempoCtre + " horas";
            tempoTexto = "Tempo Restante para Finalização";
        }

        $tempoCtre.text(ctre.tempoCtre);
        $tempoTextoCtre.text(tempoTexto);
        let $boxTempo = $tempoCtre.parents('.box-tempo');
        $boxTempo.removeClass().addClass('box-tempo ' + ctre.classTempo);
        $statusRastreabilidade.removeClass().addClass(ctre.classStatusCtre).text(ctre.textoStatusCtre);

        if (ctre.gerador != null) {

            $imagemGg.css('background-position', 'center left');
            $nomeGg.text(ctre.gerador.razao_social);
            $nrAmlurbGg.text(ctre.gerador.id_limpurb);

            $validacaoGg.removeClass();
            $validacaoGg.text(ctre.situacao).addClass(ctre.class);
            $residuoGg.text("");
            $acondicionamentoGg.text(equipamentosGg);
            $placaVeiculoGg.text(ctre.empresas_veiculos.placa);

            if (ctre.gerador.id == ctre.empresa.id) {
                $horaGg.text(ctre.hora_emissao);
            } else {
                $horaGg.text((ctre.hora_validacao != null) ? ctre.hora_validacao : 'Sem informações');
            }

        }

        if (ctre.transportador != null) {

            $imagemTr.css('background-position', 'center left');
            $nomeTr.text(ctre.transportador.razao_social);
            $nrAmlurbTr.text(ctre.transportador.id_limpurb);

            $validacaoTr.removeClass();
            $validacaoTr.text(ctre.situacao).addClass(ctre.class);
            $residuoTr.text("");
            $acondicionamentoTr.text(equipamentosTr);
            $placaVeiculoTr.text(ctre.empresas_veiculos.placa);

            if (ctre.transportador.id == ctre.empresa.id) {
                $horaTr.text(ctre.hora_emissao);
            } else {
                $horaTr.text((ctre.hora_validacao != null) ? ctre.hora_validacao : 'Sem informações');
            }

        } else {

            $imagemTr.css('background-position', 'center right');
            $nomeTr.text("Sem informações");
            $nrAmlurbTr.text("Sem informações");
            $validacaoTr.removeClass();
            $validacaoTr.text("Aguardando").addClass('validacao aguardando');
            $residuoTr.text("");
            $acondicionamentoTr.text("Sem informações");
            $horaTr.text("Sem informações");
            $placaVeiculoTr.text("Sem informações");

        }

        if (ctre.destino_final != null) {

            $imagemDf.css('background-position', 'center left');
            $nomeDf.text(ctre.destino_final.razao_social);
            $nrAmlurbDf.text(ctre.destino_final.id_limpurb);

            $validacaoDf.removeClass();
            $validacaoDf.text(ctre.destino_final.situacao).addClass(ctre.destino_final.class);
            $residuoDf.text("");
            $acondicionamentoDf.text(equipamentosDf);
            //$horaDf.text(ctre.destino_final.hora_validacao_finall);
            $placaVeiculoDf.text(ctre.empresas_veiculos.placa);

        } else {

            $imagemDf.css('background-position', 'center right');
            $nomeDf.text("Sem informações");
            $nrAmlurbDf.text("Sem informações");
            $validacaoDf.removeClass();
            $validacaoDf.text("Aguardando").addClass('validacao aguardando');
            $residuoDf.text("");
            $acondicionamentoDf.text("Sem informações");
            $horaDf.text("Sem informações");
            $placaVeiculoDf.text("Sem informações");

        }


        /*

         /// TODO POPULAR EQUIPAMENTOS


            */
    };

    let table = $('#rastreabilidade_fiscal').DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Nada foi encontrado!",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        ajax: "/painel/fiscal/rastreabilidade-ajax",
        processing: true,
        serverSide: true,
        bLengthChange: false,
        searching: true,
        ordering: false,
        columns: [
            {data: 'id', name: 'id'},
            {data: 'razaosocial', name: 'empresa.razao_social'},
            {data: 'empresa.id_limpurb', name: 'empresa.id_limpurb'},
            {data: 'alerta', name: 'alerta'},
            {data: 'rastreio', name: 'rastreio'},
            {data: 'status', name: 'status.descricao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-rastreio');
        }
    });

    table.on("click", ".linha-rastreio", function (event) {
        event.preventDefault();

        $(this).parent().find('.drop-ativo').removeClass('drop-ativo');
        $(this).addClass('drop-ativo');

        let ctreId = $(this).find('.ctre').data('id');
        let url = "/painel/fiscal/rastreabilidade-logs/";

        $.ajax({
            url: url + ctreId,
            success: function (data) {

                preencheTabelaRastreio(data.ctre);

                let $doc = $('html, body');

                $doc.animate({
                    scrollTop: $("#box-status-ctre-fiscal").offset().top
                }, 500);

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

    });

}