function initMap() {
 
    if (
        window.urlSegment3 == 'cadastro-grande-gerador' 
        || window.urlSegment3 == 'cadastro-gg-search'
        || window.urlSegment3 == 'cadastro-grande-gerador-condominio' 
        || window.urlSegment3 == 'cadastro-gc-search'
        || window.urlSegment3 == 'cadastro-pequeno-gerador' 
        || window.urlSegment3 == 'cadastro-pg-search'
        || window.urlSegment3 == 'cadastro-transportadores' 
        || window.urlSegment3 == 'cadastro-tr-search'
        || window.urlSegment3 == 'cadastro-destinos' 
        || window.urlSegment3 == 'cadastro-df-search'
        || window.urlSegment3 == 'cadastro-cooperativa' 
        || window.urlSegment3 == 'cadastro-cr-search'
        ||window.urlSegment3 == 'cadastro-servico-saude'
        ||window.urlSegment3 == 'cadastro-ss-search'
        ||window.urlSegment3 == 'cadastro-orgao-publico'
        ||window.urlSegment3 == 'cadastro-op-search'
        ) {
            
        var latlng = new google.maps.LatLng(-23.5451498, -46.6394854),
            map = new google.maps.Map(document.getElementById('mapa'), {
                center: latlng,
                zoom: 10
            }),
            url = '';
        if (window.urlSegment3 == 'cadastro-grande-gerador') {
            url = "/readGeolocation/1";
        } else if (window.urlSegment3 == 'cadastro-gg-search') {
            url = "/readGeolocation/1/" + window.urlSegment4 + "/" + window.urlSegment5;
        } else if (window.urlSegment3 == 'cadastro-grande-gerador-condominio') {
            url = "/readGeolocation/10";
        } else if (window.urlSegment3 == 'cadastro-gc-search') {
            url = "/readGeolocation/19/" + window.urlSegment4 + "/" + window.urlSegment5;
        }else if (window.urlSegment3 == 'cadastro-pequeno-gerador') {
            url = "/readGeolocation/2";
        } else if (window.urlSegment3 == 'cadastro-pg-search') {
            url = "/readGeolocation/2/" + window.urlSegment4 + "/" + window.urlSegment5;
        } else if (window.urlSegment3 == 'cadastro-transportadores') {
            url = "/readGeolocation/3";
        } else if (window.urlSegment3 == 'cadastro-tr-search') {
            url = "/readGeolocation/3/" + window.urlSegment4 + "/" + window.urlSegment5;
        } else if (window.urlSegment3 == 'cadastro-destinos') {
            url = "/readGeolocation/4";
        } else if (window.urlSegment3 == 'cadastro-df-search') {
            url = "/readGeolocation/4/" + window.urlSegment4 + "/" + window.urlSegment5;
        } else if (window.urlSegment3 == 'cadastro-cooperativa') {
            url = "/readGeolocation/5";
        } else if (window.urlSegment3 == 'cadastro-cr-search') {
            url = "/readGeolocation/5/" + window.urlSegment4 + "/" + window.urlSegment5;
        } else if (window.urlSegment3 == 'cadastro-servico-saude') {
            url = "/readGeolocation/9";
        } else if (window.urlSegment3 == 'cadastro-ss-search') {
            url = "/readGeolocation/9/" + window.urlSegment4 + "/" + window.urlSegment5;
        } else if (window.urlSegment3 == 'cadastro-orgao-publico') {
            url = "/readGeolocation/8";
        } else if (window.urlSegment3 == 'cadastro-op-search') {
            url = "/readGeolocation/8/" + window.urlSegment4 + "/" + window.urlSegment5;
        }

        $.ajax({
            type: 'GET',
            url: url,
            beforeSend: function () {

            },
            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    var locations = [],
                        conteudoPin = [],
                        markers = [];
                     

                    $.each(JSON.parse(data), function (key, val) {

                        

                        locations.push({lat: parseFloat(val.latitude), lng: parseFloat(val.longitude)});
                        var tipo = val.id_limpurb;
                        var image = window.baseUrl + '/images/ping_google.png';
                      
                        if(tipo.includes('GG')){
                            image = window.baseUrl + '/images/pin_gg.png';
                        }
                        if(tipo.includes('PG')){
                           
                            image = window.baseUrl + '/images/pin_pp.png';
                        }
                        if(tipo.includes('TR')){
                            image = window.baseUrl + '/images/pin_transp.png';
                         }
                         if(tipo.includes('DR')){
                            image =  window.baseUrl + '/images/pin_df.png';
                         }
                         if(tipo.includes('CO')){
                            image =  window.baseUrl + '/images/pin_conces.png';
                         }
                        conteudoPin[key] = {
                            razao_social: val.razao_social,
                            endereco: val.endereco + ', ' + val.numero + ' - ' + val.bairro + ', ' + val.nome_cidades + ' - ' + val.sigla_estados + ', ' + val.cep,
                            nr_amlurb: val.id_limpurb,
                            link: val.link
                        };

                        var pos = new google.maps.LatLng(parseFloat(val.latitude), parseFloat(val.longitude));

                        markers[key] = new google.maps.Marker({
                            position: pos,
                            map: map,
                            icon: image,
                            id: key
                        });
                    
                        var infowindow = new google.maps.InfoWindow({
                            content: '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<p style="margin-bottom:0;font-size: 18px;"><b>' + conteudoPin[key].razao_social + '</b></p>' +
                            '<div id="bodyContent">' +
                            '<p>' + conteudoPin[key].endereco + '</p>' +
                            '<p style="margin-bottom:0"><b>EMPRESA GRANDE GERADORA</b></p>' +
                            '<p>Nr. AMLURB: ' + conteudoPin[key].nr_amlurb + '</p>' +
                            '<p> <a href="' + conteudoPin[key].link + '" title="Ver detalhes da empresa">VER DETALHES</a></p>' +
                            '</div>' +
                            '</div>'
                        });
                    
                        google.maps.event.addListener(markers[key], 'click', function () {
                            infowindow.open(map, this);
                        })

                    });
                   

                   

                    var markerCluster = new MarkerClusterer(map, markers,
                        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }
}