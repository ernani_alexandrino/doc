// controla funcoes de select do estado
// variável global
window.cepTriggered = true;

const STR_PAD_RIGHT = 1;
const STR_PAD_LEFT = 2;
const STR_PAD_BOTH = 3;
/**
 * Monta o chosen para Cidades após o estado ser preenchido via retorno de CNPJ no preenchimento automático ou por escolha e interação no mouse
 * @param estadoId
 * @param cidadeNome
 * @param cidadeId
 */
// function montarCidades(estadoId, cidadeNome, cidadeId) {
//
//     console.log(estadoId);
//
//     if (!estadoId) {
//         return;
//     }
//
//     $.getJSON('/cidade/' + estadoId, function (data) {
//         var $selectCidades = $('#cidade'),
//             $optionsTipo = '<option selected disabled>Selecione uma cidade</option>';
//
//         //Lista dos tipos
//         $.each(data, function (idCidade, nomeCidade) {
//             var $cidadeSelecionada = '';
//
//             // Se os dados vierem da API da receita, comparamos com o nome para deixar selecionado a informação correta,
//             // senão fazemos com o id que vem da base interna
//             if (cidadeNome !== '') { // verifica por nome
//                 if (nomeCidade === cidadeNome) {
//                     $cidadeSelecionada = ' selected';
//                 }
//             } else { // verifica por id
//                 if (cidadeId !== 0) {
//                     if (idCidade === cidadeId) {
//                         $cidadeSelecionada = ' selected';
//                     }
//                 }
//             }
//
//             $optionsTipo += '<option value="' + idCidade + '" ' + $cidadeSelecionada + '>' + nomeCidade + '</option>';
//
//         });
//
//         $selectCidades.empty().html($optionsTipo).select2({
//             language: '',
//             placeholder: "Selecione uma cidade",
//             no_results_text: "Nenhum resultado encontrado!",
//             search_contains: true,
//             width: "100%"
//         }).trigger('change');
//     });
//
// }

window.montarCidades = function (estadoId, cidadeNome, cidadeId) {

    if (!estadoId) {
        return;
    }

    if (cepTriggered) {

        $.getJSON("/cidade/" + estadoId, function (data) {

            let $selectCidades = $('#cidade');
            let $optionsTipo = '<option selected disabled>Selecione uma cidade</option>';

            //Lista dos tipos
            $.each(data, function (idCidade, nomeCidade) {
                let $cidadeSelecionada = "";

                // Se os dados vierem da API da receita, comparamos com o nome para deixar selecionado a informação correta,
                // senão fazemos com o id que vem da base interna
                // verifica por nome
                if (cidadeNome !== '') {

                    if (nomeCidade === cidadeNome) {

                        $cidadeSelecionada = " selected";
                    }

                } else {

                    // verifica por id
                    if (cidadeId !== 0) {

                        if (idCidade === cidadeId) {

                            $cidadeSelecionada = " selected";
                        }
                    }
                }

                $optionsTipo += '<option value="' + idCidade + '" ' + $cidadeSelecionada + '>' + nomeCidade + '</option>';

            });

            $selectCidades.empty().html($optionsTipo).select2({
                language: '',
                placeholder: "Selecione uma cidade",
                no_results_text: "Nenhum resultado encontrado!",
                search_contains: true,
                width: "100%"
            }).trigger('change');
        });

    } else {

        let $cidade = $("#cidade");

        if ($cidade) {

            $.each($("option", $cidade), function () {

                if ($(this).text() === cidadeNome) {
                    $(this).attr('selected', 'selected');
                    $(this).trigger("change");
                }

            });
        }
    }
};

window.InputMask = function () {

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };


    $('[type="tel"]').mask(SPMaskBehavior, spOptions);

    $('#telefone, .telefone, #telefone_fixo, #telefone_responsavel, #telefone_celular, #celular_responsavel, #celular').mask(SPMaskBehavior, spOptions);
    $('#data, #data_nasc, #vencimento_ipva, #data_ctre, #data_coleta, #data_descarga, #data_coleta_modal, #data_validade').mask('00/00/0000');
    $('#hora_coleta, #hora_descarga, #hora_coleta_modal').mask('00:00');
    $('#cnpj, #cnpj_vincular, .cnpj').mask('00.000.000/0000-00');
    $('#cep').mask('00000-000');
    $('#ano_veiculo').mask('0000');
    $('#volume_total').mask('0000000000,000', {reverse: true});
    $('#volume_residuos, #tara, #area_total, #capacidade, #area_construida').mask('0000.000.000,00', {reverse: true});
    $('#placa').mask('AAA-9999', {reverse: true});
    $('#renavam').mask('00000000000', {reverse: true});

    $('#licenca_previa_emissao, #licenca_previa_vencimento').mask('00/00/0000');
    $('#licenca_instalacao_emissao, #licenca_instalacao_vencimento').mask('00/00/0000');
    $('#licenca_operacao_emissao, #licenca_operacao_vencimento').mask('00/00/0000');
    $('#alvara_prefeitura_emissao, #alvara_prefeitura_vencimento').mask('00/00/0000');
    $('#ctf_ibama_emissao, #ctf_ibama_vencimento').mask('00/00/0000');
    $('#avcb_emissao, #avcb_vencimento').mask('00/00/0000');
    $('#data_inicial, #data_final').mask('00/00/0000');

    $('.data, .calendario').mask("00/00/0000");
    $('.cpf').mask("000.000.000-00");
    $('.cnh').mask("00000000000");
    $('.rg').mask("00.000.000-A");
    // $('.im').mask("000.000.000.000");
    // $('.ie').mask("000.000.000.000");
    $('.cep').mask("00000-000");
    $('#volume').mask("0000.000");
    $('.hora').mask("00:00");

    // Criando as options para os calendários
    let optionsDatePicker = {
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true,
        todayHighlight: true,
        todayBtn: "linked"
    };

    $('.calendario').datepicker(optionsDatePicker);
    $('#data_inicial, #data_final').datepicker(optionsDatePicker);
    $('#dt_inicio_indicador_periodo, #dt_final_indicador_periodo').datepicker(optionsDatePicker);
    $('#dt_inicio_fiscal_cad_periodo, #dt_final_fiscal_cad_periodo').datepicker(optionsDatePicker);
    $('#data_inicial_fiscal_grafico, #data_final_fiscal_grafico').datepicker(optionsDatePicker);
    $('.input-daterange').datepicker(optionsDatePicker);
    $('#vencimento_ipva').datepicker(optionsDatePicker);
};

window.buscaCep = function () {

    function limpa_formulario_cep() {
        // Limpa valores do formulário de cep.
        $("#endereco").val("");
        $("#bairro").val("");
        $("#ibge").val("");

        $("#estado").attr('readonly', false);
        $("#cidade").attr('readonly', false);
    }

    function consultaAPI(cep) {

        //Preenche os campos com "..." enquanto consulta webservice.
        $("#endereco").val("...");
        $("#bairro").val("...");

        let estado = $('#estado option');

        //Consulta o webservice viacep.com.br/
        $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

            if (!("erro" in dados)) {

                //Atualiza os campos com os valores da consulta.
                $("#endereco").val(dados.logradouro);
                $("#bairro").val(dados.bairro);

                let cidadeNome = dados.localidade;
                let estadoNome = dados.uf;
                let cidadeId = 0;

                estado.each(function () {

                    $(this).removeAttr('selected');

                    if (estadoNome) {

                        if ($(this).text() === estadoNome) {
                            $(this).attr('selected', 'selected');
                            $(this).trigger('change');
                        }
                    }

                });

                estado = $("#estado option:selected").val();

                cidadeNome = removerAcentos(cidadeNome);
                cidadeNome = cidadeNome.toUpperCase();

                // prepara select de cidades
                montarCidades(estado, cidadeNome, cidadeId);

                // desabilita campos
                $("#estado").attr('readonly', true);
                $("#cidade").attr('readonly', true);

            } else {
                //CEP pesquisado não foi encontrado.
                limpa_formulario_cep();
                $(this).parent().find('.text-danger').text("");
                $(this).after("<p class=\"text-danger\">CEP Inválido. Tente Novamente!</p>");
                $(this).focus();
            }

        });

    }

    function preencher_dados_formulario(cep_value) {

        //Nova variável "cep" somente com dígitos.
        let cep = cep_value.replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep !== "") {

            //Expressão regular para validar o CEP.
            let validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) { //Valida o formato do CEP
                consultaAPI(cep);
            } else { // cep invalido
                limpa_formulario_cep();
                alert("Formato de CEP inválido.");
            }
        } else { //cep sem valor, limpa formulário.
            limpa_formulario_cep();
        }
    }

    //Quando o campo cep perde o foco.
    $("#cep").blur(function () {
        // if (cepTriggered) { // evita que funcao de acao change do #estado seja executada
            preencher_dados_formulario($("#cep").val());
            // cepTriggered = false;
        // }
    });
};

/*
 formata_cpf_cnpj

 Formata um CPF ou CNPJ

 @access public
 @return string CPF ou CNPJ formatado
*/
function formata_cpf_cnpj(valor) {

    // O valor formatado
    var formatado = false;

    // Verifica se é CPF ou CNPJ
    var valida = verifica_cpf_cnpj(valor);

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // Valida CPF
    if (valida === 'CPF') {

        // Verifica se o CPF é válido
        if (valida_cpf(valor)) {

            // Formata o CPF ###.###.###-##
            formatado = valor.substr(0, 3) + '.';
            formatado += valor.substr(3, 3) + '.';
            formatado += valor.substr(6, 3) + '-';
            formatado += valor.substr(9, 2) + '';

        }
    }

    // Valida CNPJ
    else if (valida === 'CNPJ') {

        // Verifica se o CNPJ é válido
        if (valida_cnpj(valor)) {

            // Formata o CNPJ ##.###.###/####-##
            formatado = valor.substr(0, 2) + '.';
            formatado += valor.substr(2, 3) + '.';
            formatado += valor.substr(5, 3) + '/';
            formatado += valor.substr(8, 4) + '-';
            formatado += valor.substr(12, 14) + '';

        }
    }

    // Retorna o valor
    return formatado;
}

/*
 Valida CPF

 Valida se for CPF

 @param  string cpf O CPF com ou sem pontos e traço
 @return bool True para CPF correto - False para CPF incorreto
*/
function valida_cpf(valor) {

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // Captura os 9 primeiros dígitos do CPF
    // Ex.: 02546288423 = 025462884
    var digitos = valor.substr(0, 9);

    // Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
    var novo_cpf = calc_digitos_posicoes(digitos, 0, 0);

    // Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
    var novo_cpf = calc_digitos_posicoes(novo_cpf, 11, 0);

    // Verifica se o novo CPF gerado é idêntico ao CPF enviado
    if (novo_cpf === valor) {
        // CPF válido
        return true;
    } else {
        // CPF inválido
        return false;
    }

}

/*
 valida_cnpj

 Valida se for um CNPJ

 @param string cnpj
 @return bool true para CNPJ correto
*/
window.valida_cnpj = function (valor) {

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // O valor original
    let cnpj_original = valor;

    // Captura os primeiros 12 números do CNPJ
    let primeiros_numeros_cnpj = valor.substr(0, 12);

    // Faz o primeiro cálculo
    let primeiro_calculo = calc_digitos_posicoes(primeiros_numeros_cnpj, 5, 0);

    // O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
    let segundo_calculo = calc_digitos_posicoes(primeiro_calculo, 6, 0);

    // Concatena o segundo dígito ao CNPJ
    let cnpj = segundo_calculo;

    // Verifica se o CNPJ gerado é idêntico ao enviado
    if (cnpj === cnpj_original && cnpj_original !== "00000000000000") {
        return true;
    }

    // Retorna falso por padrão
    return false;
};

/**
 * Remove acentos de caracteres
 * @return {String}  [string sem acentos]
 * @param newStringComAcento
 */
window.removerAcentos = (newStringComAcento) => {

    let string = newStringComAcento;
    let mapaAcentosHex = {
        a: /[\xE0-\xE6]/g,
        e: /[\xE8-\xEB]/g,
        i: /[\xEC-\xEF]/g,
        o: /[\xF2-\xF6]/g,
        u: /[\xF9-\xFC]/g,
        c: /\xE7/g,
        n: /\xF1/g
    };

    for (let letra in mapaAcentosHex) {
        let expressaoRegular = mapaAcentosHex[letra];
        string = string.replace(expressaoRegular, letra);
    }

    return string;
};

// valida_cnpj

/*
 valida_cpf_cnpj

 Valida o CPF ou CNPJ

 @access public
 @return bool true para válido, false para inválido
*/
function valida_cpf_cnpj(valor) {

    // Verifica se é CPF ou CNPJ
    var valida = verifica_cpf_cnpj(valor);

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // Valida CPF
    if (valida === 'CPF') {
        // Retorna true para cpf válido
        return valida_cpf(valor);
    }

    // Valida CNPJ
    else if (valida === 'CNPJ') {
        // Retorna true para CNPJ válido
        return valida_cnpj(valor);
    }

    // Não retorna nada
    else {
        return false;
    }

}

/*
 verifica_cpf_cnpj

 Verifica se é CPF ou CNPJ

 @see http://www.todoespacoonline.com/w/
*/
function verifica_cpf_cnpj(valor) {

    // Garante que o valor é uma string
    valor = valor.toString();

    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');

    // Verifica CPF
    if (valor.length === 11) {
        return 'CPF';
    }

    // Verifica CNPJ
    else if (valor.length === 14) {
        return 'CNPJ';
    }

    // Não retorna nada
    else {
        return false;
    }
}

/*
 calc_digitos_posicoes

 Multiplica dígitos vezes posições

 @param string digitos Os digitos desejados
 @param string posicoes A posição que vai iniciar a regressão
 @param string soma_digitos A soma das multiplicações entre posições e dígitos
 @return string Os dígitos enviados concatenados com o último dígito
*/
window.calc_digitos_posicoes = function (digitos, posicoes, soma_digitos) {

    // Garante que o valor é uma string
    digitos = digitos.toString();

    if (posicoes == 0) {
        posicoes = 10;
    }

    // Faz a soma dos dígitos com a posição
    // Ex. para 10 posições:
    //   0    2    5    4    6    2    8    8   4
    // x10   x9   x8   x7   x6   x5   x4   x3  x2
    //   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
    for (var i = 0; i < digitos.length; i++) {
        // Preenche a soma com o dígito vezes a posição
        soma_digitos = soma_digitos + (digitos[i] * posicoes);

        // Subtrai 1 da posição
        posicoes--;

        // Parte específica para CNPJ
        // Ex.: 5-4-3-2-9-8-7-6-5-4-3-2
        if (posicoes < 2) {
            // Retorno a posição para 9
            posicoes = 9;
        }
    }

    // Captura o resto da divisão entre soma_digitos dividido por 11
    // Ex.: 196 % 11 = 9
    soma_digitos = soma_digitos % 11;

    // Verifica se soma_digitos é menor que 2
    if (soma_digitos < 2) {
        // soma_digitos agora será zero
        soma_digitos = 0;
    } else {
        // Se for maior que 2, o resultado é 11 menos soma_digitos
        // Ex.: 11 - 9 = 2
        // Nosso dígito procurado é 2
        soma_digitos = 11 - soma_digitos;
    }

    // Concatena mais um dígito aos primeiro nove dígitos
    // Ex.: 025462884 + 2 = 0254628842
    return digitos + soma_digitos;
};

window.loading = function ($acao) {

    let $loading = $('#loading-cadastro');

    if ($acao === 1) {
        $loading.addClass('ativo');
    } else {
        $loading.removeClass('ativo');
    }
};

window.loadingAjax = function ($acao) {

    let $elementoLoading = $('#ajax-panel, .ajax-panel');

    if ($acao === 1) {
        $elementoLoading.html("<div class=\"loading\"><img src=\"/images/logo-loading.gif\" alt=\"Buscando...\"/></div>");
    } else {
        $elementoLoading.empty();
    }
};

window.getAjaxHeaders = () => {
    return {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        "contentType": "application/json; charset=utf-8"
    };
};

// VALIDACAO SEMANTICA
// verifica se todos os caracteres na string sao iguais
window.allEqual = (input) => {
    return input.split('').every(char => char === input[0]);
};

/*
 str_pad

 Preenche uma string para um certo tamanho com outra string

 @param string strInput String que terá o preenchimento
 @param string intLength Tamanho do preenchimento
 @param string strString String utilizada para preenchimento. Default: espaço 
 @param string intPadType Posição na string de input onde será aplicado o preenchimento.
                Opções:
                    STR_PAD_RIGHT - preencher a direita 
                    STR_PAD_LEFT - preencher a esquerda 
                    STR_PAD_BOTH - preencher em ambos os lados 
                Default: STR_PAD_RIGHT 
 @return string String de input completadas com a string de preenchimento até o 
                limite de tamanho estabelecido  
 */
window.str_pad = function(strInput, intLength, strString = ' ', intPadType = STR_PAD_RIGHT) {
	let strReturn = strInput.toString();
	while (strReturn.length < intLength) {
        switch (intPadType) {
            case STR_PAD_RIGHT:
                strReturn = strReturn + strString;
                break;
            case STR_PAD_LEFT:
                strReturn = strString + strReturn;
                break;
            case STR_PAD_BOTH:
                strReturn = strString + strReturn + strString;
                break;
        }
    }
    
    strReturn = strReturn.substr(0, intLength);

	return strReturn;
};