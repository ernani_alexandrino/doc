$(function () {

    funcoesPagamentos();

    if (window.location.href.includes('pagamentos')) {
        atualizaListaBoletos();
    }

});

function funcoesPagamentos() {

    let id_boleto = ''; // controle de boleto selecionado

    // mostra modal de visualizar boleto
    $('#boletos-list').on('click', 'tr', function () {
        id_boleto = $(this).attr('id');

        let url = '/painel/boletos/' + id_boleto;
        $.ajax({
            url: url,
            success: function (modal) {

                $('#modalVisualizarBoleto').empty().append(modal.html);
                $('#modalVisualizarBoleto').modal('show');

            }
        });

    });

    $('#boletos-list').on('click', '.link-acoes__2via', function (e) {
        e.stopPropagation(); // protege evento de vazar para outros elementos
        id_boleto = $(this).parents('tr:first').attr('id'); // id da tr

        let url = '/painel/boleto-segunda-via/' + id_boleto;
        $.ajax({
            url: url,
            success: function (data) {
                $('#pagamento-success').empty().append(data['success']).parent().parent().show();
                let url = '/painel/boleto-pdf/' + data['boleto_id'];
                window.open(url, '_blank');
                $("#table-boletos-vinculados").DataTable().ajax.reload();
            }
        });
    });

    $('#boletos-list').on('click', '.link-acoes__imprimir', function (e) {
        e.stopPropagation(); // protege evento de vazar para outros elementos
        id_boleto = $(this).parents('tr:first').attr('id'); // id da tr

        let url = '/painel/boleto-pdf/' + id_boleto;
        window.open(url, '_blank');
    });

    $('#modalVisualizarBoleto').on('click', '#btn-close-visualizar-boleto', function () {
        $('#modalVisualizarBoleto').modal('hide');
    });

}

let atualizaListaBoletos = () => {

    $("#table-boletos-vinculados").DataTable({
        "language": datatableLanguageParams("Esta empresa não possui nenhum boleto gerado"),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/boletos-list",
        columns: [
            {data: 'data_emissao', name: 'data_emissao'},
            {data: 'serv_guia', name: 'serv_guia'},
            {data: 'tipo', name: 'tipo'},
            {data: 'codigo_item', name: 'codigo_item'},
            {data: 'data_vencimento', name: 'data_vencimento'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'}
        ]
    });

};