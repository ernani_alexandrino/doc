$(function () {

    funcoesVeiculos();

    vinculosGeradorTransportador();

    // so busca estes dados se estiver logado como transportador
    if (window.location.href.includes('transportador/meus-equipamentos')) {
        atualizaListaVeiculosTransportador();
    }
    
    $('#anexo_documento_inmetro').on("change", enviarArquivo($('#anexo_documento_inmetro'), 'veiculo_upload'));
    $('#anexo_documento_veiculo').on("change", enviarArquivo($('#anexo_documento_veiculo'), 'veiculo_upload'));
    $('#anexo_documento_comodato').on("change", enviarArquivo($('#anexo_comodato_veiculo'), 'veiculo_upload'));

    $('#anexo_documento_inmetro_editar').on("change", enviarArquivoEditar($('#anexo_documento_inmetro_editar'), 'veiculo_upload_editar'));
    $('#anexo_documento_veiculo_editar').on("change", enviarArquivoEditar($('#anexo_documento_veiculo_editar'), 'veiculo_upload_editar'));
    $('#anexo_documento_comodato_editar').on("change", enviarArquivoEditar($('#anexo_comodato_veiculo_editar'), 'veiculo_upload_editar'));

});


function funcoesVeiculos() {

    let id_veiculo = ''; // controle de veiculo selecionado
    let msg_email = 'Você receberá um email contendo o boleto referente ao(s) veículo(s) selecionado(s)';
    let msg_boleto = 'Boleto gerado(s). Você pode verificá-lo(s) na página de pagamentos';

    // requisicao para gerar novo boleto para um veiculo
    let novoBoletoVeiculo = function(modo) {
        let url = 'novo-boleto-veiculo/' + id_veiculo;
        $.ajax({
            type: 'POST',
            url: url,
            headers: getAjaxHeaders(),
            data: {modo: modo},
            success: function (data) {
                if (modo == 'email') {
                    $('#ci-success').empty().append(msg_email).parent().parent().show();
                } else {
                    window.open('/painel/boleto-pdf/' + data['boleto_id'], '_blank')
                }
                $("#table-veiculos-vinculados").DataTable().ajax.reload();

            }
        });
        $('#modalVeiculoLicenca').modal('hide');
    };

    function apontarErrosFormVeiculos(data, $formItens) {
        let cont = 0;
        $.each(data, function () {

            $formItens.placa.parent().find('.text-danger').text("");
            if (data.error[cont].placa) {
                $formItens.placa.after('<p class="text-danger">' + data.error[cont].placa + '</p>');
            }

            $formItens.ano_veiculo.parent().find('.text-danger').text("");
            if (data.error[cont].ano_veiculo) {
                $formItens.ano_veiculo.after('<p class="text-danger">' + data.error[cont].ano_veiculo + '</p>');
            }

            $formItens.renavam.parent().find('.text-danger').text("");
            if (data.error[cont].renavam) {
                $formItens.renavam.after('<p class="text-danger">' + data.error[cont].renavam + '</p>');
            }

            $formItens.tipo.parent().find('.text-danger').text("");
            if (data.error[cont].tipo) {
                $formItens.tipo.after('<p class="text-danger">' + data.error[cont].tipo + '</p>');
            }

            $formItens.marca.parent().find('.text-danger').text("");
            if (data.error[cont].marca) {
                $formItens.marca.after('<p class="text-danger">' + data.error[cont].marca + '</p>');
            }

            $formItens.vencimento_ipva.parent().find('.text-danger').text("");
            if (data.error[cont].vencimento_ipva) {
                $formItens.vencimento_ipva.after('<p class="text-danger">' + data.error[cont].vencimento_ipva + '</p>');
            }

            $formItens.capacidade.parent().find('.text-danger').text("");
            if (data.error[cont].capacidade) {
                $formItens.capacidade.after('<p class="text-danger">' + data.error[cont].capacidade + '</p>');
            }

            $formItens.tara.parent().find('.text-danger').text("");
            if (data.error[cont].tara) {
                $formItens.tara.after('<p class="text-danger">' + data.error[cont].tara + '</p>');
            }

            $formItens.numero_inmetro.parent().find('.text-danger').text("");
            if (data.error[cont].numero_inmetro) {
                $formItens.numero_inmetro.after('<p class="text-danger">' + data.error[cont].numero_inmetro + '</p>');
            }

            $('#erro-anexo_documento_inmetro').parent().find('.text-danger').text("");
            if (data.error[cont].anexo_documento_inmetro_enviado) {
                $('#erro-anexo_documento_inmetro').after('<p class="text-danger"> O campo Documento Inmetro é obrigatório.</p>');
             
            }
            $('#erro-anexo_documento_veiculo').parent().find('.text-danger').text("");
            if (data.error[cont].anexo_documento_veiculo_enviado) {
                $('#erro-anexo_documento_veiculo').after('<p class="text-danger">O campo Documento do Veículo é obrigatório.</p>');
            }
          
        });
    }

    //mostra o modal de veiculos
    $('#btn_add_veiculo').click(function () {
        $('#btn-save-veiculo').val("add");
        $('#frmVeiculos').trigger("reset");
        $('#modalVeiculoAdicionar').modal('show');
    });

    $('#btn-cancel-veiculo').click(function () {
        $('#modalVeiculoAdicionar').modal('hide');
    });

    $("#btn-save-veiculo").click(function (e) {

        $placa = $("#placa").val();
        $capacit = $("#capacidade").val();

        // $placa = $("#placa").val().replace('-','');
        // $capacit = $("#capacidade").val().replace('.','').replace(',','');
        
        let formDataVeiculo = {
            _token: $("input[name='_token']").val(),
            placa: $placa,
            ano_veiculo: $("#ano_veiculo").val(),
            renavam: $("#renavam").val(),
            tipo: $("#tipo").val(),
            marca: $("#marca").val(),
            vencimento_ipva: $("#vencimento_ipva").val(),
            capacidade: $capacit,
            tara: $("#tara").val(),
            numero_inmetro: $("#numero_inmetro").val(),
            anexo_documento_inmetro_enviado: $("#anexo_documento_inmetro_enviado").val(),
            anexo_documento_veiculo_enviado: $("#anexo_documento_veiculo_enviado").val(),
            anexo_comodato_veiculo_enviado: $("#anexo_comodato_veiculo_enviado").val()
        };

        let url = '/painel/veiculo-salvar';

        $.ajax({
            type: 'POST',
            url: url,
            data: formDataVeiculo,
            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    $('#frmVeiculos').trigger("reset");
                    $("#table-veiculos-vinculados").DataTable().ajax.reload();
                    $('#ci-success').empty().append(data['success']).parent().parent().show();
                    $('#modalVeiculoAdicionar').modal('hide');

                } else {

                    let $formItens = {
                        placa: $("#placa"),
                        ano_veiculo: $("#ano_veiculo"),
                        renavam: $("#renavam"),
                        tipo: $("#tipo"),
                        marca: $("#marca"),
                        vencimento_ipva: $("#vencimento_ipva"),
                        capacidade: $("#capacidade"),
                        tara: $("#tara"),
                        numero_inmetro: $("#numero_inmetro"),
                        anexo_documento_inmetro_enviado: $("#erro-anexo_documento_inmetro"),
                        anexo_documento_veiculo_enviado: $("#erro-anexo_documento_veiculo"),
                        anexo_comodato_veiculo_enviado: $("#erro-anexo_comodato_veiculo")
                    };

                    apontarErrosFormVeiculos(data, $formItens);
                }

            },

            error: function (data) {
                console.log('Error: ', data);
            }

        })
    });



    $('#btn_atv_rnv_veiculo').on('click', function () {
        $.ajax({
            type: 'GET',
            url: '/painel/transportador/veiculos-ativar',
            success: function (data) {

                $('#modalAtivarRenovarVeiculos').empty().append(data.html);
                $('#modalAtivarRenovarVeiculos').modal('show');
            }
        })
    });

    // checkbox para marcar/desmarcar veiculos
    $('#modalAtivarRenovarVeiculos').on('click', '#select-all-veiculos', function () {
        let check_value = (this.checked) ? true : false;
        $('.checkbox-veiculo').each(function () {
            this.checked = check_value;
        });
    });

    $('#modalAtivarRenovarVeiculos').on('click', '#btn-save-ativar-veiculos-tr', function () {
        let veiculos = [];
        $('.checkbox-veiculo').each(function () {
            if (this.checked == true) {
                veiculos.push($(this).val());
            }
        });

        $.ajax({
            type: 'POST',
            url: '/painel/transportador/novo-boleto-varios-veiculos',
            headers: getAjaxHeaders(),
            data: {veiculos: veiculos},
            success: function () {
                $('#ci-success').empty().append(msg_boleto).parent().parent().show();
                $("#table-veiculos-vinculados").DataTable().ajax.reload();
                $('#modalAtivarRenovarVeiculos').modal('hide');
            }
        });
    });

    $('#modalAtivarRenovarVeiculos').on('click', '#btn-reset-ativar-veiculos-tr', function () {
        $('#modalAtivarRenovarVeiculos').modal('hide');
    });

    // modal veiculo individual
    $('#veiculo-list').on('click', '.ativar-veiculo', function () {
        id_veiculo = $(this).parents('tr:first').attr('id'); // id da tr
        $('#modalVeiculoLicenca h4').empty().append('Ativar Veículo');
        $('#modalVeiculoLicenca #p1').empty().append('Deseja ativar este veículo?');
        $('#modalVeiculoLicenca').modal('show');
    });
    $('#veiculo-list').on('click', '.renovar-veiculo', function () {
        id_veiculo = $(this).parents('tr:first').attr('id'); // id da tr
        $('#modalVeiculoLicenca h4').empty().append('Renovar Veículo');
        $('#modalVeiculoLicenca #p1').empty().append('Deseja renovar a licença deste veículo?');
        $('#modalVeiculoLicenca').modal('show');
    });

    // fechar modal
    $('#modalVeiculoLicenca #btn-cancel-boleto-tr').on('click', function () {
        $('#modalVeiculoLicenca').modal('hide');
    });

    $('#modalVeiculoLicenca #btn-boleto-email-tr').on('click', function () {
        novoBoletoVeiculo('email');
        $(document).scrollTop(0);
    });

    $('#modalVeiculoLicenca #btn-boleto-imprimir-tr').on('click', function () {
        novoBoletoVeiculo('imprimir');
    });


    // modal qrcode
    $('#veiculo-list').on('click', '.solicitar-qrcode', function () {
        id_veiculo = $(this).parents('tr:first').attr('id'); // id da tr
        $('#modalVeiculoQrcode').modal('show');
    });

    // fechar modal
    $('#modalVeiculoQrcode #btn-cancel-boleto-tr').on('click', function () {
        $('#modalVeiculoQrcode').modal('hide');
    });

    $('#modalVeiculoQrcode #btn-qrcode-veiculo-tr').on('click', function () {

        if ($('input[id="concordancia-qrcode"]').is(':checked')) {

            $.ajax({
                url: '/painel/novo-qrcode-veiculo/' + id_veiculo,
                success: (data) => {

                    window.open('/painel/qrcode-pdf/' + data['qrcode_id'], '_blank');
                    $('#modalVeiculoQrcode').modal('hide');

                }
            });

            $('#form-checkbox-qrcode p.text-danger').empty();

        } else {

            $('#form-checkbox-qrcode p.text-danger').empty().append("É necessário concordar com as regras para acessar o QR Code");

        }

    });

    // deletar veiculo sem licenca
    $('#veiculo-list').on('click', '.delete-veiculo-force', function () {
        id_veiculo = $(this).parents('tr:first').attr('id'); // id da tr
        $.ajax({
            type: 'DELETE',
            headers: getAjaxHeaders(),
            url: '/painel/veiculo-force-deletar/' + id_veiculo,
            success: function (data) {
                $('#ci-success').empty().append(data['success']).parent().parent().show();
                $("#table-veiculos-vinculados").DataTable().ajax.reload();
                $(document).scrollTop(0);
            }
        })
    });

    // deletar veiculo com licenca
    $('#veiculo-list').on('click', '.delete-veiculo', function () {
        id_veiculo = $(this).parents('tr:first').attr('id'); // id da tr
        // limpa campos e mostra modal
        $("#jev_p").empty();
        $("#justificativa_exclusao_veiculo").val("");
        $('#modalVeiculoExcluir').modal('show');
    });

    // editar veiculo
    $('#veiculo-list').on('click', '.editar-veiculo', function () {
        id_veiculo = $(this).parents('tr:first').attr('id'); // id da tr
        let $modal = $('#modalAprovaReprovaVeiculo');
        $('.mensagem-validade').addClass("hide");
        $.ajax({
            url: "/painel/transportador/get/veiculo/"+id_veiculo,
            type: "GET",
            beforeSend: function () {
                loading(1);
                
            },
            success: function ($retorno) {
                $modal.find('#id_veiculo').val($retorno.dados_veiculo.id)
                $modal.find('#ano_veiculo').val($retorno.dados_veiculo.ano_veiculo)
                $modal.find('#placa').val($retorno.dados_veiculo.placa)
                $modal.find('#renavam').val($retorno.dados_veiculo.renavam)
                $modal.find('#tipo').val($retorno.dados_veiculo.tipo)
                $modal.find('#marca').val($retorno.dados_veiculo.marca)
                $modal.find('#vencimento_ipva').val($retorno.dados_veiculo.vencimento_ipva)
                $modal.find('#capacidade').val($retorno.dados_veiculo.capacidade)
                $modal.find('#tara').val($retorno.dados_veiculo.tara)
                $modal.find('#numero_inmetro').val($retorno.dados_veiculo.numero_inmetro)
                $link_doc_inmetro = $link_doc_veiculo = $link_doc_comodato = null;
                if ($retorno.dados_veiculo.anexo_documento_inmetro)
                    $link_doc_inmetro = $retorno.dados_veiculo.anexo_documento_inmetro+'/'+$retorno.dados_veiculo.placa;
                if ($retorno.dados_veiculo.anexo_documento_veiculo)
                    $link_doc_veiculo = $retorno.dados_veiculo.anexo_documento_veiculo+'/'+$retorno.dados_veiculo.placa;
                if ($retorno.dados_veiculo.anexo_comodato_veiculo)
                    $link_doc_veiculo = $retorno.dados_veiculo.anexo_comodato_veiculo+'/'+$retorno.dados_veiculo.placa;
                $modal.find('#ver_anexo_documento_inmetro').attr("href", $link_doc_inmetro)
                $modal.find('#ver_anexo_documento_veiculo').attr("href", $link_doc_veiculo)
                $modal.find('#ver_anexo_comodato_veiculo').attr("href", $link_doc_comodato)
                $modal.find('.justificativa .alert').text($retorno.dados_veiculo.justificativa);
                
                $modal.modal('show');
                InputMask();
            },
            error: function ($retorno) {

                loading(0);
            }
        });
        


    });
    $('#btn-cancel-veiculo').on('click', function () {        
        $('#modalAprovaReprovaVeiculo').modal('hide');
    });
    $('#btn-excluir-veiculo-tr').on('click', function () {

        let justificativa = $("#justificativa_exclusao_veiculo").val();
        if (justificativa.length === 0) {
            $("#jev_p").empty().append("Valor precisa estar preenchido");
            return;
        }

        $.ajax({
            type: 'DELETE',
            headers: getAjaxHeaders(),
            url: '/painel/veiculo-deletar/' + id_veiculo,
            data: {justificativa: justificativa},
            success: function (data) {
                $('#ci-success').empty().append(data['success']).parent().parent().show();
                $("#table-veiculos-vinculados").DataTable().ajax.reload();
                $('#modalVeiculoExcluir').modal('hide');
            }
        })
    });

    $('#btn-cancel-veiculo-tr').on('click', function () {
        $('#modalVeiculoExcluir').modal('hide');
    });

}


function enviarArquivoEditar($input, url) {
    var _token = $("input[name='_token']").val();
    var classBtn = ".btn-upload";
    if ($input.length > 0) {
        $input.fileupload({
            url: url +'/' + $input[0].id,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf)$/i,
            maxFileSize: 3073,
            progressServerRate: 0.3,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': _token
            },
            add: function (e, data) {
                $(classBtn, $(this).parent().parent()).removeClass('error success');
                $('.progress .bar', $(this).parent()).css('width', '0%').attr('data-content', '0');
                data.submit();
            },
            progressall: function (e, data) {

                let progress = parseInt(data.loaded / data.total * 100, 10);

                $('.progress p', $(this).parent()).html('Enviando...');
                $('.progress .bar', $(this).parent()).css('width', progress + '%').attr('data-content', progress + '%');
            },
            error: function (e, data) {
               console.log(data);
            },
            done: function (e,data) {
                
                $(this).parents('.form-group').find('p.text-danger').remove();
                var result;
                $.each(data.result, function (index, val) {
                    result = val;
                });

                if (result == true) {
                    $('.progress p', $(this).parent()).html("Enviado com sucesso");
                    $(classBtn, $(this).parent().parent()).addClass('success');
                    $('.form-group', $(this).parent().parent().parent()).removeClass('error');
                    //Grava em um input hidden somente para validar se o arquivo ja foi enviado para o servidor, já que o input file fica vazio após o envio por ajax.
                    $('#' + this.id + '_enviado').val($input[0].files[0].name);
                } else {

                    let $mensagem = 'Preencha a Placa do Veículo, Renavam  e tente novamente';

                    $(this).parents('.form-group').addClass('error').append('<p class="text-danger">' + $mensagem + '</p>');

                    $('.progress p', $(this).parent()).html('Falhou, tente novamente');
                    $(classBtn, $(this).parent().parent()).addClass('error');
                }
               
            }
        });
    }
}

function enviarArquivo($input, url) {
    var _token = $("input[name='_token']").val();
    var classBtn = ".btn-upload";
    if ($input.length > 0) {
        $input.fileupload({
            url: url +'/' + $input[0].id,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf)$/i,
            maxFileSize: 3073,
            progressServerRate: 0.3,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': _token
            },
            add: function (e, data) {
                $(classBtn, $(this).parent().parent()).removeClass('error success');
                $('.progress .bar', $(this).parent()).css('width', '0%').attr('data-content', '0');
                data.submit();
            },
            progressall: function (e, data) {

                let progress = parseInt(data.loaded / data.total * 100, 10);

                $('.progress p', $(this).parent()).html('Enviando...');
                $('.progress .bar', $(this).parent()).css('width', progress + '%').attr('data-content', progress + '%');
            },
            error: function (e, data) {
               console.log(data);
            },
            done: function (e,data) {
                
                $(this).parents('.form-group').find('p.text-danger').remove();
                var result;
                $.each(data.result, function (index, val) {
                    result = val;
                });

                if (result == true) {
                    $('.progress p', $(this).parent()).html("Enviado com sucesso");
                    $(classBtn, $(this).parent().parent()).addClass('success');
                    $('.form-group', $(this).parent().parent().parent()).removeClass('error');
                    //Grava em um input hidden somente para validar se o arquivo ja foi enviado para o servidor, já que o input file fica vazio após o envio por ajax.
                    $('#' + this.id + '_enviado').val($input[0].files[0].name);
                } else {

                    let $mensagem = 'Preencha a Placa do Veículo, Renavam  e tente novamente';

                    $(this).parents('.form-group').addClass('error').append('<p class="text-danger">' + $mensagem + '</p>');

                    $('.progress p', $(this).parent()).html('Falhou, tente novamente');
                    $(classBtn, $(this).parent().parent()).addClass('error');
                }
               
            }
        });
    }
}

function vinculosGeradorTransportador() {

    let id_gerador = "";

    const resetarFormGeradorTransportador = () => {
        $('#frmVincularGerador')[0].reset();
        $('#coleta_diaria').val("").trigger('change');
        $('#equipamentos_vinculo').val("").trigger('change');
        $('#frmVincularGerador #residuos_vinculo').val("").trigger('change');
        $('#equipamentos-escolhidos').empty();
        $('#frmVincularGerador #residuos-escolhidos').empty();
        $('#razao_social_gerador').attr("readonly", false);
        $('#nome_fantasia_gerador').attr("readonly", false);
    };

    let apontarErrosFormGeradorTransportador = (data) => {

        $('#tr-razaosocial-gerador p.text-danger').empty();
        if ("razao_social" in data.error) {
            $('#tr-razaosocial-gerador p.text-danger').append(data.error['razao_social']);
        }

        $('#tr-nomefantasia-gerador p.text-danger').empty();
        if ('nome_fantasia' in data.error) {
            $('#tr-nomefantasia-gerador p.text-danger').append(data.error['nome_fantasia']);
        }

        $('#tr-responsavel-gerador p.text-danger').empty();
        if ('responsavel' in data.error) {
            $('#tr-responsavel-gerador p.text-danger').append(data.error['responsavel']);
        }

        $('#tr-email-gerador p.text-danger').empty();
        if ('email' in data.error) {
            $('#tr-email-gerador p.text-danger').append(data.error['email']);
        }

        $('#tr-cnpj-gerador p.text-danger').empty();
        if ('cnpj' in data.error) {
            $('#tr-cnpj-gerador p.text-danger').append(data.error['cnpj']);
        }

        $('#warnings-gerador p.text-warning').empty();
        if ('warning' in data.error) {
            $('#warnings-gerador p.text-warning').append(data.error['warning']);
            resetarFormGeradorTransportador();
        }

    };

    let apontarErrosFormEditGeradorTransportador = (data) => {
        $('#frmEditVincularGerador #tr-responsavel-gerador p.text-danger').empty();
        if ('responsavel' in data.error) {
            $('#frmEditVincularGerador #tr-responsavel-gerador p.text-danger').append(data.error['responsavel']);
        }

        $('#frmEditVincularGerador #tr-email-gerador p.text-danger').empty();
        if ('email' in data.error) {
            $('#frmEditVincularGerador #tr-email-gerador p.text-danger').append(data.error['email']);
        }
    };

    // mostra o modal de Geradores
    $('#btn_add_gerador').click(function () {
        $('#modalNewVinculoGerador').modal('show');
        $('#vinculo-success').empty().parent().parent().hide();
    });

    // mostra modal de editar Gerador
    $('#gerador-list').on('click', 'tr', function () {
        id_gerador = $(this).attr('id');

        $.ajax({
            url: "/painel/vinculos/" + id_gerador,
            success: function (modal) {
                $('#modalEditGerador').empty().append(modal.html);
                $('#modalEditGerador').modal('show');
            }
        });
    });

    // salvar vinculo com gerador
    $('#btn-save-new-gerador-tr').click(function (e) {
        e.preventDefault();

        let formDataGerador = {
            _token: $("input[name='_token']").val(),
            tipo_empresa: 'gerador',
            cnpj: $("#cnpj_vincular_gerador").val(),
            razao_social: $("#razao_social_gerador").val(),
            nome_fantasia: $("#nome_fantasia_gerador").val(),
            responsavel: $("#responsavel_gerador").val(),
            email: $("#email_gerador").val(),
            telefone: $("#telefone_gerador").val(),
            coleta_diaria: $("#coleta_diaria").val(),
            equipamentos_vinculo: $('input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            residuos_vinculo: $('input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join()
        };

        $.ajax({
            url: "/painel/vincular-empresa",
            type: "POST",
            headers: getAjaxHeaders(),
            data: formDataGerador,
            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    // resseta formulario, informa mensagem de sucesso
                    // e atualiza listagem
                    $('#vinculo-success').append(data['success']).parent().parent().show();
                    $('#modalNewVinculoGerador').modal('hide');

                    $("#table-geradores-vinculados").DataTable().ajax.reload();
                    resetarFormGeradorTransportador();

                }

                apontarErrosFormGeradorTransportador(data);

            }
        });
    });

    // salvar vinculo com gerador
    $('#modalEditGerador').on('click', '#btn-save-edit-gerador-tr', function (e) {
        e.preventDefault();

        let formDataGerador = {
            _token: $("#frmEditVincularGerador input[name='_token']").val(),
            responsavel: $("#frmEditVincularGerador #responsavel_gerador").val(),
            email: $("#frmEditVincularGerador #email_gerador").val(),
            telefone: $("#frmEditVincularGerador #telefone_gerador").val(),
            coleta_diaria: $("#frmEditVincularGerador #coleta_diaria").val(),
            equipamentos_vinculo: $('#frmEditVincularGerador input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            residuos_vinculo: $('#frmEditVincularGerador input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join()
        };

        let url = '/painel/vinculos/' + id_gerador;

        $.ajax({
            type: 'PATCH',
            headers: getAjaxHeaders(),
            url: url,
            data: formDataGerador,

            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    $('#vinculo-success').empty().append(data['success']).parent().parent().show();
                    $("#table-geradores-vinculados").DataTable().ajax.reload();
                    $('#modalEditGerador').modal('hide');

                }

                apontarErrosFormEditGeradorTransportador(data);
            }

        })

    });

    // reseta modal de Geradores
    $('#btn-reset-new-gerador-tr').click(function () {
        $('#modalNewVinculoGerador').modal('hide');
        apontarErrosFormGeradorTransportador({'error': {}});
        resetarFormGeradorTransportador();
    });

    // fecha modal de editar Geradores
    $('#modalEditGerador').on('click', '#btn-reset-edit-gerador-tr', function () {
        $('#modalEditGerador').modal('hide');
        $('#modalEditGerador').empty();
    });

    // traz dados sobre gerador para popular form
    $('#cnpj_vincular_gerador').focusout(function () {

        if (valida_cnpj($(this).val())) {

            let $cnpj = $(this).val().replace(/[^\d]+/g, '');
            let url = '/api/cnpj-empresa/' + $cnpj;
            let razao_social = '', nome_fantasia = '';

            $.ajax({
                url: url,
                beforeSend: function () {
                    loadingAjax(1);
                    apontarErrosFormGeradorTransportador({'error': {}});
                },
                success: function (data) {

                    loadingAjax(0);

                    // verifica fonte dos dados da empresa
                    if (data.dadosEmpresa) {
                        razao_social = data.dadosEmpresa.inf.razao_social;
                        nome_fantasia = data.dadosEmpresa.inf.nome_comercial;
                    } else {
                        razao_social = data.obj.nome;
                        nome_fantasia = data.obj.fantasia;
                    }

                    if (razao_social.length > 0) {
                        $('#razao_social_gerador').val(razao_social).attr('readonly', true);
                    }
                    if (nome_fantasia.length > 0) {
                        $('#nome_fantasia_gerador').val(nome_fantasia).attr('readonly', true);
                    }

                }

            })

        } else {
            apontarErrosFormGeradorTransportador({'error': {'cnpj': 'CNPJ Inválido. Tente Novamente!'}});
        }

    });

}

let atualizaListaVeiculosTransportador = () => {

    let msg_empty = "Você não possui nenhum veículo cadastrado. Clique abaixo para adicionar.";    
    

    $("#table-veiculos-vinculados").DataTable({
        "language": datatableLanguageParams(msg_empty),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/transportador/veiculos-list",
        columns: [
            {data: 'codigo_amlurb', name: 'codigo_amlurb'},
            {data: 'placa', name: 'placa'},
            {data: 'ano', name: 'ano'},
            {data: 'renavam', name: 'renavam'},
            {data: 'tipo', name: 'tipo'},
            {data: 'marca', name: 'marca'},
            {data: 'ipva', name: 'ipva'},
            {data: 'capacidade', name: 'capacidade', class:"maskCapacidade"},
            {data: 'status', name: 'status'},
            {data: 'validade', name: 'validade'},
            {data: 'acao', name: 'acao'}
        ]
    });

};