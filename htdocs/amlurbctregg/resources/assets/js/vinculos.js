$(function () {

    // controla os botões de inclusão e exclusão de resíduos
    // e de equipamentos nos modais do sistema

    vinculosEquipamentos();
    vinculosResiduos();

});

function vinculosEquipamentos() {

    // constroi div conforme equipamentos forem escolhidos
    let inserirEquipamentosNaLista = ($equipamentoId, $equipamentoText, $equipamentosEscolhidos) => {
        let dadosTable =
            "<div>" +
            "<div class='col-xs-11'>" +
            "<div class='nome-equipamento' data-id-equipamento='" + $equipamentoId + "'>" + $equipamentoText + "</div>" +
            "</div>" +
            "<div class='col-xs-1'>" +
            "<div id='excluir-equipamento-vinculo' class='excluir-equipamento-vinculo excluir-ico' data-id-equipamento='" + $equipamentoId + "'>X</div>" +
            "</div>" +
            "</div>";

        let inputForm = "<input type='hidden' name='equipamento_escolhido[]' id='equipamento_" + $equipamentoId + "' value='" + $equipamentoId + "'>";

        $(dadosTable).hide().appendTo($equipamentosEscolhidos).fadeIn(700);
        $equipamentosEscolhidos.prepend(inputForm);
    };

    let selecionarItensEquipamentosForm = ($formName) => {
        let $equipamentoId = $($formName + " #equipamentos_vinculo option:selected").val(),
            $equipamentoText = $($formName + " #equipamentos_vinculo option:selected").text(),
            $equipamentosEscolhidos = $($formName + " #equipamentos-escolhidos");
        inserirEquipamentosNaLista($equipamentoId, $equipamentoText, $equipamentosEscolhidos);
    };

    // botoes de incluir equipamentos
    $("#incluir_equipamentos_vinculo").click(function (e) {
        e.preventDefault();
        selecionarItensEquipamentosForm("");
    });
    $("#modalEditTransportador").on('click', '#incluir_equipamentos_vinculo', function (e) {
        e.preventDefault();
        selecionarItensEquipamentosForm("#modalEditTransportador");
    });
    $("#modalEditGerador").on('click', '#incluir_equipamentos_vinculo', function (e) {
        e.preventDefault();
        selecionarItensEquipamentosForm("#frmEditVincularGerador");
    });
    $("#modalValidarCTRE").on('click', '#incluir_equipamentos_vinculo', function (e) {
        e.preventDefault();
        selecionarItensEquipamentosForm("#modalValidarCTRE");
    });

    // botoes de excluir residuos
    $(".equipamentos-escolhidos").on('click', '.excluir-equipamento-vinculo', function (e) {
        let $equipamentoId = $(this).data('id-equipamento');
        $(this).parent().parent().parent().find("#equipamento_" + $equipamentoId).remove();
        $(this).parent().parent().parent().find("#equipamento_qtde_" + $equipamentoId).remove();
        $(this).parent().parent().remove();
    });
    $('#modalEditTransportador,#modalEditGerador,#modalValidarCTRE').on('click', '.excluir-equipamento-vinculo', function (e) {
        let $equipamentoId = $(this).data('id-equipamento');
        $(this).parent().parent().parent().find("#equipamento_" + $equipamentoId).remove();
        $(this).parent().parent().parent().find("#equipamento_qtde_" + $equipamentoId).remove();
        $(this).parent().parent().remove();
    });

};

function vinculosResiduos() {

    // constroi div conforme residuos forem escolhidos
    let inserirResiduosNaLista = ($residuoId, $residuoText, $residuosEscolhidos) => {
        let dadosTable =
            "<div>" +
            "<div class='col-xs-11'>" +
            "<div class='nome-residuo' data-id-residuo='" + $residuoId + "'>" + $residuoText + "</div>" +
            "</div>" +
            "<div class='col-xs-1'>" +
            "<div id='excluir-residuo-vinculo' class='excluir-residuo-vinculo excluir-ico' data-id-residuo='" + $residuoId + "'>X</div>" +
            "</div>" +
            "</div>";

        let inputForm = "<input type='hidden' name='residuo_escolhido[]' id='residuo_" + $residuoId + "' value='" + $residuoId + "'>";

        $(dadosTable).hide().appendTo($residuosEscolhidos).fadeIn(700);
        $residuosEscolhidos.prepend(inputForm);
    };

    let selecionarItensResiduosForm = ($formName) => {
        let $residuoId = $($formName + " #residuos_vinculo option:selected").val(),
            $residuoText = $($formName + " #residuos_vinculo option:selected").text(),
            $residuosEscolhidos = $($formName + " #residuos-escolhidos");
        inserirResiduosNaLista($residuoId, $residuoText, $residuosEscolhidos);
    };

    // botoes de incluir residuos
    $('.conteudo').on('click', "#incluir_residuos_vinculo", function (e) {
        e.preventDefault();
        selecionarItensResiduosForm("#" + $(this).closest("form").attr('id'));
    });

    // botoes de excluir residuos
    $(".residuos-escolhidos").on('click', '.excluir-residuo-vinculo', function (e) {
        let $residuoId = $(this).data('id-residuo');
        $(this).parent().parent().parent().find("#residuo_" + $residuoId).remove();
        $(this).parent().parent().parent().find("#residuo_qtde_" + $residuoId).remove();
        $(this).parent().parent().remove();
    });
    $('#modalEditTransportador,#modalEditGerador,#modalEditDestino').on('click', '.excluir-residuo-vinculo', function (e) {
        let $residuoId = $(this).data('id-residuo');
        $(this).parent().parent().parent().find("#residuo_" + $residuoId).remove();
        $(this).parent().parent().parent().find("#residuo_qtde_" + $residuoId).remove();
        $(this).parent().parent().remove();
    });

}