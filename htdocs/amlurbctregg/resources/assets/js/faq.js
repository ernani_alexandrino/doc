$(function () {

    if (window.location.href.includes("transportador/faq")) {
        faqTransportador();
    }

});

function faqTransportador() {

    $(".faq-list-li").click(function() {
        let id = $(this).attr("id");
        let data = id.split("--");

        let img = "<img class='img-responsive' src=\"/images/faqs/" + data[1] + "/" + data[2] + ".png\">";
        $("#modalFAQ-body").empty().append(img);
        $("#modalFAQ").modal("show");
    });

}