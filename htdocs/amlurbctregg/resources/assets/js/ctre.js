
$(function () {

    buscaTipoVeiculo();
    totalCtrePorPeriodo();

    emitirCtreTransportador();
    emitirCtreGerador();

    visualizacaoValidacaoCtre();

    $('#modalVisualizarCTRE').on('click', '#btn-close-ctre-vs', function() {
        $('#modalVisualizarCTRE').modal('hide');
    });

    // so busca estes dados se estiver logado como transportador
    if (window.location.href.endsWith('painel/transportador')) {
        atualizaListaCtresEmitidosTransportador();
        atualizaListaCtresRecebidosTransportador();
    }
    // so busca estes dados se estiver logado como gerador
    if (window.location.href.endsWith('painel/gerador')) {
        atualizaListaCtresEmitidosGerador();
        atualizaListaCtresRecebidosGerador();
    }
    // so busca estes dados se estiver logado como destino final
    if (window.location.href.endsWith('painel/destino-final')) {
        atualizaListaCtresRecebidosDestinoFinal();
    }

});

// preenche input do veiculo de acordo com a placa
const getTipoVeiculo = (placa_selecionada, input_tipo) => {
    let url = '/painel/buscar-tipo-veiculo/' + placa_selecionada;
    $.ajax({
        url: url,
        success: function (data) {
            input_tipo.val(data);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
};

const pegaEquipamentosAlocados = (id_empresa) => {
    $.ajax({
        url: "/painel/equipamentos-alocados-to/" + id_empresa,
        success: (equipamentos) => {
            // repopula select com equipamentos da empresa escolhida
            equipamentos.forEach(function (equip) {

                $('#equipamentos_vinculo').append(
                    '<option value="' + equip.id + '">' + equip.codigo_amlurb + ' - ' +
                    equip.equipamento.tipo_equipamento.nome + ' - ' +
                    equip.equipamento.capacidade + '</option>'
                );

            });
        }
    });
};

const pegaResiduosVinculados = (id_empresa) => {
    $.ajax({
        url: "/painel/residuos-vinculados-to/" + id_empresa,
        success: (logistica_residuos) => {
            // repopula select com residuos vinculados à empresa escolhida
            logistica_residuos.forEach(function (lr) {

                $('#residuos_vinculo').append(
                    '<option value="' + lr.residuo.id + '">' +
                    lr.residuo.nome + '</option>'
                );

            });
        }
    });
}

// funcao usada para preencher selects de emissao de CTRE
const acessaEquipamentosResiduos = (id_empresa) => {

    $('#equipamentos_vinculo').prop('disabled', false); // libera campo para alteracao
    $('#residuos_vinculo').prop('disabled', false); // libera campo para alteracao

    $('#equipamentos_vinculo').empty().append('<option value="">Selecione</option>');;
    $('#equipamentos-escolhidos').empty();

    $('#residuos_vinculo').empty().append('<option value="">Selecione</option>');;
    $('#residuos-escolhidos').empty();

    pegaEquipamentosAlocados(id_empresa);
    pegaResiduosVinculados(id_empresa);

};

const visualizarCtre = (url) => {
    $.ajax({
        url: url,
        success: function (modal) {

            $('#modalVisualizarCTRE').empty().append(modal.html);
            $('#modalVisualizarCTRE').modal('show');

        }
    });
};

// emissao de CTRE
function buscaTipoVeiculo() {

    $('#placa_veiculo').change(function () {
        let placa_selecionada = $('#placa_veiculo').val();
        getTipoVeiculo(placa_selecionada, $('#tipo_veiculo'));
    });

    $('#modalValidarCTRE').on('change', ' #vl_placa_veiculo', function () {
        let placa_selecionada = $('#vl_placa_veiculo').val();
        getTipoVeiculo(placa_selecionada, $('#vl_tipo_veiculo'));
    });

}

function totalCtrePorPeriodo() {

    let funcao_ctre_periodo = ($periodoEscolhido, $count) => {
        $.ajax({
            url: "/painel/ctre-por-periodo/" + $periodoEscolhido,
            success: function (data) {

                $('#total_ctre_emitidos').text(data.ctre.emitidos);
                $('#total_ctre_aberto').text(data.ctre.abertos);
                $('#total_ctre_finalizado').text(data.ctre.finalizados);
                $('#total_ctre_expirado').text(data.ctre.expirados);
                $('#total_ctre_expirado_finalizado').text(data.ctre.expirados_finalizados);

                // animacao contagem
                $count.each(function () {
                    $(this).prop('Counter', 0).animate({
                        Counter: $(this).text()
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });

            }

        });
    };

    $("#dt_final_indicador_periodo").change(function (e) {
        e.preventDefault();

        var dataInicial = $('#dt_inicio_indicador_periodo').val(),
            dataFinal = $('#dt_final_indicador_periodo').val(),

            periodoEscolhido = 'custom',
            $totalEmitido = $('#total_ctre_emitido'),
            $totalAberto = $('#total_ctre_aberto'),
            $totalFinalizado = $('#total_ctre_finalizado'),
            $totalExpirado = $('#total_ctre_expirado'),
            url = "/painel/ctre-por-periodo-home-fiscal/",

            $count = $('#indicador_periodo_gerador_fiscal').find('.count');

        var split = dataInicial.split('/');
        dataInicial = split[2] + "-" + split[1] + "-" + split[0];

        split = dataFinal.split('/');
        dataFinal = split[2] + "-" + split[1] + "-" + split[0];

        if (dataInicial != null && dataFinal != null) {

            $totalEmitido.text("0");
            $totalAberto.text("0");
            $totalFinalizado.text("0");
            $totalExpirado.text("0");

            $(this).parents('.nav-indicadores-periodo').find('.active').removeClass();
            $(this).parent().parent().parent().parent().parent().addClass('active');

            $.ajax({
                type: 'GET',
                url: url + periodoEscolhido + "/" + dataInicial + "/" + dataFinal,
                beforeSend: function () {

                },
                success: function (data) {

                    if ($.isEmptyObject(data.error)) {

                        $totalEmitido.text(data.ctre.emitidos);
                        $totalAberto.text(data.ctre.abertos);
                        $totalFinalizado.text(data.ctre.finalizados);
                        $totalExpirado.text(data.ctre.expirados);

                        $count.each(function () {
                            $(this).prop('Counter', 0).animate({
                                Counter: $(this).text()
                            }, {
                                duration: 700,
                                easing: 'swing',
                                step: function (now) {
                                    $(this).text(Math.ceil(now));
                                }
                            });
                        });

                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            })

        } else {
            return false;
        }
    });

    $("#indicador_ctre_periodo .indicadores-ctre-periodo .nav-indicadores-periodo .opcao-indicador-periodo").on('click', function () {

        var periodoEscolhido = $(this).data('periodo'),
            $totalEmitido = $('#total_ctre_emitido'),
            $totalAberto = $('#total_ctre_aberto'),
            $totalFinalizado = $('#total_ctre_finalizado'),
            $totalExpirado = $('#total_ctre_expirado'),
            $dataInicioCustom = $('#dt_inicio_indicador_periodo'),
            $dataFinalCustom = $('#dt_final_indicador_periodo'),
            url = "/painel/ctre-por-periodo-home-fiscal/",

            $count = $('#indicador_ctre_periodo').find('.count');

        $totalEmitido.text("0");
        $totalAberto.text("0");
        $totalFinalizado.text("0");
        $totalExpirado.text("0");
        $dataInicioCustom.text("");
        $dataFinalCustom.text("");

        $(this).parent().parent().find('.active').removeClass();
        $(this).parent().addClass('active');

        $.ajax({
            type: 'GET',
            url: url + periodoEscolhido,
            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    $totalEmitido.text(data.ctre.emitidos);
                    $totalAberto.text(data.ctre.abertos);
                    $totalFinalizado.text(data.ctre.finalizados);
                    $totalExpirado.text(data.ctre.expirados);

                    $count.each(function () {
                        $(this).prop('Counter', 0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 500,
                            easing: 'swing',
                            step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                        });
                    });
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    // transportador // gerador ?
    $("#indicadores-ctre .nav-indicadores-periodo .opcao-indicador-periodo").on('click', function () {

        let $periodoEscolhido = $(this).data('periodo');
        let $count = $('#indicadores-ctre').find('.contador');

        $(this).parent().parent().find('.active').removeClass();
        $(this).parent().addClass('active');

        funcao_ctre_periodo($periodoEscolhido, $count);

    });

    // executa se estiver na pagina inicial do transportador
    if (window.location.href.endsWith('painel/transportador')
        || window.location.href.endsWith('painel/gerador')
        || window.location.href.endsWith('painel/destino-final')
    ) {
        funcao_ctre_periodo('', $('#indicadores-ctre').find('.contador'));
    }

};

function emitirCtreTransportador() {

    let resetarFormCtreTransportador = () => {
        $('#frmCtre')[0].reset();
        $('#gerador').val('').trigger('change');
        $('#destino').val('').trigger('change');
        $('#equipamentos_vinculo').val('').trigger('change');
        $('#residuos_vinculo').val('').trigger('change');
        $('#placa_veiculo').val('').trigger('change');
        $('#equipamentos-escolhidos').empty();
        $('#residuos-escolhidos').empty();
    };

    const apontarErrosFormCtreTransportador = (data) => {
        if ('gerador' in data.error) {
            $('#ctre-tr-gerador p.text-danger').empty().append(data.error['gerador']);
        } else {
            $('#ctre-tr-gerador p.text-danger').empty();
        }

        if ('destino' in data.error) {
            $('#ctre-tr-destino p.text-danger').empty().append(data.error['destino']);
        } else {
            $('#ctre-tr-destino p.text-danger').empty();
        }

        if ('placa_veiculo' in data.error) {
            $('#ctre-tr-placa p.text-danger').empty().append(data.error['placa_veiculo']);
        } else {
            $('#ctre-tr-placa p.text-danger').empty()
        }

        if ('residuos_vinculo' in data.error) {
            $('#ctre-tr-residuos p.text-danger').empty().append(data.error['residuos_vinculo']);
        } else {
            $('#ctre-tr-residuos p.text-danger').empty()
        }
    };

    // mostra modal de CTRE
    $('#btn-new-ctre-tr').click(function () {
        $('#modalNewCTRE').modal('show');
        $('#ctre-success').empty().parent().parent().hide();
    });

    $('#ctre-tr-gerador').on('change', function () {
        acessaEquipamentosResiduos($("#gerador").val());
    });

    // reseta modal de CTRE
    $('#btn-reset-new-ctre-tr').click(function () {
        $('#modalNewCTRE').modal('hide');
        apontarErrosFormCtreTransportador({'error': {}});
        resetarFormCtreTransportador();
    });

    $('#btn-save-new-ctre-tr').click(function (e) {
        e.preventDefault();

        let formDataCtre = {
            _token: $("input[name='_token']").val(),
            gerador: $("#gerador").val(),
            destino: $("#destino").val(),
            placa_veiculo: $("#placa_veiculo").val(),
            equipamentos_vinculo: $('input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            residuos_vinculo: $('input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join()
        };

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'contentType': 'application/json; charset=utf-8'
            },
            url: "/painel/ctre",
            data: formDataCtre,

            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    // resseta formulario, informa mensagem de sucesso
                    // e atualiza listagem
                    $('#ctre-success').empty().append(data['success']).parent().parent().show();
                    $('#modalNewCTRE').modal('hide');

                    $("#table-ctres-emitidos-transportador").DataTable().ajax.reload();
                    resetarFormCtreTransportador();
                }

                apontarErrosFormCtreTransportador(data);

            }

        })

    });

};

function emitirCtreGerador() {

    let resetarFormCtreGerador = () => {
        $('#frmCtre')[0].reset();
        $('#transportador').val('').trigger('change');
        $('#equipamentos_vinculo').val('').trigger('change');
        $('#residuos_vinculo').val('').trigger('change');
        $('#equipamentos-escolhidos').empty();
        $('#residuos-escolhidos').empty();
    };

    const apontarErrosFormCtreGerador = (data) => {
        if ('transportador' in data.error) {
            $('#ctre-gg-transportador p.text-danger').empty().append(data.error['transportador']);
        } else {
            $('#ctre-gg-transportador p.text-danger').empty();
        }

        if ('residuos_vinculo' in data.error) {
            $('#ctre-gg-residuos p.text-danger').empty().append(data.error['residuos_vinculo']);
        } else {
            $('#ctre-gg-residuos p.text-danger').empty()
        }
    };

    // mostra modal de CTRE
    $('#btn-new-ctre-gg').click(function () {
        $('#modalNewCTREGerador').modal('show');
        $('#ctre-success').empty().parent().parent().hide();
    });

    $('#ctre-gg-transportador').on('change', function () {
        acessaEquipamentosResiduos($("#transportador").val());
    });

    // reseta modal de CTRE
    $('#btn-reset-new-ctre-gg').click(function () {
        $('#modalNewCTREGerador').modal('hide');
        apontarErrosFormCtreGerador({'error': {}});
        resetarFormCtreGerador();
    });

    $('#btn-save-new-ctre-gg').click(function (e) {
        e.preventDefault();

        let formDataCtre = {
            _token: $("input[name='_token']").val(),
            transportador: $("#transportador").val(),
            equipamentos_vinculo: $('input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join(),
            residuos_vinculo: $('input[name="residuo_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join()
        };

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'contentType': 'application/json; charset=utf-8'
            },
            url: "/painel/ctre",
            data: formDataCtre,

            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    // resseta formulario, informa mensagem de sucesso
                    // e atualiza listagem
                    $('#ctre-success').empty().append(data['success']).parent().parent().show();
                    $('#modalNewCTREGerador').modal('hide');

                    $("#table-ctres-emitidos-gerador").DataTable().ajax.reload();
                    resetarFormCtreGerador();
                }

                apontarErrosFormCtreGerador(data);

            }

        })

    });

};

function visualizacaoValidacaoCtre() {

    let id_ctre = '';

    // transportador / destino final
    const apontarErrosFormValidarCtre = (data) => {

        if ('destino' in data.error) {
            $('#frmValidarCtre #ctre-vl-tr-destino p.text-danger').empty().append(data.error['destino']);
        } else {
            $('#frmValidarCtre #ctre-vl-tr-destino p.text-danger').empty();
        }

        if ('placa_veiculo' in data.error) {
            $('#frmValidarCtre #ctre-vl-tr-placa p.text-danger').empty().append(data.error['placa_veiculo']);
        } else {
            $('#frmValidarCtre #ctre-vl-tr-placa p.text-danger').empty()
        }

        if ('volume_total' in data.error) {
            $('#frmValidarCtre #ctre-vl-volume-total p.text-danger').empty().append(data.error['volume_total']);
        } else {
            $('#frmValidarCtre #ctre-vl-volume-total p.text-danger').empty()
        }

    };

    // visualizar CTRE
    $("#ctre-tr-list,#ctre-gg-list").on("click", "tr", function() {
        id_ctre = $(this).attr("id");
        visualizarCtre("/painel/ctre/" + id_ctre);
    });

    $("#modalValidarCTRE").on("click", "#btn-close-ctre-vl", function() {
        $("#modalValidarCTRE").modal("hide");
    });

    $("#ctre-gg-tr-list,#ctre-df-tr-list,#ctre-tr-gg-list").on('click', 'tr', function() {
        id_ctre = $(this).attr('id');
        let url = window.location.href.endsWith("painel/transportador") ?
            "/painel/transportador/ctre-gerador/" + id_ctre :
            "/painel/ctre-validar/" + id_ctre;

        // verifica se vai visualizar ou validar CTRE
        if ($(this).find('a.table-status-validar').length !== 0) {

            $.ajax({
                url: url,
                success: function (modal) {

                    $('#modalValidarCTRE').empty().append(modal.html);
                    $('#modalValidarCTRE').modal('show');

                }
            });

        } else {
            visualizarCtre('/painel/ctre/' + id_ctre)
        }

    });

    $('#modalValidarCTRE').on('click', '#btn-validate-ctre', function() {

        let formData = {};
        if (window.location.href.endsWith('painel/transportador')) {
            formData["destino"] = $("#frmValidarCtre #vl-destino").val();
            formData["placa_veiculo"] = $('#frmValidarCtre #vl_placa_veiculo').val();
            formData["equipamentos_vinculo"] = $('#frmValidarCtre input[name="equipamento_escolhido[]"]').map(function () {
                return $(this).val();
            }).get().join();
        }
        if (window.location.href.endsWith('painel/destino-final')) {
            formData["volume_total"] = $("#frmValidarCtre #volume_total").val();
        }

        $.ajax({
            type: 'PATCH',
            headers: getAjaxHeaders(),
            url: '/painel/ctre-validar/' + id_ctre,
            data: formData,
            success: (data) => {

                if ($.isEmptyObject(data.error)) {

                    // informa mensagem de sucesso e atualiza listagem
                    $('#ctre-validate-success').empty().append(data['success']).parent().parent().show();
                    $('#modalValidarCTRE').modal('hide');

                    if (window.location.href.endsWith('painel/gerador')) {
                        $("#table-ctres-recebidos-gerador").DataTable().ajax.reload();
                    }
                    if (window.location.href.endsWith('painel/transportador')) {
                        $("#table-ctres-recebidos-transportador").DataTable().ajax.reload();
                    }
                    if (window.location.href.endsWith('painel/destino-final')) {
                        $("#table-ctres-recebidos-destino").DataTable().ajax.reload();
                    }

                }

                apontarErrosFormValidarCtre(data);

            }

        });

    });

    // invalidacao
    $("#modalValidarCTRE").on("click", "#btn-invalidate-ctre", () => {
        $("#ctre-invalidacao-div").removeAttr("hidden");
    });

    $("#modalValidarCTRE").on("click", "#btn-confirm-invalidate-ctre", () => {

        let justificativa = $("#justificativa-invalidacao-ctre").val().trim();

        if (allEqual(justificativa)) {
            $('#warnings-invalidar-ctre p.text-warning').empty().append("O sistema rejeitou seu texto.");
            return;
        }
        if (justificativa.length < 30) {
            $('#warnings-invalidar-ctre p.text-warning').empty().append("O sistema rejeitou seu texto.");
            return;
        }

        $.ajax({
            type: 'PATCH',
            headers: getAjaxHeaders(),
            url: "/painel/ctre-invalidar/" + id_ctre,
            data: {justificativa: justificativa},
            success: (data) => {

                if ($.isEmptyObject(data.error)) {

                    // informa mensagem de sucesso e atualiza listagem
                    $('#ctre-validate-success').empty().append(data['success']).parent().parent().show();
                    $('#modalValidarCTRE').modal('hide');

                    if (window.location.href.endsWith("painel/gerador")) {
                        $("#table-ctres-recebidos-gerador").DataTable().ajax.reload();
                    }
                    if (window.location.href.endsWith("painel/transportador")) {
                        $("#table-ctres-recebidos-transportador").DataTable().ajax.reload();
                    }
                    if (window.location.href.endsWith("painel/destino-final")) {
                        $("#table-ctres-recebidos-destino").DataTable().ajax.reload();
                    }

                }

            }

        });
    });

}

/// datatables

let atualizaListaCtresEmitidosTransportador = () => {

    $("#table-ctres-emitidos-transportador").DataTable({
        "language": datatableLanguageParams("Não há nenhum CTRE emitido"),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/transportador/ctre-list-emitidos",
        columns: [
            {data: 'data_emissao', name: 'data_emissao', iDataSort: 1},
            {data: 'codigo', name: 'codigo'},
            {data: 'gerador', name: 'gerador'},
            {data: 'destino', name: 'destino'},
            {data: 'status', name: 'status'},
            {data: 'validacao', name: 'validacao'}
        ]
    });

};

let atualizaListaCtresEmitidosGerador = () => {

    $("#table-ctres-emitidos-gerador").DataTable({
        "language": datatableLanguageParams("Não há nenhum CTRE emitido"),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/gerador/ctre-list-emitidos",
        columns: [
            {data: 'data_emissao', name: 'data_emissao', iDataSort: 1},
            {data: 'codigo', name: 'codigo'},
            {data: 'transportador', name: 'transportador'},
            {data: 'total_residuos', name: 'total_residuos'},
            {data: 'status', name: 'status'},
            {data: 'validacao', name: 'validacao'}
        ]
    });

};

let atualizaListaCtresRecebidosTransportador = () => {

    $("#table-ctres-recebidos-transportador").DataTable({
        "language": datatableLanguageParams("Não há nenhum CTRE recebido"),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/transportador/ctre-list-recebidos",
        columns: [
            {data: 'data_emissao', name: 'data_emissao', iDataSort: 1},
            {data: 'codigo', name: 'codigo'},
            {data: 'gerador', name: 'gerador'},
            {data: 'total_residuos', name: 'total_residuos'},
            {data: 'status', name: 'status'},
            {data: 'validacao', name: 'validacao'}
        ]
    });

};

let atualizaListaCtresRecebidosGerador = () => {

    $("#table-ctres-recebidos-gerador").DataTable({
        "language": datatableLanguageParams("Não há nenhum CTRE recebido"),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/gerador/ctre-list-recebidos",
        columns: [
            {data: 'data_emissao', name: 'data_emissao', iDataSort: 1},
            {data: 'codigo', name: 'codigo'},
            {data: 'transportador', name: 'transportador'},
            {data: 'total_equipamentos', name: 'total_equipamentos'},
            {data: 'status', name: 'status'},
            {data: 'validacao', name: 'validacao'}
        ]
    });

};

let atualizaListaCtresRecebidosDestinoFinal = () => {

    $("#table-ctres-recebidos-destino").DataTable({
        "language": datatableLanguageParams("Não há nenhum CTRE recebido"),
        "processing": true,
        "serverSide": true,
        "ajax": "/painel/destino-final/ctre-list-recebidos",
        columns: [
            {data: 'data_emissao', name: 'data_emissao', iDataSort: 1},
            {data: 'codigo', name: 'codigo'},
            {data: 'placa', name: 'placa'},
            {data: 'transportador', name: 'transportador'},
            {data: 'status', name: 'status'},
            {data: 'validacao', name: 'validacao'}
        ]
    });

};