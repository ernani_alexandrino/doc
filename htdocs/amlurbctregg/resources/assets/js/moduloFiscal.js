$(function () {

    aprovaEmpresa();
    abreModalVeiculo();
    revisaEmpresa();
    recusarCadastro();
    reportarInconsistencia();
    rastreabilidadeGerador();
    rastreabilidadeDestinoFinal();
    cadastroUsuario();
    listaEmpresaAprovacao();
    listaEmpresaRevisao();
    abrirDetalhesEmpresaAprovacao();
    abrirDetalhesEmpresaRevisao();
    ativarDesativarEmpresa();
    aprovarDocumento();
    desaprovarDocumento();
    aprovarVeiculo();
    reportarInconsistenciaVeiculo();
    selecionarFiliais();
    cadastroFilial();
    dataTableListaFiliais();

});


/**
 * Responsável pela empresa, também usado para login
 */
function cadastroFilial(){

    //Form de cadastro da empresa
    let $formCadastroFilial = $('#frmCadastroFilial');
    let $containerMensagem = $('.mensagem');

    $('#btn-save-filial', $formCadastroFilial).on('click', function (e) {
        e.preventDefault();

        let $this = $(this);
        // let $containerInput = $('.containerResponsavel');
        let $campos = $formCadastroFilial.find(':input').serialize();     
        let $url = $formCadastroFilial.attr('action');
        let $dados = {
            'url': $url,
            'campos': $campos,
            'this': $this
        };

        return $.ajax({
            url: $dados.url,
            type: 'POST',
            data: $dados.campos,
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]', $formCadastroFilial).val()
            },
            beforeSend: function () {
                $('.form-group').removeClass('error').find('p.text-danger').remove();
                loading(1);
            },
            success: function ($retorno) {
                $containerMensagem.removeClass("hide").find(".alert").html($retorno.mensagem);

                if ($retorno.status) {
                    $('.alert', $containerMensagem).addClass('alert-success');
                } else {
                    $('.alert', $containerMensagem).addClass('alert-danger');
                }

                // $(".modal-footer").hide();

                // loading(0);

                setTimeout(function () {
                    location.reload();
                }, 3000);
            },
            error: function ($retorno) {

                // Erro retornado pelo laravel quando o post não for válido.
                if ($retorno.status === 422) {

                    $.each($retorno.responseJSON.errors, function (campo, mensagem) {

                        let $elemento = $('[name="' + campo + '"]');

                        $elemento.closest(".form-group").addClass("error").append("<p class=\"text-danger\">" + mensagem + "</p>");
                    });
                }
                console.log($retorno);

                // loading(0);
                // window.scrollTo(0, 0);
            }
        });
    });

} // close function cadastroFilial()


function dataTableListaFiliais() {

    var $tabelaFiliaisCadastradas = $('#tabelaFiliaisCadastradas');
    var $empresa_id = $('#empresa_id').val();

    $tabelaFiliaisCadastradas.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há Filiais para serem exibidas.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/lista-filiais/"+$empresa_id,
            pages: 5
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'nome_responsavel', name: 'empresa.nome_responsavel'},
            {data: 'email', name: 'empresa.email'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaFiliaisCadastradas.on('click', 'tbody tr', function (event) {
        
        event.preventDefault();
        $ActiveTabelaFiliaisCadastradas = $('#tabelaFiliaisCadastradas tbody tr');
        var empresaId = $(this).find('.amlurb_id').data('id');

        
        $ActiveTabelaFiliaisCadastradas.addClass('active_table');

        window.location.href = "/painel/fiscal/cadastro-grande-gerador/" + empresaId;
    });

}

function abreModalVeiculo() {
   
    $("#cadastro_vinculos_veiculos_tr").delegate( "tr", "click", function() {
        let $modal = $('#modalAprovaReprovaVeiculo');
        $('.mensagem-validade').addClass("hide");
        var id_veiculo = $(this).find('td:eq(0)').find('div').data('id_veiculo');
        $('#btn-ativar-veiculo').prop('disabled', true);
        $('#btn-ativar-veiculo-sem-boleto').prop('disabled', true);

        $.ajax({
            url: "/painel/fiscal/get/veiculo/"+id_veiculo,
            type: "GET",
            beforeSend: function () {
                loading(1);
                
            },
            success: function ($retorno) {
                $('#id_veiculo').val($retorno.dados_veiculo.id)
                $('#ano_veiculo').val($retorno.dados_veiculo.ano_veiculo)
                $('#placa').val($retorno.dados_veiculo.placa)
                $('#renavam').val($retorno.dados_veiculo.renavam)
                $('#tipo').val($retorno.dados_veiculo.tipo)
                $('#marca').val($retorno.dados_veiculo.marca)
                $('#vencimento_ipva').val($retorno.dados_veiculo.vencimento_ipva)
                $('#capacidade').val($retorno.dados_veiculo.capacidade)
                $('#tara').val($retorno.dados_veiculo.tara)
                $('#numero_inmetro').val($retorno.dados_veiculo.numero_inmetro)
                $link_doc_inmetro = $link_doc_veiculo = $link_doc_comodato = null;
                if ($retorno.dados_veiculo.anexo_documento_inmetro)
                    $link_doc_inmetro = $retorno.dados_veiculo.anexo_documento_inmetro+'/'+$retorno.dados_veiculo.placa;
                if ($retorno.dados_veiculo.anexo_documento_veiculo)
                    $link_doc_veiculo = $retorno.dados_veiculo.anexo_documento_veiculo+'/'+$retorno.dados_veiculo.placa;
                if ($retorno.dados_veiculo.anexo_comodato_veiculo)
                    $link_doc_comodato = $retorno.dados_veiculo.anexo_comodato_veiculo+'/'+$retorno.dados_veiculo.placa;
                $('#ver_anexo_documento_inmetro').attr("href", $link_doc_inmetro)
                $('#ver_anexo_documento_veiculo').attr("href", $link_doc_veiculo)
                $('#ver_anexo_comodato_veiculo').attr("href", $link_doc_comodato)
                if($retorno.dados_veiculo.status_id == '11' || $retorno.dados_veiculo.status_id == '16'){
                    $('#btn-ativar-veiculo').prop('disabled', false);
                    $('#btn-ativar-veiculo-sem-boleto').prop('disabled', false);
                }
                $modal.modal('show');
            },
            error: function ($retorno) {

                loading(0);
            }
        });
        
       
       
    })
}

function aprovarVeiculo() {
   
    $("#modalAprovaReprovaVeiculo").delegate( "#btn-ativar-veiculo", "click", function() {
        var id_veiculo = $('#id_veiculo').val();
        let $modal = $('#modalAprovaReprovaVeiculo');
       $.ajax({
            url: "/painel/fiscal/veiculo/aprovar/"+id_veiculo+'/'+true,
            type: "GET",
            success: function ($retorno) {
                $('.mensagem-validade').removeClass("hide");
                $('#cadastro_vinculos_veiculos_tr').DataTable().ajax.reload();
                
                setTimeout(function(){
                    $('#modalAprovaReprovaVeiculo').modal('hide')
                  }, 2000);
            },
            error: function ($retorno) {
                $('.mensagem-validade').removeClass("hide").find(".alert").removeClass('alert-success').addClass('alert-danger').html('Ocorreu um erro tente novamente!');
             
            }
        });
        
       
       
    });

    $("#modalAprovaReprovaVeiculo").delegate( "#btn-ativar-veiculo-sem-boleto", "click", function() {
        var id_veiculo = $('#id_veiculo').val();
        let $modal = $('#modalDataValidadeVeiculo');
        $modal.modal('show');
        $modal.find('#btn-ativar-veiculo-sem-boleto-salvar').on('click', function(){
           
            let date = $modal.find('#data_validade').val();
            console.log(date);
            if(!date){
                alert('Digite uma data de validade!');
                return
            }
            date = date.split("/").reverse().join("-");
            $.ajax({
                url: "/painel/fiscal/veiculo/aprovar/"+id_veiculo+'/'+false+'/'+date,
                type: "GET",
                success: function ($retorno) {
                    $modal.find('.mensagem-validade').removeClass("hide");
                    $('#cadastro_vinculos_veiculos_tr').DataTable().ajax.reload();

                    setTimeout(function(){
                        $('#modalDataValidadeVeiculo').modal('hide');
                        $('#modalAprovaReprovaVeiculo').modal('hide');
                      }, 2000);
                },
                error: function ($retorno) {
                    $modal.find('.mensagem-validade').removeClass("hide").find(".alert").removeClass('alert-success').addClass('alert-danger').html('Ocorreu um erro tente novamente!');
                 
                }
            });
        });
    });
}

function reportarInconsistenciaVeiculo(){
    $("#modalAprovaReprovaVeiculo").delegate( "#btn-inconsistencia-veiculo", "click", function() {
        let $modal = $('#modalInconsistenciaVeiculo');
        var id_veiculo = $('#id_veiculo').val();
        let containerLastJustificativa = $('.last-justificativa');
         $('.mensagem').addClass("hide");
        
        $.ajax({
            url: "/painel/fiscal/incosistencia/veiculo/"+id_veiculo,
            type: "GET",
            beforeSend: function () {
                loading(1);
                
            },
            success: function (retorno) {
                $modal.find('#id_veiculo').val(id_veiculo);
                if(retorno.status ){
                    containerLastJustificativa.removeClass("hide").find(".alert").text(retorno.mensagem);
                }
               
                $modal.modal('show');
            },
            error: function (retorno) {

                loading(0);
            }
        });

        


        $modal.find('#concluir-inconsistencia-veiculo').on('click', function(){
            let mensagem = $("textarea#descricao", $modal).val();
            let containerMensagem = $('.mensagem');
            $.ajax({
                url: "/painel/fiscal/reportar-inconsistencia-veiculo",
                type: "POST",
                data: {id_veiculo: id_veiculo,  mensagem : mensagem, _token: $('meta[name="csrf-token"]').attr("content")},
                beforeSend: function () {
                    loading(1);
                },
                success: function (retorno) {
                    containerMensagem.removeClass("hide").find(".alert").html(retorno.mensagem);
                    if (retorno.status) {
                        $('.alert', containerMensagem).addClass('alert-success');
                        $("textarea#descricao", $modal).val('');
                        $('#cadastro_vinculos_veiculos_tr').DataTable().ajax.reload();
                        setTimeout(function(){
                            $('#modalInconsistenciaVeiculo').modal('hide');
                            $('#modalAprovaReprovaVeiculo').modal('hide');
                          }, 2000);
                    } else {
                        $('.alert', containerMensagem).addClass('alert-danger');
                    }
    
                },
                error: function (retorno) {
    
                    containerMensagem.removeClass("hide").find(".alert").html(retorno.mensagem);
                    $('.alert', containerMensagem).addClass('alert-danger');
                }
            });

        });
    });

    
}

function rastreabilidadeGerador() {
    $("#rastreabilidade-list-geradores tr").on('click', function (e) {
        e.preventDefault();

        var status = $(this).data('status'),
            imagem = $(this).data('imagem'),
            ctre = $(this).data('ctre'),
            ctreCodigo = $(this).data('ctre-codigo'),
            emissao = $(this).data('emissao'),
            tipoVeiculo = $(this).data('veiculo'),
            placaVeiculo = $(this).data('placa'),
            transportador = $(this).data('transportador'),
            idLimpurb = $(this).data('transportador-limpurb'),
            destino = $(this).data('destino'),
            idLimpurbDestino = $(this).data('destino-limpurb'),
            equipamentos = $(this).data('equipamentos'),
            equipamentosRastreio = '',
            classStatus = '',
            $boxStatus = $("#box_status_ctre");

        if (status == "Em Aberto") {
            classStatus = 'status-aberto';
        } else if (status == "Expirado") {
            classStatus = 'status-expirado';
        } else if (status == "Finalizado") {
            classStatus = 'status-finalizado';
        }

        //Lista dos tipos
        $.each(equipamentos, function (key, val) {
            equipamentosRastreio += '<div class="col-xs-2">' + val.equipamento_quantidade + '</div>' +
                '<div class="col-xs-10">' + val.nome + '</div>';
        });

        $boxStatus.find("#transportador_rastreio").val(transportador);
        $boxStatus.find("#nr_amlurb").val(idLimpurb);
        $boxStatus.find("#destino_final_rastreio").val(destino);
        $boxStatus.find("#nr_amlurb_df").val(idLimpurbDestino);
        $boxStatus.find("#data_emissao").val(emissao);
        $boxStatus.find("#veiculo").val(tipoVeiculo);
        $boxStatus.find("#placa_veiculo_rastreio").val(placaVeiculo);
        $boxStatus.find("#ctre").val(ctreCodigo);
        $boxStatus.find("#img-rastreabilidade").css('background-image', 'url(' + imagem + ')');
        $boxStatus.find("#acondicionamentos-rastreio").html(equipamentosRastreio);
        $boxStatus.find(".status").removeClass().addClass('status ' + classStatus);
        $boxStatus.find(".status").text(status);
        $boxStatus.css('display', 'block');
        $(this).parent().find('.drop-ativo').removeClass();
        $(this).addClass('drop-ativo');

        scrollPage('#box_status_ctre');

    })
}

function rastreabilidadeDestinoFinal() {
    $("#rastreabilidade-list-destinos tr").on('click', function (e) {
        e.preventDefault();

        var status = $(this).data('status'),
            imagem = $(this).data('imagem'),
            ctre = $(this).data('ctre'),
            ctreCodigo = $(this).data('ctre-codigo'),
            emissao = $(this).data('emissao'),
            tipoVeiculo = $(this).data('veiculo'),
            placaVeiculo = $(this).data('placa'),
            transportador = $(this).data('transportador'),
            idLimpurb = $(this).data('transportador-limpurb'),
            equipamentos = $(this).data('equipamentos'),
            equipamentosRastreio = '',
            classStatus = '',
            $boxStatus = $("#box_status_ctre");

        if (status == "Em Aberto") {
            classStatus = 'status-aberto';
        } else if (status == "Expirado") {
            classStatus = 'status-expirado';
        } else if (status == "Finalizado") {
            classStatus = 'status-finalizado';
        }

        //Lista dos tipos
        $.each(equipamentos, function (key, val) {
            equipamentosRastreio += '<div class="col-xs-2">' + val.equipamento_quantidade + '</div>' +
                '<div class="col-xs-10">' + val.nome + '</div>';
        });

        $boxStatus.find("#transportador_rastreio").val(transportador);
        $boxStatus.find("#nr_amlurb").val(idLimpurb);
        $boxStatus.find("#data_emissao").val(emissao);
        $boxStatus.find("#veiculo").val(tipoVeiculo);
        $boxStatus.find("#placa_veiculo_rastreio").val(placaVeiculo);
        $boxStatus.find("#ctre").val(ctreCodigo);
        $boxStatus.find("#img-rastreabilidade").css('background-image', 'url(' + imagem + ')');
        $boxStatus.find("#acondicionamentos-rastreio").html(equipamentosRastreio);
        $boxStatus.find(".status").removeClass().addClass('status ' + classStatus);
        $boxStatus.find(".status").text(status);
        $boxStatus.css('display', 'block');
        $(this).parent().find('.drop-ativo').removeClass();
        $(this).addClass('drop-ativo');

        scrollPage('#box_status_ctre');

    })
}

function aprovaEmpresa() {

    var msgAtivar = 'Tem certeza que deseja prosseguir com a ativação desta empresa?';
    var confirmar = 'Aprovar' ;

    $('#containerDetalhesEmpresa').on('click', '#aprovarCadastro', function () {
        let tipo = $('#tipo_').val();
        let $modal = $('#modalAvisoAprovacaoCadastro');
        // let $btnAprovar = $modal.find(".btnAprovarEmpresa");
        let $btnAprovar = $(".btnAprovarEmpresa");

        $modal.find('.modal-body .msg-header').html(msgAtivar);
        $modal.find('.modal-footer .btn-success').html(confirmar);

        $modal.modal('show');

        $btnAprovar.on("click", function () {
            let $empresaId = $("#empresa").val();
            let $btnSubmit = $("button", $modal);
            let $containerMensagem = $('.mensagem');

            var $dataValidade = '';
            if ($(this).data('validade')){
                $dataValidade = $('#dataValidade').val();
                var element = document.getElementById("alert-text");
                element.classList.remove("hide");
                if(!$dataValidade){
                    let $containerMensagemValidade = $('.mensagem-validade');
                    $containerMensagemValidade.removeClass("hide").find(".alert").html('A Data de Validade não pode fica em branco!');
                    return false;
                } // close if(!$dataValidade)
                else
                    $('#modal-aprovar-sem-boleto').modal('hide');
            } // close if ($(this).data('validade'))

            $.ajax({
                url: "/painel/fiscal/aprovar-cadastro/"+tipo,
                type: "POST",
                data: {empresa: $empresaId, _token: $('meta[name="csrf-token"]').attr("content"), validade: $dataValidade},
                beforeSend: function () {
                    loading(1);
                    $btnSubmit.prop('disabled', true);
                },
                success: function ($retorno) {

                    $containerMensagem.removeClass("hide").find(".alert").html($retorno.mensagem);

                    if ($retorno.status) {
                        $('.alert', $containerMensagem).addClass('alert-success');
                    } else {
                        $('.alert', $containerMensagem).addClass('alert-danger');
                    }

                    $(".modal-footer").hide();

                    loading(0);

                    setTimeout(function () {
                        location.reload();
                    }, 3000);

                },
                error: function ($retorno) {

                    if ($retorno.responseJSON.errors) {

                        $.each($retorno.responseJSON.errors, function (campo, mensagem) {

                            let $elemento = $('[name="' + campo + '"]');

                            $elemento.closest(".form-group").addClass("error").append("<p class=\"text-danger\">" + mensagem + "</p>");
                        });
                    }

                    $btnSubmit.prop('disabled', false);

                    loading(0);
                }
            });

        });

    });
}

function revisaEmpresa() {

    $('#containerDetalhesEmpresa').on('click', '#revisarCadastro', function () {

        let $modal = $('#modalAvisoRevisaoCadastro');
        let $btnRevisar = $(".btnRevisarEmpresa");

        $modal.modal('show');

        $btnRevisar.on("click", function () {

            let $empresaId = $("#empresa").val();
            let $btnSubmit = $("button", $modal);
            let $containerMensagem = $('.mensagem');

            $.ajax({
                url: "/painel/fiscal/aprovar-cadastro/revisao",
                type: "POST",
                data: {empresa: $empresaId, _token: $('meta[name="csrf-token"]').attr("content")},
                beforeSend: function () {
                    loading(1);
                    $btnSubmit.prop('disabled', true);
                },
                success: function ($retorno) {

                    $containerMensagem.removeClass("hide").find(".alert").html($retorno.mensagem);

                    if ($retorno.status) {
                        $('.alert', $containerMensagem).addClass('alert-success');
                    } else {
                        $('.alert', $containerMensagem).addClass('alert-danger');
                    }

                    $(".modal-footer").hide();

                    loading(0);

                    setTimeout(function () {
                        location.reload();
                    }, 3000);

                },
                error: function ($retorno) {

                    if ($retorno.responseJSON.errors) {

                        $.each($retorno.responseJSON.errors, function (campo, mensagem) {

                            let $elemento = $('[name="' + campo + '"]');

                            $elemento.closest(".form-group").addClass("error").append("<p class=\"text-danger\">" + mensagem + "</p>");
                        });
                    }

                    $btnSubmit.prop('disabled', false);

                    loading(0);
                }
            });

        });

    });
}

function recusarCadastro() {
    var msgRecusar = 'Tem certeza que deseja recusar e definir a empresa como Grande Gerador?';
    var confirmar = 'Confirmar';
    $('#containerDetalhesEmpresa').on('click', '#recusarCadastro', function (e) {
        e.preventDefault(); 
  

        let $modal = $('#modalAvisoAprovacaoCadastro');
        let $btnAprovar = $modal.find(".btnAprovarEmpresa");

        $modal.find('.modal-body .msg-header').html(msgRecusar);
        $modal.find('.modal-footer .btn-success').html(confirmar);
        $modal.modal('show');

        let $empresaId = $("#empresa").val();
        let $btnSubmit = $("button", $modal);
        let $containerMensagem = $('.mensagem');

        $btnAprovar.on("click", function () {
            $.ajax({
                url: "/painel/fiscal/recusar-cadastro",
                type: "POST",
                data: {grandeGerador: true,empresa: $empresaId, _token: $('meta[name="csrf-token"]').attr("content")},
                beforeSend: function () {
                    loading(1);
                    $btnSubmit.prop('disabled', true);
                },
                success: function ($retorno) {

                    $containerMensagem.removeClass("hide").find(".alert").html($retorno.mensagem);

                    if ($retorno.status) {
                        $('.alert', $containerMensagem).addClass('alert-success');
                    } else {
                        $('.alert', $containerMensagem).addClass('alert-danger');
                    }

                    $(".modal-footer").hide();

                    loading(0);

                    setTimeout(function () {
                        location.reload();
                    }, 3000);

                },
                error: function ($retorno) {

                    if ($retorno.responseJSON.errors) {

                        $.each($retorno.responseJSON.errors, function (campo, mensagem) {

                            let $elemento = $('[name="' + campo + '"]');

                            $elemento.closest(".form-group").addClass("error").append("<p class=\"text-danger\">" + mensagem + "</p>");
                        });
                    }

                    $btnSubmit.prop('disabled', false);

                    loading(0);
                }
            });
        });
    });
}

function reportarInconsistencia(){
    $('#containerDetalhesEmpresa').on('click', '#reportarInconsistencia', function (e) {
        e.preventDefault();

        let $modal = $('#modal-justufiq-grande-gerador');

        let $empresaId = $("#empresa").val();
        let $btnSubmit = $("button", $modal);
        let $containerMensagem = $('.mensagem');
        let $mensagem = $("textarea#reportar_inconsistencia", $modal).val();

        $.ajax({
            url: "/painel/fiscal/reportar-inconsistencia",
            type: "POST",
            data: {empresa: $empresaId,  mensagem : $mensagem, _token: $('meta[name="csrf-token"]').attr("content")},
            beforeSend: function () {
                loading(1);
                $btnSubmit.prop('disabled', true);
            },
            success: function ($retorno) {
                console.log('concluiou AJAX');
                $containerMensagem.removeClass("hide").find(".alert").html($retorno.mensagem);

                if ($retorno.status) {
                    $('.alert', $containerMensagem).addClass('alert-success');
                } else {
                    $('.alert', $containerMensagem).addClass('alert-danger');
                }

                $(".modal-footer").hide();

                loading(0);

                setTimeout(function () {
                    location.reload();
                }, 3000);

            },
            error: function ($retorno) {

                if ($retorno.responseJSON.errors) {

                    $.each($retorno.responseJSON.errors, function (campo, mensagem) {

                        let $elemento = $('[name="' + campo + '"]');

                        $elemento.closest(".form-group").addClass("error").append("<p class=\"text-danger\">" + mensagem + "</p>");
                    });
                }

                $btnSubmit.prop('disabled', false);

                loading(0);
            }
        });

    });
}

function cadastroUsuario() {

    $('#formCadastrarUsuario').on('submit', function (e) {
        e.preventDefault();

        var $this = $(this),
            $url = $(this).attr('action'),
            $dados = new FormData(this),
            $loading = $('#loadingModal');

        $.ajax({
            url: $url,
            type: "POST",
            data: $dados,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $loading.addClass('ativo');
                $('p.text-danger', $this).remove();
                $('.form-group', $this).removeClass('error');
            },
            success: function ($retorno) {

                if ($retorno.status) {
                    window.location = $retorno.redirect;
                }

            },
            error: function ($retorno) {

                $loading.removeClass('ativo');

                // Erro retornado pelo laravel quando o post não for válido.
                if ($retorno.status === 422) {

                    $.each($retorno.responseJSON.errors, function (key, value) {

                        var $elemento = $('input[name="' + key + '"]');

                        $elemento.parents('.form-group').addClass('error').append("<p class=\"text-danger\">" + value + "</p>");
                    });
                }
            }
        });

        return false;
    });

}

function listaEmpresaAprovacao() {

    let $tabelaEmpresas = $("#listaAprovacaoEmpresas");
    let $tipoEmpresa = $tabelaEmpresas.data("tipo");

    $tabelaEmpresas.DataTable({
        ajax: "/painel/fiscal/lista-empresas-aprovacao/" + $tipoEmpresa,
        processing: true,
        serverSide: true,
        bLengthChange: false,
        searching: true,
        ordering: true,
        columns: [
            {data: 'cnpj', name: 'cnpj'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'data', name: 'data', iDataSort: 1},
            {data: 'status', name: 'status', orderable: false},
        ],
        language: {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Nada foi encontrado!",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        createdRow: function (row, data, index) {
           
            $(row).addClass('linha').attr('data-empresa', data.id);
            // o if abaixo foi inserido por Mr.Goose em 21 maio 19 afim de tratar a exceção de Órgão Público que pode ou não gerar boleto
            if ($tipoEmpresa == 'orgao-publico')
                $(row).attr('data-tipo', data['empresa_informacao_complementar'].tipo_ramo_atividade_id);
        }
    });
}

function listaEmpresaRevisao() {

    let $tabelaEmpresas = $("#listaRevisaoEmpresas");
    let $tipoEmpresa = $tabelaEmpresas.data("tipo");

    $tabelaEmpresas.DataTable({
        ajax: "/painel/fiscal/lista-empresas-revisao/" + $tipoEmpresa,
        processing: true,
        serverSide: true,
        bLengthChange: false,
        searching: true,
        ordering: true,
        columns: [
            {data: 'cnpj', name: 'cnpj'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'data', name: 'data', iDataSort: 1},
            {data: 'status', name: 'status', orderable: false},
        ],
        language: {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Nada foi encontrado!",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        createdRow: function (row, data, index) {
            $(row).addClass('linha').attr('data-empresa', data.id);
        }
    });
}

function abrirDetalhesEmpresaAprovacao() {

    $('#listaAprovacaoEmpresas').on('click', '.linha', function (e) {

        var $this = $(this),
            $idEmpresa = $this.data('empresa'),
            $containerDetalhesEmpresa = $('#containerDetalhesEmpresa');
            if ($this.data('tipo')) {
                // se for Órgão Público entra aqui.
                if ($this.data('tipo') == '53' || $this.data('tipo') == '54') {
                    // municipal - não gera boleto
                    $('#modalAvisoAprovacaoCadastro .modal-footer').html(
                        '<button type="button" class="btn btn-success btnAprovarEmpresa">Aprovar</button>'+
                        '<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>'
                    );
                } // se municipal
                else{
                    // gera boleto
                    $('#modalAvisoAprovacaoCadastro .modal-footer').html(
                        '<button type="button" class="btn bt-bg-color-green" type="button" data-toggle="modal" data-target="#modal-aprovar-sem-boleto">Aprovar e Não Gerar Boleto</button>'+
                        '<button type="button" class="btn bt-bg-color-green btnAprovarEmpresa">Aprovar e Gerar Boleto</button>'+
                        '<button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>'
                    );                
                } // se estadual ou federal
            } // se for Órgão Público

        $.ajax({
            url: window.baseUrl + '/painel/fiscal/detalhes-empresa',
            data: {'empresa': $idEmpresa},
            beforeSend: function () {
                loading(1);
                $containerDetalhesEmpresa.html('');
            },
            success: function ($retorno) {

                if ($retorno.status) {
                    $containerDetalhesEmpresa.html($retorno.view);
                }

                loading();
            }
        });
    });
}

function abrirDetalhesEmpresaRevisao() {

    $('#listaRevisaoEmpresas').on('click', '.linha', function (e) {

        var $this = $(this),
            $idEmpresa = $this.data('empresa'),
            $containerDetalhesEmpresa = $('#containerDetalhesEmpresa');

        $.ajax({
            url: window.baseUrl + '/painel/fiscal/detalhes-empresa-revisao',
            data: {'empresa': $idEmpresa},
            beforeSend: function () {
                loading(1);
                $containerDetalhesEmpresa.html('');
            },
            success: function ($retorno) {

                if ($retorno.status) {
                    $containerDetalhesEmpresa.html($retorno.view);
                }

                loading();
            }
        });
    });
}

function ativarDesativarEmpresa() {

    let $form = $('#formAtivarDesativarCadastro');
    let $empresa = 0;

    $("#containerDetalhesEmpresa").on("click", "#btnAtivarEmpresa", function () {

        let $modal = $('#modalAtivacaoDesativacao');
        $empresa = $('#empresa').val();

        $modal.modal('show');
    });

    $('.fiscal-cadastros').on('click', 'tbody tr #lnkAtivarDesativarEmpresa', function (event) {
        event.stopPropagation();

        let $modal = $('#modalAtivacaoDesativacao');

        $empresa = $(this).data('id');

        $.ajax({
            url: "/painel/fiscal/empresa/show/" + $empresa,
            type: "GET",
            success: function ($retorno) {
                $empresa_titulo = formata_cpf_cnpj($retorno.cnpj) + ' - ' + $retorno.razao_social;
                $('#titulo_empresa').text($empresa_titulo);
            },
        });

        $('.modal-title').text($(this).attr('title'));

        $modal.modal('show');
    });

    $form.on("submit", function () {

        let $dados = $form.serialize();
        let $containerMensagem = $('.mensagem');
        
        $.ajax({
            url: $form.attr("action") + "/" + $empresa,
            type: "POST",
            data: $dados,
            beforeSend: function () {
                $('.form-group').removeClass('error').find('p.text-danger').remove();
                $("button[type=submit]", $form).prop('disabled', true);
            },
            success: function ($retorno) {

                $containerMensagem.removeClass("hide").find(".alert").html($retorno.mensagem);

                if ($retorno.status) {
                    $('.alert', $containerMensagem).addClass('alert-success');
                } else {
                    $('.alert', $containerMensagem).addClass('alert-danger');
                }

                $form.addClass("hide");

                setTimeout(function () {
                    location.reload();
                }, 3000);

            },
            error: function ($retorno) {

                if ($retorno.responseJSON.errors) {

                    $.each($retorno.responseJSON.errors, function (campo, mensagem) {

                        let $elemento = $('[name="' + campo + '"]');

                        $elemento.closest(".form-group").addClass("error").append("<p class=\"text-danger\">" + mensagem + "</p>");
                    });
                }

                $("button[type=submit]", $form).prop('disabled', false);
            }
        });

        return false;
    });

    $('#modalAtivacaoDesativacao').on('hidden.bs.modal', function () {

        $('#formAtivarDesativarCadastro')[0].reset();
        $('.form-group').removeClass('error').find('p.text-danger').remove();
        $('.mensagem').addClass("hide").find(".alert").removeClass("alert-danger alert-success").html("");

    });
}

function aprovarDocumento() {

    $('#containerDetalhesEmpresa').on('click', '.aprovarDocumento', function () {

        let $tipoDocumento = $(this).data('tipo');
        let $empresa = $('#empresa').val();

        alert('Aprovar \n Tipo de documento: ' + $tipoDocumento + '\n Empresa: ' + $empresa);

    });
}

function desaprovarDocumento() {

    $('#containerDetalhesEmpresa').on('click', '.desaprovarDocumento', function () {

        let $tipoDocumento = $(this).data('tipo');
        let $empresa = $('#empresa').val();

        alert('Desaprovar \n Tipo de documento: ' + $tipoDocumento + '\n Empresa: ' + $empresa);

    });
}

function selecionarFiliais() {
    
    $("#divBtnVoltarParaMatriz").hide();

    $('#tabelaFiliaisCadastradas .cadastroFilial').click(function () {
        $("#tableListaFiliaisCadastradas").hide('slow');    
        $("#divBtnVoltarParaMatriz").show('slow');          
    });

    $('#btnVoltarParaMatriz').click(function () {
        $("#divBtnVoltarParaMatriz").hide('slow');  
        $("#tableListaFiliaisCadastradas").show('slow');
    });

}
