$(function () {
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    /* MENU LATERAL */
    let itemMenuAtivo = $("a#m_" + window.routeClass);
    let paiMenuAtivo = itemMenuAtivo.parents(".dropdown");

    itemMenuAtivo.addClass("drop-ativo");

    if (paiMenuAtivo.length !== 0) {
        paiMenuAtivo.addClass('open');
    }
    /* --- */
   

    $('.dados-empresa, .documentosEmpresa, .informacoesEmpresa').find('input,select').change(function(){
        $(this).attr('name', $(this).attr('name') + '_alterado');
    });
  
    $('#button_fechar_modal, #close_fechar_modal, #close_cnpj_existente').click(function (e) {
        e.preventDefault();

        window.location = window.baseUrl + '/login';
    });

    $("#finalizar_cadastro_gc").click(function (event) {
        event.preventDefault();
        let $tipoCadastro = $(this).data("tipocadastro");
        let url = "/empresa/termos-de-uso/" + window.urlSegment3 + '/' + $tipoCadastro;

        window.location = window.baseUrl + url;

    });

    let cartaoCnpjTeste = $('#cartao_cnpj');

    cartaoCnpjTeste.change(function () {
        window.localStorage.setItem('cartaoCnpj', $(this).val());
    });

    funcoesUsuarioCadastro();
    funcoesEmpresaCadastro();
    montarTipoAtividade();
    totalCadastrosPorPeriodo();
    graficoPainelFiscal();
    elementosChosen();
    elementosTableSorter();
    totalCtreGeradoresPorPeriodo();
    hiddenTipoEstabelecimentoCondominio();
    InputMask();
    buscaCep();

    // Busca cadastro geradores
    $("#busca-cad").on("click", ".input-group-btn .dropdown-menu a", function () {
        $(this).parents('.dropdown-menu').find('.active-search').removeClass('active-search');
        $(this).addClass('active-search');
    });

    var callback = function () {
        var input = $("#busca-cad").find('.active-search').data('search'),
            inputValor = $("#input-search").val(),
            routeClass = window.routeClass,
            urlBusca = '';

        if (!input) {
            input = "todos";
        }

        if (routeClass == 'cadastro_gg_fs' || routeClass == 'cadastro_gg_search_fs') {
            urlBusca = "/painel/fiscal/cadastro-gg-search/";
        } else if (routeClass == 'cadastro_gc_fs' || routeClass == 'cadastro_gc_search_fs') {
            urlBusca = "/painel/fiscal/cadastro-gc-search/";
        } else if (routeClass == 'cadastro_pg_fs' || routeClass == 'cadastro_pg_search_fs') {
            urlBusca = "/painel/fiscal/cadastro-pg-search/";
        } else if (routeClass == 'cadastro_tr_fs' || routeClass == 'cadastro_tr_search_fs') {
            urlBusca = "/painel/fiscal/cadastro-tr-search/";
        } else if (routeClass == 'cadastro_df_fs' || routeClass == 'cadastro_df_search_fs') {
            urlBusca = "/painel/fiscal/cadastro-df-search/";
        } else if (routeClass == 'cadastro_cr_fs' || routeClass == 'cadastro_cr_search_fs') {
            urlBusca = "/painel/fiscal/cadastro-cr-search/";
        } else if (routeClass == 'cadastro_ss_fs' || routeClass == 'cadastro_ss_search_fs') {
            urlBusca = "/painel/fiscal/cadastro-ss-search/";
        } else if (routeClass == 'cadastro_op_fs' || routeClass == 'cadastro_op_search_fs') {
            urlBusca = "/painel/fiscal/cadastro-op-search/";
        }

        inputValor = inputValor.replace("/", "*");

        window.location = window.baseUrl + urlBusca + input + "/" + inputValor;
    };

    $("#buscar").click(callback);

    $('#busca-cad').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            $('#buscar').click();//Trigger search button click event
        }
    });    

    $('#formCadastroEmpresa:input').keypress(function (e) {
        if (e.which === 13) callback();
    });

    // carrega evento de montar select de cidades quando select de estado for alterado
    //$('#estado', '.containerPrimeiraEtapaContinuacao').change(function () {
   $('#formCadastroEmpresa').on('change', '#estado', function(e){
        
        e.preventDefault();
        
        let $idEstado = $(this).val();
        
        $.getJSON("/cidade/" + $idEstado, function (data) {
            let $selectCidades = $("#cidade");
            let $optionsTipo = "<option selected disabled>Selecione uma cidade</option>";

            //Lista dos tipos
            $.each(data, function (idCidade, nomeCidade) {
                $optionsTipo += "<option value=\"" + idCidade + "\">" + nomeCidade + "</option>";
            });

            $selectCidades.empty().html($optionsTipo).select2({
                language: "pt-BR",
                placeholder: "Selecione uma cidade",
                no_results_text: "Nenhum resultado encontrado!",
                search_contains: true,
                width: "100%"
            }).trigger("change");

        });
    //});    
    })

});

function verificaAlteracaoEmpresaCadastro() {
    
    if (
        $.trim($('#ramo_atividade').val()).length > 0 &&
        $.trim($('#tipo_ramo_atividade').val()).length > 0 &&
        $.trim($('#endereco').val()).length > 0 &&
        $.trim($('#numero').val()).length > 0 &&
        $.trim($('#bairro').val()).length > 0 &&
        $.trim($('#cep').val()).length > 0 &&
        $.trim($('#estado').val()).length > 0 &&
        $.trim($('#cidade').val()).length > 0
        )
    {
        // $("#btn_edit_empresa").prop("disabled", false);
      
    }
    else {
       // $("#btn_edit_empresa").prop("disabled", true);
    }
};

function funcoesEmpresaCadastro() {
    
    $('#ramo_atividade, #tipo_ramo_atividade, #endereco, #numero, #bairro, #cep, #estado, #cidade, #complemento, #ponto_referencia, #im, #ie, #telefone, #empresa_cartao_cnpj, #empresa_num_iptu, #empresa_iptu').change(verificaAlteracaoEmpresaCadastro);

    // mostra modal de solicitação de alteração de dados da empresa
    $(".btn_edit_cadastro").on("click", function (e) {
        e.stopPropagation(); // protege evento de vazar para elementos filhos
        var idTipoEmpresa = $('#tipoEmpresa').val();
        
        //se o tipo da empresa for GG checa alguns documentos
        if(idTipoEmpresa == 1){
            var errors = false;
            if($('#endereco').val() == ''){
                $('#update-endereco p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }
            if($('#numero').val() == ''){
                $('#update-numero p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }
            if($('#bairro').val() == ''){
                $('#update-bairro p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }
        
            if($('#cep').val() == ''){
                $('#update-cep p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }
            if($('#estado').val() == ''){
                $('#update-estado p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }
            if($('#cidade').val() == ''){
                $('#update-cidade p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }
            if($('#im').val() == ''){
                $('#update-im p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }

            if($('#empresa_num_iptu').val() == ''){
                $('#update-empresa_num_iptu p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }

            if($('#empresa_cartao_cnpj_enviado').val() == ''){
                if (!$('#documento_cnpj').length){
                    $('#update-empresa_cartao_cnpj p.text-danger').empty().append('Campo obrigatório!');
                    errors = true;
                }
            }

            if($('#empresa_iptu_enviado').val() == ''){
                if (!$('#documento_iptu').length){
                    $('#update-empresa_cartao_cnpj p.text-danger').empty().append('Campo obrigatório!');
                    errors = true;
                }
            }

            if($('#frequencia_geracao').val() == ''){
                $('#update-frequencia_geracao p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }

            if($('#frequencia_coleta').val() == ''){
                $('#update-frequencia_coleta p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }

            if($('#colaboradores_numero').val() == ''){
                $('#update-colaboradores_numero p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }

            if($('#energia_consumo').val() == ''){
                $('#update-energia_consumo p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }

            if($('#estabelecimento_tipo').val() == ''){
                $('#update-estabelecimento_tipo p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }

            if($('#area_total').val() == '' || $('#area_total').val() <= 0){
                $('#update-area_total p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }

            if($('#area_construida').val() == '' || $('#area_construida').val() <= 0){
                $('#update-area_construida p.text-danger').empty().append('Campo obrigatório!');
                errors = true;
            }



            if (errors == true) {
                return;
            }
        }

        var statusEmpresa = $('#statusEmpresa').val();
        if (statusEmpresa === '19') {
            salvaAteracoesCadastro(true);
            scrollPage('#painel-gerador');
            return;
        }
        $("#justificativa-solicitacao-alteracao-empresa").val('');
        $('#warnings-solicitacao-alteracao-empresa p.text-warning').empty();
        $('#modalSolicitacaoAlteracaoEmpresa').modal('show');
    });

    $("#testeValida").on("click", function (e) {
        e.stopPropagation(); // protege evento de vazar para elementos filhos
        $('#modalAvisoBotaoAlteracao').modal('show');
    });
    

    
    // atualizar dados da empresa
    $("#btn-confirm-solicitacao-alteracao-empresa").on("click", function () {
        salvaAteracoesCadastro(false);
    });

    $("#btn-cancel-solicitacao-alteracao-empresa").click(() => {
        $("#modalSolicitacaoAlteracaoEmpresa").modal("hide");
    });    
}

let apontarErrosFormEditEmpresa = (data) => {
    $('p.text-danger').empty();
    if ("ramo_atividade_id" in data.error) $('#update-ramo_atividade p.text-danger').empty().append(data.error["ramo_atividade_id"]);
    if ("tipo_ramo_atividade_id" in data.error) $('#update-tipo_ramo_atividade p.text-danger').empty().append(data.error["tipo_ramo_atividade_id"]);
    if ("endereco" in data.error) $('#update-endereco p.text-danger').empty().append(data.error["endereco"]);
    if ("numero" in data.error) $('#update-numero p.text-danger').empty().append(data.error["numero"]);
    if ("bairro" in data.error) $('#update-bairro p.text-danger').empty().append(data.error["bairro"]);
    if ("cep" in data.error) $('#update-cep p.text-danger').empty().append(data.error["cep"]);
    if ("estado_id" in data.error) $('#update-estado p.text-danger').empty().append(data.error["estado_id"]);
    if ("cidade_id" in data.error) $('#update-cidade p.text-danger').empty().append(data.error["cidade_id"]);
};

function salvaAteracoesCadastro(filial){

    if (!filial || filial == 'undefined' || filial == undefined || filial == '' || filial == false) {
        var justificativa = $("#justificativa-solicitacao-alteracao-empresa").val().trim();

        $('#warnings-solicitacao-alteracao-empresa p.text-warning').empty();

        if (justificativa == '') {
            $('#warnings-solicitacao-alteracao-empresa p.text-warning').empty().append("O texto da justificativa é obrigatório.");
            return;
        }
        if (allEqual(justificativa)) {
            $('#warnings-solicitacao-alteracao-empresa p.text-warning').empty().append("O texto da justificativa não podem conter caracteres repetidos em sequência.");
            return;
        }
        if (justificativa.length < 30) {
            $('#warnings-solicitacao-alteracao-empresa p.text-warning').empty().append("O texto da justificativa deve ser maior que 30 caracteres.");
            return;
        }
    } // close if (!filial)
    else{
        var justificativa = "Pré Cadastro de Filial.";
    }

    var empresa_alteracao = $('#dados_empresa_form').serialize();
   /* empresa_alteracao[$("#ramo_atividade").attr('name')] = $("#ramo_atividade option:selected").val();
    empresa_alteracao[$("#tipo_ramo_atividade").attr('name')] = $("#tipo_ramo_atividade option:selected").val();
    empresa_alteracao[$("#razao_social").attr('name')] = $("#razao_social").val();
    empresa_alteracao[$("#nome_fantasia").attr('name')] = $("#nome_fantasia").val();
    empresa_alteracao[$("#endereco").attr('name')] = $("#endereco").val();
    empresa_alteracao[$("#numero").attr('name')] = $("#numero").val();
    empresa_alteracao[$("#bairro").attr('name')] = $("#bairro").val();
    empresa_alteracao[$("#cep").attr('name')] = $("#cep").val();
    empresa_alteracao[$("#estado").attr('name')] = $("#estado option:selected").val();
    empresa_alteracao[$("#cidade").attr('name')] = $("#cidade option:selected").val();
    empresa_alteracao[$("#complemento").attr('name')] = $("#complemento").val();
    empresa_alteracao[$("#ponto_referencia").attr('name')] = $("#ponto_referencia").val();
    empresa_alteracao[$("#im").attr('name')] = $("#im").val();
    empresa_alteracao[$("#ie").attr('name')] = $("#ie").val();
    empresa_alteracao[$("#telefone_empresa").attr('name')] = $("#telefone_empresa").val();
    empresa_alteracao[$("#empresa_cartao_cnpj").attr('name')] = $("#empresa_cartao_cnpj").val();
    empresa_alteracao[$("#empresa_num_iptu").attr('name')] = $("#empresa_num_iptu").val();
    empresa_alteracao[$("#empresa_iptu").attr('name')] = $("#empresa_iptu").val();

    //dados texto/datas do transportador
    if (document.getElementById('transportador_anotacao_responsabilidade_tecnica_emissao')) {
        empresa_alteracao[$("#transportador_anotacao_responsabilidade_tecnica_emissao").attr('name')] = $("#transportador_anotacao_responsabilidade_tecnica_emissao").val();
        empresa_alteracao[$("#transportador_anotacao_responsabilidade_tecnica_vencimento").attr('name')] = $("#transportador_anotacao_responsabilidade_tecnica_vencimento").val();

        empresa_alteracao[$("#rg_reponsavel_tecnico").attr('name')] = $("#rg_reponsavel_tecnico").val();
        empresa_alteracao[$("#nome_reponsavel_tecnico").attr('name')] = $("#nome_reponsavel_tecnico").val();
        empresa_alteracao[$("#cpf_responsavel_tecnico").attr('name')] = $("#cpf_responsavel_tecnico").val();
        empresa_alteracao[$("#transportador_certificado_dispensa_licenca_emissao").attr('name')] = $("#transportador_certificado_dispensa_licenca_emissao").val();

        empresa_alteracao[$("#transportador_certificado_dispensa_licenca_vencimento").attr('name')] = $("#transportador_certificado_dispensa_licenca_vencimento").val();
    
    }
    */
    
    let justificativas = {
        justificativa: justificativa,
    };
    
    $.ajax({
        type: 'post',
        headers: getAjaxHeaders(),
        url: '/painel/empresa/solicitar-alteracao',
        data: {
            empresa_alteracao: empresa_alteracao,
            justificativas: justificativas
        },             
        success: (data) => {

            $('#modalSolicitacaoAlteracaoEmpresa').modal('hide');
            
            if ($.isEmptyObject(data.error)) {
                // informa mensagem de sucesso
                $('#update-success').empty().append(data['success']).parent().parent().show();
                $("#btn_edit_empresa").prop("disabled", true);
             }

            apontarErrosFormEditEmpresa(data);

        },
        error: function (data) {
            console.log('Error:', data);
        }            
    });
}

function funcoesUsuarioCadastro() {

    let apontarErrosFormEditUsuario = (data) => {
        $('p.text-danger').empty();
        if ("nome" in data.error) $('#update-nome p.text-danger').empty().append(data.error["nome"]);
        if ("email" in data.error) $('#update-email p.text-danger').empty().append(data.error["email"]);
        if ("password" in data.error) $('#update-password p.text-danger').empty().append(data.error["password"]);
    };

    // atualizar dados de usuario
    $("#btn_edit_usuario").on("click", function () {

        let pass1 = $("#password1").val(),
            pass2 = $("#password2").val();

        // verifica se usuario pretende alterar senha
        if (pass1.length > 0 || pass2.length > 0) {

            if (pass1 != pass2) {
                apontarErrosFormEditUsuario({
                    "error": {
                        password: "As senhas informadas não estão iguais. Por favor redigite."
                    }
                });
                return;
            }

        }

        let formData = {
            nome: $("#nome-responsavel").val(),
            email: $("#email-responsavel").val(),
            cargo: $("#cargo").val(),
            celular: $("#celular").val(),
            telefone_responsavel: $("#telefone_responsavel").val(),
            ramal: $("#ramal").val(),
            password: pass1
        };

        $.ajax({
            type: 'PATCH',
            headers: getAjaxHeaders(),
            url: "/painel/usuario",
            data: formData,
            success: (data) => {

                if ($.isEmptyObject(data.error)) {
                    // informa mensagem de sucesso
                    $('#update-success').empty().append(data['success']).parent().parent().show();
                    $('#password1').val(''); $('#password2').val('');
                }

                apontarErrosFormEditUsuario(data);

            }
        });

    });

}

/*--
+ Funcão montaTipoAtividade By Mr.Goose
+ 17 abr 19
+ Afim de
    - ser utilizada pela Função já existente montarTipoAtividade() e
    - ser utilizada na Edição do Cadasro pela Empresa
+ idTipo = id do Ramo de Atividade enviado pelo change ou ao carregar a página
+ idSelect = id do Tipo de Atividade enviado ao carregar a página
*/
function montaTipoAtividade(idTipo, idSelect) {
    if(idSelect == undefined || idSelect == '' || idSelect == null)
        idSelect = idTipo;

    /*-/ Abaixo, inserido By Mr.Goose em 11 abr 19, afim de tratar o Caso Especial de Cooperativas /-*/
    if (idTipo === '4') {
        // Entra aqui se FOR Coo (Coopertativa)
        $('.remove-required').removeClass('required');
        $('.btnValidaTransportador').addClass('btnValidaDocsCoo');
    }
    else{
        // Entra aqui se NÃO for Coo (Cooperativa) ou seja é QUALQUER OUTRO Ramo De Atividade.
        $('.remove-required').addClass('required');
        $('.btnValidaTransportador').removeClass('btnValidaDocsCoo');
    }
    /*-/ Acima, inserido By Mr.Goose em 11 abr 19, afim de tratar o Caso Especial de Cooperativas /-*/

    let $idRamo = idTipo,
        url = "/buscar-tipo-atividade/";

    $.getJSON(url + $idRamo, function (data) {
        
        let $selectTipoRamoAtividade = $("#tipo_atividade");
        if($("#tipo_ramo_atividade").length)
            $selectTipoRamoAtividade = $("#tipo_ramo_atividade");
        let $optionsTipo = '<option value="0" disabled>Selecione uma opção</option>';

        $selecionado = false; // var $selecionado by Mr.Goose em 17 abr 19 afim de marcar selecionado a opção zero, se NÃO for edição.
        //Lista dos tipos
        $.each(data, function (idCidade, nomeCidade) {
            $cidadeSelecionada = '';

            if(idSelect == idCidade){
                $cidadeSelecionada = " selected";
                $selecionado = true;
            }

            $optionsTipo += '<option value="' + idCidade + '" ' + $cidadeSelecionada + '>' + nomeCidade + '</option>';
        });
        if (!$selecionado)
            $optionsTipo = $optionsTipo.replace('disabled', 'selected disabled');

        $selectTipoRamoAtividade.html($optionsTipo).select2({
            language: 'pt-br',
            placeholder: "Selecione uma opção",
            search_contains: true,
            width: '100%'
        }).trigger('change');
       
    });

}

window.montarTipoAtividade = function(idTipo) {

    let $selectRamo = $('#ramo_atividade');

    $selectRamo.on("change", function () {
        if($(this).val() != undefined && $(this).val() != '' && $(this).val() != null)
            idTipo = $(this).val();
            montaTipoAtividade(idTipo);            
    });    
}

function totalCadastrosPorPeriodo() {

    let $totalGerador = $('#total_gerador'),
        $totalPeqGerador = $('#total_pequeno_gerador'),
        $totalTransportador = $('#total_transportador'),
        $totalDestino = $('#total_destino'),
        $totalCooperativa = $('#total_cooperativa'),
        $totalEcoponto = $('#total_ecoponto'),
        $totalPev = $('#total_pev'),
        $totalEquipamento = $('#total_equipamento'),
        $totalVeiculo = $('#total_veiculo');

    let $count = $('#indicadores_periodo').find('.count');

    let zeraContagens = () => {
        $totalGerador.text("0");
        $totalTransportador.text("0");
        $totalDestino.text("0");
        $totalPeqGerador.text("0");
        $totalCooperativa.text("0");
        $totalEcoponto.text("0");
        $totalPev.text("0");
        $totalEquipamento.text("0");
        $totalVeiculo.text("0");
    };

    let alterarContagensFiscal = (data) => {
        $totalGerador.text(data.cadastros.geradores);
        $totalTransportador.text(data.cadastros.transportadores);
        $totalDestino.text(data.cadastros.destinosFinais);
        $totalPeqGerador.text(data.cadastros.pequenosGeradores);
        $totalCooperativa.text(data.cadastros.cooperativas);
        $totalEcoponto.text(data.cadastros.ecopontos);
        $totalPev.text(data.cadastros.pev);
        $totalEquipamento.text(data.cadastros.equipamentos);
        $totalVeiculo.text(data.cadastros.veiculos);
    };

    let alterarContagens = (data) => {
        $totalGerador.text(data.vinculos.geradores);
        $totalPeqGerador.text(data.vinculos.pequenosGeradores);
        $totalDestino.text(data.vinculos.destinos);
        $totalTransportador.text(data.vinculos.transportadores);
    };

    let animacaoContagens = (data) => {
        if (window.urlSegment2 == 'fiscal') {
            alterarContagensFiscal(data);
        } else {
            alterarContagens(data);
        }

        $count.each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 700,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    };

    let acessaCadastrosPeriodoMudaContagens = (url) => {
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                animacaoContagens(data);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    };

    $("#indicadores_periodo .nav-indicadores-periodo .opcao-indicador-periodo").on('click', function () {

        zeraContagens();

        let periodoEscolhido = $(this).data('periodo'),
            url = "/painel/vinculos-por-periodo/" + periodoEscolhido;

        if (window.urlSegment2 == 'fiscal') {
            url = '/painel/fiscal/cadastros-por-periodo/' + periodoEscolhido;
        }

        $(this).parent().parent().find('.active').removeClass();
        $(this).parent().addClass('active');

        acessaCadastrosPeriodoMudaContagens(url);

    });

    $("#dt_final_fiscal_cad_periodo").on('change', function () {

        let dataInicial = $('#dt_inicio_fiscal_cad_periodo').val(),
            dataFinal = $(this).val();

        let split = dataInicial.split('/');
        dataInicial = split[2] + "-" + split[1] + "-" + split[0];

        split = dataFinal.split('/');
        dataFinal = split[2] + "-" + split[1] + "-" + split[0];

        if (dataInicial != null && dataFinal != null) {

            zeraContagens();

            var url = "/painel/vinculos-por-periodo/custom";

            if (window.urlSegment2 == 'fiscal') {
                url = '/painel/fiscal/cadastros-por-periodo/custom';
            }

            url += "/" + dataInicial + "/" + dataFinal;

            $(this).parents('.nav-indicadores-periodo').find('.active').removeClass();
            $(this).parent().parent().parent().parent().parent().addClass('active');

            acessaCadastrosPeriodoMudaContagens(url);

        } else {
            return false;
        }
    });

    // carrega contagens se estiver acessando a pagina
    if (window.location.href.endsWith("painel/transportador")) {
        acessaCadastrosPeriodoMudaContagens("/painel/vinculos-por-periodo/total");
    }


}

function graficoPainelFiscal() {

    if (window.routeClass == 'painel_fs' && $('#ctre_balanco_periodo_fiscal').length > 0) {
        new Chart(document.getElementById("ctre_balanco_periodo_fiscal"), {
            type: 'line',
            data: {
                labels: [0],
                datasets: [
                    {
                        label: "Expirado",
                        backgroundColor: "rgba(255, 75, 51, 0.7)",
                        data: [0]
                    },
                    {
                        label: "Em Aberto",
                        backgroundColor: "rgba(255, 198, 25, 0.7)",
                        data: [0]
                    },
                    {
                        label: "Finalizado",
                        backgroundColor: "rgba(62, 184, 22, 0.7)",
                        data: [0]
                    }
                ]
            },
        });
    }

    $("#data_final_fiscal_grafico").change(function (e) {
        e.preventDefault();

        var dataInicial = $('#data_inicial_fiscal_grafico').val(),
            dataFinal = $('#data_final_fiscal_grafico').val(),
            url = '/painel/fiscal/ctre-balanco-periodo',
            split = '',
            indice = '',
            legenda = [],
            emAberto = [],
            expirado = [],
            finalizados = [];

        split = dataInicial.split('/');
        dataInicial = split[2] + "-" + split[1] + "-" + split[0];

        split = dataFinal.split('/');
        dataFinal = split[2] + "-" + split[1] + "-" + split[0];

        if (dataInicial != null && dataFinal != null) {

            $.ajax({
                type: 'GET',
                url: url + "/" + dataInicial + "/" + dataFinal,
                beforeSend: function () {

                },
                success: function (data) {

                    if ($.isEmptyObject(data.error)) {

                        $.each(data.ctre, function (key, val) {

                            indice = key;
                            legenda.push(indice);

                            $.each(val, function (key, val) {

                                if (key == 'emaberto') {
                                    emAberto.push(val.ctre_cont);
                                }

                                if (key == 'expirado') {
                                    expirado.push(val.ctre_cont);
                                }

                                if (key == 'finalizado') {
                                    finalizados.push(val.ctre_cont);
                                }
                            });
                        });

                        var grafico = document.getElementById("ctre_balanco_periodo_fiscal"),
                            myChart = new Chart(grafico, {
                                type: 'line',
                                data: {
                                    labels: legenda,
                                    datasets: [
                                        {
                                            label: "Expirado",
                                            backgroundColor: "rgba(255, 75, 51, 0.7)",
                                            data: expirado
                                        },
                                        {
                                            label: "Em Aberto",
                                            backgroundColor: "rgba(255, 198, 25, 0.7)",
                                            data: emAberto
                                        },
                                        {
                                            label: "Finalizado",
                                            backgroundColor: "rgba(62, 184, 22, 0.7)",
                                            data: finalizados
                                        }
                                    ]
                                },
                            });

                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            })

        } else {
            return false;
        }
    });
}

function elementosChosen() {

    var $select2Option = {
        language: "pt-BR",
        width: '100%',
        placeholder: {
            id: '-1', // the value of the option
            text: 'Selecione uma opção'
        }
    };

    $(".ramo-atividade, .tipo-atividade").select2($select2Option);
    $(".estados, .cidades").select2($select2Option);
    $(".residuos").select2($select2Option);
    $(".equipamentos_vinculo").select2($select2Option);
    $(".transportador, .gerador, .destino").select2($select2Option);
    $(".placa_veiculo").select2($select2Option);
    $("#recipient-message").select2($select2Option);
}

function elementosTableSorter() {

    $("#lancamentos_ctre").tablesorter({
        sortList: [
            [0, 0],
            [2, 0]
        ]
    });

}

function totalCtreGeradoresPorPeriodo() {

    var empresaId = window.urlSegment4;

    $("#indicador_periodo_gerador_fiscal .nav-indicadores-periodo .opcao-indicador-periodo").on('click', function () {

        var periodoEscolhido = $(this).data('periodo'),
            $totalEmitido = $('#total_ctre_emitido'),
            $totalAberto = $('#total_ctre_aberto'),
            $totalFinalizado = $('#total_ctre_finalizado'),
            $totalExpirado = $('#total_ctre_expirado'),
            url = "/painel/fiscal/ctre-por-periodo-fiscal/",

            $count = $('#indicador_periodo_gerador_fiscal').find('.count');

        $totalEmitido.text("0");
        $totalAberto.text("0");
        $totalFinalizado.text("0");
        $totalExpirado.text("0");

        $(this).parent().parent().find('.active').removeClass();
        $(this).parent().addClass('active');

        $.ajax({
            type: 'GET',
            url: url + periodoEscolhido + "/" + empresaId,
            success: function (data) {

                if ($.isEmptyObject(data.error)) {

                    $totalEmitido.text(data.ctre.emitidos);
                    $totalAberto.text(data.ctre.abertos);
                    $totalFinalizado.text(data.ctre.finalizados);
                    $totalExpirado.text(data.ctre.expirados);

                    $count.each(function () {
                        $(this).prop('Counter', 0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 500,
                            easing: 'swing',
                            step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                        });
                    });
                }
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
}

function hiddenTipoEstabelecimentoCondominio() {

    $("#tipo_atividade").change(function (event) {
        event.preventDefault();

        var $tipoAtividade = $(this),
            tipoAtividadeValor = $tipoAtividade.val(),
            $tipoEstabelecimentoDiv = $("#tipo_estabelecimento");

        if (tipoAtividadeValor == 54 || tipoAtividadeValor == 55 || tipoAtividadeValor == 56 || tipoAtividadeValor == 57 || tipoAtividadeValor == 58 || tipoAtividadeValor == 59) {
            $tipoEstabelecimentoDiv.css('display', 'none');

        } else {
            $tipoEstabelecimentoDiv.css('display', 'block');
        }
    });
}

function scrollPage(alvoScroll) {
    var $doc = $('html, body');

    $doc.animate({
        scrollTop: $("" + alvoScroll + "").offset().top
    }, 500);
}


let datatableLanguageParams = (msg_empty) => {

    let msg_info_empty = "Não há registros com esses parametros";

    return {
        "lengthMenu": "Mostrando _MENU_ registros por página",
        "zeroRecords": msg_info_empty,
        "info": "Mostrando página _PAGE_ Até _PAGES_",
        "infoEmpty": msg_empty,
        "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
        "sSearch": "FILTRO",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        }
    }
};
/*- Abaixo, inserido by Mr.Goose a partir de 17 abr 19 afim de tratar a Edição dos Dados pela Empresa -*/
$(document).ready(function() {
    if ($("#id_ramo_atividade").length && $("#id_tipo_ramo_atividade").length){
        var $id_ramo_atividade = $('#id_ramo_atividade').val(),
            $id_tipo_atividade = $('#id_tipo_ramo_atividade').val();
        montaTipoAtividade($id_ramo_atividade, $id_tipo_atividade);
        setTimeout(function(){
            $('#update-ramo_atividade select#ramo_atividade').prop("disabled", true);
            $('#update-tipo_ramo_atividade select#tipo_ramo_atividade').prop("disabled", true);
        }, 1000);      
    }
    $('.btn_edit_usuario').prop("disabled", true); // linha inserida By Mr.Goose em 24 abr 19 às 20h temporariamente, até que a Edicação de Cadastro esteja 100% concluída, conforme ordens do Chicko.
});
/*- acima, inserido by Mr.Goose a partir de 17 abr 19 afim de tratar a Edição dos Dados pela Empresa -*/
    function modalAviso(){
        $('#modalAvisoBotaoAlteracao').modal('show');
    }
