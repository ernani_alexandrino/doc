$(function () {

    dataTableResiduos();
    dataTableCadastroGeradoresFiscal();
    dataTableCadastroGeradoresCondominioFiscal();
    dataTableCadastroPeqGeradoresFiscal();
    dataTableCadastroCooperativaFiscal();
    dataTableCadastroTransportadoresFiscal();
    dataTableCadastroDestinosFiscal();
    dataTableCadastroServicosFiscal();
    dataTableCadastroOrgaosFiscal();

    if (window.urlSegment4.length !== 0) {
        // funcoes abaixo necessitam de parametro da url para executar
        dataTableVinculosGeradoresFiscal();
        dataTableSearchGrandesGeradores();
        dataTableSearchGrandesGeradoresCondominio();
        dataTableSearchPeqGeradores();
        dataTableSearchCooperativa();
        dataTableSearchDestinosFinais();
        dataTableSearchServicosSaude();
        dataTableSearchOrgaosPublicos();
        dataTableSearchTransportadores();
        dataTableVinculosTransportadoresGgFiscal();
        dataTableVinculosTransportadoresDfFiscal();
        dataTableVinculosTransportadoresVeiculosFiscal();
        dataTableVinculosTransportadoresEquipamentosFiscal();
    }

});

function dataTableResiduos() {

    $('#residuos-cadastrados').dataTable({
        stateSave: true,
        "iDisplayLength": 10,
        "order": [
            [1, "asc"]
        ],
        "pagingType": "full_numbers",
        "sDom": '<"total-records"<"left"i><"right"l>>rt<"bottom"p><"clear">',
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ Resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },

        },
        "bLengthChange": false,
        columnDefs: [{
            targets: 'no-sort',
            orderable: false,
            searchable: true
        },]
    });

}

function dataTableCadastroGeradoresFiscal() {

    var $tabelaCadastroGrandesGeradores = $('#cadastro_grandes_geradores');

    $tabelaCadastroGrandesGeradores.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-gg-ajax/",
            pages: 5
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'bairro', name: 'empresa.empresa_endereco.bairro'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroGrandesGeradores.on('click', 'tbody tr', function (event) {
        
        event.preventDefault();
        $ActiveTabelaCadastroGrandesGeradores = $('#cadastro_grandes_geradores tbody tr');
        var empresaId = $(this).find('.amlurb_id').data('id');

        
        $ActiveTabelaCadastroGrandesGeradores.addClass('active_table');

        window.location.href = "/painel/fiscal/cadastro-grande-gerador/" + empresaId;
    });
}

function dataTableCadastroGeradoresCondominioFiscal() {

    var $tabelaCadastroGrandesGeradoresCondominio = $('#cadastro_grandes_geradores_condominio');

    $tabelaCadastroGrandesGeradoresCondominio.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-gc-ajax/",
            pages: 5
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'bairro', name: 'empresa.empresa_endereco.bairro'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroGrandesGeradoresCondominio.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-grande-gerador-condominio/" + empresaId;
    });
}

function dataTableCadastroPeqGeradoresFiscal() {

    var $tabelaCadastroPequenosGeradores = $('#cadastro_peq_geradores');

    $tabelaCadastroPequenosGeradores.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-pg-ajax/",
            pages: 5
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'bairro', name: 'empresa.empresa_endereco.bairro'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroPequenosGeradores.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-pequeno-gerador/" + empresaId;
    });
}

function dataTableCadastroCooperativaFiscal() {

    var $tabelaCadastroCooperativa = $('#cadastro_cooperativa');

    $tabelaCadastroCooperativa.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-cr-ajax/",
            pages: 5
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'bairro', name: 'empresa.empresa_endereco.bairro'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroCooperativa.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-cooperativa/" + empresaId;
    });
}

function dataTableCadastroTransportadoresFiscal() {

    var $tabelaCadastroTransportadores = $('#cadastro_transportadores');

    $tabelaCadastroTransportadores.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-tr-ajax/",
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'bairro', name: 'empresa.empresa_endereco.bairro'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroTransportadores.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        let empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-transportadores/" + empresaId;
    });
}

function dataTableCadastroDestinosFiscal() {

    var $tabelaCadastroDestinos = $('#cadastro_destinos');

    $tabelaCadastroDestinos.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-df-ajax/",
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'bairro', name: 'empresa.empresa_endereco.bairro'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroDestinos.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-destinos/" + empresaId;

    });
}

function dataTableCadastroServicosFiscal() {

    var $tabelaCadastroServicos = $('#cadastro_servico_saude');

    $tabelaCadastroServicos.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-ss-ajax/",
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'bairro', name: 'empresa.empresa_endereco.bairro'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroServicos.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-servico-saude/" + empresaId;

    });
}

function dataTableCadastroOrgaosFiscal() {

    var $tabelaCadastroOrgaos = $('#cadastro_orgao_publico');

    $tabelaCadastroOrgaos.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-op-ajax/",
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'nome_comercial', name: 'empresa.nome_comercial'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'bairro', name: 'empresa.empresa_endereco.bairro'},
            {data: 'status', name: 'empresa.status.descricao'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroOrgaos.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-orgao-publico/" + empresaId;

    });
}

function dataTableVinculosGeradoresFiscal() {

    let $tableVinculosGeradores = $('#cadastro_vinculos_geradores'),
        empresaId = window.urlSegment4;

    $tableVinculosGeradores.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ajax": window.location.origin + "/painel/fiscal/vinculos-ajax/" + empresaId,
        columns: [
            {data: 'id', name: 'empresa_vinculada.id_limpurb'},
            {data: 'razao_social', name: 'empresa_vinculada.razao_social'},
            {data: 'cnpj', name: 'empresa_vinculada.cnpj'},
            {data: 'data_vencimento', name: 'data_vencimento', iDataSort: 1},
            {data: 'status', name: 'status'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-vinculo-gg');
        }
    });
}

function dataTableSearchGrandesGeradores() {

    var input = window.urlSegment4,
        inputValor = window.urlSegment5,
        $tabelaCadastroGrandesGeradores = $("#cadastro_grandes_geradores_search");

    $tabelaCadastroGrandesGeradores.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-gg-search-ajax/" + input + "/" + inputValor,
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'id_limpurb'},
            {data: 'nome_comercial', name: 'nome_comercial'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'bairro', name: 'bairro'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroGrandesGeradores.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-grande-gerador/" + empresaId;
    });
}

function dataTableSearchGrandesGeradoresCondominio() {

    var input = window.urlSegment4,
        inputValor = window.urlSegment5,
        $tabelaCadastrosGrandesGeradoresCondominio = $("#cadastro_grandes_geradores_condominio_search");

    $tabelaCadastrosGrandesGeradoresCondominio.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-gc-search-ajax/" + input + "/" + inputValor,
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'id_limpurb'},
            {data: 'nome_comercial', name: 'nome_comercial'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'bairro', name: 'bairro'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastrosGrandesGeradoresCondominio.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-grande-gerador-condominio/" + empresaId;
    });
}

function dataTableSearchPeqGeradores() {

    var input = window.urlSegment4,
        inputValor = window.urlSegment5,
        $tabelaPequenosGeradores = $("#cadastro_peq_geradores_search");

    $tabelaPequenosGeradores.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-pg-search-ajax/" + input + "/" + inputValor,
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'id_limpurb'},
            {data: 'nome_comercial', name: 'nome_comercial'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'bairro', name: 'bairro'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaPequenosGeradores.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-pequeno-gerador/" + empresaId;
    });
}

function dataTableSearchCooperativa() {

    var input = window.urlSegment4,
        inputValor = window.urlSegment5,
        $tabelaCadastroCooperativa = $("#cadastro_cooperativa_search");

    $tabelaCadastroCooperativa.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-cr-search-ajax/" + input + "/" + inputValor,
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'id_limpurb'},
            {data: 'nome_comercial', name: 'nome_comercial'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'bairro', name: 'bairro'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroCooperativa.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-cooperativa/" + empresaId;
    });
}

function dataTableSearchServicosSaude() {

    var input = window.urlSegment4,
        inputValor = window.urlSegment5,
        $tabelaCadastroServicosSaude = $("#cadastro_servico_saude_search");

    $tabelaCadastroServicosSaude.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-ss-search-ajax/" + input + "/" + inputValor,
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'id_limpurb'},
            {data: 'nome_comercial', name: 'nome_comercial'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'bairro', name: 'bairro'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroServicosSaude.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-servico-saude/" + empresaId;
    });
}

function dataTableSearchOrgaosPublicos() {

    var input = window.urlSegment4,
        inputValor = window.urlSegment5,
        $tabelaCadastroOrgaosPublicos = $("#cadastro_orgao_publico_search");

    $tabelaCadastroOrgaosPublicos.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-op-search-ajax/" + input + "/" + inputValor,
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'id_limpurb'},
            {data: 'nome_comercial', name: 'nome_comercial'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'bairro', name: 'bairro'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroOrgaosPublicos.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-orgao-publico/" + empresaId;
    });
}

function dataTableSearchTransportadores() {

    var input = window.urlSegment4,
        inputValor = window.urlSegment5,
        $tabelaCadastroTransportador = $("#cadastro_transportadores_search");

    $tabelaCadastroTransportador.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-tr-search-ajax/" + input + "/" + inputValor,
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'id_limpurb'},
            {data: 'nome_comercial', name: 'nome_comercial'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'bairro', name: 'bairro'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroTransportador.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-transportadores/" + empresaId;
    });
}

function dataTableSearchDestinosFinais() {

    var input = window.urlSegment4,
        inputValor = window.urlSegment5,
        $tabelaCadastroDestinosFinais = $("#cadastro_destinos_finais_search");

    $tabelaCadastroDestinosFinais.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/fiscal/cadastro-df-search-ajax/" + input + "/" + inputValor,
            pages: 6
        }),
        columns: [
            {data: 'id', name: 'id_limpurb'},
            {data: 'nome_comercial', name: 'nome_comercial'},
            {data: 'razao_social', name: 'razao_social'},
            {data: 'cnpj', name: 'cnpj'},
            {data: 'bairro', name: 'bairro'},
            {data: 'status', name: 'status'},
            {data: 'acao', name: 'acao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-cadastro-gg');
        }
    });

    $tabelaCadastroDestinosFinais.on('click', 'tbody tr', function (event) {
        event.preventDefault();

        var empresaId = $(this).find('.amlurb_id').data('id');

        window.location.href = "/painel/fiscal/cadastro-destinos/" + empresaId;
    });
}

function dataTableVinculosTransportadoresGgFiscal() {

    let $tableVinculosTransportadoresGg = $('#cadastro_vinculos_geradores_tr'),
        empresaId = window.urlSegment4;

    $tableVinculosTransportadoresGg.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": window.location.origin + "/painel/fiscal/vinculos-ajax/" + empresaId,
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'data_vencimento', name: 'data_vencimento', iDataSort: 1},
            {data: 'status', name: 'status'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-vinculo-gg');
        }
    });
}

function dataTableVinculosTransportadoresDfFiscal() {

    let $tableVinculosTransportadoresDf = $('#cadastro_vinculos_destinos_tr'),
        empresaId = window.urlSegment4;

    $tableVinculosTransportadoresDf.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": window.location.origin + "/painel/fiscal/vinculos-ajax/" + empresaId,
        columns: [
            {data: 'id', name: 'empresa.id_limpurb'},
            {data: 'razao_social', name: 'empresa.razao_social'},
            {data: 'cnpj', name: 'empresa.cnpj'},
            {data: 'data_vencimento', name: 'data_vencimento', iDataSort: 1},
            {data: 'status', name: 'status'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-vinculo-gg');
        }
    });
}

function dataTableVinculosTransportadoresVeiculosFiscal() {

    var $tableVinculosTransportadoresVl = $('#cadastro_vinculos_veiculos_tr'),
        empresaId = window.urlSegment4;

    $tableVinculosTransportadoresVl.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": window.location.origin + "/painel/fiscal/vinculos-tr-vl-ajax/" + empresaId,
        columns: [
            {data: 'placa', name: 'placa'},
            {data: 'tipo', name: 'tipo'},
            {data: 'marca', name: 'marca'},
            {data: 'ano', name: 'ano_veiculo'},
            {data: 'renavam', name: 'renavam'},
            {data: 'venc_ipva', name: 'vencimento_ipva', iDataSort: 1},
            {data: 'capacidade', name: 'capacidade'},
            {data: 'tara', name: 'tara'},
            {data: 'status', name: 'status.descricao'}
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-vinculo-gg');
        }
    });
}

function dataTableVinculosTransportadoresEquipamentosFiscal() {

    let $tableVinculosTransportadoresEq = $('#cadastro_vinculos_equipamentos_tr'),
        empresaId = window.urlSegment4;

    $tableVinculosTransportadoresEq.DataTable({
        "language": {
            "lengthMenu": "Mostrando _MENU_ registros por página",
            "zeroRecords": "Não há registros para serem exibidos.",
            "info": "Mostrando página _PAGE_ Até _PAGES_",
            "infoEmpty": "Não há registros com esses parametros",
            "infoFiltered": "(Filtrados de um total de _MAX_ registros)",
            "sSearch": "FILTRO",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            }
        },
        "processing": true,
        "serverSide": true,
        "bLengthChange": false,
        "searching": false,
        "ordering": false,
        "ajax": window.location.origin + "/painel/fiscal/vinculos-tr-eq-ajax/" + empresaId,
        columns: [
            {data: 'equipamento_id', name: 'id'},
            {data: 'equipamento_nome', name: 'equipamento.nome'},
            {data: 'status', name: 'status.descricao'},
        ],
        createdRow: function (row, data, index) {
            $(row).addClass('linha-vinculo-gg');
        }
    });
}