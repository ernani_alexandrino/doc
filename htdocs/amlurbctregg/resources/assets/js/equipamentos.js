$(function () { // gerador e transportador

    let id_equipamento = ''; // controle de equipamento selecionado

    if (window.location.href.endsWith('meus-equipamentos')) {
        atualizaListaEquipamentos();
    }

    /// metodos CRUD

    $('#btn_add_equipamento').click(function () {
        $('#btn-save').val("add");
        $('#frmProducts').trigger("reset");
        $('#modalEquipamentoAdicionar').modal('show');
    });

    $('#btn-cancel-equipamento').click(function () {
        $('#modalEquipamentoAdicionar').modal('hide');
    });

    $("#modalEquipamentoAdicionar #btn-save").click(function (e) {
        var formData = {
            _token: $("input[name='_token']").val(),
            equipamento: $('#equipamentos').val(),
            quantidade: $('#quantidade').val(),
        };

        //usado para determinar o tipo de envio POST ou PUT
        var state = $('#btn-save').val();
        var equipamentoId = $('#product_id').val();
        var url = '/painel/equipamento-salvar';

        if (state == "update") {
            type = "PUT"; //para update
            url += '/' + equipamentoId;
        }

        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            success: function (data) {

                $('#frmEquipamentos').trigger("reset");
                $('#modalEquipamentoAdicionar').modal('hide');

                // atualiza listagem e mostra mensagem de sucesso
                $('#ci-success').empty().append(data['success']).parent().parent().show();
                $("#table-equipamentos-vinculados").DataTable().ajax.reload();

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    /// funcoes ativacao equipamentos

    let ativarEquipamento = () => {
        $.ajax({
            type: 'POST',
            headers: getAjaxHeaders(),
            url: '/painel/equipamento/' + id_equipamento + '/ativar',
            success: (data) => {
                $('#ci-success').empty().append(data['success']).parent().parent().show();
                $("#table-equipamentos-vinculados").DataTable().ajax.reload();
                $(document).scrollTop(0);
            }
        })
    };

    let ativarVariosEquipamentos = (equipamentos) => {
        $.ajax({
            type: 'POST',
            url: '/painel/ativar-varios-equipamentos',
            headers: getAjaxHeaders(),
            data: {equipamentos: equipamentos},
            success: (data) => {
                $('#ci-success').empty().append(data['success']).parent().parent().show();
                $("#table-equipamentos-vinculados").DataTable().ajax.reload();
                $('#modalAtivarRenovarEquipamentos').modal('hide');
                $(document).scrollTop(0);
            }
        });
    };

    /// modal de ativar/renovar equipamentos

    $('#btn_atv_rnv_equipamento').on('click', () => {
        $.ajax({
            type: 'GET',
            url: '/painel/equipamentos-ativar',
            success: (data) => {
                $('#modalAtivarRenovarEquipamentos').empty().append(data.html);
                $('#modalAtivarRenovarEquipamentos').modal('show');
            }
        })
    });

    // checkbox para marcar/desmarcar equipamentos
    $('#modalAtivarRenovarEquipamentos').on('click', '#select-all-equipamentos', function () {
        let check_value = (this.checked) ? true : false;
        $('.checkbox-equipamento').each(function () {
            this.checked = check_value;
        });
    });

    $('#modalAtivarRenovarEquipamentos').on('click', '#btn-save-ativar-equipamentos', () => {
        let equipamentos = [];

        $('.checkbox-equipamento').each(function () {
            if (this.checked == true) {
                equipamentos.push($(this).attr('value'));
            }
        });

        ativarVariosEquipamentos(equipamentos);
    });

    $('#modalAtivarRenovarEquipamentos').on('click', '#btn-reset-ativar-equipamentos', () => {
        $('#modalAtivarRenovarEquipamentos').modal('hide');
    });

    /// modal equipamento individual

    $('#equipamento-list').on('click', '.ativar-equipamento', function () {
        id_equipamento = $(this).parents('tr:first').attr('id'); // id da tr
        $('#modalAtivarRenovarEquipamento h4').empty().append('Ativar Equipamento');
        $('#modalAtivarRenovarEquipamento #p1').empty().append('Deseja ativar este equipamento?');
        $('#modalAtivarRenovarEquipamento').modal("show");
    });

    $('#equipamento-list').on('click', '.renovar-equipamento', function () {
        id_equipamento = $(this).parents('tr:first').attr('id'); // id da tr
        $('#modalAtivarRenovarEquipamento h4').empty().append('Renovar Equipamento');
        $('#modalAtivarRenovarEquipamento #p1').empty().append('Deseja renovar este equipamento?');
        $('#modalAtivarRenovarEquipamento').modal("show");
    });

    $("#modalAtivarRenovarEquipamento").on('click', '#btn-confirmar-equipamento', () => {
        ativarEquipamento();
        $('#modalAtivarRenovarEquipamento').modal("hide");
    });

    $("#modalAtivarRenovarEquipamento").on('click', '#btn-cancel-equipamento', () => {
        $('#modalAtivarRenovarEquipamento').modal("hide");
    });

    // modal qrcode
    $('#equipamento-list').on('click', '.solicitar-qrcode', function () {
        id_equipamento = $(this).parents('tr:first').attr('id'); // id da tr
        $('#modalEquipamentoQrcode').modal('show');
    });

    // fechar modal
    $('#modalEquipamentoQrcode #btn-cancel-qrcode').on('click', function () {
        $('#modalEquipamentoQrcode').modal('hide');
    });

    $('#modalEquipamentoQrcode #btn-qrcode-equipamento').on('click', function () {

        if ($('input[id="concordancia-qrcode"]').is(':checked')) {

            $.ajax({
                url: '/painel/novo-qrcode-equipamento/' + id_equipamento,
                success: (data) => {

                    window.open('/painel/qrcode-pdf/' + data['qrcode_id'], '_blank');
                    $('#modalEquipamentoQrcode').modal('hide');

                }
            });

            $('#form-checkbox-qrcode p.text-danger').empty();

        } else {

            $('#form-checkbox-qrcode p.text-danger').empty().append("É necessário concordar com as regras para acessar o QR Code");

        }

    });

    // deletar equipamento (recem adicionado)
    $('#equipamento-list').on('click', '.delete-equipamento-force', function () {
        id_equipamento = $(this).parents('tr:first').attr('id'); // id da tr
        $.ajax({
            type: 'DELETE',
            headers: getAjaxHeaders(),
            url: '/painel/equipamento-force-deletar/' + id_equipamento,
            success: function (data) {
                $('#ci-success').empty().append(data['success']).parent().parent().show();
                $("#table-equipamentos-vinculados").DataTable().ajax.reload();
                $(document).scrollTop(0);
            }
        })
    });

    // deletar equipamento com licenca (modal)
    $('#equipamento-list').on('click', '.delete-equipamento', function () {
        id_equipamento = $(this).parents('tr:first').attr('id'); // id da tr
        // limpa campos e mostra modal
        $("#jee_p").empty();
        $("#justificativa_exclusao_equipamento").val("");
        $('#modalEquipamentoExcluir').modal('show');
    });

    $('#btn-excluir-equipamento').on('click', function () {

        let justificativa = $("#justificativa_exclusao_equipamento").val();
        if (justificativa.length === 0) {
            $("#jee_p").empty().append("Valor precisa estar preenchido");
            return;
        }

        $.ajax({
            type: 'DELETE',
            headers: getAjaxHeaders(),
            url: '/painel/equipamento-deletar/' + id_equipamento,
            data: {justificativa: justificativa},
            success: function (data) {
                $('#ci-success').empty().append(data['success']).parent().parent().show();
                $("#table-equipamentos-vinculados").DataTable().ajax.reload();
                $('#modalEquipamentoExcluir').modal('hide');
                $(document).scrollTop(0);
            }
        })
    });

    $('#btn-cancel-excluir-equipamento').on('click', function () {
        $('#modalEquipamentoExcluir').modal('hide');
    });

});

let atualizaListaEquipamentos = () => { // gerador e transportador

    let msg_empty = "Você não possui nenhum equipamento cadastrado. Clique em 'ADICIONAR EQUIPAMENTO'.";

    $("#table-equipamentos-vinculados").DataTable({
        "language": datatableLanguageParams(msg_empty),
        "processing": true,
        "serverSide": true,
        //"ajax": "/painel/equipamentos-list",
        "ajax": $.fn.dataTable.pipeline({
            url: window.location.origin + "/painel/equipamentos-list",
            pages: 3
        }),
        columns: [
            {data: 'codigo_amlurb', name: 'codigo_amlurb'},
            {data: 'nome', name: 'nome'},
            {data: 'vinculo', name: 'vinculo'},
            {data: 'status', name: 'status'},
            {data: 'validade', name: 'validade'},
            {data: 'acao', name: 'acao'}
        ]
    });

};