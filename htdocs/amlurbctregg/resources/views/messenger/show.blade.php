@extends('layouts.master')

@section('content')
    <div class="col-md-6">
        <h2>{{ $thread->subject }}</h2>
        @each('messenger.partials.messages', $thread->messages, 'message')

        @include('messenger.partials.form-message')
    </div>
@stop