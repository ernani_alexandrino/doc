@extends('layouts.master')

@section('content')
    <div class="col-xs-12">
        <h2>Criar Nova Mensagem</h2>
    </div>
    <form action="{{ route('messages.store') }}" method="post">
        {{ csrf_field() }}
        <div class="col-xs-12 col-sm-8">
            @if($users->count() > 0)
                <div class="form-group">
                    <label for="destinatario">Selecione o usuário/empresa</label>
                    <select name="recipients[]" id="recipient-message" class="form-control">
                        <option value="">Selecione</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{!! $user->name !!} - {!! $user->empresa->razao_social !!}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            <!-- Subject Form Input -->
            <div class="form-group">
                <label class="control-label">Assunto</label>
                <input type="text" class="form-control" name="subject" placeholder="Assunto"
                       value="{{ old('subject') }}">
            </div>

            <!-- Message Form Input -->
            <div class="form-group">
                <label class="control-label">Mensagem</label>
                <textarea name="message" class="form-control">{{ old('message') }}</textarea>
            </div>

        <!-- Submit Form Input -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </form>
@stop
