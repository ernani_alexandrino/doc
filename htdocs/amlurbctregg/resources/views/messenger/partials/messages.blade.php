<div class="media">
    {{--<a class="pull-left" href="#">--}}
        {{--<img src="//www.gravatar.com/avatar/{{ md5($message->user->email) }} ?s=64"--}}
             {{--alt="{{ $message->user->name }}" class="img-circle">--}}
    {{--</a>--}}
    <div class="media-body">
        <h4 class="media-heading">{{ $message->user->name }}</h4>
        <p>{{ $message->body }}</p>
        <div class="text-muted">
            <small>Postado {{ $message->created_at->diffForHumans() }}</small>
        </div>
    </div>
</div>