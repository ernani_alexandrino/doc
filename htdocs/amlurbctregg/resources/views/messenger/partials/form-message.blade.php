<h3>Adicionar nova mensagem</h3>
<form action="{{ route('messages.update', $thread->id) }}" method="post">
{{ method_field('put') }}
{{ csrf_field() }}

    <div class="form-group">
        <textarea name="message" class="form-control">{{ old('message') }}</textarea>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
</form>