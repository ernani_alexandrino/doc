@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                Cadastro aprovado com sucesso</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">

                <p style="font-size: 15px; color: #787878;">Parabéns!</p>
                <br>

                <p style="font-size: 15px; color: #787878;">
                    A AMLURB analisou a autodeclaração da empresa {{ $dadosEmail['empresa']['razao_social'] }}
                    (Código AMLURB {{ $dadosEmail['empresa']['id_limpurb'] }}) ao sistema CTR-E e a classificação
                    como <strong>{{ $dadosEmail['tipoEmpresa'] }}</strong>, de acordo com o DECRETO Nº 58.701, foi confirmada. Os próximos passos são:
                </p>

                @if(($dadosEmail['idTipoEmpresa'] != config('enums.empresas_tipo.destino_final_reciclado')) &&
                    ($dadosEmail['tipoEmpresa'] != config('enums.empresas_tipo.cooperativa_residuos')) &&
                    ($dadosEmail['tipoRamoAtividadeId'] != config('enums.tipo_atividade.servico_de_administracao_municipal_direta')) &&
                    ($dadosEmail['tipoRamoAtividadeId'] != config('enums.tipo_atividade.servico_de_administracao_municipal_indireta')) &&
                    (empty($dadosEmail['dataValidade'])))
                    <br>

                    <p><strong>Pagamento</strong></p>

                    <p style="font-size: 15px; color: #787878;">
                        Seu acesso ao sistema será apenas após pagamento da taxa AMLURB e publicação no Diário Oficial.
                    </p>
                    <p style="font-size: 15px; color: #787878;">
                        Acesse o boleto de pagamento da taxa AMLURB <a href="{{ (empty($dadosEmail['boleto'])) ? '#' : $dadosEmail['boleto'] }}" target="_blank">neste link</a>.
                    </p>

                    <p style="font-size: 15px; color: #787878;">
                        O mesmo está válido durante <strong>cinco dias</strong> após a data de geração.                        
                        É possível que o sistema leve até dois dias úteis para identificar o pagamento e
                        até 5 dias úteis para a publicação no Diário Oficial.
                    </p>
                @endif

                <br>

                <p style="font-size: 15px; color: #787878;">
                    Você poderá acessar o sistema CTR-E com os dados abaixo:
                </p>
                <p>Usuário: {{ $dadosEmail['empresa']['email'] }}</p>
                <p>
                    @if(!isset($dadosEmail['senha']) || is_null($dadosEmail['senha']))
                        Senha de acesso: Utilizar a senha enviada anteriormente.
                    @else
                        Senha de acesso: {{ $dadosEmail['senha'] }}
                    @endif
                </p>

                @if(($dadosEmail['idTipoEmpresa'] != config('enums.empresas_tipo.destino_final_reciclado')) &&
                    ($dadosEmail['tipoEmpresa'] != config('enums.empresas_tipo.cooperativa_residuos')) &&
                    ($dadosEmail['tipoRamoAtividadeId'] != config('enums.tipo_atividade.servico_de_administracao_municipal_direta')) &&
                    ($dadosEmail['tipoRamoAtividadeId'] != config('enums.tipo_atividade.servico_de_administracao_municipal_indireta')) &&
                    (empty($dadosEmail['dataValidade'])))

                    <br>

                    <p><strong>QR Code / Adesivo</strong></p>

                    <p style="font-size: 15px; color: #787878;">
                        Você poderá imprimir o adesivo de QR Code efetuando o login no sistema. Seu QRCode deverá ficar visível conforme o passo a passo disponível no sistema.
                    </p>

                    <p><strong>Ativação e Vínculos</strong></p>
                    <p><strong>(Transportador, Condomínio, Equipamentos e Destinos)</strong></p>

                    <p style="font-size: 15px; color: #787878;">
                        Para seu cadastro ser ativado, é preciso que você vincule ao menos um transportador de seus resíduos e o condomínio no qual você está localizado (se aplicável). Após esse passo, aguarde a validação final da AMLURB. Você deverá fazer isso acessando o sistema com seu login.
                    </p>

                @endif

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop