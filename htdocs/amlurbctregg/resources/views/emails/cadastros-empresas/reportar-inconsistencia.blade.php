@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
               Inconsistência de dados</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">
                <p style="font-size: 15px; color: #787878;">
                    A Amlurb analisou a autodeclaração da empresa {{ $dadosEmail['empresa']['razao_social'] }}
                    (Código AMLURB {{ $dadosEmail['empresa']['id_limpurb'] }}) ao sistema CTR-E, e identificou
                    que foram encontradas inconsistências nos dados declarados.
                </p>
                <p>
                    Por isso você precisa acessar o sistema CTR-E para alterar os dados informados
                </p>
                <br>

                <p>
                    <strong>Relato da AMLURB</strong>
                </p>
                <p>
                    {{ $dadosEmail['msgInconsistencia'] }}
                </p>

                <br>
                <p><strong>Acesso</strong></p>

                <p style="font-size: 15px; color: #787878;">
                    Acesse o sistema CTR-e para <strong> revisar seu cadastro</strong>
                    <a href="{{config('app.url')}}" target="_blank">CTR-E</a>,
                    com os dados abaixo:
                </p>
                <p style="font-size: 15px; color: #787878;">
                    Email: {{ $dadosEmail['empresa']['email'] }}
                </p>
                <p style="font-size: 15px; color: #787878;">
                    @if(!isset($dadosEmail['senha']) || is_null($dadosEmail['senha']))
                        Senha de acesso: Utilizar a senha enviada anteriormente.
                    @else
                        Senha de acesso: {{ $dadosEmail['senha'] }}
                    @endif
                </p>
                <br>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop