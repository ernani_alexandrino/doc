@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td>
            <h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                CADASTRO REALIZADO COM SUCESSO
            </h2>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">
                <p style="font-size: 15px; color: #787878;">
                    Parabéns!</strong>
                </p>

                <p style="font-size: 15px; color: #787878;">
                    Por meio de sua autodeclaração ao sistema CTR-E, a empresa <strong>{{ $dadosEmail['razaoSocial'] }}</strong>,
                    inscrição municipal {{ $dadosEmail['inscricaoMunicipal'] }}, foi registrada em nosso sistema e classificada como
                    <strong>{{ $dadosEmail['tipoCadastro'] }}</strong>, de acordo com o DECRETO Nº 58.701.
                </p>

                @include('emails.cadastros-empresas.quadro')

                <p>
                    Atenção: De acordo com a Resolução 130/AMLURB/2019, senhaseu empreendimento pode ser requisitado a um novo enquadramento de Categoria AMLURB, antes mesmo do período de vencimento do seu cadastro.
                </p>

                <p>
                    Informamos que 1 ano após a publicação de seu deferimento no Diário Oficial da Cidade
                    e na página da AMLURB você deve <strong>renovar</strong> seu cadastro.
                </p>

                <h3>Acesse seu cadastro</h3>
                <p>
                    Para <strong>consultar ou alterar</strong> seu cadastro, acesse
                    <a href="{{config('app.url')}}" target="_blank">www.ctre.com.br</a>
                    e informe seu e-mail {{ $dadosEmail['email'] }} e a senha {{ $dadosEmail['senha'] }}.
                </p>

                <br>

                <p><strong>QR Code</strong></p>

                <p style="font-size: 15px; color: #787878;">
                    Você também pode imprimir o adesivo QR Code do seu empreendimento efetuando o Login no Link abaixo:
                </p>
                <p>
                    <a href="{{config('app.url')}}/painel/pequeno-gerador" target="_blank">
                        Clique aqui para acessar arquivo e guia sobre QR Code
                    </a>
                </p>

                @include('emails.includes.assinatura')
            </div>
        </td>
        
    </tr>

@stop