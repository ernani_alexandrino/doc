@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                Cadastro não concluído</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo"
                 style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">
                <p style="font-size: 15px; color: #787878;">Olá, <strong>{{ $dadosEmail['responsavel'] }}</strong></p>
                <p style="font-size: 15px; color: #787878;">Podemos ajudá-lo?</p>
                <p style="font-size: 15px; color: #787878;">Você efetuou um cadastro para utilização do sistema CTRE
                    da empresa <strong>{{ $dadosEmail['razaoSocial'] }}</strong>, porém nosso sistema não identificou o pagamento da sua fatura.</p>
                <p style="font-size: 15px; color: #787878;">Para seguir com o acesso, gere uma segunda via do boleto no botão abaixo.</p>

                <a href="#" target="_blank" class="btn btn-gerar-boleto"
                   style="width: 211px;height: 50px;background-color: #f60;font-size: 15px;color: #fff;text-transform: uppercase;margin: 30px auto;display: block;text-align: center;line-height: 48px;text-decoration: none;font-weight: bold;"
                   title="Gerar boleto" rel="noopener">Gerar 2ª via de Boleto</a>

                <p style="font-size: 15px; color: #787878;">Em caso de dúvidas, entre em contato conosco pelos nossos canais de atendimento.</p>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop