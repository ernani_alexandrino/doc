@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td>
            <h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                CADASTRO AGUARDANDO VALIDAÇÃO
            </h2>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">
                <p style="font-size: 15px; color: #787878;">
                    Parabéns!</strong>
                </p>

                <p style="font-size: 15px; color: #787878;">
                    Por meio de sua autodeclaração ao sistema CTR-E, a empresa <strong>{{ $dadosEmail['razaoSocial'] }}</strong>,
                    inscrição municipal {{ $dadosEmail['inscricaoMunicipal'] }}, foi registrada em nosso sistema e classificada como
                    <strong>{{ $dadosEmail['tipoCadastro'] }}</strong>, de acordo com o DECRETO Nº 58.701.
                </p>

                @include('emails.cadastros-empresas.quadro')

                <p>
                    Seu cadastro está agora em análise pela AMLURB. Após validado, você receberá
                    um e-mail com as informações para os próximos passos.
                </p>

                <p>
                    Informamos que 1 ano após a publicação de seu deferimento no Diário Oficial da Cidade
                    e na página da AMLURB você deve <strong>renovar</strong> seu cadastro.
                </p>

                @include('emails.includes.assinatura')
            </div>
        </td>
        
    </tr>

@stop