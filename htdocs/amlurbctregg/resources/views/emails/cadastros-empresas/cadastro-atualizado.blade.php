@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                Cadastro Atualizado com Sucesso</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left; width: 100%;">
                <p style="font-size: 15px; color: #787878;">Parabéns, <strong>{{ $dadosEmail['responsavel'] }}</strong></p>
                <p style="font-size: 15px; color: #787878;">A empresa <strong>{{ $dadosEmail['razaoSocial'] }}</strong>, inscrição municipal
                    <strong>{{ $dadosEmail['inscricaoMunicipal'] }}</strong>, atualizou o seu registro em nossa base de dados.</p>

                <p style="font-size: 15px; color: #787878;">Em instantes você receberá um e-mail com seu nome de usuário e senha para utilização do CTR-E.</p>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop