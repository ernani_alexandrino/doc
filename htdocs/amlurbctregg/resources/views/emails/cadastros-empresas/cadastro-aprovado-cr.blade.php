@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                Cadastro aprovado com sucesso</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">
                <p style="font-size: 15px; color: #787878;">
                    A Amlurb analisou a autodeclaração da empresa {{ $dadosEmail['empresa']['razao_social'] }}
                    (Código AMLURB {{ $dadosEmail['empresa']['id_limpurb'] }}) ao sistema CTR-E e a classificação
                    como <strong>{{ $dadosEmail['tipoEmpresa'] }}</strong> foi aprovada, de acordo com o DECRETO Nº 58.701.
                </p>

                <p><strong>Acesso</strong></p>

                <p style="font-size: 15px; color: #787878;">
                    Você já consegue acessar o sistema
                    <a href="{{config('app.url')}}" target="_blank">CTR-E</a>,
                    com os dados abaixo:
                </p>
                <p style="font-size: 15px; color: #787878;">
                    Email: {{ $dadosEmail['empresa']['email'] }}
                </p>
                <p style="font-size: 15px; color: #787878;">
                    @if(!isset($dadosEmail['senha']) || is_null($dadosEmail['senha']))
                        Senha de acesso: Utilizar a senha enviada anteriormente.
                    @else
                        Senha de acesso: {{ $dadosEmail['senha'] }}
                    @endif
                </p>

                <br>

                <p><strong>QR Code</strong></p>

                <p style="font-size: 15px; color: #787878;">
                    Você também pode imprimir o adesivo QR Code do seu empreendimento efetuando o Login no Sistema:
                </p>
                <p>
                    <a href="{{config('app.url')}}/painel/cooperativa/meu-cadastro" target="_blank">
                        Clique aqui para acessar arquivo e guia sobre QR Code
                    </a>
                </p>
                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop