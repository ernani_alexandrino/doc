<ul>
    <li>
        <p>
            <strong>Categoria AMLURB:</strong>
            {{ $dadosEmail['tipoCadastro'] }}
        </p>
    </li>
    <li>
        <p>
            <strong>Protocolo:</strong>
            {{ $dadosEmail['protocolo'] }}
        </p>
    </li>
    <li>
        <p>
            <strong>Código AMLURB:</strong>
            {{ $dadosEmail['id_limpurb'] }}
        </p>
    </li>
    <li>
        <p>
            <strong>Razão Social:</strong>
            {{ $dadosEmail['razaoSocial'] }}
        </p>
    </li>
    @if(isset($dadosEmail['nomeFantasia']))
    <li>
        <p>
            <strong>Nome Fantasia:</strong>
            {{ $dadosEmail['nomeFantasia'] }}
        </p>
    </li>
    @endif
</ul>