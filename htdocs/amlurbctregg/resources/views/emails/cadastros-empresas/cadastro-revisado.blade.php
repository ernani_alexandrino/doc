@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                Cadastro revisado com sucesso</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo"
                 style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">
                <p style="font-size: 15px; color: #787878;">Parabéns, <strong>{{ $dadosEmail['responsavel'] }}</strong>
                </p>

                <p style="font-size: 15px; color: #787878;">A empresa <strong>{{ $dadosEmail['razaoSocial'] }}</strong>,
                    inscrição municipal
                    <strong>{{ $dadosEmail['inscricaoMunicipal'] }}</strong>, teve suas alterações solicitadas revisadas e confirmadas em nossos sistemas.</p>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop