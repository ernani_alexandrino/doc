@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">CTRE - VOCÊ FOI CADASTRADO PELO {{ $dadosEmail['tipo_empresa'] }} {{ $dadosEmail['vinculador_razao_social'] }}</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">

                <p style="font-size: 15px; color: #787878;">Olá, <strong>{{ $dadosEmail['responsavel'] }}</strong></p>
                <p style="font-size: 15px; color: #787878;">A empresa <strong>{{ $dadosEmail['vinculador_razao_social'] }}</strong> vinculou o seu CNPJ como um {{ $dadosEmail['tipo_empresa_vinculada'] }} em nosso sistema.</p>

                <p style="font-size: 15px; color: #787878;">Acesse o CTR-E para completar as informações no seu cadastro, no link:
                    <a href="https://ctre.com.br">https://ctre.com.br</a>
                </p>

                <br>

                <p style="font-size: 15px; color: #787878;">Dados da empresa que vinculou o seu cadastro:</p>
                <p style="font-size: 15px; color: #787878;">CNPJ: {{ $dadosEmail['vinculador_cnpj'] }}</p>
                <p style="font-size: 15px; color: #787878;">Razão Social: {{ $dadosEmail['vinculador_razao_social'] }}</p>
                <p style="font-size: 15px; color: #787878;">E-mail: {{ $dadosEmail['vinculador_email'] }}</p>
                <p style="font-size: 15px; color: #787878;">Telefone: {{ $dadosEmail['vinculador_telefone'] }}</p>
                <p style="font-size: 15px; color: #787878;">Responsável: {{ $dadosEmail['vinculador_responsavel'] }}</p>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop