@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">CTRE – Vinculos Pendentes</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">
                
                <div>
                    <p style="font-size: 17px; color: #787878;margin-bottom: 20px;">Parabéns!</p>
                </div>
                <div>
                    <p style="font-size: 15px; color: #787878;">O cadastro referente a empresa {{$empresa->razao_social}} ({{$empresa->id_limpurb}}) pode prosseguir aos próximos passos:</p>
                </div>
                <div>                                      
                    <h2 style="font-size: 17px; color: #787878;"><strong>Acesso ao sistema </strong></h2>
                    <p style="font-size: 15px; color: #787878;">Você pode acessar o sistema CTR-E com os dados abaixo:</p>
                    <p style="font-size: 15px; color: #787878;"> - Usuário: {{$empresa->users[0]->email}} </p>
                    <p style="font-size: 15px; color: #787878;"> - Senha de acesso: Utilizar a senha enviada anteriormente</p>
                </div>
                <div>
                    <p style="font-size: 15px; color: #787878;">Com esse acesso você deve:</p>
                </div>  
                @if($empresa->empresas_x_empresas_tipos->empresas_tipo->id == \config('enums.empresas_tipo.condominio_misto'))
                {{-- Modelo de email para CM: --}}  
                              
                <div>
                    <h2 style="font-size: 17px; color: #787878;"><strong>Vínculos </strong></h2>
                    <p style="font-size: 15px; color: #787878;">Para finalizar seu cadastro, é necessario você vincular os seguintes entes:</p>
                    <p style="font-size: 15px; color: #787878;">- Um <strong>transportador</strong> privado cadastrado na AMLURB</p>
                    <p style="font-size: 15px; color: #787878;">- Uma <strong>cooperativa</strong> ambiental de resíduos</p>
                    <p style="font-size: 15px; color: #787878;">- Os <strong>grandes geradores</strong> hospedados no seu condomínio</p>    
                </div>                 
              
                @else
                {{-- Modelo de email para GG/OP/SS: --}}
                <div>
                    <h2 style="font-size: 17px; color: #787878;"><strong>Vínculos de transportadores ou condomínio </strong></h2>
                    <p style="font-size: 15px; color: #787878;">Para finalizar seu cadastro, é necessário você vincular os seguintes entes:</p>
                    <p style="font-size: 15px; color: #787878;">- Um <strong>transportador</strong> privado cadastrado na AMLURB</p>
                    <p style="font-size: 15px; color: #787878;">Se você está hospedado dentro de um condomínio misto (comercial…) e que você não tem contrato direto com o transportador, você deve vincular:</p>
                    <p style="font-size: 15px; color: #787878;">- O <strong> condomínio misto</strong> que tem o contrato com o transportador</p>
                </div>  

                @endif

                <div>
                    <p style="font-size: 15px; color: #787878;margin-top:15px;">Enquanto esse vínculo não for feito, o seu cadastro <span style="text-decoration: underline;">não estará finalizado</span> e sua empresa não será ativada no sistema da AMLURB.</p>
                    <p style="font-size: 15px; color: #787878;margin-top:15px;">Você tem <span style="text-decoration: underline; color:red;">{{str_replace('após', '', \Carbon\Carbon::parse($empresa->empresa_aprovacao->data_limte_vinculo)->endOfDay()->diffforhumans(\Carbon\Carbon::now()->startOfDay()))}}</span> para finalizar seu cadastramento. </p>
                    <p style="font-size: 15px; color: #787878;"> Depois deste prazo, seu cadastro sera considerado "Inconsistente".</p>
                </div>
                <div>
                    <h2 style="font-size: 17px; color: #787878;"><strong>QR Code / Adesivo</strong></h2>
                    <p style="font-size: 15px; color: #787878;"><span style="text-decoration: underline;">Você deve imprimir o adesivo de QR Code</span> da sua empresa  efetuando o login no sistema e clicando na aba “Meu Cadastro”. Seu QRCode deverá ficar visível conforme a guia disponível no sistema.</p>
                </div>
              
                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>
    <style>
        .box-conteudo p{
            margin-top:0;
            margin-bottom:0;
            line-height: 25px;
        }
        .box-conteudo h2{
            margin-top:20;
            margin-bottom:0;
            line-height: 25px;
        }
    </style>
@stop