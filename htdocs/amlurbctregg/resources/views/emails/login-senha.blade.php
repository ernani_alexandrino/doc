@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                Acesso ao CTR-E</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo"
                 style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">
                <p style="font-size: 15px; color: #787878;">Olá <strong> {{ $dadosEmail['responsavel'] }}</strong></p>
                <p style="font-size: 15px; color: #787878;">O pagamento da empresa {{ $dadosEmail['razaoSocial'] }} foi realizado com
                    sucesso. Seguem os seus dados de acesso no sistema da Amlurb. Guarde-os em um local seguro e de fácil acesso.
                </p>

                <div class="btn btn-gerar-boleto"
                     style="width: 347px;height: 79px; text-align: left; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px; background-color: #ff6600ab;font-size: 15px;color: #fff;margin: 30px auto;display: block;line-height: 48px;text-decoration: none; padding: 5px 20px;">
                    <p style="padding: 5px 0; color: #FFF; margin: 0; line-height: normal;"><strong style="color:red;">SEU
                            LOGIN:</strong> {{ $dadosEmail['login'] }}
                    </p>
                    <p style="padding: 5px 0; color: #FFF; margin: 0; line-height: normal;"><strong style="color:red;">SUA
                            SENHA:</strong> {{ $dadosEmail['password'] }}
                    </p>
                </div>

                <p style="font-size: 15px; color: #787878;">
                    Sua senha foi gerada automaticamente. Após entrar no sistema, é possível alterá-la acessando
                    as configurações pelo menu lateral da plataforma.
                </p>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop