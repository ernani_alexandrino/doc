@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">2ª Via do Boleto</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;width:100%;">
                <p style="font-size: 15px; color: #787878;">
                    {{ $dadosEmail['empresa']['razao_social'] }}
                </p>
                <p style="font-size: 15px; color: #787878;">
                    Clique no botão abaixo para gerar a 2ª via do boleto.
                </p>

                <br>

                <p style="font-size: 15px; color: #787878;">
                    <a href="{{ (empty($dadosEmail['boleto'])) ? '#' : $dadosEmail['boleto'] }}" 
                       target="_blank">
                        Gerar 2ª Via do Boleto
                    </a>
                </p>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop