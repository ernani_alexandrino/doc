@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">Cadastro de Veículos </br> realizado com sucesso</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;width:100%;">
                <p style="font-size: 15px; color: #787878;">
                    {{ $dadosEmail['empresa']['razao_social'] }}
                </p>
                <p style="font-size: 15px; color: #787878;">
                    Seu veículo foi registrado com sucesso em nosso sistema e seu cadastro será efetivado após o pagamento do boleto.
                </p>

                <br>
                <div>
                    <p>Tipo: {{$dadosEmail['veiculo']['tipo']}}</p>
                    <p>Placa: {{$dadosEmail['veiculo']['placa']}}</p>
                    <p>Código: {{$dadosEmail['veiculo']['codigo_amlurb']}}</p>
                </div>
                <br>

                <p style="font-size: 15px; color: #787878;">
                    Após a aprovação do pagamento, você receberá um e-mail de confirmação.
                </p>

                <p style="font-size: 15px; color: #787878;">
                    É possível que o sistema leve até dois dias úteis para identificar o pagamento.
                </p>

                <p style="font-size: 15px; color: #787878;">
                    Você pode visualizar este e outros boletos pelo sistema em sua
                    <a href="{{config('app.url')}}/painel/transportador/pagamentos" target="_blank">
                        seção de pagamentos do CTRE
                    </a>
                </p>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop