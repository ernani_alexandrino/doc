<div class="assinatura" style=" float: left; width: 100%; border-bottom: 3px solid #f60; padding-bottom: 12px; margin-top: 40px;">
    <p style="font-size: 15px; color: #787878;">
      Caso tenha alguma dúvida sobre cadastro no sistema CTR-E RGG, entre em contato pelo 11 3397-1784 ou pelo canal de atendimento 156 para solicitações gerais.
    </p>
    <p style="font-size: 15px; color: #787878;">
        Atenciosamente,
    </p>
    <p style="font-size: 15px; color: #787878;">
        <strong>Amlurb - Autoridade Municipal de Limpeza Urbana</strong>
    </p>
    <div class="logo-assinatura" style="float: right;">
        <img class="logo-amlurb" src="{{ asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb">
    </div>
</div>