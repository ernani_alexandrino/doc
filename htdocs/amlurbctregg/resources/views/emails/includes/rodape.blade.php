<div class="rodape" style="background: #fff; margin-top: 27px; padding: 10px;">
    @include('emails.includes.email_automatico')
    @if(in_array('boleto', $dadosEmail))
        <p style="font-size: 11px; margin: 0; line-height: 14px;"> Boleto com validade limitada. Verifique a data de vencimento antes de realizar o pagamento.</p>
    @endif

    @include('emails.includes.atendimento')
    @include('emails.includes.direitos')
</div>