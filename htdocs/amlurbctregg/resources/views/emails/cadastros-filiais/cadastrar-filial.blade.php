@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td>
            <h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">
                CADASTRAMENTO DE FILIAL
            </h2>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;">              
                <p style="font-size: 15px; color: #787878;">
                    Olá <strong> 
                            {{ $dadosEmail['responsavel'] }}
                        </strong>,
                </p>
                <p style="font-size: 15px; color: #787878;">                    
                    Sua empresa foi indicada como grande geradora de resíduos e vinculada ao CNPJ
                    <strong>{{ $dadosEmail['vinculador_cnpj'] }}</strong>
                    da empresa
                    <strong>{{ $dadosEmail['vinculador_razao_social'] }}</strong>
                    com número AMLURB
                    <strong>{{ $dadosEmail['id_limpurb_filial'] }}</strong>
                </p>
                <div class="btn btn-gerar-boleto"
                     style="width: 347px;height: 79px; text-align: left; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px; border: 1px solid #ccc;font-size: 15px;color: #fff;margin: 30px auto;display: block;line-height: 48px;text-decoration: none; padding: 5px 20px;">
                    <p style="padding: 5px 0; color: #787878; margin: 0; line-height: normal;"><strong>SEU
                        LOGIN:</strong> {{ $dadosEmail['login'] }}
                    </p>
                    <p style="padding: 5px 0; color: #787878; margin: 0; line-height: normal;"><strong>SUA
                        SENHA:</strong> {{ $dadosEmail['password'] }}
                    </p>
                </div>

                <p style="font-size: 15px; color: #787878;">                    
                    Você agora deve iniciar o cadastramento da sua filial para poder efetuar a correta destinação de resíduos na cidade de São Paulo, de acordo  com o Decreto n°- 58.701, de 04/04/2019.<br>
                    Clique no link abaixo e faça o cadastro.
                </p>
                <a href="https://ctre.com.br" target="_blank"
                   style="background-color: #FFA52A;    width: 280px;height: 40px;color:#fff;padding:9px 12px">
                    CADASTRAR FILIAL
                </a>


                @include('emails.includes.assinatura')
                
            </div>
        </td>
        
    </tr>

@stop