<ul>
    <li>
        <p>
            <strong>Nome da Filial:</strong>
            {{ $dadosEmail['tipoCadastro'] }}
        </p>
    </li>
    <li>
        <p>
            <strong>Protocolo:</strong>
            {{ $dadosEmail['protocolo'] }}
        </p>
    </li>
    <li>
        <p>
            <strong>Código AMLURB:</strong>
            {{ $dadosEmail['id_limpurb'] }}
        </p>
    </li>
    <li>
        <p>
            <strong>Razão Social:</strong>
            {{ $dadosEmail['razaoSocial'] }}
        </p>
    </li>
</ul>