@extends('emails.email-base')

@section('conteudo')

    <tr>
        <td><img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb" width="222" height="auto" style="display: block;"></td>
        <td><h2 style="font-size: 17px; text-transform: uppercase; color: #969696; margin-top: 50px; float: right;">Confirmação de </br> cadastro de veículos</h2></td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="box-conteudo" style="background: #FFF; border-top: 5px solid #f60; padding: 22px 15px; float: left;width:100%;">
                <p style="font-size: 15px; color: #787878;">Olá, {{ $dadosEmail['responsavel'] }}</p>
                <p style="font-size: 15px; color: #787878;">O pagamento do cadastro de veículos da empresa {{ $dadosEmail['razaoSocial'] }} foi realizado com sucesso!</p>
                <p style="font-size: 15px; color: #787878;">

                <table style="width:100%" border="1" style="border-collapse:collapse;">
                    <tr>
                        <th>Veículo</th>
                        <th>Placa</th>
                        <th>Id Amlurb</th>
                        <th>Validade</th>
                    </tr>
                    @foreach($dadosEmail['veiculos'] as $veiculo)
                        <tr>
                            <td>{{$veiculo['tipo']}}</td>
                            <td>{{$veiculo['placa']}}</td>
                            <td>{{$veiculo['id']}}</td>
                            <td>{{$veiculo['validade']}}</td>
                        </tr>
                    @endforeach
                </table>

                </p>

                @include('emails.includes.assinatura')
            </div>
        </td>
    </tr>

@stop