<p>&nbsp;</p>
<!-- cabecalho -->
<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="tituloIndicador tituloIndicador__ctre">
                <span>Status geral CTR-e</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8">
            <ul class="nav nav-pills nav-indicadores-periodo">
                <li role="presentation">
                    <a href="#!" data-periodo="diario" class="opcao-indicador-periodo">dia</a>
                </li>
                <li role="presentation">
                    <a href="#!" data-periodo="semanal" class="opcao-indicador-periodo">semana</a>
                </li>
                <li role="presentation">
                    <a href="#!" data-periodo="mensal" class="opcao-indicador-periodo">mês</a>
                </li>
                <li role="presentation">
                    <a href="#!" data-periodo="trimestral" class="opcao-indicador-periodo">trimestre</a>
                </li>
                <li role="presentation">
                    <a href="#!" data-periodo="anual" class="opcao-indicador-periodo">ano</a>
                </li>
                <li role="presentation">
                    <a href="#!" data-periodo="total" class="opcao-indicador-periodo">total</a>
                </li>
            </ul>
        </div>
    </div>
</div>