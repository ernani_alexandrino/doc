<div id="ctre-invalidacao-div" hidden>

    <div class="row">
        <p class="modal-bts-center">Informe abaixo os motivos de estar invalidando este CTR-E</p>
    </div>

    <div class="row">

        <div class="row text-center" id="warnings-invalidar-ctre">
            <p class="text-warning"></p>
        </div>

        <div class="col-xs-12">
            <textarea id="justificativa-invalidacao-ctre"></textarea>
        </div>

        <div class="col-xs-12">
            <div class="modal-bts-center">
                <button type="button" class="btn btn-default btn-success btn-invalidar" id="btn-confirm-invalidate-ctre">Confirmar</button>
            </div>
        </div>

    </div>

</div>