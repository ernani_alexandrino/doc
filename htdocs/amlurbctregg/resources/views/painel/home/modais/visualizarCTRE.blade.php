<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Informações do CTR-E Emitido por {{$emissor}}</h4>
        </div>

        <div class="modal-body">

            <form id="frmVisualizarCtre" name="frmCtre" class="form-horizontal" novalidate="">

                <!-- primeira linha modal CTRE -->
                <div class="row">

                    <div class="col-xs-3">
                        <div class="">
                            <label for="numero">Nr. CTR-E</label>
                            <input name="numero" id="ctre-vs-numero" class="form-control gerador"
                                   readonly value="{{$ctre->codigo}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-2">
                        <div class="">
                            <label for="numero">Data de Emissão</label>
                            <input name="numero" id="ctre-vs-emissao" class="form-control gerador"
                                   readonly value="{{$ctre->data_emissao->format('d/m/Y')}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="">
                            <label for="numero">Data de Vencimento</label>
                            <input name="numero" id="ctre-vs-vencimento" class="form-control gerador"
                                   readonly value="{{$ctre->data_expiracao}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-2">
                        <div class="">
                            <label for="numero">Status</label>
                            <input name="numero" id="ctre-vs-status" class="form-control gerador"
                                   readonly value="{{$ctre->status->descricao}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-2">
                        <div class="">
                            <label for="numero">Validação</label>
                            <div class="ctre-vs-validacao-div form-control">

                                @if($ctre->empresa_id == $ctre->transportador_id)
                                    <span class="table-status-validacao table-status-validacao__t">T</span>
                                    <span class="table-status-validacao {{ (!is_null($ctre->data_validacao)) ? 'table-status-validacao__g' : '' }} ">G</span>
                                @else <!-- gerador -->
                                    <span class="table-status-validacao table-status-validacao__g">G</span>
                                    <span class="table-status-validacao {{ (!is_null($ctre->data_validacao)) ? 'table-status-validacao__t' : '' }} ">T</span>
                                @endif

                                <span class="table-status-validacao {{ (!is_null($ctre->data_validacao_final)) ? 'table-status-validacao__d' : '' }} ">D</span>

                            </div>
                        </div>
                    </div>

                </div>

                <!-- verifica se usuario logado eh gerador ou transportador -->
                @if(\Illuminate\Support\Facades\Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == \config('enums.empresas_tipo.transportador'))

                    <!-- segunda linha modal CTRE (Transportador logado) -->

                    <div class="row">

                        <div class="col-xs-8">
                            <div class="">
                                <label for="gerador">Gerador</label>
                                <input name="gerador" id="ctre-vs-gerador" class="form-control gerador"
                                       readonly value="{{$ctre->gerador->razao_social}}">
                                <p class="text-danger"></p>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="">
                                <label for="placa_veiculo">Placa do Veículo</label>
                                <input name="placa_veiculo" class="form-control placa_veiculo" id="ctre-vs-placa"
                                    readonly value="{{$ctre->empresas_veiculos->placa or ''}}">
                                <p class="text-danger"></p>
                            </div>
                        </div>

                    </div>

                    <!-- terceira linha modal CTRE (Transportador logado) -->

                    <div class="row">

                        <div class="col-xs-8">
                            <div class="">
                                <label for="destino">Destino Final</label>
                                <input name="destino" id="ctre-vs-destino" class="form-control gerador"
                                       readonly value="{{$ctre->destino_final->razao_social or ''}}">
                                <p class="text-danger"></p>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="">
                                <label for="tipo_veiculo">Tipo de Veículo</label>
                                <input type="text" name="tipo_veiculo" id="ctre-vs-tipo" class="form-control"
                                       readonly value="{{$ctre->empresas_veiculos->tipo or ''}}">
                                <p class="text-danger"></p>
                            </div>
                        </div>

                    </div>

                @else <!-- usuario logado eh gerador -->

                    <!-- segunda linha modal CTRE (Gerador logado) -->

                    <div class="row">

                        <div class="col-xs-8">
                            <div class="">
                                <label for="transportador">Transportador</label>
                                <input name="transportador" id="ctre-vs-transportador" class="form-control transportador"
                                       readonly value="{{$ctre->transportador->razao_social}}">
                                <p class="text-danger"></p>
                            </div>
                        </div>

                        <div class="col-xs-2">
                            <div class="">
                                <label for="placa_veiculo">Placa do Veículo</label>
                                <input name="placa_veiculo" class="form-control placa_veiculo" id="ctre-vs-placa"
                                       readonly value="{{$ctre->empresas_veiculos->placa or ''}}">
                                <p class="text-danger"></p>
                            </div>
                        </div>

                        <div class="col-xs-2">
                            <div class="">
                                <label for="tipo_veiculo">Tipo de Veículo</label>
                                <input type="text" name="tipo_veiculo" id="ctre-vs-tipo" class="form-control"
                                       readonly value="{{$ctre->empresas_veiculos->tipo or ''}}">
                                <p class="text-danger"></p>
                            </div>
                        </div>

                    </div>

                @endif

                <!-- quarta linha modal CTRE (terceira linha se for gerador logado) - equipamentos e residuos -->

                <div class="row">

                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="">
                                    <label for="residuos_vinculo">Resíduos</label>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="equipamentos_vinculo">Acondicionamentos</label>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-6 caixa-incluidos">
                        <div id="vs-residuos-escolhidos" class="residuos-escolhidos">
                            @foreach($ctre->ctre_residuos as $ctre_residuo)
                                <div class="col-xs-11">
                                    <div class="nome-residuo" data-id-residuo="{{$ctre_residuo->residuo->id}}">
                                        {{$ctre_residuo->residuo->nome}}
                                    </div>
                                </div>
                            @endforeach
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 caixa-incluidos">
                        <div id="vs-equipamentos-escolhidos" class="acondicionamentos-escolhidos">
                            @foreach($ctre->ctre_equipamentos as $ctre_equipamento)
                                <div class="col-xs-11">
                                    <div class="nome-equipamento" data-id-equipamento="{{$ctre_equipamento->empresas_x_equipamentos->id}}">
                                        {{
                                            $ctre_equipamento->empresas_x_equipamentos->codigo_amlurb . ' - ' .
                                            $ctre_equipamento->empresas_x_equipamentos->equipamento->tipo_equipamento->nome
                                        }}
                                    </div>
                                </div>
                            @endforeach
                            <p class="text-danger"></p>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success btn-close-ctre" id="btn-close-ctre-vs">Fechar</button>
                        </div>
                    </div>

                </div>

            </form>

        </div>

    </div>
</div>