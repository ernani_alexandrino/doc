<div class="col-xs-12 col-sm-3">
    <div class="box-indicador">
        <div class="image-indicador total-emitido"></div>
        <div class="corpo-indicador">
            <div class="contador" id="total_ctre_emitidos">
                <!-- ajax -->
            </div>
            <div class="titulo-indicador">
                Total CTR </br> Emitidos
            </div>
            <div class="diviser"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-3">
    <div class="box-indicador">
        <div class="image-indicador total-aberto"></div>
        <div class="corpo-indicador">
            <div class="contador" id="total_ctre_aberto">
                <!-- ajax -->
            </div>
            <div class="titulo-indicador">
                Total CTR </br> em Aberto
            </div>
            <div class="diviser"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-3">
    <div class="box-indicador">
        <div class="image-indicador total-finalizados"></div>
        <div class="corpo-indicador">
            <div class="contador" id="total_ctre_finalizado">
                <!-- ajax -->
            </div>
            <div class="titulo-indicador">
                Total CTR </br> Finalizados
            </div>
            <div class="diviser"></div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-3">
    <div class="box-indicador">
        <div class="image-indicador total-expirados"></div>
        <div class="corpo-indicador">
            <div class="contador" id="total_ctre_expirado">
                <!-- ajax -->
            </div>
            <div class="titulo-indicador">
                Total CTR </br> Expirados
            </div>
            <div class="diviser"></div>
        </div>
    </div>
</div>