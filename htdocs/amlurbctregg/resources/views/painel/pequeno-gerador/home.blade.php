@extends('layouts.home-painel-pequeno-gerador')

@section('conteudo')

    <div class="col-xs-12 dados-qrcode">

        @include('painel.meu-cadastro.qrcode')

    </div>
    <form id ="dados_empresa_form">
    <div class="cadastro-gerador">

        @include('painel.meu-cadastro.dados-empresa')
        @include('painel.meu-cadastro.modais.solicitar-alteracao-empresa')

    </div>

    <div class="informacoes-gerador">

        @include('painel.meu-cadastro.informacoes-gerador')
        <div class="row">
            <div class="modal-bts-center">
                <button type="button" id="btn_edit_empresa" name="btn_edit_empresa" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa btn_edit_cadastro">
                    Alterar Cadastro
                </button>
            </div>
        </div> 
    </div>

    <hr>

    <div class="cadastro-credenciais">

        @include('painel.meu-cadastro.dados-credenciais')

    </div>
</div>
@endsection
