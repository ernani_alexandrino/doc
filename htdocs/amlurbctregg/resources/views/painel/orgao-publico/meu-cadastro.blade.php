@extends('layouts.home-painel-orgao-publico')

@section('conteudo')

    @if(\Illuminate\Support\Facades\Auth::user()->empresa->status_id == \config('enums.status.ativo'))
        <div class="col-xs-12 dados-qrcode">

            @include('painel.meu-cadastro.qrcode')

        </div>

        <br><br><br>
    @endif
    <form id ="dados_empresa_form">
    @if(
        ($empresa->empresa_informacao_complementar->tipo_ramo_atividade->id != \config('enums.tipo_atividade.servico_de_administracao_municipal_direta')) &&
        ($empresa->empresa_informacao_complementar->tipo_ramo_atividade->id != \config('enums.tipo_atividade.servico_de_administracao_municipal_indireta')) &&
        (\Illuminate\Support\Facades\Auth::user()->empresa->status_id == \config('enums.status.pagamento_pendente'))
    )
        {{-- Apenas opara Órgãos estaduais e federais --}}
        <div class="col-xs-12">

            @include('painel.meu-cadastro.pagamento')

        </div>
    @endif

    <div class="cadastro-gerador">

        @include('painel.meu-cadastro.dados-empresa')
        @include('painel.meu-cadastro.modais.solicitar-alteracao-empresa')

    </div>

    <hr>

    <div class="informacoes-gerador">

        @include('painel.meu-cadastro.informacoes-gerador')
        <div class="row">
            <div class="modal-bts-center">
                <button type="button" id="btn_edit_empresa" name="btn_edit_empresa" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa btn_edit_cadastro">
                    Alterar Cadastro
                </button>
            </div>
        </div>
    </div>

    <hr>

    <div class="cadastro-credenciais">

        @include('painel.meu-cadastro.dados-credenciais')

    </div>
        </div>
    </form>
@endsection