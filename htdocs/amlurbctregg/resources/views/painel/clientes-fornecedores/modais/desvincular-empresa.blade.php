<div class="modal fade" id="modalDesvincularEmpresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Remover Vínculo</h4>
            </div>
            <div class="modal-body text-center">
                <p>Deseja remover o vínculo com esta empresa?</p>
                <p>Favor informar abaixo o motivo da remoção deste vínculo.</p>
            </div>

            <div class="row text-center" id="warnings-remocao-vinculo">
                <p class="text-warning"></p>
            </div>

            <div class="row text-center">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <div class="form-group">
                        <textarea id="justificativa-remocao-vinculo" class="form-control"></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="modal-bts-center">
                        <button type="button" class="btn btn-default btn-success btn-delete-vinculo" id="btn-delete-vinculo">Confirmar</button>
                        <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-delete-vinculo">Cancelar</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>