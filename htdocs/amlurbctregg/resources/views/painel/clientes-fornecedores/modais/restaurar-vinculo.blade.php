<div class="modal fade" id="modalRestaurarVinculo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Restaurar Vínculo</h4>
            </div>
            <div class="modal-body text-center">
                <p>Deseja restaurar o vínculo com essa empresa?</p>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="modal-bts-center">
                        <button type="button" class="btn btn-default btn-success btn-restore-vinculo" id="btn-restore-vinculo">Confirmar</button>
                        <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-restore-vinculo">Cancelar</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>