<div class="col-xs-12">

    <h2>
        Relação de Vínculos Removidos
    </h2>

    <div class="box-configuracoes">

        <div class="row">

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-vinculos" id="table-vinculos-removidos">
                        <thead>
                        <tr>
                            <th width="20%">NR. AMLURB</th>
                            <th width="50%">EMPRESA</th>
                            <th width="23%">STATUS AMLURB</th>
                            <th class="camp-actions-table">AÇÕES</th>
                        </tr>
                        </thead>
                        <tbody id="vinculos-removidos-list" name="vinculos-removidos-list">
                            <!-- popula via Javascript (Ajax/Datatables) -- atualizaListaVinculosRemovidos() -->
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

</div>