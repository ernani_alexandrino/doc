@extends('layouts.home-painel-condominio-misto')

@section('conteudo')

    @if(\Illuminate\Support\Facades\Auth::user()->empresa->status_id == \config('enums.status.ativo'))
        <div class="col-xs-12 dados-qrcode">

            @include('painel.meu-cadastro.qrcode')

        </div>

        <br><br><br>
    @endif

    @if(
        (\Illuminate\Support\Facades\Auth::user()->empresa->status_id == \config('enums.status.pagamento_pendente'))
    )
    <div class="col-xs-12 dados-qrcode">

        @include('painel.meu-cadastro.pagamento')

    </div>
    @endif
    <form id ="dados_empresa_form">
    <div class="cadastro-gerador">

        @include('painel.meu-cadastro.dados-empresa')
        @include('painel.meu-cadastro.modais.solicitar-alteracao-empresa')

    </div>

    <hr>

    <div class="informacoes-gerador">

        @include('painel.meu-cadastro.informacoes-gerador')

    </div>

    <hr>

    <div class="informacoes-gerador"> <!-- RESPONSÁVEL PELO EMPREENDIMENTO -->

        @include('painel.meu-cadastro.condominio')
        <div class="row">
            <div class="modal-bts-center">
                <button type="button" id="btn_edit_empresa" name="btn_edit_empresa" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa btn_edit_cadastro">
                    Alterar Cadastro
                </button>
            </div>
        </div> 
    </div>

    <hr>

    <div class="cadastro-credenciais">

        @include('painel.meu-cadastro.dados-credenciais')

    </div>
    </form>
@endsection