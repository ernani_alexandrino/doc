@extends('layouts.home-painel-condominio-misto')

@section('conteudo')

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="vinculo-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <div class="col-xs-12 texto-informativo">
        Aqui você cadastra as empresas transportadoras de resíduos que prestam serviços à sua empresa.<br>
        Caso sua empresa esteja localizada dentro de um Condomínio Misto, você deve cadastrá-lo aqui também.
    </div>

    <!-- transportadores -->
    <div>
        @include('painel.vinculos.listas.transportadores')
        @include('painel.vinculos.modais.transportador')
        <div class="modal fade" id="modalEditTransportador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>
    </div>

    <hr>

    <!-- geradores -->
    <div>
        @include('painel.transportador.clientes-fornecedores.geradores-vinculados')
        @include('painel.transportador.clientes-fornecedores.modais.cadastroGerador')
        <div class="modal fade" id="modalEditGerador" tabindex="-1" role="dialog" aria-hidden="true">

        </div>
    </div>

    <hr>

    <!-- Cooperativa -->
    <div>
        @include('painel.vinculos.listas.cooperativas')
        @include('painel.vinculos.modais.cadastroCooperativa')
        <div class="modal fade" id="modalEditCooperativa" tabindex="-1" role="dialog" aria-hidden="true">

        </div>
    </div>

    <hr>

    <!-- destinos finais -->
    <div>
        @include('painel.vinculos.listas.destinos-finais')
        @include('painel.vinculos.modais.destino-final')
        <div class="modal fade" id="modalEditDestino" tabindex="-1" role="dialog" aria-hidden="true">

        </div>
    </div>

    <hr>

    @include('painel.clientes-fornecedores.modais.desvincular-empresa')
    @include('painel.clientes-fornecedores.modais.restaurar-vinculo')

    <!-- vinculos deletados -->
    <div>
        @include('painel.clientes-fornecedores.vinculos-removidos')
    </div>

@stop