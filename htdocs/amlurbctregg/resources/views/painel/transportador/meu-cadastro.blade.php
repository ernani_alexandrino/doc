@extends('layouts.home-painel-transportador')

@section('conteudo')

    @if(\Illuminate\Support\Facades\Auth::user()->empresa->status_id == \config('enums.status.ativo'))
        <div class="informacoes-qrcode">

            @include('painel.meu-cadastro.qrcode')

        </div>

        <br><br><br>
    @endif
    <form id ="dados_empresa_form">
    @if(
        (\Illuminate\Support\Facades\Auth::user()->empresa->status_id == \config('enums.status.pagamento_pendente'))
    )
    <div class="col-xs-12 dados-qrcode">

        @include('painel.meu-cadastro.pagamento')

    </div>
    @endif

    <div class="cadastro-transportador">

        @include('painel.meu-cadastro.dados-empresa')
        @include('painel.meu-cadastro.modais.solicitar-alteracao-empresa')
        @include('painel.meu-cadastro.modais.aviso-de-alteracao')

    </div>

    <hr>
    @include('painel.transportador.meu-cadastro.socios-transportador-final')
    <hr>
    <div class="documentos-transportador">

        @include('painel.transportador.meu-cadastro.documentos-transportador')

    </div>

    <hr>
</form>
    <div class="cadastro-credenciais">

        @include('painel.meu-cadastro.dados-credenciais')

    </div>


@endsection