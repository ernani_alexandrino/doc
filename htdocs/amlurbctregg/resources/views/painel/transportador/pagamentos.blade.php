@extends('layouts.home-painel-transportador')

@section('conteudo')

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="pagamento-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <div class="col-xs-12 texto-informativo">
        Aqui você gerencia os boletos de pagamento de licenças de veículos e equipamentos cadastrados, solicita a 2a via e consulta informações sobre veículos e equipamentos referentes a cada boleto.
    </div>

    @include('painel.pagamentos.boletos')

    <div class="modal fade" id="modalVisualizarBoleto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>

@stop