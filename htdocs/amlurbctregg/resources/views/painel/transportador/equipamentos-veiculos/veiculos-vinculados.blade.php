<div class="col-xs-12">

    <h2>1.Relação de Veículos</h2>

    <div class="box-configuracoes">

        <div class="row">
            <div class="col-xs-8 btn-top-table">
                <div class="modal-bts-center">
                    <button id="btn_add_veiculo" name="btn_add_veiculo" class="btn btn-default modal-bts-center__externo btn-veiculo">
                        Adicionar Veículo
                    </button>
                    <button id="btn_atv_rnv_veiculo" name="btn_atv_rnv_veiculo" class="btn btn-default btn-success modal-bts-center__externo">
                        Ativar/ Renovar Grupo de Veículos
                    </button>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-veiculos" id="table-veiculos-vinculados">
                        <thead>
                            <tr>       
                                <th>NR. AMLURB</th>
                                <th>PLACA</th>
                                <th>ANO</th>
                                <th>RENAVAM</th>
                                <th>TIPO DE VEÍCULO</th>
                                <th>MARCA</th>
                                <th>IPVA</th>
                                <th>CAPACIDADE</th>
                                <th>STATUS</th>
                                <th>VALIDADE</th>
                                <th class="camp-actions-table">AÇÕES</th>
                            </tr>
                        </thead>
                        <tbody id="veiculo-list" name="veiculo-list">
                            <!-- AJAX list veiculos -->
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row" id="veiculos-pagination">

            </div>

            

        </div>

    </div>

</div>