<div class="modal fade" id="modalAprovaReprovaVeiculo">
<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Informações do Veículo</h4>
            </div>
            <div class="modal-body">
            <div class="row justificativa">
                    <div class="col-xs-12">
                        <label style="width: 100%;" for="anexo_documento_inmetro_editar">Justificativa Inconsistência:</label>
                        <p id="alert-text" class="alert alert-text text-center alert-danger">te</p>
                    </div>
                </div>
            <div class="row mensagem-validade hide">
                    <div class="col-xs-12">
                        <p id="alert-text" class="alert alert-text text-center alert-success">O veículo foi aprovado com sucesso.</p>
                    </div>
                </div>
                {{ Form::open(['route' => 'edit.save', 'id' => 'frmVeiculos', 'class' => 'form-horizontal' ]) }}
                   
                    <div class="row margin-bottom-10">

                    <input class="form-control hide" id="id_veiculo"  name="id_veiculo" type="text">
                    <input class="form-control hide" id="gerar_boleto"  name="gerar_boleto" type="text" value="true">
                        <div class="col-xs-4">
                            {!! Form::label('ano_veiculo', 'Ano') !!}
                            {!! Form::text('ano_veiculo', null, ['class' => 'form-control', 'id' => 'ano_veiculo', 'placeholder' => '1999','maxlength'=>'4', ]) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('placa', 'Placa') !!}
                            {!! Form::text('placa', null, ['class' => 'form-control', 'id' => 'placa', 'placeholder' => 'AAA-0000', 'maxlength'=>'8', ]) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('renavam', 'Renavam') !!}
                            {!! Form::text('renavam', null, ['class' => 'form-control', 'id' => 'renavam', 'placeholder' => '00000000000', 'maxlength'=>'11', ]) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-6">
                            {!! Form::label('tipo', 'Tipo de Veículo') !!}
                            {!! Form::text('tipo', null, ['class' => 'form-control', 'id' => 'tipo', 'placeholder' => 'Ex: Caminhão', ]) !!}
                        </div>
                        <div class="col-xs-6">
                            {!! Form::label('marca', 'Marca') !!}
                            {!! Form::text('marca', null, ['class' => 'form-control', 'id' => 'marca', 'placeholder' => 'Ex: Volvo', ]) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-4">
                            {!! Form::label('vencimento_ipva', 'Vencimento IPVA') !!}
                            {!! Form::text('vencimento_ipva', null, ['class' => 'form-control', 'id' => 'vencimento_ipva', 'placeholder' => 'dd/mm/aaaa', ]) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('capacidade', 'Capacidade (tonelada)') !!}
                            {!! Form::text('capacidade', null, ['class' => 'form-control', 'id' => 'capacidade', 'placeholder' => 'Ex: 15,00', ]) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('tara', 'Tara (tonelada)') !!}
                            {!! Form::text('tara', null, ['class' => 'form-control', 'id' => 'tara', 'placeholder' => 'Ex: 10,00', ]) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-4">
                            {!! Form::label('numero_inmetro', 'Número Inmetro') !!}
                            {!! Form::text('numero_inmetro', null, ['class' => 'form-control required', 'id' => 'numero_inmetro', ]) !!}
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label style="width: 100%;" for="anexo_documento_inmetro_editar">Documento Inmetro</label>
                                <input type="hidden" name="anexo_editar_documento_inmetro_enviado" id="anexo_documento_inmetro_enviado_editar"/>
                                <label for="anexo_documento_inmetro_editar" class="btn-upload" id="erro-anexo_documento_inmetro_editar">
                                    <input type="file" name="anexo_documento_inmetro_editar" id="anexo_documento_inmetro_editar"
                                        class="form-control file" value="{{ old('anexo_documento_inmetro') }}"/>
                                    <div class="progress">
                                        <p>Escolha um arquivo </p>
                                        <div class="bar"></div>
                                    </div>
                                    <span><i class="icon fa-upload"></i></span>
                                </label>
                                <p class="text-danger erro-anexo_documento_inmetro"></p>
                                <a href="" id="ver_anexo_documento_inmetro" class="form-control ver-doc" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label style="width: 100%;" for="anexo_documento_veiculo_editar">Documento do Veículo</label>
                                <input type="hidden" name="anexo_editar_documento_veiculo_enviado" id="anexo_editar_documento_veiculo_enviado"/>
                                <label for="anexo_documento_veiculo_editar" class="btn-upload" id="erro-anexo_documento_veiculo_editar">
                                    <input type="file" name="anexo_documento_veiculo_editar" id="anexo_documento_veiculo_editar"
                                        class="form-control file" value="{{ old('anexo_documento_veiculo') }}"/>
                                    <div class="progress">
                                        <p>Escolha um arquivo </p>
                                        <div class="bar"></div>
                                    </div>
                                    <span><i class="icon fa-upload"></i></span>
                                </label>
                                <p class="text-danger erro-anexo_documento_veiculo "></p>
                                <a href="" id="ver_anexo_documento_veiculo" class="form-control ver-doc" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                           
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label style="width: 100%;" for="anexo_comodato_veiculo_editar">
                                    Comodato do veiculo<br>
                                    <span style="font-size: 11px;">(caso não pertenece a sua empresa)</span>
                                </label>
                                <input type="hidden" name="anexo_editar_comodato_veiculo_enviado" id="anexo_editar_comodato_veiculo_enviado"/>
                                <label for="anexo_comodato_veiculo_editar" class="btn-upload" id="erro-anexo_comodato_veiculo_editar">
                                    <input type="file" name="anexo_comodato_veiculo_editar" id="anexo_comodato_veiculo_editar"
                                        class="form-control file" value="{{ old('anexo_comodato_veiculo') }}"/>
                                    <div class="progress">
                                        <p>Escolha um arquivo </p>
                                        <div class="bar"></div>
                                    </div>
                                    <span><i class="icon fa-upload"></i></span>
                                </label>
                                <p class="text-danger erro-anexo_comodato_veiculo "></p>
                                <a href="" id="ver_anexo_comodato_veiculo" class="form-control ver-doc" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                           
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                    </div>

                
            </div>
            <div class="modal-footer">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="">
                            <button type="submit" class="btn btn-default btn-success" id="btn-salvar-editar-veiculo" value="add">Salvar </button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-veiculo">Cancelar</button>
                        </div>
                    </div>

                </div> 
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
