<div class="modal fade" id="modalVeiculoAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Cadastrar novo veículo</h4>
            </div>
            <div class="modal-body">

                <div class="col-xs-12 texto-informativo text-center">
                    <p> 
                        Cadastre aqui seu veículo para emitir seu CTR-E.
                    </p>
                    <p> 
                        Após o cadastro os veículos aparecerão na lista como inativos. Ative-os clicando no ícone verde na listagem de veículos ou no botão ativar/ renovar veículos abaixo da listagem.
                    </p>
                    <span>&nbsp;</span>
                </div>

                   <form id="frmVeiculos" name="frmVeiculos" class="form-horizontal" novalidate="">
                    <div class="row margin-bottom-10">

                        
                        <div class="col-xs-4">
                            {!! Form::label('ano_veiculo', 'Ano') !!}
                            {!! Form::text('ano_veiculo', null, ['class' => 'form-control', 'id' => 'ano_veiculo', 'placeholder' => '1999','maxlength'=>'4']) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('placa', 'Placa') !!}
                            {!! Form::text('placa', null, ['class' => 'form-control', 'id' => 'placa', 'placeholder' => 'AAA-0000', 'maxlength'=>'8']) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('renavam', 'Renavam') !!}
                            {!! Form::text('renavam', null, ['class' => 'form-control', 'id' => 'renavam', 'placeholder' => '00000000000', 'maxlength'=>'11']) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-6">
                            {!! Form::label('tipo', 'Tipo de Veículo') !!}
                            {!! Form::text('tipo', null, ['class' => 'form-control', 'id' => 'tipo', 'placeholder' => 'Ex: Caminhão']) !!}
                        </div>
                        <div class="col-xs-6">
                            {!! Form::label('marca', 'Marca') !!}
                            {!! Form::text('marca', null, ['class' => 'form-control', 'id' => 'marca', 'placeholder' => 'Ex: Volvo']) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-4">
                            {!! Form::label('vencimento_ipva', 'Vencimento IPVA') !!}
                            {!! Form::text('vencimento_ipva', null, ['class' => 'form-control', 'id' => 'vencimento_ipva', 'placeholder' => 'dd/mm/aaaa']) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('capacidade', 'Capacidade (tonelada)') !!}
                            {!! Form::text('capacidade', null, ['class' => 'form-control', 'id' => 'capacidade', 'placeholder' => 'Ex: 15,00']) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('tara', 'Tara (tonelada)') !!}
                            {!! Form::text('tara', null, ['class' => 'form-control', 'id' => 'tara', 'placeholder' => 'Ex: 10,00']) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-4">
                            {!! Form::label('numero_inmetro', 'Número Inmetro') !!}
                            {!! Form::text('numero_inmetro', null, ['class' => 'form-control required', 'id' => 'numero_inmetro']) !!}
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group" style="margin:0;">
                                <label style="width: 100%;" for="anexo_documento_inmetro">Documento Inmetro</label>
                                <input type="hidden" name="anexo_documento_inmetro_enviado" id="anexo_documento_inmetro_enviado"/>
                                <label for="anexo_documento_inmetro" class="btn-upload" id="erro-anexo_documento_inmetro">
                                    <input type="file" name="anexo_documento_inmetro" id="anexo_documento_inmetro"
                                        class="form-control file" value="{{ old('anexo_documento_inmetro') }}"/>
                                    <div class="progress">
                                        <p>Escolha um arquivo </p>
                                        <div class="bar"></div>
                                    </div>
                                    <span><i class="icon fa-upload"></i></span>
                                </label>
                                <p class="text-danger erro-anexo_documento_inmetro"></p>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group" style="margin:0;">
                                <label style="width: 100%;" for="anexo_documento_veiculo">Documento do Veículo</label>
                                <input type="hidden" name="anexo_documento_veiculo_enviado" id="anexo_documento_veiculo_enviado"/>
                                <label for="anexo_documento_veiculo" class="btn-upload" id="erro-anexo_documento_veiculo">
                                    <input type="file" name="anexo_documento_veiculo" id="anexo_documento_veiculo"
                                        class="form-control file" value="{{ old('anexo_documento_veiculo') }}"/>
                                    <div class="progress">
                                        <p>Escolha um arquivo </p>
                                        <div class="bar"></div>
                                    </div>
                                    <span><i class="icon fa-upload"></i></span>
                                </label>
                                <p class="text-danger erro-anexo_documento_veiculo "></p>
                            </div>
                           
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-4">
                            <div class="form-group" style="margin:0;">
                                <label style="width: 100%;" for="anexo_comodato_veiculo">
                                    Comodato do veiculo<br>
                                    <span style="font-size: 11px;">(caso não pertenece a sua empresa)</span>
                                </label>
                                <input type="hidden" name="anexo_comodato_veiculo_enviado" id="anexo_comodato_veiculo_enviado"/>
                                <label for="anexo_comodato_veiculo" class="btn-upload" id="erro-anexo_comodato_veiculo">
                                    <input type="file" name="anexo_comodato_veiculo" id="anexo_comodato_veiculo"
                                        class="form-control file" value="{{ old('anexo_comodato_veiculo') }}"/>
                                    <div class="progress">
                                        <p>Escolha um arquivo </p>
                                        <div class="bar"></div>
                                    </div>
                                    <span><i class="icon fa-upload"></i></span>
                                </label>
                                <p class="text-danger erro-anexo_comodato_veiculo "></p>
                            </div>
                           
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success" id="btn-save-veiculo" value="add">Salvar</button>
                            <input type="hidden" id="veiculo_id" name="veiculo_id" value="0">
                            <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-veiculo">Cancelar</button>
                        </div>
                    </div>

                </div> 
            </div>
        </div>
    </div>
</div>