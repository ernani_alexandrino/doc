<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Ativar / Renovar Veículos</h4>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-xs-12 texto-informativo text-center">
                    <p>
                        Selecione os veículos que deseja ativar ou renovar.
                    </p>
                </div>
            </div>
            <div class="row">

                <form id="frmAtivarRenovarVeiculos" name="frmCtre" class="form-horizontal" novalidate="">

                    <div class="containerAtivarRenovarVeiculos table-responsive">

                        <table class="table table-ativar-veiculos" id="table-ativar-veiculos">


                            <thead>
                                <th>
                                    <input type="checkbox" name="checkbox-veiculo-all" id="select-all-veiculos">
                                </th>
                                <th>NR. AMLURB</th>
                                <th>PLACA</th>
                                <th>ANO</th>
                                <th>RENAVAM</th>
                                <th>TIPO DE VEÍCULO</th>
                                <th>MARCA</th>
                                <th>STATUS</th>
                                <th>VALIDADE</th>
                            </thead>
                            <tbody id="ativar-veiculos-list" name="ativar-veiculos-list">
                                @if(count($veiculos))
                                    @foreach($veiculos as $veiculo)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="checkbox-veiculo" name="checkbox-veiculo" value="{{$veiculo->id}}">
                                            </td>
                                            <td>{{$veiculo->codigo_amlurb}}</td>
                                            <td>{{$veiculo->placa}}</td>
                                            <td>{{$veiculo->ano_veiculo}}</td>
                                            <td>{{$veiculo->renavam}}</td>
                                            <td>{{$veiculo->tipo}}</td>
                                            <td>{{$veiculo->marca}}</td>
                                            <td>{{$veiculo->status->descricao}}</td>
                                            <td>{{$veiculo->data_validade}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9">
                                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                                <div class="alert alert-warning margin-top-15 text-center">
                                                    Você não possui nenhum veículo a ativar ou renovar.
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>


                        </table>

                    </div>

                </form>

            </div>

            <div class="row">

                <div class="col-xs-12">
                    <div class="modal-bts-center">
                        <button type="button" class="btn btn-default btn-success btn-save-ativar-veiculos" id="btn-save-ativar-veiculos-tr">Confirmar</button>
                        <button type="reset" class="btn btn-default btn-danger" id="btn-reset-ativar-veiculos-tr">Cancelar</button>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>