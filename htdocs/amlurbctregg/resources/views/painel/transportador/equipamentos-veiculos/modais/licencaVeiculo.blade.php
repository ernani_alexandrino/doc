<div class="modal fade" id="modalVeiculoLicenca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-xs-12 texto-informativo text-center">
                        <p id="p1"></p>
                        <p>
                            Escolha uma das opções abaixo para receber seu boleto de pagamento da licença de uso do veículo.
                            Caso queira é possível imprimir uma segunda via deste boleto no ítem Pagamentos do menu lateral.
                        </p>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center modal-bts-center__tres">
                            <button type="button" class="btn btn-default btn-success btn-boleto-email" id="btn-boleto-email-tr">Receber Boleto por E-Mail</button>
                            <button type="button" class="btn btn-default btn-success btn-boleto-imprimir" id="btn-boleto-imprimir-tr">Imprimir Boleto</button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-boleto-tr">Cancelar Ativação</button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>