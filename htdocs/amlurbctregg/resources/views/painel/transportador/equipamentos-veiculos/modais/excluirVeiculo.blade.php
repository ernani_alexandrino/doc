<div class="modal fade" id="modalVeiculoExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Excluir Veículo</h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-xs-12 texto-informativo text-center">
                        <p>
                            Atenção, ao confirmar você estará removendo o veículo de sua lista, assim como a licença a ele atrelada.
                            Para adicioná-lo novamente será necessário a emissão de um novo boleto. 
                        </p>
                        <p>
                            Selecione abaixo o motivo da remoção.
                        </p>
                    </div>
                </div>

                <div class="row">

                    <select name="justificativa_exclusao_veiculo" id="justificativa_exclusao_veiculo" class="form-control">
                        <option value="">Selecione</option>
                        <option value="Inutilizado por Idade Limite">Inutilizado por Idade Limite</option>
                        <option value="Venda de Veículo">Venda de Veículo</option>
                        <option value="Sinistro de Veículo">Sinistro de Veículo</option>
                    </select>
                    <br>
                    <p class="text-warning row text-center" id="jev_p"></p>

                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success btn-excluir-veiculo" id="btn-excluir-veiculo-tr">Confirmar</button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-veiculo-tr">Cancelar</button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>