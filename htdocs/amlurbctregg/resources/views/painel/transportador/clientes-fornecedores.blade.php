@extends('layouts.home-painel-transportador')

@section('conteudo')

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="vinculo-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <div class="col-xs-12 texto-informativo">
        Aqui você cadastra as empresas geradoras de resíduos a qual você presta serviços, assim como as empresas de
        destinos finais.
    </div>

    <!-- geradores -->
    <div>
        @include('painel.transportador.clientes-fornecedores.geradores-vinculados')
        @include('painel.transportador.clientes-fornecedores.modais.cadastroGerador')
        <div class="modal fade" id="modalEditGerador" tabindex="-1" role="dialog" aria-hidden="true">

        </div>
    </div>

    <!-- transportadores -->
    <div>
        @include('painel.vinculos.listas.transportadores')
        @include('painel.vinculos.modais.transportador')
        <div class="modal fade" id="modalEditTransportador" tabindex="-1" role="dialog" aria-hidden="true">

        </div>
    </div>

    <hr>

    <!-- destinos finais -->
    <div>
        @include('painel.vinculos.listas.destinos-finais')
        @include('painel.vinculos.modais.destino-final')
        <div class="modal fade" id="modalEditDestino" tabindex="-1" role="dialog" aria-hidden="true"></div>

    </div>

    @include('painel.clientes-fornecedores.modais.desvincular-empresa')
    @include('painel.clientes-fornecedores.modais.restaurar-vinculo')

    <!-- vinculos deletados -->
    <div>
        @include('painel.clientes-fornecedores.vinculos-removidos')
    </div>

@stop