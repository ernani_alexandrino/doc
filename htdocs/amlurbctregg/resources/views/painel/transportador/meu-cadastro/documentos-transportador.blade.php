<div class="documentosEmpresa">

    <h2>2. Documentos</h2>

    <p>
        Nos campos abaixo, você visualiza os documentos de sua empresa. Caso precise alterar algum item, clique no botão "Solicitar Alteração".
    </p>

    <!-- TODO trocar exibicao de documentos pelos documentos da tabela nova -->

    <div class="row">

        <div class="col-xs-12">
            <br/>
            <h3>Capacidade Jurídica</h3>
        </div>

        <div class="col-xs-12 col-sm-4" style="display: none;">
            <div class="form-group">
                <label style="width: 100%;" for="">Cartão CNPJ</label>
                <input type="text" id="" class="form-control" value=""
                       />
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_contrato_social">Contrato Social</label>
                @if(!empty($documentos['transportador_contrato_social']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_contrato_social'], 'empresa' => $empresa->id]) }}" id="ver_transportador_contrato_social" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_contrato_social" class="btn-upload">
                    <input type="hidden" name="transportador_contrato_social_enviado" id="transportador_contrato_social_enviado"/>
                    <input type="file" name="transportador_contrato_social" id="transportador_contrato_social"
                           class="form-control file" value="{{ old('transportador_contrato_social') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_contrato_social'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_doc_ccm">CCM</label>
                @if(!empty($documentos['transportador_doc_ccm']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_doc_ccm'], 'empresa' => $empresa->id]) }}" id="ver_transportador_doc_ccm" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_doc_ccm" class="btn-upload">
                    <input type="hidden" name="transportador_doc_ccm_enviado" id="transportador_doc_ccm_enviado"/>
                    <input type="file" name="transportador_doc_ccm" id="transportador_doc_ccm"
                           class="form-control file" value="{{ old('transportador_doc_ccm') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_doc_ccm'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>

    </div>

    <div class="row">
        <hr style="border-color: #CCCCCC;" />
    </div>

    <div class="row">

        <div class="col-xs-12">
            <h3>Identidade Financeira</h3>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_certidao_falencia_concordata">Certidão Negativa de Falência</label>
                @if(!empty($documentos['transportador_certidao_falencia_concordata']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_falencia_concordata'], 'empresa' => $empresa->id]) }}" id="ver_transportador_certidao_falencia_concordata" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_certidao_falencia_concordata" class="btn-upload">
                    <input type="hidden" name="transportador_certidao_falencia_concordata_enviado" id="transportador_certidao_falencia_concordata_enviado"/>
                    <input type="file" name="transportador_certidao_falencia_concordata" id="transportador_certidao_falencia_concordata"
                           class="form-control file" value="{{ old('transportador_certidao_falencia_concordata') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_certidao_falencia_concordata'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_balanco_financeiro">Balanço Patrimonial</label>
                @if(!empty($documentos['transportador_balanco_financeiro']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_balanco_financeiro'], 'empresa' => $empresa->id]) }}" id="ver_transportador_balanco_financeiro" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_balanco_financeiro" class="btn-upload">
                    <input type="hidden" name="transportador_balanco_financeiro_enviado" id="transportador_balanco_financeiro_enviado"/>
                    <input type="file" name="transportador_balanco_financeiro" id="transportador_balanco_financeiro"
                           class="form-control file" value="{{ old('transportador_balanco_financeiro') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_balanco_financeiro'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_demonstrativo_contabil">Demonstrativo Contábil do Último Exercício</label>
                @if(!empty($documentos['transportador_demonstrativo_contabil']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_demonstrativo_contabil'], 'empresa' => $empresa->id]) }}" id="ver_transportador_demonstrativo_contabil" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_demonstrativo_contabil" class="btn-upload">
                    <input type="hidden" name="transportador_demonstrativo_contabil_enviado" id="transportador_demonstrativo_contabil_enviado"/>
                    <input type="file" name="transportador_demonstrativo_contabil" id="transportador_demonstrativo_contabil"
                           class="form-control file" value="{{ old('transportador_demonstrativo_contabil') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_demonstrativo_contabil'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>

    </div>

    <div class="row">
        <hr style="border-color: #CCCCCC;" />
    </div>

    <div class="row">

        <div class="col-xs-12">
            <h3>Regularidade Fiscal</h3>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_certidao_negativa_municipal">Certidão Negativa Municipal</label>
                @if(!empty($documentos['transportador_certidao_negativa_municipal']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_municipal'], 'empresa' => $empresa->id]) }}" id="ver_transportador_certidao_negativa_municipal" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_certidao_negativa_municipal" class="btn-upload">
                    <input type="hidden" name="transportador_certidao_negativa_municipal_enviado" id="transportador_certidao_negativa_municipal_enviado"/>
                    <input type="file" name="transportador_certidao_negativa_municipal" id="transportador_certidao_negativa_municipal"
                           class="form-control file" value="{{ old('transportador_certidao_negativa_municipal') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_certidao_negativa_municipal'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_certidao_negativa_estadual">Certidão Negativa Estadual</label>
                @if(!empty($documentos['transportador_certidao_negativa_estadual']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_estadual'], 'empresa' => $empresa->id]) }}" id="ver_transportador_certidao_negativa_estadual" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_certidao_negativa_estadual" class="btn-upload">
                    <input type="hidden" name="transportador_certidao_negativa_estadual_enviado" id="transportador_certidao_negativa_estadual_enviado"/>
                    <input type="file" name="transportador_certidao_negativa_estadual" id="transportador_certidao_negativa_estadual"
                           class="form-control file" value="{{ old('transportador_certidao_negativa_estadual') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_certidao_negativa_estadual'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_certidao_negativa_federal">Certidão Negativa Federal</label>
                @if(!empty($documentos['transportador_certidao_negativa_federal']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_federal'], 'empresa' => $empresa->id]) }}" id="ver_transportador_certidao_negativa_federal" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_certidao_negativa_federal" class="btn-upload">
                    <input type="hidden" name="transportador_certidao_negativa_federal_enviado" id="transportador_certidao_negativa_federal_enviado"/>
                    <input type="file" name="transportador_certidao_negativa_federal" id="transportador_certidao_negativa_federal"
                           class="form-control file" value="{{ old('transportador_certidao_negativa_federal') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_certidao_negativa_federal'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_crf_caixa">Certificado de Regularidade Fiscal da Caixa</label>
                @if(!empty($documentos['transportador_crf_caixa']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_crf_caixa'], 'empresa' => $empresa->id]) }}" id="ver_transportador_certidao_negativa_federal" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_crf_caixa" class="btn-upload">
                    <input type="hidden" name="transportador_crf_caixa_enviado" id="transportador_crf_caixa_enviado"/>
                    <input type="file" name="transportador_crf_caixa" id="transportador_crf_caixa"
                           class="form-control file" value="{{ old('transportador_crf_caixa') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_crf_caixa'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
       </div> 
    <div class="row">
        <hr style="border-color: #CCCCCC;" />
    </div>

    <div class="row">

        <div class="col-xs-12">
            <h3>Capacidade Técnica</h3>
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_anotacao_responsabilidade_tecnica">ART</label>
                @if(!empty($documentos['transportador_anotacao_responsabilidade_tecnica']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_anotacao_responsabilidade_tecnica'], 'empresa' => $empresa->id]) }}" id="ver_transportador_anotacao_responsabilidade_tecnica" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_anotacao_responsabilidade_tecnica" class="btn-upload">
                    <input type="hidden" name="transportador_anotacao_responsabilidade_tecnica_enviado" id="transportador_anotacao_responsabilidade_tecnica_enviado"/>
                    <input type="file" name="transportador_anotacao_responsabilidade_tecnica" id="transportador_anotacao_responsabilidade_tecnica"
                           class="form-control file" value="{{ old('transportador_anotacao_responsabilidade_tecnica') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_anotacao_responsabilidade_tecnica'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Emissão</label>
                <input type="text" id="transportador_anotacao_responsabilidade_tecnica_emissao" name="transportador_anotacao_responsabilidade_tecnica_emissao" class="form-control calendario" value="{{ (empty($documentos['transportador_anotacao_responsabilidade_tecnica_emissao'])) ? '' : $documentos['transportador_anotacao_responsabilidade_tecnica_emissao']->format('d/m/Y') }}"
                       />
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                <input type="text" id="transportador_anotacao_responsabilidade_tecnica_vencimento" name="transportador_anotacao_responsabilidade_tecnica_vencimento" class="form-control calendario" value="{{ (empty($documentos['transportador_anotacao_responsabilidade_tecnica_vencimento'])) ? '' : $documentos['transportador_anotacao_responsabilidade_tecnica_vencimento']->format('d/m/Y') }}"
                {{ (empty($documentos['transportador_anotacao_responsabilidade_tecnica_vencimento'])) ? 'disabled' : '' }}   />
            </div>
        </div>
        <div class="col-xs-3 col-md-4">
            <div class="form-group">
                <div class="checkbox">
                    <input type="checkbox"
                           name="transportador_anotacao_responsabilidade_tecnica_vencimento_indeterminada"
                           value="transportador_anotacao_responsabilidade_tecnica_vencimento"
                           class="checkVencimento"
                           id="transportador_anotacao_responsabilidade_tecnica_vencimento_indeterminada" {{ (empty($documentos['transportador_anotacao_responsabilidade_tecnica_vencimento'])) ? 'checked="true"' : '' }} />
                    <label for="transportador_anotacao_responsabilidade_tecnica_vencimento_indeterminada"
                           class="dataIndeterminada"> Data de vencimento indeterminada
                    </label>
                </div>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_rg_responsavel_tecnico">Cópia do RG do Responsável Técnico</label>
                @if(!empty($documentos['transportador_rg_responsavel_tecnico']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_rg_responsavel_tecnico'], 'empresa' => $empresa->id]) }}" id="ver_transportador_rg_responsavel_tecnico" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_rg_responsavel_tecnico" class="btn-upload">
                    <input type="hidden" name="transportador_rg_responsavel_tecnico_enviado" id="transportador_rg_responsavel_tecnico_enviado"/>
                    <input type="file" name="transportador_rg_responsavel_tecnico" id="transportador_rg_responsavel_tecnico"
                           class="form-control file" value="{{ old('transportador_rg_responsavel_tecnico') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_rg_responsavel_tecnico'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="">N° do RG do Resp. Técnico</label>
                <input type="text" id="rg_reponsavel_tecnico" name="rg_reponsavel_tecnico" class="form-control" value="{{ (empty($documentos['transportador_rg_num_responsavel_tecnico'])) ? '' : $documentos['transportador_rg_num_responsavel_tecnico'] }}"
                       />
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="">Nome do Resp. Técnico</label>
                <input type="text" id="nome_reponsavel_tecnico" name="nome_reponsavel_tecnico" class="form-control" value="{{ (empty($documentos['transportador_nome_responsavel_tecnico'])) ? '' : $documentos['transportador_nome_responsavel_tecnico'] }}"
                       />
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_cpf_responsavel_tecnico">Cópia do CPF do Responsável Técnico</label>
                @if(!empty($documentos['transportador_cpf_responsavel_tecnico']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_cpf_responsavel_tecnico'], 'empresa' => $empresa->id]) }}" id="ver_transportador_cpf_responsavel_tecnico" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_cpf_responsavel_tecnico" class="btn-upload">
                    <input type="hidden" name="transportador_cpf_responsavel_tecnico_enviado" id="transportador_cpf_responsavel_tecnico_enviado"/>
                    <input type="file" name="transportador_cpf_responsavel_tecnico" id="transportador_cpf_responsavel_tecnico"
                           class="form-control file" value="{{ old('transportador_cpf_responsavel_tecnico') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_cpf_responsavel_tecnico'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="">N° do CPF do Resp. Técnico</label>
                <input type="text" id="cpf_responsavel_tecnico" name="cpf_responsavel_tecnico" class="form-control" value="{{ (empty($documentos['transportador_cpf_num_responsavel_tecnico'])) ? '' : $documentos['transportador_cpf_num_responsavel_tecnico'] }}"
                       />
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_crea_responsavel_tecnico">Cópia do CREA do Responsável Técnico</label>
                @if(!empty($documentos['transportador_crea_responsavel_tecnico']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_crea_responsavel_tecnico'], 'empresa' => $empresa->id]) }}" id="transportador_crea_responsavel_tecnico" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_crea_responsavel_tecnico" class="btn-upload">
                    <input type="hidden" name="transportador_crea_responsavel_tecnico_enviado" id="transportador_crea_responsavel_tecnico_enviado"/>
                    <input type="file" name="transportador_crea_responsavel_tecnico" id="transportador_crea_responsavel_tecnico"
                           class="form-control file" value="{{ old('transportador_crea_responsavel_tecnico') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_crea_responsavel_tecnico'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="">N° do CREA do Resp. Técnico</label>
                <input type="text" id="transportador_crea_num_responsavel_tecnico" name="transportador_crea_num_responsavel_tecnico" class="form-control" value="{{ (empty($documentos['transportador_crea_num_responsavel_tecnico'])) ? '' : $documentos['transportador_crea_num_responsavel_tecnico'] }}"
                       />
            </div>
        </div>

    </div>
 
    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_certificado_dispensa_licenca">CDL</label>
                @if(!empty($documentos['transportador_certificado_dispensa_licenca']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_certificado_dispensa_licenca'], 'empresa' => $empresa->id]) }}" id="ver_transportador_certificado_dispensa_licenca" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_certificado_dispensa_licenca" class="btn-upload">
                    <input type="hidden" name="transportador_certificado_dispensa_licenca_enviado" id="transportador_certificado_dispensa_licenca_enviado"/>
                    <input type="file" name="transportador_certificado_dispensa_licenca" id="transportador_certificado_dispensa_licenca"
                           class="form-control file" value="{{ old('transportador_certificado_dispensa_licenca') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_certificado_dispensa_licenca'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Emissão</label>
                <input type="text" id="transportador_certificado_dispensa_licenca_emissao" name="transportador_certificado_dispensa_licenca_emissao" class="form-control calendario" value="{{ (empty($documentos['transportador_certificado_dispensa_licenca_emissao'])) ? '' : $documentos['transportador_certificado_dispensa_licenca_emissao']->format('d/m/Y') }}"
                       />
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                <input type="text" id="transportador_certificado_dispensa_licenca_vencimento" name="transportador_certificado_dispensa_licenca_vencimento" class="form-control calendario" value="{{ (empty($documentos['transportador_certificado_dispensa_licenca_vencimento'])) ? '' : $documentos['transportador_certificado_dispensa_licenca_vencimento']->format('d/m/Y') }}"
                {{ (empty($documentos['transportador_certificado_dispensa_licenca_vencimento'])) ? 'disabled' : '' }}   />
            </div>
        </div>
        <div class="col-xs-3 col-md-4">
            <div class="form-group">
                <div class="checkbox">
                    <input type="checkbox"
                           name="transportador_certificado_dispensa_licenca_vencimento_indeterminada"
                           value="transportador_certificado_dispensa_licenca_vencimento"
                           class="checkVencimento"
                           id="transportador_certificado_dispensa_licenca_vencimento_indeterminada" {{ (empty($documentos['transportador_certificado_dispensa_licenca_vencimento'])) ? 'checked="true"' : '' }} />
                    <label for="transportador_certificado_dispensa_licenca_vencimento_indeterminada"
                           class="dataIndeterminada"> Data de vencimento indeterminada
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="licencadefault" style="width: 100%;" for="">Licença</label>
                <select class="form-control" id="licencadefault" name="licencadefault" style="color: #444" >
                    <option value="" {{ (empty($documentos['licencadefault'])) ? 'selected="true"' : '' }} >Selecione apenas uma</option>
                    <option value="17" {{ (!empty($documentos['licencadefault']) && $documentos['licencadefault'] == '17') ? 'selected="true"' : '' }} >Licença Prévia</option>
                    <option value="18" {{ (!empty($documentos['licencadefault']) && $documentos['licencadefault'] == '18') ? 'selected="true"' : '' }} >Licença de Instalação</option>
                    <option value="19" {{ (!empty($documentos['licencadefault']) && $documentos['licencadefault'] == '19') ? 'selected="true"' : '' }} >Licença de Operação</option>
                </select>
                @if($errors->has('licencadefault'))
                    <p class="text-danger">{{ $errors->first('licencadefault') }}</p>
                @endif
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="licenca">Licença</label>
                @if(!empty($documentos['licenca']))
                    <a href="{{ route('file', ['arquivo' => $documentos['licenca'], 'empresa' => $empresa->id]) }}" id="ver_licenca" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="licenka" class="btn-upload">
                    <input type="hidden" name="licenka_enviado" id="licenka_enviado"/>
                    <input type="file" name="licenka" class="licenka" id="licenka"
                           class="form-control file" value="{{ old('licenka') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['licenca'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Emissão</label>
                <input type="text" id="emissaolicenca" name="emissaolicenca" class="form-control calendario" value="{{ (empty($documentos['licenca_emissao'])) ? '' : $documentos['licenca_emissao']->format('d/m/Y') }}"
                       />
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                <input type="text" id="licencavencimento" name="licencavencimento" class="form-control calendario" value="{{ (empty($documentos['licenca_vencimento'])) ? '' : $documentos['licenca_vencimento']->format('d/m/Y') }}"
                {{ (empty($documentos['licenca_vencimento'])) ? 'disabled' : '' }}   />
            </div>
        </div>
        <div class="col-xs-3 col-md-4">
            <div class="form-group">
                <div class="checkbox">
                    <input type="checkbox"
                           name="licencavencimento_indeterminada"
                           value="licencavencimento"
                           class="checkVencimento"
                           id="licencavencimento_indeterminada" {{ (empty($documentos['licenca_vencimento'])) ? 'checked="true"' : '' }} />
                    <label for="licencavencimento_indeterminada"
                           class="dataIndeterminada"> Data de vencimento indeterminada
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_alvara_prefeitura">Alvará Prefeitura</label>
                @if(!empty($documentos['transportador_alvara_prefeitura']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_alvara_prefeitura'], 'empresa' => $empresa->id]) }}" id="ver_transportador_alvara_prefeitura" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_alvara_prefeitura" class="btn-upload">
                    <input type="hidden" name="transportador_alvara_prefeitura_enviado" id="transportador_alvara_prefeitura_enviado"/>
                    <input type="file" name="transportador_alvara_prefeitura" id="transportador_alvara_prefeitura"
                           class="form-control file" value="{{ old('transportador_alvara_prefeitura') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_alvara_prefeitura'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Emissão</label>
                <input type="text" id="transportador_alvara_prefeitura_emissao" name="transportador_alvara_prefeitura_emissao" class="form-control calendario" value="{{ (empty($documentos['transportador_alvara_prefeitura_emissao'])) ? '' : $documentos['transportador_alvara_prefeitura_emissao']->format('d/m/Y') }}"
                       />
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                <input type="text" id="transportador_alvara_prefeitura_vencimento" name="transportador_alvara_prefeitura_vencimento" class="form-control calendario" value="{{ (empty($documentos['transportador_alvara_prefeitura_vencimento'])) ? '' : $documentos['transportador_alvara_prefeitura_vencimento']->format('d/m/Y') }}"
                {{ (empty($documentos['transportador_alvara_prefeitura_vencimento'])) ? 'disabled' : '' }}   />
            </div>
        </div>
        <div class="col-xs-3 col-md-4">
            <div class="form-group">
                <div class="checkbox">
                    <input type="checkbox"
                           name="transportador_alvara_prefeitura_vencimento_indeterminada"
                           value="transportador_alvara_prefeitura_vencimento"
                           class="checkVencimento"
                           id="transportador_alvara_prefeitura_vencimento_indeterminada" {{ (empty($documentos['transportador_alvara_prefeitura_vencimento'])) ? 'checked="true"' : '' }} />
                    <label for="transportador_alvara_prefeitura_vencimento_indeterminada"
                           class="dataIndeterminada"> Data de vencimento indeterminada
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_ctf_ibama">CTF Ibama</label>
                @if(!empty($documentos['transportador_ctf_ibama']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_ctf_ibama'], 'empresa' => $empresa->id]) }}" id="ver_transportador_ctf_ibama" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_ctf_ibama" class="btn-upload">
                    <input type="hidden" name="transportador_ctf_ibama_enviado" id="transportador_ctf_ibama_enviado"/>
                    <input type="file" name="transportador_ctf_ibama" id="transportador_ctf_ibama"
                           class="form-control file" value="{{ old('transportador_ctf_ibama') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_ctf_ibama'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Emissão</label>
                <input type="text" id="transportador_ctf_ibama_emissao" name="transportador_ctf_ibama_emissao" class="form-control calendario" value="{{ (empty($documentos['transportador_ctf_ibama_emissao'])) ? '' : $documentos['transportador_ctf_ibama_emissao']->format('d/m/Y') }}"
                       />
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                <input type="text" id="transportador_ctf_ibama_vencimento" name="transportador_ctf_ibama_vencimento" class="form-control calendario" value="{{ (empty($documentos['transportador_ctf_ibama_vencimento'])) ? '' : $documentos['transportador_ctf_ibama_vencimento']->format('d/m/Y') }}"
                {{ (empty($documentos['transportador_ctf_ibama_vencimento'])) ? 'disabled' : '' }}   />
            </div>
        </div>
        <div class="col-xs-3 col-md-4">
            <div class="form-group">
                <div class="checkbox">
                    <input type="checkbox"
                           name="transportador_ctf_ibama_vencimento_indeterminada"
                           value="transportador_ctf_ibama_vencimento"
                           class="checkVencimento"
                           id="transportador_ctf_ibama_vencimento_indeterminada" {{ (empty($documentos['transportador_ctf_ibama_vencimento'])) ? 'checked="true"' : '' }} />
                    <label for="transportador_ctf_ibama_vencimento_indeterminada"
                           class="dataIndeterminada"> Data de vencimento indeterminada
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label style="width: 100%;" for="transportador_avcb">AVCB</label>
                @if(!empty($documentos['transportador_avcb']))
                    <a href="{{ route('file', ['arquivo' => $documentos['transportador_avcb'], 'empresa' => $empresa->id]) }}" id="ver_transportador_avcb" class="form-control ver-doc" target="_blank">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                @endif
                <label for="transportador_avcb" class="btn-upload">
                    <input type="hidden" name="transportador_avcb_enviado" id="transportador_avcb_enviado"/>
                    <input type="file" name="transportador_avcb" id="transportador_avcb"
                           class="form-control file" value="{{ old('transportador_avcb') }}"/>
                    <div class="progress">
                        <p>Escolha um arquivo {{ (empty($documentos['transportador_avcb'])) ? '' : 'para alterar' }}</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Emissão</label>
                <input type="text" id="transportador_avcb_emissao" name="transportador_avcb_emissao" class="form-control calendario" value="{{ (empty($documentos['transportador_avcb_emissao'])) ? '' : $documentos['transportador_avcb_emissao']->format('d/m/Y') }}"
                       />
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label style="width: 100%;" for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                <input type="text" id="transportador_avcb_vencimento" name="transportador_avcb_vencimento" class="form-control calendario" value="{{ (empty($documentos['transportador_avcb_vencimento'])) ? '' : $documentos['transportador_avcb_vencimento']->format('d/m/Y') }}"
                {{ (empty($documentos['transportador_avcb_vencimento'])) ? 'disabled' : '' }}   />
            </div>
        </div>
        <div class="col-xs-3 col-md-4">
            <div class="form-group">
                <div class="checkbox">
                    <input type="checkbox"
                           name="transportador_avcb_vencimento_indeterminada"
                           value="transportador_avcb_vencimento"
                           class="checkVencimento"
                           id="transportador_avcb_vencimento_indeterminada" {{ (empty($documentos['transportador_avcb_vencimento'])) ? 'checked="true"' : '' }} />
                    <label for="transportador_avcb_vencimento_indeterminada"
                           class="dataIndeterminada"> Data de vencimento indeterminada
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="row" >

        <div class="modal-bts-center">
            <button type="button" id="btn_edit_empresa" name="btn_edit_empresa" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa btn_edit_cadastro ">
                Solicitar Alteração
            </button>
        </div>

    </div>


</div>