<div class="documentosEmpresa">

        <div class="row">
            <div class="col-xs-12">
                <h2>Responsável pelo Empreendimento</h2>
            </div>
        </div>

        <div class="row">
            <div id="ajax-panel"></div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group  {{ ($errors->has('nome_socio')) ? 'error':'' }}">
                    <label for="nome_socio">Nome do Sócio</label>
                    <input type="hidden" name="id_socio_1_alterado" value="{{ $empreendimentoSocioPrincial->id or ''}}" id="id_socio_1" class="form-control">
                    <input type="text" name="nome_socio" value="{{ $empreendimentoSocioPrincial->nome or ''}}" id="nome_socio" class="form-control">
                </div>

                @if($errors->has('nome_socio'))
                <p class="text-danger">{{ $errors->first('nome_socio') }}</p>
                @endif
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="form-group {{ ($errors->has('numero_rg_socio')) ? 'error':'' }}">
                    <label for="rg" >Número do RG do Sócio </label>
                    <input type="text" placeholder="Número do RG do Sócio" value="{{ $empreendimentoSocioPrincial->rg or ''}}" name="numero_rg_socio"
                           id="numero_rg_socio" class="form-control required rg" style="margin-bottom: 10px;">
                    <label for="rg">Cópia do RG do Sócio </label>
                   
                    <label for="numero_rg_socio_file" class="btn-upload">
                        <input type="file" name="numero_rg_socio_file" id="numero_rg_socio_file" class="form-control file" value="{{ old('numero_rg_socio_file') }}" />
                        <div class="progress">
                            <p>Escolha um arquivo</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>
                    @if(isset($documentos['numero_rg_socio_file']) && !empty($documentos['numero_rg_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_2" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                    @endif
                    @if($errors->has('numero_rg_socio_file'))
                    <p class="text-danger">{{ $errors->first('numero_rg_socio_file') }}</p>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group {{ ($errors->has('numero_cpf_socio')) ? 'error':'' }}">
                    <label for="rg">Número do CPF do Sócio </label>
                    <input type="text" placeholder="Número do CPF do Sócio " value="{{ $empreendimentoSocioPrincial->cpf or ''}}" name="numero_cpf_socio" id="numero_cpf_socio" class="form-control required cpf" style="margin-bottom: 10px;">
                    <label for="rg" >Cópia do CPF do Sócio </label>
                    <label for="numero_cpf_socio_file" class="btn-upload">
                        <input type="hidden" value="{{ old('numero_cpf_socio_file_enviado') }}" name="numero_cpf_socio_file_enviado"
                               id="numero_cpf_socio_file_enviado" />
                        <input type="file" value="{{ old('numero_cpf_socio_file') }}" name="numero_cpf_socio_file"
                               id="numero_cpf_socio_file" class="form-control required file">
                        <div class="progress">
                            <p>Escolha um arquivo</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>
                    @if(isset($documentos['numero_cpf_socio_file']) && !empty($documentos['numero_cpf_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_2" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                    @endif
                    @if($errors->has('numero_cpf_socio_file'))
                    <p class="text-danger">{{ $errors->first('numero_cpf_socio_file') }}</p>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="diviser" style=" border-bottom: 1px solid #5d5d5d;"></div>
        </div>

        <div class="row">
            <div id="ajax-panel"></div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group  {{ ($errors->has('nome_socio_2')) ? 'error':'' }}">
                    <label for="numero_cpf_socio_2_file">Nome do Sócio 2</label>
                    <input type="hidden" name="id_socio_2_alterado" value="{{ $socios['id_socio_2'] or ''}}" id="id_socio_2" class="form-control">
                    <input type="text" name="nome_socio_2" id="nome_socio_2" value="{{ $socios['nome_socio_2'] or ''}}" class="form-control">
                </div>

                @if($errors->has('nome_socio_2'))
                <p class="text-danger">{{ $errors->first('nome_socio_2') }}</p>
                @endif
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="form-group {{ ($errors->has('numero_rg_socio_2_file')) ? 'error':'' }}">
                    <label for="rg">Número do RG do Sócio 2</label>
                    <input type="text" placeholder="Número do RG do Sócio 2" value="{{ $socios['numero_rg_socio_2'] or ''}}" name="numero_rg_socio_2"
                           id="numero_rg_socio_2" class="form-control rg" style="margin-bottom: 10px;">
                    <label for="rg">Cópia do RG do Sócio 2</label>
                    <label for="numero_rg_socio_2_file" class="btn-upload">
                        <input type="hidden" value="{{ old('numero_rg_socio_2_file_enviado') }}" name="numero_rg_socio_2_file_enviado"
                               id="numero_rg_socio_2_file_enviado" />
                        <input type="file" name="numero_rg_socio_2_file" id="numero_rg_socio_2_file" class="form-control file"
                               value="{{ old('numero_rg_socio_2_file') }}" />
                        <div class="progress">
                            <p>Escolha um arquivo</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>
                    @if(isset($documentos['numero_rg_socio_2_file']) && !empty($documentos['numero_rg_socio_2_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_2_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_2" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                    @endif
                    @if($errors->has('numero_rg_socio_2_file'))
                    <p class="text-danger">{{ $errors->first('numero_rg_socio_2_file') }}</p>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group {{ ($errors->has('numero_cpf_socio_2_file')) ? 'error':'' }}">
                    <label for="rg">Número do CPF do Sócio 2</label>
                    <input type="text" placeholder="Número do CPF do Sócio 2" value="{{ $socios['numero_cpf_socio_2'] or ''}}" name="numero_cpf_socio_2"
                           id="numero_cpf_socio_2" class="form-control cpf" style="margin-bottom: 10px;">
                    <label for="rg">Cópia do CPF do Sócio 2</label>
                    <label for="numero_cpf_socio_2_file" class="btn-upload">
                        <input type="hidden" value="{{ old('numero_cpf_socio_2_file_enviado') }}" name="numero_cpf_socio_2_file_enviado"
                               id="numero_cpf_socio_2_file_enviado" />
                        <input type="file" value="{{ old('numero_cpf_socio_2_file') }}" name="numero_cpf_socio_2_file" id="numero_cpf_socio_2_file"
                               class="form-control file">
                        <div class="progress">
                            <p>Escolha um arquivo</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>
                    @if(isset($documentos['numero_cpf_socio_2_file']) && !empty($documentos['numero_cpf_socio_2_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_2_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_2" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                    @endif
                    @if($errors->has('numero_cpf_socio_2_file'))
                    <p class="text-danger">{{ $errors->first('numero_cpf_socio_2_file') }}</p>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="diviser" style=" border-bottom: 1px solid #5d5d5d;"></div>
        </div>

        <div class="row">
            <div id="ajax-panel"></div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group  {{ ($errors->has('nome_socio_3')) ? 'error':'' }}">
                    <label for="nome_socio_3">Nome do Sócio 3</label>
                    <input type="hidden" name="id_socio_3_alterado" value="{{ $socios['id_socio_3'] or ''}}" id="id_socio_3" class="form-control">
                    <input type="text" name="nome_socio_3" id="nome_socio_3" value="{{ $socios['nome_socio_3'] or ''}}" class="form-control">
                </div>

                @if($errors->has('nome_socio_3'))
                <p class="text-danger">{{ $errors->first('nome_socio_3') }}</p>
                @endif
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="form-group {{ ($errors->has('numero_rg_socio_3_file')) ? 'error':'' }}">
                    <label for="rg">Número do RG do Sócio 3</label>
                    <input type="text" placeholder="Número do RG do Sócio 3" value="{{ $socios['numero_rg_socio_3'] or ''}}" name="numero_rg_socio_3"
                           id="numero_rg_socio_3" class="form-control rg" style="margin-bottom: 10px;">
                    <label for="rg">Cópia do RG do Sócio 3</label>
                    <label for="numero_rg_socio_3_file" class="btn-upload">
                        <input type="hidden" value="{{ old('numero_rg_socio_3_file_enviado') }}" name="numero_rg_socio_3_file_enviado"
                               id="numero_rg_socio_3_file_enviado" />
                        <input type="file" name="numero_rg_socio_3_file" id="numero_rg_socio_3_file" class="form-control file"
                               value="{{ old('numero_rg_socio_3_file') }}" />
                        <div class="progress">
                            <p>Escolha um arquivo</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>
                    @if(isset($documentos['numero_rg_socio_3_file']) && !empty($documentos['numero_rg_socio_3_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_3_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_2" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                    @endif
                    @if($errors->has('numero_rg_socio_3_file'))
                    <p class="text-danger">{{ $errors->first('numero_rg_socio_3_file') }}</p>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group {{ ($errors->has('numero_cpf_socio_3_file')) ? 'error':'' }}">
                    <label for="rg">Número do CPF do Sócio 3</label>
                    <input type="text" placeholder="Número do CPF do Sócio 3" value="{{ $socios['numero_cpf_socio_3'] or ''}}"
                           name="numero_cpf_socio_3" id="numero_cpf_socio_3" class="form-control cpf" style="margin-bottom: 10px;">
                    <label for="rg">Cópia do CPF do Sócio 3</label>
                    <label for="numero_cpf_socio_3_file" class="btn-upload">
                        <input type="hidden" value="{{ old('numero_cpf_socio_3_file_enviado') }}" name="numero_cpf_socio_3_file_enviado"
                               id="numero_cpf_socio_3_file_enviado" />
                        <input type="file" value="{{ old('numero_cpf_socio_3_file') }}" name="numero_cpf_socio_3_file" id="numero_cpf_socio_3_file" class="form-control file">
                        <div class="progress">
                            <p>Escolha um arquivo</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>
                    @if(isset($documentos['numero_cpf_socio_3_file']) && !empty($documentos['numero_cpf_socio_3_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_3_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_2" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                    @endif
                    @if($errors->has('numero_cpf_socio_3_file'))
                    <p class="text-danger">{{ $errors->first('numero_cpf_socio_3_file') }}</p>
                    @endif
                </div>
            </div>

        </div>
 

