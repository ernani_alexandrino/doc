@extends('layouts.home-painel-transportador')

@section('conteudo')

    @include('painel.transportador.home.indicadores')

    @include('painel.transportador.home.indicadores-periodo')

    @include('painel.transportador.home.ctres')

@endsection
