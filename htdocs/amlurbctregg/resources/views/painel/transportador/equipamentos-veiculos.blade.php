@extends('layouts.home-painel-transportador')

@section('conteudo')

    <div class="col-xs-12 texto-informativo">
        <p>
            Aqui você cadastra os veículos que serão usados no transporte de resíduos e os equipamentos usados para acondicionamento e manuseio de resíduos. 
        </p>
        <p>
            Após o cadastro de um veículo, é preciso ativá-lo para gerar o boleto de pagamento e licença de uso. É possível gerar um único boleto para ativar ou renovar mais de um veículo.
            Para ativar apenas um veículo, clique no primeiro item da coluna "Ações". Para ativar ou renovar um ou mais de um veículo, use o botão "Ativar/Renovar Veículos".
        </p>
    </div>


    @if (Session::has('message'))
    <div class="col col-xs-12">
        <p class='alert alert-{{ Session::get('context') }} btn-flex btn-inline-block col-xs-12'>
            @if (Session::get('context') == 'warning')
                <span class="lamp sprite"></span>
            @endif

            {{ Session::get('message') }}
        </p>
    </div>
@endif



    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="ci-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <!-- veiculos -->
    @include('painel.transportador.equipamentos-veiculos.veiculos-vinculados')

    @include('painel.transportador.equipamentos-veiculos.modais.cadastroVeiculo')
    @include('painel.transportador.equipamentos-veiculos.modais.licencaVeiculo')
    @include('painel.transportador.equipamentos-veiculos.modais.qrcodeVeiculo')
    @include('painel.transportador.equipamentos-veiculos.modais.excluirVeiculo')
    @include('painel.transportador.equipamentos-veiculos.modais.editarVeiculo')
    <div class="modal fade" id="modalAtivarRenovarVeiculos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>

    <div class="col-xs-12 texto-informativo">
        <p>
            Aqui você cadastra os equipamentos que serão usados no transporte de resíduos.
        </p>
        <p>
            Após o cadastro de um equipamento, é preciso ativá-lo para consolidar seus dados no sistema CTR-E.
            Para ativar ou renovar apenas um equipamento, clique no primeiro item da coluna "Ações". Para ativar ou renovar um ou mais de um equipamento, use o botão "Ativar/Renovar Equipamentos".
        </p>
    </div>

    <!-- equipamentos -->

    @include('painel.equipamentos.equipamentos-vinculados')

    @include('painel.equipamentos.modais.cadastrar-equipamento')
    @include('painel.equipamentos.modais.qrcode-equipamento')
    @include('painel.equipamentos.modais.excluir-equipamento')
    @include('painel.equipamentos.modais.ativar-renovar-equipamento')
    
    <div class="modal fade" id="modalAtivarRenovarEquipamentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>

@stop