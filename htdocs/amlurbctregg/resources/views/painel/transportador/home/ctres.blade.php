
<div class="col-xs-12">

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="ctre-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <!-- cabecalho -->
    <div class="row box-emitidas-recebidas">
        <div class="col-xs-12 col-sm-4">
            <div class="titulo-indicador titulo-indicador__emitidas-recebidas"></div>
        </div>

        <div class="col-xs-12 col-sm-8">
            <a id="btn-new-ctre-tr" type="button" class="btn btn-default btn-emitir-ctre" style="font-weight: bold;">
                Emitir CTR-E
            </a>
        </div>
    </div>
    <p>
        Ao clicar no botão  "Emitir CTR-E" ao lado, você emite seus documentos de transporte de forma fácil e rápida.
    </p>
    <p>
        Na tabela abaixo, você visualiza todos os CTRs emitidos e acompanha o status de cada processo de validação.  Mais abaixo, você tem acesso aos CTRs emitidos por seus geradores e consegue validá-los clicando no botão "Validar", na coluna "Validação".  Esta ação é importante para a conclusão de todo processo do CTR-E.
    </p>
</div>

@include('painel.transportador.home.modais.newCTRE')

@include('painel.transportador.home.ctres-emitidos')
<div class="modal fade" id="modalVisualizarCTRE" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!-- AJAX -->
</div>

<div class="row" style="display: none">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div id="ctre-validate-success" class="alert alert-success margin-top-20 text-center">

        </div>
    </div>
</div>

@include('painel.transportador.home.ctres-recebidos')
<div class="modal fade" id="modalValidarCTRE" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!-- AJAX -->
</div>