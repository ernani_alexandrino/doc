<div class="col-xs-12">

    <h2>1. CTR-E Emitidas pelo Transportador</h2>

    <div class="box-configuracoes">

        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-ctres" id="table-ctres-emitidos-transportador">
                        <thead>
                            <tr>
                                <th>DATA</th>
                                <th>NR. CTR-E</th>
                                <th>GERADOR</th>
                                <th>DESTINO</th>
                                <th>STATUS</th>
                                <th class="camp-actions-table">VALIDAÇÃO</th>
                            </tr>
                        </thead>
                        <tbody id="ctre-tr-list" name="ctre-list">
                            <!-- popula via Javascript (Ajax) -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>