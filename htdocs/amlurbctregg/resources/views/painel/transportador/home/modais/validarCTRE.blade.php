<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Validação do CTR-E Emitido pelo Gerador</h4>
        </div>

        <div class="modal-body">

            <form id="frmValidarCtre" name="frmCtre" class="form-horizontal" novalidate="">

                <div class="row">
                    <div class="col-xs-12 texto-informativo text-center">
                        <p>
                            Confira as informações e valide a emissão do CTR-E emitido pelo transportador.
                        </p>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-3">
                        <div class="">
                            <label for="numero">Nr. CTR-E</label>
                            <input name="numero" id="ctre-vs-tr-numero" class="form-control gerador"
                                   readonly value="{{$ctre->codigo}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-2">
                        <div class="">
                            <label for="numero">Data de Emissão</label>
                            <input name="numero" id="ctre-vs-tr-emissao" class="form-control gerador"
                                   readonly value="{{$ctre->data_emissao->format('d/m/Y')}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="">
                            <label for="numero">Data de Vencimento</label>
                            <input name="numero" id="ctre-vs-tr-vencimento" class="form-control gerador"
                                   readonly value="{{$ctre->data_expiracao}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-2">
                        <div class="">
                            <label for="numero">Status</label>
                            <input name="numero" id="ctre-vs-tr-status" class="form-control gerador"
                                   readonly value="{{$ctre->status->descricao}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-2">
                        <div class="">
                            <label for="numero">Validação</label>
                            <div class="ctre-vs-validacao-div form-control">

                                <span class="table-status-validacao table-status-validacao__g">G</span>
                                <span class="table-status-validacao {{ (!is_null($ctre->data_validacao)) ? 'table-status-validacao__t' : '' }} ">T</span>
                                <span class="table-status-validacao {{ (!is_null($ctre->data_validacao_final)) ? 'table-status-validacao__d' : '' }} ">D</span>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-8">
                        <div class="">
                            <label for="gerador">Gerador</label>
                            <input name="gerador" id="ctre-vs-tr-gerador" class="form-control gerador"
                                   readonly value="{{$ctre->gerador->razao_social}}">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="" id="ctre-vl-tr-placa">
                            <label for="placa_veiculo">Placa Veículo</label>
                            <select name="placa_veiculo" class="form-control placa_veiculo" id="vl_placa_veiculo">
                                <option value="" selected>Selecione</option>
                                @foreach($veiculos as $veiculo)
                                    <option value="{{$veiculo}}">{{$veiculo}}</option>
                                @endforeach
                            </select>
                            <p class="text-danger"></p>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-8">
                        <div class="" id="ctre-vl-tr-destino">
                            <label for="destino">Destino</label>
                            <select name="destino" id="vl-destino" class="form-control destino">
                                <option value="">Selecione</option>
                                @foreach($destinos as $destino)
                                    <option value="{{ $destino->id }}">{{ $destino->razao_social }}</option>
                                @endforeach
                            </select>
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="">
                            <label for="tipo_veiculo">Tipo de Veículo</label>
                            <input type="text" name="tipo_veiculo" id="vl_tipo_veiculo" class="form-control" readonly placeholder="Ex: Caminhão">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="" id="ctre-vl-tr-residuos">
                                    <label for="residuos_vinculo">Resíduos</label>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="" id="ctre-vl-tr-equipamentos">
                                    <label for="equipamentos_vinculo">Acondicionamentos</label>
                                    <div class="form-item-linha">
                                        <select name="equipamentos_vinculo" id="equipamentos_vinculo" class="form-control equipamentos_vinculo form-item-linha__item">
                                            <option value="">Selecione</option>
                                            @foreach($equipamentos as $equipamento )
                                                <option value="{{ $equipamento->id }}">
                                                    {{
                                                    $equipamento->codigo_amlurb . ' - ' .
                                                    $equipamento->equipamento->tipo_equipamento->nome . ' - ' .
                                                    $equipamento->equipamento->capacidade
                                                     }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <button id="incluir_equipamentos_vinculo" type="button" class="btn btn-default btn-incluir-equipamento equipamento form-item-linha__bt">Incluir</button>
                                        <label for="incluir_equipamentos_vinculo" class="hidden-element">Incluir</label>
                                    </div>
                                    <p class="text-danger">
                                        @if(!count($equipamentos))
                                            Não há nenhum acondicionamento vinculado a este gerador no momento. Acesse os Cadastros Externos para realizar as vinculações
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-6 caixa-incluidos">
                        <div id="residuos-escolhidos" class="residuos-escolhidos">
                            @foreach($ctre->ctre_residuos as $ctre_residuo)
                                <div class="col-xs-11">
                                    <div class="nome-residuo" data-id-residuo="{{$ctre_residuo->residuo->id}}">
                                        {{$ctre_residuo->residuo->nome}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 caixa-incluidos">
                        <div id="equipamentos-escolhidos" class="equipamentos-escolhidos">

                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-validar" id="btn-validate-ctre">Validar</button>
                            <button type="button" class="btn btn-danger btn-invalidar" id="btn-invalidate-ctre">Invalidar</button>
                            <button type="button" class="btn btn-default btn-success btn-close-ctre" id="btn-close-ctre-vl">Fechar</button>
                        </div>
                    </div>

                </div>

                <br>

                @include('painel.home.modais.invalidar-ctre-div')

            </form>

        </div>

    </div>
</div>