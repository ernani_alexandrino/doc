<div class="modal fade" id="modalNewCTRE" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Emitir Novo CTR-E para o Gerador</h4>
            </div>

            <div class="modal-body">
                <div class="row modal-body__instrucoes">
                    <div class="col-xs-12">
                        Preencha todos os campos para a emissão do seu CTR-E.
                    </div>
                </div>

                <form id="frmCtre" name="frmCtre" class="form-horizontal" novalidate="">

                    {{ csrf_field() }}

                    <div class="row">

                        <div class="col-xs-8">
                            <div class="" id="ctre-tr-gerador">
                                <label for="gerador">Gerador</label>
                                <select name="gerador" id="gerador" class="form-control gerador">
                                    <option value="">Selecione</option>
                                    @foreach($geradores as $gerador )
                                        <option value="{{ $gerador->id }}">
                                            {{ $gerador->razao_social }}
                                        </option>
                                    @endforeach
                                </select>
                                <p class="text-danger"></p>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="" id="ctre-tr-placa">
                                <label for="placa_veiculo">Placa Veículo</label>
                                <select name="placa_veiculo" class="form-control placa_veiculo" id="placa_veiculo">
                                    <option value="" selected>Selecione</option>
                                    @foreach($veiculos as $veiculo)
                                        <option value="{{$veiculo}}">{{$veiculo}}</option>
                                    @endforeach
                                </select>
                                <p class="text-danger"></p>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-8">
                            <div class="" id="ctre-tr-destino">
                                <label for="destino">Destino</label>
                                <select name="destino" id="destino" class="form-control destino">
                                    <option value="">Selecione</option>
                                    @foreach($destinos as $destino)
                                        <option value="{{ $destino->id }}">{{ $destino->razao_social }}</option>
                                    @endforeach
                                </select>
                                <p class="text-danger"></p>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="">
                                <label for="tipo_veiculo">Tipo de Veículo</label>
                                <input type="text" name="tipo_veiculo" id="tipo_veiculo" class="form-control" readonly placeholder="Ex: Caminhão">
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="" id="ctre-tr-residuos">
                                        <label for="residuos_vinculo">Resíduos</label>
                                        <div class="form-item-linha">   
                                            <select name="residuos_vinculo" id="residuos_vinculo" class="form-control residuos_vinculo form-item-linha__item" disabled>
                                                <option value="">Selecione</option>
                                            </select>
                                            <button id="incluir_residuos_vinculo" class="btn btn-default btn-incluir-residuo residuo form-item-linha__bt">Incluir</button>
                                            <label for="incluir_residuos_vinculo" class="hidden-element">Incluir</label>
                                        </div>
                                        <p class="text-danger"></p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="" id="ctre-tr-equipamentos">
                                        <label for="equipamentos_vinculo">Acondicionamentos</label>
                                        <div class="form-item-linha">   
                                            <select name="equipamentos_vinculo" id="equipamentos_vinculo" class="form-control equipamentos_vinculo form-item-linha__item" disabled>
                                                <option value="">Selecione</option>
                                            </select>
                                            <button id="incluir_equipamentos_vinculo" type="button" class="btn btn-default btn-incluir-equipamento equipamento form-item-linha__bt">Incluir</button>
                                            <label for="incluir_equipamentos_vinculo" class="hidden-element">Incluir</label>
                                        </div>
                                        <p class="text-danger"></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 caixa-incluidos">
                            <div id="residuos-escolhidos" class="residuos-escolhidos">

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 caixa-incluidos">
                            <div id="equipamentos-escolhidos" class="equipamentos-escolhidos">

                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="modal-bts-center">
                                <button type="button" class="btn btn-default btn-success btn-save-ctre" id="btn-save-new-ctre-tr">Emitir CTR-E</button>
                                <button type="reset" class="btn btn-default btn-danger" id="btn-reset-new-ctre-tr">Cancelar</button>
                            </div>
                        </div>

                    </div>

                </form>

            </div>

        </div>
    </div>
</div>