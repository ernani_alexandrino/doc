<div class="col-xs-12">

    <h2>2. CTR-E Recebidas de Geradores</h2>

    <div class="box-configuracoes">

        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-ctres" id="table-ctres-recebidos-transportador">
                        <thead>
                            <tr>
                                <th>DATA</th>
                                <th>NR. CTR-E</th>
                                <th>GERADOR</th>
                                <th>RESÍDUOS</th>
                                <th>STATUS</th>
                                <th class="camp-actions-table">VALIDAÇÃO</th>
                            </tr>
                        </thead>
                        <tbody id="ctre-tr-gg-list" name="ctre-list">
                            <!-- popula via Javascript (Ajax) -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>