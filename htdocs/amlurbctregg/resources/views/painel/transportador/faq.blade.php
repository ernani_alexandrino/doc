@extends('layouts.home-painel-transportador')

@section('conteudo')

    <div class="col-xs-12 texto-informativo">
        Aqui você encontra as informações sobre a utilização do sistema CTRE.
    </div>

    <div class="col-xs-12 faq-list">

        <h2>Painel de Controle</h2>
        <ul>
            <li>
                <a href="#" class="faq-list-li" id="faq--tr--1-1">
                    Como consultar o Status Geral CTR-E?
                </a>
            </li>
            <li>
                <a href="#" class="faq-list-li" id="faq--tr--1-2">
                    Como consultar os Cadastros por Período?
                </a>
            </li>
            <li>
                <a href="#" class="faq-list-li" id="faq--tr--1-3">
                    Como consultar a tabela de CTR-E emitidas pelo Transportador?
                </a>
            </li>
            <li>
                <a href="#" class="faq-list-li" id="faq--tr--1-4">
                    Como consultar a tabela de CTR-E recebidas de Geradores?
                </a>
            </li>
        </ul>

        <h2>Meus Equip/ Veículos</h2>
        <ul>
            <li>teste</li>
            <li>teste</li>
        </ul>

        <h2>Clientes/ Fornecedores</h2>
        <ul>
            <li>lorem ipsum</li>
            <li>lorem ipsum</li>
        </ul>

        <h2>Pagamentos</h2>
        <ul>
            <li>kawabanga</li>
            <li>bazinga</li>
        </ul>

    </div>

    <div class="modal fade" id="modalFAQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body modalFAQ-body" id="modalFAQ-body">
                    <!-- AJAX -->
                </div>
            </div>
        </div>
    </div>

@stop