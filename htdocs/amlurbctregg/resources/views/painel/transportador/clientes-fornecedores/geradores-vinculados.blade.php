<div class="col-xs-12">

    <h2>Relação de Geradores Vinculados</h2>

    <div class="box-configuracoes">

        <div class="row">
            <div class="col-xs-8 btn-top-table">
                <div class="modal-bts-center">
                    <button id="btn_add_gerador" name="btn_add_gerador" class="btn btn-default modal-bts-center__externo btn-gerador">
                        Vincular Gerador
                    </button>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-geradores table-padrao" id="table-geradores-vinculados">
                        <thead>
                        <tr>
                            <th>NR. AMLURB</th>
                            <th>CNPJ</th>
                            <th>EMPRESA</th>
                            <th>EQUIP.</th>
                            <th>RESÍDUOS</th>
                            <th>FREQUÊNCIA</th>
                            <th>STATUS AMLURB</th>
                            
                            <th class="camp-actions-table">AÇÕES</th>
                        </tr>
                        </thead>
                        <tbody id="gerador-list" name="gerador-list" class="vinculos-list">
                            <!-- popula via Javascript (Ajax/Datatables) -- atualizaListaVinculosGeradorTransportador() -->
                        </tbody>
                    </table>
                </div>
            </div>
           

        </div>

    </div>

</div>