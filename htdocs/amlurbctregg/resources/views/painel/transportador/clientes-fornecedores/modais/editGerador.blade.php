<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Informações do Geradore Vinculado</h4>
        </div>

        <div class="modal-body">

            <form id="frmEditVincularGerador" name="frmCtre" class="form-horizontal" novalidate="">

                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-4" id="tr-edit-cnpj-gerador">
                        <label for="cnpj_vincular_gerador">CNPJ</label>
                        <input type="text" name="cnpj_vincular" id="cnpj_vincular_gerador"
                               class="form-control checkCNPJ cnpj" placeholder="00.000.000/0000-00"
                                readonly value="{{$vinculo->cnpj}}">
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4" id="tr-amlurb-gerador">
                        <label for="amlurb_vincular_gerador">NR. AMLURB</label>
                        <input type="text" name="amlurb" id="amlurb_vincular_gerador"
                               class="form-control" readonly value="{{$vinculo->id_limpurb}}">
                    </div>

                    <div class="col-sm-4" id="tr-status-gerador">
                        <label for="status_vincular_gerador">Status Amlurb</label>
                        <input type="text" name="status" id="status_vincular_gerador"
                               class="form-control" readonly value="{{$vinculo->status->descricao}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" id="tr-razaosocial-gerador">
                        <label for="razao_social_gerador">Razão Social</label>
                        <input type="text" name="razao_social" id="razao_social_gerador" class="form-control"
                            readonly value="{{$vinculo->razao_social}}">
                    </div>

                    <div class="col-sm-6" id="tr-nomefantasia-gerador">
                        <label for="nome_fantasia_gerador">Nome Fantasia</label>
                        <input type="text" name="nome_fantasia" id="nome_fantasia_gerador" class="form-control"
                            readonly value="{{$vinculo->nome_comercial}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" id="tr-responsavel-gerador">
                        <label for="responsavel_gerador">Nome do Contato</label>
                        <input type="text" name="responsavel" id="responsavel_gerador" class="form-control"
                            value="{{$vinculo->empresa_vinculada->empresa_responsavel->nome}}">
                        <p class="text-danger"></p>
                    </div>

                    <div class="col-sm-6" id="tr-email-gerador">
                        <label for="email_gerador">E-mail de Contato</label>
                        <input type="email" name="email" id="email_gerador" class="form-control"
                               value="{{$vinculo->empresa_vinculada->empresa_responsavel->email}}">
                        <p class="text-danger"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label for="telefone_gerador">Telefone</label>
                        <input type="tel" name="telefone" id="telefone_gerador" class="form-control"
                               placeholder="(00) 0000-0000" value="{{$vinculo->empresa_vinculada->empresa_responsavel->telefone}}">
                    </div>

                    <div class="col-sm-6">
                        <label for="coleta_diaria" class="required">Frequência de Coleta</label>
                        <select name="coleta_diaria" id="coleta_diaria" class="form-control destino">
                            <option value="">Selecione</option>
                            @foreach($frequenciaColeta as $dados)
                                @if($dados->id == $vinculo->empresa_vinculada->frequencia_coleta_id)
                                    <option value="{{ $dados->id }}" selected>{{ $dados->nome }}</option>
                                @else
                                    <option value="{{ $dados->id }}">{{ $dados->nome }}</option>
                                @endif
                            @endforeach
                        </select>
                        <p class="text-danger"></p>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="" id="tr-residuos-gerador">
                                    <label for="residuos_vinculo">Resíduos Coletados</label>
                                    <div class="form-item-linha">
                                        <select name="residuos_vinculo" id="residuos_vinculo" class="form-control residuos_vinculo form-item-linha__item">
                                            <option value="">Selecione</option>
                                            @foreach($residuos as $residuo )
                                                <option value="{{ $residuo->id }}">
                                                    {{ $residuo->nome }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <button id="incluir_residuos_vinculo" class="btn btn-default btn-incluir-residuo residuo form-item-linha__bt">Incluir</button>
                                        <label for="incluir_residuos_vinculo" class="hidden-element">Incluir</label>
                                    </div>
                                    <p class="text-danger"></p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="" id="tr-equipamentos-gerador">
                                    <label for="equipamentos_vinculo">Equipamentos Vinculados</label>
                                    <div class="form-item-linha">
                                        <select name="equipamentos_vinculo" id="equipamentos_vinculo" class="form-control equipamentos_vinculo form-item-linha__item">
                                            <option value="">Selecione</option>
                                            @foreach($equipamentos as $equipamento )
                                                <option value="{{ $equipamento->id }}">
                                                    {{
                                                    $equipamento->codigo_amlurb . ' - ' .
                                                    $equipamento->equipamento->tipo_equipamento->nome . ' - ' .
                                                    $equipamento->equipamento->capacidade
                                                     }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <button id="incluir_equipamentos_vinculo" type="button" class="btn btn-default btn-incluir-equipamento equipamento form-item-linha__bt">Incluir</button>
                                        <label for="incluir_equipamentos_vinculo" class="hidden-element">Incluir</label>
                                    </div>
                                    <p class="text-danger"></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-6 caixa-incluidos">
                        <div id="residuos-escolhidos" class="residuos-escolhidos">
                            @foreach($vinculo->logistica_residuos as $lr)
                                <input name="residuo_escolhido[]" id="residuo_{{$lr->residuo_id}}" value="{{$lr->residuo_id}}" type="hidden">
                            @endforeach
                            @foreach($vinculo->logistica_residuos as $lr)
                                <div>
                                    <div class="col-xs-11">
                                        <div class="nome-residuo" data-id-residuo="{{$lr->residuo_id}}">
                                            {{$lr->residuo->nome}}
                                        </div>
                                    </div>
                                    <div class="col-xs-1">
                                        <div id="excluir-residuo-vinculo" class="excluir-residuo-vinculo excluir-ico" data-id-residuo="{{$lr->residuo_id}}">X</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 caixa-incluidos">
                        <div id="equipamentos-escolhidos" class="equipamentos-escolhidos">
                            @foreach($equipamentos_alocados as $equipamento)
                                <input name="equipamento_escolhido[]" id="equipamento_{{$equipamento->id}}"
                                       value="{{$equipamento->id}}" type="hidden">
                            @endforeach
                            @foreach($equipamentos_alocados as $equipamento)
                                <div>
                                    <div class='col-xs-11'>
                                        <div class='nome-equipamento' data-id-equipamento="{{$equipamento->id}}">
                                            {{
                                                $equipamento->codigo_amlurb . ' - ' . $equipamento->equipamento->tipo_equipamento->nome
                                            }}
                                        </div>
                                    </div>
                                    <div class='col-xs-1'>
                                        <div id='excluir-equipamento' class='excluir-equipamento-vinculo excluir-ico' data-id-equipamento="{{$equipamento->id}}">X</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success btn-save-gerador" id="btn-save-edit-gerador-tr">Salvar</button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-reset-edit-gerador-tr">Cancelar</button>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>
</div>