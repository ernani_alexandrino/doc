<div class="col-xs-12 col-sm-2">
    <div class="row">

        <div class="menu-lateral">

            <img class="logo-ctre-menu" src="{{ asset('images/novo-logo-letra-em-branco.svg') }}" alt="CTRE - Controle de Transporte de Resíduos - Eletrônico">

            <div class="user" style="display:none;">
                @if(empty(Auth::user()->user_perfils->imagem_perfil))
                    <div class="img-user">

                    </div>
                @else
                    <div class="img-user img-circle" style="background-image: url({{ asset('uploads/imagens_perfil/'.Auth::user()->user_perfils->imagem_perfil) }});">

                    </div>
                @endif
                <div class="inf-user">
                    <p>{{ auth()->user()->name }}</p>
                    <span></span>
                </div>
            </div>

            <ul class="nav nav-pills nav-stacked">
            @if(Auth::user()->empresa->status_id == \config('enums.status.ativo') || Auth::user()->empresa->status_id ==  \config('enums.status.vinculos_pendentes') )
                    <li><a href="{{ route('painel_tr')}}" id="m_painel_tr"><i class="painel-controle"></i>Painel de Controle</a></li>
                    <li><a href="{{ route('eq_tr') }}" id="m_eq_tr"><i class="icon-meus-equipamentos-internos"></i>Meus Equip/Veículos</a></li>
                    <li><a href="{{ route('cf_tr') }}" id="m_cf_tr"><i class="icon-clientes-fornecedores"></i>Clientes/Fornecedores</a></li>
                    <li><a href="{{ route('pg_tr') }}" id="m_pg_tr"><i class="pagamentos"></i>Pagamentos</a></li>
                @endif
                <li><a href="{{ route('mc_tr') }}" id="m_mc_tr"><i class="icon-meu-cadastro"></i>Meu Cadastro</a></li>
                <!-- item abaixo ocultado temporariamente para testes -->
                <!--<li><a href="{{ route('faq_tr') }}" id="faq_tr"><i class="faq"></i>FAQ</a></li>-->
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="logout"></i>Logout
                    </a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>

        </div>
    </div>
</div>
