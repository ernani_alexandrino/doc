<div class="col-xs-12">

    <h2>
        @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.transportador'))
            1
        @else
            2
        @endif
        .Relação de Equipamentos
    </h2>

    <div class="box-configuracoes">

        <div class="row">
            <div class="col-xs-8 btn-top-table">
                <div class="modal-bts-center">
                    <button id="btn_add_equipamento" name="btn_add" class="btn btn-default modal-bts-center__externo btn-equipamento">
                        Adicionar Equipamento
                    </button>
                    <button id="btn_atv_rnv_equipamento" name="btn_atv_rnv_equipamento" class="btn btn-default btn-success modal-bts-center__externo">
                        Ativar/ Renovar Equipamento
                    </button>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-equipamentos" id="table-equipamentos-vinculados">
                        <thead>
                        <tr>
                            <th>NR. AMLURB</th>
                            <th width="40%">NOME</th>
                            <th>VÍNCULO</th>
                            <th>STATUS</th>
                            <th>VALIDADE</th>
                            <th class="camp-actions-table">AÇÕES</th>
                        </tr>
                        </thead>
                        <tbody id="equipamento-list" name="equipamento-list">
                        <!-- AJAX LIST EQUIPAMENTOS -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="diviser"></div>
        </div>

        <div class="clearfix"></div>

    </div>

</div>