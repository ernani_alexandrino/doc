<div class="modal fade" id="modalEquipamentoExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Excluir Equipamento</h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-xs-12 texto-informativo text-center">
                        <p>
                            ATENÇÃO: Ao confirmar, você estará removendo o equipamento de sua lista, assim como sua licença.
                        </p>
                        <p>
                            Para prosseguir, selecione o motivo da remoção e confirme.
                        </p>
                    </div>
                </div>

                <div class="row">

                    <select name="justificativa_exclusao_equipamento" id="justificativa_exclusao_equipamento" class="form-control">
                        <option value="">Selecione</option>
                        <option value="Venda de Equipamento">Venda de Equipamento</option>
                        <option value="Deixado em Estoque">Deixado em Estoque</option>
                        <option value="Sucateamento de Equipamento">Sucateamento de Equipamento</option>
                    </select>
                    <br>
                    <p class="text-warning row text-center" id="jee_p"></p>

                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success btn-excluir-equipamento" id="btn-excluir-equipamento">Confirmar</button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-excluir-equipamento">Cancelar</button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>