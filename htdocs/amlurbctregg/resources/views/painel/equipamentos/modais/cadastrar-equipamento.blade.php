<div class="modal fade" id="modalEquipamentoAdicionar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Cadastrar novo equipamento</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12 texto-informativo text-center">
                    <p> 
                        Cadastre aqui seus equipamentos para emitir o CTR-E. 
                    </p>
                    <p> 
                        Após o cadastro, os equipamentos aparecerão na listagem como inativos. Para ativá-los, clique no ícone verde na listagem de equipamentos ou no botão "Renovar/Ativar Equipamentos"
                    </p>
                    <span>&nbsp;</span>
                </div>
                <form id="frmEquipamentos" name="frmEquipamentos" class="form-horizontal" novalidate="">
                    <div class="row">
                        <div class="col-xs-8">
                            <label for="equipamentos">Equipamento</label>
                            <select name="equipamentos" class="form-control equipamento" id="equipamentos">
                                <option value="">Selecione um equipamento</option>
                                @foreach($equipamentos as $equipamento)
                                    <option value="{{$equipamento->id}}">
                                        {{ $equipamento->tipo_equipamento->nome . ' - ' . $equipamento->capacidade}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-4">
                            <label for="quantidade">Quantidade</label>
                            <input type="text" name="quantidade" id="quantidade" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success" id="btn-save" value="add">Salvar</button>
                            <input type="hidden" id="product_id" name="product_id" value="0">
                            <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-equipamento">Cancelar</button>
                        </div>
                    </div>

                </div>             
            </div>
        </div>
    </div>
</div>