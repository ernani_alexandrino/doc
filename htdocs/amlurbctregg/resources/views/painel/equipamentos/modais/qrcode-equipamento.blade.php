<div class="modal fade" id="modalEquipamentoQrcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Etiqueta adesiva de fiscalização ambiental de uso obrigatório</h4>
            </div>

            <div class="modal-body container-qrcode">

                <div class="row">
                    <div class="col-xs-3 texto-informativo text-center">
                        <img class="" src="{{ asset('images/qr-code.png') }}" alt="QRcode">
                    </div>
                    <div class="col-xs-9 texto-informativo text-justify">
                        <p>
                            A etiqueta adesiva de fiscalização com a tecnologia QR Code é um recurso obrigatório para o controle de empresas autorizadas pela Amlurb. 
                        </p>
                        <p>
                            Baixe aqui o arquivo para a impressão da etiqueta. A impressão deve ser feita em alta qualidade, por uma gráfica de sua preferência, no formato 10,5 cm x 14,8 cm (A6), em 4 cores, no material de vinil ou BOPP sem transparência. 
                        </p>
                        
                        <p>
                            <a href="#">Consulte aqui as regras de uso da etiqueta QR Code </a>
                            [Decreto nº - 58.701, de 04/04/2019].
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 texto-informativo text-center" id="form-checkbox-qrcode">
                        <p>&nbsp;</p>
                        <p>
                            <input type="checkbox" name="" id="concordancia-qrcode">
                            Li e concordo com as regras de uso da etiqueta QR Code de fiscalização ambiental.
                        </p>
                        <p class="text-danger"></p>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center modal-bts-center__tres bt-x-grande">
                            <button type="button" class="btn btn-default btn-success btn-qrcode-equipamento" id="btn-qrcode-equipamento">
                                Baixar o arquivo da etiqueta
                            </button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-qrcode">
                                Cancelar
                            </button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>