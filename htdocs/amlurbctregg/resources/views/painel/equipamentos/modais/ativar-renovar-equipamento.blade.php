<div class="modal fade" id="modalAtivarRenovarEquipamento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>

            <div class="modal-body">

                <div class="row">
                    <div class="col-xs-12 texto-informativo text-center">
                        <p id="p1">
                        </p>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success" id="btn-confirmar-equipamento">Confirmar</button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-equipamento">Cancelar</button>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>