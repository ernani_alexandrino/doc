<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Ativar / Renovar Equipamentos</h4>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-xs-12 texto-informativo text-center">
                    <p>
                        Aqui você seleciona o(s) equipamento(s) que deseja ativar ou renovar, podendo gerar um único boleto para mais de um equipamento.
                    </p>
                </div>
            </div>

            <div class="row">

                <div class="containerAtivarRenovarVeiculos">

                    <form id="frmAtivarRenovarEquipamentos" name="frmCtre" class="form-horizontal" novalidate="" class="table-responsive">

                        <table class="table table-ativar-equipamentos" id="table-ativar-equipamentos">
                            <thead>
                                <th>
                                    <input type="checkbox" name="checkbox-equipamento-all" id="select-all-equipamentos">
                                </th>
                                <th>NR. AMLURB</th>
                                <th width="50%">NOME</th>
                                <th>STATUS</th>
                                <th>VALIDADE</th>
                            </thead>
                            <tbody id="ativar-equipamentos-list" name="ativar-equipamentos-list">
                                @if(count($equipamentos))
                                    @foreach($equipamentos as $equipamento)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="checkbox-equipamento" name="checkbox-equipamento" value="{{$equipamento->id}}">
                                            </td>
                                            <td>{{$equipamento->codigo_amlurb}}</td>
                                            <td>{{$equipamento->equipamento->tipo_equipamento->nome}}</td>
                                            <td>{{$equipamento->status->descricao}}</td>
                                            <td>{{$equipamento->data_validade}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">
                                            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                                <div class="alert alert-warning margin-top-15 text-center">
                                                    Você não possui nenhum equipamento a ativar ou renovar.
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                    </form>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">
                    <div class="modal-bts-center">
                        <button type="button" class="btn btn-default btn-success btn-save-ativar-equipamentos" id="btn-save-ativar-equipamentos">Confirmar</button>
                        <button type="reset" class="btn btn-default btn-danger" id="btn-reset-ativar-equipamentos">Cancelar</button>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>