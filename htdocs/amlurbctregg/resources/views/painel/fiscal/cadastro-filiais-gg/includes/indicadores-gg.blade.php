<div class="col-xs-12">
    <div class="row">
        <div class="indicadores-fs-cad-gg" id="indicador_periodo_gerador_fiscal">

            <div class="col-xs-12">
                <div class="row">

                    <div class="col-xs-12 col-sm-4">
                        <h3>{{ $dadosCadastrais->nome_comercial or '' }}</h3>
                        <p>{{ $dadosCadastrais->razao_social or '' }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <ul class="nav nav-pills nav-indicadores-periodo">
                            <li role="presentation">
                                <a href="#!" data-periodo="diario" class="opcao-indicador-periodo">dia</a>
                            </li>
                            <li role="presentation">
                                <a href="#!" data-periodo="semanal" class="opcao-indicador-periodo">semana</a>
                            </li>
                            <li role="presentation">
                                <a href="#!" data-periodo="mensal" class="opcao-indicador-periodo">mês</a>
                            </li>
                            <li role="presentation">
                                <a href="#!" data-periodo="trimestral" class="opcao-indicador-periodo">trimestre</a>
                            </li>
                            <li role="presentation">
                                <a href="#!" data-periodo="semestral" class="opcao-indicador-periodo">semestre</a>
                            </li>
                            <li role="presentation">
                                <a href="#!" data-periodo="anual" class="opcao-indicador-periodo">ano</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-sm-3">
                <div class="box-indicador">
                    <div class="image-indicador total-emitido"></div>
                    <div class="corpo-indicador">
                        <div class="contador count" id="total_ctre_emitido">
                            {{ $ctre->emitidos or 0 }}
                        </div>
                        <div class="titulo-indicador">
                            CTR Emitidos
                        </div>
                        <div class="diviser"></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="box-indicador">
                    <div class="image-indicador total-aberto"></div>
                    <div class="corpo-indicador">
                        <div class="contador count" id="total_ctre_aberto">
                            {{ $ctre->abertos or 0 }}
                        </div>
                        <div class="titulo-indicador">
                            CTR Em Aberto
                        </div>
                        <div class="diviser"></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="box-indicador">
                    <div class="image-indicador total-finalizados"></div>
                    <div class="corpo-indicador">
                        <div class="contador count" id="total_ctre_finalizado">
                            {{ $ctre->finalizados or 0 }}
                        </div>
                        <div class="titulo-indicador">
                            CTR Finalizados
                        </div>
                        <div class="diviser"></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="box-indicador">
                    <div class="image-indicador total-expirados"></div>
                    <div class="corpo-indicador">
                        <div class="contador count" id="total_ctre_expirado">
                            {{ $ctre->expirados or 0 }}
                        </div>
                        <div class="titulo-indicador">
                            CTR Expirados
                        </div>
                        <div class="diviser"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>