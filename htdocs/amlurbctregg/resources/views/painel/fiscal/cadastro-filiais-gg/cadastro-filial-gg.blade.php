@extends('layouts.home-painel-fiscal') @section('conteudo')

<h1></span></h1>

<div class="col-xs-12">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h2>Relação de Filiais</h2>
        </div>
        <div class="col-xs-12 col-sm-6">
            @include('painel.fiscal.cadastro-filiais-gg.includes.search')
        </div>
    </div>

    <div class="box-rastreabilidade">

        <div class="row">
            <div class="col-xs-12">

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="lista-empresas">
                        <div class="table-responsive">
                            <table class="table table-hover fiscal-cadastros table-white" id="cadastro_filiais_grandes_geradores">
                                <thead>
                                    <tr>
                                        <th>Nr. Amlurb</th>
                                        <th>Filial</th>
                                        <th>Responsável</th>
                                        <th>E-mail</th>
                                        <th>Status Amlurb</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="cursor-pointer odd" role="row">
                                        <td class="cadastroFilial">
                                            <div class="amlurb_id" data-id="18081">GG2525329/2017</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="nome_comercial">nome filial</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="razao_social">nome responsavel</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="email">teste@teste.com</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="endereco">Ativo</div>
                                        </td>
                                        <td>
                                            <div class="acao text-center">
                                                <a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="1001" title="Inativar empresa">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="cursor-pointer odd" role="row">
                                        <td class="cadastroFilial">
                                            <div class="amlurb_id" data-id="18081">GG2525329/2017</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="nome_comercial">nome filial</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="razao_social">nome responsavel</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="email">teste@teste.com</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="endereco">Ativo</div>
                                        </td>
                                        <td>
                                            <div class="acao text-center">
                                                <a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="1001" title="Inativar empresa">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="cursor-pointer odd" role="row">
                                        <td class="cadastroFilial">
                                            <div class="amlurb_id" data-id="18081">GG2525329/2017</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="nome_comercial">nome filial</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="razao_social">nome responsavel</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="email">teste@teste.com</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="endereco">Ativo</div>
                                        </td>
                                        <td>
                                            <div class="acao text-center">
                                                <a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="1001" title="Inativar empresa">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="cursor-pointer odd" role="row">
                                        <td class="cadastroFilial">
                                            <div class="amlurb_id" data-id="18081">GG2525329/2017</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="nome_comercial">nome filial</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="razao_social">nome responsavel</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="email">teste@teste.com</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="endereco">Ativo</div>
                                        </td>
                                        <td>
                                            <div class="acao text-center">
                                                <a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="1001" title="Inativar empresa">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr class="cursor-pointer odd" role="row">
                                        <td class="cadastroFilial">
                                            <div class="amlurb_id" data-id="18081">GG2525329/2017</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="nome_comercial">nome filial</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="razao_social">nome responsavel</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="email">teste@teste.com</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="endereco">Ativo</div>
                                        </td>
                                        <td>
                                            <div class="acao text-center">
                                                <a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="1001" title="Inativar empresa">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="cursor-pointer odd" role="row">
                                        <td class="cadastroFilial">
                                            <div class="amlurb_id" data-id="18081">GG2525329/2017</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="nome_comercial">nome filial</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="razao_social">nome responsavel</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="email">teste@teste.com</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="endereco">Ativo</div>
                                        </td>
                                        <td>
                                            <div class="acao text-center">
                                                <a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="1001" title="Inativar empresa">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="cursor-pointer odd" role="row">
                                        <td class="cadastroFilial">
                                            <div class="amlurb_id" data-id="18081">GG2525329/2017</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="nome_comercial">nome filial</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="razao_social">nome responsavel</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="email">teste@teste.com</div>
                                        </td>
                                        <td class="cadastroFilial">
                                            <div class="endereco">Ativo</div>
                                        </td>
                                        <td>
                                            <div class="acao text-center">
                                                <a href="#" class="text-danger" id="lnkAtivarDesativarEmpresa" data-id="1001" title="Inativar empresa">
                                                    <i class="fa fa-times-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane " id="mapa-empresas">
                        <div id="mapa">

                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="diviser"></div>
        </div>

    </div>

</div>

    @include('painel.fiscal.cadastro-filiais-gg.includes.indicadores-gg') 
    @include('painel.fiscal.cadastro-filiais-gg.includes.tabs')
 @endsection