@extends('layouts.home-painel-fiscal')

@section('conteudo')

    <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

    <div class="col-xs-12">

        <h2>Relação de CTR-E</h2>

        <div class="box-rastreabilidade">

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-hover table-rastreabilidade table-padrao" id="rastreabilidade_fiscal">
                            <thead>
                            <tr>
                                <th>Nr. CTR-E</th>
                                <th>Empresa</th>
                                <th>Nr. Amlurb</th>
                                <th>Alertas</th>
                                <th>Rastreio</th>
                                <th>Status CTR-E</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>

        </div>

    </div>

    <div class="col-xs-12" id="box-status-ctre-fiscal">

        <h2>Status do CTR-E <span class="status status-aberto" id="status-rastreabilidade-fiscal">Em Aberto</span></h2>

        {{--
        Para definir uma inconsistencia, adicione a class "inconsistencia" no elemento "linha".
        --}}

        <div class="col-xs-12">
            <div class="row box-rastreio-relacao-ctre">

                <div class="linha">
                    <div class="column">
                        <div class="box-tempo">
                            <div class="tempo" id="tempo"></div>
                            <div class="tempo-texto" id="tempo-texto">Tempo restante para finalização</div>
                        </div>
                    </div>
                    <div class="column box-rastreio-gg">
                        <div class="imagem"></div>
                        <div class="titulo">Gerador</div>
                    </div>
                    <div class="column box-rastreio-tr">
                        <div class="imagem"></div>
                        <div class="titulo">Transportador</div>
                    </div>
                    <div class="column box-rastreio-df">
                        <div class="imagem"></div>
                        <div class="titulo">Destino Final</div>
                    </div>
                </div>

                <div class="linha">
                    <div class="column">
                        <div class="nome"><p>Nome</p></div>
                    </div>
                    <div class="column box-rastreio-gg">
                        <div class="nome">Sem informações</div>
                    </div>
                    <div class="column box-rastreio-tr">
                        <div class="nome">Sem informações</div>
                    </div>
                    <div class="column box-rastreio-df">
                        <div class="nome">Sem informações</div>
                    </div>
                </div>

                <div class="linha">
                    <div class="column">
                        <div class="nr-amlurb"><p>Nr. Amlurb</p></div>
                    </div>
                    <div class="column box-rastreio-gg">
                        <div class="nr-amlurb">Sem informações</div>
                    </div>
                    <div class="column box-rastreio-tr">
                        <div class="nr-amlurb">Sem informações</div>
                    </div>
                    <div class="column box-rastreio-df">
                        <div class="nr-amlurb">Sem informações</div>
                    </div>
                </div>

                <div class="linha">
                    <div class="column">
                        <div class="validacao"><p>Validação</p></div>
                    </div>
                    <div class="column box-rastreio-gg">
                        <div class="validacao aguardando">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-tr">
                        <div class="validacao aguardando">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-df">
                        <div class="validacao aguardando">Aguardando</div>
                    </div>
                </div>

                <div class="linha containerResiduo">
                    <div class="column">
                        <div class="residuo"><p>Resíduo</p></div>
                    </div>
                    <div class="column box-rastreio-gg">
                        <div class="residuo">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-tr">
                        <div class="residuo">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-df">
                        <div class="residuo">Aguardando</div>
                    </div>
                </div>

                <div class="linha containerAcondicionamento">
                    <div class="column">
                        <div class="acondicionamento"><p>Acondicionamento</p></div>
                    </div>
                    <div class="column box-rastreio-gg">
                        <div class="acondicionamento">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-tr">
                        <div class="acondicionamento">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-df">
                        <div class="acondicionamento">Aguardando</div>
                    </div>
                </div>

                <div class="linha">
                    <div class="column">
                        <div class="hora-ctre"><p>Data</p></div>
                    </div>
                    <div class="column box-rastreio-gg">
                        <div class="hora-ctre">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-tr">
                        <div class="hora-ctre">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-df">
                        <div class="hora-ctre">Aguardando</div>
                    </div>
                </div>

                <div class="linha containerPlacaVeiculo">
                    <div class="column">
                        <div class="placa-veiculo"><p>Placa Veículo</p></div>
                    </div>
                    <div class="column box-rastreio-gg">
                        <div class="placa-veiculo">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-tr">
                        <div class="placa-veiculo">Aguardando</div>
                    </div>
                    <div class="column box-rastreio-df">
                        <div class="placa-veiculo">Aguardando</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection