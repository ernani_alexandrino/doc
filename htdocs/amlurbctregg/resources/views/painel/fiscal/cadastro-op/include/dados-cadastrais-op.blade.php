@if(isset($dadosCadastrais))
    <div class="col-xs-12">
        <div class="box-cadastro">

            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="cnpj">CNPJ</label>
                        <input type="text" name="cnpj" id="cnpj" class="form-control"
                               value="{{ $dadosCadastrais->cnpj or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="ramo_atividade">Ramo de Atividade</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->ramo_atividade))
                            <input type="text" name="ramo_atividade" id="ramo_atividade" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->ramo_atividade->nome }}"
                                   disabled>
                        @else
                            <input type="text" name="ramo_atividade" id="ramo_atividade" class="form-control" value=""
                                   disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="tipo_atividade">Tipo</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->tipo_ramo_atividade->nome))
                            <input type="text" name="tipo_atividade" id="tipo_atividade" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->tipo_ramo_atividade->nome }}"
                                   disabled>
                        @else
                            <input type="text" name="tipo_atividade" id="tipo_atividade" class="form-control" value=""
                                   disabled>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="razao_social">Razão Social</label>
                        <input type="text" name="razao_social" id="razao_social" class="form-control"
                               value="{{ $dadosCadastrais->razao_social or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nome_fantasia">Nome Fantasia</label>
                        <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control"
                               value="{{ $dadosCadastrais->nome_comercial or '' }}" disabled>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="licenca_municiapl">CCM</label>
                        <input type="text" name="licenca_municipal" id="licenca_municipal" class="form-control"
                               value="{{ $dadosCadastrais->im or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="inscricao_estadual">Inscrição Estadual</label>
                        <input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control"
                               value="{{ $dadosCadastrais->ie or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="telefone">Telefone</label>
                        <input type="text" name="telefone" id="telefone" class="form-control"
                               value="{{ $dadosCadastrais->telefone or '' }}" disabled>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        @if(isset($dadosCadastrais->empresa_endereco->cep))
                            <input type="text" name="cep" id="cep" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->cep }}" disabled>
                        @else
                            <input type="text" name="cep" id="cep" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="estados">Estado</label>
                        @if(isset($dadosCadastrais->empresa_endereco->estado->sigla))
                            <input type="text" name="estado" id="estado" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->estado->sigla }}" disabled>
                        @else
                            <input type="text" name="estado" id="estado" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="cidade">Cidade</label>
                        @if(isset($dadosCadastrais->empresa_endereco->cidade->nome))
                            <input type="text" name="cidade" id="cidade" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->cidade->nome }}" disabled>
                        @else
                            <input type="text" name="cidade" id="cidade" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="endereco">Endereço</label>
                        @if(isset($dadosCadastrais->empresa_endereco->endereco))
                            <input type="text" name="endereco" id="endereco" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->endereco }}" disabled>
                        @else
                            <input type="text" name="endereco" id="endereco" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="numero">Número</label>
                        @if(isset($dadosCadastrais->empresa_endereco->numero))
                            <input type="text" name="numero" id="numero" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->numero }}" disabled>
                        @else
                            <input type="text" name="numero" id="numero" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="complemento">Bairro</label>
                        @if(isset($dadosCadastrais->empresa_endereco->bairro))
                            <input type="text" name="bairro" id="bairro" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->bairro }}" disabled>
                        @else
                            <input type="text" name="bairro" id="bairro" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="complemento">Complemento</label>
                        @if(isset($dadosCadastrais->empresa_endereco->complemento))
                            <input type="text" name="complemento" id="complemento" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->complemento }}" disabled>
                        @else
                            <input type="text" name="complemento" id="complemento" class="form-control" value=""
                                   disabled>
                        @endif
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="ponto_referencia">Ponto de Referência</label>
                        @if(isset($dadosCadastrais->empresa_endereco->ponto_referencia))
                            <input type="text" name="ponto_referencia" id="ponto_referencia" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->ponto_referencia }}" disabled>
                        @else
                            <input type="text" name="ponto_referencia" id="ponto_referencia" class="form-control"
                                   value="" disabled>
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>

            <div class="clearfix"></div>

        </div>

    </div>

    <div class="col-xs-12">

        <h2>Responsável</h2>

        <div class="box-cadastro">

            <div class="row">

                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="responsavel">Nome Completo do Responsável</label>
                        <input type="text" name="responsavel" id="responsavel" class="form-control"
                               value="{{ $dadosCadastrais->nome_responsavel or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="nome_fantasia">E-mail</label>
                        <input type="text" name="email" id="email" class="form-control"
                               value="{{ $dadosCadastrais->email or '' }}" disabled>
                    </div>
                </div>

                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="cargo">Cargo</label>
                        <input type="text" name="cargo" id="cargo" class="form-control"
                               value="{{ $dadosCadastrais->cargo or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="telefone_responsavel">Telefone</label>
                        <input type="tel" name="telefone_responsavel" id="telefone_responsavel" class="form-control"
                               value="{{ $dadosCadastrais->telefone_responsavel or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="ramal">Ramal</label>
                        <input type="text" name="ramal" id="ramal" class="form-control"
                               value="{{ $dadosCadastrais->ramal or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="celular_responsavel">Celular</label>
                        <input type="text" name="celular_responsavel" id="celular_responsavel" class="form-control"
                               value="{{ $dadosCadastrais->celular or '' }}" disabled>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>

            <div class="clearfix"></div>

        </div>

    </div>

    <div class="col-xs-12">

        <h2>Frequência</h2>

        <div class="box-cadastro">

            <div class="row">

                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="frequencia_geracao">Geração Diária de Resíduos</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->frequencia_geracao->nome))
                            <input type="text" name="frequencia_geracao" id="frequencia_geracao" class="form-control" value="{{ $dadosCadastrais->empresa_informacao_complementar->frequencia_geracao->nome }}" disabled>
                        @else
                            <input type="text" name="frequencia_geracao" id="frequencia_geracao" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="frequencia_coleta">Frequência Coleta</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->frequencia_coleta->nome))
                            <input type="text" name="frequencia_coleta" id="frequencia_coleta" class="form-control" value="{{ $dadosCadastrais->empresa_informacao_complementar->frequencia_coleta->nome }}" disabled>
                        @else
                            <input type="text" name="frequencia_coleta" id="frequencia_coleta" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>

            <div class="clearfix"></div>

        </div>

    </div>

    <div class="col-xs-12">

        <h2>Outras Informações</h2>

        <div class="box-cadastro">

            <div class="row">

                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="numero_colaboradores">Número de Colaboradores</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->colaboradores_numero->nome))
                            <input type="text" name="numero_colaboradores" id="numero_colaboradores" class="form-control" value="{{ $dadosCadastrais->empresa_informacao_complementar->colaboradores_numero->nome }}" disabled>
                        @else
                            <input type="text" name="numero_colaboradores" id="numero_colaboradores" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="consumo_energia">Consumo Mensal de Energia</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->energia_consumo->nome))
                            <input type="text" name="consumo_energia" id="consumo_energia" class="form-control" value="{{ $dadosCadastrais->empresa_informacao_complementar->energia_consumo->nome }}" disabled>
                        @else
                            <input type="text" name="consumo_energia" id="consumo_energia" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="area_total">Área Total (m²)</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->area_total))
                            <input type="text" name="area_total" id="area_total" class="form-control" value="{{ $dadosCadastrais->empresa_informacao_complementar->area_total }}" disabled>
                        @else
                            <input type="text" name="area_total" id="area_total" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label for="area_construida">Área Construída (m²)</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->area_construida))
                            <input type="text" name="area_construida" id="area_construida" class="form-control" value="{{ $dadosCadastrais->empresa_informacao_complementar->area_construida }}" disabled>
                        @else
                            <input type="text" name="area_construida" id="area_construida" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="diviser margin-bottom-80"></div>
            </div>

            <div class="clearfix"></div>

        </div>

    </div>

@else

    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div class="alert alert-warning text-center" role="alert">
            Informações deste usuário estão incompletas!
        </div>
    </div>

@endif