@extends('layouts.home-painel-fiscal')

@section('conteudo')

    <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

    <div class="col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h2>Relação de Órgão Público</h2>
            </div>
            <div class="col-xs-12 col-sm-6">
                @include('painel.fiscal.cadastro-op.include.search')
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="termo-pesquisado">
                    O termo pesquisado foi "<span>{{ $filtroValor }}</span>" utilizando a coluna
                    "<span>{{ $filtro }}</span>".
                </div>
            </div>
        </div>

        <div class="box-rastreabilidade">

            <div class="row">
                <div class="col-xs-12">

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="lista-empresas">

                            <div class="table-responsive">
                                <table class="table table-hover fiscal-cadastros" id="cadastro_orgao_publico_search">
                                    <thead>
                                    <tr>
                                        <th>Nr. Amlurb</th>
                                        <th>Empresa</th>
                                        <th>Razão Social</th>
                                        <th>CNPJ</th>
                                        <th>Bairro</th>
                                        <th>Status</th>
                                        <th>Ação</th>                                         
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div role="tabpanel" class="tab-pane" id="mapa-empresas">

                            <div id="mapa"></div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>
        </div>
    </div>

    @if($dadosCadastrais)

        @include('painel.fiscal.cadastro-op.include.tabs')

    @endif

    @include('painel.fiscal.aprovacao.modalAvisoAtivacaoDesativacao')
    
@endsection