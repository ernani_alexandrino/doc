<div class="col-xs-12">
    <div class="dashboard-fiscal">
        <div class="row">

            <div class="col-xs-6">
                <div class="row">

                    <div class="balanco-periodo">
                        <div class="col-xs-12">
                            <div class="titulo-dashboard"></div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="data_inicial">Data Inicial</label>
                                <div class="input-group">
                                    <input name="data_inicial" id="data_inicial_fiscal_grafico" type="text"
                                           class="form-control"
                                           placeholder="00/00/0000" aria-label="Data inicial">
                                </div>
                                {{--<input type="text" name="data_inicial" class="form-control" id="data_inicial">--}}
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="data_inicial">Data Final</label>
                                <div class="input-group">
                                    <input type="text" name="data_final" id="data_final_fiscal_grafico"
                                           class="form-control"
                                           placeholder="00/00/0000" aria-label="Data final">
                                </div>
                                {{--<input type="text" name="data_final" class="form-control" id="data_final">--}}
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <canvas class="grafico" id="ctre_balanco_periodo_fiscal" height="193"></canvas>
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-xs-6">
                <div class="lancamentos-ctre">
                    <div class="titulo-dashboard"></div>

                    {{-- <table class="table table-dashboard table-striped tablesorter " id="lancamentos_ctre"> --}}
                     <table class="table table-ctres dataTable no-footer table-padrao" id="lancamentos_ctre">   
                        <thead>
                        <tr>
                            <th class="header">Data</th>
                            <th class="header">Número</th>
                            <th class="header">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($ctreEmitidos as $ctre)
                            <tr>
                                <td>{{ $ctre->data_emissao->format('d/m/Y') }}</td>
                                <td>{{ $ctre->codigo }}</td>
                                <td>{{ $ctre->status->descricao }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3">
                                    Não há CTRE emitido!
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>