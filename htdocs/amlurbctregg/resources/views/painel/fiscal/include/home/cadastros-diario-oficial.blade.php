<div class="" id="">

    <div class="col-xs-12">
        <div class="indicadores-ctre-periodo indicadores-periodo" style="background-color: transparent;margin-top: 30px;">
            <div class="row">
                <div class="col-xs-12 col-lg-6">
                    <div class="titulo-diario-oficial"></div>
                </div>
                <div class="col-xs-12">
                    <div class="box-indicador-periodo">
                        <div class="col-xs-12">
                            <h2>Novo Envio</h2>
                            

                            <div class="row check-radio-custom">
                                <div class="col-xs-12">
                                    <form class="form-horizontal">
                                        <div class="col-xs-5">
                                            <div class="form-group">
                                                <input type="radio" id="por-periodo" name="por-periodo" value="por-periodo">
                                                <label for="por-periodo" class="">Por período</label>      
                                            </div>

                                            <div class="form-group">
                                                <label for="data_inicial" class="col-sm-4 control-label">Data/ Hora Inicial</label>
                                                <div class="input-group">
                                                    <input name="data_inicial" id="" type="text"
                                                           class="form-control"
                                                           placeholder="00/00/0000 hh:mm" aria-label="Data inicial">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="data_inicial" class="col-sm-4 control-label">Data/ Hora Final</label>
                                                <div class="input-group">
                                                    <input name="data_inicial" id="" type="text"
                                                           class="form-control"
                                                           placeholder="00/00/0000 hh:mm" aria-label="Data inicial">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <input type="radio" id="por-ultimo" name="por-periodo" value="por-periodo">
                                                <label for="por-ultimo" class="">A partir do último envio</label>      
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">Último envio em:</label>
                                                <div class="input-group">
                                                    <span class="form-control" style="border: none;">00/00/0000 hh:mm</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-3">
                                            <button class="btn btn-default btn-warning modal-bts-center__externo" 
                                                    style="margin-top: 25px;float: right;"
                                                    type="button"
                                                    data-toggle="modal" 
                                                    data-target="#modal-loading-cadastros">
                                                Enviar cadastros
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <hr style="border-top: 3px solid #eee;">
                                <div class="col-xs-12">
                                    <h2>Relação dos Últimos Envios</h2>
                                </div>

                                <table class="table table-dashboard table-striped tablesorter" id="">
                                    <thead>
                                    <tr>
                                        <th class="header" width="28%">Data de envio</th>
                                        <th class="header" width="28%">Data/ Hora Inicial do Período</th>
                                        <th class="header" width="28%">Data/ Hora Final do Período</th>
                                        <th class="header" width="16%">Enviar Novamente</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>xx/xx/xxxx - 00:00</td>
                                            <td>xx/xx/xxxx - 00:00</td>
                                            <td>xx/xx/xxxx - 00:00</td>
                                            <td>
                                                <button class="btn btn-default btn-warning modal-bts-center__externo">
                                                    Enviar cadastros
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="modal-loading-cadastros" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <img class="" src="{{ asset('images/CTR-E-modal.png') }}"
                 alt="CTRE - Controle de Transporte de Resíduos - Eletrônico">
            </div>
            <div class="modal-body">
                <p class="text-center">ENVIANDO CADASTROS, AGUARDE.</p>
                <div class="status-bar text-center col-xs-12">
                    <a href="javascript:void(0);" class="status-bar__full"><span style="left: 0;right: unset;">0</span><span>20%</span></a>
                    <a href="javascript:void(0);"><span>40%</span></a>
                    <a href="javascript:void(0);"><span>60%</span></a>
                    <a href="javascript:void(0);"><span>80%</span></a>
                    <a href="javascript:void(0);"><span style="right: 0;">100%</span></a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">CANCELAR</button>
            </div>
        </div>

    </div>
</div>