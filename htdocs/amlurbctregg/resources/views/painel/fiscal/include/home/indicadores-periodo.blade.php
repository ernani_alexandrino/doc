<div class="indicadores-periodo" id="indicadores_periodo">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-lg-4">
                <div class="titulo-indicador"></div>
            </div>
            
            <div class="col-xs-12 col-lg-8">                    
                <ul class="nav nav-pills nav-indicadores-periodo">
                    <li role="presentation">
                        <a href="#!" data-periodo="diario" class="opcao-indicador-periodo" title="Filtrar por dia">dia</a>
                    </li>
                    <li role="presentation">
                        <a href="#!" data-periodo="semanal" class="opcao-indicador-periodo">semana</a>
                    </li>
                    <li role="presentation">
                        <a href="#!" data-periodo="mensal" class="opcao-indicador-periodo">mês</a>
                    </li>
                    <li role="presentation">
                        <a href="#!" data-periodo="trimestral" class="opcao-indicador-periodo">trimestre</a>
                    </li>
                    <li role="presentation">
                        <a href="#!" data-periodo="semestral" class="opcao-indicador-periodo">semestre</a>
                    </li>
                    <li role="presentation">
                        <a href="#!" data-periodo="anual" class="opcao-indicador-periodo">ano</a>
                    </li>
                    <li role="presentation">

                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="input-group margin-left-30">
                                        <div class="input-group-btn">
                                            <button class="btn"><i class="fa fa-flag"></i></button>
                                        </div>
                                        <input type="text" class="form-control" name="dt_inicio_fiscal_cad_periodo"
                                               id="dt_inicio_fiscal_cad_periodo" placeholder="dd/mm/aaaa"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button class="btn"><i class="fa fa-map-marker"></i></button>
                                        </div>
                                        <input type="text" class="form-control" name="dt_final_fiscal_cad_periodo"
                                               id="dt_final_fiscal_cad_periodo" placeholder="dd/mm/aaaa"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <a href="{{ route('cadastro_gg_fs') }}" title="Cadastro de Grandes Geradores">
                <div class="col-xs-12 col-sm-5">
                    <div class="indicador-grande-gerador"></div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="indicador-total count"
                         id="total_gerador">{{ $cadastrosIndicadores->geradores or 0 }}</div>
                    <div class="indicador-nome">Cadastros Totais</div>
                    <div class="indicador-total-vencido count" id="total_gerador_vencido">0</div>
                    <div class="indicador-vencido">Cadastros Vencidos</div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="titulo-box-indicador titulo-gg">
                            Grandes Geradores
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <a href="{{ route('cadastro_pg_fs') }}" title="Cadastro de Pequenos Geradores">
                <div class="col-xs-12 col-sm-5">
                    <div class="indicador-pequeno-gerador"></div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="indicador-total count"
                         id="total_pequeno_gerador">{{ $cadastrosIndicadores->pequenosGeradores or 0 }}</div>
                    <div class="indicador-nome">Cadastros Totais</div>
                    <div class="indicador-total-vencido count" id="total_pequeno_gerador_vencido">0</div>
                    <div class="indicador-vencido">Cadastros Vencidos</div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="titulo-box-indicador titulo-peq-gg">
                            Pequenos Geradores
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <a href="{{ route('cadastro_tr_fs') }}" title="Cadastro de Transportadores">
                <div class="col-xs-12 col-sm-5">
                    <div class="indicador-transportador"></div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="indicador-total count"
                         id="total_transportador">{{ $cadastrosIndicadores->transportadores or 0 }}</div>
                    <div class="indicador-nome">Cadastros Totais</div>
                    <div class="indicador-total-vencido count" id="total_transportador_vencido">0</div>
                    <div class="indicador-vencido">Cadastros Vencidos</div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="titulo-box-indicador titulo-tr">
                            Transportadores
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <a href="{{ route('cadastro_df_fs') }}" title="Cadastro de Destinos Finais">
                <div class="col-xs-12 col-sm-5">
                    <div class="indicador-destino-final"></div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="indicador-total count"
                         id="total_destino">{{ $cadastrosIndicadores->destinosFinais or 0 }}</div>
                    <div class="indicador-nome">Cadastros Totais</div>
                    <div class="indicador-total-vencido count" id="total_destino_vencido">0</div>
                    <div class="indicador-vencido">Cadastros Vencidos</div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="titulo-box-indicador titulo-df">
                            Destinos Finais
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <a href="{{ route('cadastro_cr_fs') }}" title="Cadastro de Cooperativas">
                <div class="col-xs-12 col-sm-5">
                    <div class="indicador-cooperativa"></div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="indicador-total count"
                         id="total_cooperativa">{{ $cadastrosIndicadores->cooperativas or 0 }}</div>
                    <div class="indicador-nome">Cadastros Totais</div>
                    <div class="indicador-total-vencido count" id="total_cooperativa_vencido">0</div>
                    <div class="indicador-vencido">Cadastros Vencidos</div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="titulo-box-indicador titulo-cr">
                            Cooperativas
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <a href="{{ route('cadastro_ss_fs') }}" title="Cadastro de Serviço de Saúde">
                <div class="col-xs-12 col-sm-5">
                    <div class="indicador-servico-saude"></div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="indicador-total count"
                         id="total_servico_saude">{{ $cadastrosIndicadores->servicosSaude or 0 }}</div>
                    <div class="indicador-nome">Cadastros Totais</div>
                    <div class="indicador-total-vencido count" id="total_servico_saude_vencido">0</div>
                    <div class="indicador-vencido">Cadastros Vencidos</div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="titulo-box-indicador titulo-ss">
                            Serviço de Saúde
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <a href="{{ route('cadastro_op_fs') }}" title="Cadastro de Órgão Público">
                <div class="col-xs-12 col-sm-5">
                    <div class="indicador-orgao-publico"></div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="indicador-total count"
                         id="total_orgao_publico">{{ $cadastrosIndicadores->orgaosPublicos or 0 }}</div>
                    <div class="indicador-nome">Cadastros Totais</div>
                    <div class="indicador-total-vencido count" id="total_orgao_publico_vencido">0</div>
                    <div class="indicador-vencido">Cadastros Vencidos</div>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="titulo-box-indicador titulo-op">
                            Órgão Público
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <div class="col-xs-12 col-sm-5">
                <div class="indicador-veiculo"></div>
            </div>
            <div class="col-xs-12 col-sm-7">
                <div class="indicador-total count" id="total_veiculo">{{ $cadastrosIndicadores->veiculos or 0 }}</div>
                <div class="indicador-nome">Cadastros Totais</div>
                <div class="indicador-total-vencido count" id="total_veiculo_vencido">0</div>
                <div class="indicador-vencido">Cadastros Vencidos</div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="titulo-box-indicador titulo-vl">
                        Veículos
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3 col-lg-3">
        <div class="box-indicador-periodo">
            <div class="col-xs-12 col-sm-5">
                <div class="indicador-equipamento"></div>
            </div>
            <div class="col-xs-12 col-sm-7">
                <div class="indicador-total count"
                     id="total_equipamento">{{ $cadastrosIndicadores->equipamentos or 0 }}</div>
                <div class="indicador-nome">Cadastros Totais</div>
                <div class="indicador-total-vencido count" id="total_equipamento_vencido">0</div>
                <div class="indicador-vencido">Cadastros Vencidos</div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="titulo-box-indicador titulo-eq">
                        Equipamentos
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>