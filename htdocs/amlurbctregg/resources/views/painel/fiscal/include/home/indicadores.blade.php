<div class="indicadores-fs" id="indicador_ctre_periodo">

    <div class="col-xs-12">
        <div class="indicadores-ctre-periodo">
            <div class="row">
                <div class="col-xs-12 col-lg-3">
                    <div class="titulo-indicador"></div>
                </div>
                <div class="col-xs-12 col-lg-9">
                    <ul class="nav nav-pills nav-indicadores-periodo">
                        <li role="presentation">
                            <a href="#!" data-periodo="diario" class="opcao-indicador-periodo" title="Filtrar por dia">
                                dia
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#!" data-periodo="semanal" class="opcao-indicador-periodo"
                               title="Filtrar por semana">
                                semana
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#!" data-periodo="mensal" class="opcao-indicador-periodo" title="Filtrar por mês">
                                mês
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#!" data-periodo="trimestral" class="opcao-indicador-periodo"
                               title="Filtrar por trimestre">
                                trimestre
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#!" data-periodo="semestral" class="opcao-indicador-periodo"
                               title="Filtrar por semestre">
                                semestre
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#!" data-periodo="anual" class="opcao-indicador-periodo" title="Filtrar por ano">
                                ano
                            </a>
                        </li>
                        <li role="presentation">

                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row">
                                        <div class="input-group margin-left-30">
                                            <div class="input-group-btn">
                                                <button class="btn"><i class="fa fa-flag"></i></button>
                                            </div>
                                            <input type="text" class="form-control" name="dt_inicio_indicador_periodo"
                                                   id="dt_inicio_indicador_periodo" placeholder="dd/mm/aaaa">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="row">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <button class="btn"><i class="fa fa-map-marker"></i></button>
                                            </div>
                                            <input type="text" class="form-control" name="dt_final_indicador_periodo"
                                                   id="dt_final_indicador_periodo" placeholder="dd/mm/aaaa">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="containerCadastros">
        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="box-indicador">
                <div class="image-indicador total-emitido"></div>
                <div class="corpo-indicador">
                    <div class="contador count" id="total_ctre_emitido">
                        {{ $ctreIndicadores->emitidos or 0 }}
                    </div>
                    <div class="diviser-emitido"></div>
                    <div class="titulo-indicador">
                        CTR Emitidos
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="box-indicador">
                <div class="image-indicador total-aberto"></div>
                <div class="corpo-indicador">
                    <div class="contador count" id="total_ctre_aberto">
                        {{ $ctreIndicadores->abertos or 0 }}
                    </div>
                    <div class="diviser-em-aberto"></div>
                    <div class="titulo-indicador">
                        CTR em Aberto
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="box-indicador">
                <div class="image-indicador total-finalizados"></div>
                <div class="corpo-indicador">
                    <div class="contador count" id="total_ctre_finalizado">
                        {{ $ctreIndicadores->finalizados or 0 }}
                    </div>
                    <div class="diviser-finalizado"></div>
                    <div class="titulo-indicador">
                        CTR Finalizados
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="box-indicador">
                <div class="image-indicador total-expirados"></div>
                <div class="corpo-indicador">
                    <div class="contador count" id="total_ctre_expirado">
                        {{ $ctreIndicadores->expirados or 0 }}
                    </div>
                    <div class="diviser-expirado"></div>
                    <div class="titulo-indicador">
                        CTR Expirados
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
