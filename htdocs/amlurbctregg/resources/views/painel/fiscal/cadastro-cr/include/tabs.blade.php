<div class="col-xs-12">
    <div class="row">

        <ul class="nav nav-tabs nav-justified nav-tabs-cadastros-fiscal" role="tablist">
            <li role="presentation" class="active">
                <a href="#dados-cadastrais" aria-controls="dados-cadastrais" role="tab" data-toggle="tab">
                    Dados Cadastrais
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="dados-cadastrais">

                @include('painel.fiscal.cadastro-cr.include.dados-cadastrais-cr')

            </div>
        </div>
    </div>
</div>
