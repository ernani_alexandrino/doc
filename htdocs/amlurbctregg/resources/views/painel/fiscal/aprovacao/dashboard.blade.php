@extends('layouts.home-painel-fiscal')

@section('conteudo')

    <div class="containerAprovacaoCadastro">

        <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

        @include('painel.fiscal.aprovacao.indicadores')

    </div>
@endsection
