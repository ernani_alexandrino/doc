<div class="modal fade" id="modalAprovaReprovaVeiculo">
<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Informações do Veículo</h4>
            </div>
            <div class="modal-body">

            <div class="row mensagem-validade hide">
                    <div class="col-xs-12">
                        <p id="alert-text" class="alert alert-text text-center alert-success">O veículo foi aprovado com sucesso.</p>
                    </div>
                </div>
                   <form id="frmVeiculos" name="frmVeiculos" class="form-horizontal" novalidate="">
                    <div class="row margin-bottom-10">

                    <input class="form-control hide" id="id_veiculo"  name="id_veiculo" type="text">
                    <input class="form-control hide" id="gerar_boleto"  name="gerar_boleto" type="text" value="true">
                        <div class="col-xs-4">
                            {!! Form::label('ano_veiculo', 'Ano') !!}
                            {!! Form::text('ano_veiculo', null, ['class' => 'form-control', 'id' => 'ano_veiculo', 'placeholder' => '1999','maxlength'=>'4', 'readonly']) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('placa', 'Placa') !!}
                            {!! Form::text('placa', null, ['class' => 'form-control', 'id' => 'placa', 'placeholder' => 'AAA-0000', 'maxlength'=>'8', 'readonly']) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('renavam', 'Renavam') !!}
                            {!! Form::text('renavam', null, ['class' => 'form-control', 'id' => 'renavam', 'placeholder' => '00000000000', 'maxlength'=>'11', 'readonly']) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-6">
                            {!! Form::label('tipo', 'Tipo de Veículo') !!}
                            {!! Form::text('tipo', null, ['class' => 'form-control', 'id' => 'tipo', 'placeholder' => 'Ex: Caminhão', 'readonly']) !!}
                        </div>
                        <div class="col-xs-6">
                            {!! Form::label('marca', 'Marca') !!}
                            {!! Form::text('marca', null, ['class' => 'form-control', 'id' => 'marca', 'placeholder' => 'Ex: Volvo', 'readonly']) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-4">
                            {!! Form::label('vencimento_ipva', 'Vencimento IPVA') !!}
                            {!! Form::text('vencimento_ipva', null, ['class' => 'form-control', 'id' => 'vencimento_ipva', 'placeholder' => 'dd/mm/aaaa', 'readonly']) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('capacidade', 'Capacidade (tonelada)') !!}
                            {!! Form::text('capacidade', null, ['class' => 'form-control', 'id' => 'capacidade', 'placeholder' => 'Ex: 15,00', 'readonly']) !!}
                        </div>
                        <div class="col-xs-4">
                            {!! Form::label('tara', 'Tara (tonelada)') !!}
                            {!! Form::text('tara', null, ['class' => 'form-control', 'id' => 'tara', 'placeholder' => 'Ex: 10,00', 'readonly']) !!}
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-4">
                            {!! Form::label('numero_inmetro', 'Número Inmetro') !!}
                            {!! Form::text('numero_inmetro', null, ['class' => 'form-control required', 'id' => 'numero_inmetro', 'readonly']) !!}
                        </div>
                        <div class="col-xs-4">
                            <div class="">
                                <label style="width: 100%;" for="anexo_documento_inmetro">Documento Inmetro</label>
                                <a href="" id="ver_anexo_documento_inmetro" class="form-control ver-doc" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-4">

                            <div class="">
                                <label style="width: 100%;" for="anexo_documento_veiculo">Documento do Veículo</label>
                                <a href="" id="ver_anexo_documento_veiculo" class="form-control ver-doc" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                               
                            </div>
                           
                        </div>
                    </div>

                    <div class="row margin-bottom-10">
                        <div class="col-xs-4">

                            <div class="">
                                <label style="width: 100%;" for="anexo_comodato_veiculo">
                                    Comodato do veiculo<br>
                                    <span style="font-size: 11px;">(caso não pertenece a sua empresa)</span>
                                </label>
                                <a href="" id="ver_anexo_comodato_veiculo" class="form-control ver-doc" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                               
                            </div>
                           
                        </div>
                    </div>
                    <div class="row margin-bottom-10">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="">
                            <button type="button" class="btn btn-default btn-success" id="btn-ativar-veiculo" value="add">Ativar e Gerar Boleto </button>
                            <button type="button" class="btn btn-default btn-success" id="btn-ativar-veiculo-sem-boleto" value="add">Ativar sem Gerar Boleto</button>
                            <input type="hidden" id="veiculo_id" name="veiculo_id" value="0">
                            <button class="btn btn-danger" type="button" id="btn-inconsistencia-veiculo">
                            Reportar inconsistência
                        </button>
                        </div>
                    </div>

                </div> 
            </div>
        </div>
    </div>
</div>
