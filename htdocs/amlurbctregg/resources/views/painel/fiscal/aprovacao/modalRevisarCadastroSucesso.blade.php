<div class="modal fade" id="modalCadastroAprovadoSucesso">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Aprovação do cadastro da empresa</h2>
            </div>
            <div class="modal-body">

                <div class="ajax-load"></div>

                <div class="alert alert-success text-center hide" role="alert">
                    <p>Empresa aprovada com sucesso</p>
                </div>

                <div class="alert alert-danger text-center hide" role="alert">
                    <p>Não foi possível completar a solicitaçao de aprovação.</p>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default custom_send_fiscal" data-dismiss="modal" title="Fechar">
                    Fechar
                </button>
            </div>
        </div>
    </div>
</div>
