@extends('layouts.home-painel-fiscal')

@section('conteudo')

    <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

    <div id="loading-cadastro">
        <div class="loading"><img src="{{ asset('images/logo-loading.gif') }}" alt="Carregando..."/></div>
    </div>

    <div class="containerTableAprovacao">

        <div class="col-xs-12">
            <table class="table table-hover" data-tipo="{{ $tipoEmpresa }}" id="listaAprovacaoEmpresas">
                <thead>
                <tr>
                    <th>
                        <div class="flex">CNPJ</div>
                    </th>
                    <th>
                        <div class="flex">Empresa</div>
                    </th>
                    <th>
                        <div class="flex">Data</div>
                    </th>
                    <th>
                        <div class="flex">Status</div>
                    </th>
                </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        @if(isset($empresas->links))
            <div class="col-xs-12">
                {{ $empresas->links('pagination.custom') }}
            </div>
        @endif

        <div class="clearfix"></div>

    </div>

    <div id="containerDetalhesEmpresa"></div>

    @include('painel.fiscal.aprovacao.modalAvisoAprovacao')
    @include('painel.fiscal.aprovacao.modalAprovarCadastroSucesso')
    @include('painel.fiscal.aprovacao.modalAvisoAtivacaoDesativacao')

@endsection
