<div class="modal fade" id="modalAvisoAprovacaoCadastro">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Aprovação de cadastro de empresa</h4>
            </div>
            <div class="modal-body">

                <p class="msg-header">Tem certeza que deseja prosseguir com a ativação desta empresa?</p>

                <div class="row mensagem hide">
                    <div class="col-xs-12">
                        <p class="alert text-center" role="alert"></p>
                    </div>
                </div>

            </div>
            @if($tipoEmpresa == 'pequeno-gerador' || $tipoEmpresa == 'destino-final' || $tipoEmpresa == 'cooperativa')
                <div class="modal-footer">
                    <button type="button" class="btn btn-success btnAprovarEmpresa">Aprovar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                </div>
            @else
                <div class="modal-footer">
                    <button type="button" class="btn bt-bg-color-green"
                            type="button"
                            data-toggle="modal" 
                            data-target="#modal-aprovar-sem-boleto">Aprovar e Não Gerar Boleto</button>
                    <button type="button" class="btn bt-bg-color-green btnAprovarEmpresa">Aprovar e Gerar Boleto</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
                </div>
            @endif
        </div>
    </div>
</div>




<!-- Modal -->
<div id="modal-aprovar-sem-boleto" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: 360px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <div class="row mensagem-validade">
                    <div class="col-xs-12">
                        <p id="alert-text" class="alert alert-text text-center alert-success hide">A empresa foi aprovada com sucesso.</p>
                    </div>
                </div>

                <form id="aprovar-validade-cadastro">

                    <div class="col-xs-12">
                        <div class="col-xs-9" style="padding: 0;">
                            <div class="form-group">
                                <label for="">Data de Validade do Cadastro</label>
                                <input type="text" id="dataValidade" name="dataValidade" class="form-control calendario" style="width: 170px;" value="" readonly/>
                            </div>
                        </div>
                    </div>

                    <div class="text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Voltar</button>
                        <button type="button" class="btn bt-bg-color-green alert-bt btnAprovarEmpresa" data-validade="true">Aprovar</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>