<div class="modal fade" id="modalAtivacaoDesativacao">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ativar empresa</h4>
                <h3 id="titulo_empresa"></h3>
            </div>
            <div class="modal-body">

                <p class="msg-header">Descreva o motivo para esta operação</p>

                <div class="row mensagem hide">
                    <div class="col-xs-12">
                        <p class="alert text-center" role="alert"></p>
                    </div>
                </div>

                {{ Form::open(['route' => 'fiscal.ativar-desativar.empresa', 'id' => 'formAtivarDesativarCadastro']) }}

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="descricao">Descrição</label>
                            <textarea name="descricao" id="descricao" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-success">Concluir operação</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
