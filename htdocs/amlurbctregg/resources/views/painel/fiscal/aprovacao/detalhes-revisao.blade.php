<hr/>
<div class="col-xs-12" >
    <div class="detalhesEmpresa" id ="containerDetalhesEmpresa">

        <input type="hidden" id="empresa" value="{{ $empresa->id }}"/>

        <div class="row">

            <div class="col-xs-12">
                <h2>Dados cadastrais</h2>
            </div>
            <input type="hidden" id="tipo_" value="revisao">
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="cnpj">CNPJ</label>
                    <input type="text" id="cnpj" class="form-control" value="{{ $empresa->cnpj }}" readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="ramo_atividade" @if ( isset($empresa_alteracao['dados_empresa']['ramo_atividade_id']) && $empresa->empresa_informacao_complementar->ramo_atividade_id != $empresa_alteracao['dados_empresa']['ramo_atividade_id']) class="text-danger" @endif>Ramo de Atividade @if (isset($empresa_alteracao['dados_empresa']['ramo_atividade_id']) && $empresa->empresa_informacao_complementar->ramo_atividade_id != $empresa_alteracao['dados_empresa']['ramo_atividade_id'])<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->ramo_atividade_id) ? 'Vazio' : $empresa->empresa_informacao_complementar->ramo_atividade->nome }}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                    <input type="text" id="ramo_atividade" class="form-control"
                           value="{{ (isset($empresa_alteracao->ramo_atividade->nome) &&  !empty($empresa_alteracao->ramo_atividade->nome))? $empresa_alteracao->ramo_atividade->nome : $empresa->empresa_informacao_complementar->ramo_atividade->nome  }}" readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="tipo_atividade" @if (isset($empresa_alteracao['dados_empresa']['tipo_ramo_atividade_id']) && $empresa->empresa_informacao_complementar->tipo_ramo_atividade_id != $empresa_alteracao['dados_empresa']['tipo_ramo_atividade_id']) class="text-danger" @endif>Tipo @if (isset($empresa_alteracao['dados_empresa']['tipo_ramo_atividade_id']) && $empresa->empresa_informacao_complementar->tipo_ramo_atividade_id != $empresa_alteracao['dados_empresa']['tipo_ramo_atividade_id'])<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->tipo_ramo_atividade_id) ? 'Vazio' : $empresa->empresa_informacao_complementar->tipo_ramo_atividade->nome }}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                    <input type="text" id="tipo_atividade" class="form-control"
                           value="{{ (isset($empresa_alteracao['dados_empresa']['tipo_ramo_atividade_id']) &&  !empty($empresa_alteracao['dados_empresa']['tipo_ramo_atividade_id']))? $empresa_alteracao['dados_empresa']['tipo_ramo_atividade_id'] : $empresa->empresa_informacao_complementar->ramo_atividade->nome  }}"
                           readonly/>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="empresa_cartao_cnpj">Ver Cartão CNPJ</label>
                    @if(!empty($documentos['empresa_cartao_cnpj']))
                        <a href="{{ route('file', ['arquivo' => $documentos['empresa_cartao_cnpj'], 'empresa' => $empresa->id]) }}" id="empresa_cartao_cnpj" class="form-control" target="_blank">
                            arquivo atual
                        </a>
                        @endif
                        @if(isset($empresa_alteracao['documentos']['empresa_cartao_cnpj_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['empresa_cartao_cnpj_alterado'], 'empresa' => $empresa->cnpj]) }}" id="empresa_cartao_cnpj" class="form-control" target="_blank">
                                arquivo novo
                            </a>
                        @endif
                        @if(empty($documentos['empresa_cartao_cnpj']) && !isset($empresa_alteracao['documentos']['empresa_cartao_cnpj_alterado']))
                        <input id="empresa_cartao_cnpj" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['empresa_num_iptu'])) class="text-danger" @endif>Número do IPTU @if ( isset($empresa_alteracao['dados_empresa']['empresa_num_iptu']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['empresa_num_iptu']) ? 'Vazio' : $documentos['empresa_num_iptu']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['empresa_num_iptu']) &&  !empty($empresa_alteracao['dados_empresa']['empresa_num_iptu'])){{$empresa_alteracao['dados_empresa']['empresa_num_iptu']}}
                                    @else @if(!empty($documentos['empresa_num_iptu'])) {{$documentos['empresa_num_iptu']}}
                                        @else
                                            Vazio
                                        @endif
                                    @endif
                                    " readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="empresa_iptu">Ver Cópia do IPTU</label>

                    @if(!empty($documentos['empresa_iptu']))
                        <a href="{{ route('file', ['arquivo' => $documentos['empresa_iptu'], 'empresa' => $empresa->id]) }}" id="empresa_iptu" class="form-control" target="_blank">
                            arquivo atual
                        </a>
                    @endif
                    @if(isset($empresa_alteracao['documentos']['empresa_iptu_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['empresa_iptu_alterado'], 'empresa' => $empresa->cnpj]) }}" id="empresa_iptu" class="form-control" target="_blank">
                                arquivo novo
                            </a>
                    @endif
                    @if(empty($documentos['empresa_iptu']) && !isset($empresa_alteracao['documentos']['empresa_iptu_alterado']))
                    <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                    @endif
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['razao_social'])) class="text-danger" @endif>Razão Social  @if ( isset($empresa_alteracao['dados_empresa']['razao_social']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->razao_social) ? 'Vazio' :$empresa->razao_social}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['razao_social']) &&  !empty($empresa_alteracao['dados_empresa']['razao_social'])){{$empresa_alteracao['dados_empresa']['razao_social']}}
                                    @else @if(!empty($empresa->razao_social)) {{$empresa->razao_social}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['nome_fantasia'])) class="text-danger" @endif>Nome Fantasia  @if ( isset($empresa_alteracao['dados_empresa']['nome_fantasia']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->nome_comercial) ? 'Vazio' :$empresa->nome_comercial}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['nome_fantasia']) &&  !empty($empresa_alteracao['dados_empresa']['nome_fantasia'])){{$empresa_alteracao['dados_empresa']['nome_fantasia']}}
                                    @else @if(!empty($empresa->nome_comercial)) {{$empresa->nome_comercial}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['im'])) class="text-danger" @endif>CCM  @if ( isset($empresa_alteracao['dados_empresa']['im']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->im) ? 'Vazio' : $empresa->im}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['im']) &&  !empty($empresa_alteracao['dados_empresa']['im'])){{$empresa_alteracao['dados_empresa']['im']}}
                                    @else @if(!empty($empresa->im)) {{$empresa->im}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['ie'])) class="text-danger" @endif>Inscrição Estadual  @if ( isset($empresa_alteracao['dados_empresa']['ie']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->ie) ? 'Vazio' : $empresa->ie}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['ie']) &&  !empty($empresa_alteracao['dados_empresa']['ie'])){{$empresa_alteracao['dados_empresa']['ie']}}
                                    @else @if(!empty($empresa->ie)) {{$empresa->ie}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['telefone'])) class="text-danger" @endif>Telefone  @if ( isset($empresa_alteracao['dados_empresa']['telefone']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->telefone) ? 'Vazio' : $empresa->telefone}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['telefone']) &&  !empty($empresa_alteracao['dados_empresa']['telefone'])){{$empresa_alteracao['dados_empresa']['telefone']}}
                                    @else @if(!empty($empresa->telefone)) {{$empresa->telefone}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['cep'])) class="text-danger" @endif>CEP  @if ( isset($empresa_alteracao['dados_empresa']['cep']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_endereco->cep) ? 'Vazio' : $empresa->empresa_endereco->cep}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['cep']) &&  !empty($empresa_alteracao['dados_empresa']['cep'])){{$empresa_alteracao['dados_empresa']['cep']}}
                                    @else @if(!empty($empresa->empresa_endereco->cep)) {{$empresa->empresa_endereco->cep}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>

            <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['estado_id'])) class="text-danger" @endif>Estado  @if ( isset($empresa_alteracao['dados_empresa']['estado_id']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_endereco->estado->sigla) ? 'Vazio' : $empresa->empresa_endereco->estado->sigla}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['estado_id']) &&  !empty($empresa_alteracao['dados_empresa']['estado_id'])){{$empresa_alteracao['dados_empresa']['estado_id']}}
                                    @else @if(!empty($empresa->empresa_endereco->estado->sigla)) {{$empresa->empresa_endereco->estado->sigla}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                     <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['cidade_id'])) class="text-danger" @endif>Cidade  @if ( isset($empresa_alteracao['dados_empresa']['cidade_id']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_endereco->cidade->nome) ? 'Vazio' : $empresa->empresa_endereco->cidade->nome}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['cidade_id']) &&  !empty($empresa_alteracao['dados_empresa']['cidade_id'])){{$empresa_alteracao['dados_empresa']['cidade_id']}}
                                    @else @if(!empty($empresa->empresa_endereco->cidade->nome)) {{$empresa->empresa_endereco->cidade->nome}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['endereco'])) class="text-danger" @endif>Endereço  @if ( isset($empresa_alteracao['dados_empresa']['endereco']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_endereco->endereco) ? 'Vazio' : $empresa->empresa_endereco->endereco}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['endereco']) &&  !empty($empresa_alteracao['dados_empresa']['endereco'])){{$empresa_alteracao['dados_empresa']['endereco']}}
                                    @else @if(!empty($empresa->empresa_endereco->endereco)) {{$empresa->empresa_endereco->endereco}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['numero'])) class="text-danger" @endif>Número  @if ( isset($empresa_alteracao['dados_empresa']['numero']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_endereco->numero) ? 'Vazio' : $empresa->empresa_endereco->numero}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['numero']) &&  !empty($empresa_alteracao['dados_empresa']['numero'])){{$empresa_alteracao['dados_empresa']['numero']}}
                                    @else @if(!empty($empresa->empresa_endereco->numero)) {{$empresa->empresa_endereco->numero}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['bairro'])) class="text-danger" @endif>Bairro  @if ( isset($empresa_alteracao['dados_empresa']['bairro']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_endereco->bairro) ? 'Vazio' : $empresa->empresa_endereco->bairro}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['bairro']) &&  !empty($empresa_alteracao['dados_empresa']['bairro'])){{$empresa_alteracao['dados_empresa']['bairro']}}
                                    @else @if(!empty($empresa->empresa_endereco->bairro)) {{$empresa->empresa_endereco->bairro}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-6">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['complemento'])) class="text-danger" @endif>Complemento  @if ( isset($empresa_alteracao['dados_empresa']['complemento']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_endereco->complemento) ? 'Vazio' : $empresa->empresa_endereco->complemento}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['complemento']) &&  !empty($empresa_alteracao['dados_empresa']['complemento'])){{$empresa_alteracao['dados_empresa']['complemento']}}
                                    @else @if(!empty($empresa->empresa_endereco->complemento)) {{$empresa->empresa_endereco->complemento}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>
            <div class="col-xs-6">
                    <div class="form-group">
                            <label for="" @if ( isset($empresa_alteracao['dados_empresa']['ponto_referencia'])) class="text-danger" @endif>Ponto de Referência  @if ( isset($empresa_alteracao['dados_empresa']['ponto_referencia']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_endereco->ponto_referencia) ? 'Vazio' : $empresa->empresa_endereco->ponto_referencia}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                            <input type="text" id="consumo_energia" class="form-control"
                                value="@if(isset($empresa_alteracao['dados_empresa']['ponto_referencia']) &&  !empty($empresa_alteracao['dados_empresa']['ponto_referencia'])){{$empresa_alteracao['dados_empresa']['ponto_referencia']}}
                                    @else @if(!empty($empresa->empresa_endereco->ponto_referencia)) {{$empresa->empresa_endereco->ponto_referencia}}
                                            @else
                                                Vazio
                                            @endif
                                        @endif
                            " readonly/>
                    </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="justificativa">Justificativa</label>
                    <textarea id="justificativa" class="form-control" readonly>{{ $justificativas->justificativa }}</textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <hr/>
        </div>
        @if(isset($empresa->empresa_informacao_complementar->frequencia_geracao->nome) || isset($empresa->empresa_informacao_complementar->frequencia_coleta->nome))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Frequência</h2>
                </div>

                <div class="col-xs-12 col-sm-6">

                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['frequencia_geracao'])) class="text-danger" @endif>Geração Diária de Resíduos @if ( isset($empresa_alteracao['dados_empresa']['frequencia_geracao']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->frequencia_geracao->nome) ? 'Vazio' : $empresa->empresa_informacao_complementar->frequencia_geracao->nome}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value=" 
                                @if(isset($empresa_alteracao['dados_empresa']['frequencia_geracao']) &&  !empty($empresa_alteracao['dados_empresa']['frequencia_geracao'])) 
                                    {{$empresa_alteracao['dados_empresa']['frequencia_geracao']}} 
                                @else 
                                    @if(!empty($empresa->empresa_informacao_complementar->frequencia_geracao->nome))
                                         {{$empresa->empresa_informacao_complementar->frequencia_geracao->nome}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">

                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['frequencia_coleta'])) class="text-danger" @endif>Frequência Coleta @if ( isset($empresa_alteracao['dados_empresa']['frequencia_coleta']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->frequencia_coleta->nome) ? 'Vazio' : $empresa->empresa_informacao_complementar->frequencia_coleta->nome}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value=" 
                                @if(isset($empresa_alteracao['dados_empresa']['frequencia_coleta']) &&  !empty($empresa_alteracao['dados_empresa']['frequencia_coleta'])) 
                                    {{$empresa_alteracao['dados_empresa']['frequencia_coleta']}} 
                                @else 
                                    @if(!empty($empresa->empresa_informacao_complementar->frequencia_coleta->nome))
                                         {{$empresa->empresa_informacao_complementar->frequencia_coleta->nome}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif
        @if(!empty($showInfoCompl))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Outras Informações</h2>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-2">
                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['colaboradores_numero'])) class="text-danger" @endif>Número de Colaboradores @if ( isset($empresa_alteracao['dados_empresa']['colaboradores_numero']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->colaboradores_numero->nome) ? 'Vazio' : $empresa->empresa_informacao_complementar->colaboradores_numero->nome}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['colaboradores_numero']) &&  !empty($empresa_alteracao['dados_empresa']['colaboradores_numero'])){{$empresa_alteracao['dados_empresa']['colaboradores_numero']}} 
                                @else @if(!empty($empresa->empresa_informacao_complementar->colaboradores_numero->nome)) {{$empresa->empresa_informacao_complementar->colaboradores_numero->nome}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-2">

                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['energia_consumo'])) class="text-danger" @endif>Consumo Mensal de Energia @if ( isset($empresa_alteracao['dados_empresa']['energia_consumo']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->energia_consumo->nome) ? 'Vazio' : $empresa->empresa_informacao_complementar->energia_consumo->nome}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['energia_consumo']) &&  !empty($empresa_alteracao['dados_empresa']['energia_consumo'])){{$empresa_alteracao['dados_empresa']['energia_consumo']}}
                                @else  @if(!empty($empresa->empresa_informacao_complementar->energia_consumo->nome)){{$empresa->empresa_informacao_complementar->energia_consumo->nome}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-2">

                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['estabelecimento_tipo'])) class="text-danger" @endif>Local do empreendimento @if ( isset($empresa_alteracao['dados_empresa']['estabelecimento_tipo']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->estabelecimento_tipo->nome) ? 'Vazio' : $empresa->empresa_informacao_complementar->estabelecimento_tipo->nome}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['estabelecimento_tipo']) &&  !empty($empresa_alteracao['dados_empresa']['estabelecimento_tipo'])){{$empresa_alteracao['dados_empresa']['estabelecimento_tipo']}}
                                @else @if(!empty($empresa->empresa_informacao_complementar->estabelecimento_tipo->nome)) {{$empresa->empresa_informacao_complementar->estabelecimento_tipo->nome}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['area_total'])) class="text-danger" @endif>Área Total (m²) @if ( isset($empresa_alteracao['dados_empresa']['area_total']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->area_total) ? 'Vazio' : $empresa->empresa_informacao_complementar->area_total}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['area_total']) &&  !empty($empresa_alteracao['dados_empresa']['area_total'])){{$empresa_alteracao['dados_empresa']['area_total']}}
                                @else @if(!empty($empresa->empresa_informacao_complementar->area_total)) {{$empresa->empresa_informacao_complementar->area_total}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['area_construida'])) class="text-danger" @endif>Área Construída (m²)@if ( isset($empresa_alteracao['dados_empresa']['area_construida']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($empresa->empresa_informacao_complementar->area_construida) ? 'Vazio' : $empresa->empresa_informacao_complementar->area_construida}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['area_construida']) &&  !empty($empresa_alteracao['dados_empresa']['area_construida'])){{$empresa_alteracao['dados_empresa']['area_construida']}}
                                @else @if(!empty($empresa->empresa_informacao_complementar->area_construida)){{$empresa->empresa_informacao_complementar->area_construida}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
            </div>

            <div class="row">
                <hr/>
            </div>
        @endif
       



        

        @php //echo '<pre>'; print_r($empresa); @endphp

        {{-- Exibe a documentação somente se for transportador ou gerenciador de resíduo --}} 
        @if(isset($empresa->empresas_x_empresas_tipos->empresa_tipo_id) && ($empresa->empresa_informacao_complementar->tipo_ramo_atividade_id == config('enums.empresas_tipo.transportador') || $empresa->empresa_informacao_complementar->tipo_ramo_atividade_id == config('enums.tipo_atividade.gerenciador_residuo')))
            <div class="row documentosEmpresa">

                <div class="col-xs-12">
                    <h2>Arquivos</h2>
                </div>

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>CAPACIDADE JURÍDICA</h5>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="cartao_cnpj" class="control-label">Cartão CNPJ</label>
                                @if(isset($empresa->empresa_informacao_complementar->cartao_cnpj) && !empty($empresa->empresa_informacao_complementar->cartao_cnpj))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id,$empresa->empresa_informacao_complementar->cartao_cnpj])}}"
                                        class="ver-doc align-center" title="Abrir Cartão CNPJ" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento align-center"
                                                data-tipo="cartao_cnpj">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento align-center"
                                                data-tipo="cartao_cnpj">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="contrato_social" class="control-label">Contrato Social</label>
                                @if(isset($empresa->empresa_informacao_complementar->contrato_social) && !empty($empresa->empresa_informacao_complementar->contrato_social))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id,$empresa->empresa_informacao_complementar->contrato_social])}}"
                                        class="ver-doc" title="Abrir Contrato social" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="contrato_social">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="contrato_social">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>IDENTIDADE FINANCEIRA</h5>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_falencia" class="control-label">Certidão Falência Concordata</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_falencia_concordata) && !empty($empresa->empresa_informacao_complementar->certidao_falencia_concordata))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id,$empresa->empresa_informacao_complementar->certidao_falencia_concordata])}}"
                                        class="ver-doc" title="Abrir Certidão de falência concordata"
                                        target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certidao_falencia_concordata">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certidao_falencia_concordata">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="balanco_financeiro" class="control-label">Balanço Financeiro</label>
                                @if(isset($empresa->empresa_informacao_complementar->balanco_financeiro) && !empty($empresa->empresa_informacao_complementar->balanco_financeiro))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->balanco_financeiro])}}"
                                        class="ver-doc" title="Abrir balanço financeiro" target="_blank">
                                            <sidao_negativa_municipal))

                                    <div class="flequare-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="demonstrativo_contabil">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="demonstrativo_contabil">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>REGULARIDADE FISCAL</h5>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_negativa_municipal" class="control-label">Certidão Negativa
                                    Municipal</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_negativa_municipal) && !empty($empresa->empresa_informacao_complementar->certidao_negativa_municipal))

                                    <div class="flequare-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="demonstrativo_contabil">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="demonstrativo_contabil">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>REGULARIDADE FISCAL</h5>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_negativa_municipal" class="control-label">Certidão Negativa
                                    Municipal</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_negativa_municipal) && !empty($empresa->empresa_informacao_complementar->certidao_negativa_municipal))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->certidao_negativa_municipal])}}"
                                        class="ver-doc" title="Abrir certidão negativa" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certidao_negativa_municipal">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certidao_negativa_municipal">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_negativa_estadual" class="control-label">Certidão Negativa
                                    Estadual</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_negativa_estadual) && !empty($empresa->empresa_informacao_complementar->certidao_negativa_estadual))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->certidao_negativa_estadual])}}"
                                        class="ver-doc" title="Abrir certidão negativa estadual"
                                        target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certidao_negativa_estadual">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certidao_negativa_estadual">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_negativa_federal" class="control-label">Certidão Negativa
                                    Federal</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_negativa_federal) && !empty($empresa->empresa_informacao_complementar->certidao_negativa_federal))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->certidao_negativa_federal])}}"
                                        class="ver-doc" title="Abrir certidão negativa federal"
                                        target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certidao_negativa_federal">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certidao_negativa_federal">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>CAPACIDADE TÉCNICA</h5>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="art" class="control-label">ART
                                    <span>(Anotação Responsabilidade Técnica)</span></label>
                                @if(isset($empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica) && !empty($empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica])}}"
                                        class="ver-doc" title="Abrir ART" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="anotacao_responsabilidade_tecnica">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="anotacao_responsabilidade_tecnica">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="cdl" class="control-label">CDL
                                    <span>(Certificado de Dispensa de Licença)</span></label>
                                @if(isset($empresa->empresa_informacao_complementar->certificado_dispensa_licenca) && !empty($empresa->empresa_informacao_complementar->certificado_dispensa_licenca))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->certificado_dispensa_licenca])}}"
                                        class="ver-doc" title="Abrir CDL" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certificado_dispensa_licenca">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certificado_dispensa_licenca">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->certificado_dispensa_licenca_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->certificado_dispensa_licenca_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="licenca_previa" class="control-label">Licença Prévia</label>
                                @if(isset($empresa->empresa_informacao_complementar->licenca_previa) && !empty($empresa->empresa_informacao_complementar->licenca_previa))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->licenca_previa])}}"
                                        class="ver-doc" title="Abrir licença prévia" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="licenca_previa">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="licenca_previa">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_previa_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_previa_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="licenca_instalacao" class="control-label">Licença Instalação</label>
                                @if(isset($empresa->empresa_informacao_complementar->licenca_instalacao) && !empty($empresa->empresa_informacao_complementar->licenca_instalacao))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->licenca_instalacao])}}"
                                        class="ver-doc" title="Abrir licença instalação" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="licenca_instalacao">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="licenca_instalacao">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_instalacao_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_instalacao_vencimento) }}"
                                            readonly/>
                                    </div>
                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="licenca_operacao" class="control-label">Licença Operação</label>
                                @if(isset($empresa->empresa_informacao_complementar->licenca_operacao) && !empty($empresa->empresa_informacao_complementar->licenca_operacao))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->licenca_operacao])}}"
                                        class="ver-doc" title="Abrir licença operação" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="licenca_operacao">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="licenca_operacao">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_operacao_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_operacao_vencimento) }}"
                                            readonly/>
                                    </div>
                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="alvara_prefeitura" class="control-label">Alvará da Prefeitura</label>
                                @if(isset($empresa->empresa_informacao_complementar->alvara_prefeitura) && !empty($empresa->empresa_informacao_complementar->alvara_prefeitura))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->alvara_prefeitura])}}"
                                        class="ver-doc" title="Abrir alvará da prefeitura"
                                        target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="alvara_prefeitura">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="alvara_prefeitura">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->alvara_prefeitura_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->alvara_prefeitura_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="ctf_ibama" class="control-label">CTF Ibama</label>
                                @if(isset($empresa->empresa_informacao_complementar->ctf_ibama) && !empty($empresa->empresa_informacao_complementar->ctf_ibama))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->ctf_ibama])}}"
                                        class="ver-doc" title="Abrir CTF Ibama" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="ctf_ibama">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="ctf_ibama">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->ctf_ibama_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->ctf_ibama_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="avcb" class="control-label">AVCB</label>
                                @if(isset($empresa->empresa_informacao_complementar->avcb) && !empty($empresa->empresa_informacao_complementar->avcb))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->avcb])}}"
                                        class="ver-doc" title="Abrir AVCB" target="_blank">
                                            <span class="glyphicon glyphicon-eye-open"></span>
                                        </a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="avcb">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="avcb">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->avcb_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->avcb_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif

        {{--  Novas Infos para aprovação de cadastro de transportador  --}}
        @if(!empty($showRespEmp))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Responsável pelo Empreendimento</h2>
                </div>

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['nome_socio'])) class="text-danger" @endif>Nome do Sócio @if ( isset($empresa_alteracao['dados_empresa']['nome_socio']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio_principal']['nome']) ? 'Vazio' : $responsaveis['socio_principal']['nome']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['nome_socio']) &&  !empty($empresa_alteracao['dados_empresa']['nome_socio'])){{$empresa_alteracao['dados_empresa']['nome_socio']}}
                                @else @if(!empty($responsaveis['socio_principal']['nome'])) {{$responsaveis['socio_principal']['nome']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                        
                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['numero_rg_socio'])) class="text-danger" @endif>Número do RG do Sócio @if ( isset($empresa_alteracao['dados_empresa']['numero_rg_socio']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio_principal']['rg']) ? 'Vazio' : $responsaveis['socio_principal']['rg']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['numero_rg_socio']) &&  !empty($empresa_alteracao['dados_empresa']['numero_rg_socio'])){{$empresa_alteracao['dados_empresa']['numero_rg_socio']}}
                                @else @if(!empty($responsaveis['socio_principal']['rg'])) {{$responsaveis['socio_principal']['rg']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="frequencia_geracao" @if ( isset($empresa_alteracao['dados_empresa']['numero_cpf_socio'])) class="text-danger" @endif>Número do CPF do Sócio @if ( isset($empresa_alteracao['dados_empresa']['numero_cpf_socio']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio_principal']['cpf']) ? 'Vazio' : $responsaveis['socio_principal']['cpf']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['numero_cpf_socio']) &&  !empty($empresa_alteracao['dados_empresa']['numero_cpf_socio'])){{$empresa_alteracao['dados_empresa']['numero_cpf_socio']}}
                                @else @if(!empty($responsaveis['socio_principal']['cpf'])) {{$responsaveis['socio_principal']['cpf']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="numero_rg_socio_file">RG do Sócio</label>

                        @if(!empty($documentos['numero_rg_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_file" class="form-control" target="_blank">
                                arquivo atual
                            </a>
                        @endif
                        @if(isset($empresa_alteracao['documentos']['numero_rg_socio_file_alterado']))
                                <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['numero_rg_socio_file_alterado'], 'empresa' => $empresa->cnpj]) }}" id="numero_rg_socio_file" class="form-control" target="_blank">
                                    arquivo novo
                                </a>
                        @endif
                        @if(empty($documentos['numero_rg_socio_file']) && !isset($empresa_alteracao['documentos']['numero_rg_socio_file_alterado']))
                        <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_cpf_socio_file">CPF do Sócio</label>

                        @if(!empty($documentos['numero_cpf_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_cpf_socio_file" class="form-control" target="_blank">
                                arquivo atual
                            </a>
                        @endif
                        @if(isset($empresa_alteracao['documentos']['numero_cpf_socio']))
                                <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['numero_cpf_socio'], 'empresa' => $empresa->cnpj]) }}" id="numero_cpf_socio_file" class="form-control" target="_blank">
                                    arquivo novo
                                </a>
                        @endif
                        @if(empty($documentos['numero_cpf_socio_file']) && !isset($empresa_alteracao['documentos']['numero_cpf_socio']))
                        <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>

                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['nome_socio_2'])) class="text-danger" @endif>Nome do Sócio 2 @if ( isset($empresa_alteracao['dados_empresa']['nome_socio_2']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio2']['nome']) ? 'Vazio' : $responsaveis['socio2']['nome']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['nome_socio_2']) &&  !empty($empresa_alteracao['dados_empresa']['nome_socio_2'])){{$empresa_alteracao['dados_empresa']['nome_socio_2']}}
                                @else @if(!empty($responsaveis['socio2']['nome'])) {{$responsaveis['socio2']['nome']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['numero_rg_socio_2'])) class="text-danger" @endif>Número do RG do Sócio 2 @if ( isset($empresa_alteracao['dados_empresa']['numero_rg_socio_2']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio2']['rg']) ? 'Vazio' : $responsaveis['socio2']['rg']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['numero_rg_socio_2']) &&  !empty($empresa_alteracao['dados_empresa']['numero_rg_socio_2'])){{$empresa_alteracao['dados_empresa']['numero_rg_socio_2']}}
                                @else @if(!empty($responsaveis['socio2']['rg'])) {{$responsaveis['socio2']['rg']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['numero_cpf_socio_2'])) class="text-danger" @endif>Número do CPF do Sócio 2 @if ( isset($empresa_alteracao['dados_empresa']['numero_cpf_socio_2']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio2']['cpf']) ? 'Vazio' : $responsaveis['socio2']['cpf']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['numero_cpf_socio_2']) &&  !empty($empresa_alteracao['dados_empresa']['numero_cpf_socio_2'])){{$empresa_alteracao['dados_empresa']['numero_cpf_socio_2']}}
                                @else @if(!empty($responsaveis['socio2']['rg'])) {{$responsaveis['socio2']['rg']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="numero_cpf_socio_file">RG do Sócio 2</label>

                        @if(!empty($documentos['numero_rg_socio_2_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_2_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_2_file" class="form-control" target="_blank">
                                arquivo atual
                            </a>
                        @endif
                        @if(isset($empresa_alteracao['documentos']['numero_rg_socio_2_file_alterado']))
                                <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['numero_rg_socio_2_file_alterado'], 'empresa' => $empresa->cnpj]) }}" id="numero_rg_socio_2_file" class="form-control" target="_blank">
                                    arquivo novo
                                </a>
                        @endif
                        @if(empty($documentos['numero_rg_socio_2_file']) && !isset($empresa_alteracao['documentos']['numero_rg_socio_2_file_alterado']))
                        <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="numero_cpf_socio_file">CPF do Sócio 2</label>

                            @if(!empty($documentos['numero_cpf_socio_2_file']))
                                <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_2_file'], 'empresa' => $empresa->id]) }}" id="numero_cpf_socio_2_file" class="form-control" target="_blank">
                                    arquivo atual
                                </a>
                            @endif
                            @if(isset($empresa_alteracao['documentos']['numero_cpf_socio_2_file_alterado']))
                                    <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['numero_cpf_socio_2_file_alterado'], 'empresa' => $empresa->cnpj]) }}" id="numero_cpf_socio_2_file" class="form-control" target="_blank">
                                        arquivo novo
                                    </a>
                            @endif
                            @if(empty($documentos['numero_cpf_socio_2_file']) && !isset($empresa_alteracao['documentos']['numero_cpf_socio_2_file_alterado']))
                            <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                        </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;"/>
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['nome_socio_3'])) class="text-danger" @endif>Nome do Sócio 3 @if ( isset($empresa_alteracao['dados_empresa']['nome_socio_3']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio3']['nome']) ? 'Vazio' : $responsaveis['socio3']['nome']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['nome_socio_3']) &&  !empty($empresa_alteracao['dados_empresa']['nome_socio_3'])){{$empresa_alteracao['dados_empresa']['nome_socio_3']}}
                                @else @if(!empty($responsaveis['socio3']['nome'])) {{$responsaveis['socio3']['nome']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['numero_rg_socio_3'])) class="text-danger" @endif>Número do RG do Sócio 3 @if ( isset($empresa_alteracao['dados_empresa']['numero_rg_socio_3']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio3']['rg']) ? 'Vazio' : $responsaveis['socio3']['rg']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['numero_rg_socio_3']) &&  !empty($empresa_alteracao['dados_empresa']['numero_rg_socio_3'])){{$empresa_alteracao['dados_empresa']['numero_rg_socio_3']}}
                                @else @if(!empty($responsaveis['socio3']['rg'])) {{$responsaveis['socio3']['rg']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['numero_cpf_socio_3'])) class="text-danger" @endif>Número do CPF do Sócio 3 @if ( isset($empresa_alteracao['dados_empresa']['numero_cpf_socio_3']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['socio3']['cpf']) ? 'Vazio' : $responsaveis['socio3']['cpf']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['numero_cpf_socio_3']) &&  !empty($empresa_alteracao['dados_empresa']['numero_cpf_socio_3'])){{$empresa_alteracao['dados_empresa']['numero_cpf_socio_3']}}
                                @else @if(!empty($responsaveis['socio3']['cpf'])) {{$responsaveis['socio3']['cpf']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="numero_cpf_socio_file">RG do Sócio 3</label>

                            @if(!empty($documentos['numero_rg_socio_3_file']))
                                <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_3_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_3_file" class="form-control" target="_blank">
                                    arquivo atual
                                </a>
                            @endif
                            @if(isset($empresa_alteracao['documentos']['numero_rg_socio_3_file_alterado']))
                                    <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['numero_rg_socio_3_file_alterado'], 'empresa' => $empresa->cnpj]) }}" id="numero_rg_socio_3_file" class="form-control" target="_blank">
                                        arquivo novo
                                    </a>
                            @endif
                            @if(empty($documentos['numero_rg_socio_3_file']) && !isset($empresa_alteracao['documentos']['numero_rg_socio_3_file_alterado']))
                            <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                        </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="numero_cpf_socio_file">CPF do Sócio 3</label>

                            @if(!empty($documentos['numero_cpf_socio_3_file']))
                                <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_3_file'], 'empresa' => $empresa->id]) }}" id="numero_cpf_socio_3_file" class="form-control" target="_blank">
                                    arquivo atual
                                </a>
                            @endif
                            @if(isset($empresa_alteracao['documentos']['numero_cpf_socio_3_file_alterado']))
                                    <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['numero_cpf_socio_3_file_alterado'], 'empresa' => $empresa->cnpj]) }}" id="numero_cpf_socio_3_file" class="form-control" target="_blank">
                                        arquivo novo
                                    </a>
                            @endif
                            @if(empty($documentos['numero_cpf_socio_3_file']) && !isset($empresa_alteracao['documentos']['numero_cpf_socio_3_file_alterado']))
                            <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                        </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif {{--  // end if(!empty($showRespEmp))  --}}
        
        @if(!empty($showSind))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Responsável pelo Empreendimento (Dados do Síndico)</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['condominio_sindico'])) class="text-danger" @endif>Nome do Síndico Responsável @if ( isset($empresa_alteracao['dados_empresa']['condominio_sindico']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['sindico']['nome']) ? 'Vazio' : $responsaveis['sindico']['nome']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['condominio_sindico']) &&  !empty($empresa_alteracao['dados_empresa']['condominio_sindico'])){{$empresa_alteracao['dados_empresa']['condominio_sindico']}}
                                @else @if(!empty($responsaveis['sindico']['nome'])) {{$responsaveis['sindico']['nome']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['condominio_sindico_rg'])) class="text-danger" @endif>Número do RG do Síndico @if ( isset($empresa_alteracao['dados_empresa']['condominio_sindico_rg']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['sindico']['rg']) ? 'Vazio' : $responsaveis['sindico']['rg']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['condominio_sindico_rg']) &&  !empty($empresa_alteracao['dados_empresa']['condominio_sindico_rg'])){{$empresa_alteracao['dados_empresa']['condominio_sindico_rg']}}
                                @else @if(!empty($responsaveis['sindico']['rg'])) {{$responsaveis['sindico']['rg']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="" @if ( isset($empresa_alteracao['dados_empresa']['condominio_sindico_cpf'])) class="text-danger" @endif>Número CPF RG do Síndico @if ( isset($empresa_alteracao['dados_empresa']['condominio_sindico_cpf']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($responsaveis['sindico']['cpf']) ? 'Vazio' : $responsaveis['sindico']['cpf']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="consumo_energia" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['condominio_sindico_cpf']) &&  !empty($empresa_alteracao['dados_empresa']['condominio_sindico_cpf'])){{$empresa_alteracao['dados_empresa']['condominio_sindico_cpf']}}
                                @else @if(!empty($responsaveis['sindico']['cpf'])) {{$responsaveis['sindico']['cpf']}}
                                    @else
                                        Vazio
                                    @endif
                                @endif
                                " readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="numero_cpf_socio_file">RG do Síndico</label>

                            @if(!empty($documentos['condominio_sindico_cpf']))
                                <a href="{{ route('file', ['arquivo' => $documentos['condominio_sindico_cpf'], 'empresa' => $empresa->id]) }}" id="condominio_sindico_cpf" class="form-control" target="_blank">
                                    arquivo atual
                                </a>
                            @endif
                            @if(isset($empresa_alteracao['documentos']['condominio_sindico_rg_alterado']))
                                    <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['condominio_sindico_rg_alterado'], 'empresa' => $empresa->cnpj]) }}" id="condominio_sindico_rg_alterado" class="form-control" target="_blank">
                                        arquivo novo
                                    </a>
                            @endif
                            @if(empty($documentos['condominio_sindico_cpf']) && !isset($empresa_alteracao['documentos']['condominio_sindico_rg_alterado']))
                            <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                        </div>
                    </div>
                <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="numero_cpf_socio_file">CPF do Síndico</label>

                            @if(!empty($documentos['condominio_sindico_cpf']))
                                <a href="{{ route('file', ['arquivo' => $documentos['condominio_sindico_cpf'], 'empresa' => $empresa->id]) }}" id="condominio_sindico_cpf" class="form-control" target="_blank">
                                    arquivo atual
                                </a>
                            @endif
                            @if(isset($empresa_alteracao['documentos']['condominio_sindico_cpf_alterado']))
                                    <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['condominio_sindico_cpf_alterado'], 'empresa' => $empresa->cnpj]) }}" id="condominio_sindico_cpf_alterado" class="form-control" target="_blank">
                                        arquivo novo
                                    </a>
                            @endif
                            @if(empty($documentos['condominio_sindico_cpf']) && !isset($empresa_alteracao['documentos']['condominio_sindico_cpf_alterado']))
                            <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                        </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="numero_cpf_socio_file">Cópia da Ata de Assembléias de eleição do síndico</label>

                            @if(!empty($documentos['condominio_ata']))
                                <a href="{{ route('file', ['arquivo' => $documentos['condominio_ata'], 'empresa' => $empresa->id]) }}" id="condominio_ata" class="form-control" target="_blank">
                                    arquivo atual
                                </a>
                            @endif
                            @if(isset($empresa_alteracao['documentos']['condominio_ata_alterado']))
                                    <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['condominio_ata_alterado'], 'empresa' => $empresa->cnpj]) }}" id="condominio_ata" class="form-control" target="_blank">
                                        arquivo novo
                                    </a>
                            @endif
                            @if(empty($documentos['condominio_ata']) && !isset($empresa_alteracao['documentos']['condominio_ata_alterado']))
                            <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                        </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                        <div class="form-group">
                            <label for="numero_cpf_socio_file">Cópia do documento de instituição e contribuição do condomínio</label>

                            @if(!empty( $documentos['condominio_contribuicao']))
                                <a href="{{ route('file', ['arquivo' =>  $documentos['condominio_contribuicao'], 'empresa' => $empresa->id]) }}" id="condominio_contribuicao" class="form-control" target="_blank">
                                    arquivo atual
                                </a>
                            @endif
                            @if(isset($empresa_alteracao['documentos']['condominio_contribuicao_alterado']))
                                    <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['condominio_contribuicao_alterado'], 'empresa' => $empresa->cnpj]) }}" id="condominio_contribuicao" class="form-control" target="_blank">
                                        arquivo novo
                                    </a>
                            @endif
                            @if(empty($documentos['condominio_contribuicao']) && !isset($empresa_alteracao['documentos']['condominio_contribuicao_alterado']))
                            <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                        </div>
                 </div>
                <div class="col-xs-12 col-sm-4">

                        <div class="form-group">
                            <label for="numero_cpf_socio_file">Cópia da procuração da administração</label>

                            @if(!empty( $documentos['condominio_procuracao']))
                                <a href="{{ route('file', ['arquivo' =>  $documentos['condominio_procuracao'], 'empresa' => $empresa->id]) }}" id="condominio_procuracao" class="form-control" target="_blank">
                                    arquivo atual
                                </a>
                            @endif
                            @if(isset($empresa_alteracao['documentos']['condominio_procuracao_alterado']))
                                    <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['condominio_procuracao_alterado'], 'empresa' => $empresa->cnpj]) }}" id="condominio_procuracao" class="form-control" target="_blank">
                                        arquivo novo
                                    </a>
                            @endif
                            @if(empty($documentos['condominio_procuracao']) && !isset($empresa_alteracao['documentos']['condominio_procuracao_alterado']))
                            <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                        </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif {{--  // end if(!empty($showSind))  --}}
        
        @if(!empty($showPres))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Responsável pelo Empreendimento</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Nome do Presidente</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['presidente']['nome'])) ? $responsaveis['presidente']['nome'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do RG do Presidente</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['presidente']['rg'])) ? $responsaveis['presidente']['rg'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do CPF do Presidente</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['presidente']['cpf'])) ? $responsaveis['presidente']['cpf'] : '' }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_rg_socio_file">RG do Presidente</label>
                        @if(!empty($documentos['numero_rg_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_rg_socio_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_cpf_socio_file">CPF do Presidente</label>
                        @if(!empty($documentos['numero_cpf_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_cpf_socio_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_cpf_socio_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif {{--  // end if(!empty($showPres))  --}}
        
        @if(!empty($showDocs))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Documentação</h2>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">
                    <h2>Capacidade Jurídica</h2>
                </div>

                <div class="col-xs-12 col-sm-4" style="display: none;">
                    <div class="form-group">
                        <label for="">Cartão CNPJ</label>
                        <input type="text" id="" class="form-control" value=""
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_contrato_social">Contrato Social</label>
                        @if(!empty($documentos['transportador_contrato_social']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_contrato_social'], 'empresa' => $empresa->id]) }}" id="transportador_contrato_social" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_contrato_social_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_contrato_social_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_contrato_social" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_contrato_social']) && !isset($empresa_alteracao['documentos']['transportador_contrato_social_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_doc_ccm">CCM</label>
                        @if(!empty($documentos['transportador_doc_ccm']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_doc_ccm'], 'empresa' => $empresa->id]) }}" id="transportador_doc_ccm" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_doc_ccm_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_doc_ccm_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_doc_ccm" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_doc_ccm']) && !isset($empresa_alteracao['documentos']['transportador_doc_ccm_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <h2>Identidade Financeira</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_certidao_falencia_concordata">Certidão Negativa de Falência</label>
                        @if(!empty($documentos['transportador_certidao_falencia_concordata']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_falencia_concordata'], 'empresa' => $empresa->id]) }}" id="transportador_certidao_falencia_concordata" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_certidao_falencia_concordata_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_certidao_falencia_concordata_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_certidao_falencia_concordata" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_certidao_falencia_concordata']) && !isset($empresa_alteracao['documentos']['transportador_certidao_falencia_concordata_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_balanco_financeiro">Balanço Patrimonial</label>
                        @if(!empty($documentos['transportador_balanco_financeiro']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_balanco_financeiro'], 'empresa' => $empresa->id]) }}" id="transportador_balanco_financeiro" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_balanco_financeiro_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_balanco_financeiro_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_balanco_financeiro" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_balanco_financeiro']) && !isset($empresa_alteracao['documentos']['transportador_balanco_financeiro_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_demonstrativo_contabil">Demonstrativo Contábil do Último Exercício</label>
                        @if(!empty($documentos['transportador_demonstrativo_contabil']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_demonstrativo_contabil'], 'empresa' => $empresa->id]) }}" id="transportador_demonstrativo_contabil" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_demonstrativo_contabil_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_demonstrativo_contabil_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_demonstrativo_contabil" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_demonstrativo_contabil']) && !isset($empresa_alteracao['documentos']['transportador_demonstrativo_contabil_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <h2>Regularidade Fiscal</h2>
                </div>

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="transportador_certidao_negativa_municipal">Certidão Negativa Municipal</label>
                        @if(!empty($documentos['transportador_certidao_negativa_municipal']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_municipal'], 'empresa' => $empresa->id]) }}" id="transportador_certidao_negativa_municipal" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_certidao_negativa_municipal_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_certidao_negativa_municipal_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_certidao_negativa_municipal" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_certidao_negativa_municipal']) && !isset($empresa_alteracao['documentos']['transportador_certidao_negativa_municipal_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_certidao_negativa_estadual">Certidão Negativa Estadual</label>
                        @if(!empty($documentos['transportador_certidao_negativa_estadual']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_estadual'], 'empresa' => $empresa->id]) }}" id="transportador_certidao_negativa_estadual" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_certidao_negativa_estadual_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_certidao_negativa_estadual_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_certidao_negativa_municipal" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_certidao_negativa_estadual']) && !isset($empresa_alteracao['documentos']['transportador_certidao_negativa_estadual_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="transportador_certidao_negativa_federal">Certidão Negativa Federal</label>
                        @if(!empty($documentos['transportador_certidao_negativa_federal']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_federal'], 'empresa' => $empresa->id]) }}" id="transportador_certidao_negativa_federal" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_certidao_negativa_federal_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_certidao_negativa_federal_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_certidao_negativa_federal" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_certidao_negativa_federal']) && !isset($empresa_alteracao['documentos']['transportador_certidao_negativa_federal_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="transportador_crf_caixa">Certificado de Regularidade Fiscal da Caixa</label>
                        @if(!empty($documentos['transportador_crf_caixa']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_crf_caixa'], 'empresa' => $empresa->id]) }}" id="transportador_crf_caixa" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_crf_caixa_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_crf_caixa_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_crf_caixa" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_crf_caixa']) && !isset($empresa_alteracao['documentos']['transportador_crf_caixa_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <h2>Capacidade Técnica</h2>
                </div>

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="transportador_anotacao_responsabilidade_tecnica">ART</label>
                        @if(!empty($documentos['transportador_anotacao_responsabilidade_tecnica']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_anotacao_responsabilidade_tecnica'], 'empresa' => $empresa->id]) }}" id="transportador_anotacao_responsabilidade_tecnica" class="form-control" target="_blank">
                            arquivo atual
                            </a>
                        @endif    
                        @if(isset($empresa_alteracao['documentos']['transportador_anotacao_responsabilidade_tecnica_alterado']))
                        <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_anotacao_responsabilidade_tecnica_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_anotacao_responsabilidade_tecnica" class="form-control" target="_blank">
                        arquivo novo
                        </a>
                        @endif
                        @if(empty($documentos['transportador_anotacao_responsabilidade_tecnica']) && !isset($empresa_alteracao['documentos']['transportador_anotacao_responsabilidade_tecnica_alterado']))
                             <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_emissao'])) class="text-danger" @endif>Data Emissão @if ( isset($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_emissao']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_anotacao_responsabilidade_tecnica_data_emissao']) ? 'Vazio' : $documentos['transportador_anotacao_responsabilidade_tecnica_data_emissao']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_emissao']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_emissao'])) {{datetime2data($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_emissao'])}} @else @if(!empty($documentos['transportador_anotacao_responsabilidade_tecnica_data_emissao'])) {{datetime2data($documentos['transportador_anotacao_responsabilidade_tecnica_data_emissao'])}}
                                    @else Vazio
                                    @endif
                                @endif" readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_vencimento'])) class="text-danger" @endif>Data Vencimento @if ( isset($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_vencimento']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_anotacao_responsabilidade_tecnica_data_vencimento']) ? 'Vazio' : $documentos['transportador_anotacao_responsabilidade_tecnica_data_vencimento']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_vencimento']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_vencimento']))  {{datetime2data($empresa_alteracao['dados_empresa']['transportador_anotacao_responsabilidade_tecnica_vencimento'])}} @else @if(!empty($documentos['transportador_anotacao_responsabilidade_tecnica_data_vencimento'])) {{datetime2data($documentos['transportador_anotacao_responsabilidade_tecnica_data_vencimento'])}} @else
                                        Vazio
                                    @endif
                                @endif
                            " readonly/>        
                     </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                            <label for="transportador_rg_responsavel_tecnico">Cópia do RG do Responsável Técnico</label>
                            @if(!empty($documentos['transportador_rg_responsavel_tecnico']))
                                <a href="{{ route('file', ['arquivo' => $documentos['transportador_rg_responsavel_tecnico'], 'empresa' => $empresa->id]) }}" id="transportador_rg_responsavel_tecnico" class="form-control" target="_blank">
                                arquivo atual
                                </a>
                            @endif    
                            @if(isset($empresa_alteracao['documentos']['transportador_rg_responsavel_tecnico_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_rg_responsavel_tecnico_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_rg_responsavel_tecnico" class="form-control" target="_blank">
                            arquivo novo
                            </a>
                            @endif
                            @if(empty($documentos['transportador_rg_responsavel_tecnico']) && !isset($empresa_alteracao['documentos']['transportador_rg_responsavel_tecnico_alterado']))
                                <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['rg_reponsavel_tecnico'])) class="text-danger" @endif>N° do RG do Resp. Técnico @if ( isset($empresa_alteracao['dados_empresa']['rg_reponsavel_tecnico']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_rg_num_responsavel_tecnico']) ? 'Vazio' : $documentos['transportador_rg_num_responsavel_tecnico']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['rg_reponsavel_tecnico']) &&  !empty($empresa_alteracao['dados_empresa']['rg_reponsavel_tecnico'])) {$empresa_alteracao['dados_empresa']['rg_reponsavel_tecnico']}}  @else @if(!empty($documentos['transportador_rg_num_responsavel_tecnico'])) {{$documentos['transportador_rg_num_responsavel_tecnico']}} @else  Vazio
                                  @endif
                                @endif" readonly/>  
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['nome_reponsavel_tecnico'])) class="text-danger" @endif>Nome do Resp. Técnico  @if ( isset($empresa_alteracao['dados_empresa']['nome_reponsavel_tecnico']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_nome_responsavel_tecnico']) ? 'Vazio' : $documentos['transportador_nome_responsavel_tecnico']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['nome_reponsavel_tecnico']) &&  !empty($empresa_alteracao['dados_empresa']['nome_reponsavel_tecnico'])) {{$empresa_alteracao['dados_empresa']['nome_reponsavel_tecnico']}} @else  @if(!empty($documentos['transportador_nome_responsavel_tecnico'])) {{$documentos['transportador_nome_responsavel_tecnico']}}
                                @else Vazio
                                        @endif
                                @endif
                            " readonly/>
                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                            <label for="transportador_cpf_responsavel_tecnico">Cópia do CPF do Responsável Técnico</label>
                            @if(!empty($documentos['transportador_cpf_responsavel_tecnico']))
                                <a href="{{ route('file', ['arquivo' => $documentos['transportador_cpf_responsavel_tecnico'], 'empresa' => $empresa->id]) }}" id="transportador_cpf_responsavel_tecnico" class="form-control" target="_blank">
                                arquivo atual
                                </a>
                            @endif    
                            @if(isset($empresa_alteracao['documentos']['transportador_cpf_responsavel_tecnico_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_cpf_responsavel_tecnico_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_cpf_responsavel_tecnico" class="form-control" target="_blank">
                            arquivo novo
                            </a>
                            @endif
                            @if(empty($documentos['transportador_cpf_responsavel_tecnico']) && !isset($empresa_alteracao['documentos']['transportador_cpf_responsavel_tecnico_alterado']))
                                <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['cpf_responsavel_tecnico'])) class="text-danger" @endif>N° do CPF do Resp. Técnico  @if ( isset($empresa_alteracao['dados_empresa']['cpf_responsavel_tecnico']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_cpf_num_responsavel_tecnico']) ? 'Vazio' : $documentos['transportador_cpf_num_responsavel_tecnico']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['cpf_responsavel_tecnico']) &&  !empty($empresa_alteracao['dados_empresa']['cpf_responsavel_tecnico']))
                             {{$empresa_alteracao['dados_empresa']['cpf_responsavel_tecnico']}} 
                             @else 
                             @if(!empty($documentos['transportador_cpf_num_responsavel_tecnico']))
                                            {{$documentos['transportador_cpf_num_responsavel_tecnico']}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                            " readonly/>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                            <label for="transportador_crea_responsavel_tecnico">Cópia do CREA do Responsável Técnico</label>
                            @if(!empty($documentos['transportador_crea_responsavel_tecnico']))
                                <a href="{{ route('file', ['arquivo' => $documentos['transportador_crea_responsavel_tecnico'], 'empresa' => $empresa->id]) }}" id="transportador_crea_responsavel_tecnico" class="form-control" target="_blank">
                                arquivo atual
                                </a>
                            @endif    
                            @if(isset($empresa_alteracao['documentos']['transportador_crea_responsavel_tecnico_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_crea_responsavel_tecnico_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_crea_responsavel_tecnico" class="form-control" target="_blank">
                            arquivo novo
                            </a>
                            @endif
                            @if(empty($documentos['transportador_crea_responsavel_tecnico']) && !isset($empresa_alteracao['documentos']['transportador_crea_responsavel_tecnico_alterado']))
                                <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_crea_num_responsavel_tecnico'])) class="text-danger" @endif>N° do CREA do Resp. Técnico  @if ( isset($empresa_alteracao['dados_empresa']['transportador_crea_num_responsavel_tecnico']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_crea_num_responsavel_tecnico']) ? 'Vazio' : $documentos['transportador_crea_num_responsavel_tecnico']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_crea_num_responsavel_tecnico']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_crea_num_responsavel_tecnico']))
                             {{$empresa_alteracao['dados_empresa']['transportador_crea_num_responsavel_tecnico']}} 
                             @else 
                             @if(!empty($documentos['transportador_crea_responsavel_tecnico_alterado']))
                                            {{$documentos['transportador_crea_responsavel_tecnico_alterado']}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                            " readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                            <label for="transportador_certificado_dispensa_licenca">CDL</label>
                            @if(!empty($documentos['transportador_certificado_dispensa_licenca']))
                                <a href="{{ route('file', ['arquivo' => $documentos['transportador_certificado_dispensa_licenca'], 'empresa' => $empresa->id]) }}" id="transportador_certificado_dispensa_licenca" class="form-control" target="_blank">
                                arquivo atual
                                </a>
                            @endif    
                            @if(isset($empresa_alteracao['documentos']['transportador_certificado_dispensa_licenca_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_certificado_dispensa_licenca_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_certificado_dispensa_licenca" class="form-control" target="_blank">
                            arquivo novo
                            </a>
                            @endif
                            @if(empty($documentos['transportador_certificado_dispensa_licenca']) && !isset($empresa_alteracao['documentos']['transportador_certificado_dispensa_licenca_alterado']))
                                <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_emissao'])) class="text-danger" @endif>Data de Emissão @if ( isset($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_emissao']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_certificado_dispensa_licenca_data_emissao']) ? 'Vazio' : $documentos['transportador_certificado_dispensa_licenca_data_emissao']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_emissao']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_emissao']))  {{datetime2data($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_emissao'])}} @else @if(!empty($documentos['transportador_certificado_dispensa_licenca_data_emissao'])) {{datetime2data($documentos['transportador_certificado_dispensa_licenca_data_emissao'])}}
                                    @else
                                     Vazio
                                    @endif
                                @endif" readonly/>  
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_vencimento'])) class="text-danger" @endif>Data de Vencimento @if ( isset($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_vencimento']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_certificado_dispensa_licenca_data_vencimento']) ? 'Vazio' : $documentos['transportador_certificado_dispensa_licenca_data_vencimento']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_vencimento']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_vencimento'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['transportador_certificado_dispensa_licenca_vencimento'])}} 
                                    @else 
                                        @if(!empty($documentos['transportador_certificado_dispensa_licenca_data_vencimento']))
                                            {{datetime2data($documentos['transportador_certificado_dispensa_licenca_data_vencimento'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['licencadefault'])) class="text-danger" @endif>Licença @if ( isset($empresa_alteracao['dados_empresa']['licencadefault']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['licencadefault']) ? 'Vazio' : $documentos['licencadefault']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['licencadefault']) &&  !empty($empresa_alteracao['dados_empresa']['licencadefault'])) 
                                    {{$empresa_alteracao['dados_empresa']['licencadefault']}} 
                                    @else 
                                        @if(!empty($documentos['licencadefault']))
                                            {{$documentos['licencadefault']}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                            <label for="licenca">Licença</label>
                            @if(!empty($documentos['licenca']))
                                <a href="{{ route('file', ['arquivo' => $documentos['licenca'], 'empresa' => $empresa->id]) }}" id="licenca" class="form-control" target="_blank">
                                arquivo atual
                                </a>
                            @endif    
                            @if(isset($empresa_alteracao['documentos']['licenka_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['licenka_alterado'], 'empresa' => $empresa->cnpj]) }}" id="licenca" class="form-control" target="_blank">
                            arquivo novo
                            </a>
                            @endif
                            @if(empty($documentos['licenca']) && !isset($empresa_alteracao['documentos']['licenka_alterado']))
                                <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['emissaolicenca'])) class="text-danger" @endif>Data de Emissão @if ( isset($empresa_alteracao['dados_empresa']['emissaolicenca']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['licenca_data_emissao']) ? 'Vazio' : $documentos['licenca_data_emissao']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value=" @if(isset($empresa_alteracao['dados_empresa']['emissaolicenca']) &&  !empty($empresa_alteracao['dados_empresa']['emissaolicenca'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['emissaolicenca'])}} 
                                    @else 
                                        @if(!empty($documentos['licenca_data_emissao']))
                                            {{datetime2data($documentos['licenca_data_emissao'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['licencavencimento'])) class="text-danger" @endif>Data de Vencimento @if ( isset($empresa_alteracao['dados_empresa']['licencavencimento']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['licenca_data_vencimento']) ? 'Vazio' : $documentos['licenca_data_vencimento']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value=" @if(isset($empresa_alteracao['dados_empresa']['licencavencimento']) &&  !empty($empresa_alteracao['dados_empresa']['licencavencimento'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['licencavencimento'])}} 
                                    @else 
                                        @if(!empty($documentos['licenca_data_vencimento']))
                                            {{datetime2data($documentos['licenca_data_vencimento'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                            <label for="licenca">Alvará Prefeitura</label>
                            @if(!empty($documentos['transportador_alvara_prefeitura']))
                                <a href="{{ route('file', ['arquivo' => $documentos['transportador_alvara_prefeitura'], 'empresa' => $empresa->id]) }}" id="transportador_alvara_prefeitura" class="form-control" target="_blank">
                                arquivo atual
                                </a>
                            @endif    
                            @if(isset($empresa_alteracao['documentos']['transportador_alvara_prefeitura_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_alvara_prefeitura_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_alvara_prefeitura" class="form-control" target="_blank">
                            arquivo novo
                            </a>
                            @endif
                            @if(empty($documentos['transportador_alvara_prefeitura']) && !isset($empresa_alteracao['documentos']['transportador_alvara_prefeitura_alterado']))
                                <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_emissao'])) class="text-danger" @endif>Data de Emissão @if ( isset($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_emissao']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_alvara_prefeitura_data_emissao']) ? 'Vazio' : $documentos['transportador_alvara_prefeitura_data_emissao']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_emissao']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_emissao'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_emissao'])}} 
                                    @else 
                                        @if(!empty($documentos['transportador_alvara_prefeitura_data_emissao']))
                                            {{datetime2data($documentos['transportador_alvara_prefeitura_data_emissao'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_vencimento'])) class="text-danger" @endif>Data de Vencimento @if ( isset($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_vencimento']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_alvara_prefeitura_data_vencimento']) ? 'Vazio' : $documentos['transportador_alvara_prefeitura_data_vencimento']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_vencimento']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_vencimento'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['transportador_alvara_prefeitura_vencimento'])}} 
                                    @else 
                                        @if(!empty($documentos['transportador_alvara_prefeitura_data_vencimento']))
                                            {{datetime2data($documentos['transportador_alvara_prefeitura_data_vencimento'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                            <label for="licenca">CTF Ibama</label>
                            @if(!empty($documentos['transportador_ctf_ibama']))
                                <a href="{{ route('file', ['arquivo' => $documentos['transportador_ctf_ibama'], 'empresa' => $empresa->id]) }}" id="transportador_ctf_ibama" class="form-control" target="_blank">
                                arquivo atual
                                </a>
                            @endif    
                            @if(isset($empresa_alteracao['documentos']['transportador_ctf_ibama_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_ctf_ibama_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_ctf_ibama" class="form-control" target="_blank">
                            arquivo novo
                            </a>
                            @endif
                            @if(empty($documentos['transportador_ctf_ibama']) && !isset($empresa_alteracao['documentos']['transportador_ctf_ibama_alterado']))
                                <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_emissao'])) class="text-danger" @endif>Data de Emissão @if ( isset($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_emissao']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_ctf_ibama_data_emissao']) ? 'Vazio' : $documentos['transportador_ctf_ibama_data_emissao']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_emissao']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_emissao'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_emissao'])}} 
                                    @else 
                                        @if(!empty($documentos['transportador_ctf_ibama_data_emissao']))
                                            {{datetime2data($documentos['transportador_ctf_ibama_data_emissao'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                     <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_vencimento'])) class="text-danger" @endif>Data de Vencimento @if ( isset($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_vencimento']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_ctf_ibama_data_vencimento']) ? 'Vazio' : $documentos['transportador_ctf_ibama_data_vencimento']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_vencimento']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_vencimento'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['transportador_ctf_ibama_vencimento'])}} 
                                    @else 
                                        @if(!empty($documentos['transportador_ctf_ibama_data_vencimento']))
                                            {{datetime2data($documentos['transportador_ctf_ibama_data_vencimento'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                            <label for="licenca">AVCB</label>
                            @if(!empty($documentos['transportador_avcb']))
                                <a href="{{ route('file', ['arquivo' => $documentos['transportador_avcb'], 'empresa' => $empresa->id]) }}" id="transportador_avcb" class="form-control" target="_blank">
                                arquivo atual
                                </a>
                            @endif    
                            @if(isset($empresa_alteracao['documentos']['transportador_avcb_alterado']))
                            <a href="{{ route('file_temp', ['arquivo' => $empresa_alteracao['documentos']['transportador_avcb_alterado'], 'empresa' => $empresa->cnpj]) }}" id="transportador_avcb" class="form-control" target="_blank">
                            arquivo novo
                            </a>
                            @endif
                            @if(empty($documentos['transportador_avcb']) && !isset($empresa_alteracao['documentos']['transportador_avcb_alterado']))
                                <input id="" readonly="" class="form-control file" value="nenhum arquivo enviado">
                            @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_avcb_emissao'])) class="text-danger" @endif>Data de Emissão @if ( isset($empresa_alteracao['dados_empresa']['transportador_avcb_emissao']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_avcb_data_emissao']) ? 'Vazio' : $documentos['transportador_avcb_data_emissao']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_avcb_emissao']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_avcb_emissao'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['transportador_avcb_emissao'])}} 
                                    @else 
                                        @if(!empty($documentos['transportador_avcb_data_emissao']))
                                            {{datetime2data($documentos['transportador_avcb_data_emissao'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">

                    <div class="form-group">
                        <label for="data_emissao" @if ( isset($empresa_alteracao['dados_empresa']['transportador_avcb_vencimento'])) class="text-danger" @endif>Data de Vencimento @if ( isset($empresa_alteracao['dados_empresa']['transportador_avcb_vencimento']))<a href="#" data-toggle="tooltip" title="Conteúdo anterior: {{ empty($documentos['transportador_avcb_data_vencimento']) ? 'Vazio' : $documentos['transportador_avcb_data_vencimento']}}"><span class="glyphicon glyphicon-info-sign"></span></a>@endif</label>
                        <input type="text" id="ramo_atividade" class="form-control"
                            value="@if(isset($empresa_alteracao['dados_empresa']['transportador_avcb_vencimento']) &&  !empty($empresa_alteracao['dados_empresa']['transportador_avcb_vencimento'])) 
                                    {{datetime2data($empresa_alteracao['dados_empresa']['transportador_avcb_vencimento'])}} 
                                    @else 
                                        @if(!empty($documentos['transportador_avcb_data_vencimento']))
                                            {{datetime2data($documentos['transportador_avcb_data_vencimento'])}}
                                        @else
                                            Vazio
                                        @endif
                                @endif
                             " readonly/>  
                    </div>
                </div>

            </div>

            <div class="row">
                <hr />
            </div>
        @endif {{--  // end if(!empty($showDocs))  --}}

        <div class="row">

<div class="col-xs-12">
    <h2>Responsável</h2>
</div>

<div class="col-xs-12 col-sm-6">
    <div class="form-group">
        <label for="responsavel">Nome Completo do Responsável</label>
        <input type="text" id="responsavel" class="form-control" value="{{ $empresa->nome_responsavel }}"
               readonly/>
    </div>
</div>
<div class="col-xs-12 col-sm-6">
    <div class="form-group">
        <label for="email">E-mail</label>
        <input type="text" id="email" class="form-control" value="{{ $empresa->email }}" readonly/>
    </div>
</div>
<div class="col-xs-6 col-sm-3">
    <div class="form-group">
        <label for="cargo">Cargo</label>
        <input type="text" id="cargo" class="form-control" value="{{ $empresa->cargo }}" readonly/>
    </div>
</div>
<div class="col-xs-6 col-sm-3">
    <div class="form-group">
        <label for="telefone_responsavel">Telefone</label>
        <input type="tel" id="telefone_responsavel" class="form-control"
               value="{{ $empresa->telefone_responsavel }}" readonly/>
    </div>
</div>
<div class="col-xs-6 col-sm-3">
    <div class="form-group">
        <label for="ramal">Ramal</label>
        <input type="text" id="ramal" class="form-control" value="{{ $empresa->ramal }}" readonly/>
    </div>
</div>
<div class="col-xs-6 col-sm-3">
    <div class="form-group">
        <label for="celular_responsavel">Celular</label>
        <input type="text" id="celular_responsavel" class="form-control" value="{{ $empresa->celular }}"
               readonly/>
    </div>
</div>
</div>

<div class="row">
<hr/>
</div>
        {{--  /Novas Infos para aprovação de cadastro de transportador  --}}
        <div class="row margin-bottom-80">

            <div class="col-xs-12">

                <div class="text-center">

                    

                    <div class="box-cadastro">
                    <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#modal-justufiq-grande-gerador">
                            Reportar inconsistência
                        </button>
                        @if($empresa->empresas_x_empresas_tipos->empresa_tipo_id == 2)
                            <button type="button" class="btn btn-warning" id="recusarCadastro"
                                    data-empresa="{{ $empresa->id }}"> Recusar e definir como grande gerador
                            </button>
                        @endif
                        <button type="button" class="btn btn-success" id="aprovarCadastro"
                                data-empresa="{{ $empresa->id }}"><i class="fa fa-check"></i>Confirmar revisão do cadastro
                        </button>
                    </div>
                </div>
            </div>

        </div>
       

    </div>
</div>



<div id="modal-justufiq-grande-gerador" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" style="color: #f00;">&times;</button>
                <form>
                    <h2>Reportar inconsistência</h2>
                    <div class="row mensagem hide">
                        <div class="col-xs-12">
                            <p class="alert text-center" role="alert"></p>
                        </div>
                    </div>
                    <p>Descreva no campo abaixo o motivo da inconsistência.</p>
                    <div class="col-xs-12" style="padding: 0;">
                        <textarea id="reportar_inconsistencia" class="" name="reportar_inconsistencia" style="width: 100%;"></textarea>
                    </div>

                    <div class="text-center">
                        <button type="submit" id="reportarInconsistencia" class="btn btn-warning">Enviar</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<style type="text/css">
    .ver-doc {
        background-color: #008f52 !important;
        color: #fff !important;
        width: 45px;
        font-size: 18px !important;
    }
</style>