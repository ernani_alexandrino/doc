<div class="modal fade" id="modalAvisoRevisaoCadastro">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Revisão de cadastro de empresa</h4>
            </div>
            <div class="modal-body">

                <p class="msg-header">Tem certeza que deseja aprovar a revisão dos dados desta empresa?</p>

                <div class="row mensagem hide">
                    <div class="col-xs-12">
                        <p class="alert text-center" role="alert"></p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btnRevisarEmpresa">Aprovar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>