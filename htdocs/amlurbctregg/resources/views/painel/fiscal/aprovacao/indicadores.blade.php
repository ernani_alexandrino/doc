<div class="indicadores">

    <div class="col-xs-12">
        <div class="header">
            <div class="titulo">
                <h2><i class="fa fa-share-square-o"></i> Cadastros enviados</h2>
            </div>

            <div class="total">
                <span>{{ $cadastros['enviados_total'] }}</span>
            </div>
        </div>
    </div>

    <?php

        function montaBox($chave) {
            $box = "<a href='{$chave['link']}' class='{$chave['class']}' title='{$chave['title']}'>";
            $box .= "<div class='image-indicador'></div>";
            $box .= "<div class='corpo-indicador'>";
            $box .= "<div class='contador count'>{$chave['total']}</div>";
            $box .= "<div class='diviser-indicador'></div>";
            $box .= "<div class='titulo-indicador'>{$chave['title']}</div>";
            $box .= "</div></a>";
            echo $box;
        }

    ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="containerBoxIndicador shadow containerCadastros">

                {{ montaBox($cadastros['enviados']['pequeno_gerador']) }}
                {{ montaBox($cadastros['enviados']['grande_gerador']) }}
                {{ montaBox($cadastros['enviados']['transportador']) }}
                {{ montaBox($cadastros['enviados']['destino_final']) }}
                {{ montaBox($cadastros['enviados']['cooperativa']) }}
                {{ montaBox($cadastros['enviados']['condominio_misto']) }}
                {{ montaBox($cadastros['enviados']['servico_saude']) }}
                {{ montaBox($cadastros['enviados']['orgao_publico']) }}

            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="header">
            <div class="titulo">
                <h2><i class="fa fa-share-square-o reverse"></i> Cadastros a revisar</h2>
            </div>

            <div class="total">
                <span>{{ $cadastros['revisar_total'] }}</span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="containerBoxIndicador containerCadastros">

                {{ montaBox($cadastros['revisar']['pequeno_gerador']) }}
                {{ montaBox($cadastros['revisar']['grande_gerador']) }}
                {{ montaBox($cadastros['revisar']['transportador']) }}
                {{ montaBox($cadastros['revisar']['destino_final']) }}
                {{ montaBox($cadastros['revisar']['cooperativa']) }}
                {{ montaBox($cadastros['revisar']['condominio_misto']) }}
                {{ montaBox($cadastros['revisar']['servico_saude']) }}
                {{ montaBox($cadastros['revisar']['orgao_publico']) }}

            </div>
        </div>
    </div>
</div>
