<div class="modal fade" id="modalDataValidadeVeiculo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Ativação veículo sem boleto</h2>
            </div>
            <div class="modal-body">

                <div class="ajax-load"></div>
                <div class="row mensagem-validade hide">
                    <div class="col-xs-12">
                        <p id="alert-text" class="alert alert-text text-center alert-success">O veículo foi aprovado com sucesso.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <div class="col-xs-7">
                            {!! Form::label('data_validade', 'Informe a data de validade:') !!}
                            {!! Form::text('data_validade', null, ['class' => 'form-control', 'id' => 'data_validade', 'placeholder' => 'dd/mm/aaaa' , 'required']) !!}
                        </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                 <button type="button" class="btn btn-default btn-success" id="btn-ativar-veiculo-sem-boleto-salvar" value="add">Ativar</button>
                <button type="button" class="btn btn-default custom_send_fiscal" data-dismiss="modal" title="Fechar">
                    Fechar
                </button>
            </div>
        </div>
    </div>
</div>
