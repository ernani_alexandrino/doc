<hr/>
<?php

?>
<div class="col-xs-12">
    <div class="detalhesEmpresa">

        <input type="hidden" id="empresa" value="{{ $empresa->id }}"/>

        <div class="row">

            <div class="col-xs-12">
                <h2>Dados cadastrais</h2> 
            </div>
            <div class="dados-empresa">
            <div class="container-nr-amlurb">
    
                <div class="text-center">
                    <label for="nr-amlurb-anterior">NR. AMLURB ANTERIOR</label>
                    <input type="text" name="nr-amlurb-anterior" id="nr-amlurb-anterior" class="form-control  text-center" readonly
                    value="{{ (!is_null($empresa->id_limpurb_base_amlurb)) ? $empresa->id_limpurb_base_amlurb : '-' }}">
                </div>
                <!--div class="col-xs-2"></div-->
                <div class="text-center">
                    <label for="nr-amlurb-atual">NR. AMLURB ATUAL</label>
                    <input type="text" name="nr-amlurb-atual" id="nr-amlurb-atual" class="form-control  text-center" readonly value="{{ $empresa->id_limpurb }}">
                </div>                         
            </div>
        </div>

            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="cnpj">CNPJ</label>
                    <input type="text" id="cnpj" class="form-control" value="{{ $empresa->cnpj }}" readonly/>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="empresa_cartao_cnpj">Cartão CNPJ</label>
                    @if(!empty($documentos['empresa_cartao_cnpj']))
                        <a href="{{ route('file', ['arquivo' => $documentos['empresa_cartao_cnpj'], 'empresa' => $empresa->id]) }}" id="empresa_cartao_cnpj" class="form-control ver-doc" target="_blank">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    @else
                        <input id="empresa_cartao_cnpj" readonly="" class="form-control file" value="nenhum arquivo enviado">
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="empresa_num_iptu">Número do IPTU</label>
                    <input type="text" id="empresa_num_iptu" class="form-control" value="{{ (empty($documentos['empresa_num_iptu'])) ? '' : $documentos['empresa_num_iptu'] }}"
                           readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="empresa_iptu">Cópia do IPTU</label>
                    @if(!empty($documentos['empresa_iptu']))
                        <a href="{{ route('file', ['arquivo' => $documentos['empresa_iptu'], 'empresa' => $empresa->id]) }}" id="empresa_iptu" class="form-control ver-doc" target="_blank">
                            <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                    @else
                        <input id="empresa_iptu" readonly="" class="form-control file" value="nenhum arquivo enviado">
                    @endif
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="razao_social">Razão Social</label>
                    <input type="text" id="razao_social" class="form-control" value="{{ $empresa->razao_social }}"
                           readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="nome_fantasia">Nome Fantasia</label>
                    <input type="text" id="nome_fantasia" class="form-control" value="{{ $empresa->nome_comercial }}"
                           readonly/>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-2">
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" id="cep" class="form-control" value="{{ $empresa->empresa_endereco->cep }}"
                           readonly/>
                </div>
            </div>

            <div class="col-xs-12 col-sm-5">
                <div class="form-group">
                    <label for="endereco">Endereço</label>
                    <input type="text" id="endereco" class="form-control"
                           value="{{ $empresa->empresa_endereco->endereco }}" readonly/>
                </div>
            </div>
            <div class="col-xs-6 col-sm-2">
                <div class="form-group">
                    <label for="numero">Número</label>
                    <input type="text" id="numero" class="form-control"
                           value="{{ $empresa->empresa_endereco->numero }}" readonly/>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                    <label for="bairro">Bairro</label>
                    <input type="text" id="bairro" class="form-control"
                           value="{{ $empresa->empresa_endereco->bairro }}" readonly/>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-2">
                <div class="form-group">
                    <label for="estado">Estado</label>
                    <input type="text" id="estado" class="form-control"
                           value="{{ $empresa->empresa_endereco->estado->sigla }}" readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <label for="cidade">Cidade</label>
                    <input type="text" id="cidade" class="form-control"
                           value="{{ $empresa->empresa_endereco->cidade->nome }}" readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="form-group">
                    <label for="complemento">Complemento</label>
                    <input type="text" id="complemento" class="form-control"
                           value="{{ $empresa->empresa_endereco->complemento }}" readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="ponto_referencia">Ponto de Referência</label>
                    <input type="text" id="ponto_referencia" class="form-control"
                           value="{{ $empresa->empresa_endereco->ponto_referencia }}" readonly/>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="licenca_municipal">CCM</label>
                    <input type="text" id="licenca_municipal" class="form-control" value="{{ $empresa->im }}" readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="inscricao_estadual">Inscrição Estadual</label>
                    <input type="text" id="inscricao_estadual" class="form-control" value="{{ $empresa->ie }}"
                           readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group">
                    <label for="telefone">Telefone</label>
                    <input type="text" id="telefone" class="form-control" value="{{ $empresa->telefone }}" readonly/>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="ramo_atividade">Ramo de Atividade</label>
                    <input type="text" id="ramo_atividade" class="form-control"
                           value="{{ $empresa->empresa_informacao_complementar->ramo_atividade->nome }}" readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="tipo_atividade">Tipo</label>
                    <input type="text" id="tipo_atividade" class="form-control"
                           value="{{ $empresa->empresa_informacao_complementar->tipo_ramo_atividade->nome }}"
                           readonly/>
                </div>
            </div>

        </div>

        <div class="row">
            <hr/>
        </div>

        <div class="row">

            <div class="col-xs-12">
                <h2>Responsável pelo CTR-e</h2>
            </div>

            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="responsavel">Nome Completo do Responsável</label>
                    <input type="text" id="responsavel" class="form-control" value="{{ $empresa->nome_responsavel }}"
                           readonly/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="text" id="email" class="form-control" value="{{ $empresa->email }}" readonly/>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                    <label for="cargo">Cargo</label>
                    <input type="text" id="cargo" class="form-control" value="{{ $empresa->cargo }}" readonly/>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                    <label for="celular_responsavel">Celular</label>
                    <input type="text" id="celular_responsavel" class="form-control" value="{{ $empresa->celular }}"
                           readonly/>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                    <label for="telefone_responsavel">Telefone</label>
                    <input type="tel" id="telefone_responsavel" class="form-control"
                           value="{{ $empresa->telefone_responsavel }}" readonly/>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="form-group">
                    <label for="ramal">Ramal</label>
                    <input type="text" id="ramal" class="form-control" value="{{ $empresa->ramal }}" readonly/>
                </div>
            </div>
        </div>

        <div class="row">
            <hr/>
        </div>

        @if(isset($empresa->empresa_informacao_complementar->frequencia_geracao->nome) || isset($empresa->empresa_informacao_complementar->frequencia_coleta->nome))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Frequência de Geração de resíduos</h2>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="frequencia_geracao">Geração Diária de Resíduos</label>
                        <input type="text" id="frequencia_geracao" class="form-control"
                               value="{{ $empresa->empresa_informacao_complementar->frequencia_geracao->nome }}"
                               readonly/>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="frequencia_coleta">Frequência Coleta</label>
                        <input type="text" id="frequencia_coleta" class="form-control"
                               value="{{ $empresa->empresa_informacao_complementar->frequencia_coleta->nome }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif

        @if(!empty($showInfoCompl))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Outras Informações</h2>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-2">
                    <div class="form-group">
                        <label for="numero_colaboradores">Número de Colaboradores</label>
                        <input type="text" id="numero_colaboradores" class="form-control"
                               value="{{ (empty($empresa->empresa_informacao_complementar->colaboradores_numero->nome)) ? '' : $empresa->empresa_informacao_complementar->colaboradores_numero->nome }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-2">
                    <div class="form-group">
                        <label for="consumo_energia">Consumo Mensal de Energia</label>
                        <input type="text" id="consumo_energia" class="form-control"
                               value="{{ (empty($empresa->empresa_informacao_complementar->energia_consumo->nome)) ? '' : $empresa->empresa_informacao_complementar->energia_consumo->nome }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-2">
                    <div class="form-group">
                        <label for="estabelecimento_tipo">Local do empreendimento</label>
                        <input type="text" id="estabelecimento_tipo" class="form-control"
                               value="{{ (empty($empresa->empresa_informacao_complementar->estabelecimento_tipo->nome)) ? '' : $empresa->empresa_informacao_complementar->estabelecimento_tipo->nome }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="form-group">
                        <label for="area_total">Área Total (m²)</label>
                        <input type="text" id="area_total" class="form-control"
                               value="{{ (empty($empresa->empresa_informacao_complementar->area_total)) ? '' : $empresa->empresa_informacao_complementar->area_total }}" readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="form-group">
                        <label for="area_construida">Área Construída (m²)</label>
                        <input type="text" id="area_construida" class="form-control"
                               value="{{ (empty($empresa->empresa_informacao_complementar->area_construida)) ? '' : $empresa->empresa_informacao_complementar->area_construida }}" readonly/>
                    </div>
                </div>
            </div>

            <div class="row">
                <hr/>
            </div>
        @endif

        @php //echo '<pre>'; print_r($empresa); @endphp

        {{-- Exibe a documentação somente se for transportador ou gerenciador de resíduo --}} 
        @if(isset($empresa->empresa_informacao_complementar->tipo_ramo_atividade_id) && ($empresa->empresa_informacao_complementar->tipo_ramo_atividade_id == config('enums.tipo_atividade.transportador') || $empresa->empresa_informacao_complementar->tipo_ramo_atividade_id == config('enums.tipo_atividade.gerenciador_residuo')))
            <div class="row documentosEmpresa">

                <div class="col-xs-12">
                    <h2>Arquivos</h2>
                </div>

                <div class="col-xs-12">

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>CAPACIDADE JURÍDICA</h5>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="cartao_cnpj" class="control-label">Cartão CNPJ</label>
                                @if(isset($empresa->empresa_informacao_complementar->cartao_cnpj) && !empty($empresa->empresa_informacao_complementar->cartao_cnpj))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id,$empresa->empresa_informacao_complementar->cartao_cnpj])}}"
                                        class="btn btn-primary align-center" title="Abrir Cartão CNPJ" target="_blank">Abrir
                                            arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento align-center"
                                                data-tipo="cartao_cnpj">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento align-center"
                                                data-tipo="cartao_cnpj">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="contrato_social" class="control-label">Contrato Social</label>
                                @if(isset($empresa->empresa_informacao_complementar->contrato_social) && !empty($empresa->empresa_informacao_complementar->contrato_social))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id,$empresa->empresa_informacao_complementar->contrato_social])}}"
                                        class="btn btn-primary" title="Abrir Contrato social" target="_blank">Abrir
                                            arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="contrato_social">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="contrato_social">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>IDENTIDADE FINANCEIRA</h5>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_falencia" class="control-label">Certidão Falência Concordata</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_falencia_concordata) && !empty($empresa->empresa_informacao_complementar->certidao_falencia_concordata))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id,$empresa->empresa_informacao_complementar->certidao_falencia_concordata])}}"
                                        class="btn btn-primary" title="Abrir Certidão de falência concordata"
                                        target="_blank">Abrir arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certidao_falencia_concordata">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certidao_falencia_concordata">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="balanco_financeiro" class="control-label">Balanço Financeiro</label>
                                @if(isset($empresa->empresa_informacao_complementar->balanco_financeiro) && !empty($empresa->empresa_informacao_complementar->balanco_financeiro))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->balanco_financeiro])}}"
                                        class="btn btn-primary" title="Abrir balanço financeiro" target="_blank">Abrir
                                            arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="balanco_financeiro">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="balanco_financeiro">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="demonstrativo_contabil" class="control-label">Demonstrativo Contábil</label>
                                @if(isset($empresa->empresa_informacao_complementar->demonstrativo_contabil) && !empty($empresa->empresa_informacao_complementar->demonstrativo_contabil))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->demonstrativo_contabil])}}"
                                        class="btn btn-primary" title="Abrir demonstrativo contábil"
                                        target="_blank">Abrir
                                            arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="demonstrativo_contabil">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="demonstrativo_contabil">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>REGULARIDADE FISCAL</h5>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_negativa_municipal" class="control-label">Certidão Negativa
                                    Municipal</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_negativa_municipal) && !empty($empresa->empresa_informacao_complementar->certidao_negativa_municipal))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->certidao_negativa_municipal])}}"
                                        class="btn btn-primary" title="Abrir certidão negativa" target="_blank">Abrir
                                            arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certidao_negativa_municipal">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certidao_negativa_municipal">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_negativa_estadual" class="control-label">Certidão Negativa
                                    Estadual</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_negativa_estadual) && !empty($empresa->empresa_informacao_complementar->certidao_negativa_estadual))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->certidao_negativa_estadual])}}"
                                        class="btn btn-primary" title="Abrir certidão negativa estadual"
                                        target="_blank">Abrir arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certidao_negativa_estadual">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certidao_negativa_estadual">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-4">
                            <div class="form-group">
                                <label for="certidao_negativa_federal" class="control-label">Certidão Negativa
                                    Federal</label>
                                @if(isset($empresa->empresa_informacao_complementar->certidao_negativa_federal) && !empty($empresa->empresa_informacao_complementar->certidao_negativa_federal))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->certidao_negativa_federal])}}"
                                        class="btn btn-primary" title="Abrir certidão negativa federal"
                                        target="_blank">Abrir arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certidao_negativa_federal">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certidao_negativa_federal">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 green">
                            <h5>CAPACIDADE TÉCNICA</h5>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="art" class="control-label">ART
                                    <span>(Anotação Responsabilidade Técnica)</span></label>
                                @if(isset($empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica) && !empty($empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica])}}"
                                        class="btn btn-primary" title="Abrir ART" target="_blank">Abrir arquivo <i
                                                    class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="anotacao_responsabilidade_tecnica">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="anotacao_responsabilidade_tecnica">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->anotacao_responsabilidade_tecnica_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="cdl" class="control-label">CDL
                                    <span>(Certificado de Dispensa de Licença)</span></label>
                                @if(isset($empresa->empresa_informacao_complementar->certificado_dispensa_licenca) && !empty($empresa->empresa_informacao_complementar->certificado_dispensa_licenca))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->certificado_dispensa_licenca])}}"
                                        class="btn btn-primary" title="Abrir CDL" target="_blank">Abrir arquivo <i
                                                    class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="certificado_dispensa_licenca">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="certificado_dispensa_licenca">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->certificado_dispensa_licenca_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->certificado_dispensa_licenca_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="licenca_previa" class="control-label">Licença Prévia</label>
                                @if(isset($empresa->empresa_informacao_complementar->licenca_previa) && !empty($empresa->empresa_informacao_complementar->licenca_previa))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->licenca_previa])}}"
                                        class="btn btn-primary" title="Abrir licença prévia" target="_blank">Abrir
                                            arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="licenca_previa">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="licenca_previa">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_previa_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_previa_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="licenca_instalacao" class="control-label">Licença Instalação</label>
                                @if(isset($empresa->empresa_informacao_complementar->licenca_instalacao) && !empty($empresa->empresa_informacao_complementar->licenca_instalacao))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->licenca_instalacao])}}"
                                        class="btn btn-primary" title="Abrir licença instalação" target="_blank">Abrir
                                            arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="licenca_instalacao">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="licenca_instalacao">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_instalacao_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_instalacao_vencimento) }}"
                                            readonly/>
                                    </div>
                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <hr/>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="licenca_operacao" class="control-label">Licença Operação</label>
                                @if(isset($empresa->empresa_informacao_complementar->licenca_operacao) && !empty($empresa->empresa_informacao_complementar->licenca_operacao))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->licenca_operacao])}}"
                                        class="btn btn-primary" title="Abrir licença operação" target="_blank">Abrir
                                            arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="licenca_operacao">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="licenca_operacao">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_operacao_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->licenca_operacao_vencimento) }}"
                                            readonly/>
                                    </div>
                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="alvara_prefeitura" class="control-label">Alvará da Prefeitura</label>
                                @if(isset($empresa->empresa_informacao_complementar->alvara_prefeitura) && !empty($empresa->empresa_informacao_complementar->alvara_prefeitura))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->alvara_prefeitura])}}"
                                        class="btn btn-primary" title="Abrir alvará da prefeitura"
                                        target="_blank">Abrir arquivo <i class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="alvara_prefeitura">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="alvara_prefeitura">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->alvara_prefeitura_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->alvara_prefeitura_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="ctf_ibama" class="control-label">CTF Ibama</label>
                                @if(isset($empresa->empresa_informacao_complementar->ctf_ibama) && !empty($empresa->empresa_informacao_complementar->ctf_ibama))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->ctf_ibama])}}"
                                        class="btn btn-primary" title="Abrir CTF Ibama" target="_blank">Abrir arquivo <i
                                                    class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="ctf_ibama">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="ctf_ibama">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->ctf_ibama_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->ctf_ibama_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-3">
                            <div class="form-group">
                                <label for="avcb" class="control-label">AVCB</label>
                                @if(isset($empresa->empresa_informacao_complementar->avcb) && !empty($empresa->empresa_informacao_complementar->avcb))

                                    <div class="flex">
                                        <a href="{{route('download',[$empresa->id, $empresa->empresa_informacao_complementar->avcb])}}"
                                        class="btn btn-primary" title="Abrir AVCB" target="_blank">Abrir arquivo <i
                                                    class="fa fa-share-square-o"></i></a>

                                        <button type="button" class="btn btn-success aprovarDocumento"
                                                data-tipo="avcb">
                                            <i class="fa fa-check"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger desaprovarDocumento"
                                                data-tipo="avcb">
                                            <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                    <div class="flex datas">
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->avcb_emissao) }}"
                                            readonly/>
                                        <input type="text" class="form-control"
                                            value="{{ datetime2data($empresa->empresa_informacao_complementar->avcb_vencimento) }}"
                                            readonly/>
                                    </div>

                                @else
                                    <p>Não há arquivo</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif

        {{--  Novas Infos para aprovação de cadastro de transportador  --}}
        @if(!empty($showRespEmp))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Responsável pelo Empreendimento</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Nome do Sócio</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio_principal']['nome'])) ? $responsaveis['socio_principal']['nome'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do RG do Sócio</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio_principal']['rg'])) ? $responsaveis['socio_principal']['rg'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do CPF do Sócio</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio_principal']['cpf'])) ? $responsaveis['socio_principal']['cpf'] : '' }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_rg_socio_file">RG do Sócio</label>
                        @if(!empty($documentos['numero_rg_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_rg_socio_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_cpf_socio_file">CPF do Sócio</label>
                        @if(!empty($documentos['numero_cpf_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_cpf_socio_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_cpf_socio_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Nome do Sócio 2</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio2']['nome'])) ? $responsaveis['socio2']['nome'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do RG do Sócio 2</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio2']['rg'])) ? $responsaveis['socio2']['rg'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do CPF do Sócio 2</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio2']['cpf'])) ? $responsaveis['socio2']['cpf'] : '' }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_rg_socio_2_file">RG do Sócio 2</label>
                        @if(!empty($documentos['numero_rg_socio_2_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_2_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_2_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_rg_socio_2_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_cpf_socio_2_file">CPF do Sócio 2</label>
                        @if(!empty($documentos['numero_cpf_socio_2_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_2_file'], 'empresa' => $empresa->id]) }}" id="numero_cpf_socio_2_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_cpf_socio_2_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;"/>
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Nome do Sócio 3</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio3']['nome'])) ? $responsaveis['socio3']['nome'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do RG do Sócio 3</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio3']['rg'])) ? $responsaveis['socio3']['rg'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do CPF do Sócio 3</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['socio3']['cpf'])) ? $responsaveis['socio3']['cpf'] : '' }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_rg_socio_3_file">RG do Sócio 3</label>
                        @if(!empty($documentos['numero_rg_socio_3_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_3_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_3_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_rg_socio_3_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_cpf_socio_3_file">do CPF do Sócio 3</label>
                        @if(!empty($documentos['numero_cpf_socio_3_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_3_file'], 'empresa' => $empresa->id]) }}" id="numero_cpf_socio_3_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_cpf_socio_3_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif {{--  // end if(!empty($showRespEmp))  --}}
        
        @if(!empty($showSind))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Responsável pelo Empreendimento (Dados do Síndico)</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Nome do Síndico Responsável</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['sindico']['nome'])) ? $responsaveis['sindico']['nome'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do RG do Síndico</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['sindico']['rg'])) ? $responsaveis['sindico']['rg'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do CPF do Síndico</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['sindico']['cpf'])) ? $responsaveis['sindico']['cpf'] : '' }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="condominio_sindico_rg">RG do Síndico</label>
                        @if(!empty($documentos['condominio_sindico_rg']))
                            <a href="{{ route('file', ['arquivo' => $documentos['condominio_sindico_rg'], 'empresa' => $empresa->id]) }}" id="condominio_sindico_rg" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="condominio_sindico_rg" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="condominio_sindico_cpf">CPF do Síndico</label>
                        @if(!empty($documentos['condominio_sindico_cpf']))
                            <a href="{{ route('file', ['arquivo' => $documentos['condominio_sindico_cpf'], 'empresa' => $empresa->id]) }}" id="condominio_sindico_cpf" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="condominio_sindico_cpf" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="condominio_ata">Cópia da Ata de Assembléias de eleição do síndico</label>
                        @if(!empty($documentos['condominio_ata']))
                            <a href="{{ route('file', ['arquivo' => $documentos['condominio_ata'], 'empresa' => $empresa->id]) }}" id="condominio_ata" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="condominio_ata" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="condominio_contribuicao">Cópia do documento de instituição e contribuição do condomínio</label>
                        @if(!empty($documentos['condominio_contribuicao']))
                            <a href="{{ route('file', ['arquivo' => $documentos['condominio_contribuicao'], 'empresa' => $empresa->id]) }}" id="condominio_contribuicao" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="condominio_contribuicao" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="condominio_procuracao">Cópia da procuração da administração</label>
                        @if(!empty($documentos['condominio_procuracao']))
                            <a href="{{ route('file', ['arquivo' => $documentos['condominio_procuracao'], 'empresa' => $empresa->id]) }}" id="condominio_procuracao" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="condominio_procuracao" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif {{--  // end if(!empty($showSind))  --}}
        
        @if(!empty($showPres))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Responsável pelo Empreendimento</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Nome do Presidente</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['presidente']['nome'])) ? $responsaveis['presidente']['nome'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do RG do Presidente</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['presidente']['rg'])) ? $responsaveis['presidente']['rg'] : '' }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Número do CPF do Presidente</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($responsaveis['presidente']['cpf'])) ? $responsaveis['presidente']['cpf'] : '' }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    {{-- Div esclusiva para manter alinhamento do layout --}}
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_rg_socio_file">RG do Presidente</label>
                        @if(!empty($documentos['numero_rg_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_rg_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_rg_socio_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_rg_socio_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="numero_cpf_socio_file">CPF do Presidente</label>
                        @if(!empty($documentos['numero_cpf_socio_file']))
                            <a href="{{ route('file', ['arquivo' => $documentos['numero_cpf_socio_file'], 'empresa' => $empresa->id]) }}" id="numero_cpf_socio_file" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="numero_cpf_socio_file" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr/>
            </div>
        @endif {{--  // end if(!empty($showPres))  --}}
        
        @if(!empty($showDocs))
            <div class="row">

                <div class="col-xs-12">
                    <h2>Documentação</h2>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">
                    <h2>Capacidade Jurídica</h2>
                </div>

                <div class="col-xs-12 col-sm-4" style="display: none;">
                    <div class="form-group">
                        <label for="">Cartão CNPJ</label>
                        <input type="text" id="" class="form-control" value=""
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_contrato_social">Contrato Social</label>
                        @if(!empty($documentos['transportador_contrato_social']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_contrato_social'], 'empresa' => $empresa->id]) }}" id="transportador_contrato_social" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_contrato_social" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_doc_ccm">CCM</label>
                        @if(!empty($documentos['transportador_doc_ccm']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_doc_ccm'], 'empresa' => $empresa->id]) }}" id="transportador_doc_ccm" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_doc_ccm" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <h2>Identidade Financeira</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_certidao_falencia_concordata">Certidão Negativa de Falência</label>
                        @if(!empty($documentos['transportador_certidao_falencia_concordata']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_falencia_concordata'], 'empresa' => $empresa->id]) }}" id="transportador_certidao_falencia_concordata" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_certidao_falencia_concordata" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_balanco_financeiro">Balanço Patrimonial</label>
                        @if(!empty($documentos['transportador_balanco_financeiro']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_balanco_financeiro'], 'empresa' => $empresa->id]) }}" id="transportador_balanco_financeiro" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_balanco_financeiro" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_demonstrativo_contabil">Demonstrativo Contábil do Último Exercício</label>
                        @if(!empty($documentos['transportador_demonstrativo_contabil']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_demonstrativo_contabil'], 'empresa' => $empresa->id]) }}" id="transportador_demonstrativo_contabil" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_demonstrativo_contabil" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <h2>Regularidade Fiscal</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_certidao_negativa_municipal">Certidão Negativa Municipal</label>
                        @if(!empty($documentos['transportador_certidao_negativa_municipal']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_municipal'], 'empresa' => $empresa->id]) }}" id="transportador_certidao_negativa_municipal" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_certidao_negativa_municipal" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_certidao_negativa_estadual">Certidão Negativa Estadual</label>
                        @if(!empty($documentos['transportador_certidao_negativa_estadual']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_estadual'], 'empresa' => $empresa->id]) }}" id="transportador_certidao_negativa_estadual" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_certidao_negativa_estadual" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_certidao_negativa_federal">Certidão Negativa Federal</label>
                        @if(!empty($documentos['transportador_certidao_negativa_federal']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certidao_negativa_federal'], 'empresa' => $empresa->id]) }}" id="transportador_certidao_negativa_federal" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_certidao_negativa_federal" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <hr style="border-color: #CCCCCC;" />
            </div>

            <div class="row">

                <div class="col-xs-12">
                    <h2>Capacidade Técnica</h2>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_anotacao_responsabilidade_tecnica">ART</label>
                        @if(!empty($documentos['transportador_anotacao_responsabilidade_tecnica']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_anotacao_responsabilidade_tecnica'], 'empresa' => $empresa->id]) }}" id="transportador_anotacao_responsabilidade_tecnica" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_anotacao_responsabilidade_tecnica" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Emissão</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_anotacao_responsabilidade_tecnica_data_emissao'])) ? '' : $documentos['transportador_anotacao_responsabilidade_tecnica_data_emissao']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_anotacao_responsabilidade_tecnica_data_vencimento'])) ? '' : $documentos['transportador_anotacao_responsabilidade_tecnica_data_vencimento']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_rg_responsavel_tecnico">Cópia do RG do Responsável Técnico</label>
                        @if(!empty($documentos['transportador_rg_responsavel_tecnico']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_rg_responsavel_tecnico'], 'empresa' => $empresa->id]) }}" id="transportador_rg_responsavel_tecnico" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_rg_responsavel_tecnico" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">N° do RG do Resp. Técnico</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_rg_num_responsavel_tecnico'])) ? '' : $documentos['transportador_rg_num_responsavel_tecnico'] }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Nome do Resp. Técnico</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_nome_responsavel_tecnico'])) ? '' : $documentos['transportador_nome_responsavel_tecnico'] }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_cpf_responsavel_tecnico">Cópia do CPF do Responsável Técnico</label>
                        @if(!empty($documentos['transportador_cpf_responsavel_tecnico']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_cpf_responsavel_tecnico'], 'empresa' => $empresa->id]) }}" id="transportador_cpf_responsavel_tecnico" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_cpf_responsavel_tecnico" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">N° do CPF do Resp. Técnico</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_cpf_num_responsavel_tecnico'])) ? '' : $documentos['transportador_cpf_num_responsavel_tecnico'] }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_certificado_dispensa_licenca">CDL</label>
                        @if(!empty($documentos['transportador_certificado_dispensa_licenca']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_certificado_dispensa_licenca'], 'empresa' => $empresa->id]) }}" id="transportador_certificado_dispensa_licenca" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_certificado_dispensa_licenca" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Emissão</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_certificado_dispensa_licenca_data_emissao'])) ? '' : $documentos['transportador_certificado_dispensa_licenca_data_emissao']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_certificado_dispensa_licenca_data_vencimento'])) ? '' : $documentos['transportador_certificado_dispensa_licenca_data_vencimento']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Licença</label>
                        <input type="text" id="" class="form-control" value="{{ (isset($documentos['licencadefault'])) ? $documentos['licencadefault'] : '' }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="licenca">Licença</label>
                        @if(!empty($documentos['licenca']))
                            <a href="{{ route('file', ['arquivo' => $documentos['licenca'], 'empresa' => $empresa->id]) }}" id="licenca" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="licenca" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Emissão</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['licenca_data_emissao'])) ? '' : $documentos['licenca_data_emissao']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['licenca_data_vencimento'])) ? '' : $documentos['licenca_data_vencimento']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_alvara_prefeitura">Alvará Prefeitura</label>
                        @if(!empty($documentos['transportador_alvara_prefeitura']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_alvara_prefeitura'], 'empresa' => $empresa->id]) }}" id="transportador_alvara_prefeitura" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_alvara_prefeitura" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Emissão</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_alvara_prefeitura_data_emissao'])) ? '' : $documentos['transportador_alvara_prefeitura_data_emissao']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_alvara_prefeitura_data_vencimento'])) ? '' : $documentos['transportador_alvara_prefeitura_data_vencimento']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_ctf_ibama">CTF Ibama</label>
                        @if(!empty($documentos['transportador_ctf_ibama']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_ctf_ibama'], 'empresa' => $empresa->id]) }}" id="transportador_ctf_ibama" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_ctf_ibama" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Emissão</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_ctf_ibama_data_emissao'])) ? '' : $documentos['transportador_ctf_ibama_data_emissao']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_ctf_ibama_data_vencimento'])) ? '' : $documentos['transportador_ctf_ibama_data_vencimento']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="transportador_avcb">AVCB</label>
                        @if(!empty($documentos['transportador_avcb']))
                            <a href="{{ route('file', ['arquivo' => $documentos['transportador_avcb'], 'empresa' => $empresa->id]) }}" id="transportador_avcb" class="form-control ver-doc" target="_blank">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        @else
                            <input id="transportador_avcb" readonly="" class="form-control file" value="nenhum arquivo enviado">
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Emissão</label>
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_avcb_data_emissao'])) ? '' : $documentos['transportador_avcb_data_emissao']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="">Data de Vencimento</label> {{-- Pode ser uma data exata ou indeterminada --}}
                        <input type="text" id="" class="form-control" value="{{ (empty($documentos['transportador_avcb_data_vencimento'])) ? '' : $documentos['transportador_avcb_data_vencimento']->format('d/m/Y') }}"
                               readonly/>
                    </div>
                </div>

            </div>

            <div class="row">
                <hr />
            </div>
        @endif {{--  // end if(!empty($showDocs))  --}}

        {{--  /Novas Infos para aprovação de cadastro de transportador  --}}

        <div class="row margin-bottom-80">

            <div class="col-xs-12">

                <div class="text-center">

                    {{-- <button type="button" class="btn btn-primary" id="btnAtivarEmpresa"
                            data-empresa="{{ $empresa->id }}">
                        Ativar cadastro
                    </button> --}}

                    <div class="box-cadastro">
                        <button class="btn btn-danger"
                                type="button"
                                data-toggle="modal" 
                                data-target="#modal-justufiq-grande-gerador">
                            Reportar inconsistência
                        </button>
                        @if($empresa->empresas_x_empresas_tipos->empresa_tipo_id == 2)
                            <button type="button" class="btn btn-warning" id="recusarCadastro"
                                    data-empresa="{{ $empresa->id }}"> Recusar e definir como grande gerador
                            </button>
                        @endif
                        <button type="button" class="btn btn-success" id="aprovarCadastro"
                                data-empresa="{{ $empresa->id }}"> Liberar para próxima etapa
                        </button>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>


<!-- Modal -->
<div id="modal-justufiq-grande-gerador" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" style="color: #f00;">&times;</button>
                <form>
                    <h2>Reportar inconsistência</h2>
                    <div class="row mensagem hide">
                        <div class="col-xs-12">
                            <p class="alert text-center" role="alert"></p>
                        </div>
                    </div>
                    <p>Descreva no campo abaixo o motivo da inconsistência.</p>
                    <div class="col-xs-12" style="padding: 0;">
                        <textarea id="reportar_inconsistencia" class="" name="reportar_inconsistencia" style="width: 100%;"></textarea>
                    </div>

                    <div class="text-center">
                        <button type="submit" id="reportarInconsistencia" class="btn btn-warning">Enviar</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>



<style type="text/css">
    .box-cadastro button {
        width: 320px;
        text-transform: uppercase;
        margin: 0 10px;
    }
    .ver-doc {
        background-color: #008f52 !important;
        color: #fff !important;
        width: 45px;
        font-size: 18px !important;
    }
</style>