<div class="modal fade" id="modalInconsistenciaVeiculo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Inconsistência Veículo</h4>
                <h3 id="titulo_empresa"></h3>
            </div>
            <div class="modal-body">

                <p class="msg-header">Descreva o motivo para esta operação</p>

                <div class="row mensagem hide">
                    <div class="col-xs-12">
                        <p class="alert text-center" role="alert"></p>
                    </div>
                </div>
                <input type="hidden" name="id_veiculo" id="id_veiculo">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="descricao">Descrição</label>
                            <textarea name="descricao" id="descricao" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row last-justificativa hide">
               
                    <div class="col-xs-12">
                    <div class="form-group">
                    <label for="descricao">Última Inconsistência:</label>
                        <textarea name="descricao" id="descricao" rows="5" class="form-control alert" readonly></textarea>
                     
                    </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="button" class="btn btn-success" id="concluir-inconsistencia-veiculo">Concluir operação</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>
