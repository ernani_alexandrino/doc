<div class="col-xs-12">
    <h2>Geradores do Condomínio</h2>

    <div class="box-rastreabilidade">

        <div class="row">

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover table-cadastro-vinculos-gg table-padrao" id="cadastro_vinculos_geradores_condominio">
                        <thead>
                        <tr>
                            <th>Nr. Amlurb</th>
                            <th>Empresa</th>
                            <th>CNPJ</th>
                            <th>Data Vencimento</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12">
    <h2>Transportadores Vinculados</h2>

    <div class="box-rastreabilidade">

        <div class="row">

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover table-cadastro-vinculos-gg table-padrao" id="cadastro_vinculos_geradores">
                        <thead>
                        <tr>
                            <th>Nr. Amlurb</th>
                            <th>Empresa</th>
                            <th>CNPJ</th>
                            <th>Data Vencimento</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>