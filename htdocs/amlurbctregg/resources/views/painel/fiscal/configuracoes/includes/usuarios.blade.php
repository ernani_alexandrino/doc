<div class="col-xs-12 margin-top green">
    <p>LISTA DE USUÁRIOS CADASTRADOS</p>
</div>

<div class="col-xs-12">
    <table class="table table-hover table-padrao" id="transportadores-cadastrados">
        <thead>
        <tr>
            <th width="15%">Nome</th>
            <th width="35%">E-mail</th>
            <th width="27%">Cargo</th>
            <th width="11%">Perfil</th>
            <th class="no-sort camp-actions-table">Ação</th>
        </tr>
        </thead>
        <tbody>

        @foreach(Auth::user()->empresa->users AS $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{isset($user->user_perfils->cargo) ? $user->user_perfils->cargo : ''}}</td>
                <td>{{ $user->perfil_id }}</td>
                <td>
                    <div class="flex">
                        <a title="Editar usuário" onclick="editarUsuario({{ $user->id }});" class="editarRegistro">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a href="{{ route('destroy_user',$user->id) }}"
                           onclick="return confirm('Tem certeza que deseja deletar esse usuário?');"
                           class="excluirRegistro" title="Excluir usuário">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>

<div class="col-xs-12 text-center">
    <button type="button" class="btn btn-fechar custom_send_fiscal" onclick="cadastrarUsuario();">CADASTRAR
        NOVO USUÁRIO
    </button>
</div>

