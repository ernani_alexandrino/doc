<div class="col-xs-12">
    <div class="row">

        {{ Form::open(array('route' => array('usuario.atualizar', Auth::user()->id,'painel.configuracoes'),'files' => true)) }}

        <div class="col-xs-12 margin-top">
            <p class="green">INFORMAÇÕES DO SEU USUÁRIO</p>
        </div>

        @if(Session::has('messages'))
            <div class="col-xs-12">
                <div class="alert alert-{{ Session::get('messages')['type'] }}" role="alert">
                    <p>{{ Session::get('messages')['message'] }}</p>
                </div>
            </div>
        @endif

        <div class="col-xs-12 col-sm-6 col-lg-3">
            @include('painel.meu-cadastro.usuario_formfields.nome')
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
            @include('painel.meu-cadastro.usuario_formfields.sobrenome')
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
            @include('painel.meu-cadastro.usuario_formfields.email')
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
            @include('painel.meu-cadastro.usuario_formfields.data_nascimento')
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-4">
            @include('painel.meu-cadastro.usuario_formfields.cargo')
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
            @include('painel.meu-cadastro.usuario_formfields.celular')
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
            @include('painel.meu-cadastro.usuario_formfields.telefone')
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-2">
            @include('painel.meu-cadastro.usuario_formfields.ramal')
        </div>

        <div class="col-xs-12 col-sm-6 col-lg-3">
            <div class="form-group">
                <label for="permissoes">Permissões</label>
                {!! Form::select('permissao', $permissoes, Auth::user()->perfil_id, ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="col-xs-12">
            <hr/>
        </div>

        @include('painel.meu-cadastro.usuario_formfields.imagem_perfil')

        <div class="col-xs-12">
            <hr/>
        </div>

        <div class="col-xs-12">
            <p class="green">ALTERAR SENHA DE ACESSO</p>
        </div>
        <div class="col-xs-12 col-sm-6">
            @include('painel.meu-cadastro.usuario_formfields.senha')
        </div>
        <div class="col-xs-12 col-sm-6">
            @include('painel.meu-cadastro.usuario_formfields.repita_senha')
        </div>

        <div class="col-xs-12 text-center">
            <button type="submit" class="btn custom_send_fiscal">SALVAR</button>
        </div>


        <div class="clearfix"></div>
        {{ Form::close() }}

        <br/>

    </div>
</div>
