@extends('layouts.include.script')

@section('scripts-complementar')
    <script src="{{ asset('js/fiscal.js') }}?v={{config('app.version')}}"></script>
@endsection
