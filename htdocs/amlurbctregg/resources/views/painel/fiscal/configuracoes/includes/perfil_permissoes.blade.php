<div class="col-xs-12 margin-top green">
    <p>PERFIL DE PERMISSÕES</p>
</div>

<div class="col-xs-12">
    <table class="table  table-hover table-padrao" id="perfil-permissoes-table">
        <thead>
        <tr>
            <th width="15%">
                Nome do Perfil
            </th>
            <th width="25%">
                Acessos
            </th>
            <th width="25%">
                Responsabilidades (Notif. por E-mail)
            </th>
            <th width="10%" class="no-sort">
                Ação
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($perfis as $perfil)
            <tr>
                <td>{{$perfil->nome}}</td>
                <td>
                    @foreach($permissions as $permissionIcon)
                        <span class="icons-permission {{ $permissionIcon->name }} {{ !isset($perfil->acesso[$permissionIcon->name]) ? 'inativo': null }}"></span>
                    @endforeach
                </td>
                <td>{{ $perfil->email }}</td>
                <td>
                    <div class="flex">
                        <a href="#!" title="Editar Perfil"
                           onclick="callModalEdit('{{ route('perfil.edit',$perfil->id) }}','#modalEditarPerfil');"
                           class="editarRegistro">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                        <a href="{{ route('perfil.delete',$perfil->id) }}"
                           onclick="return confirm('Tem certeza que deseja deletar esse perfil?');"
                           class="excluirRegistro">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<div class="col-xs-12 text-center">
    <button type="button" class="btn btn-fechar custom_send_fiscal" data-toggle="modal"
            data-target="#modalCadastroPerfil">CADASTRAR
    </button>
</div>
