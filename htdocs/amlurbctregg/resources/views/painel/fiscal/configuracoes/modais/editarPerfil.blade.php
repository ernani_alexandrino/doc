<!-- Modal -->
<div class="modal-dialog modal-lg" style="width: 78%;">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" title="Fechar">&times;</button>
            <h4 class="modal-title green text-center">CADASTRAR PERFIL </h4>
        </div>
        <div class="modal-body">

            {{ Form::open(array('route' => array('perfil.update', $perfil->id), 'files' => true)) }}

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarInputNome" class="control-label">Nome do Perfil</label>
                        <input id="editarInputNome" value="{{$perfil->nome}}" name="nome" class="form-control"
                               type="text" required>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-responsive table-hover " id="permissoes-table">
                        <thead>
                        <tr>
                            <th width="20%">
                                Acesso ao Menu
                            </th>
                            <th colspan="{{$cols}}">
                                Ações Específicas
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <label class="form-check-label-sm">
                                            <input class="form-check-input check-actions" type="checkbox"
                                                   name="permissions[]"
                                                   value="{{$permission->name}}" {{isset($perfil->acesso[$permission->name]) ? 'checked': null}}>
                                            <span class="icons-permission {{$permission->name}}">
                                            </span>
                                            {{$permission->label}}
                                        </label>
                                    </div>
                                </td>
                                @foreach($permission->actions as $action)
                                    <td>
                                        <div class="form-check">
                                            <label class="form-check-label-sm">
                                                <input class="form-check-input check-permission" type="checkbox"
                                                       name="{{ $permission->name }}[]"
                                                       value="{{ $action->name }}" {{ permission_has_action($permission->name,$action->name,$perfil->acesso) ? 'checked': null }}>
                                                {{ $action->label }}
                                            </label>
                                        </div>
                                    </td>
                                @endforeach
                                @if($permission->actions->count() < $cols)
                                    <td colspan="{{ $cols - $permission->actions->count() }}"></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-responsive" id="perfil-email-table">
                        <thead>
                        <tr>
                            <th colspan="{{ $tiposEmail->count() }}">
                                Notificações por E-mail
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($tiposEmail as $email)
                                <td>
                                    <div class="form-check">
                                        <label class="form-check-label-sm">
                                            <input class="form-check-input" type="checkbox" name="emails[]"
                                                   value="{{ $email->id }}" {{ isset($perfil->emails[$email->id]) ? 'checked': null }}>
                                            {{ $email->nome }}
                                        </label>
                                    </div>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <hr/>

            <div class="row">
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-fechar custom_send_fiscal">SALVAR</button>
                </div>
            </div>

            {{ Form::close() }}

        </div>
    </div>
</div>
