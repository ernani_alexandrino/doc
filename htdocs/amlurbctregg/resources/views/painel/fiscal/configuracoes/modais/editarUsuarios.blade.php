<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" title="Fechar">&times;</button>
            <h4 class="modal-title green text-center">EDITAR USUÁRIO</h4>
        </div>
        <div class="modal-body">

            {{ Form::open(array('route' => array('usuario.atualizar', $usuario->id,'painel.configuracoes'), 'files' => true)) }}

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarNome">Nome</label>
                        <input type="text" name="nome" id="editarNome" class="form-control" value="{{$usuario->name}}"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarSobrenome">Sobrenome</label>
                        <input type="text" id="editarSobrenome" name="sobrenome"
                               value="{{ $usuario->sobrenome }}"
                               class="form-control"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarDataNascimento">Data de Nascimento</label>
                        <input type="text" name="data_nascimento" id="editarDataNascimento"
                               value="{{ $usuario->user_perfils->data_nascimento }}" class="form-control calendario"/>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarCargo">Cargo</label>
                        <input type="text" id="editarCargo" class="form-control" name="cargo"
                               value="{{ $usuario->user_perfils->cargo }}"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarTelefone">Telefone</label>
                        <input type="tel" name="telefone" id="editarTelefone" class="form-control telefone"
                               value="{{ $usuario->user_perfils->telefone }}"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarRamal">Ramal</label>
                        <input type="text" name="ramal" id="editarRamal"
                               value="{{ $usuario->user_perfils->ramal }}"
                               class="form-control"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarCelular">Celular</label>
                        <input type="tel" name="celular" id="editarCelular"
                               value="{{ $usuario->user_perfils->celular }}"
                               class="form-control telefone"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarEmail">E-mail <span
                                    class="notation">(será usado para fazer login) </span></label>
                        <input type="email" name="email" id="editarEmail" value="{{ $usuario->email }}"
                               class="form-control"/>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarPermissao">Perfil <span class="notation">(permissão) </span></label>
                        {!! Form::select('permissao', $perfils, $usuario->perfil_id, ['class'=>'form-control', 'id'=>'editarPermissao']) !!}
                    </div>
                </div>
            </div>

            <hr/>

            <div class="row">

                <div class="col-xs-12">
                    <h4 class="modal-title green">FOTO DO PERFIL</h4>
                </div>

                <div class="col-md-4">
                    <p><label for="editarImagem">Imagem de Perfil Atual</label></p>
                    @if(isset($usuario->user_perfils->imagem_perfil))
                        <img src="{{ asset('uploads/imagens_perfil/'.$usuario->user_perfils->imagem_perfil) }}"
                             class="img-responsive" alt="{{ $usuario->name }}"/>
                    @endif
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="editarImagem">Nova Imagem do Perfil</label>
                        <input type="file" name="imagem_perfil" id="editarImagem" class="form-control"/>
                    </div>
                </div>
            </div>

            <hr/>

            <div class="row">

                <div class="col-xs-12">
                    <h4 class="modal-title green">ALTERAR SENHA DE ACESSO</h4>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="editarNovaSenha">Nova Senha</label>
                        <input type="password" name="password" id="editarNovaSenha" class="form-control"/>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="editarRepitaSenha" class="control-label">Repita Nova Senha</label>
                        <input type="password" name="password_confirmation" id="editarRepitaSenha"
                               class="form-control"/>
                    </div>
                </div>
            </div>

            <hr/>

            <div class="row">
                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn btn-fechar custom_send_fiscal" id="editar_usuario">SALVAR</button>
                </div>
            </div>

            {{ Form::close() }}

        </div>
    </div>
</div>
