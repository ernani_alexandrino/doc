<!-- Modal -->
<div id="modalCadastroUsuario" class="modal fade " role="dialog">

    <div id="loadingModal">
        <div class="containerLoading">
            <img src="/images/logo-loading.gif" alt="Loading..."/>
        </div>
    </div>

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title green text-center">CADASTRAR USUÁRIO</h4>
            </div>

            <div class="modal-body">
                {{ Form::open(array('route' => array('painel.usuario.cadastro'),'files' => true, 'id="formCadastrarUsuario"')) }}

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadUsuarioNome" class="control-label">Nome</label>
                            <input type="text" id="cadUsuarioNome" name="nome" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadSobrenome" class="control-label">Sobrenome</label>
                            <input type="text" id="cadSobrenome" name="sobrenome" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadDataNascimento" class="control-label">Data de Nascimento</label>
                            <input type="text" id="cadDataNascimento" name="data_nascimento"
                                   class="form-control calendario"/>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadCargo" class="control-label">Cargo</label>
                            <input type="text" id="cadCargo" name="cargo" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadTelefone" class="control-label">Telefone</label>
                            <input type="tel" id="cadTelefone" name="telefone" class="form-control telefone"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadRamal" class="control-label">Ramal</label>
                            <input type="text" id="cadRamal" name="ramal" class="form-control"/>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadCelular" class="control-label">Celular</label>
                            <input type="text" name="celular" id="cadCelular" class="form-control telefone"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadEmail" class="control-label">Email <span class="notation">(será usado para fazer login) </span></label>
                            <input type="email" name="email" id="cadEmail" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cadPermissao">Perfil <span class="notation">(permissão) </span></label>
                            <select name="permissao" class="form-control" id="cadPermissao">
                                @foreach($perfis as $perfil)
                                    <option value="{{$perfil->id}}">{{$perfil->nome}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">

                    <div class="col-xs-12">
                        <h4 class="modal-title green">FOTO DO PERFIL</h4>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="imagem_perfil" class="control-label">Nova Imagem do Perfil</label>
                            <input type="file" name="imagem_perfil" class="form-control" id="imagem_perfil"/>
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">

                    <div class="col-xs-12">
                        <h4 class="modal-title green">ALTERAR SENHA DE ACESSO</h4>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cadNovaSenha" class="control-label">Nova Senha</label>
                            <input type="password" name="password" id="cadNovaSenha" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cadRepitaSenha" class="control-label">Repita Nova Senha</label>
                            <input type="password" name="password_confirmation" id="cadRepitaSenha"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn btn-fechar custom_send_fiscal">SALVAR</button>
                    </div>
                </div>

                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
