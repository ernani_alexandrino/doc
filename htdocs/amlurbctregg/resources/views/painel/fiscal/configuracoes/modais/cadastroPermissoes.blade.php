<!-- Modal -->
<div id="modalCadastroPerfil" class="modal fade " role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" title="Fechar">&times;</button>
                <h4 class="modal-title green text-center">CADASTRAR / EDITAR PERFIL DE PERMISSÕES</h4>
            </div>
            <div class="modal-body">

                {{ Form::open(array('route' => array('perfil.store'))) }}

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="PermissoesNome" class="control-label">Nome do Perfil</label>
                            <input id="PermissoesNome" name="nome" class="form-control" type="text" required/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <table class="table table-sm table-hover " id="permissoes-table">
                            <thead>
                            <tr>
                                <th width="250">Acesso ao Menu</th>
                                <th colspan="{{$cols}}">Ações Específicas</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($permissions as $permission)
                                <tr>
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input class="form-check-inpu check-actionst" type="checkbox"
                                                       name="permissions[]" value="{{ $permission->name }}">
                                                <span class="icons-permission {{ $permission->name }}"></span>{{ $permission->label }}
                                            </label>
                                        </div>
                                    </td>
                                    @foreach($permission->actions as $action)
                                        <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input class="form-check-input check-permission" type="checkbox"
                                                           name="{{ $permission->name }}[]"
                                                           value="{{ $action->name }}"> {{$action->label}}
                                                </label>
                                            </div>
                                        </td>
                                    @endforeach

                                    @if($permission->actions->count() < $cols)
                                        <td colspan="{{ $cols - $permission->actions->count() }}"></td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <table class="table " id="perfil-email-table">
                            <thead>
                            <tr>
                                <th colspan="{{ $tiposEmail->count() }}">Notificações por E-mail</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach($tiposEmail as $email)
                                    <td>
                                        <div class="checkbox">
                                            <label>
                                                <input class="form-check-input" type="checkbox" name="emails[]"
                                                       value="{{ $email->id }}"> {{ $email->nome }}
                                            </label>
                                        </div>
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <hr/>

                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-fechar custom_send_fiscal">SALVAR</button>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
