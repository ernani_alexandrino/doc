@extends('layouts.home-painel-fiscal')

@section('conteudo')

    <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

    <div class="cadastro-gerador">
        <div role="tab-v1">
            <!-- Nav tabs -->
            <ul class="nav nav-pills nav-justified bg-transparent-tab" id="fiscal" role="tablist">
                <li role="presentation" class="active">
                    <a href="#geral" aria-controls="geral" role="tab" data-toggle="tab" title="Perfil">
                        PERFIL
                    </a>
                </li>
                <li role="presentation">
                    <a href="#usuarios" aria-controls="usuarios" role="tab" data-toggle="tab" title="Usuários">
                        USUÁRIOS
                    </a>
                </li>
                <li role="presentation">
                    <a href="#permissoes" aria-controls="usuarios" role="tab" data-toggle="tab" title="Permissões">
                        PERMISSÕES
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="geral">
                    @include('painel.fiscal.configuracoes.includes.perfil_user')
                </div>
                <div role="tabpanel" class="tab-pane fade" id="usuarios">
                    @include('painel.fiscal.configuracoes.includes.usuarios')
                </div>
                <div role="tabpanel" class="tab-pane fade" id="permissoes">
                    @include('painel.fiscal.configuracoes.includes.perfil_permissoes')
                </div>
            </div>
        </div>
    </div>

    @include('painel.fiscal.configuracoes.modais.cadastroUsuario')

    @include('painel.fiscal.configuracoes.modais.cadastroPermissoes')

    <div id="modalEditarPerfil" class="modal fade " role="dialog"></div>

    <div id="modalEditUsuario" class="modal fade " role="dialog"></div>

@endsection