<div class="col-xs-12">
    <div class="row">

        <ul class="nav nav-tabs nav-justified nav-tabs-cadastros-fiscal" role="tablist">
            <li role="presentation" class="active">
                <a href="#dados-cadastrais" aria-controls="dados-cadastrais" role="tab" data-toggle="tab">Dados Cadastrais</a>
            </li>
            <li role="presentation">
                <a href="#vinculos" aria-controls="vinculos" role="tab" data-toggle="tab">Vínculos</a>
            </li>
            <li role="presentation">
                <a href="#inconsistencias" aria-controls="inconsistencias" role="tab" data-toggle="tab">Inconsistências</a>
            </li>
            <li role="presentation">
                <a href="#inf-financeiras" aria-controls="inf-financeiras" role="tab" data-toggle="tab">Informações Financeiras</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="dados-cadastrais">

                @include('painel.fiscal.cadastro-df.include.dados-cadastrais-df')

            </div>
            <div role="tabpanel" class="tab-pane" id="vinculos">

                @include('painel.fiscal.cadastro-df.include.vinculos')

            </div>
            <div role="tabpanel" class="tab-pane" id="inconsistencias">

                @include('painel.fiscal.cadastro-df.include.incosistencias')

            </div>
            <div role="tabpanel" class="tab-pane" id="inf-financeiras">

                @include('painel.fiscal.cadastro-df.include.informacoes-financeiras')

            </div>
        </div>

    </div>

</div>