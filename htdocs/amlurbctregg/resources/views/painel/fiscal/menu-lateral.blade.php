<div class="col-xs-12 col-sm-2">
    <div class="row">

        <div class="menu-lateral">

            <img class="logo-ctre-menu" src="{{ asset('images/novo-logo-letra-em-branco.svg') }}"
                 alt="CTRE - Controle de Transporte de Resíduos - Eletrônico">

            <div class="user">
                @if(empty(Auth::user()->user_perfils->imagem_perfil))
                    <div class="img-user"></div>
                @else
                    <div class="img-user img-circle"
                         style="background-image: url({{ asset('uploads/imagens_perfil/'.Auth::user()->user_perfils->imagem_perfil) }});"></div>
                @endif
                <div class="inf-user">
                    <p>{{ auth()->user()->name }}</p>
                    <span></span>
                </div>
            </div>

            <ul class="nav nav-pills nav-stacked">
                <li>
                    <a href="{{ route('fiscal') }}" id="m_fiscal" title="Painel de controle">
                        <i class="painel-controle"></i>Painel de Controle
                    </a>
                </li>
                <li>
                    <a href="{{ route('rastreabilidade_fs') }}" id="m_rastreabilidade_fs" title="Rastreabilidade">
                        <i class="rastreabilidade"></i>Rastreabilidade
                    </a>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                       aria-expanded="false" title="Cadastros empresas">
                        <i class="cadastro-geradores"></i> Cadastros Empresas <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('cadastro_gg_fs') }}" id="m_cadastro_gg_fs" title="Grandes Geradores">
                                <i class="grandes-geradores"></i>Grandes Geradores
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cadastro_gc_fs') }}" id="m_cadastro_gc_fs"
                               title="Grandes Geradores Mistos">
                                <i class="gerador-mistos"></i>Condomínios Mistos
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cadastro_pg_fs') }}" id="m_cadastro_pg_fs" title="Pequenos Geradores">
                                <i class="pequenos-geradores"></i>Pequenos Geradores
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cadastro_tr_fs') }}" id="m_cadastro_tr_fs" title="Transportadores">
                                <i class="transportadorres"></i>Transportadores
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cadastro_df_fs') }}" id="m_cadastro_df_fs" title="Destinos Finais">
                                <i class="destinos-finais"></i>Destinos Finais
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cadastro_cr_fs') }}" id="m_cadastro_cr_fs" title="Cooperativas">
                                <i class="cooperativas"></i>Cooperativas
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cadastro_ss_fs') }}" id="m_cadastro_ss_fs" title="Serviço de Saúde">
                                <i class="servicoSaude"></i>Serviço de Saúde
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cadastro_op_fs') }}" id="m_cadastro_op_fs" title="Órgão Público">
                                <i class="orgaoPublico"></i>Órgão Público
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('residuo_fs') }}" id="m_residuo_fs" title="Cadastro resíduo">
                        <i class="residuo"></i>Cadastro Resíduo
                    </a>
                </li>
                <li>
                    <a href="{{ route('fiscal.aprovacao.dashboard') }}" id="m_fiscal.aprovacao.dashboard" title="Aprovação de cadastro">
                        <i class="aprovacao-cadastro"></i> Aprovação de cadastro
                    </a>
                </li>
                <li>
                    <a href="{{ route('messages') }}" id="m_messages" title="Mensagens">
                        <i class="chat"></i>Mensagens @include('messenger.unread-count')
                    </a>
                </li>
                <li>
                    <a href="{{ route('painel.configuracoes') }}" id="m_configuracoes" title="Configurações">
                        <i class="configuracoes"></i>Configurações
                    </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                       title="Logout">
                        <i class="logout"></i>Logout
                    </a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>

        </div>
    </div>
</div>