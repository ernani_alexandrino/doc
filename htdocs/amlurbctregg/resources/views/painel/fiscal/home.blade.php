@extends('layouts.home-painel-fiscal')

@section('conteudo')

    <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

    @include('painel.fiscal.include.home.indicadores')

    @include('painel.fiscal.include.home.indicadores-periodo')

    {{-- @include('painel.fiscal.include.home.cadastros-diario-oficial')--}}

    @include('painel.fiscal.include.home.dashboard')

@endsection
