@extends('layouts.home-painel-fiscal')

@section('conteudo')

    <div class="ajax-panel"></div>

    <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

    <div class="col-xs-12">

        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h2>Relação de Empresas Transportadoras</h2>
            </div>
            <div class="col-xs-12 col-sm-6">
                @include('painel.fiscal.cadastro-tr.include.search')
            </div>
        </div>

        <div class="box-rastreabilidade">

            <div class="row">
                <div class="col-xs-12">

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane active" id="lista-empresas">

                            <div class="table-responsive">
                                <table class="table table-hover fiscal-cadastros" id="cadastro_transportadores">
                                    <thead>
                                    <tr>
                                        <th>Nr. Amlurb</th>
                                        <th>Empresa</th>
                                        <th>Razão Social</th>
                                        <th>CNPJ</th>
                                        <th>Bairro</th>
                                        <th>Status</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="mapa-empresas">

                            <div id="mapa"></div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>

        </div>

    </div>

    @include('painel.fiscal.cadastro-tr.include.indicadores-tr')

    @include('painel.fiscal.cadastro-tr.include.tabs')

    @include('painel.fiscal.aprovacao.modalAvisoAtivacaoDesativacao')
    @include('painel.fiscal.aprovacao.modalAprovaReprovaVeiculo')
    @include('painel.fiscal.aprovacao.modalInconsistenciaVeiculo')
    @include('painel.fiscal.aprovacao.modalDataValidadeVeiculo')

@endsection