@if(isset($dadosCadastrais))

    <div class="col-xs-12">
        <div class="box-cadastro">

            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="cnpj">CNPJ</label>
                        <input type="text" name="cnpj" id="cnpj" class="form-control"
                               value="{{ $dadosCadastrais->cnpj or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="ramo_atividade">Ramo de Atividade</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->ramo_atividade))
                            <input type="text" name="ramo_atividade" id="ramo_atividade" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->ramo_atividade->nome }}"
                                   disabled>
                        @else
                            <input type="text" name="ramo_atividade" id="ramo_atividade" class="form-control" value=""
                                   disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="tipo_atividade">Tipo</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->tipo_ramo_atividade->nome))
                            <input type="text" name="tipo_atividade" id="tipo_atividade" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->tipo_ramo_atividade->nome }}"
                                   disabled>
                        @else
                            <input type="text" name="tipo_atividade" id="tipo_atividade" class="form-control" value=""
                                   disabled>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="razao_social">Razão Social</label>
                        <input type="text" name="razao_social" id="razao_social" class="form-control"
                               value="{{ $dadosCadastrais->razao_social or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="nome_fantasia">Nome Fantasia</label>
                        <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control"
                               value="{{ $dadosCadastrais->nome_comercial or '' }}" disabled>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="licenca_municipal">CCM</label>
                        <input type="text" name="licenca_municipal" id="licenca_municipal" class="form-control"
                               value="{{ $dadosCadastrais->im or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="inscricao_estadual">Inscrição Estadual</label>
                        <input type="text" name="inscricao_estadual" id="inscricao_estadual" class="form-control"
                               value="{{ $dadosCadastrais->ie or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="telefone">Telefone</label>
                        <input type="text" name="telefone" id="telefone" class="form-control"
                               value="{{ $dadosCadastrais->telefone or '' }}" disabled>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        @if(isset($dadosCadastrais->empresa_endereco->cep))
                            <input type="text" name="cep" id="cep" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->cep }}" disabled>
                        @else
                            <input type="text" name="cep" id="cep" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="estado">Estado</label>
                        @if(isset($dadosCadastrais->empresa_endereco->estado->sigla))
                            <input type="text" name="estado" id="estado" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->estado->sigla }}" disabled/>
                        @else
                            <input type="text" name="estado" id="estado" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="cidade">Cidade</label>
                        @if(isset($dadosCadastrais->empresa_endereco->cidade->nome))
                            <input type="text" name="cidade" id="cidade" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->cidade->nome }}" disabled>
                        @else
                            <input type="text" name="cidade" id="cidade" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="endereco">Endereço</label>
                        @if(isset($dadosCadastrais->empresa_endereco->endereco))
                            <input type="text" name="endereco" id="endereco" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->endereco }}" disabled>
                        @else
                            <input type="text" name="endereco" id="endereco" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label for="numero">Número</label>
                        @if(isset($dadosCadastrais->empresa_endereco->numero))
                            <input type="text" name="numero" id="numero" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->numero }}" disabled>
                        @else
                            <input type="text" name="numero" id="numero" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label for="complemento">Bairro</label>
                        @if(isset($dadosCadastrais->empresa_endereco->bairro))
                            <input type="text" name="bairro" id="bairro" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->bairro }}" disabled>
                        @else
                            <input type="text" name="bairro" id="bairro" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="complemento">Complemento</label>
                        @if(isset($dadosCadastrais->empresa_endereco->complemento))
                            <input type="text" name="complemento" id="complemento" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->complemento }}" disabled>
                        @else
                            <input type="text" name="complemento" id="complemento" class="form-control" value=""
                                   disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="ponto_referencia">Ponto de Referência</label>
                        @if(isset($dadosCadastrais->empresa_endereco->ponto_referencia))
                            <input type="text" name="ponto_referencia" id="ponto_referencia" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_endereco->ponto_referencia }}" disabled>
                        @else
                            <input type="text" name="ponto_referencia" id="ponto_referencia" class="form-control"
                                   value="" disabled>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

    <div class="col-xs-12">

        <h2>Responsável</h2>

        <div class="box-cadastro">

            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="responsavel">Nome Completo do Responsável</label>
                        <input type="text" name="responsavel" id="responsavel" class="form-control"
                               value="{{ $dadosCadastrais->nome_responsavel or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="email" name="email" id="email" class="form-control"
                               value="{{ $dadosCadastrais->email or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label for="cargo">Cargo</label>
                        <input type="text" name="cargo" id="cargo" class="form-control"
                               value="{{ $dadosCadastrais->cargo or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label for="telefone_responsavel">Telefone</label>
                        <input type="tel" name="telefone_responsavel" id="telefone_responsavel" class="form-control"
                               value="{{ $dadosCadastrais->telefone_responsavel or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label for="ramal">Ramal</label>
                        <input type="text" name="ramal" id="ramal" class="form-control"
                               value="{{ $dadosCadastrais->ramal or '' }}" disabled>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="form-group">
                        <label for="celular_responsavel">Celular</label>
                        <input type="text" name="celular_responsavel" id="celular_responsavel" class="form-control"
                               value="{{ $dadosCadastrais->celular or '' }}" disabled>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

    <div class="col-xs-12">

        <h2>Frequência</h2>

        <div class="box-cadastro">

            <div class="row">

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="frequencia_geracao">Geração Diária de Resíduos</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->frequencia_geracao->nome))
                            <input type="text" name="frequencia_geracao" id="frequencia_geracao" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->frequencia_geracao->nome }}"
                                   disabled>
                        @else
                            <input type="text" name="frequencia_geracao" id="frequencia_geracao" class="form-control"
                                   value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="frequencia_coleta">Frequência Coleta</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->frequencia_coleta->nome))
                            <input type="text" name="frequencia_coleta" id="frequencia_coleta" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->frequencia_coleta->nome }}"
                                   disabled>
                        @else
                            <input type="text" name="frequencia_coleta" id="frequencia_coleta" class="form-control"
                                   value="" disabled>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="diviser"></div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

    <div class="col-xs-12">

        <h2>Outras Informações</h2>

        <div class="box-cadastro">

            <div class="row">

                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="numero_colaboradores">Número de Colaboradores</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->colaboradores_numero->nome))
                            <input type="text" name="numero_colaboradores" id="numero_colaboradores"
                                   class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->colaboradores_numero->nome }}"
                                   disabled>
                        @else
                            <input type="text" name="numero_colaboradores" id="numero_colaboradores"
                                   class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="consumo_energia">Consumo Mensal de Energia</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->energia_consumo->nome))
                            <input type="text" name="consumo_energia" id="consumo_energia" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->energia_consumo->nome }}"
                                   disabled>
                        @else
                            <input type="text" name="consumo_energia" id="consumo_energia" class="form-control" value=""
                                   disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="area_total">Área Total (m²)</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->area_total))
                            <input type="text" name="area_total" id="area_total" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->area_total }}"
                                   disabled>
                        @else
                            <input type="text" name="area_total" id="area_total" class="form-control" value="" disabled>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="area_construida">Área Construída (m²)</label>
                        @if(isset($dadosCadastrais->empresa_informacao_complementar->area_construida))
                            <input type="text" name="area_construida" id="area_construida" class="form-control"
                                   value="{{ $dadosCadastrais->empresa_informacao_complementar->area_construida }}"
                                   disabled>
                        @else
                            <input type="text" name="area_construida" id="area_construida" class="form-control" value=""
                                   disabled>
                        @endif
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="diviser"></div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

    <div class="col-xs-12">

        <h2>Arquivos</h2>

        <div class="box-cadastro">

            <div class="row">

                <div class="col-xs-12 green">
                    <h5>CAPACIDADE JURÍDICA</h5>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="cartao_cnpj" class="control-label">Cartão CNPJ</label>
                        @if(isset($documentos['empresa_cartao_cnpj']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['empresa_cartao_cnpj']])}}"
                                   class="ver-doc form-control" title="Abrir Cartão CNPJ" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>

                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="contrato_social" class="control-label">Contrato Social</label>
<!--                        --><?php
//                            echo '<pre>';
//                        print_r($documentos);
//                        exit;
//                        ?>
                        @if(isset($documentos['transportador_contrato_social']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_contrato_social']])}}"
                                   class="ver-doc form-control" title="Abrir Contrato Social" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>

                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12">
                    <hr/>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12 green">
                    <h5>IDENTIDADE FINANCEIRA</h5>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="certidao_falencia" class="control-label">Certidão Falência Concordata</label>
                        @if(isset($documentos['transportador_certidao_falencia_concordata']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_certidao_falencia_concordata']])}}"
                                   class="ver-doc form-control" title="Abrir Certidão de Falência" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="balanco_financeiro" class="control-label">Balanço Financeiro</label>
                        @if(isset($documentos['transportador_balanco_financeiro']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_balanco_financeiro']])}}"
                                   class="ver-doc form-control" title="Abrir Balanço Financeiro" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="demonstrativo_contabil" class="control-label">Demonstrativo Contábil</label>
                        @if(isset($documentos['transportador_demonstrativo_contabil']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_demonstrativo_contabil']])}}"
                                   class="ver-doc form-control" title="Abrir Demonstrativo contábil" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12">
                    <hr/>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12 green">
                    <h5>REGULARIDADE FISCAL</h5>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="certidao_negativa_municipal" class="control-label">Certidão Negativa
                            Municipal</label>
                        @if(isset($documentos['transportador_certidao_negativa_municipal']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_certidao_negativa_municipal']])}}"
                                   class="ver-doc form-control" title="Abrir Certidão Negativa Municipal" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="certidao_negativa_estadual" class="control-label">Certidão Negativa Estadual</label>
                        @if(isset($documentos['transportador_certidao_negativa_estadual']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_certidao_negativa_estadual']])}}"
                                   class="ver-doc form-control" title="Abrir Certidão Negativa Estadual" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="certidao_negativa_federal" class="control-label">Certidão Negativa Federal</label>
                        @if(isset($documentos['transportador_certidao_negativa_federal']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_certidao_negativa_federal']])}}"
                                   class="ver-doc form-control" title="Abrir Certidão Negativa Estadual" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12">
                    <hr/>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12 green">
                    <h5>CAPACIDADE TÉCNICA</h5>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="art" class="control-label">ART
                            <span>(Anotação Responsabilidade Técnica)</span></label>
                        @if(isset($documentos['transportador_anotacao_responsabilidade_tecnica']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_anotacao_responsabilidade_tecnica']])}}"
                                   class="ver-doc form-control" title="Abrir Notação Responsabilidade Técnica" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="cdl" class="control-label">CDL
                            <span>(Certificado de Dispensa de Licença)</span></label>
                        @if(isset($documentos['transportador_certificado_dispensa_licenca']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_certificado_dispensa_licenca']])}}"
                                   class="ver-doc form-control" title="Abrir Certificado de Dispensa de Licença" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="licenca_previa" class="control-label">Licença Prévia</label>
                        @if(isset($documentos['transportador_licenca_previa']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_licenca_previa']])}}"
                                   class="ver-doc form-control" title="Abrir Certificado de Dispensa Prévia" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="licenca_instalacao" class="control-label">Licença Instalação</label>
                        @if(isset($documentos['licenca']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['licenca']])}}"
                                   class="ver-doc form-control" title="Abrir Certificado de Dispensa Prévia" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12">
                    <hr/>
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="licenca_operacao" class="control-label">Licença Operação</label>
                        @if(isset($documentos['transportador_licenca_operacao']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_licenca_operacao']])}}"
                                   class="ver-doc form-control" title="Abrir Licença de Operação" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="alvara_prefeitura" class="control-label">Alvará da Prefeitura</label>
                        @if(isset($documentos['transportador_alvara_prefeitura']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_alvara_prefeitura']])}}"
                                   class="ver-doc form-control" title="Abrir Alvará Prefeitura" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="ctf_ibama" class="control-label">CTF Ibama</label>
                        @if(isset($documentos['transportador_ctf_ibama']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_ctf_ibama']])}}"
                                   class="ver-doc form-control" title="Abrir CTF Ibama" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <div class="form-group">
                        <label for="avcb" class="control-label">AVCB</label>
                        @if(isset($documentos['transportador_avcb']))
                            <div class="flex">
                                <a href="{{route('download',[$dadosCadastrais->id, $documentos['transportador_avcb']])}}"
                                   class="ver-doc form-control" title="Abrir AVCB" target="_blank">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                </a>
                            </div>
                        @else
                            <p>Não há arquivo</p>
                        @endif
                    </div>
                </div>

                <div class="col-xs-12">
                    <hr/>
                </div>
            </div>
        </div>
    </div>

@else

    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div class="alert alert-warning text-center" role="alert">
            <p>As informações deste usuário estão incompletas!</p>
        </div>
    </div>

@endif
