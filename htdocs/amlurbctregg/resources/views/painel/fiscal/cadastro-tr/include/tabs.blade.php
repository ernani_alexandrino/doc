<div class="col-xs-12">
    <div class="row">

        <ul class="nav nav-tabs nav-justified nav-tabs-cadastros-fiscal" role="tablist">
            <li role="presentation" class="active">
                <a href="#dados-cadastrais" aria-controls="dados-cadastrais" role="tab" data-toggle="tab"
                   title="Dados cadastrais">
                    Dados Cadastrais
                </a>
            </li>
            <li role="presentation">
                <a href="#vinculos" aria-controls="vinculos" role="tab" data-toggle="tab" title="Vínculos">
                    Vínculos
                </a>
            </li>
            <li role="presentation">
                <a href="#inconsistencias" aria-controls="inconsistencias" role="tab" data-toggle="tab"
                   title="Inconsistências">
                    Inconsistências
                </a>
            </li>
            <li role="presentation">
                <a href="#inf-financeiras" aria-controls="inf-financeiras" role="tab" data-toggle="tab"
                   title="Informações Financeiras">
                    Informações Financeiras
                </a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="dados-cadastrais">

                @include('painel.fiscal.cadastro-tr.include.dados-cadastrais-tr')

            </div>
            <div role="tabpanel" class="tab-pane" id="vinculos">

                @include('painel.fiscal.cadastro-tr.include.vinculos')

            </div>
            <div role="tabpanel" class="tab-pane" id="inconsistencias">

                @include('painel.fiscal.cadastro-tr.include.incosistencias')

            </div>
            <div role="tabpanel" class="tab-pane" id="inf-financeiras">

                @include('painel.fiscal.cadastro-tr.include.informacoes-financeiras')

            </div>
        </div>
    </div>
</div>
