<div class="row">
    <div class="col-xs-12 col-sm-9">

        <div class="input-group busca busca-cad-gg" id="busca-cad">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i> <span
                            class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#" data-search="id-limpurb" title="Nr. Amlurb">Nr. Amlurb</a></li>
                    <li><a href="#" data-search="nome-comercial" title="Empresa">Empresa</a></li>
                    <li><a href="#" data-search="razao-social" title="Razão Social">Razão Social</a></li>
                    <li><a href="#" data-search="cnpj" title="Cnpj">Cnpj</a></li>
                    <li><a href="#" data-search="bairro" title="Bairro">Bairro</a></li>
                </ul>
            </div>
            <input type="text" class="form-control input-search" placeholder="Escolha o termo e aperte Enter"
                   name="search" id="input-search" aria-label="Escolha o termo e aperte Enter">
            <button class="btn btn-primary" id="buscar">Buscar</button>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3">

        <ul class="nav nav-tabs nav-justified nav-search" role="tablist">
            <li role="presentation" class="active">
                <a href="#lista-empresas" aria-controls="lista-empresas" role="tab" data-toggle="tab"><i
                            class="fa fa-list" aria-hidden="true"></i></a>
            </li>
            <li role="presentation">
                <a href="#mapa-empresas" aria-controls="mapa-empresas" role="tab" data-toggle="tab"
                   class="mapa-empresas"><i class="fa fa-map" aria-hidden="true"></i></a>
            </li>
        </ul>

    </div>
</div>
