<div class="col-xs-12">

    <h2>Geradores Vinculados</h2>

    <div class="box-rastreabilidade">

        <div class="row">

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover table-cadastro-vinculos-gg table-padrao" id="cadastro_vinculos_geradores_tr">
                        <thead>
                        <tr>
                            <th>Nr. Amlurb</th>
                            <th>Empresa</th>
                            <th>CNPJ</th>
                            <th>Data Vencimento</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12">

    <h2>Destinos Finais Vinculados</h2>

    <div class="box-rastreabilidade">

        <div class="row">

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover table-cadastro-vinculos-df table-padrao" id="cadastro_vinculos_destinos_tr">
                        <thead>
                        <tr>
                            <th>Nr. Amlurb</th>
                            <th>Empresa</th>
                            <th>CNPJ</th>
                            <th>Data Vencimento</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12">

    <h2>Veículos Vinculados</h2>

    <div class="box-rastreabilidade">

        <div class="row">

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover table-cadastro-vinculos-vl table-padrao" id="cadastro_vinculos_veiculos_tr">
                        <thead>
                        <tr>
                            <th>Placa</th>
                            <th>Tipo</th>
                            <th>Marca</th>
                            <th>Ano</th>
                            <th>Renavam</th>
                            <th>IPVA</th>
                            <th>Capacidade</th>
                            <th>Tara</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12">

    <h2>Equipamentos Vinculados</h2>

    <div class="box-rastreabilidade">

        <div class="row">

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-hover table-cadastro-vinculos-eq table-padrao" id="cadastro_vinculos_equipamentos_tr">
                        <thead>
                        <tr>
                            <th>Nr. Amlurb</th>
                            <th>Equipamento</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
