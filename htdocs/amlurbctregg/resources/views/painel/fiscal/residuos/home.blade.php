@extends('layouts.home-painel-fiscal')

@section('conteudo')

    <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

    <div class="cadastro-residuo">

        <div class="col-xs-12">

            <article class="clearfix">
                <table class="table table-hover table-residuos table-padrao" id="residuos-cadastrados">
                    <thead>
                    <tr>
                        <th width="35%">Nome</th>
                        <th width="15%">Código</th>
                        <th width="16%" class="no-sort">Ação</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($residuos as $residuo)
                        <tr>
                            <td>{{$residuo->nome}}</td>
                            <td>{{$residuo->codigo}}</td>
                            <td class="controls">
                                <a href="#!" title="Editar resíduo" onclick="editarResiduos({{ $residuo->id }})">
                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                </a>
                                <a href="{{route('residuo_destroy_fs',$residuo->id)}}" class="deletar"
                                   onclick="return confirm('Tem certeza que deseja deletar esse resíduo?');"
                                   title="Deletar resíduo">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </article>
        </div>

        <div class="col-xs-12 text-center">
            <button type="button" class="btn custom_send_fiscal" onclick="cadastrarResiduo();">
                CADASTRAR NOVO RESÍDUO
            </button>
        </div>

    </div>

    @include('painel.fiscal.residuos.modais.cadastroResiduo')

    <div id="modalEditResiduos" class="modal fade " role="dialog"></div>

@endsection