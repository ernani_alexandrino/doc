<div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" title="Fechar">&times;</button>
            <h4 class="modal-title green text-center">EDITAR RESÍDUO</h4>
        </div>
        <div class="modal-body">
            {{Form::open(array('route' => array('residuo_update_fs', $residuo->id), 'files' => true))}}
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="editarResiduoNome" class="control-label">Nome</label>
                        <input id="editarResiduoNome" name="nome" value="{{$residuo->nome}}" class="form-control"
                               type="text" required/>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="editarResiduoCodigo" class="control-label">CÓDIGO</label>
                        <input id="editarResiduoCodigo" readonly name="codigo" value="{{$residuo->codigo}}" class="form-control"
                               required/>
                    </div>
                </div>

                <div class="col-xs-12 text-center">
                    <button type="submit" class="btn custom_send_fiscal" id="editar_usuario">SALVAR</button>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
