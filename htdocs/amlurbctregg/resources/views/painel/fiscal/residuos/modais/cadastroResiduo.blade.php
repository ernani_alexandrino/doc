<!-- Modal -->
<div id="modalCadastroResiduo" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" title="Fechar">&times;</button>
                <h4 class="modal-title green text-center">CADASTRAR RESÍDUO</h4>
            </div>

            <div class="modal-body">

                {{Form::open(array('route' => array('residuo_store_fs'),'files' => true))}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cadResiduoNome" class="control-label">Nome</label>
                            <input id="cadResiduoNome" name="nome" class="form-control" type="text" required/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cadResiduoCodigo" class="control-label">CÓDIGO</label>
                            <input id="cadResiduoCodigo" readonly name="codigo" class="form-control" value="{{$proximo_codigo}}" required/>
                        </div>
                    </div>

                    <div class="col-xs-12 text-center">
                        <button type="submit" class="btn custom_send_fiscal">SALVAR</button>
                    </div>

                    <div class="clearfix"></div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
