@extends('layouts.home-painel-fiscal') @section('conteudo')

<h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

@if (!isset($dadosCadastrais->id_matriz) || is_null($dadosCadastrais->id_matriz))
<div class="col-xs-12">

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h2>Relação de Grandes Geradores</h2>
        </div>
        <div class="col-xs-12 col-sm-6">
            @include('painel.fiscal.cadastro-gg.include.search')
        </div>
    </div>

    <div class="box-rastreabilidade">

        <div class="row">
            <div class="col-xs-12">

                <div class="tab-content">

                    
                    <div role="tabpanel" class="tab-pane active" id="lista-empresas">
                        <div class="table-responsive">
                            <table class="table table-hover fiscal-cadastros" id="cadastro_grandes_geradores">
                                <thead>
                                    <tr>
                                        <th>Nr. Amlurb</th>
                                        <th>Empresa</th>
                                        <th>Razão Social</th>
                                        <th>CNPJ</th>
                                        <th>Bairro</th>
                                        <th>Status</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane " id="mapa-empresas">
                        <div id="mapa">

                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="diviser"></div>
        </div>

    </div>

</div>
@endif

@include('painel.fiscal.cadastro-gg.include.indicadores-gg') 
@include('painel.fiscal.cadastro-gg.include.tabs') 
@include('painel.fiscal.aprovacao.modalAvisoAtivacaoDesativacao') 
@endsection