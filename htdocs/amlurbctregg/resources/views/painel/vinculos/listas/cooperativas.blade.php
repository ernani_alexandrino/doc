<div class="col-xs-12">

    <h2>Relação de Cooperativas Vinculadas</h2>

    <div class="box-configuracoes">

        <div class="row">

            <div class="col-xs-8 btn-top-table">
                <div class="modal-bts-center">
                    <button id="btn_add_cooperativa" name="btn_add_cooperativa" class="btn btn-default modal-bts-center__externo btn-gerador">
                        Vincular Cooperativa
                    </button>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-cooperativa table-padrao" id="table-cooperativas-vinculados">
                        <thead>
                        <tr>
                            <th>NR. AMLURB</th>
                            <th>EMPRESA</th>
                            <th>RESÍDUOS</th>
                            @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.transportador'))
                                <th>EQUIP.</th>
                                <th>FREQUÊNCIA</th>
                            @endif
                            <th>STATUS AMLURB</th>
                            <th class="camp-actions-table">AÇÕES</th>
                        </tr>
                        </thead>
                        <tbody id="cooperativa-list" name="cooperativa-list" class="vinculos-list">
                            <!-- popula via Javascript (Ajax/Datatable) -- atualizaListaVinculosTransportador() -->
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

</div>