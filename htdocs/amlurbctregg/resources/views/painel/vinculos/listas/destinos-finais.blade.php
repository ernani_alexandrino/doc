<div class="col-xs-12">

    <h2>
        Relação de Destinos Finais Vinculados
    </h2>

    <div class="box-configuracoes">

        <div class="row">
            @if(\Illuminate\Support\Facades\Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == \config('enums.empresas_tipo.transportador'))
                <div class="col-xs-8 btn-top-table">
                    <div class="modal-bts-center">
                        <button id="btn_add_destino" name="btn_add_destino" class="btn btn-default modal-bts-center__externo btn-destino">
                            Vincular Destino Final
                        </button>
                    </div>
                </div>
            @else
                <div class="col-xs-8 btn-top-table">
                    <div class="modal-bts-center">
                        <button id="btn_add_destino" name="btn_add_destino" class="btn btn-default modal-bts-center__externo btn-destino">
                            Vincular Destino Final
                        </button>
                    </div>
                </div>
            @endif
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-destinos" id="table-destinos-vinculados">
                        <thead>
                            <tr>
                                <th>NR. AMLURB</th>
                                <th width="35%">EMPRESA</th>
                                <th>RESÍDUOS</th>
                                <th>VOLUME ANUAL</th>
                                <th>STATUS AMLURB</th>
                                <th class="camp-actions-table">AÇÕES</th>
                            </tr>
                        </thead>
                        <tbody id="destino-list" name="destino-list" class="vinculos-list">
                            <!-- popula via Javascript (Ajax/Datatables) -- atualizaListaVinculosDestino() -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
