<div class="col-xs-12">

    <h2>Relação com Condomínio Misto</h2>

    <div class="box-configuracoes">

        <div class="row">

            @if(is_null($vinculo_condominio))
                <div class="modal-bts-center">
                    <button id="btn_add_condominio" name="btn_add_condominio" class="btn btn-default modal-bts-center__externo btn-gerador">
                        Vincular Condomínio
                    </button>
                </div>
            @else
                <div class="modal-bts-center">
                    <button type="button" class="btn btn-default modal-bts-center__externo btn-gerador" data-toggle="modal" data-target="#modalAlertaVinculoCondominioMisto">
                        Vincular Condomínio
                    </button>
                </div>
            @endif

            <div class="col-xs-12">
                <table class="table table-responsive table-padrao" id="table-condominions-vinculados">
                    <thead>
                        <tr>
                            <th>Condomínio</th>
                            <th>CNPJ</th>
                            <th>Responsável</th>
                            <th>Email</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody id="vinculo-condominio" class="vinculos-list">
                        @if(is_null($vinculo_condominio))
                            <tr>
                                <td colspan="5">
                                    Esta empresa não está associada a nenhum Condomínio Misto
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>{{$vinculo_condominio->razao_social}}</td>
                                <td>{{$vinculo_condominio->cnpj}}</td>
                                <td>{{$vinculo_condominio->empresa_vinculada->empresa_responsavel->nome}}</td>
                                <td>{{$vinculo_condominio->empresa_vinculada->empresa_responsavel->email}}</td>
                                <td>
                                    <button class='btn btn-delete delete-vinculo'
                                        value='{{$vinculo_condominio->empresa_vinculada->id}}' title='Remover Vínculo'>
                                        <img src='{{asset('images/remover.png')}}' alt='Remover vínculo'>
                                    </button>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    @include('painel.vinculos.modais.alerta-vinculo')

</div>
