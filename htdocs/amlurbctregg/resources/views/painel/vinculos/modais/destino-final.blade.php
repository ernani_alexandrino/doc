<div class="modal fade" id="modalNewVinculoDestino" tabindex="-1" role="dialog"
     aria-labelledby="modalNewVinculoDestinoLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="modalNewVinculoDestinoLabel">Vincular Novo Destino Final</h4>
            </div>

            <div class="modal-body">
                <div class="row modal-body__instrucoes">
                    <div class="col-xs-12">                    
                        Cadastre aqui a empresa de destinação de resíduos à qual você vai emitir o CTR-E
                    </div>
                </div>

                <form id="frmVincularDestino" name="frmCtre" class="form-horizontal" novalidate="">

                    {{ csrf_field() }}

                    <div class="row" id="warnings-destino">
                        <div class="col-xs-12">
                            <p class="text-danger" role="alert"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6" id="cnpj-destino">
                            <label for="cnpj_vincular_destino">Informe o CNPJ</label>
                            <input type="text" name="cnpj_vincular" id="cnpj_vincular_destino"
                                   class="form-control checkCNPJ cnpj" placeholder="00.000.000/0000-00">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row ajax-panel"><!-- loading gif --></div>

                    <div class="row">
                        <div class="col-sm-6" id="razaosocial-destino">
                            <label for="razao_social_destino">Razão Social</label>
                            <input type="text" name="razao_social" id="razao_social_destino" class="form-control">
                            <p class="text-danger"></p>
                        </div>

                        <div class="col-sm-6" id="nomefantasia-destino">
                            <label for="nome_fantasia_destino">Nome Fantasia</label>
                            <input type="text" name="nome_fantasia" id="nome_fantasia_destino" class="form-control">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4" id="responsavel-destino">
                            <label for="responsavel_destino">Nome do Contato</label>
                            <input type="text" name="responsavel" id="responsavel_destino" class="form-control">
                            <p class="text-danger"></p>
                        </div>

                        <div class="col-sm-5" id="email-destino">
                            <label for="email_destino">E-mail</label>
                            <input type="email" name="email" id="email_destino" class="form-control">
                            <p class="text-danger"></p>
                        </div>

                        <div class="col-sm-3">
                            <label for="telefone_destino">Telefone</label>
                            <input type="tel" name="telefone_responsavel" id="telefone_destino" class="form-control"
                                   placeholder="(00) 0000-0000">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12 col-md-6">
                            <div id="residuos-destino">
                                <label for="residuos_vinculo">Resíduos Transportados</label>
                                <div class="form-item-linha">
                                    <select name="residuos_vinculo" id="residuos_vinculo" class="form-control residuos_vinculo form-item-linha__item">
                                        <option value="">Selecione</option>
                                        @foreach($residuos as $residuo )
                                            <option value="{{ $residuo->id }}">
                                                {{ $residuo->nome }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <button id="incluir_residuos_vinculo" class="btn btn-default btn-incluir-residuo residuo form-item-linha__bt">Incluir</button>
                                    <label for="incluir_residuos_vinculo" class="hidden-element">Incluir</label>
                                </div>
                                <p class="text-danger"></p>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <label for="volume_residuos">Volume Anual de Resíduos Transportados (ton)</label>
                            <input type="text" name="volume_residuos" id="volume_residuos" class="form-control">
                            <p class="text-danger"></p>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12 col-md-6 caixa-incluidos">
                            <label>Lista de resíduos</label>
                            <div id="residuos-escolhidos" class="residuos-escolhidos"></div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="modal-bts-center">
                                <button type="button" class="btn btn-default btn-success btn-save-destino"
                                        id="btn-save-new-destino">Salvar
                                </button>
                                <button type="reset" class="btn btn-default btn-danger" id="btn-reset-new-destino">
                                    Cancelar
                                </button>
                            </div>
                        </div>

                    </div>

                </form>

            </div>
        </div>
    </div>
</div>