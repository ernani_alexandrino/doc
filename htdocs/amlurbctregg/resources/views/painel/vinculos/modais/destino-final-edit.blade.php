<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Informações do Destino Final Vinculado</h4>
        </div>

        <div class="modal-body">
            <div class="row modal-body__instrucoes">
                <div class="col-xs-12">
                    Cadastre aqui a empresa de destinação de resíduos à qual você presta serviços para emitir o CTR-E
                </div>
            </div>

            <form id="frmEditVincularDestino" name="frmCtre" class="form-horizontal">

                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-6" id="cnpj-destino">
                        <label for="cnpj_vincular_destino">CNPJ</label>
                        <input type="text" name="cnpj_vincular" id="cnpj_vincular_destino"
                               class="form-control checkCNPJ cnpj" placeholder="00.000.000/0000-00"
                               readonly value="{{$vinculo->cnpj}}">
                    </div>

                    <div class="col-sm-4" id="amlurb-destino">
                        <label for="amlurb_vincular_destino">NR. AMLURB</label>
                        <input type="text" name="amlurb" id="amlurb_vincular_destino"
                               class="form-control" readonly value="{{$vinculo->id_limpurb}}">
                    </div>

                    <div class="col-sm-4" id="status-destino">
                        <label for="status_vincular_destino">Status Amlurb</label>
                        <input type="text" name="status" id="status_vincular_destino"
                               class="form-control" readonly value="{{$vinculo->status->descricao}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" id="razaosocial-destino">
                        <label for="razao_social_destino">Razão Social</label>
                        <input type="text" name="razao_social" id="razao_social_destino" class="form-control"
                               readonly value="{{$vinculo->razao_social}}">
                    </div>

                    <div class="col-sm-6" id="nomefantasia-destino">
                        <label for="nome_fantasia_destino">Nome Fantasia</label>
                        <input type="text" name="nome_fantasia" id="nome_fantasia_destino" class="form-control"
                               readonly value="{{$vinculo->nome_comercial}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4" id="responsavel-destino">
                        <label for="responsavel_destino">Nome do Contato</label>
                        <input type="text" name="responsavel" id="responsavel_destino" class="form-control"
                               value="{{$vinculo->empresa_vinculada->empresa_responsavel->nome}}">
                        <p class="text-danger"></p>
                    </div>

                    <div class="col-sm-5" id="email-destino">
                        <label for="email_destino">E-mail</label>
                        <input type="email" name="email" id="email_destino" class="form-control"
                               value="{{$vinculo->empresa_vinculada->empresa_responsavel->email}}">
                        <p class="text-danger"></p>
                    </div>

                    <div class="col-sm-3">
                        <label for="telefone_destino">Telefone</label>
                        <input type="tel" name="telefone_responsavel" id="telefone_destino" class="form-control"
                               placeholder="(00) 0000-0000"
                               value="{{$vinculo->empresa_vinculada->empresa_responsavel->telefone}}">
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="" id="residuos-destino">
                                    <label for="residuos_vinculo">Resíduos Transportados</label>
                                    <div class="form-item-linha">
                                        <select name="residuos_vinculo" id="residuos_vinculo" class="form-control residuos_vinculo form-item-linha__item">
                                            <option value="">Selecione</option>
                                            @foreach($residuos as $residuo )
                                                <option value="{{ $residuo->id }}">
                                                    {{ $residuo->nome }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <button id="incluir_residuos_vinculo" class="btn btn-default btn-incluir-residuo residuo form-item-linha__bt">Incluir</button>
                                        <label for="incluir_residuos_vinculo" class="hidden-element">Incluir</label>
                                    </div>
                                    <p class="text-danger"></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <label for="volume_residuos">Volume Anual de Resíduos Transportados (ton)</label>
                        <input type="text" name="volume_residuos" id="volume_residuos" class="form-control"
                               value="{{str_replace('.', ',', $vinculo->empresa_vinculada->volume_residuo)}}">
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-6 caixa-incluidos">
                        <div id="residuos-escolhidos" class="residuos-escolhidos">
                            @foreach($vinculo->logistica_residuos as $lr)
                                <input name="residuo_escolhido[]" id="residuo_{{$lr->residuo_id}}"
                                       value="{{$lr->residuo_id}}" type="hidden">
                            @endforeach
                            @foreach($vinculo->logistica_residuos as $lr)
                                <div>
                                    <div class="col-xs-11">
                                        <div class="nome-residuo" data-id-residuo="{{$lr->residuo_id}}">
                                            {{$lr->residuo->nome}}
                                        </div>
                                    </div>
                                    <div class="col-xs-1">
                                        <div id="excluir-residuo-vinculo" class="excluir-residuo-vinculo excluir-ico"
                                             data-id-residuo="{{$lr->residuo_id}}">X
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success btn-save-destino"
                                    id="btn-save-edit-destino">Salvar
                            </button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-reset-edit-destino">
                                Cancelar
                            </button>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>
</div>