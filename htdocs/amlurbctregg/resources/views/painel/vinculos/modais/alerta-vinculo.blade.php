<div class="modal fade" id="modalAlertaVinculoCondominioMisto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Atenção</h4>
            </div>

            <div class="modal-body text-center">
                <p>Esta empresa só pode estar vinculada a um condomínio.</p>
                <p>Caso ocorra mudança de endereço, você deve remover o vínculo atual para adicionar o novo vínculo.</p>
                <div class="modal-bts-center">
                    <button type="button" class="btn btn-default btn-success" data-dismiss="modal" aria-label="Close">Ok</button>
                </div>
            </div>
        </div>

    </div>
</div>