<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Informações da Cooperativa Vinculada</h4>
        </div>

        <div class="modal-body">

            <form id="frmEditVincularCooperativa" name="frmCtre" class="form-horizontal" novalidate="">

                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-4" id="edit-cnpj-cooperativa">
                        <label for="cnpj_vincular_cooperativa">CNPJ</label>
                        <input type="text" name="cnpj_vincular" id="cnpj_vincular_cooperativa"
                               class="form-control checkCNPJ cnpj" placeholder="00.000.000/0000-00"
                                readonly value="{{$vinculo->cnpj}}">
                    </div>
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4" id="amlurb-cooperativa">
                        <label for="amlurb_vincular_cooperativa">NR. AMLURB</label>
                        <input type="text" name="amlurb" id="amlurb_vincular_cooperativa"
                               class="form-control" readonly value="{{$vinculo->id_limpurb}}">
                    </div>

                    <div class="col-sm-4" id="status-cooperativa">
                        <label for="status_vincular_cooperativa">Status Amlurb</label>
                        <input type="text" name="status" id="status_vincular_cooperativa"
                               class="form-control" readonly value="{{$vinculo->status->descricao}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" id="razaosocial-cooperativa">
                        <label for="razao_social_cooperativa">Razão Social</label>
                        <input type="text" name="razao_social" id="razao_social_cooperativa" class="form-control"
                            readonly value="{{$vinculo->razao_social}}">
                    </div>

                    <div class="col-sm-6" id="nomefantasia-cooperativa">
                        <label for="nome_fantasia_cooperativa">Nome Fantasia</label>
                        <input type="text" name="nome_fantasia" id="nome_fantasia_cooperativa" class="form-control"
                            readonly value="{{$vinculo->nome_comercial}}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6" id="responsavel-cooperativa">
                        <label for="responsavel_cooperativa">Nome do Contato</label>
                        <input type="text" name="responsavel" id="responsavel_cooperativa" class="form-control"
                            value="{{$vinculo->empresa_vinculada->empresa_responsavel->nome}}">
                        <p class="text-danger"></p>
                    </div>

                    <div class="col-sm-6" id="email-cooperativa">
                        <label for="email_cooperativa">E-mail de Contato</label>
                        <input type="email" name="email" id="email_cooperativa" class="form-control"
                               value="{{$vinculo->empresa_vinculada->empresa_responsavel->email}}">
                        <p class="text-danger"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label for="telefone_cooperativa">Telefone</label>
                        <input type="tel" name="telefone" id="telefone_cooperativa" class="form-control"
                               placeholder="(00) 0000-0000" value="{{$vinculo->empresa_vinculada->empresa_responsavel->telefone}}">
                    </div>

                    @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.transportador')
                           && Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.destino_final_reciclado'))
                        <div class="col-sm-6">
                            <label for="coleta_diaria" class="required">Frequência de Coleta</label>
                            <select name="coleta_diaria" id="coleta_diaria" class="form-control destino">
                                <option value="">Selecione</option>
                                @foreach($frequenciaColeta as $dados)
                                    @if(isset($vinculo->empresa_vinculada->frequencia_coleta->id) && ($dados->id == $vinculo->empresa_vinculada->frequencia_coleta->id))
                                        <option value="{{ $dados->id }}" selected>{{ $dados->nome }}</option>
                                    @else
                                        <option value="{{ $dados->id }}">{{ $dados->nome }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <p class="text-danger"></p>
                        </div>
                    @endif

                    @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == config('enums.empresas_tipo.destino_final_reciclado'))
                        <div class="col-sm-6">
                            <label for="volume_residuos">Volume Anual de Resíduos Transportados (ton)</label>
                            <input type="text" name="volume_residuos" id="volume_residuos" class="form-control" value="{{$vinculo->empresa_vinculada->volume_residuo}}">
                            <p class="text-danger"></p>
                        </div>
                    @endif
                </div>

                <div class="row">

                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="" id="residuos-transportador">
                                    <label for="residuos_vinculo">
                                        @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == config('enums.empresas_tipo.transportador'))
                                            Resíduos Repassados
                                        @elseif(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == config('enums.empresas_tipo.destino_final_reciclado'))
                                            Resíduos Recebidos
                                        @else
                                            Resíduos Coletados
                                        @endif
                                    </label>
                                    <div class="form-item-linha">
                                        <select name="residuos_vinculo" id="residuos_vinculo" class="form-control residuos_vinculo form-item-linha__item">
                                            <option value="">Selecione</option>
                                            @foreach($residuos as $residuo )
                                                <option value="{{ $residuo->id }}">
                                                    {{ $residuo->nome }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <button id="incluir_residuos_vinculo" class="btn btn-default btn-incluir-residuo residuo form-item-linha__bt">Incluir</button>
                                        <label for="incluir_residuos_vinculo" class="hidden-element">Incluir</label>
                                    </div>
                                    <p class="text-danger"></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.transportador')
                           && Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.destino_final_reciclado'))
                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="" id="equipamentos-cooperativa">
                                        <label for="equipamentos_vinculo">Equipamentos Vinculados</label>
                                        <div class="form-item-linha">
                                            <select name="equipamentos_vinculo" id="equipamentos_vinculo" class="form-control equipamentos_vinculo form-item-linha__item">
                                                <option value="">Selecione</option>
                                                @foreach($equipamentos as $equipamento )
                                                    <option value="{{ $equipamento->id }}">
                                                        {{
                                                        $equipamento->codigo_amlurb . ' - ' .
                                                        $equipamento->equipamento->tipo_equipamento->nome . ' - ' .
                                                        $equipamento->equipamento->capacidade
                                                         }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <button id="incluir_equipamentos_vinculo" type="button" class="btn btn-default btn-incluir-equipamento equipamento form-item-linha__bt">Incluir</button>
                                            <label for="incluir_equipamentos_vinculo" class="hidden-element">Incluir</label>
                                        </div>
                                        <p class="text-danger"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-6 caixa-incluidos">
                        <div id="residuos-escolhidos" class="residuos-escolhidos">
                            @foreach($vinculo->logistica_residuos as $lr)
                                <input name="residuo_escolhido[]" id="residuo_{{$lr->residuo_id}}" value="{{$lr->residuo_id}}" type="hidden">
                            @endforeach
                            @foreach($vinculo->logistica_residuos as $lr)
                                <div>
                                    <div class="col-xs-11">
                                        <div class="nome-residuo" data-id-residuo="{{$lr->residuo_id}}">
                                            {{$lr->residuo->nome}}
                                        </div>
                                    </div>
                                    <div class="col-xs-1">
                                        <div id="excluir-residuo-vinculo" class="excluir-residuo-vinculo excluir-ico" data-id-residuo="{{$lr->residuo_id}}">X</div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.transportador')
                           && Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.destino_final_reciclado'))
                        <div class="col-xs-12 col-sm-6 caixa-incluidos">
                            <div id="equipamentos-escolhidos" class="equipamentos-escolhidos">
                                @foreach($equipamentos_alocados as $equipamento)
                                    <input name="equipamento_escolhido[]" id="equipamento_{{$equipamento->id}}"
                                           value="{{$equipamento->id}}" type="hidden">
                                @endforeach
                                @foreach($equipamentos_alocados as $equipamento)
                                    <div>
                                        <div class='col-xs-11'>
                                            <div class='nome-equipamento' data-id-equipamento="{{$equipamento->id}}">
                                                {{
                                                    $equipamento->codigo_amlurb . ' - ' . $equipamento->equipamento->tipo_equipamento->nome
                                                }}
                                            </div>
                                        </div>
                                        <div class='col-xs-1'>
                                            <div id='excluir-equipamento-vinculo' class='excluir-equipamento-vinculo excluir-ico' data-id-equipamento="{{$equipamento->id}}">X</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif

                </div>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            <button type="button" class="btn btn-default btn-success btn-save-cooperativa" id="btn-save-edit-cooperativa">Salvar</button>
                            <button type="reset" class="btn btn-default btn-danger" id="btn-reset-edit-cooperativa">Cancelar</button>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>
</div>