<div class="modal fade" id="modalNewVinculoCondominio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Vincular Condomínio</h4>
            </div>

            <div class="modal-body">
                <div class="row modal-body__instrucoes">
                    <div class="col-xs-12">
                        Cadastre aqui o Condomínio responsável por recolher seus resíduos
                    </div>
                </div>

                <form id="frmVincularCondominio" name="frmCtre" class="form-horizontal" novalidate="">

                    {{ csrf_field() }}

                    <div class="row" id="warnings-transportador">
                        <p class="text-warning"></p>
                    </div>

                    <div class="row">
                        <div class="col-sm-4" id="cnpj-condominio">
                            <label for="cnpj_vincular_transportador">Informe o CNPJ</label>
                            <input type="text" name="cnpj_vincular" id="cnpj_vincular_condominio"
                                   class="form-control checkCNPJ cnpj" placeholder="00.000.000/0000-00">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row ajax-panel"><!-- loading gif --></div>

                    <div class="row">
                        <div class="col-sm-6" id="razaosocial-condominio">
                            <label for="razao_social_condominio">Razão Social</label>
                            <input type="text" name="razao_social" id="razao_social_condominio" class="form-control">
                            <p class="text-danger"></p>
                        </div>

                        <div class="col-sm-6" id="nomefantasia-condominio">
                            <label for="nome_fantasia_condominio">Nome Fantasia</label>
                            <input type="text" name="nome_fantasia" id="nome_fantasia_condominio" class="form-control">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6" id="responsavel-condominio">
                            <label for="responsavel_condominio">Nome do Contato</label>
                            <input type="text" name="responsavel" id="responsavel_condominio" class="form-control">
                            <p class="text-danger"></p>
                        </div>

                        <div class="col-sm-6" id="email-condominio">
                            <label for="email_condominio">E-mail de Contato</label>
                            <input type="email" name="email" id="email_condominio" class="form-control">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="modal-bts-center">
                                <button type="button" class="btn btn-default btn-success btn-save-condominio" id="btn-save-new-condominio">Salvar</button>
                                <button type="reset" class="btn btn-default btn-danger" id="btn-reset-new-condominio">Cancelar</button>
                            </div>
                        </div>

                    </div>

                </form>

            </div>
        </div>
    </div>
</div>