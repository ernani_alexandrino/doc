<div class="modal fade" id="modalNewVinculoCooperativa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
                <h4 class="modal-title" id="myModalLabel">Vincular Nova Cooperativa</h4>
            </div>

            <div class="modal-body">
                <div class="row modal-body__instrucoes">
                    <div class="col-xs-12">
                        @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == config('enums.empresas_tipo.transportador'))
                            Cadastre aqui transportadores contratados pela sua empresa, exclusivamente, para a destinação de resíduos da sua unidade até o destino.
                        @elseif(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == config('enums.empresas_tipo.destino_final_reciclado'))
                            Cadastre aqui a empresa transportadora que traz resíduos à sua empresa
                        @else
                            Cadastre aqui a empresa transportadora de resíduos que presta serviços à sua empresa para emitir o CTR-E
                        @endif
                    </div>
                </div>

                <form id="frmVincularCooperativa" name="frmCtre" class="form-horizontal" novalidate="">

                    {{ csrf_field() }}

                    <div class="row" id="warnings-transportador">
                        <p class="text-warning"></p>
                    </div>

                    <div class="row">
                        <div class="col-sm-4" id="cnpj-cooperativa">
                            <label for="cnpj_vincular_cooperativa">Informe o CNPJ</label>
                            <input type="text" name="cnpj_vincular" id="cnpj_vincular_cooperativa"
                                   class="form-control checkCNPJ cnpj" placeholder="00.000.000/0000-00">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row ajax-panel"><!-- loading gif --></div>

                    <div class="row">
                        <div class="col-sm-6" id="razaosocial-cooperativa">
                            <label for="razao_social_cooperativa">Razão Social</label>
                            <input type="text" name="razao_social" id="razao_social_cooperativa" class="form-control">
                            <p class="text-danger"></p>
                        </div>

                        <div class="col-sm-6" id="nomefantasia-cooperativa">
                            <label for="nome_fantasia_cooperativa">Nome Fantasia</label>
                            <input type="text" name="nome_fantasia" id="nome_fantasia_cooperativa" class="form-control">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6" id="responsavel-cooperativa">
                            <label for="responsavel_cooperativa">Nome do Contato</label>
                            <input type="text" name="responsavel" id="responsavel_cooperativa" class="form-control">
                            <p class="text-danger"></p>
                        </div>

                        <div class="col-sm-6" id="email-cooperativa">
                            <label for="email_cooperativa">E-mail de Contato</label>
                            <input type="email" name="email" id="email_cooperativa" class="form-control">
                            <p class="text-danger"></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label for="telefone_cooperativa">Telefone</label>
                            <input type="tel" name="telefone" id="telefone_cooperativa" class="form-control"
                                   placeholder="(00) 0000-0000">
                            <p class="text-danger"></p>
                        </div>

                        @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.transportador')
                           && Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.destino_final_reciclado'))
                            <div class="col-sm-6">
                                <label for="coleta_diaria" class="required">Frequência de Coleta</label>
                                <select name="coleta_diaria" id="coleta_diaria" class="form-control destino">
                                    <option value="">Selecione</option>
                                    @foreach($frequenciaColeta as $dados)
                                        <option value="{{ $dados->id }}">{{ $dados->nome }}</option>
                                    @endforeach
                                </select>
                                <p class="text-danger"></p>
                            </div>
                        @endif

                        @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == config('enums.empresas_tipo.destino_final_reciclado'))
                            <div class="col-sm-6">
                                <label for="volume_residuos">Volume Anual de Resíduos Transportados (ton)</label>
                                <input type="text" name="volume_residuos" id="volume_residuos" class="form-control">
                                <p class="text-danger"></p>
                            </div>
                        @endif
                    </div>

                    <div class="row">

                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="" id="residuos-transportador">
                                        <label for="residuos_vinculo">
                                            @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == config('enums.empresas_tipo.transportador'))
                                                Resíduos Repassados
                                            @elseif(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == config('enums.empresas_tipo.destino_final_reciclado'))
                                                Resíduos Recebidos
                                            @else
                                                Resíduos Coletados
                                            @endif
                                        </label>
                                        <div class="form-item-linha">
                                            <select name="residuos_vinculo" id="residuos_vinculo" class="form-control residuos_vinculo form-item-linha__item">
                                                <option value="">Selecione</option>
                                                @foreach($residuos as $residuo )
                                                    <option value="{{ $residuo->id }}">
                                                        {{ $residuo->nome }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <button id="incluir_residuos_vinculo" class="btn btn-default btn-incluir-residuo residuo form-item-linha__bt">Incluir</button>
                                            <label for="incluir_residuos_vinculo" class="hidden-element">Incluir</label>
                                        </div>
                                        <p class="text-danger"></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.transportador')
                           && Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.destino_final_reciclado'))
                            <div class="col-xs-6">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="" id="equipamentos-transportador">
                                            <label for="equipamentos_vinculo">Equipamentos Vinculados</label>
                                            <div class="form-item-linha">
                                                <select name="equipamentos_vinculo" id="equipamentos_vinculo" class="form-control equipamentos_vinculo form-item-linha__item">
                                                    <option value="">Selecione</option>
                                                    @foreach($equipamentos as $equipamento )
                                                        <option value="{{ $equipamento->id }}">
                                                            {{
                                                            $equipamento->codigo_amlurb . ' - ' .
                                                            $equipamento->equipamento->tipo_equipamento->nome . ' - ' .
                                                            $equipamento->equipamento->capacidade
                                                             }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <button id="incluir_equipamentos_vinculo" type="button" class="btn btn-default btn-incluir-equipamento equipamento form-item-linha__bt">Incluir</button>
                                                <label for="incluir_equipamentos_vinculo" class="hidden-element">Incluir</label>
                                            </div>
                                            <p class="text-danger"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 caixa-incluidos">
                            <div id="residuos-escolhidos" class="residuos-escolhidos">

                            </div>
                        </div>

                        @if(Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.transportador')
                           && Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id != config('enums.empresas_tipo.destino_final_reciclado'))
                            <div class="col-xs-12 col-sm-6 caixa-incluidos">
                                <div id="equipamentos-escolhidos" class="equipamentos-escolhidos">

                                </div>
                            </div>
                        @endif

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="modal-bts-center">
                                <button type="button" class="btn btn-default btn-success btn-save-cooperativa" id="btn-save-new-cooperativa">Salvar</button>
                                <button type="reset" class="btn btn-default btn-danger" id="btn-reset-new-cooperativa">Cancelar</button>
                            </div>
                        </div>

                    </div>

                </form>

            </div>
        </div>
    </div>
</div>