<div class="col-xs-12">

    <h2>1.Relação de Boletos Gerados</h2>

    <div class="box-configuracoes">

        <div class="row">

            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-boletos" id="table-boletos-vinculados">
                        <thead>
                            <tr>
                                <th>EMISSÃO</th>
                                <th>NR. BOLETO</th>
                                <th>TIPO</th>
                                <th>CODIGO ITEM</th>
                                <th>VENCIMENTO</th>
                                <th>STATUS</th>
                                <th class="camp-actions-table">AÇÕES</th>
                            </tr>
                        </thead>
                        <tbody id="boletos-list" name="boletos-list">
                            <!-- AJAX list boletos -->
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

</div>