@extends('painel.pagamentos.modais.visualizarBoleto')

@section('boleto-pago-list')

<thead>
    <th width="20%">NR. AMLURB</th>
    <th>Equipamento</th>
</thead>
<tbody id="#" name="">
    <tr>
        <td>{{$equipamento->codigo_amlurb}}</td>
        <td>{{$equipamento->equipamento->tipo_equipamento->nome . ' - ' . $equipamento->equipamento->capacidade}}</td>
    </tr>
</tbody>

@stop
