@extends('painel.pagamentos.modais.visualizarBoleto')

@section('boleto-pago-list')

<thead>
    <th width="20%">NR. AMLURB</th>
    <th>Veículo</th>
</thead>
<tbody id="#" name="">
    <tr>
        <td>{{$veiculo->codigo_amlurb}}</td>
        <td>{{$veiculo->placa . ' - ' . $veiculo->marca}}</td>
    </tr>
</tbody>

@stop