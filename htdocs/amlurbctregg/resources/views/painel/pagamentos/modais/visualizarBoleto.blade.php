<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Informações do Boleto</h4>
        </div>

        <div class="modal-body">

            <div class="row">

                <div class="col-sm-3">
                    <label for="nr_boleto">Nr. Boleto</label>
                    <input type="text" name="nr_boleto" id="nr_boleto"
                           class="form-control" readonly value="{{$boleto->codTipoServGuia}}">
                </div>

                <div class="col-sm-3">
                    <label for="data_emissao">Data Emissão</label>
                    <input type="text" name="data_emissao" id="data_emissao"
                           class="form-control" readonly value="{{$boleto->created_at}}">
                </div>

                <div class="col-sm-3">
                    <label for="data_vencimento">Data de Vencimento</label>
                    <input type="text" name="data_vencimento" id="data_vencimento"
                           class="form-control" readonly value="{{$boleto->dataDeVencimento}}">
                </div>

                <div class="col-sm-3">
                    <label for="boleto_status">Status</label>
                    <input type="text" name="boleto_status" id="boleto_status"
                           class="form-control" readonly value="{{$boleto->status->descricao}}">
                </div>

            </div>

            <div class="row">
                <div class="col-xs-12">
                    <p>&nbsp;</p>
                    <label for="">Equipamentos/Veículos Relacionados</label>

                    <div class="containerInfoBoletos">

                        <form id="table-info-boleto" name="" class="form-horizontal" novalidate="">

                            <table class="table " id="table-boleto-pago">

                                @yield('boleto-pago-list')

                            </table>

                        </form>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-xs-12">
                    <div class="modal-bts-center">
                        <button type="button" class="btn btn-default btn-success " id="btn-close-visualizar-boleto">Fechar</button>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>