<div class="col-xs-12 col-sm-2">
    <div class="row">
        <div class="menu-lateral">

            <img class="logo-ctre-menu" src="{{ asset('images/novo-logo-letra-em-branco.svg') }}"
                 alt="CTRE - Controle de Transporte de Resíduos - Eletrônico">

            <div class="user" style="display:none;">
                @if(empty(Auth::user()->user_perfils->imagem_perfil))
                    <div class="img-user">

                    </div>
                @else
                    <div class="img-user img-circle" style="background-image: url({{ asset('uploads/imagens_perfil/'.Auth::user()->user_perfils->imagem_perfil) }});">

                    </div>
                @endif
                <div class="inf-user">
                    <p>{{ auth()->user()->name }}</p>
                    <span></span>
                </div>
            </div>

            <ul class="nav nav-pills nav-stacked">
            @if(Auth::user()->empresa->status_id == \config('enums.status.ativo') || Auth::user()->empresa->status_id ==  \config('enums.status.vinculos_pendentes') )
                    <li><a href="{{ route('painel_ss') }}" id="m_painel_gg"><i class="painel-controle"></i>Painel de Controle</a></li>
                    <li><a href="{{ route('eq_ss') }}" id="ci_tr"><i class="cadastro-internos"></i>Meus Equipamentos</a></li>
                    <li><a href="{{ route('cf_ss') }}" id="m_cadastro_vinculo_gg"><i class="cadastro-externos"></i>Clientes/Fornecedores</a></li>
                @endif
                <li><a href="{{ route('mc_ss') }}" id="m_config_gg"><i class="configuracoes"></i>Meu Cadastro</a></li>
                <li>
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="logout"></i>Logout
                    </a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>

        </div>
    </div>

</div>
