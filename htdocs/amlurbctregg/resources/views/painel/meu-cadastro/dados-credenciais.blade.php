<div class="dados-empresa">

    <h2>
        @if(\Illuminate\Support\Facades\Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == \config('enums.empresas_tipo.transportador'))
            3. Responsável
        @elseif(\Illuminate\Support\Facades\Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == \config('enums.empresas_tipo.destino_final_reciclado'))
            2. Responsável
        @else
            4. Responsável
        @endif
    </h2>

    <p>
        Nos campos abaixo você pode alterar os dados sobre o responsável de sua empresa, assim como mudar a senha de acesso.
        Lembrando que se for alterado o endereço de email, também será alterado o login de acesso cadastrado inicialmente.
    </p>

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="update-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-6" id="update-nome">
            <div class="form-group">
                <label for="nome-responsavel" class="required">Nome Completo do Responsável</label>
                <input type="text" name="nome-responsavel" id="nome-responsavel" class="form-control"
                    value="{{ \Illuminate\Support\Facades\Auth::user()->name }}">
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-6" id="update-email">
            <div class="form-group">
                <label for="email-responsavel" class="required">Email</label>
                <input type="text" name="email-responsavel" id="email-responsavel" class="form-control"
                    value="{{ \Illuminate\Support\Facades\Auth::user()->email }}">
                <p class="text-danger"></p>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-3">
            <div class="form-group">
                <label for="cargo">Cargo</label>
                <input type="text" name="cargo" id="cargo" class="form-control"
                    value="{{ $empresa->cargo }}">
                <p></p>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="form-group">
                <label for="celular">Celular</label>
                <input type="text" name="celular" id="celular" class="form-control"
                    value="{{ $empresa->celular }}">
                <p></p>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="form-group">
                <label for="telefone_responsavel">Telefone</label>
                <input type="text" name="telefone_responsavel" id="telefone_responsavel" class="form-control"
                    value="{{ $empresa->telefone_responsavel }}">
                <p></p>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="form-group">
                <label for="ramal">Ramal</label>
                <input type="text" name="ramal" id="ramal" class="form-control"
                    value="{{ $empresa->ramal }}">
                <p></p>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-6" id="update-password">
            <div class="form-group">
                <label for="password1">Cadastrar Nova Senha</label>
                <input type="password" name="password1" id="password1" class="form-control">
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="form-group">
                <label for="password2">Confirmar Nova Senha</label>
                <input type="password" name="password2" id="password2" class="form-control">
            </div>
        </div>

    </div>

    <div class="row">
        <div class="modal-bts-center">
            <button id="btn_edit_usuario" name="btn_edit_usuario" class="btn btn-default btn-warning modal-bts-center__externo" type="button">
                Salvar Alterações do Responsável
            </button>
        </div>
    </div>

</div>