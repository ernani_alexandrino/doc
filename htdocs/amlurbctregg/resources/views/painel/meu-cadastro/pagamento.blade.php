<div class="dados-pagamento dados-empresa">

    <h2>Pagamento</h2>

    <div class="row">

        <div class="col-xs-6">
            <p>O boleto será enviado para o email cadastrado.</p>
            <div class="text-center">
                <a href="{{ route('boleto.2via', ['id' => $empresa->id]) }}" 
                   target="_blank"
                   class="btn btn-success" onclick="javascript: setTimeout(function () {
                        window.location.reload();
                    }, 4000);" >
                    Gerar 2ª Via do Boleto
                </a>
            </div>

            <br><br><br>
        </div>
    </div>

</div>
