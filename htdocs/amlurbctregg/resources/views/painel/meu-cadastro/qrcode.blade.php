<div class="dados-qrcode dados-empresa">

    <h2>Dados QR Code</h2>

    <div class="row">
        <div class="col-xs-6">
            <div class="text-center">
                <a href="{{ (empty($empresa->empresa_qrcodes->qrcode_id)) ? route('gerar_qrcode', ['id' => $empresa->id]) : route('qrcode_pdf', ['id' => $empresa->empresa_qrcodes->qrcode_id]) }}" 
                   target="_blank" 
                   class="btn btn-success">
                    Visualizar QR Code da Minha Empresa
                </a>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="text-center">
                <a href="{{ route('guia_qrcode') }}" 
                   target="_blank"
                   class="btn btn-success">
                    Visualizar Guia QR Code
                </a>
            </div>
        </div>
    </div>

</div>