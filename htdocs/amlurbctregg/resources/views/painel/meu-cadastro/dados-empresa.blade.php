<div class="dados-empresa " id="formCadastroEmpresa">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="tipoEmpresa" id="tipoEmpresa" value="{{ $idTipoEmpresa or '' }}">
    <input type="hidden" name="statusEmpresa" id="statusEmpresa" value="{{ $empresa->status_id or '' }}">
    <h2>1. Dados da Empresa</h2>

    <p>
        Aqui você confere se os dados da sua empresa cadastrados em nosso sistema estão corretos.
         Ao alterar o cadastro, a CTR-e enviará à Amlurb uma solicitação de atualização. Caso aprovada,
          a alteração será incorporada no sistema.
    </p>

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="update-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 container-nr-amlurb">

            <div class="text-center">
                <label for="nr-amlurb-anterior">NR. AMLURB ANTERIOR</label>
                <input type="text" name="nr-amlurb-anterior" id="nr-amlurb-anterior" class="form-control  text-center" readonly
                       value="{{ (!is_null($empresa->id_limpurb_base_amlurb)) ? $empresa->id_limpurb_base_amlurb : '-' }}">
            </div>
            <!--div class="col-xs-2"></div-->
            <div class="text-center">
                <label for="nr-amlurb-atual">NR. AMLURB ATUAL</label>
                <input type="text" name="nr-amlurb-atual" id="nr-amlurb-atual" class="form-control  text-center" readonly value="{{ $empresa->id_limpurb }}">
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-4" id="update-ramo_atividade">
            <div class="form-group">
                <input type="hidden" id="id_ramo_atividade" value="{{ $empresa->empresa_informacao_complementar->ramo_atividade->id }}">
                <label for="ramo_atividade">Ramo de Atividade</label>
                {{ Form::select('ramo_atividade', $ramoAtividade, $empresa->empresa_informacao_complementar->ramo_atividade->id, ['class' => 'form-control', 'id' => 'ramo_atividade']) }}
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-4" id="update-tipo_ramo_atividade">
            <div class="form-group">
                <input type="hidden" id="id_tipo_ramo_atividade" value="{{ $empresa->empresa_informacao_complementar->tipo_ramo_atividade->id }}">
                <label for="tipo_ramo_atividade">Tipo</label>
                {{ Form::select('tipo_ramo_atividade', [], null, ['class' => 'form-control', 'id' => 'tipo_ramo_atividade']) }}
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-4">
            <label for="cnpj">CNPJ</label>
            <input type="text" name="cnpj" id="cnpj" class="form-control" readonly value="{{ $empresa->cnpj }}">
            <p></p>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-6">
            <label for="razao_social">Razão Social</label>
            <input type="text" name="razao_social" id="razao_social" class="form-control" value="{{ $empresa->razao_social }}" {{ (empty($empresa->id_matriz)) ? '' : 'readonly' }} >
            <p></p>
        </div>

        <div class="col-xs-6">
            <label for="nome_fantasia">Nome Fantasia</label>
            <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control" value="{{ $empresa->nome_comercial }}" {{ (empty($empresa->id_matriz)) ? '' : 'readonly' }} >
            <p></p>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-5" id="update-endereco">
            <div class="form-group">
                <label for="endereco" class="required">Endereço</label>
                <input type="text" name="endereco" id="endereco" class="form-control" value="{{ (isset($empresa->empresa_endereco->endereco)) ? $empresa->empresa_endereco->endereco : '' }}" maxlength="255">
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-2" id="update-numero">
            <div class="form-group">
                <label for="numero" class="required">Número</label>
                <input type="text" name="numero" id="numero" class="form-control" value="{{ (isset($empresa->empresa_endereco->numero)) ? $empresa->empresa_endereco->numero : '' }}" maxlength="50">
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-3" id="update-bairro">
            <div class="form-group">
                <label for="bairro" class="required">Bairro</label>
                <input type="text" name="bairro" id="bairro" class="form-control" value="{{ (isset($empresa->empresa_endereco->bairro)) ? $empresa->empresa_endereco->bairro : '' }}" maxlength="100">
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-2" id="update-cep">
            <div class="form-group">
                <label for="cep" class="required">CEP</label>
                <input type="text" name="cep" id="cep" class="form-control" value="{{ (isset($empresa->empresa_endereco->cep)) ? $empresa->empresa_endereco->cep : '' }}" placeholder="00000-000" maxlength="10">
                <p class="text-danger"></p>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-2" id="update-estado">
            <div class="form-group">
                <label for="endereco" class="required">Estado</label>
                {{ Form::select('estado', $estados, (isset($empresa->empresa_endereco->estado->id)) ? $empresa->empresa_endereco->estado->id : '', ['class' => 'form-control', 'id' => 'estado']) }}
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-3" id="update-cidade">
            <div class="form-group">
                <label for="cidade" class="required">Cidade</label>
                {{ Form::select('cidade', $cidades, (isset($empresa->empresa_endereco->cidade->id)) ? $empresa->empresa_endereco->cidade->id : '', ['class' => 'form-control', 'id' => 'cidade']) }}
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="form-group">
                <label for="complemento">Complemento</label>
                <input type="text" name="complemento" id="complemento" class="form-control" value="{{ (isset($empresa->empresa_endereco->complemento)) ? $empresa->empresa_endereco->complemento : '' }}" maxlength="100">
                <p></p>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="form-group">
                <label for="ponto_referencia">Ponto de Referência</label>
                <input type="text" name="ponto_referencia" id="ponto_referencia" class="form-control" value="{{ (isset($empresa->empresa_endereco->ponto_referencia)) ? $empresa->empresa_endereco->ponto_referencia : '' }}" maxlength="255">
                <p></p>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-3" id="update-im">
            <div class="form-group">
                <label for="im" class="required">CCM</label>
                <input type="text" name="im" id="im" class="form-control" value="{{ $empresa->im }}" maxlength="15">
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-5">
            <div class="form-group">
                <label for="ie">Inscrição Estadual</label>
                <input type="text" name="ie" id="ie" class="form-control" value="{{ $empresa->ie }}" maxlength="15">
                <p></p>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="form-group">
                <label for="telefone_empresa">Telefone</label>
                <input type="tel" name="telefone_empresa" id="telefone_empresa" class="form-control" value="{{ $empresa->telefone }}" placeholder="(00) 0000-0000" maxlength="50">
                <p></p>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4" id="update-empresa_cartao_cnpj">

            <div class="">
                <label for="empresa_cartao_cnpj" class="required">Cartão CNPJ</label>
            </div>
            @if(!empty($documentos['empresa_cartao_cnpj']))
            <div class="col-xs-2">
                <div class="form-group">
                    <a href="{{ route('file', ['arquivo' => $documentos['empresa_cartao_cnpj'], 'empresa' => $empresa->id]) }}" target="blank" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa ver-doc" id="documento_cnpj" style="background-color: #009053; border-color:  #009053">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                </div>
            </div>
            @endif
            @if(empty($empresa->id_matriz))
            <div class="col-xs-{{ (empty($documentos['empresa_cartao_cnpj'])) ? '12' : '10' }}">

                <div class="form-group {{ ($errors->has('empresa_cartao_cnpj')) ? 'error':'' }}">
                    <label for="empresa_cartao_cnpj" class="btn-upload">
                        <input type="hidden" name="empresa_cartao_cnpj_enviado" id="empresa_cartao_cnpj_enviado"/>
                        <input type="file" name="empresa_cartao_cnpj" id="empresa_cartao_cnpj"
                               class="form-control file" value="{{ old('empresa_cartao_cnpj') }}"/>
                        <div class="progress">
                            <p>Escolha um arquivo {{ (empty($documentos['empresa_cartao_cnpj'])) ? '' : 'para alterar' }}</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>

                    @if($errors->has('empresa_cartao_cnpj'))
                        <p class="text-danger">{{ $errors->first('empresa_cartao_cnpj') }}</p>
                    @endif
                    <p class="text-danger"></p>
                </div>

            </div>
            @endif
        </div>




        <div class="col-xs-12 col-sm-4" id="update-empresa_num_iptu">

            <div class="form-group {{ ($errors->has('empresa_num_iptu')) ? 'error':'' }}">
                <label for="empresa_num_iptu" class="required">Número do IPTU</label>
                
                <input type="text" name="empresa_num_iptu"
                       id="empresa_num_iptu"
                       class="form-control"
                       value="{{ (empty($documentos['empresa_num_iptu'])) ? '' : $documentos['empresa_num_iptu'] }}"/>

                @if($errors->has('empresa_num_iptu'))
                    <p class="text-danger">{{ $errors->first('empresa_num_iptu') }}</p>
                @endif
                <p class="text-danger"></p>
            </div>

        </div>
        <div class="col-xs-12 col-sm-4"  id="update-empresa_iptu">

            <div class="">
                <label for="empresa_iptu" class="required">Cópia do IPTU</label>
            </div>
            @if(!empty($documentos['empresa_iptu']))
            <div class="col-xs-2">
                <div class="form-group">
                    <a href="{{ route('file', ['arquivo' => $documentos['empresa_iptu'], 'empresa' => $empresa->id]) }}" target="blank" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa ver-doc"  id="documento_iptu"style="background-color: #009053; border-color:  #009053">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-xs-{{ (empty($documentos['empresa_iptu'])) ? '12' : '10' }}">

                <div class="form-group {{ ($errors->has('empresa_iptu')) ? 'error':'' }}">
                    <label for="empresa_iptu" class="btn-upload">
                        <input type="hidden" name="empresa_iptu_enviado" id="empresa_iptu_enviado"/>
                        <input type="file" name="empresa_iptu" id="empresa_iptu"
                               class="form-control file" value="{{ old('empresa_iptu') }}"/>
                        <div class="progress">
                            <p>Escolha um arquivo {{ (empty($documentos['empresa_iptu'])) ? '' : 'para alterar' }}</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>

                    @if($errors->has('empresa_iptu'))
                        <p class="text-danger">{{ $errors->first('empresa_iptu') }}</p>
                    @endif
                    <p class="text-danger"></p>
                </div>

            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .ver-doc {
        background-color: #008f52 !important;
        color: #fff !important;
        width: 45px;
        font-size: 18px !important;
        float: left;
        margin: 0px 10px 0 0;
    }
</style>

