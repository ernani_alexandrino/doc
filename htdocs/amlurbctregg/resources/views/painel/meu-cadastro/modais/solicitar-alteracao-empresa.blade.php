<div class="modal fade" id="modalSolicitacaoAlteracaoEmpresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Alteração de Dados da Empresa</h4>
            </div>
            <div class="modal-body text-center">
                <p>Deseja alterar os dados da empresa?</p>
                <p>Favor informar abaixo o motivo desta alteração.</p>
            </div>

            <div class="row text-center" id="warnings-solicitacao-alteracao-empresa">
                <p class="text-warning"></p>
            </div>

            <div class="row text-center">
                <div class="col-xs-12 form-group">
                    <label for="justificativa-solicitacao-alteracao-empresa" class="required">Justificativa</label>
                    <textarea id="justificativa-solicitacao-alteracao-empresa"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="modal-bts-center">
                        <button type="button" class="btn btn-default btn-success" id="btn-confirm-solicitacao-alteracao-empresa">Confirmar</button>
                        <button type="reset" class="btn btn-default btn-danger" id="btn-cancel-solicitacao-alteracao-empresa">Cancelar</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>