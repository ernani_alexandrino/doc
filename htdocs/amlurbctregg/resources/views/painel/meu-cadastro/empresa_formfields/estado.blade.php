<div class="form-group">
    <label for="estados">Estado</label>
    {!! Form::select('estados', $estado, old('estados'), ['placeholder' => 'Selecione', 'class' => 'form-control estados', 'id' => 'estado']) !!}
    @if($errors->has('estados'))
        <p class="text-danger">{{ $errors->first('estados') }}</p>
    @endif
</div>