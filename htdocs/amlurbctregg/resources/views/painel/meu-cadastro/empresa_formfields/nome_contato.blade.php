<div class="form-group">
    <label for="responsavel">Nome do Contato</label>
    <input type="text" name="responsavel" id="responsavel" class="form-control"
           value="{{ old('responsavel') }}">
    @if($errors->has('responsavel'))
        <p class="text-danger">{{ $errors->first('responsavel') }}</p>
    @endif
</div>