<div class="form-group">
    <label for="numero">Número</label>
    <input type="number" name="numero" id="numero" class="form-control" value="{{ old('numero') }}">
    @if($errors->has('numero'))
        <p class="text-danger">{{ $errors->first('numero') }}</p>
    @endif
</div>