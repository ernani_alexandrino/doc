<div class="form-group">
    <label for="telefone_responsavel">Telefone</label>
    <input type="tel" name="telefone_responsavel" id="telefone_responsavel" class="form-control"
           placeholder="(00) 0000-0000" value="{{ old('telefone_responsavel') }}">
    @if($errors->has('telefone_responsavel'))
        <p class="text-danger">{{ $errors->first('telefone_responsavel') }}</p>
    @endif
</div>