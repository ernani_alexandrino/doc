<div class="form-group">
    <label for="razao_social">Razão Social</label>
    <input type="text" name="razao_social" id="razao_social" class="form-control" value="{{ old('razao_social') }}">
    @if($errors->has('razao_social'))
        <p class="text-danger">{{ $errors->first('razao_social') }}</p>
    @endif
</div>