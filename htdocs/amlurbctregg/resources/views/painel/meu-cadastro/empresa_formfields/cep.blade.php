<div class="form-group">
    <label for="cep">CEP</label>
    <input type="text" name="cep" id="cep" class="form-control" placeholder="00000-000"
           value="{{ old('cep') }}">
    @if($errors->has('cep'))
        <p class="text-danger">{{ $errors->first('cep') }}</p>
    @endif
</div>