<div class="form-group">
    <label for="bairro">Bairro</label>
    <input type="text" name="bairro" id="bairro" class="form-control" value="{{ old('bairro') }}">
    @if($errors->has('bairro'))
        <p class="text-danger">{{ $errors->first('bairro') }}</p>
    @endif
</div>