<div class="form-group">
    <label for="cidade">Cidade</label>
    {!! Form::select('cidade', [], old('cidade'), ['placeholder' => 'Selecione uma cidade', 'class' => 'form-control cidades', 'id' => 'cidade']) !!}
    @if($errors->has('cidade'))
        <p class="text-danger">{{ $errors->first('cidade') }}</p>
    @endif
</div>