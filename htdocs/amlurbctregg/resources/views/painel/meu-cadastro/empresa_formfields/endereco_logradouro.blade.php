<div class="form-group">
    <label for="endereco">Endereço</label>
    <input type="text" name="endereco" id="endereco" class="form-control" value="{{ old('endereco') }}">
    @if($errors->has('endereco'))
        <p class="text-danger">{{ $errors->first('endereco') }}</p>
    @endif
</div>