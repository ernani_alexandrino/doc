<div class="row">
    <div class="col-sm-10">
        <div class="form-group">
            <label for="cnpj_vincular">Informe o CNPJ</label>
            <input type="text" name="cnpj_vincular" id="cnpj_vincular"
                   class="form-control checkCNPJ cnpj" value="{{ old('cnpj_vincular') }}"
                   placeholder="00.000.000/0000-00">
            @if($errors->has('cnpj_vincular'))
                <p class="text-danger">{{ $errors->first('cnpj_vincular') }}</p>
            @endif
        </div>
    </div>
    <div class="col-sm-2">
        <div id="ajax-panel"></div>
    </div>
</div>