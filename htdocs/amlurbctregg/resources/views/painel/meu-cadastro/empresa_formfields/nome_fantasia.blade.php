<div class="form-group">
    <label for="nome_fantasia">Nome Fantasia</label>
    <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control" value="{{ old('nome_fantasia') }}">
    @if($errors->has('nome_fantasia'))
        <p class="text-danger">{{ $errors->first('nome_fantasia') }}</p>
    @endif
</div>