<div class="form-group">
    <label for="ramal" class="control-label">Ramal </label>

    <?php
        $ramal = '';
        if (isset(Auth::user()->user_perfils->ramal)) {
            $ramal = Auth::user()->user_perfils->ramal;
        } else {
            $empresa = \Amlurb\Models\Empresa::whereId(Auth::user()->empresa_id)->first();
            $ramal = $empresa->ramal;
        }
    ?>

    <input type="text" id="ramal" class="form-control" name="ramal" value="{{$ramal}}"/>

    @if($errors->has('ramal'))
        <p class="text-danger">{{ $errors->first('ramal') }}</p>
    @endif
</div>