<div class="form-group">
    <label for="data_nascimento" class="control-label">Data de Nascimento</label>
    <input type="text" id="data_nascimento" class="form-control datepicker" name="data_nascimento"
           value="{{isset(Auth::user()->user_perfils->data_nascimento) ? Auth::user()->user_perfils->data_nascimento : ''}}">

    @if($errors->has('data_nascimento'))
        <p class="text-danger">{{ $errors->first('data_nascimento') }}</p>
    @endif
</div>