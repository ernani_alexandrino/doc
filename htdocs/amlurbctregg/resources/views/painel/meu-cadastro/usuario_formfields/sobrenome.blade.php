<div class="form-group">
    <label for="sobrenome" class="control-label">Sobrenome</label>
    <input type="text" name="sobrenome" id="sobrenome" class="form-control"
           value="{{ Auth::user()->sobrenome }}"/>

    @if($errors->has('sobrenome'))
        <p class="text-danger">{{ $errors->first('sobrenome') }}</p>
    @endif
</div>