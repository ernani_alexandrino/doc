<div class="form-group">
    <label for="senha" class="control-label">Nova Senha </label>
    <input id="password" class="form-control " name="password" value="" type="password" minlength="8"/>

    @if($errors->has('senha'))
        <p class="text-danger">{{ $errors->first('senha') }}</p>
    @endif
</div>