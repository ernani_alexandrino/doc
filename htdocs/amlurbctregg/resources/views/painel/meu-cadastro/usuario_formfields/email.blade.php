<div class="form-group">
    <label for="email" class="control-label">E-mail</label>
    <input type="email" class="form-control" name="email" id="email"
           value="{{Auth::user()->email}}">

    @if($errors->has('email'))
        <p class="text-danger">{{ $errors->first('email') }}</p>
    @endif
</div>