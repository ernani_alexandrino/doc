<div class="form-group">
    <label for="celular" class="control-label">Celular </label>

    <?php
        $celular = '';
        if (isset(Auth::user()->user_perfils->celular)) {
            $celular = Auth::user()->user_perfils->celular;
        } else {
            $empresa = \Amlurb\Models\Empresa::whereId(Auth::user()->empresa_id)->first();
            $celular = $empresa->celular;
        }
    ?>

    <input type="tel" id="celular" class="form-control" name="celular" value="{{$celular}}">

    @if($errors->has('celular'))
        <p class="text-danger">{{ $errors->first('celular') }}</p>
    @endif
</div>