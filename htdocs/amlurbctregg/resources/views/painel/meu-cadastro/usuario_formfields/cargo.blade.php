<div class="form-group">
    <label for="cargo" class="control-label">Cargo</label>

    <?php
        $cargo = '';
        if (isset(Auth::user()->user_perfils->cargo)) {
            $cargo = Auth::user()->user_perfils->cargo;
        } else {
            $empresa = \Amlurb\Models\Empresa::whereId(Auth::user()->empresa_id)->first();
            $cargo = $empresa->cargo;
        }
    ?>

    <input type="text" id="cargo" class="form-control" name="cargo" value="{{$cargo}}">

    @if($errors->has('cargo'))
        <p class="text-danger">{{ $errors->first('cargo') }}</p>
    @endif
</div>