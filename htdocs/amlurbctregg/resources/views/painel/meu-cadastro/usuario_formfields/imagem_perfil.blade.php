<div class="col-xs-12">
    <p class="green">FOTO DO PERFIL</p>
</div>

<div class="col-xs-4 col-md-3">
    <div class="form-group">
        @if(isset(Auth::user()->user_perfils->imagem_perfil) && !empty(Auth::user()->user_perfils->imagem_perfil))
            <img src="{{ asset('uploads/imagens_perfil/'.Auth::user()->user_perfils->imagem_perfil) }}"
                 class="img-responsive" alt="{{ Auth::user()->name }}">
        @else
            <img src="{{url('images/user.png')}}" alt="{{ Auth::user()->name }}"/>
        @endif
    </div>
</div>
<div class="col-xs-8 col-md-4">
    <div class="form-group">
        <label for="files" class="btn input custom">Clique para selecionar o arquivo de envio</label>
        <input id="files" name="imagem_perfil" style="visibility:hidden;" type="file">
    </div>
</div>

<div class="col-xs-4 col-md-8">
    <input type="submit" class="btn custom_send_gerador" value="ENVIAR">
</div>

@if($errors->has('imagem_perfil'))
    <div class="col-xs-12">
        <p class="text-danger">{{ $errors->first('imagem_perfil') }}</p>
    </div>
@endif