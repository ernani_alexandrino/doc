<div class="form-group">
    <label for="telefone">Telefone </label>

    <?php
        $telefone = '';
        if (isset(Auth::user()->user_perfils->telefone)) {
            $telefone = Auth::user()->user_perfils->telefone;
        } else {
            $empresa = \Amlurb\Models\Empresa::whereId(Auth::user()->empresa_id)->first();
            $telefone = $empresa->telefone;
        }
    ?>

    <input type="tel" id="telefone" class="form-control" name="telefone" value="{{$telefone}}">

    @if($errors->has('telefone'))
        <p class="text-danger">{{ $errors->first('telefone') }}</p>
    @endif
</div>