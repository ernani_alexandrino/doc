<div class="form-group">
    <label for="nome" class="control-label">Nome</label>
    <input type="text" name="nome" id="nome" class="form-control" value="{{Auth::user()->name}}"/>

    @if($errors->has('nome'))
        <p class="text-danger">{{ $errors->first('nome') }}</p>
    @endif
</div>