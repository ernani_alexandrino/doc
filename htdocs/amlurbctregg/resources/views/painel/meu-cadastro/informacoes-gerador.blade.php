<div class="informacoesEmpresa">

    <!-- GRANDE GERADOR, PEQUENO GERADOR, CONDOMINIO MISTO, ORGAO PUBLICO, SERVIÇO DE SAÚDE -->

    <h2>2. Frequência de Resíduos</h2>

    <p>
            Nesta tela você deve informar o volume diário de geração de resíduos do seu
            empreendimento e a frequência em que eles são coletados.
    </p>

    <div class="row">

        <div class="col-xs-3" id="update-frequencia_geracao">
            <div class="form-group">
                <label for="frequencia_geracao" class="required">Geração Diária de Resíduos</label>
            @if(isset($empresa->empresa_informacao_complementar->frequencia_geracao->id))
                {{ Form::select('frequencia_geracao', $frequencia_geracao, $empresa->empresa_informacao_complementar->frequencia_geracao->id, ['class' => 'form-control', 'id' => 'frequencia_geracao']) }}
                @else
                    {{ Form::select('frequencia_geracao', $frequencia_geracao, '', ['class' => 'form-control', 'id' => 'frequencia_geracao']) }}
                @endif
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-3"  >
            <div class="form-group">
                <label for="frequencia_coleta" class="required">Frequência de Coleta</label>
                @if(isset($empresa->empresa_informacao_complementar->frequencia_geracao->id))
                    {{ Form::select('frequencia_coleta', $frequencia_coleta, (empty($empresa->empresa_informacao_complementar->frequencia_coleta->id)) ? '' : $empresa->empresa_informacao_complementar->frequencia_coleta->id, ['class' => 'form-control', 'id' => 'frequencia_coleta']) }}
                @else
                    {{ Form::select('frequencia_coleta', $frequencia_coleta, '', ['class' => 'form-control', 'id' => 'frequencia_coleta']) }}
                @endif

                <p class="text-danger"></p>
            </div>
        </div>

    </div>

    <h2>3. Informações Complementares</h2>
    <p>Nesta tela você deve informar o volume diário de geração de resíduos do seu
            empreendimento e a frequência em que eles são coletados.</p>

    <div class="row">

        <div class="col-xs-3" id="update-colaboradores_numero">
            <div class="form-group">
                <label for="colaboradores_numero" class="required">Número de Colaboradores</label>

                @if(isset($empresa->empresa_informacao_complementar->frequencia_geracao->id))
                    {{ Form::select('colaboradores_numero', $colaboradores_numero, (empty($empresa->empresa_informacao_complementar->colaboradores_numero->id)) ? '' : $empresa->empresa_informacao_complementar->colaboradores_numero->id, ['class' => 'form-control', 'id' => 'colaboradores_numero']) }}
                @else
                    {{ Form::select('colaboradores_numero', $colaboradores_numero, '', ['class' => 'form-control', 'id' => 'colaboradores_numero']) }}
                @endif
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-3" id="update-energia_consumo">
            <div class="form-group">
                <label for="energia_consumo" class="required">Consumo Mensal de Energia</label>
                @if(isset($empresa->empresa_informacao_complementar->frequencia_geracao->id))
                    {{ Form::select('energia_consumo', $energia_consumo, (empty($empresa->empresa_informacao_complementar->energia_consumo->id)) ? '' : $empresa->empresa_informacao_complementar->energia_consumo->id, ['class' => 'form-control', 'id' => 'energia_consumo']) }}
                @else
                    {{ Form::select('energia_consumo', $energia_consumo, '', ['class' => 'form-control', 'id' => 'energia_consumo']) }}
                @endif

                <p></p>
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-3" id="update-estabelecimento_tipo">
            <div class="form-group">
                <label for="estabelecimento_tipo" class="required">Local do Empreendimento</label>
                @if(isset($empresa->empresa_informacao_complementar->frequencia_geracao->id))
                    {{ Form::select('estabelecimento_tipo', $estabelecimento_tipo, (empty($empresa->empresa_informacao_complementar->estabelecimento_tipo->id)) ? '' : $empresa->empresa_informacao_complementar->estabelecimento_tipo->id, ['class' => 'form-control', 'id' => 'estabelecimento_tipo']) }}
                @else

                    {{ Form::select('estabelecimento_tipo', $estabelecimento_tipo, '', ['class' => 'form-control', 'id' => 'estabelecimento_tipo']) }}
                @endif
            
                <p class="text-danger"></p>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-3"  id="update-area_total">
            <div class="form-group">
                <label for="area_total" class="required">Área Total (m²)</label>
                <input type="text" name="area_total" id="area_total" class="form-control" value="{{ $empresa->empresa_informacao_complementar->area_total }}">
                <p class="text-danger"></p>
            </div>
        </div>

        <div class="col-xs-3" id="update-area_construida">
            <div class="form-group">
                <label for="area_construida" class="required">Área Construída (m²)</label>
                <input type="text" name="area_construida" id="area_construida" class="form-control" value="{{ $empresa->empresa_informacao_complementar->area_construida }}">
                <p class="text-danger"></p>
            </div>
        </div>

    </div>
</div>