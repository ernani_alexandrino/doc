{{--Cadastro quinta etapa condomínio--}}
<div class="informacoesEmpresa">

    <div class="row">
        <div class="col-xs-12">
            <h2 class="margin-top-40">Dados do Síndico</h2>
        </div>
    </div>

    <div class="row">
        <div id="ajax-panel"></div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group  {{ ($errors->has('sindico')) ? 'error':'' }}">
            <input type="hidden" name="id_sindico_alterado" value="{{ $sindico->id or ''}}" id="id_socio_1" class="form-control">
                <label for="condominio_sindico" class="required">Nome do Sindico Responsável</label>
                <input type="text" value="{{ $sindico->nome or ''}}" name="condominio_sindico" id="condominio_sindico" class="form-control">
            </div>

            @if($errors->has('sindico'))
                <p class="text-danger">{{ $errors->first('sindico') }}</p>
            @endif
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="form-group {{ ($errors->has('condominio_sindico_rg')) ? 'error':'' }}">
                <label for="rg" class="required">RG do Síndico </label>
                <input type="text" value="{{ $sindico->rg or ''}}"  name="condominio_sindico_rg_text" id="condominio_sindico_rg_text"
                       class="form-control required rg" style="margin-bottom: 10px;">

                <div>
                    @if(!empty($documentos['condominio_sindico_rg']))
                    <div class="col-xs-2">
                        <div class="form-group">
                            <a href="{{ route('file', ['arquivo' => $documentos['condominio_sindico_rg'], 'empresa' => $empresa->id]) }}" target="blank" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa ver-doc" style="background-color: #009053; border-color:  #009053">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        </div>
                    </div>
                    @endif
                    <div class="col-xs-{{ (empty($documentos['condominio_sindico_rg'])) ? '12' : '10' }}">

                        <div class="form-group {{ ($errors->has('condominio_sindico_rg')) ? 'error':'' }}">
                            <label for="condominio_sindico_rg" class="btn-upload">
                                <input type="hidden" name="condominio_sindico_rg_enviado" id="condominio_sindico_rg_enviado"/>
                                <input type="file" name="condominio_sindico_rg" id="condominio_sindico_rg"
                                       class="form-control file" value="{{ old('condominio_sindico_rg') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo {{ (empty($documentos['condominio_sindico_rg'])) ? '' : 'para alterar' }}</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('condominio_sindico_rg'))
                                <p class="text-danger">{{ $errors->first('condominio_sindico_rg') }}</p>
                            @endif
                        </div>

                    </div>
                </div>

                @if($errors->has('condominio_sindico_rg'))
                    <p class="text-danger">{{ $errors->first('condominio_sindico_rg') }}</p>
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group {{ ($errors->has('condominio_sindico_cpf')) ? 'error':'' }}">
                <label for="rg" class="required">CPF do Síndico </label>
                <input type="text"value="{{ $sindico->cpf or ''}}" name="condominio_sindico_cpf_text"
                       id="" class="form-control required cpf" style="margin-bottom: 10px;">
                <div>
                    @if(!empty($documentos['condominio_sindico_cpf']))
                    <div class="col-xs-2">
                        <div class="form-group">
                            <a href="{{ route('file', ['arquivo' => $documentos['condominio_sindico_cpf'], 'empresa' => $empresa->id]) }}" target="blank" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa ver-doc" style="background-color: #009053; border-color:  #009053">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                        </div>
                    </div>
                    @endif
                    <div class="col-xs-{{ (empty($documentos['condominio_sindico_cpf'])) ? '12' : '10' }}">

                        <div class="form-group {{ ($errors->has('condominio_sindico_cpf')) ? 'error':'' }}">
                            <label for="condominio_sindico_cpf" class="btn-upload">
                                <input type="hidden" name="condominio_sindico_cpf_enviado" id="condominio_sindico_cpf_enviado"/>
                                <input type="file" name="condominio_sindico_cpf" id="condominio_sindico_cpf"
                                       class="form-control file" value="{{ old('condominio_sindico_cpf') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo {{ (empty($documentos['condominio_sindico_cpf'])) ? '' : 'para alterar' }}</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('condominio_sindico_cpf'))
                                <p class="text-danger">{{ $errors->first('condominio_sindico_cpf') }}</p>
                            @endif
                        </div>

                    </div>
                </div>

                @if($errors->has('condominio_sindico_cpf'))
                    <p class="text-danger">{{ $errors->first('condominio_sindico_cpf') }}</p>
                @endif
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <h2 class="margin-top-40">Dados do Condomínio</h2>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-4">

            <div class="form-group">
                <label for="condominio_ata" class="required">Cópia da Ata de Assembléias de eleição do síndico</label>
            </div>
            @if(!empty($documentos['condominio_ata']))
            <div class="col-xs-2">
                <div class="form-group">
                    <a href="{{ route('file', ['arquivo' => $documentos['condominio_ata'], 'empresa' => $empresa->id]) }}" target="blank" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa ver-doc" style="background-color: #009053; border-color:  #009053">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-xs-{{ (empty($documentos['condominio_ata'])) ? '12' : '10' }}">

                <div class="form-group {{ ($errors->has('condominio_ata')) ? 'error':'' }}">
                    <label for="condominio_ata" class="btn-upload">
                        <input type="hidden" name="condominio_ata_enviado" id="condominio_ata_enviado"/>
                        <input type="file" name="condominio_ata" id="condominio_ata"
                               class="form-control file" value="{{ old('condominio_ata') }}"/>
                        <div class="progress">
                            <p>Escolha um arquivo {{ (empty($documentos['condominio_ata'])) ? '' : 'para alterar' }}</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>

                    @if($errors->has('condominio_ata'))
                        <p class="text-danger">{{ $errors->first('condominio_ata') }}</p>
                    @endif
                </div>

            </div>
        </div>

        <div class="col-xs-12 col-sm-4">

            <div class="form-group">
                <label for="condominio_contribuicao" class="required">Cópia do documento de instituição e contribuição do condomínio</label>
            </div>
            @if(!empty($documentos['condominio_contribuicao']))
            <div class="col-xs-2">
                <div class="form-group">
                    <a href="{{ route('file', ['arquivo' => $documentos['condominio_contribuicao'], 'empresa' => $empresa->id]) }}" target="blank" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa ver-doc" style="background-color: #009053; border-color:  #009053">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-xs-{{ (empty($documentos['condominio_contribuicao'])) ? '12' : '10' }}">

                <div class="form-group {{ ($errors->has('condominio_contribuicao')) ? 'error':'' }}">
                    <label for="condominio_contribuicao" class="btn-upload">
                        <input type="hidden" name="condominio_contribuicao_enviado" id="condominio_contribuicao_enviado"/>
                        <input type="file" name="condominio_contribuicao" id="condominio_contribuicao"
                               class="form-control file" value="{{ old('condominio_contribuicao') }}"/>
                        <div class="progress">
                            <p>Escolha um arquivo {{ (empty($documentos['condominio_contribuicao'])) ? '' : 'para alterar' }}</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>

                    @if($errors->has('condominio_contribuicao'))
                        <p class="text-danger">{{ $errors->first('condominio_contribuicao') }}</p>
                    @endif
                </div>

            </div>
        </div>

        <div class="col-xs-12 col-sm-4">

            <div class="form-group">
                <label for="condominio_procuracao">Cópia da procuração da administração</label>
            </div>
            @if(!empty($documentos['condominio_procuracao']))
            <div class="col-xs-2">
                <div class="form-group">
                    <a href="{{ route('file', ['arquivo' => $documentos['condominio_procuracao'], 'empresa' => $empresa->id]) }}" target="blank" class="btn btn-default btn-warning modal-bts-center__externo btn-empresa ver-doc" style="background-color: #009053; border-color:  #009053">
                        <span class="glyphicon glyphicon-eye-open"></span>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-xs-{{ (empty($documentos['condominio_procuracao'])) ? '12' : '10' }}">

                <div class="form-group {{ ($errors->has('condominio_procuracao')) ? 'error':'' }}">
                    <label for="condominio_procuracao" class="btn-upload">
                        <input type="hidden" name="condominio_procuracao_enviado" id="condominio_procuracao_enviado"/>
                        <input type="file" name="condominio_procuracao" id="condominio_procuracao"
                               class="form-control file" value="{{ old('condominio_procuracao') }}"/>
                        <div class="progress">
                            <p>Escolha um arquivo {{ (empty($documentos['condominio_procuracao'])) ? '' : 'para alterar' }}</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>

                    @if($errors->has('condominio_procuracao'))
                        <p class="text-danger">{{ $errors->first('condominio_procuracao') }}</p>
                    @endif
                </div>

            </div>
        </div>
    </div>
  
</div>
