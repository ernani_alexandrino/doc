@extends('layouts.home-painel-destino')

@section('conteudo')

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="vinculo-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <div class="col-xs-12 texto-informativo">
        Aqui você cadastra as empresas transportadoras de resíduos que prestam serviços à sua empresa.
    </div>

    <!-- transportadores -->
    <div>
        @include('painel.vinculos.listas.transportadores')
        @include('painel.vinculos.modais.transportador')
        <div class="modal fade" id="modalEditTransportador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        </div>

        @include('painel.clientes-fornecedores.modais.desvincular-empresa')
        @include('painel.clientes-fornecedores.modais.restaurar-vinculo')
    </div>

    <!-- vinculos deletados -->
    <div>
        @include('painel.clientes-fornecedores.vinculos-removidos')
    </div>

@stop