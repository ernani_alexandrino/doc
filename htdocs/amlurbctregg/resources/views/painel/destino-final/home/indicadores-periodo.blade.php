<div class="indicadores-periodo" id="indicadores_periodo">
    <!-- cabecalho -->
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="titulo-indicador"></div>
            </div>
            <div class="col-xs-12 col-sm-8">
                <ul class="nav nav-pills nav-indicadores-periodo">
                    <li role="presentation">
                        <a href="#!" data-periodo="mensal" class="opcao-indicador-periodo">mês</a>
                    </li>
                    <li role="presentation">
                        <a href="#!" data-periodo="anual" class="opcao-indicador-periodo">ano</a>
                    </li>
                    <li role="presentation">
                        <a href="#!" data-periodo="total" class="opcao-indicador-periodo">total</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- estatisticas -->
    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
        <div class="row">
            <div class="box-indicador-periodo">
                <div class="col-xs-12 col-sm-5">
                    <div class="indicador-transportador"></div>
                </div>
                <div class="col-xs-12 col-sm-7">
                    <div class="indicador-total count" id="total_transportador">0</div>
                    <div class="indicador-nome">Transportadores</div>
                </div>
            </div>
        </div>
    </div>
</div>