<div class="indicadores-df" id="indicadores-ctre">
    @include('painel.home.indicadores-ctre')

    <!-- estatisticas -->
    <div class="col-xs-12 col-sm-3">
        <div class="box-indicador">
            <div class="image-indicador total-emitido"></div>
            <div class="corpo-indicador">
                <div class="contador" id="total_ctre_aberto">
                    <!-- ajax -->
                </div>
                <div class="titulo-indicador">
                    Total CTR Recebidos em Aberto
                </div>
                <div class="diviser"></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3">
        <div class="box-indicador">
            <div class="image-indicador total-finalizados"></div>
            <div class="corpo-indicador">
                <div class="contador" id="total_ctre_finalizado">
                    <!-- ajax -->
                </div>
                <div class="titulo-indicador">
                    Total CTR Recebidos Finalizados
                </div>
                <div class="diviser"></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3">
        <div class="box-indicador">
            <div class="image-indicador total-expirados"></div>
            <div class="corpo-indicador">
                <div class="contador" id="total_ctre_expirado">
                    <!-- ajax -->
                </div>
                <div class="titulo-indicador">
                    Total CTR Expirados
                </div>
                <div class="diviser"></div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-3">
        <div class="box-indicador">
            <div class="image-indicador total-finalizados-expirados"></div>
            <div class="corpo-indicador">
                <div class="contador" id="total_ctre_expirado_finalizado">
                    <!-- ajax -->
                </div>
                <div class="titulo-indicador">
                    Total CTR </br> Expirados Finalizados
                </div>
                <div class="diviser"></div>
            </div>
        </div>
    </div>
</div>