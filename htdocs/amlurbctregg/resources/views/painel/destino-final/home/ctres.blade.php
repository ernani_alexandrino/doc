<div class="col-xs-12">

    <!-- cabecalho -->
    <div class="row box-emitidas-recebidas">
        <div class="col-xs-12 col-sm-4">
            <div class="tituloIndicador tituloIndicador__emitir">
                <span>CTR-e Recebidas</span>
            </div>
        </div>

    </div>

    <p class="texto-informativo">
        Na tabela abaixo você vê os CTRs que seus Transportadores emitiram para que
        você possa validá-los, clicando no botão Validar da coluna Validação. Essa ação é de
        extrema importância para a conclusão de todo o processo do CTR-E.
    </p>
</div>

<div class="row" style="display: none">
    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
        <div id="ctre-validate-success" class="alert alert-success margin-top-20 text-center">

        </div>
    </div>
</div>

@include('painel.destino-final.home.ctres-recebidos')
<div class="modal fade" id="modalVisualizarCTRE" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!-- AJAX -->
</div>

<div class="modal fade" id="modalValidarCTRE" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!-- AJAX -->
</div>