<div class="col-xs-12">

    <h2>1. CTR-E Recebidas de Transportadores</h2>

    <div class="box-configuracoes">

        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-ctres" id="table-ctres-recebidos-destino">
                        <thead>
                            <tr>
                                <th>DATA</th>
                                <th>NR. CTR-E</th>
                                <th>PLACA VEÍCULO</th>
                                <th>TRANSPORTADOR</th>
                                <th>STATUS</th>
                                <th class="camp-actions-table">AÇÕES</th>
                            </tr>
                        </thead>
                        <tbody id="ctre-df-tr-list" name="ctre-list">
                            <!-- popula via Javascript (Ajax) -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>