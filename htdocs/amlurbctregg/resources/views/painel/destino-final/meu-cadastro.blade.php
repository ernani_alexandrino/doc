@extends('layouts.home-painel-destino')

@section('conteudo')

    @if(\Illuminate\Support\Facades\Auth::user()->empresa->status_id == \config('enums.status.ativo'))
        <div class="informacoes-qrcode">

            @include('painel.meu-cadastro.qrcode')

        </div>

        <br><br><br>
    @endif
    <form id ="dados_empresa_form">
    <div class="cadastro-destino-final">

        @include('painel.meu-cadastro.dados-empresa')
        @include('painel.meu-cadastro.modais.solicitar-alteracao-empresa')
        @include('painel.meu-cadastro.modais.aviso-de-alteracao')

    </div>
   
    <hr>
    @include('painel.destino-final.socios-destino-final')
    <hr>
    <div class="documentos-destino-final">

        @include('painel.destino-final.documentos-destino-final')

    </div>
  
    <hr>
    <div class="cadastro-credenciais">

        @include('painel.meu-cadastro.dados-credenciais')

    </div>
    
</form>

@endsection