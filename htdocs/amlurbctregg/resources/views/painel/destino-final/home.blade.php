@extends('layouts.home-painel-destino')

@section('conteudo')

    @include('painel.destino-final.home.indicadores')

    @include('painel.destino-final.home.indicadores-periodo')

    @include('painel.destino-final.home.ctres')

@endsection
