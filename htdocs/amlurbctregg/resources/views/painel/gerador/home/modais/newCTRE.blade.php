<div class="modal fade" id="modalNewCTREGerador" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Emitir Novo CTR-E para o Transportador</h4>
            </div>

            <div class="modal-body">
                <div class="row modal-body__instrucoes">
                    <div class="col-xs-12">
                        Preencha todos os campos para a emissão do seu CTR-E. Campos com (*) são obrigatórios.
                    </div>
                </div>

                <form id="frmCtre" name="frmCtreGerador" class="form-horizontal" novalidate="">

                    {{ csrf_field() }}

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="" id="ctre-gg-transportador">
                                <label for="transportador">Transportador*</label>
                                <select name="transportador" id="transportador" class="form-control transportador">
                                    <option value="">Selecione</option>
                                    @foreach($transportadores as $transportador )
                                        <option value="{{ $transportador->id }}">
                                            {{ $transportador->razao_social }}
                                        </option>
                                    @endforeach
                                </select>
                                <p class="text-danger"></p>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="" id="ctre-gg-residuos">
                                        <label for="residuos_vinculo">Resíduos*</label>
                                        <div class="form-item-linha">   
                                            <select name="residuos_vinculo" id="residuos_vinculo" class="form-control residuos_vinculo form-item-linha__item" disabled>
                                                <option value="">Selecione</option>
                                            </select>
                                            <button id="incluir_residuos_vinculo" class="btn btn-default btn-incluir-residuo residuo form-item-linha__bt">Incluir</button>
                                            <label for="incluir_residuos_vinculo" class="hidden-element">Incluir</label>
                                        </div>
                                        <p class="text-danger"></p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-6">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="" id="ctre-gg-equipamentos">
                                        <label for="equipamentos_vinculo">Acondicionamentos</label>
                                        <div class="form-item-linha">   
                                            <select name="equipamentos_vinculo" id="equipamentos_vinculo" class="form-control equipamentos_vinculo form-item-linha__item" disabled>
                                                <option value="">Selecione</option>
                                            </select>
                                            <button id="incluir_equipamentos_vinculo" type="button" class="btn btn-default btn-incluir-equipamento equipamento form-item-linha__bt">Incluir</button>
                                            <label for="incluir_equipamentos_vinculo" class="hidden-element">Incluir</label>
                                        </div>
                                        <p class="text-danger"></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 caixa-incluidos">
                            <div id="residuos-escolhidos" class="residuos-escolhidos">

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 caixa-incluidos">
                            <div id="equipamentos-escolhidos" class="equipamentos-escolhidos">

                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="modal-bts-center">
                                <button type="button" class="btn btn-default btn-warning btn-save-ctre" id="btn-save-new-ctre-gg">Emitir CTR-E</button>
                                <button type="reset" class="btn btn-default btn-danger" id="btn-reset-new-ctre-gg">Cancelar</button>
                            </div>
                        </div>

                    </div>

                </form>

            </div>

        </div>
    </div>
</div>