<div class="col-xs-12">

    <h2>1. CTR-E Emitidas pelo Gerador</h2>

    <div class="box-configuracoes">

        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-ctres" id="table-ctres-emitidos-gerador">
                        <thead>
                            <tr>
                                <th>DATA</th>
                                <th>NR. CTR-E</th>
                                <th>TRANSPORTADOR</th>
                                <th>RESÍDUOS</th>
                                <th>STATUS</th>
                                <th style="text-align: center;">VALIDAÇÃO</th>
                            </tr>
                        </thead>
                        <tbody id="ctre-gg-list" name="ctre-list">
                            <!-- popula via Javascript (Ajax) -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>