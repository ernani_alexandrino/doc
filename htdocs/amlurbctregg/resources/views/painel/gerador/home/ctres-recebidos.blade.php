<div class="col-xs-12">

    <h2>2. CTR-E Recebidas de Transportadores</h2>

    <div class="box-configuracoes">

        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-ctres" id="table-ctres-recebidos-gerador">
                        <thead>
                            <tr>
                                <th>DATA</th>
                                <th>NR. CTR-E</th>
                                <th>TRANSPORTADOR</th>
                                <th>EQUIPAMENTOS</th>
                                <th>STATUS</th>
                                <th style="text-align: center;">VALIDAÇÃO</th>
                            </tr>
                        </thead>
                        <tbody id="ctre-gg-tr-list" name="ctre-list">
                            <!-- popula via Javascript (Ajax) -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>