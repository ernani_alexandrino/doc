@extends('layouts.home-painel-gerador')

@section('conteudo')

    <div class="row" style="display: none">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div id="ci-success" class="alert alert-success margin-top-20 text-center">

            </div>
        </div>
    </div>

    <div class="col-xs-12 texto-informativo">
        <p>
            Aqui você cadastra os equipamentos que serão usados no transporte de resíduos. 
        </p>
        <p>
            Após o cadastro de um equipamento, é preciso ativá-lo para consolidar seus dados no sistema CTR-E.
            Para ativar ou renovar apenas um equipamento, clique no primeiro item da coluna "Ações". Para ativar ou renovar um ou mais de um equipamento, use o botão "Ativar/Renovar Equipamentos".
        </p>
        <p>
            Caso os equipamentos em uso sejam do transportador, não será necessário cadastrá-los.
        </p>
    </div>

    <!-- equipamentos -->

    @include('painel.equipamentos.equipamentos-vinculados')

    @include('painel.equipamentos.modais.cadastrar-equipamento')
    @include('painel.equipamentos.modais.qrcode-equipamento')
    @include('painel.equipamentos.modais.excluir-equipamento')
    @include('painel.equipamentos.modais.ativar-renovar-equipamento')
    <div class="modal fade" id="modalAtivarRenovarEquipamentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    </div>

@stop