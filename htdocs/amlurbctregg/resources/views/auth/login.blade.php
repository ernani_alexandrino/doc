@extends('layouts.home')
@section('conteudo')
<section id="login">
   <div class="container-fluid">
      <div class="row d-flex flex-row justify-content-around">
         <div class="col-xs-12 col-md-6">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <img class="logo-ctre-login" src="{{ asset('images/novo-logo-ctre.svg') }}"
                     alt="CTRE - Controle de Transportes de Resíduos - Eletrônico">
               </div>
               <div class="panel-body">
                  <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                     {{ csrf_field() }}
                     @if ($errors->any())
                     <div class="col-xs-12">
                        <div class="alert alert-danger mensagem">
                           @foreach ($errors->all() as $error)
                           <p>{{ $error }}</p>
                           @endforeach
                        </div>
                     </div>
                     @endif
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="input-group">
                              <span class="input-group-addon" id="basic-cidades">
                              <i class="icon-cidade"></i>
                              </span>
                              {!! Form::select('cidades', ['26' => 'SAO PAULO'], old('cidades',26), ['placeholder' => 'Selecione a cidade', 'class' => 'form-control cidades', 'id' => 'cidades']) !!}
                           </div>
                        </div>
                     </div>
                     <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <div class="col-xs-12">
                           <div class="input-group">
                              <span class="input-group-addon" id="addonLoginEmail">
                              <i class="icon-login"></i>
                              </span>
                              <input type="email" name="email" class="form-control input-login" id="email"
                                 value="{{ old('email') }}" placeholder="E-mail" aria-label="Email"
                                 aria-describedby="addonLoginEmail" required autofocus/>
                           </div>
                        </div>
                     </div>
                     <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <div class="col-xs-12">
                           <div class="input-group">
                              <span class="input-group-addon" id="basic-addon2">
                              <i class="icon-senha"></i>
                              </span>
                              <input type="password" name="password" class="form-control input-login"
                                 id="password" placeholder="Senha" aria-label="Password"
                                 aria-describedby="basic-addon2" required/>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-xs-12">
                           <button type="submit" class="btn btn-login">Entrar</button>
                           <a class="btn btn-link" href="{{ route('password.request') }}"
                              title="Esqueci meus dados de acesso!">
                           Esqueci meus dados de acesso! 
                           </a>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <div class="cadastro-aqui">
               <p style="font-size: 22px;">Não tem acesso? <a href="{{ url('cadastro') }}" title="Cadastre-se aqui">Cadastre-se
                  aqui.</a>
               </p>
            </div>
         </div>
         <div class="login-cidade__infos">
            <div class="panel panel-default div_faq">
               <div class="tabLogin">
                  <ul>
                     <li class="tablink activeTabLogin" onclick="openTab(event,'Decreto')">Decreto</li>
                     <li class="tablink" onclick="openTab(event,'Perguntas')">Perguntas frequentes 
                        <span style="color: #f08719;font-weight: bold;">( ? )</span>
                     </li>
                  </ul>
               </div>
               <div id="Decreto" class="panelLogin">
                  <div class="panel-heading text-center" style="padding-bottom: 0;padding-top: 10px;">
                     <img src="{{ asset('images/brasao_da_cidade_de_sao_paulo.png') }}"
                        alt="Plataforma Verde - Digitalizando o Lixo"/>
                  </div>
                  <div class="panel-body" style="padding-top: 0;">
                     <h3 class="text-center">Cidade de São Paulo</h3>
                     <p>O Controle de Transporte de Resíduos (CTR-E) é um sistema de fiscalização e
                        rastreabilidade criado para cadastrar todos os entes privados(geradores, transportadores, 
                        cooperativas e destinos finais), que fazem parte do sistema de limpeza urbana que geram mais
                        de 200L/ dia. A tecnologia permite que a Prefeitura Municipal de São Paulo, por meio da
                        Autoridade Municipal de Limpeza Urbana – AMLURB, saiba como o resíduo é coletado, 
                        transportado e por fim, destinado. Com isto, esperam-se melhorias na zeladoria urbana, 
                        na saúde pública, além de economia de recursos públicos.<br>
                        De acordo com o 
                        <a href="http://diariooficial.imprensaoficial.com.br/nav_v5/index.asp?c=1&e=20190405&p=1" target="_blank">
                        decreto Nº 58.701/2019  
                        </a>
                        e a
                        <a href="http://diariooficial.imprensaoficial.com.br/nav_v5/index.asp?c=1&e=20190410&p=1" target="_blank">
                        Resolução 130/AMLURB/2019
                        </a>, todos os estabelecimentos privados (indústria, comércio e 
                        serviços) situados no município de São Paulo devem realizar seu cadastro perante à Amlurb, por 
                        meio do sistema que, baseado nas informações fornecidas, irá classificá-los como pequenos ou 
                        grandes geradores, a partir de autodeclaração, sendo os mesmos sujeitos às sanções e responsabilidades,
                        de acordo com o ART. 299 do Código Penal – 
                        <a href="http://www.planalto.gov.br/ccivil_03/Decreto-Lei/Del2848compilado.htm" target="_balnk">
                        Decreto Lei 2848/40.
                        </a>
                        <br>
                        Para qualquer dúvida ligue pelo nosso canal de atendimento 156.
                        <br>
                        Para iniciar o seu cadastro no 
                        sistema CTR-E, clique no "<a href="{{ url('cadastro') }}" title="Cadastre-se aqui">Cadastre-se
                        aqui</a>".
                     </p>
                  </div>
               </div>
               <div id="Perguntas" class="panelLogin" style="display:none">
                  <div class="panel-body" style="padding-top: 0;">
                     <div class="accordionWrapper">
                        <div class="accordionItem close noOpacity">
                           <h2 class="accordionItemHeading">INFORMAÇÕES SOBRE O DECRETO </h2>
                           <div class="accordions accordionItemContent">
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-1" />
                                 <label for= "accordion-1">O que é o decreto Nº 58.701?</label>
                                 <div class="accordion-content">
                                    <p>O decreto Nº 58.701, de 4 de abril de 2019, regulamenta os artigos 123, 140, 141 e 142  da Lei nº 13.478, de 30 de dezembro de 2002, que dispõe sobre a organização do Sistema de Limpeza Urbana do Município de São Paulo, fixa competências voltadas à fiscalização das posturas municipais e à aplicação das respectivas penalidades previstas na referida lei, bem como revoga os decretos que especifica.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-3" />
                                 <label for= "accordion-3">Artigo 123</label>
                                 <div class="accordion-content">
                                    <p>Art. 123 - A prestação dos serviços de limpeza urbana no regime privado dependerá de prévia expedição de autorização pela Autoridade Municipal de Limpeza Urbana - AMLURB e poderá ser onerosa.</p>
                                    <p>§ 1º - Sem prejuízo do disposto acima, a Autoridade Municipal de Limpeza Urbana - AMLURB definirá os casos de serviços de limpeza urbana prestados em regime privado que não dependerão de autorização.</p>
                                    <p>§ 2º - O prestador dispensado de autorização deverá comunicar o início de suas atividades previamente à Autoridade Municipal de Limpeza Urbana - AMLURB.</p>
                                    <p>§ 3º - A Autoridade Municipal de Limpeza Urbana - AMLURB poderá condicionar a expedição de autorização ao pagamento de preço público proporcional à vantagem econômica usufruída.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-4" />
                                 <label for= "accordion-4">Artigo 140 </label>
                                 <div class="accordion-content">
                                    <p>Art. 140 - Os grandes geradores ficam obrigados a cadastrar-se junto à Autoridade Municipal de Limpeza Urbana - AMLURB, na forma e no prazo em que dispuser a regulamentação.</p>
                                    <p> § 1º - Do cadastro constará declaração de volume e massa mensal de resíduos sólidos produzidos pelo estabelecimento, o operador contratado para a realização dos serviços de coleta e o destino da destinação final dos resíduos sólidos, além de outros elementos necessários ao controle e fiscalização pelo Município.</p>
                                    <p> § 2º - Havendo alteração na quantidade de resíduos sólidos produzidos, o estabelecimento gerador atualizará seu cadastro junto à Autoridade Municipal de Limpeza Urbana - AMLURB em 30 (trinta) dias, contados da alteração.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-5" />
                                 <label for= "accordion-5">Artigo 141</label>
                                 <div class="accordion-content">
                                    <p>Art. 141 - Os grandes geradores deverão contratar os autorizatários dos serviços prestados em regime privado de que trata esta lei para a execução dos serviços de coleta, transporte, tratamento e destinação final dos resíduos referidos no presente Capítulo, mantendo via original do contrato à disposição da fiscalização.</p>
                                    <p>§ 1º - É vedado aos grandes geradores a disposição dos resíduos nos locais próprios da coleta de resíduos domiciliares ou de serviços de saúde, bem como em qualquer área pública, incluindo passeios e sistema viário, sob pena de multa.</p>
                                    <p> § 2º - No caso de descumprimento da norma estabelecida no parágrafo anterior, sem prejuízo da multa nele prevista, o grande gerador arcará com os custos e ônus decorrentes da coleta, transporte, tratamento e destinação final de seus resíduos, recolhendo junto à Autoridade Municipal de Limpeza Urbana - AMLURB, os valores correspondentes.</p>
                                    <p> § 3º - Os valores pagos pelo grande gerador para cobrir os custos e ônus mencionados no parágrafo anterior serão destinados a custear o serviço de limpeza urbana de coleta, transporte, tratamento e destinação final de resíduos sólidos domiciliares e serão depositadas na conta vinculada especial prevista no artigo 80 desta lei.  </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-6" />
                                 <label for= "accordion-6">Artigo 142</label>
                                 <div class="accordion-content">
                                    <p>Art. 142 - Os grandes geradores deverão manter em seu poder registros e comprovantes de cada coleta feita, da quantidade coletada e da destinação dada aos resíduos.</p>
                                    <p> § 1º - Os registros e comprovantes de que trata o "caput" deste artigo deverão ser apresentados à fiscalização quando solicitados, sob pena de multa e de cobrança de todos os custos e ônus resultantes da coleta, transporte, tratamento e destinação dos resíduos produzidos pelo grande gerador no período sem comprovação, acrescidos de correção monetária.</p>
                                    <p> § 2º - A fiscalização poderá estimar a quantidade de resíduos produzidos pelo estabelecimento gerador, por meio de diligências em pelo menos 3 (três) dias diferentes.</p>
                                    <p> § 3º - A estimativa de que trata o parágrafo anterior subsidiará a cobrança prevista no artigo anterior, sem prejuízo da aplicação da multa prevista.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-7" />
                                 <label for= "accordion-7">O que é a resolução?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Resolução é um instrumento administrativo com a finalidade de disciplinar a matéria de sua competência. Estabelece de forma detalhada o rito descrito nos Decretos ou Leis de competência desta Autarquia. A resolução Nº 130/AMLURB/2019 - regulamenta o cadastro dos operadores do sistema de limpeza urbana do município e o controle de transporte de resíduos sólidos para os grandes geradores – CTR-E GG.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="accordionItem close noOpacity">
                           <h2 class="accordionItemHeading">QUEM DEVE SE CADASTRAR NO CTR-E RGG? </h2>
                           <div class="accordions accordionItemContent">
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-8" />
                                 <label for= "accordion-8">Quem deve se cadastrar?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Todas as empresas situadas no município de São Paulo, assim como todas as empresas com sede fora da capital, mas que prestam serviços no processo de transporte, manuseio, reciclagem ou destino final de resíduos sólidos gerados na cidade.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-9" />
                                 <label for= "accordion-9">Por que devo me cadastrar no CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>A Prefeitura de São Paulo, por meio da AMLURB (Autoridade Municipal de Limpeza Urbana), em cumprimento ao PGIRS – Plano de Gestão Integrada de Resíduos Sólidos da Cidade de São Paulo, pretende melhorar o gerenciamento de todas as emissões e destinos de resíduos sólidos gerados na cidade. Para isso, necessita cadastrar todas as empresas envolvidas no processo. A iniciativa pretende diminuir os gastos com a coleta pública do lixo, melhorar as ações de zeladoria da cidade e aumentar o controle das etapas do sistema, além de minimizar a proliferação de pragas urbanas (roedores, aves e insetos) a partir da melhoria do sistema de coleta e destinação do lixo. Portanto, o cadastramento de todas as empresas é fundamental para o controle efetivo de todos os entes envolvidos.    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-10" />
                                 <label for= "accordion-10">Como saber se sou um grande gerador? </label>
                                 <div class="accordion-content">
                                    <p>De acordo com a LEI 13.478/02, suas alterações, os Decretos regulamentadores e em consonância com o PGIRS - Plano Municipal de Gestão Integrada de Resíduos Sólidos, todas as instituições do território nacional, de qualquer segmento, porte ou natureza pública ou privada, que gerem, no mínimo, 200 litros de resíduos do tipo domiciliar por dia, ou mais de 50 quilos de inertes (entulho, terra e materiais de construção), bem como condomínios de edifícios empresariais, residenciais ou de uso misto, em que a soma dos resíduos do tipo domiciliar gerados pelos condôminos some volume médio diário acima de 1.000 litros, são classificadas como grandes geradoras.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-11" />
                                 <label for= "accordion-11">Não sei quantos litros de resíduos minha empresa gera por dia. Como posso calcular o volume?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Utiliza-se como referência os sacos de lixo nos quais constam sua capacidade (50L, 100L, 200L etc).</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-12" />
                                 <label for= "accordion-12">Sou um transportador cadastrado no sistema do RCC (construção civil). Preciso me recadastrar também?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Todo transportador que realizar serviços relacionados à retirada de resíduos da Construção Civil (RCC) deverá possuir o devido cadastro na AMLURB e nos dois sistemas.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-13" />
                                 <label for= "accordion-13">Já estou cadastrado na AMLURB. Devo me cadastrar no CTR-E RGG também?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Sim. Este cadastro, mais amplo, servirá para unificar todos os cadastros feitos anteriormente, portanto, será preciso efetuar recadastro online para continuar a atuar na cidade de São Paulo.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-14" />
                                 <label for= "accordion-14">Sou microempreendedor. Preciso me cadastrar no CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Sim, toda empresa com CNPJ (ME, MEI, EIRELE, etc) deve se cadastrar no sistema, independentemente do porte ou ramo de atividade, objetivando o mapeamento da cidade no que tange a geração de resíduos.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-15" />
                                 <label for= "accordion-15">Sou uma unidade de serviço de saúde. Preciso me cadastrar no sistema CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Todos os geradores têm que se cadastrar. Porém na hora de declarar os volumes gerados, não deve considerar os resíduos infectantes que são coletados pela prefeitura. Só deve declarar os volumes de resíduos comuns classe II-A.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-16" />
                                 <label for= "accordion-16">Sou um transportador com sede em outro município, mas transporto resíduos na capital. Devo me cadastrar no sistema CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Todas as empresas situadas no município de São Paulo, assim como todas as empresas com sede fora da capital, mas que prestam serviços no processo de transporte, manuseio, reciclagem ou destino final de resíduos sólidos gerados na cidade, devem se cadastrar no sistema CTR-E RGG.</p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-17" />
                                 <label for= "accordion-17">Minha empresa ainda não tem licença de funcionamento da CETESB. Preciso me cadastrar no sistema?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Se você gera resíduos no município, sim.</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="accordionItem close noOpacity">
                           <h2 class="accordionItemHeading">REGRAS SOBRE CADASTRAMENTO</h2>
                           <div class="accordions accordionItemContent">
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-18" />
                                 <label for= "accordion-18">A partir de quando devo me cadastrar no CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>De acordo com o Decreto Nº 58.701 publicado no dia 5 de abril 2019, os estabelecimentos terão 90 dias de prazo para se cadastrarem no sistema CTR-E RGG. Após essa data, estarão sujeitos às penalidades previstas na legislação.  </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-19" />
                                 <label for= "accordion-19">Qual o custo de inscrição no CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Não há custo para o uso do sistema CTR-E RGG. Porém há a taxa AMLURB que já aplicava no processo de cadastramento físico. Por exemplo, os grandes geradores deverão pagar uma taxa anual estabelecida pelo Decreto de Preços Publico de: R$ 228 (duzentos e vinte e oito reais) e para os Transportadores R$ 117 (cento e dezessete reais).
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-20" />
                                 <label for= "accordion-20">Já paguei meu cadastro antes do novo decreto. Vou ter que pagar de novo?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Não. Observe se o cadastro está vigente e se foi realizado nos últimos 12 meses. Neste caso, ele estará isento da taxa.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-21" />
                                 <label for= "accordion-21">Qual é a validade do cadastro pago para a AMLURB?
                                 </label>
                                 <div class="accordion-content">
                                    <p>A validade do cadastro na AMLURB é de 1 ano, para todos os entes envolvidos na cadeia de resíduos sólidos. Após esse período é preciso realizar o recadastro no sistema CTR-E RGG.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-22" />
                                 <label for= "accordion-22">Sempre cadastrei meus geradores na AMLURB. Posso continuar fazendo o cadastro deles?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Cada empresa deve ser responsável pelo seu cadastro perante a AMLURB. Além disso, é necessário ter um e-mail cadastrado por CNPJ. Informamos ainda que a AMLURB não se responsabiliza por cadastros realizados por terceiros.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-23" />
                                 <label for= "accordion-23">Sou transportador, mas também sou gerador. Como faço o cadastro?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Nesse caso será preciso fazer um cadastro para cada CNPJ.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-24" />
                                 <label for= "accordion-24">Minha empresa gera resíduos, mas ainda não tenho uma empresa de transportes para enviá-los para um destino autorizado. Como posso contratar este serviço?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Você pode buscar a lista de transportadores licenciados na AMLURB no site www.amlurb.sp.gov.br
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-25" />
                                 <label for= "accordion-25">O que é o Controle de Transporte de Resíduos (CTR)?
                                 </label>
                                 <div class="accordion-content">
                                    <p>O Controle de Transporte de Resíduos (CTR) é o documento numerado de que fornece informações sobre a expedição, o transporte e o recebimento dos resíduos no local de destinação final.</p>
                                    <p>A implementação do Controle de Transporte de Resíduos (CTR-E RGG) se deu por meio da resolução no 130/AMLURB/2019, visando preservar as regras que regem o segmento, sem alteração na legislação atual, criando mecanismos de controle e monitoramento.
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="accordionItem close noOpacity">
                           <h2 class="accordionItemHeading">ASSUNTOS SOBRE O SISTEMA CTR-E RGG</h2>
                           <div class="accordions accordionItemContent">
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-26" />
                                 <label for= "accordion-26">Como acesso o sistema CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Você acessa o sistema CRT-E RGG pelo site <a href="www.amlurb.sp.gov.br" target="_blank">www.amlurb.sp.gov.br</a> a partir de qualquer dispositivo com acesso à internet (computador, notebook, celular, tablet). Importante: para o cadastro, use uma conta de e-mail válida (exemplo: nome@dominio.com.br), pois ela servirá como login de acesso ao sistema e será o seu elo de comunicação com a AMLURB, que enviará todas as mensagens e instruções para o endereço eletrônico cadastrado.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-27" />
                                 <label for= "accordion-27">Como criar meu login e senha de acesso no sistema CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Ao acessar o site www.amlurb.sp.gov.br, na aba cadastros / formulários, clique em CTR-RGG e realize o cadastro. Em seguida, informe o CNPJ da empresa e clique em “Prosseguir”. Alguns dados de sua empresa serão mostrados automaticamente. Preencha os campos restantes e clique em “Prosseguir”.</p>
                                       <p>Na tela seguinte, insira os dados da pessoa que será a responsável pelo acesso ao sistema CTR-E RGG e clique em “Prosseguir”. Uma nova tela será mostrada para que você informe o volume diário de geração de resíduos de sua empresa e a frequência com que eles são recolhidos.</p>
                                       <p>No passo seguinte, responda às informações complementares sobre a sua empresa e clique em “Prosseguir”. Em seguida, leia com atenção aos “Termos de Uso e Política de Privacidade” e clique em “Concluir Cadastro” para finalizar o processo. Após uma avaliação da AMLURB, você receberá o seu login pelo e-mail cadastrado no sistema.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-28" />
                                 <label for= "accordion-28">Se eu não tiver todos os documentos em arquivo no momento, posso completar o cadastro depois?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Não. Para realizar o cadastro no sistema CTR-E RGG é preciso anexar todos os documentos. Por isso, reúna o arquivo de todos os documentos. Para consultar a lista de documentos necessários para o cadastro, clique aqui
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-30" />
                                 <label for= "accordion-30">Posso cadastrar duas empresas com o mesmo e-mail?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Não é possível cadastrar duas empresas com o mesmo e-mail. O sistema só aceita um e-mail por CNPJ.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-31" />
                                 <label for= "accordion-31">Já sou cadastrado no sistema CTR-E RGG, mas perdi o e-mail de acesso?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Entre em contato com a AMLURB para cadastrar um novo e-mail de acesso.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-32" />
                                 <label for= "accordion-32">Como faço para trocar o e-mail de acesso ao sistema CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Entre em contato com a AMLURB para cadastrar um novo e-mail de acesso.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-33" />
                                 <label for= "accordion-33">Fiz o meu cadastro no sistema CTR-E RGG, mas ainda não consigo acessar. O que devo fazer?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Seu cadastro no sistema CTR-E RGG é a primeira etapa do processo. Após essa etapa, o seu cadastro será revisado pela AMLURB e, em seguida, você receberá um e-mail com instruções para o pagamento da taxa. Após o pagamento, você poderá acessar ao sistema com um login (email) e uma senha. Precisará ainda você fazer o vínculos necessários (vincular os seus transportadores, destinatários, cooperativas, ..) para ser ativado no sistema.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-34" />
                                 <label for= "accordion-34">Há algum suporte para a utilização do sistema CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>Você pode contar com uma equipe de suporte para atender as demais dúvidas de uso e sobre procedimentos e legislação relacionados:<br>
                                       AMLURB:<br>
                                       - Reclamações e solicitações gerais: Central 156<br>
                                       - Dúvidas sobre Cadastro: 11 3397-1784<br>
                                       - Dúvidas sobre Fiscalização: 11 3397-1726<br>
                                       Também há um atendimento físico no local da AMLURB.
                                    </p>
                                 </div>
                              </div>
                              <div class="accordion-item">
                                 <input type="radio" name="accordion" id="accordion-35" />
                                 <label for= "accordion-35">De qual sistema operacional posso acessar o sistema CTR-E RGG?
                                 </label>
                                 <div class="accordion-content">
                                    <p>O sistema CTR-E pode ser acessado com qualquer navegador internet. 
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="">
            <div class="mapa-nacional"></div>
         </div>
      </div>
   </div>
   <script>
      function openTab(evt, tabName) {
          var i, x, tablinks;
          x = document.getElementsByClassName("panelLogin");
          for (i = 0; i < x.length; i++) {
              x[i].style.display = "none";
          }
          tablinks = document.getElementsByClassName("tablink");
          for (i = 0; i < x.length; i++) {
              tablinks[i].className = tablinks[i].className.replace(" activeTabLogin", "");
          }
          document.getElementById(tabName).style.display = "block";
          evt.currentTarget.className += " activeTabLogin";
      }
      
      var accItem = document.getElementsByClassName('accordionItem');
      var accHD = document.getElementsByClassName('accordionItemHeading');
      for (i = 0; i < accHD.length; i++) {
          accHD[i].addEventListener('click', toggleItem, false);
      }
      function toggleItem() {
          var itemClass = this.parentNode.className;
          for (i = 0; i < accItem.length; i++) {
              accItem[i].className = 'accordionItem close noOpacity';
          }
          if (itemClass == 'accordionItem close noOpacity') {
              this.parentNode.className = 'accordionItem open';
          }
      }
      
      
      
      
      
   </script>
</section>
@endsection