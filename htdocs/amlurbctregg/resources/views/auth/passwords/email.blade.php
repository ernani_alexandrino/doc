@extends('layouts.home')

@section('conteudo')
    <section id="login">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img class="logo-ctre-login" src="{{ asset('images/novo-logo-ctre.svg') }}"
                                 alt="CTRE - Controle de Transportes de Resíduos - Eletrônico">
                        </div>

                        <div class="panel-body">

                            @if (session('status'))
                                <div class="alert alert-success text-center">
                                    <p>{{ session('status') }}</p>
                                </div>
                            @endif

                            @if ($errors->has('email'))
                                <div class="alert alert-danger mensagem text-center">
                                    <p>{{ $errors->first('email') }}</p>
                                </div>
                            @endif

                            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="addonLoginEmail">
                                                <i class="icon-login"></i>
                                            </span>
                                            <input type="email" name="email" class="form-control input-login" id="email"
                                                   value="{{ old('email') }}" placeholder="E-mail" aria-label="Email"
                                                   aria-describedby="addonLoginEmail" required autofocus/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-login">Alterar senha</button>

                                        <a class="btn btn-link" href="{{ route('login') }}" title="Login">Login</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="cadastro-aqui">
                        <p>Não tem acesso? <a href="{{ url('cadastro') }}" title="Cadastre-se aqui">Cadastre-se
                                aqui.</a></p>
                    </div>
                </div>

                <div class="col-xs-12 col-md-6">
                    <div class="mapa-nacional">

                    </div>
                    {{-- <div class="logo-plataforma-verde">
                        <a href="https://plataformaverde.com.br" title="Plataforma Verde - Digitalizando o Lixo"
                           rel="noopener" target="_blank">
                            <img src="{{ asset('images/power-by-pv.png') }}"
                                 alt="Plataforma Verde - Digitalizando o Lixo"/>
                        </a>
                    </div> --}}
                </div>
            </div>
        </div>
    </section>
@endsection
