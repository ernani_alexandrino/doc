@extends("layouts.home-painel")

@section('conteudo')

    <section id="cadastro-empresa">

        <div class="container-fluid">
            <div class="row">

                @include('site.menu-lateral')

                <div class="col-xs-12 col-sm-10">
                    <div class="conteudo">

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="cadastroHeader">
                                    <h1>Cadastro de empresas <span>* campos obrigatórios</span></h1>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">

                            <div class="row" style="display: none;">
                                <div class="cadastroMensagem">
                                    <div class="col-xs-12 col-sm-3">
                                        <img src="{{ asset('images/mapa.png') }}" width="210" alt="">
                                    </div>

                                    <div class="col-xs-12 col-sm-9" id="div-texto-informativo-cadastro">
                                        <!-- ajax -->
                                    </div>
                                </div>
                            </div>

                            <div class="stepwizard" style="display: none;">
                                <div class="stepwizard-row setup-panel">

                                    <!-- TELAS 1 E 2 SAO IGUAIS PARA TODOS -->
                                    <div class="stepwizard-step">
                                        <a href="#tela-1" type="button" class="btn btn-success btn-circle">1/5</a>
                                    </div>
                                    <div class="stepwizard-step">
                                        <a href="#tela-2" type="button" class="btn btn-default btn-circle"
                                           disabled="disabled">2/5</a>
                                    </div>

                                    <!-- GERADOR -->
                                    <div class="stepwizard-step">
                                        <a href="#tela-4g" type="button" class="btn btn-default btn-circle"
                                           disabled="disabled">4/5</a>
                                    </div>

                                    <!-- CONDOMINIO MISTO -->

                                    <div class="stepwizard-step">
                                        <a href="#tela-3cm" type="button" class="btn btn-default btn-circle"
                                           disabled="disabled">3/5</a>
                                    </div>

                                    <!-- TRANSPORTADOR / DESTINO FINAL -->

                                    <div class="stepwizard-step">
                                        <a href="#tela-3tr" type="button" class="btn btn-default btn-circle"
                                           disabled="disabled">3/5</a>
                                    </div>

                                    <!-- COOPERATIVA -->
                                    <div class="stepwizard-step">
                                        <a href="#tela-3co" type="button" class="btn btn-default btn-circle"
                                           disabled="disabled">3/5</a>
                                    </div>

                                    <!-- DOCUMENTOS COOPERATIVA -->

                                    <div class="stepwizard-step">
                                        <a href="#tela-4co" type="button" class="btn btn-default btn-circle"
                                           disabled="disabled">3/5</a>
                                    </div>

                                    <!-- DOCUMENTOS TRANSPORTADOR / DESTINO FINAL -->

                                    <div class="stepwizard-step">
                                        <a href="#tela-4tr" type="button" class="btn btn-default btn-circle"
                                           disabled="disabled">4/5</a>
                                    </div>

                                    <!-- TERMOS DE USO -->

                                    <div class="stepwizard-step">
                                        <a href="#tela-5" type="button" class="btn btn-default btn-circle"
                                           disabled="disabled">5/5</a>
                                    </div>


                                </div>
                            </div>

                            @include('layouts.loading')

                            {{--@include('cadastro.transportador')--}}

                            {{ Form::open(['url' => 'cadastro', 'id' => 'formCadastroEmpresa']) }}

                            {{--Cadastro base--}}
                            <div class="containerPrimeiraEtapa setup-content" id="tela-1">

                                @if(!is_null(Illuminate\Support\Facades\Auth::user()))
                                    @if(\Illuminate\Support\Facades\Auth::user()->empresa->empresas_x_empresas_tipos->empresa_tipo_id == \config('enums.empresas_tipo.pequeno_gerador'))
                                        <div class="row">



                                            <!-- TODO mostrar botao de logout para pequeno gerador -->

                                        </div>
                                    @endif
                                @endif

                                <div class="row">
                                    <div class="col-xs-12">
                                        <h2>Passo 1 de 5. Dados da empresa</h2>
                                        <p>Caso seja o primeiro cadastro online da empresa enforme o CNPJ e clique em prosseguir.</p>                                        
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="form-group">
                                            <label for="cnpj" class="required">Informe o CNPJ</label>
                                            <input type="text" value="{{$cnpj}}" name="cnpj" id="cnpj" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-2">
                                        <div class="form-group">
                                            <label class="btn-block">&nbsp;</label>

                                            <button type="button" class="btn btnSubmit cnpjClica cnpjContinua btnProsseg">
                                                Prosseguir
                                            </button>
                                        </div>
                                    </div>
                                    @if(empty($empresa_id))
                                    <div class="col-xs-12 mensagemCnpj hide">
                                        <div class="alert"></div>
                                    </div>
                                    @endif                                    
                                </div>
                                <div class="ajax-panel">
                                    <!-- loading gif -->
                                </div>

                                <div class="containerPrimeiraEtapaContinuacao" style="display: none;">


                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4">

                                            <div class="form-group {{ ($errors->has('empresa_cartao_cnpj')) ? 'error':'' }}">
                                                <label for="empresa_cartao_cnpj" class="required">Cartão CNPJ (<span class="cnpj_digitado" style="font-size: 16px;"></span>)</label>
                                                <label for="empresa_cartao_cnpj" class="btn-upload">
                                                    <input type="hidden" name="empresa_cartao_cnpj_enviado" id="empresa_cartao_cnpj_enviado"/>
                                                    <input type="file" name="empresa_cartao_cnpj" id="empresa_cartao_cnpj"
                                                           class="form-control file" value="{{ old('empresa_cartao_cnpj') }}"/>
                                                    <div class="progress">
                                                        <p>Escolha um arquivo</p>
                                                        <div class="bar"></div>
                                                    </div>
                                                    <span><i class="icon fa-upload"></i></span>
                                                </label>

                                                @if($errors->has('empresa_cartao_cnpj'))
                                                    <p class="text-danger">{{ $errors->first('empresa_cartao_cnpj') }}</p>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="col-xs-12 col-sm-4">

                                            <div class="form-group {{ ($errors->has('empresa_num_iptu')) ? 'error':'' }}">
                                                <label for="empresa_num_iptu" class="required">Número do IPTU</label>
                                                <input type="text" name="empresa_num_iptu"
                                                       id="empresa_num_iptu"
                                                       class="form-control"
                                                       value=""/>

                                                @if($errors->has('empresa_num_iptu'))
                                                    <p class="text-danger">{{ $errors->first('empresa_num_iptu') }}</p>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="col-xs-12 col-sm-4">

                                            <div class="form-group {{ ($errors->has('empresa_iptu')) ? 'error':'' }}">
                                                <label for="empresa_iptu" class="required">Cópia do IPTU</label>
                                                <label for="empresa_iptu" class="btn-upload">
                                                    <input type="hidden" name="empresa_iptu_enviado" id="empresa_iptu_enviado"/>
                                                    <input type="file" name="empresa_iptu" id="empresa_iptu"
                                                           class="form-control file" value="{{ old('empresa_iptu') }}"/>
                                                    <div class="progress">
                                                        <p>Escolha um arquivo</p>
                                                        <div class="bar"></div>
                                                    </div>
                                                    <span><i class="icon fa-upload"></i></span>
                                                </label>

                                                @if($errors->has('empresa_iptu'))
                                                    <p class="text-danger">{{ $errors->first('empresa_iptu') }}</p>
                                                @endif
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="razao_social" class="required">Razão social</label>
                                                <input type="text" name="razao_social" id="razao_social"
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="nome_fantasia" class="required">Nome fantasia</label>
                                                <input type="text" name="nome_fantasia" id="nome_fantasia"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="cep" class="required">CEP</label>
                                                <input type="text" name="cep" id="cep" class="form-control" maxlength="10">
                                            </div>
                                        </div>

                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label for="endereco" class="required">Endereço</label>
                                                <input type="text" name="endereco" id="endereco" class="form-control" maxlength="255">
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="numero" class="required">Número</label>
                                                <input type="text" name="numero" id="numero" class="form-control" maxlength="5" >
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="bairro" class="required">Bairro</label>
                                                <input type="text" name="bairro" id="bairro" class="form-control" maxlength="100">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-2" style="padding-right:0">
                                            <div class="form-group">
                                                <label for="estado" class="required">Estado</label>
                                                {{ Form::select('estado',$estados,null, ['class' => 'form-control estados', 'id' => 'estado']) }}
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="cidade" class="required">Cidade</label>
                                                {{ Form::select('cidade', [], null, ['class' => 'form-control cidades', 'id' => 'cidade']) }}
                                                
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="complemento">Complemento</label>
                                                <input type="text" name="complemento" id="complemento"
                                                       class="form-control" maxlength="100">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="ponto_referencia">Ponto de referência</label>
                                                <input type="text" name="ponto_referencia" id="ponto_referencia"
                                                       class="form-control" maxlength="255">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="licenca_municipal"
                                                       title="Cadastro de contribuintes mobiliários"
                                                       class="required">CCM</label>
                                                <input type="text" name="licenca_municipal" id="licenca_municipal"
                                                       class="form-control" maxlength="15">
                                            </div>
                                        </div>

                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label for="inscricao_estadual">Inscrição estadual (deixe em branco
                                                    caso seja
                                                    isento)</label>
                                                <input type="text" name="inscricao_estadual" id="inscricao_estadual"
                                                       maxlength="15" class="form-control" maxlength="15"/>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="telefone" class="required">Telefone</label>
                                                <input type="tel" name="telefone" id="telefone" class="form-control" maxlength="50">
                                            </div>
                                        </div>

                                    </div> 

                                    <div class="row">

                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label for="ramo_atividade" class="required">Ramo de atividade</label>
                                                {{ Form::select('ramo_atividade', $ramoAtividades, null, ['class' => 'form-control', 'id' => 'ramo_atividade']) }}
                                            </div>
                                        </div>

                                        <div class="col-sm-1 nopadding">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <a href="javascript:void(0);" class="form-control information">i
                                                    <div class="information__text">
                                                        <p class="text-center">Neste campo, você deve informar o ramo de atividade onde seu empredimento atua:</p> 

                                                        <p><b>Gerador de Resíduos:</b> 
                                                        industria, serviço e comércio, orgao publico, serviço de saude, condominio misto.</p>

                                                        <p><b>Fornecedor de Serviço:</b> 
                                                        cooperativa ambiental, transporte comercio e tratamento de resíduos, destino final de resíduos.</p>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="tipo_atividade" class="required">Tipo</label>                                                
                                                {{ Form::select('tipo_atividade', $tipoAtividades, null, ['class' => 'form-control', 'id' => 'tipo_atividade']) }}
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="flex justify-content-end">
                                                <a href="#!" class="btn btnSubmit btnValidaPrimeiraEtapa btnProsseg">
                                                    Prosseguir
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            @include('cadastro.responsavel')

                            <!-- GERADOR -->
                            @include('cadastro.informacoes-complementares')

                            <!-- CONDOMINIO MISTO -->
                            @include('cadastro.condominio')

                            <!-- TRANSPORTADOR -->
                            @include('cadastro.socio')
                            @include('cadastro.transportador')

                            <!-- COOPERATIVA -->
                            @include('cadastro.responsavel-cooperativa')
                            @include('cadastro.cooperativa')

                            <!-- TERMOS DE USO -->
                            @include('cadastro.termos-de-uso')

                            {{ Form::close() }}
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    @include("cadastro.modal.aviso-servico-de-saude")
@endsection()