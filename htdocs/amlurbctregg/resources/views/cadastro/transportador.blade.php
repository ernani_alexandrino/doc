{{--Cadastro do transportador--}}
<div class="containerTransportador setup-content" id="tela-4tr" style="display: none;">

    <div class="row">
        <div class="col-xs-12">
            <p>Nesta tela você envia a documentação financeira, jurídica, fiscal e técnica. Para
                enviá-las, clique no botão verde de cada campo para abrir a janela de seleção
                de arquivos do seu computador. Selecione o arquivo desejado e clique no
                botão “Enviar”. Tenha em mãos os arquivos nos formatos PDF, JPG, BMP ou
                PNG. O limite máximo de tamanho é de 3MB.
                </p>
            <p>Obs.: o nome dos arquivos não pode conter caracteres especiais, acentos,
                cedilhas, espaços ou barras. Evite erros renomeando os arquivos antes de
                clicar no botão de envio.</p>
            <h2>Passo 3 de 5. Documentação</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <br/>

            <h2 class="margin-top-40">Capacidade Jurídica</h2>

            <div class="box-cadastro">

                <div id="ajax-panel"></div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('empresa_cartao_cnpj')) ? 'error':'' }}">
                            <label for="empresa_cartao_cnpj">Cartão CNPJ (<span class="cnpj_digitado" style="font-size: 16px;"></span>)</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_contrato_social')) ? 'error':'' }}">
                            <label for="transportador_contrato_social" class="remove-required required">Contrato Social (limite máximo do arquivo é 10MB)</label>

                            <label for="transportador_contrato_social" class="btn-upload">
                                <input type="hidden" name="transportador_contrato_social_enviado"
                                       id="transportador_contrato_social_enviado"/>
                                <input type="file" name="transportador_contrato_social" id="transportador_contrato_social"
                                       class="form-control file" value="{{ old('transportador_contrato_social') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_contrato_social'))
                                <p class="text-danger">{{ $errors->first('transportador_contrato_social') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="transportador_doc_ccm" class="remove-required required">CCM (<span id="ccm_digitado" style="font-size: 16px;"></span>)</label>

                            <label for="transportador_doc_ccm" class="btn-upload">
                                <input type="hidden" name="transportador_doc_ccm_enviado"
                                       id="transportador_doc_ccm_enviado"/>
                                <input type="file" name="transportador_doc_ccm" id="transportador_doc_ccm"
                                       class="form-control file" value=""/>
                                <!-- <input type="file" name="transportador_doc_ccm" id="transportador_doc_ccm"
                                       class="form-control file" value="{{ old('transportador_doc_ccm') }}"/> -->
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            <!-- @if($errors->has('transportador_ccm_digitado'))
                                <p class="text-danger">{{ $errors->first('transportador_ccm_digitado') }}</p>
                            @endif -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="diviser"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <h2>Identidade Financeira</h2>

            <div class="box-cadastro">

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_certidao_falencia_concordata')) ? 'error':'' }}">
                            <label for="transportador_certidao_falencia_concordata" class="remove-required required">Certidão Negativa de Falência</label>

                            <label for="transportador_certidao_falencia_concordata" class="btn-upload">
                                <input type="hidden" name="transportador_certidao_falencia_concordata_enviado"
                                       id="transportador_certidao_falencia_concordata_enviado"/>
                                <input type="file" name="transportador_certidao_falencia_concordata"
                                       id="transportador_certidao_falencia_concordata" class="form-control file"
                                       value="{{ old('transportador_certidao_falencia_concordata') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_certidao_falencia_concordata'))
                                <p class="text-danger">{{ $errors->first('transportador_certidao_falencia_concordata') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_balanco_financeiro')) ? 'error':'' }}">
                            <label for="transportador_balanco_financeiro" class="remove-required required">Balanço Patrimonial</label>

                            <label for="transportador_balanco_financeiro" class="btn-upload">
                                <input type="hidden" name="transportador_balanco_financeiro_enviado"
                                id="transportador_balanco_financeiro_enviado"/>
                                <input type="file" name="transportador_balanco_financeiro"
                                       id="transportador_balanco_financeiro" class="form-control file"
                                       value="{{ old('transportador_balanco_financeiro') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_balanco_financeiro'))
                                <p class="text-danger">{{ $errors->first('transportador_balanco_financeiro') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_demonstrativo_contabil')) ? 'error':'' }}">
                            <label for="transportador_demonstrativo_contabil" class="remove-required required">Demonstrativo Contábil do Último
                                Exercício</label>

                            <label for="transportador_demonstrativo_contabil" class="btn-upload">
                                <input type="hidden" name="transportador_demonstrativo_contabil_enviado"
                                id="transportador_demonstrativo_contabil_enviado"/>
                                <input type="file" name="transportador_demonstrativo_contabil"
                                       id="transportador_demonstrativo_contabil" class="form-control file"
                                       value="{{ old('transportador_demonstrativo_contabil') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_demonstrativo_contabil'))
                                <p class="text-danger">{{ $errors->first('transportador_demonstrativo_contabil') }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="diviser"></div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <h2>Regularidade Fiscal</h2>

            <div class="box-cadastro">

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_certidao_negativa_municipal')) ? 'error':'' }}">
                            <label for="transportador_certidao_negativa_municipal" class="remove-required required">Certidão
                                Negativa Municipal</label>

                            <label for="transportador_certidao_negativa_municipal" class="btn-upload">
                                <input type="hidden" name="transportador_certidao_negativa_municipal_enviado"
                                       id="transportador_certidao_negativa_municipal_enviado"/>
                                <input type="file" name="transportador_certidao_negativa_municipal"
                                       id="transportador_certidao_negativa_municipal" class="form-control file"
                                       value="{{ old('transportador_certidao_negativa_municipal') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_certidao_negativa_municipal'))
                                <p class="text-danger">{{ $errors->first('transportador_certidao_negativa_municipal') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_certidao_negativa_estadual')) ? 'error':'' }}">
                            <label for="transportador_certidao_negativa_estadual" class="remove-required required">Certidão
                                Negativa Estadual</label>

                            <label for="transportador_certidao_negativa_estadual" class="btn-upload">
                                <input type="hidden" name="transportador_certidao_negativa_estadual_enviado"
                                       id="transportador_certidao_negativa_estadual_enviado"/>
                                <input type="file" name="transportador_certidao_negativa_estadual"
                                       id="transportador_certidao_negativa_estadual" class="form-control file"
                                       value="{{ old('transportador_certidao_negativa_estadual') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_certidao_negativa_estadual'))
                                <p class="text-danger">{{ $errors->first('transportador_certidao_negativa_estadual') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_certidao_negativa_federal')) ? 'error':'' }}">
                            <label for="transportador_certidao_negativa_federal" class="remove-required required">Certidão
                                Negativa Federal</label>

                            <label for="transportador_certidao_negativa_federal" class="btn-upload">
                                <input type="hidden" name="transportador_certidao_negativa_federal_enviado"
                                       id="transportador_certidao_negativa_federal_enviado"/>
                                <input type="file" name="transportador_certidao_negativa_federal"
                                       id="transportador_certidao_negativa_federal" class="form-control file"
                                       value="{{ old('transportador_certidao_negativa_federal') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_certidao_negativa_federal'))
                                <p class="text-danger">{{ $errors->first('transportador_certidao_negativa_federal') }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_crf_caixa')) ? 'error':'' }}">
                            <label for="transportador_crf_caixa" class="remove-required required">Certificado de Regularidade Fiscal da Caixa</label>

                            <label for="transportador_crf_caixa" class="btn-upload">
                                <input type="hidden" name="transportador_crf_caixa_enviado"
                                       id="transportador_crf_caixa_enviado"/>
                                <input type="file" name="transportador_crf_caixa"
                                       id="transportador_crf_caixa" class="form-control file"
                                       value="{{ old('transportador_crf_caixa') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_crf_caixa'))
                                <p class="text-danger">{{ $errors->first('transportador_crf_caixa') }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="diviser"></div>
                </div>

            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">

            <h2>Capacidade Técnica</h2>

            <div class="box-cadastro">

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_anotacao_responsabilidade_tecnica')) ? 'error':'' }}">
                            <label for="transportador_anotacao_responsabilidade_tecnica" class="remove-required required">ART
                                (Anotação de Responsabilidade Técnica)</label>

                            <label for="transportador_anotacao_responsabilidade_tecnica" class="btn-upload">
                                <input type="hidden" name="transportador_anotacao_responsabilidade_tecnica_enviado"
                                       id="transportador_anotacao_responsabilidade_tecnica_enviado"/>
                                <input type="file" name="transportador_anotacao_responsabilidade_tecnica"
                                       id="transportador_anotacao_responsabilidade_tecnica" class="form-control file"
                                       value="{{ old('transportador_anotacao_responsabilidade_tecnica') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_anotacao_responsabilidade_tecnica'))
                                <p class="text-danger">{{ $errors->first('transportador_anotacao_responsabilidade_tecnica') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_anotacao_responsabilidade_tecnica_emissao')) ? 'error':'' }}">
                                    <label for="transportador_anotacao_responsabilidade_tecnica_emissao" class="remove-required required">Data
                                        de
                                        Emissão</label>
                                    <input type="text" name="transportador_anotacao_responsabilidade_tecnica_emissao"
                                           id="transportador_anotacao_responsabilidade_tecnica_emissao"
                                           class="form-control calendario"
                                           value="{{ old('transportador_anotacao_responsabilidade_tecnica_emissao') }}"/>

                                    @if($errors->has('transportador_anotacao_responsabilidade_tecnica_emissao'))
                                        <p class="text-danger">{{ $errors->first('transportador_anotacao_responsabilidade_tecnica_emissao') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_anotacao_responsabilidade_tecnica_vencimento')) ? 'error':'' }}">
                                    <label for="transportador_anotacao_responsabilidade_tecnica_vencimento">Data de
                                        Vencimento</label>
                                    <input type="text" name="transportador_anotacao_responsabilidade_tecnica_vencimento"
                                           id="transportador_anotacao_responsabilidade_tecnica_vencimento"
                                           class="form-control calendario"
                                           value="{{ old('transportador_anotacao_responsabilidade_tecnica_vencimento') }}"/>

                                    @if($errors->has('transportador_anotacao_responsabilidade_tecnica_vencimento'))
                                        <p class="text-danger">{{ $errors->first('transportador_anotacao_responsabilidade_tecnica_vencimento') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3 col-md-4">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                               name="transportador_anotacao_responsabilidade_tecnica_vencimento_indeterminada"
                                               value="transportador_anotacao_responsabilidade_tecnica_vencimento"
                                               class="checkVencimento"
                                               id="transportador_anotacao_responsabilidade_tecnica_vencimento_indeterminada"/>
                                        <label for="transportador_anotacao_responsabilidade_tecnica_vencimento_indeterminada"
                                               class="dataIndeterminada"> Data de vencimento indeterminada
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">

                        <div class="form-group {{ ($errors->has('transportador_rg_responsavel_tecnico')) ? 'error':'' }}">
                            <label for="transportador_rg_responsavel_tecnico" class="remove-required required">Cópia do RG do Responsável Técnico</label>

                            <label for="transportador_rg_responsavel_tecnico" class="btn-upload">
                                <input type="hidden" name="transportador_rg_responsavel_tecnico_enviado"
                                       id="transportador_rg_responsavel_tecnico_enviado"/>
                                <input type="file" name="transportador_rg_responsavel_tecnico"
                                       id="transportador_rg_responsavel_tecnico" class="form-control file"
                                       value="{{ old('transportador_rg_responsavel_tecnico') }}"/>

                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_rg_responsavel_tecnico'))
                                <p class="text-danger">{{ $errors->first('transportador_rg_responsavel_tecnico') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">

                                <div class="form-group {{ ($errors->has('transportador_rg_num_responsavel_tecnico')) ? 'error':'' }}">
                                    <label for="transportador_rg_num_responsavel_tecnico" class="remove-required required">N° do RG do Resp. Técnico</label>
                                    <input type="text" name="transportador_rg_num_responsavel_tecnico"
                                           id="transportador_rg_num_responsavel_tecnico"
                                           class="form-control"
                                           value=""/>

                                    @if($errors->has('transportador_rg_num_responsavel_tecnico'))
                                        <p class="text-danger">{{ $errors->first('transportador_rg_num_responsavel_tecnico') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_nome_responsavel_tecnico')) ? 'error':'' }}">
                                    <label for="transportador_nome_responsavel_tecnico" class="remove-required required">Nome do Resp. Técnico</label>
                                    <input type="text" name="transportador_nome_responsavel_tecnico"
                                           id="transportador_nome_responsavel_tecnico"
                                           class="form-control"
                                           value=""/>

                                    <!-- @if($errors->has('transportador_nome_responsavel_tecnico'))
                                        <p class="text-danger">{{ $errors->first('transportador_nome_responsavel_tecnico') }}</p>
                                    @endif -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">

                        <div class="form-group {{ ($errors->has('transportador_cpf_responsavel_tecnico')) ? 'error':'' }}">
                            <label for="transportador_cpf_responsavel_tecnico" class="remove-required required">Cópia do CPF do Responsável Técnico</label>

                            <label for="transportador_cpf_responsavel_tecnico" class="btn-upload">
                                <input type="hidden" name="transportador_cpf_responsavel_tecnico_enviado"
                                       id="transportador_cpf_responsavel_tecnico_enviado"/>
                                <input type="file" name="transportador_cpf_responsavel_tecnico"
                                       id="transportador_cpf_responsavel_tecnico" class="form-control file"
                                       value="{{ old('transportador_cpf_responsavel_tecnico') }}"/>

                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_cpf_responsavel_tecnico'))
                                <p class="text-danger">{{ $errors->first('transportador_cpf_responsavel_tecnico') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">

                                <div class="form-group {{ ($errors->has('transportador_cpf_num_responsavel_tecnico')) ? 'error':'' }}">
                                    <label for="transportador_cpf_num_responsavel_tecnico" class="remove-required required">N° do CPF do Resp. Técnico</label>
                                    <input type="text" name="transportador_cpf_num_responsavel_tecnico"
                                           id="transportador_cpf_num_responsavel_tecnico"
                                           class="form-control"
                                           value=""
                                           />

                                    <!-- @if($errors->has('transportador_cpf_num_responsavel_tecnico'))
                                        <p class="text-danger">{{ $errors->first('transportador_cpf_num_responsavel_tecnico') }}</p>
                                    @endif -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">

                        <div class="form-group {{ ($errors->has('transportador_crea_responsavel_tecnico')) ? 'error':'' }}">
                            <label for="transportador_crea_responsavel_tecnico" class="remove-required required">Cópia do CREA do Responsável Técnico</label>

                            <label for="transportador_crea_responsavel_tecnico" class="btn-upload">
                                <input type="hidden" name="transportador_crea_responsavel_tecnico_enviado"
                                       id="transportador_crea_responsavel_tecnico_enviado"/>
                                <input type="file" name="transportador_crea_responsavel_tecnico"
                                       id="transportador_crea_responsavel_tecnico" class="form-control file"
                                       value="{{ old('transportador_crea_responsavel_tecnico') }}"/>

                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_crea_responsavel_tecnico'))
                                <p class="text-danger">{{ $errors->first('transportador_crea_responsavel_tecnico') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">

                                <div class="form-group {{ ($errors->has('transportador_crea_num_responsavel_tecnico')) ? 'error':'' }}">
                                    <label for="transportador_crea_num_responsavel_tecnico" class="remove-required required">N° do CREA do Resp. Técnico</label>
                                    <input type="text" name="transportador_crea_num_responsavel_tecnico"
                                           id="transportador_crea_num_responsavel_tecnico"
                                           class="form-control"
                                           value=""
                                           />

                                    <!-- @if($errors->has('transportador_crea_num_responsavel_tecnico'))
                                        <p class="text-danger">{{ $errors->first('transportador_crea_num_responsavel_tecnico') }}</p>
                                    @endif -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_certificado_dispensa_licenca')) ? 'error':'' }}">
                            <label for="transportador_certificado_dispensa_licenca">CDL (Certificado de Dispensa de
                                Licença)</label>

                            <label for="transportador_certificado_dispensa_licenca" class="btn-upload">
                                <input type="hidden" name="transportador_certificado_dispensa_licenca_enviado"
                                       id="transportador_certificado_dispensa_licenca_enviado"/>
                                <input type="file" name="transportador_certificado_dispensa_licenca"
                                       id="transportador_certificado_dispensa_licenca" class="form-control file"
                                       value="{{ old('transportador_certificado_dispensa_licenca') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_certificado_dispensa_licenca'))
                                <p class="text-danger">{{ $errors->first('transportador_certificado_dispensa_licenca') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_certificado_dispensa_licenca_emissao')) ? 'error':'' }}">
                                    <label for="transportador_certificado_dispensa_licenca_emissao">Data de
                                        Emissão</label>
                                    <input type="text" name="transportador_certificado_dispensa_licenca_emissao"
                                           id="transportador_certificado_dispensa_licenca_emissao"
                                           class="form-control calendario"
                                           value="{{ old('transportador_certificado_dispensa_licenca_emissao') }}"/>

                                    @if($errors->has('transportador_certificado_dispensa_licenca_emissao'))
                                        <p class="text-danger">{{ $errors->first('transportador_certificado_dispensa_licenca_emissao') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_certificado_dispensa_licenca_vencimento')) ? 'error':'' }}">
                                    <label for="transportador_certificado_dispensa_licenca_vencimento">Data de
                                        Vencimento</label>
                                    <input type="text" name="transportador_certificado_dispensa_licenca_vencimento"
                                           id="transportador_certificado_dispensa_licenca_vencimento"
                                           class="form-control calendario"
                                           value="{{ old('transportador_certificado_dispensa_licenca_vencimento') }}"/>

                                    @if($errors->has('transportador_certificado_dispensa_licenca_vencimento'))
                                        <p class="text-danger">{{ $errors->first('transportador_certificado_dispensa_licenca_vencimento') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3 col-md-4">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                               name="transportador_certificado_dispensa_licenca_vencimento_indeterminada"
                                               value="transportador_certificado_dispensa_licenca_vencimento"
                                               class="checkVencimento"
                                               id="transportador_certificado_dispensa_licenca_vencimento_indeterminada"/>
                                        <label for="transportador_certificado_dispensa_licenca_vencimento_indeterminada">
                                            Data de
                                            vencimento indeterminada
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group {{ ($errors->has('licencadefault')) ? 'error':'' }}">
                            <label for="licencadefault">Licença </label>

                            <select class="form-control" id="licencadefault" name="licencadefault" style="color: #444" >
                                <option value="">Selecione apenas uma</option>
                                <option value="17">Licença Prévia</option>
                                <option value="18">Licença de Instalação</option>
                                <option value="19">Licença de Operação</option>
                            </select>
                            @if($errors->has('licencadefault'))
                                <p class="text-danger">{{ $errors->first('licencadefault') }}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('licenka')) ? 'error':'' }}">
                            <label for="licenka">Licença</label>

                            <label for="licenka" class="btn-upload">
                                <input type="hidden" name="licenka_enviado"
                                       id="licenka_enviado"/>
                                <input type="file" name="licenka" id="licenka"
                                       class="form-control file" value="{{ old('licenka') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('licenka'))
                                <p class="text-danger">{{ $errors->first('licenka') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('emissaolicenca')) ? 'error':'' }}">
                                    <label for="emissaolicenca">Data de Emissão</label>
                                    <input type="text" name="emissaolicenca"
                                           id="emissaolicenca"
                                           class="form-control calendario"
                                           value="{{ old('emissaolicenca') }}"/>

                                    @if($errors->has('emissaolicenca'))
                                        <p class="text-danger">{{ $errors->first('emissaolicenca') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('licencavencimento')) ? 'error':'' }}">
                                    <label for="licencavencimento">Data de Vencimento</label>
                                    <input type="text" name="licencavencimento"
                                           id="licencavencimento" class="form-control calendario"
                                           value="{{ old('licencavencimento') }}"/>

                                    @if($errors->has('licencavencimento'))
                                        <p class="text-danger">{{ $errors->first('licencavencimento') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3 col-md-4">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                               name="licencavencimento_indeterminada"
                                               value="licencavencimento" class="checkVencimento"
                                               id="licencavencimento_indeterminada"/>
                                        <label for="licencavencimento_indeterminada"> Data de
                                            vencimento indeterminada
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_alvara_prefeitura')) ? 'error':'' }}">
                            <label for="transportador_alvara_prefeitura" class="remove-required required">Alvará Prefeitura</label>

                            <label for="transportador_alvara_prefeitura" class="btn-upload">
                                <input type="hidden" name="transportador_alvara_prefeitura_enviado"
                                       id="transportador_alvara_prefeitura_enviado"/>
                                <input type="file" name="transportador_alvara_prefeitura"
                                       id="transportador_alvara_prefeitura" class="form-control file"
                                       value="{{ old('transportador_alvara_prefeitura') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_alvara_prefeitura'))
                                <p class="text-danger">{{ $errors->first('transportador_alvara_prefeitura') }}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_alvara_prefeitura_emissao')) ? 'error':'' }}">
                                    <label for="transportador_alvara_prefeitura_emissao" class="remove-required required">Data de
                                        Emissão</label>
                                    <input type="text" name="transportador_alvara_prefeitura_emissao"
                                           id="transportador_alvara_prefeitura_emissao" class="form-control calendario"
                                           value="{{ old('transportador_alvara_prefeitura_emissao') }}"/>

                                    @if($errors->has('transportador_alvara_prefeitura_emissao'))
                                        <p class="text-danger">{{ $errors->first('transportador_alvara_prefeitura_emissao') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_alvara_prefeitura_vencimento')) ? 'error':'' }}">
                                    <label for="transportador_alvara_prefeitura_vencimento">Data de Vencimento</label>
                                    <input type="text" name="transportador_alvara_prefeitura_vencimento"
                                           id="transportador_alvara_prefeitura_vencimento" class="form-control calendario"
                                           value="{{ old('transportador_alvara_prefeitura_vencimento') }}"/>

                                    @if($errors->has('transportador_alvara_prefeitura_vencimento'))
                                        <p class="text-danger">{{ $errors->first('transportador_alvara_prefeitura_vencimento') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3 col-md-4">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                               name="transportador_alvara_prefeitura_vencimento_indeterminada"
                                               value="transportador_alvara_prefeitura_vencimento"
                                               class="checkVencimento"
                                               id="transportador_alvara_prefeitura_vencimento_indeterminada"/>
                                        <label for="transportador_alvara_prefeitura_vencimento_indeterminada"> Data de
                                            vencimento
                                            indeterminada
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group">
                                {{-- <div class="form-group {{ ($errors->has('transportador_ctf_ibama')) ? 'error':'' }}"> --}}
                            <label for="transportador_ctf_ibama">CTF Ibama</label>

                            <label for="transportador_ctf_ibama" class="btn-upload">
                                <input type="hidden" name="transportador_ctf_ibama_enviado"
                                       id="transportador_ctf_ibama_enviado" 
                                       />
                                <input type="file" name="transportador_ctf_ibama"
                                       id="transportador_ctf_ibama" 
                                       class="form-control file"
                                       value="{{ old('transportador_ctf_ibama') }}"
                                       />
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            {{-- @if($errors->has('transportador_ctf_ibama'))
                                <p class="text-danger">{{ $errors->first('transportador_ctf_ibama') }}</p>
                            @endif --}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <label for="transportador_ctf_ibama_emissao">Data de Emissão</label>
                                    <input type="text"
                                     name="transportador_ctf_ibama_emissao" 
                                    id="transportador_ctf_ibama_emissao"
                                           class="form-control calendario"
                                           value="{{ old('transportador_ctf_ibama_emissao') }}"/>

                                    @if($errors->has('transportador_ctf_ibama_emissao'))
                                        <p class="text-danger">{{ $errors->first('transportador_ctf_ibama_emissao') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_ctf_ibama_vencimento')) ? 'error':'' }}">
                                    <label for="transportador_ctf_ibama_vencimento">Data de Vencimento</label>
                                    <input type="text"
                                    name="transportador_ctf_ibama_vencimento"
                                    id="transportador_ctf_ibama_vencimento"
                                           class="form-control calendario"
                                           value="{{ old('transportador_ctf_ibama_vencimento') }}"/>

                                    @if($errors->has('transportador_ctf_ibama_vencimento'))
                                        <p class="text-danger">{{ $errors->first('transportador_ctf_ibama_vencimento') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3 col-md-4">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                               name="transportador_ctf_ibama_vencimento_indeterminada"
                                               value="transportador_ctf_ibama_vencimento"
                                               class="checkVencimento"
                                               id="transportador_ctf_ibama_vencimento_indeterminada"/>
                                        <label for="transportador_ctf_ibama_vencimento_indeterminada"> Data de vencimento
                                            indeterminada
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->has('transportador_avcb')) ? 'error':'' }}">
                            <label for="transportador_avcb" class="remove-required required">AVCB</label>

                            <label for="transportador_avcb" class="btn-upload">
                                <input type="hidden" name="transportador_avcb_enviado" id="transportador_avcb_enviado"/>
                                <input type="file" name="transportador_avcb"
                                       id="transportador_avcb" class="form-control file" value="{{ old('transportador_avcb') }}"/>
                                <div class="progress">
                                    <p>Escolha um arquivo</p>
                                    <div class="bar"></div>
                                </div>
                                <span><i class="icon fa-upload"></i></span>
                            </label>

                            @if($errors->has('transportador_avcb'))
                                <p class="text-danger">{{ $errors->first('transportador_avcb') }}</p>
                            @endif
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_avcb_emissao')) ? 'error':'' }}">
                                    <label for="transportador_avcb_emissao" class="remove-required required">Data de Emissão</label>
                                    <input type="text" name="transportador_avcb_emissao" id="transportador_avcb_emissao"
                                           class="form-control calendario"
                                           value="{{ old('transportador_avcb_emissao') }}"/>

                                    @if($errors->has('transportador_avcb_emissao'))
                                        <p class="text-danger">{{ $errors->first('transportador_avcb_emissao') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group {{ ($errors->has('transportador_avcb_vencimento')) ? 'error':'' }}">
                                    <label for="transportador_avcb_vencimento">Data de Vencimento</label>
                                    <input type="text" name="transportador_avcb_vencimento" id="transportador_avcb_vencimento"
                                           class="form-control calendario"
                                           value="{{ old('transportador_avcb_vencimento') }}"/>

                                    @if($errors->has('transportador_avcb_vencimento'))
                                        <p class="text-danger">{{ $errors->first('transportador_avcb_vencimento') }}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xs-3 col-md-4">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox"
                                               name="transportador_avcb_vencimento_indeterminada"
                                               value="transportador_avcb_vencimento"
                                               class="checkVencimento"
                                               id="transportador_avcb_vencimento_indeterminada"/>
                                        <label for="transportador_avcb_vencimento_indeterminada"> Data de vencimento
                                            indeterminada
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="diviser"></div>

        <div class="col-xs-12">
            <div class="msg"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="flex justify-content-end">
                <a href="#tela-2" type="button" class="btn prevBtn btnVoltar"
                   title="Voltar para página anterior">
                    Voltar
                </a>
                <input type="button" class="btn btnSubmit btnValidaTransportador btnProsseg"
                   value="Prosseguir">
                
                
            </div>
        </div>
    </div>

</div>
