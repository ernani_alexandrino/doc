
<div class="containerCooperativa setup-content" id="tela-4co" style="display: none;">

    <div class="row">
        <div class="col-xs-12">
            <h2>4. Documentação</h2>
            <p>Aqui você envia a documentação de suas licenças e certidões. Para enviá-las, clique no botão verde de
                cada campo pra abrir a janela de seleção de arquivos do seu computador. Selecione o arquivo desejado e
                clique bo botão Enviar. Use os formatos PDF, JPG, BMP ou PNG. Para arquivos de contrato social, o limite
                máximo de tamanho é 10MB. Demais documentos devem ter, no máximo, 3MB por arquivo.</p>
            <p>Obs.: o nome dos arquivos não podem conter caracteres especiais, acentos, cedilhas, espaços ou barras.
                Evite erros de envio renomeando os arquivos antes de clicar no botão de envio.</p>
        </div>
    </div>

    <div class="row">

        <!--
        TODO replicar dados de documentos do Transportador
        TODO porem sem obrigatoriedade de envio de nenhum doc
         -->

    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="flex justify-content-end">
                <a href="#!" type="button" class="btn prevBtn btnVoltar"
                   title="Voltar para página anterior">
                    Voltar
                </a>
                <a href="#!" type="button" class="btn btnSubmit btnValidaCooperativa btnProsseg"
                   title="Prosseguir">
                    Prosseguir
                </a>
            </div>
        </div>
    </div>

</div>
