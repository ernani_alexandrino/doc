<div class="containerSocio2 setup-content" id="tela-3co" style="display: none;">
        <div class="row">
            <div class="col-xs-12">
    
                <p>Nesta tela você deve informar os dados do(s) responsável(is) pelo empreendimento. Preencha os campos com nome completo, número do documento e cópia escaneada do RG ou CPF.</p>
                <h2>Passo 2.2 de 5. Responsável pelo Empreendimento</h2>
            </div>
        </div>
     <div class="row">
            <div id="ajax-panel"></div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group  {{ ($errors->has('nome_socio')) ? 'error':'' }}">
                    <label for="nome_socio" class="required">Nome do Presidente</label>
                    <input type="text" name="nome_socio" id="nome_socio" class="form-control">
                    <input type="hidden" name="presidente"  value ="cooperativa" class="form-control">
                </div>
    
                @if($errors->has('nome_presidente'))
                <p class="text-danger">{{ $errors->first('nome_presidente') }}</p>
                @endif
            </div>

            <div class="col-xs-12 col-sm-4">
                <div class="form-group {{ ($errors->has('numero_rg_socio')) ? 'error':'' }}">
                    <label for="rg" class="required">Número do RG do Presidente</label>
                    <input type="text" name="numero_rg_socio" id="numero_rg_socio" placeholder="Número do RG do Presidente"
                           value="{{ old('numero_rg_presidente') }}" name="numero_rg_socio" id="numero_rg_socio" class="form-control required rg" style="margin-bottom: 10px;">
                    <label for="rg" class="required">Cópia do RG do Presidente</label>
                    <label for="numero_rg_presidente_file" class="btn-upload">
                        <input type="hidden" value="{{ old('numero_rg_presidente_file_enviado') }}" name="numero_rg_presidente_file_enviado"
                               id="numero_rg_presidente_file_enviado" />
                        <input type="file" name="numero_rg_presidente_file" id="numero_rg_presidente_file" class="form-control file"
                               value="{{ old('numero_rg_presidente_file') }}" />
                        <div class="progress">
                            <p>Escolha um arquivo</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>
                    @if($errors->has('numero_rg_socio_file'))
                    <p class="text-danger">{{ $errors->first('numero_rg_socio_file') }}</p>
                    @endif
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="form-group {{ ($errors->has('numero_cpf_socio')) ? 'error':'' }}">
                    <label for="rg" class="required">Número do CPF do Presidente</label>
                    <input type="text" placeholder="Número do CPF do Sócio" value="{{ old('numero_cpf_socio') }}" name="numero_cpf_socio" id="numero_cpf_socio" class="form-control required cpf" style="margin-bottom: 10px;">
                    <label for="rg" class="required">Cópia do CPF do Presidente</label>
                    <label for="numero_cpf_presidente_file" class="btn-upload">
                        <input type="hidden" value="{{ old('numero_cpf_presidente_file_enviado') }}" name="numero_cpf_presidente_file_enviado"
                               id="numero_cpf_presidente_file_enviado" />
                        <input type="file" value="{{ old('numero_cpf_presidente_file') }}" name="numero_cpf_presidente_file" id="numero_cpf_presidente_file" class="form-control file">
                        <div class="progress">
                            <p>Escolha um arquivo</p>
                            <div class="bar"></div>
                        </div>
                        <span><i class="icon fa-upload"></i></span>
                    </label>
    
                    @if($errors->has('numero_cpf_presidente_file'))
                    <p class="text-danger">{{ $errors->first('numero_cpf_presidente_file') }}</p>
                    @endif
                </div>
            </div>
        </div>
     
        <div class="row">
            <div class="col-xs-12">
                <div class="flex justify-content-end">
                    <a href="#tela-2" type="button" class="btn prevBtn btnVoltar" title="Voltar para página anterior">
                            Voltar
                        </a>    
                    <!-- adicionar classe, rota e action para prosseguir nesse passo -->
                    <a href="#tela-4co" type="button" class="btn btnSubmit btnValidaSocio2 btnProsseg" title="Prosseguir">
                            Prosseguir
                        </a>
                </div>
            </div>
        </div>
    
    </div>