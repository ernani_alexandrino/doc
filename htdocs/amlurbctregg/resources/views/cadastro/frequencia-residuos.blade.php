{{--Cadastro do gerador--}}
<div class="containerFrequenciaResiduos setup-content" id="step-3" style="display: none;">

    <div class="row">
        <div class="col-xs-12">
            <h2>3. Frequência de resíduos</h2>
            <p>Informe o volume diário de geração de resíduos de sua empresa e a frequência em que eles são recolhidos.</p>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6">
            <div class="form-group">
                <label for="geracao_diaria" class="required">Geração Diária de
                    Resíduos</label>
                @if($frequenciaGeracao)
                    @foreach($frequenciaGeracao as $dados)
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input geracao_diaria" name="geracao_diaria"
                                       value="{{ $dados['id'] }}"/>
                                {{ $dados['nome'] }}
   
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label for="coleta_diaria" class="required">Frequência de Coleta</label>
                @if($frequenciaColeta)
                    @foreach($frequenciaColeta as $dados)
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="coleta_diaria"
                                       value="{{ $dados['id'] }}"/>
                                {{ $dados['nome'] }}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="flex justify-content-end">
                <a href="#step-2" type="button" class="btn prevBtn btnVoltar" title="Voltar para página anterior">
                    Voltar
                </a>

                {{--Adiciona hfref e classe nextBtn na validação do form--}}
                <a href="#!" class="btn btnSubmit btnValidaFrequenciaResiduo btnProsseg" title="Prosseguir">
                    Prosseguir
                </a>
            </div>
        </div>
    </div>

</div>
