
@extends("layouts.home-painel")

@section('conteudo')

    <section id="cadastro-empresa">

        @include('site.menu-lateral')

        <div class="col-xs-12 col-sm-10">
            <div class="row">
                <div class="conteudo-sucesso">

                    <h1>Cadastro Empresa</h1>

                    <div class="containerSucesso text-center">
                        <div class="row">
                            <div class="col-xs-12">
                                <img src="{{ asset('images/mapa.png') }}" alt="Mapa Amlurb" width="210">
                            </div>
                        </div>

                        <div class="row">

                            @include('cadastro.corpo-sucesso')

                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <a href="{{ url('/') }}" class="btn btnSubmit bt-go-home" title="Ir para home">Voltar para o Site</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection()
