{{--Termos de uso--}}
<div class="containerTermosUso setup-content" id="tela-5" style="display: none;">

    <div class="row">
        <div class="col-xs-12">
            <h2>Termos de uso de política de privacidade</h2>
            <p>
                Ao clicar em “Concluir Cadastro”, você está de acordo com os termos de uso e política de privacidade, assim como declara que as informações contidas em sua autodeclaração à AMLURB são verídicas e de sua inteira responsabilidade, estando a empresa e o responsável pelo cadastro sujeitos às sanções e responsabilidades, de acordo com o ART. 299 do Código Penal – Decreto Lei 2.848/40
                <a href="#" class="link termoDeUso" id="termoDeUso" title="Termos de uso e política de privacidade">Termos de uso e política de privacidade</a>.
                Comprometo-me ainda a atualizar este cadastro quando houver qualquer alteração dos dados apresentados.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="termo-privacidade" style="display: none">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="flex justify-content-end">
                <a href="#!" type="button" class="btn prevBtn btnVoltar" title="Voltar para página anterior">
                    Voltar
                </a>
                <button type="submit" class="btn btnSumit btnValidaTermosDeUso btnProsseg">Concluir cadastro</button>
            </div>
        </div>
    </div>

</div>
