{{--Dados do responsável--}}
<div class="containerResponsavel setup-content" id="tela-2" style="display: none;">

    <div class="row">
        <div class="col-xs-12">
            <p>Preencha abaixo os dados da pessoa que será responsável por acessar o sistema CTR-E em sua organização.</p>
            <h2>Passo 2 de 5. Responsável pelo CTRE</h2>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="responsavel" class="required">Nome Completo do
                    Responsável</label>
                <input type="text" name="responsavel" id="responsavel"
                       class="form-control">
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="email" class="required">E-mail
                    <span>(será usado para fazer login no sistema)</span></label>
                <input type="email" name="email" id="email" class="form-control">
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label for="cargo" class="required">Cargo</label>
                <input type="text" name="cargo" id="cargo" class="form-control">
            </div>
        </div>

        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label for="celular_responsavel" class="required">Celular</label>
                <input type="tel" name="celular_responsavel" id="celular_responsavel"
                       class="form-control">
            </div>
        </div>

        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label for="telefone_responsavel" class="required">Telefone</label>
                <input type="tel" name="telefone_responsavel" id="telefone_responsavel"
                       class="form-control">
            </div>
        </div>

        <div class="col-xs-12 col-sm-3">
            <div class="form-group">
                <label for="ramal">Ramal</label>
                <input type="text" name="ramal" id="ramal" class="form-control"
                       maxlength="15">
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="flex justify-content-end">
                <a href="#tela-1" type="button" class="btn prevBtn btnVoltar" title="Voltar para página anterior">
                    Voltar
                </a>

                {{--Adiciona hfref e classe nextBtn na validação do form--}}
                <a href="#!" type="button" class="btn btnSubmit btnValidaResponsavel btnProsseg"
                   title="Prosseguir">
                    Prosseguir
                </a>
            </div>
        </div>
    </div>

</div>
