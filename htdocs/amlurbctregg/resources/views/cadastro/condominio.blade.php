{{--Cadastro quinta etapa condomínio--}}
<div class="containerCondominioResponsavel setup-content" id="tela-3cm" style="display: none;">

    <div class="row">
        <div class="col-xs-12">
            
            <p>Nesta tela você deve informar os dados do responsável pelo empreendimento.
                Preencha os campos com nome do síndico, número do documento e cópia do
                RG ou CPF, e anexe as cópias da ata da assembleia da eleição para síndico e
                do documento da instituição e constituição do condomínio.</p>
                <h2>Passo 2.2 de 5 Responsável pelo Empreendimento</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h2 class="margin-top-40"  style=" border-bottom: 1px solid #5d5d5d;">Dados do Síndico</h2>
        </div>
    </div>

    <div class="row">
        <div id="ajax-panel"></div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group  {{ ($errors->has('sindico')) ? 'error':'' }}">
                <label for="condominio_sindico" class="required">Nome do Sindico Responsável</label>
                <input type="text" name="condominio_sindico" id="condominio_sindico" class="form-control">
            </div>

            @if($errors->has('sindico'))
                <p class="text-danger">{{ $errors->first('sindico') }}</p>
            @endif
        </div>

        <div class="col-xs-12 col-sm-4">
            <div class="form-group {{ ($errors->has('condominio_file_rg')) ? 'error':'' }}">
                <label for="rg" class="required">RG do Síndico </label>
                <input type="text"  placeholder="Número Rg do Síndico" value="{{ old('sindico_rg') }}" name="condominio_sindico_rg" id="condominio_sindico_rg"
                       class="form-control required rg" style="margin-bottom: 10px;">
                <label for="condominio_file_rg" class="btn-upload">
                    <input type="hidden" value="{{ old('condominio_file_rg_enviado') }}" name="condominio_file_rg_enviado" id="condominio_file_rg_enviado"/>
                    <input type="file" name="condominio_file_rg" id="condominio_file_rg" class="form-control file"  value="{{ old('condominio_file_rg') }}" />
                    <div class="progress">
                        <p>Escolha um arquivo</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>
                @if($errors->has('condominio_file_rg'))
                    <p class="text-danger">{{ $errors->first('condominio_file_rg') }}</p>
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group {{ ($errors->has('condominio_sindico_cpf')) ? 'error':'' }}">
                <label for="rg" class="required">CPF do Síndico </label>
                <input type="text"  placeholder="Número CPF do Síndico" value="{{ old('condominio_sindico_cpf') }}" name="condominio_sindico_cpf"
                       id="condominio_sindico_cpf" class="form-control required cpf" style="margin-bottom: 10px;">
                <label for="condominio_file_cpf" class="btn-upload">
                    <input type="hidden" value="{{ old('condominio_file_cpf_enviado') }}"name="condominio_file_cpf_enviado" id="condominio_file_cpf_enviado"/>
                    <input type="file"  value="{{ old('condominio_file_cpf') }}" name="condominio_file_cpf" id="condominio_file_cpf" class="form-control required file" >
                    <div class="progress">
                        <p>Escolha um arquivo</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>

                @if($errors->has('condominio_file_cpf'))
                    <p class="text-danger">{{ $errors->first('condominio_file_cpf') }}</p>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="diviser" style=" border-bottom: 1px solid #5d5d5d;"></div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h2 class="margin-top-40">Dados do Condomínio</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group {{ ($errors->has('condominio_ata')) ? 'error':'' }}">
                <label for="rg" class="required">Cópia da Ata de Assembléias de eleição do síndico (limite máximo do arquivo é 10MB)</label>
                <label for="condominio_ata" class="btn-upload">
                    <input type="hidden" value="{{ old('condominio_ata_enviado') }}"  name="condominio_ata_enviado" id="condominio_ata_enviado"/>
                    <input type="file"  value="{{ old('condominio_ata') }}" name="condominio_ata" id="condominio_ata" class="form-control required file">
                    <div class="progress">
                        <p>Escolha um arquivo</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>

                @if($errors->has('condominio_ata'))
                    <p class="text-danger">{{ $errors->first('condominio_ata') }}</p>
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group {{ ($errors->has('condominio_contribuicao')) ? 'error':'' }}">
                <label for="rg">Cópia do documento de instituição e contribuição do condomínio (limite máximo do arquivo é 10MB)</label>
                <label for="condominio_contribuicao" class="btn-upload">
                    <input type="hidden" value="{{ old('condominio_contribuicao_enviado') }}" name="condominio_contribuicao_enviado" id="condominio_contribuicao_enviado"/>
                    <input type="file"  value="{{ old('condominio_contribuicao') }}" name="condominio_contribuicao" id="condominio_contribuicao" class="form-control required file">
                    <div class="progress">
                        <p>Escolha um arquivo</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>

                @if($errors->has('condominio_contribuicao'))
                    <p class="text-danger">{{ $errors->first('condominio_contribuicao') }}</p>
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group {{ ($errors->has('condominio_procuracao')) ? 'error':'' }}">
                <label for="rg">Cópia da procuração da administração</label>
                <label for="condominio_procuracao" class="btn-upload">
                    <input type="hidden" value="{{ old('condominio_procuracao_enviado') }}" name="condominio_procuracao_enviado" id="condominio_procuracao_enviado"/>
                    <input type="file"  value="{{ old('condominio_procuracao') }}" name="condominio_procuracao" id="condominio_procuracao" class="form-control required file">
                    <div class="progress">
                        <p>Escolha um arquivo</p>
                        <div class="bar"></div>
                    </div>
                    <span><i class="icon fa-upload"></i></span>
                </label>

                @if($errors->has('condominio_procuracao'))
                    <p class="text-danger">{{ $errors->first('condominio_procuracao') }}</p>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="flex justify-content-end">
                <a href="#tela-2" type="button" class="btn prevBtn btnVoltar" title="Voltar para página anterior">
                    Voltar
                </a>

                <!-- adicionar classe, rota e action para prosseguir nesse passo -->
                <a href="#tela-4g" type="button" class="btn btnSubmit btnValidaCondominio btnProsseg" title="Prosseguir">
                    Prosseguir
                </a>
            </div>
        </div>
    </div>

</div>
