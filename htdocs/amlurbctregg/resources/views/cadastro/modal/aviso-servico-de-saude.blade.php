<div class="modal fade" id="modalAvisoCadastroServicoSaude" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">                   
                <h4 class="modal-title" id="myModalLabel">AVISO Serviço de Saúde</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12 texto-informativo text-center" style="margin-top:0">
                    <p> 
                        O cadastro no sistema CTRE RGG deve ser feito somente considerando os resíduos comuns da Classe II-A (Não Infectantes) e NÃO os resíduos do Classe I (infectantes).
                    </p>
                    <p> 
                        Os volumes de geração de resíduos que devem ser declarados aqui são os volumes de resíduos comuns  unicamente.<br>
                        Clique em OK para continuar ou cancelar para abandonar
                    </p>
                    <span>&nbsp;</span>
                </div>                 
            </div>
            <div class="modal-footer">
                <div class="row">

                    <div class="col-xs-12">
                        <div class="modal-bts-center">
                            
                            <button type="reset" class="btn btn-default btn-danger" 
                            id="cancelarModalAvisoServicoSaude">Cancelar</button>

                            <button type="button" class="btn btn-default btn-success"
                            style="float:none;opacity:1;margin-top: 0;"
                            id="confirmarModalAvisoServicoSaude">Ok</button>
                           
                        </div>
                    </div>

                </div>             
            </div>
        </div>
    </div>
</div>