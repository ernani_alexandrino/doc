<div class="col-xs-12">
    <div class="dadosDaEmpresa">

        <div class="sucesso-cadastro" style="background:#fff;border-radius:15px; padding:15px;width:90%;margin:0 0 0 5%;margin-top: 15px;">
            <div class="col-xs-12">
                <h2> Parabéns, seu cadastrado foi realizado com sucesso!</h2>
            </div>

            <p>
                @if(session('tipoEmpresa') != config('enums.empresas_tipo.pequeno_gerador'))

                    Por meio de sua autodeclaração ao sistema CTR-E, a empresa <strong>{{ session('razao_social') }}</strong>,
                    inscrição municipal {{ session('inscricaoMunicipal') }}, foi registrada em nosso sistema e classificada como
                    <strong>{{ session('tipoCadastro') }}</strong>, de acordo com o DECRETO Nº 58.701.

                @else

                    Por meio de sua autodeclaração ao sistema CTR-E, você foi classificado como
                    <strong>{{ session('tipoCadastro') }}</strong>, de acordo com o DECRETO Nº 58.701.

                @endif
            </p>

            <ul>
                <li>
                    <p>
                        <strong>Categoria AMLURB:</strong>
                        {{ session('tipoCadastro') }}
                    </p>
                </li>
                @if(session('protocolo'))
                    <li>
                        <p>
                            <strong>Protocolo:</strong>
                            {{ session('protocolo') }}
                        </p>
                    </li>
                @endif
                <li>
                    <p>
                        <strong>Código Amlurb:</strong>
                        {{ session('id_limpurb') }}
                    </p>
                </li>
                <li>
                    <p>
                        <strong>Razão social:</strong>
                        {{ session('razaoSocial') }}
                    </p>
                </li>
            </ul>

            @if(session('tipoEmpresa') != config('enums.empresas_tipo.pequeno_gerador'))
                <p>
                    Seu cadastro está agora em análise pela AMLURB. Após validado, você receberá
                    um e-mail com as informações dos próximos passos.
                </p>
            @endif

            <p>
                Para <strong>consultar ou alterar</strong> seu cadastro, acesse
                <a href="https://ctre.com.br" target="_blank">www.ctre.com.br</a>
                e informe seu e-mail {{ session('email') }} e a senha que você irá receber por e-mail.
            </p>

            @if(session('tipoEmpresa') == config('enums.empresas_tipo.pequeno_gerador'))
                Atenção: De acordo com a Resolução 130/AMLURB/2019, seu empreendimento pode ser requisitado a um novo enquadramento
                de Categoria AMLURB, antes mesmo do período de vencimento do seu cadastro.
            @endif


            <p>
                Informamos que 1 ano após a publicação de seu deferimento no Diário Oficial da Cidade e na página da AMLURB você deve <strong>renovar</strong> seu cadastro.
            </p>
            <br>

            <p>
                Caso tenha alguma dúvida, entre em contato conosco pelo telefone 156.<br>
                Atenciosamente,<br>
                Amlurb – Autoridade Municipal de Limpeza Urbana
            </p>

        </div>

    </div>

</div>