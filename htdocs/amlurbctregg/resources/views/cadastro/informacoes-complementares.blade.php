{{--Quarta etapa gerador--}}
<div class="containerInformacoesComplementares setup-content" id="tela-4g" style="display: none;">

    <div class="row">
        <div class="col-xs-12">
            <p>Nesta tela você deve informar o volume diário de geração de resíduos do seu empreendimento e a frequência em que eles são coletados.</p>
            <p>Nesta tela você comunica informações complementares da sua empresa, como número de colaboradores, consumo mensal de energia, local, área total e área construída do empreendimento.</p>
            
            <h2>Passo 3 de 5. Frequência de resíduos</h2>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6">
            <div class="form-group">
                <label for="geracao_diaria" class="required">Geração Diária de
                    Resíduos</label>
                @if($frequenciaGeracao)
                    @foreach($frequenciaGeracao as $dados)
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="geracao_diaria"
                                       value="{{ $dados['id'] }}"/>
                                {{ $dados['nome'] }}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group">
                <label for="coleta_diaria" class="required">Frequência de Coleta</label>
                @if($frequenciaColeta)
                    @foreach($frequenciaColeta as $dados)
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="coleta_diaria"
                                       value="{{ $dados['id'] }}"/>
                                {{ $dados['nome'] }}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-sm-3">
            <div class="form-group">
                <label for="numero_colaboradores" class="required">Número de
                    Colaboradores</label>
                @if($colaboradorNumero)
                    @foreach($colaboradorNumero as $dados)
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="numero_colaboradores"
                                       value="{{ $dados['id'] }}"/>
                                {{ $dados['nome'] }}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
                <label for="consumo_mensal" class="required">Consumo Mensal de
                    Energia</label>
                @if($energiaConsumo)
                    @foreach($energiaConsumo as $dados)
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="consumo_mensal"
                                       value="{{ $dados['id'] }}"/>
                                {{ $dados['nome'] }}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
                <label for="estabelecimento_tipo" class="required">Local do
                    empreendimento</label>
                @if($estabelecimentoTipo)
                    @foreach($estabelecimentoTipo as $dados)
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="estabelecimento_tipo"
                                       value="{{ $dados['id'] }}"/>
                                {{ $dados['nome'] }}
                            </label>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="col-sm-3">

            <div class="row">

                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="area_total" class="required">Área Total (m²)</label>
                        
                        <a href="javascript:void(0);" class="form-control information" style="float: right;
                        margin-right: 90px;">i
                            <div class="information__text">
                                <p class="">
                                    A área total é a soma da área privativa<br> mais a área de uso comum.
                                </p> 
                            </div>
                        </a>
                        <input type="text" name="area_total" id="area_total" class="form-control">
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="area_construida" class="required">Área Construída (m²)</label>
                        <input type="text" name="area_construida" id="area_construida" class="form-control">
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="flex justify-content-end">
                <a href="#!" type="button" class="btn prevBtn btnVoltar" title="Voltar para página anterior">
                    Voltar
                </a>

                {{--Adiciona hfref e classe nextBtn na validação do form--}}
                <a href="#!" type="button" class="btn btnSubmit btnValidaInformacoesComplementares btnProsseg"
                   title="Prosseguir">
                    Prosseguir
                </a>
            </div>
        </div>
    </div>

</div>
