@extends('layouts.home')

@section('conteudo')
    <section id="manutencao">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 cont-info">
                    <div>
                        <img class="logo-ctre-login" src="{{ asset('images/logo-manutencao.png') }}"
                             alt="CTRE - Controle de Transportes de Resíduos - Eletrônico" width="300">
                    </div>
                    <div class="">   
                        <p><b>Aviso</b></p>
                        <p>
                            O sistema CTRE se encontra em manutenção no presente momento.
                            Assim que forem concluídas as atualizações, reestabeleceremos a conexão.
                        </p>
                        <p>Agradecemos a compreensão.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style type="">
        body {
            background-image: url(../images/fundo-city.png) !important;
        }
    </style>
@endsection
