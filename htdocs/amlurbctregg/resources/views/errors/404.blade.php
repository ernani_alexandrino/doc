<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CTRE - 404</title>

    @include('layouts.include.styles')
    <link rel="stylesheet" href="{{ asset('css/amlurb.css') }}"/>
</head>
<body>
<header>
</header>
<section class="error">
    <p>Esta operação não pode ser realizada.</p>
    <p>Ops</p>
    <p>Tente novamente mais tarde!</p>
</section>
<footer>
</footer>
</body>
</html>
