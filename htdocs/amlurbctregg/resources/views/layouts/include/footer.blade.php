<footer>
    <div class="hidden-xs col-sm-2">
        <div class="row">
            <div class="logo-amlurb"></div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-10">
        <div class="row">
            <div class="copyright">
                CTR-E v1.0.0 - © {{date('Y')}} - Todos os direitos reservados.                
            </div>
        </div>
    </div>
</footer>