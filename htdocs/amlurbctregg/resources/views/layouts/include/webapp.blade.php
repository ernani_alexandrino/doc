<meta name="description" content="Controle de transporte de resíduos eletrônico."/>

<meta property="og:title" content="CTR-E"/>
<meta property="og:description" content="Controle de transporte de resíduos eletrônico."/>
<meta property="og:image" content="{{ asset('images/icons/icon-128x128.png') }}"/>

<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/ico/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/ico/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/ico/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/ico/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/ico/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/ico/apple-icon-120x120.png') }}">
{{-- <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/ico/apple-icon-144x144.png') }}"> --}}
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/ico/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/ico/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192" href="{{ asset('images/ico/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/ico/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/ico/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/ico/favicon-16x16.png') }}">

<meta name="msapplication-TileColor" content="#5d5d5d">
{{-- <meta name="msapplication-TileImage" content="{{ asset('images/ico/ms-icon-144x144.png') }}"> --}}
<meta name="theme-color" content="#5d5d5d">

<!-- Startup configuration -->
<link rel="manifest" href="{{ asset('manifest.json') }}"/>
@if(config('app.chat') && Request::path() != 'login')
<!-- Start of  Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=d5d7036a-07d5-46dd-80c0-ec7d5330ad1f"> </script>
<!-- End of  Zendesk Widget script -->
@endif

