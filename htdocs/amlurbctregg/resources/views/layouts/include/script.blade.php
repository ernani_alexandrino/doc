<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700">
<link rel="stylesheet" href="{{ asset('js/select2/css/select2.css') }}">
<link rel="stylesheet" href="{{ asset('css/amlurb.css') }}?{{config('app.version')}}">

<!-- Scripts -->
<script src="{{ asset('js/jquery.min.js') }}"></script>

<script type="text/javascript">
    //    window.analytics = '';
    window.baseUrl = "{{ URL::to('/') }}";
    window.currentUrl = "{{ Request::url() }}";
    //    window.accessToken = '4f4f8cef7b834b3e82cbafc40c792495';
    window.routeClass = "{{ Request::route()->getName() }}";
    window.urlSegment1 = "{{ Request::segment(1) }}";
    window.urlSegment2 = "{{ Request::segment(2) }}";
    window.urlSegment3 = "{{ Request::segment(3) }}";
    window.urlSegment4 = "{{ Request::segment(4) }}";
    window.urlSegment5 = "{{ Request::segment(5) }}";
</script>

<script src="{{ asset('js/select2/js/select2.js') }}"></script>
<script src="{{ asset('js/select2/js/i18n/pt-BR.js') }}"></script>

<script src="{{ asset('js/upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('js/upload/js/jquery.fileupload-process.js') }}"></script>
<script src="{{ asset('js/upload/js/jquery.fileupload-ui.js') }}"></script>
<script src="{{ asset('js/upload/js/jquery.fileupload-validate.js') }}"></script>

<script src="{{ asset('js/plugins.js') }}?{{config('app.version')}}"></script>
<script src="{{ asset('js/amlurb.js') }}?{{config('app.version')}}"></script>
<script src="{{ asset('js/dejota.js') }}"></script>

@yield('scripts-complementar')

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCY6m_DC7HDEpQj1wRAs8RYWozAaE39guI&callback=initMap"></script>
<script async defer
        src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

