<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @include('layouts.include.webapp')

    @isset($titulo)
        <title>Amlurb - {{ $titulo->principal . " - " . $titulo->subtitulo }}</title>
    @else
        <title>Amlurb - Controle de Resíduos Eletrônico</title>
    @endisset

    @include('layouts.include.styles')
</head>
<body class="fundo">

@yield('conteudo')

@include('layouts.include.script')

</body>
</html>