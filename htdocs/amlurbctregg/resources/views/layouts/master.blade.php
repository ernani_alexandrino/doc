@extends('layouts.home-painel')

@section('conteudo')
    <section id="{{$idLayoutPainel}}">

        @include("{$menuLateral}")

        <div class="col-xs-1 col-sm-10">
            <div class="row">
                <div class="conteudo">
                    <h1>{{ $titulo->principal }} <span>{{ $titulo->subtitulo }}</span></h1>

                    <nav class="navbar navbar-invers chat">
                        <div class="col-xs-12" style="padding:0;">
                            {{-- 
                                <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                                        aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#">Chat</a>
                            </div> --}}
                            <div id="navbar" class="collapse navbar-collapse" style="padding:0;">
                                <ul class="nav navbar-nav">
                                    <li><a href="#">Chat</a></li>
                                    <li><a href="/messages" id="m_messages" class="active">Mensagens 
                                        @include('messenger.unread-count')</a></li>
                                    @if($idLayoutPainel == 'painel-fiscal')
                                        <li><a href="/messages/create" id="m_messages.create">Criar Nova Mensagem</a></li>
                                    @endif
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>

                    <div class="chat-corpo">
                        <div class="col-xs-12">
                            <div class="row">

                                @yield('content')

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>

@endsection