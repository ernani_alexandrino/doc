<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('layouts.include.webapp')

    @isset($titulo)
        <title>Amlurb - {{ $titulo->principal . " - " . $titulo->subtitulo }}</title>
    @else
        <title>Amlurb - Controle de Resíduos Eletrônico</title>
    @endisset

    @include('layouts.include.styles')
</head>
<body class="fundo-painel">

<section id="painel-pequeno-gerador">

    @include('painel.pequeno-gerador.menu-lateral-pg')

    <div class="col-xs-12 col-sm-10">
        <div class="row">
            <div class="conteudo">

                @include('layouts.home-validade-cadastro')

                @yield('conteudo')

            </div>
        </div>
    </div>

</section>

@include('layouts.include.footer')

@include('painel.gerador.includes.script')

</body>
</html>