@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# Whoops!
@else
# Hello!
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
            $color = 'green';
            break;
        case 'error':
            $color = 'red';
            break;
        default:
            $color = 'blue';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
<div class="assinatura" style=" float: left; width: 100%; border-bottom: 3px solid #f60; padding-bottom: 12px; margin-top: 40px;">
    <p style="font-size: 15px; color: #787878;">Atenciosamente,</p>
    <p style="font-size: 15px; color: #787878;"><strong>Amlurb - Autoridade Municipal de Limpeza Urbana</strong></p>
    <div class="logo-assinatura" style="float: right;">
        <img class="logo-amlurb" src="{{  asset('/images/logo_amlurb_rgb.svg') }}" alt="Amlurb">
    </div>
</div>
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
<div class="rodape" style="background: #fff; margin-top: 27px; padding: 10px;">
    <p style="font-size: 11px; margin: 0; line-height: 14px;">Esse e-mail foi gerado automaticamente e não precisa ser respondido.</p>
    <p style="font-size: 11px; margin: 0; line-height: 14px;">Se houver algum problema para clicar no botão REDEFINIR SENHA, copie e cole este link na url do seu navegador: [{{ $actionUrl }}</p>
    <p style="font-size: 15px; color: #787878;"><strong>CANAIS DE ATENDIMENTO</strong></p>

    <p style="font-size: 11px; margin: 0; line-height: 14px;">
        As dúvidas serão sanadas através do telefone 0800 123 4567, com atendimendo de segunda a
        sexta-feira (exceto feriados), das 8h às 17h (horário de Brasília) ou através do e-mail <strong>atendimento@amlurb.com.br</strong>
    </p>
    <p style="font-size: 11px; margin: 0; line-height: 14px;">2017 Amlurb - Autoridade Municipal de Limpeza Urbana - Todos os direitos reservados.</p>
</div>
@endcomponent
@endisset
@endcomponent
