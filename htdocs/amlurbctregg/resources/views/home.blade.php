@extends('layouts.home-painel')

@section('conteudo')
    <section id="painel-gerador">

        @include('painel.gerador.menu-lateral')

        <div class="col-xs-12 col-sm-9">
            <div class="row">
                <div class="conteudo">
                    <h1>Módulo Gerador <span>Painel de Controle</span></h1>

                    <div class="indicadores-gerador">
                        <div class="col-xs-3">
                            <div class="box-indicador">
                                <div class="image-indicador total-emitido"></div>
                                <div class="corpo-indicador">
                                    <div class="contador">
                                        0
                                    </div>
                                    <div class="titulo-indicador">
                                        Total CTR Emitidos
                                    </div>
                                    <div class="diviser"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="box-indicador">
                                <div class="image-indicador total-aberto"></div>
                                <div class="corpo-indicador">
                                    <div class="contador">
                                        0
                                    </div>
                                    <div class="titulo-indicador">
                                        Total CTR em Aberto
                                    </div>
                                    <div class="diviser"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="box-indicador">
                                <div class="image-indicador total-finalizados"></div>
                                <div class="corpo-indicador">
                                    <div class="contador">
                                        0
                                    </div>
                                    <div class="titulo-indicador">
                                        Total CTR Finalizados
                                    </div>
                                    <div class="diviser"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="box-indicador">
                                <div class="image-indicador total-expirados"></div>
                                <div class="corpo-indicador">
                                    <div class="contador">
                                        0
                                    </div>
                                    <div class="titulo-indicador">
                                        Total CTR Expirados
                                    </div>
                                    <div class="diviser"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="dashboard-gerador">
                        <div class="col-xs-6">
                            <div class="row">

                                <div class="balanco-periodo">
                                    <div class="col-xs-12">
                                        <div class="titulo-dashboard"></div>
                                    </div>

                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="data_inicial">Data Inicial</label>
                                            <div class="input-group">
                                                <input name="data_inicial" type="text" class="form-control"
                                                       placeholder="00/00/0000" aria-label="Data inicial"
                                                       aria-describedby="addonDataInicial">
                                                <span class="input-group-addon" id="addonDataInicial"><i></i></span>
                                            </div>
                                            {{--<input type="text" name="data_inicial" class="form-control" id="data_inicial">--}}
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="data_inicial">Data Final</label>
                                            <div class="input-group">
                                                <input type="text" name="data_final" class="form-control"
                                                       placeholder="00/00/0000" aria-label="Data final"
                                                       aria-describedby="addonDataFinal">
                                                <span class="input-group-addon" id="addonDataFinal"><i></i></span>
                                            </div>
                                            {{--<input type="text" name="data_final" class="form-control" id="data_final">--}}
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="grafico"></div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="col-xs-6">
                            <div class="lancamentos-ctre">
                                <div class="titulo-dashboard"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
@endsection
