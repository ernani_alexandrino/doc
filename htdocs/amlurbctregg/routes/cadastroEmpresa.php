<?php

Route::post('valida-empresa', 'CadastroEmpresaController@validaEmpresa');
Route::post('valida-responsavel', 'CadastroEmpresaController@validaResponsavel');
Route::post('valida-informacoes-complementares', 'CadastroEmpresaController@validaInformacoesComplementares');
Route::post('valida-responsavel-empreendimento', 'CadastroEmpresaController@validaResponsavelEmpreendimento');
Route::post('valida-responsavel-empreendimento-coo', 'CadastroEmpresaController@validaResponsavelEmpreendimentoCoo');
Route::post('valida-condominio-responsavel', 'CadastroEmpresaController@validaCondominioResponsavel');
Route::post('valida-condominio-arquivos/{nomeCampo}', 'CadastroEmpresaController@validaCondominioArquivos');
Route::post('valida-empresa-arquivos/{nomeCampo}', 'CadastroEmpresaController@validaEmpresaArquivos');
Route::post('valida-transportador-arquivos/{nomeCampo}', 'CadastroEmpresaController@validaTransportadorArquivos');
Route::post('valida-transportador', 'CadastroEmpresaController@validaTransportador');
Route::post('valida-docs-coo', 'CadastroEmpresaController@validaDocsCoo');
Route::post('valida-cooperativa', 'CadastroEmpresaController@validaCooperativa');
Route::post('valida-termos-de-uso', 'CadastroEmpresaController@validaTermosDeUso');

Route::get('buscar-tipo-atividade/{id}', 'SiteController@buscarTipoAtividade');
Route::get('cadastro-realizado', 'CadastroEmpresaController@empresaCadastroSucesso')->name('cadastro.sucesso');
Route::get('painel/fiscal/cadastro-filial-grande-gerador', 'CadastroEmpresaController@cadastrarFilial');
Route::get('cadastro-de-filial', 'CadastroEmpresaController@cadastroDeFilial');

//Route::resource('cadastroExistente/{id}', 'CadastroEmpresaController');
Route::resource('cadastro', 'CadastroEmpresaController');
Route::resource('cidade', 'CidadeController');