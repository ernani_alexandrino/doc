<?php

// Descomentar abaixo para deixar o sistema offline
//include __DIR__ . '/ctreOffline.php';

// Descomentar abaixo para deixar o cadastro offline e somente funcionando os logins que tem status_id == 1 na table users
//include __DIR__ . '/ctreTestesOnline.php';

if (\config('auth.manutencao')) {
    include __DIR__ . '/ctreOffline.php';
}

include __DIR__ . '/ctreOnline.php';




