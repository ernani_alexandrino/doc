<?php

Route::group(['middleware' => 'auth', 'prefix' => 'painel'], function () {

    // Rotas Gerador
    include __DIR__ . '/gerador.php';
    include __DIR__ . '/pequeno_gerador.php';

    // Rotas Transportador
    include __DIR__ . '/transportador.php';

    // Rotas Destino Final
    include __DIR__ . '/destino_final.php';

    // Rotas Órgão Público
    include __DIR__ . '/orgao_publico.php';

    // Rotas Serviço Saúde
    include __DIR__ . '/servico_saude.php';

    // Rotas Fiscal
    include __DIR__ . '/fiscal.php';

    // Rotas Condomínio Misto
    include __DIR__ . '/condominio_misto.php';

    //CRUD USUÁRIOS
    Route::post('criar-usuario', 'UsuariosController@store')->name('painel.usuario.cadastro');
    Route::get('user/{id}/show', 'UsuariosController@edit')->name('get_usuario');
    Route::post('atualizar-usuario/{id}/{rota}', 'UsuariosController@update')->name('usuario.atualizar');
    Route::patch('usuario', 'UsuariosController@updateFromConfiguracoesEmpresa');
    Route::get('user/destroy/{id}', 'UsuariosController@destroy')->name('destroy_user');

    // perfil
    Route::post('/perfil/store', 'PermissoesController@store')->name('perfil.store');
    Route::get('/perfil/edit/{id}', 'PermissoesController@edit')->name('perfil.edit');
    Route::post('/perfil/update/{id}', 'PermissoesController@update')->name('perfil.update');
    Route::get('/perfil/delete/{id}', 'PermissoesController@destroy')->name('perfil.delete');

    // Rotas para o Controller Gerenciador, que é o controller principal dos tipos de empresa
    Route::get('configuracoes', 'Gerenciador\PainelController@configuracoes')->name('painel.configuracoes');
    Route::post('veiculo-salvar/{id?}', 'Gerenciador\PainelController@veiculoSalvar');
    Route::get('veiculo-deletar/{id?}', 'Gerenciador\PainelController@veiculoDeletar');
    Route::get('vinculos-por-periodo/{periodo?}', 'Gerenciador\PainelController@vinculosPorPeriodo');

    Route::post('veiculo/editar/save', 'VeiculosController@veiculoEditarSalvar')->name('edit.save');

    Route::get('ctre-por-periodo/{periodo?}', 'Gerenciador\PainelController@ctrePorPeriodo');
    Route::get('ctre-por-periodo-home-fiscal/{periodo?}/{dataInicio?}/{dataFinal?}',
        'Gerenciador\PainelController@ctrePorPeriodoHomeFiscalAjax');

    // ctre
    Route::get('ctre/{id}', 'CtreController@showCtre');
    Route::get('equipamentos-alocados-to/{id}', 'EquipamentosController@getEquipamentosAlocadosTo');
    Route::get('residuos-vinculados-to/{id}', 'Gerenciador\PainelController@getResiduosVinculadosTo');
    Route::post('ctre', 'CtreController@emissaoCtre');
    Route::get('ctre-validar/{id}', 'CtreController@showCtreValidar');
    Route::patch('ctre-validar/{id}', 'CtreController@validarCtre');
    Route::patch('ctre-invalidar/{id}', 'CtreController@invalidarCtre');

    // equipamentos
    Route::post('equipamento-salvar', 'EquipamentosController@equipamentoSalvar');
    Route::delete('equipamento-deletar/{id?}', 'EquipamentosController@equipamentoDeletar');
    Route::delete('equipamento-force-deletar/{id?}', 'EquipamentosController@equipamentoForceDeletar');
    Route::get('equipamentos-list', 'EquipamentosController@listEquipamentos');
    Route::get('equipamentos-ativar', 'EquipamentosController@listEquipamentosAtivar');
    Route::post('equipamento/{id}/ativar', 'EquipamentosController@ativarEquipamento');
    Route::post('ativar-varios-equipamentos', 'EquipamentosController@ativarVariosEquipamentos');

    // veiculos
    Route::get('buscar-tipo-veiculo/{placa}', 'VeiculosController@buscarTipoVeiculo');
    Route::post('veiculo-salvar/{id?}', 'VeiculosController@veiculoSalvar');
    Route::delete('veiculo-deletar/{id?}', 'VeiculosController@veiculoDeletar');
    Route::delete('veiculo-force-deletar/{id?}', 'VeiculosController@veiculoForceDeletar');

    // qr codes
    Route::get('novo-qrcode-equipamento/{id}', 'QrcodeController@novoQRCodeEquipamento');
    Route::get('novo-qrcode-veiculo/{id}', 'QrcodeController@novoQRCodeVeiculo');
    Route::get('qrcode-pdf/{id}', 'QrcodeController@qrcodePDF')->name('qrcode_pdf');
    Route::get('guia-qrcode', 'QrcodeController@guia')->name('guia_qrcode');

    // boletos -- gerador e transportador
    Route::get('boletos/{id}', 'BoletoController@showBoleto');
    Route::get('boleto-pdf/{id}', 'BoletoController@showBoletoPDF');
    Route::get('boleto-segunda-via/{id}', 'BoletoController@boletoSegundaVia');
    Route::get('boletos-list', 'BoletoController@listBoletos');

    // vinculos
    Route::post('vincular-empresa', 'VinculosController@vincularEmpresa')->name('vc_ve');
    Route::post('vincular-gerador', 'VinculosController@vincularGerador');
    Route::post('vincular-transportador', 'VinculosController@vincularTransportador');
    Route::post('vincular-cooperativa', 'VinculosController@vincularCooperativa');
    Route::post('vincular-destino-final', 'VinculosController@vincularDestinoFinal');
    Route::post('vincular-condominio', 'VinculosController@vincularCondominio');
    Route::get('vinculos/{empresa_id}', 'VinculosController@show');
    Route::patch('vinculos/{empresa_id}', 'VinculosController@update');
    Route::delete('vinculos/{id}', 'VinculosController@delete');
    Route::patch('vinculo-restaurar/{id}', 'VinculosController@restore');
    Route::get('vinculos-removidos-list', 'VinculosController@listVinculosRemovidos');

    Route::get('geradores-vinculados-list', 'VinculosController@listGeradoresVinculados');
    Route::get('transportadores-vinculados-list', 'VinculosController@listTransportadoresVinculados');
    Route::get('cooperativas-vinculadas-list', 'VinculosController@listCooperativasVinculadas');
    Route::get('destinos-vinculados-list', 'VinculosController@listDestinosVinculados');

    // configurações - dados da empresa - gerador e transportador
    Route::post('empresa/solicitar-alteracao', 'EmpresasController@solicitarAlteracaoDados');

});