<?php

Route::group(['middleware' => 'authDestinoFinal', 'prefix' => 'destino-final'], function () {

    Route::get('/', 'DestinoFinalController@index')->name('painel_df');

    // alterar cadastro
    Route::get('meu-cadastro', 'DestinoFinalController@meuCadastro')->name('mc_df');

    //Vínculos
    Route::get('clientes-fornecedores', 'DestinoFinalController@clientesFornecedores')->name('cf_df');

    // listagens Ajax
    Route::get('ctre-list-recebidos', 'DestinoFinalController@listCtreRecebidos');

});