<?php

Route::group(['middleware' => 'authGerador', 'prefix' => 'gerador'], function () {

    Route::get('/', 'GeradorController@index')->name('painel_gg');
    Route::get('meus-equipamentos', 'GeradorController@meusEquipamentos')->name('eq_gg');
    Route::get('meu-cadastro', 'GeradorController@meuCadastro')->name('mc_gg');

    //Vínculos
    Route::get('clientes-fornecedores', 'GeradorController@clientesFornecedores')->name('cf_gg');

    // listagens AJAX
    Route::get('ctre-list-emitidos', 'GeradorController@listCtreEmitidos');
    Route::get('ctre-list-recebidos', 'GeradorController@listCtreRecebidos');

});