<?php

Route::group(['middleware' => 'authAdministrador', 'prefix' => 'fiscal'], function () {

    Route::get('/', 'FiscalController@index')->name('fiscal');

    //Configurações
    Route::get('configuracoes', 'FiscalController@configuracoes')->name('painel_fs');

    //Rastreabilidade
    Route::get('rastreabilidade', 'FiscalController@rastreabilidade')->name('rastreabilidade_fs');

    // datatables vinculos
    Route::get('vinculos-ajax/{empresaId?}', 'FiscalController@vinculosAjax');
   
    //get veiculo by id
    Route::get('get/veiculo/{idVeiculo}', 'FiscalController@getVeiculo');
    Route::get('veiculo/aprovar/{idVeiculo}/{boleto}/{data?}', 'FiscalController@aprovarVeiculo');
    //reportar inconsistencia veiculo
    Route::post('reportar-inconsistencia-veiculo', 'FiscalController@reportarInconsistenciaVeiculo');
    //retorna a ultima incosistencia
    Route::get('incosistencia/veiculo/{idVeiculo}', 'FiscalController@getInconsistenciaVeiculo');

    // Cadastro de Filial
    Route::post('cadastro-filial', 'FiscalController@cadastroFilial')->name('cadastro_filial');
    Route::get('lista-filiais/{empresaId?}', 'FiscalController@listaFiliaisAjax');

    // cadastros
    Route::get('cadastro-grande-gerador/{empresaId?}', 'FiscalController@cadastroGg')->name('cadastro_gg_fs'); 
    Route::get('cadastro-pequeno-gerador/{empresaId?}', 'FiscalController@cadastroPg')->name('cadastro_pg_fs');
    Route::get('cadastro-grande-gerador-condominio/{empresaId?}','FiscalController@cadastroGc')->name('cadastro_gc_fs');
    Route::get('cadastro-transportadores/{empresaId?}','FiscalController@cadastroTr')->name('cadastro_tr_fs');
    Route::get('cadastro-destinos/{empresaId?}', 'FiscalController@cadastroDf')->name('cadastro_df_fs');
    Route::get('cadastro-cooperativa/{empresaId?}', 'FiscalController@cadastroCr')->name('cadastro_cr_fs');
    Route::get('cadastro-servico-saude/{empresaId?}', 'FiscalController@cadastroSs')->name('cadastro_ss_fs');
    Route::get('cadastro-orgao-publico/{empresaId?}', 'FiscalController@cadastroOp')->name('cadastro_op_fs');


    // Cadastro Grande Gerador
    Route::get('cadastro-gg-ajax', 'FiscalController@cadastroGgAjax');
    Route::get('cadastro-gg-search/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroGgSearch')->name('cadastro_gg_search_fs');
    Route::get('cadastro-gg-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroGgSearchAjax');

    // Cadastro Grande Gerador Condominio
    Route::get('cadastro-gc-ajax', 'FiscalController@cadastroGcAjax');
    Route::get('cadastro-gc-search/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroGcSearch')->name('cadastro_gc_search_fs');
    Route::get('cadastro-gc-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroGcSearchAjax');

    // Cadastro Pequeno Gerador
    Route::get('cadastro-pg-ajax', 'FiscalController@cadastroPgAjax');
    Route::get('cadastro-pg-search/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroPgSearch')->name('cadastro_pg_search_fs');
    Route::get('cadastro-pg-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroPgSearchAjax');

    // Cadastro Transportador
    Route::get('cadastro-tr-ajax', 'FiscalController@cadastroTransportadorAjax');
    Route::get('cadastro-tr-search/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroTransportadorSearch')->name('cadastro_tr_search_fs');
    Route::get('cadastro-tr-search-ajax/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroTransportadorSearchAjax');
        
    Route::get('vinculos-tr-vl-ajax/{empresaId?}', 'FiscalController@vinculosTrVeiculosAjax');
    Route::get('vinculos-tr-eq-ajax/{empresaId?}', 'FiscalController@vinculosTrEquipamentosAjax');

    // Cadastro Destino Final
    Route::get('cadastro-df-ajax', 'FiscalController@cadastroDfAjax');
    Route::get('cadastro-df-search/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroDfSearch')->name('cadastro_df_search_fs');
    Route::get('cadastro-df-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroDfSearchAjax');

    // Cadastro Cooperativas
    Route::get('cadastro-cr-ajax', 'FiscalController@cadastroCrAjax');
    Route::get('cadastro-cr-search/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroCrSearch')->name('cadastro_cr_search_fs');
    Route::get('cadastro-cr-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroCrSearchAjax');

    // Cadastro Serviço de Saúde
    Route::get('cadastro-ss-ajax', 'FiscalController@cadastroSsAjax');
    Route::get('cadastro-ss-search/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroSsSearch')->name('cadastro_ss_search_fs');
    Route::get('cadastro-ss-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroSsSearchAjax');

    // Cadastro Órgão Público
    Route::get('cadastro-op-ajax', 'FiscalController@cadastroOpAjax');
    Route::get('cadastro-op-search/{filtro?}/{filtroValor?}',
        'FiscalController@cadastroOpSearch')->name('cadastro_op_search_fs');
    Route::get('cadastro-op-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroOpSearchAjax');

    Route::get('cadastros-por-periodo/{periodo?}/{dataInicial?}/{dataFinal?}',
        'FiscalController@cadastrosPorPeriodo');

    Route::get('rastreabilidade-ajax', 'FiscalController@rastreabilidadeAjax');
    Route::get('rastreabilidade-logs/{ctreId?}', 'FiscalController@rastreabilidadeLogsAjax');

    Route::get('ctre-por-periodo-fiscal/{periodo?}/{empresaId?}', 'FiscalController@ctrePorPeriodoFiscalAjax');
    Route::get('ctre-balanco-periodo/{dataInicial?}/{dataFinal?}', 'FiscalController@ctreBalancoPeriodo');

    Route::get('residuos', 'ResiduosController@index')->name('residuo_fs');
    Route::post('residuos/store', 'ResiduosController@store')->name('residuo_store_fs');
    Route::get('residuos/{id}/edit', 'ResiduosController@edit')->name('residuo_edit_fs');
    Route::post('residuos/{id}/update', 'ResiduosController@update')->name('residuo_update_fs');
    Route::get('residuos/{id}/destroy', 'ResiduosController@destroy')->name('residuo_destroy_fs');

    // Aprovar cadastro : aprovacao
    Route::get('aprovacao-de-cadastro', 'AprovacaoCadastroController@index')->name('fiscal.aprovacao.dashboard');

    Route::get('aprovacao-de-cadastro/{tipoEmpresa}',
        'AprovacaoCadastroController@lista')->name('fiscal.aprovacao.lista');
    Route::get('revisao-de-cadastro/{tipoEmpresa}',
        'AprovacaoCadastroController@listaRevisao')->name('fiscal.revisao.lista');

    Route::post('aprovar-cadastro/{tipo?}', 'AprovacaoCadastroController@aprovarCadastro')->name('fiscal.aprovacao.aprovar');
    Route::post('revisar-cadastro', 'AprovacaoCadastroController@revisarCadastro')->name('fiscal.aprovacao.revisar');
    Route::post('recusar-cadastro', 'AprovacaoCadastroController@recusarCadastro')->name('fiscal.aprovacao.recusar');
    Route::post('reportar-inconsistencia', 'AprovacaoCadastroController@reportarInconsistencia')->name('fiscal.aprovacao.reportar');

    Route::get('lista-empresas-aprovacao/{tipoEmpresa}',
        'AprovacaoCadastroController@pegaListaEmpresasParaAprovacao')->name('fiscal.aprovacao.lista.aprovacao');

    Route::get('lista-empresas-revisao/{tipoEmpresa}',
        'AprovacaoCadastroController@pegaListaEmpresasParaRevisao')->name('fiscal.aprovacao.lista.revisao');
        
    Route::get('detalhes-empresa',
        'AprovacaoCadastroController@exibirDetalhesEmpresa')->name('fiscal.aprovacao.detalhesEmpresa');

    Route::get('detalhes-empresa-revisao',
        'AprovacaoCadastroController@exibirDetalhesEmpresaRevisao')->name('fiscal.aprovacao.detalhesEmpresaRevisao');

    // Ativar/Desativar empresa
    Route::post('ativar-desativar-empresa/{empresa?}',
        'AprovacaoCadastroController@ativarDesativarEmpresa')->name('fiscal.ativar-desativar.empresa');

    Route::get('empresa/show/{id}', 'EmpresasController@show');        
    
});
