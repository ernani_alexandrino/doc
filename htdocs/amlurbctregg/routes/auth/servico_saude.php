<?php

Route::group(['middleware' => 'authServicoSaude', 'prefix' => 'servico-saude'], function () {

    Route::get('/', 'ServicoSaudeController@index')->name('painel_ss');
    Route::get('meus-equipamentos', 'ServicoSaudeController@meusEquipamentos')->name('eq_ss');
    Route::get('meu-cadastro', 'ServicoSaudeController@meuCadastro')->name('mc_ss');

    //Vínculos
    Route::get('clientes-fornecedores', 'ServicoSaudeController@clientesFornecedores')->name('cf_ss');

});