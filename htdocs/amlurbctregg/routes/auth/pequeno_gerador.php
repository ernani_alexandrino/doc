<?php

Route::group(['middleware' => 'authPequenoGerador', 'prefix' => 'pequeno-gerador'], function () {

    Route::get('/', 'PequenoGeradorController@index')->name('painel_pg');

});