<?php

Route::group(['middleware' => 'authCondominioMisto', 'prefix' => 'condominio-misto'], function () {

    Route::get('/', 'CondominioMistoController@index')->name('painel_cm');
    Route::get('meus-equipamentos', 'CondominioMistoController@meusEquipamentos')->name('eq_cm');
    Route::get('meu-cadastro', 'CondominioMistoController@meuCadastro')->name('mc_cm');

    // //Vínculos
    Route::get('clientes-fornecedores', 'CondominioMistoController@clientesFornecedores')->name('cf_cm');

});