<?php

Route::group(['middleware' => 'authOrgaoPublico', 'prefix' => 'orgao-publico'], function () {

    Route::get('/', 'OrgaoPublicoController@index')->name('painel_op');
    Route::get('meus-equipamentos', 'OrgaoPublicoController@meusEquipamentos')->name('eq_op');
    Route::get('meu-cadastro', 'OrgaoPublicoController@meuCadastro')->name('mc_op');

    //Vínculos
    Route::get('clientes-fornecedores', 'OrgaoPublicoController@clientesFornecedores')->name('cf_op');

});