<?php

Route::group(['middleware' => 'authTransportador', 'prefix' => 'transportador'], function () {

    Route::get('/', 'TransportadorController@index')->name('painel_tr');

    Route::get('get/veiculo/{idVeiculo}', 'TransportadorController@getVeiculo');
    //Ctre
    Route::get('ctre-gerador/{id}', 'CtreController@showCtreGeradorValidar');

    //Equipamentos e Veículos
    Route::get('meus-equipamentos', 'TransportadorController@meusEquipamentos')->name('eq_tr');

    //Vínculos
    Route::get('clientes-fornecedores', 'TransportadorController@clientesFornecedores')->name('cf_tr');

    // pagamentos
    Route::get('pagamentos', 'TransportadorController@pagamentos')->name('pg_tr');
    Route::post('novo-boleto-veiculo/{id}', 'BoletoController@novoBoletoVeiculo');
    Route::post('novo-boleto-varios-veiculos', 'BoletoController@novoBoletoVariosVeiculos');
    Route::post('veiculo_upload/{doc}', 'VeiculosController@uploadDocumentos');
    Route::post('veiculo_upload_editar/{doc}', 'VeiculosController@uploadDocumentosEditar');
    // alterar cadastro
    Route::get('meu-cadastro', 'TransportadorController@meuCadastro')->name('mc_tr');

    // FAQ
    Route::get('faq', 'TransportadorController@faq')->name('faq_tr');

    // listagens AJAX
    Route::get('ctre-list-emitidos', 'TransportadorController@listCtreEmitidos');
    Route::get('ctre-list-recebidos', 'TransportadorController@listCtreRecebidos');
    Route::get('veiculos-list', 'TransportadorController@listVeiculos');
    Route::get('veiculos-ativar', 'TransportadorController@listVeiculosAtivar');

});