<?php
use Illuminate\Support\Facades\Mail;

/* ****************** ROTAS PARA O SISTEMA CTRE FUNCIONAR 100%  ******************************* */

Route::get('/firebase', 'SiteController@firebasePutVeiculos')->name('firebase');

Auth::routes();
Route::redirect('/', '/login');
Route::redirect('/manutencao', '/login');

Route::get('qrcode/{id}', 'QrcodeController@qrCode')->name('qrcode');
Route::get('teste-gerar-qrcode-empresa/{id}', 'QrcodeController@testeGerarQrCodeEmpresa')->name('gerar_qrcode');

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'Gerenciador\PainelController@index')->name('home');
Route::get('/download/{empresaId}/{aquivo}', 'Gerenciador\PainelController@download')->name('download');

// Boleto
Route::get('boleto/{arquivo}', 'Gerenciador\PainelController@downloadBoleto')->name('download.boleto');
Route::get('boleto-segunda-via/{id}', 'Gerenciador\PainelController@boletoSegundaVia')->name('boleto.2via');

// SiteController // cadastro
//include __DIR__ . '/empresa.php';

// Cadastro de empresas
include __DIR__ . '/cadastroEmpresa.php';

// ambiente logado
include __DIR__ . '/auth/painel.php';

Route::get('/api/cnpj-empresa/{cnpj}', 'ApiController@cnpjEmpresa');

Route::get('/geolocation', 'GeoLocationController@getEnderecoEmpresas');
Route::get('/downloadJSONFile/{tipo?}',
    array('as' => 'downloadJSONFile', 'uses' => 'GeoLocationController@downloadJsonGeolocation'));
Route::get('/readGeolocation/{tipo?}/{filter?}/{termoPesquisado?}', 'GeoLocationController@readJsonGeolocation');
Route::get('/codamlurb', 'GeoLocationController@codAmlurb');

Route::group(['middleware' => 'auth', 'prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});

Route::group(['middleware' => 'auth', 'prefix' => 'painel'], function () {
    // Download Automático do Arquivo By Mr.Goose em abr 19
    Route::get('file/{arquivo}/{empresa}', function ($arquivo, $empresa) {
        return getFileS3('licencas', $arquivo, $empresa);
    })->name('file');

    Route::get('temp_file/{arquivo}/{empresa}', function ($arquivo, $empresa) {
        return getFileS3('licencas_temp', $arquivo, $empresa);
    })->name('file_temp');

    Route::get('/download/veiculo/{arquivo}/{empresa}/{placa?}', function ($arquivo, $empresa, $placa = null) {
        return getFileS3('veiculos', $arquivo, $empresa, $placa);
    })->name('download_veiculo');
});


Route::get('get-location-from-ip', function () {
    $dadosEmail['emailReceptor'] =  preg_replace('/\s+/', '', mb_strtolower('djemerson233.almeida@gmail.com'));
    $validator = Validator::make(['email'=> $dadosEmail['emailReceptor']], [
        "email" => 'mailgun_email:mailbox'
    ]);

    if ($validator->fails()) {

        $retornoAjax['erro'] = $validator->errors();

        return response()->json($retornoAjax);
    }
 
    die;
    Mail::raw('Hi, welcome user!', function ($message) {
        $message->to('djemerson.almeida@gmail.com')
          ->subject('teste');
    });
});

/* ======================================================= */
