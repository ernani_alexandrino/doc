<?php

/**
 * CTRE funciona apenas com os logins de testes mas sem realizar cadastros
 */

Route::get('/firebase', 'SiteController@firebasePutVeiculos')->name('firebase');


Auth::routes();

Route::redirect('/', '/login');

Route::post('/autenticate',  'Auth\LoginController@autenticate')->name('autenticate');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'Gerenciador\PainelController@index')->name('home');
Route::get('/download/{aquivo}', 'Gerenciador\PainelController@download')->name('download');

Route::prefix('empresa')->group(function () {
    Route::get('cadastro', function(){
        return view('index');
    });
    Route::post('salvar', function(){
        return view('index');
    });
    Route::post('salvar-primeira-etapa', function(){
        return view('index');
    });
    Route::post('salvar-segunda-etapa', function(){
        return view('index');
    });
    Route::get('buscar-tipo-atividade/{id}', function(){
        return view('index');
    });
    Route::get('buscar-cidade/{id}', function(){
        return view('index');
    });
    Route::redirect('login', function(){
        return view('index');
    });
    Route::get('cadastro-terceira-etapa/{id}', function(){
        return view('index');
    });
    Route::post('salvar-terceira-etapa', function(){
        return view('index');
    });
    Route::get('cadastro-realizado/{protocoloId?}', function(){
        return view('index');
    });
    Route::get('cadastro-quarta-etapa/{id}', function(){
        return view('index');
    });
    Route::post('salvar-quarta-etapa', function(){
        return view('index');
    });
});

Route::group(['middleware' => 'auth', 'prefix' => 'painel'], function () {

    // Rotas Gerador
    Route::group(['middleware' => 'authGerador', 'prefix' => 'gerador'], function() {

        Route::get('/', 'GeradorController@index')->name('painel_gg');

        //Configurações
        Route::get('configuracoes', 'GeradorController@configuracoes')->name('config_gg');
        Route::post('equipamento-salvar', 'GeradorController@equipamentoSalvar');
        Route::get('equipamento-deletar/{id?}', 'GeradorController@equipamentoDeletar');

        //Equipamentos
        Route::get('equipamentos', 'GeradorController@equipamento')->name('equipamento_gg');
        Route::post('equipamento-salvar', 'GeradorController@equipamentoSalvar');
        Route::get('equipamento-deletar/{id?}', 'GeradorController@equipamentoDeletar');

        //Vínculos
        Route::get('cadastro-gg-tr', 'GeradorController@vinculoGgTr')->name('cadastro_vinculo_gg');
        Route::post('vinculo-salvar', 'GeradorController@vinculoGgTrSalvar')->name('vinculo_salvar_gg');
        Route::get('cadastro-gc-gg', 'GeradorController@vinculoGcGgShow')->name('cadastro_vinculo_gc');
        Route::post('vinculo-gc-gg-salvar', 'GeradorController@vinculoGcGgStore')->name('vinculo_salvar_gc');
        Route::get('vinculo-gc-gg-deletar/{empresaId?}', 'GeradorController@vinculoGcGgDestroy')->name('vinculo_deletar_gc');

        //Ctre
        Route::get('ctre', 'GeradorController@ctreEmissao')->name('ctre_gg');
        Route::get('ctre-composicao/{idCtre?}', 'GeradorController@ctreComposicao');
        Route::post('ctre-salvar', 'GeradorController@ctreEmissaoSalvar')->name('ctre_salvar_gg');
        Route::get('ctre-validar-salvar/{idLogCtre?}', 'GeradorController@ctreValidarSalvar');

        //Rastreabilidade
        Route::get('rastreabilidade', 'GeradorController@rastreabilidade')->name('rastreabilidade_gg');

    });

    // Rotas Transportador
    Route::group(['middleware' => 'authTransportador', 'prefix' => 'transportador'], function() {

        Route::get('/', 'TransportadorController@index')->name('painel_tr');

        //Configuracoes
        Route::get('configuracoes', 'TransportadorController@configuracoes')->name('config_tr');

        //Equipamentos e Veículos
        Route::get('equipamentos-veiculos', 'TransportadorController@equipamentoVeiculo')->name('equipamento_veiculo_tr');
        Route::get('equipamentos', 'TransportadorController@equipamento')->name('equipamento_tr');
        Route::post('equipamento-salvar', 'TransportadorController@equipamentoSalvar');
        Route::get('equipamento-deletar/{id?}', 'TransportadorController@equipamentoDeletar');
        Route::get('veiculos', 'TransportadorController@veiculo')->name('veiculo_tr');
        Route::post('veiculo-salvar/{id?}', 'TransportadorController@veiculoSalvar');
        Route::get('veiculo-deletar/{id?}', 'TransportadorController@veiculoDeletar');

        //Vínculos
        Route::get('cadastro-gg-tr', 'TransportadorController@vinculoGgTr')->name('cadastro_vinculo_tr_gg');
        Route::get('cadastro-tr-df', 'TransportadorController@vinculoTrDf')->name('cadastro_vinculo_tr');
        Route::post('vinculo-salvar', 'TransportadorController@vinculoTrDfSalvar')->name('vinculo_salvar_tr');
        Route::post('vinculo-salvar-tr-gg', 'TransportadorController@vinculoGgTrSalvar')->name('vinculo_salvar_tr_gg');

        //Ctre
        Route::get('ctre', 'TransportadorController@ctreEmissao')->name('ctre_tr');
        Route::get('ctre-composicao/{idEmpresa?}/{idCtre?}', 'TransportadorController@ctreComposicao');
        Route::post('ctre-salvar', 'TransportadorController@ctreEmissaoSalvar')->name('ctre_salvar_tr');
        Route::post('ctre-validar-salvar/{idEmpresa?}/{idCtre?}', 'TransportadorController@ctreValidarSalvar');

        //Rastreabilidade
        Route::get('rastreabilidade', 'TransportadorController@rastreabilidade')->name('rastreabilidade_tr');
    });

    // Rotas Destino Final
    Route::group(['middleware' => 'authDestinoFinal', 'prefix' => 'destino-final'], function() {

        Route::get('/', 'DestinoFinalController@index')->name('painel_df');

        //Configurações
        Route::get('configuracoes', 'DestinoFinalController@configuracoes')->name('config_df');
        Route::post('equipamento-salvar', 'DestinoFinalController@equipamentoSalvar');
        Route::get('equipamento-deletar/{id?}', 'DestinoFinalController@equipamentoDeletar');

        //Vínculos
        Route::get('cadastro-tr-df', 'DestinoFinalController@vinculoTrDf')->name('cadastro_vinculo_df');
        Route::post('vinculo-salvar', 'DestinoFinalController@vinculoTrDfSalvar')->name('vinculo_salvar_df');

        //Ctre
        Route::get('ctre', 'DestinoFinalController@ctreEmissao')->name('ctre_df');
        Route::get('ctre-composicao/{idEmpresa?}/{idCtre?}', 'DestinoFinalController@ctreComposicao');
        Route::get('ctre-tabela/{placaVeiculo?}', 'DestinoFinalController@ctreTabelaAjax');
        Route::post('ctre-salvar', 'DestinoFinalController@ctreEmissaoSalvar');

        //Rastreabilidade
        Route::get('rastreabilidade', 'DestinoFinalController@rastreabilidade')->name('rastreabilidade_df');

    });

    // Rotas Fiscal
    Route::group(['middleware' => 'authAdministrador', 'prefix' => 'fiscal'], function() {
        Route::get('/','FiscalController@index')->name('painel_fs');

        //Rastreabilidade
        Route::get('rastreabilidade', 'FiscalController@rastreabilidade')->name('rastreabilidade_fs');

        // Cadastro Grande Gerador
        Route::get('cadastro-grande-gerador/{empresaId?}', 'FiscalController@cadastroGg')->name('cadastro_gg_fs');
        Route::get('cadastro-gg-ajax', 'FiscalController@cadastroGgAjax');
        Route::get('cadastro-gg-search/{filtro?}/{filtroValor?}', 'FiscalController@cadastroGgSearch')->name('cadastro_gg_search_fs');;
        Route::get('cadastro-gg-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroGgSearchAjax');

        // Cadastro Pequeno Gerador
        Route::get('cadastro-pequeno-gerador/{empresaId?}', 'FiscalController@cadastroPg')->name('cadastro_pg_fs');
        Route::get('cadastro-pg-ajax', 'FiscalController@cadastroPgAjax');
        Route::get('cadastro-pg-search/{filtro?}/{filtroValor?}', 'FiscalController@cadastroPgSearch')->name('cadastro_pg_search_fs');;
        Route::get('cadastro-pg-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroPgSearchAjax');

        // Cadastro Transportador
        Route::get('cadastro-transportadores/{empresaId?}', 'FiscalController@cadastroTr')->name('cadastro_tr_fs');
        Route::get('cadastro-tr-ajax', 'FiscalController@cadastroTrAjax');
        Route::get('cadastro-tr-search/{filtro?}/{filtroValor?}', 'FiscalController@cadastroTrSearch')->name('cadastro_tr_search_fs');;
        Route::get('cadastro-tr-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroTrSearchAjax');
        Route::get('vinculos-tr-vl-ajax/{empresaId?}', 'FiscalController@vinculosTrVeiculosAjax');
        Route::get('vinculos-tr-eq-ajax/{empresaId?}', 'FiscalController@vinculosTrEquipamentosAjax');

        // Cadastro Destino Final
        Route::get('cadastro-destinos/{empresaId?}', 'FiscalController@cadastroDf')->name('cadastro_df_fs');
        Route::get('cadastro-df-ajax', 'FiscalController@cadastroDfAjax');
        Route::get('cadastro-df-search/{filtro?}/{filtroValor?}', 'FiscalController@cadastroDfSearch')->name('cadastro_df_search_fs');;
        Route::get('cadastro-df-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroDfSearchAjax');

        // Cadastro Cooperativas
        Route::get('cadastro-cooperativa/{empresaId?}', 'FiscalController@cadastroCr')->name('cadastro_cr_fs');
        Route::get('cadastro-cr-ajax', 'FiscalController@cadastroCrAjax');
        Route::get('cadastro-cr-search/{filtro?}/{filtroValor?}', 'FiscalController@cadastroCrSearch')->name('cadastro_cr_search_fs');
        Route::get('cadastro-cr-search-ajax/{filtro?}/{filtroValor?}', 'FiscalController@cadastroCrSearchAjax');

        Route::get('cadastros-por-periodo/{periodo?}', 'FiscalController@cadastrosPorPeriodo');

        Route::get('rastreabilidade-ajax', 'FiscalController@rastreabilidadeAjax');
        Route::get('rastreabilidade-logs/{ctreId?}', 'FiscalController@rastreabilidadeLogsAjax');

        Route::get('ctre-por-periodo-home-fiscal/{periodo?}/{dataInicio?}/{dataFinal?}', 'FiscalController@ctrePorPeriodoHomeFiscalAjax');

        Route::get('ctre-por-periodo-fiscal/{periodo?}/{empresaId?}', 'FiscalController@ctrePorPeriodoFiscalAjax');
        ;
        Route::get('residuos', 'ResiduosController@index')->name('residuo_fs');
        Route::post('residuos/store', 'ResiduosController@store')->name('residuo_store_fs');

        Route::get('residuos/{id}/edit', 'ResiduosController@edit')->name('residuo_edit_fs');
        Route::post('residuos/{id}/update', 'ResiduosController@update')->name('residuo_update_fs');
        Route::get('residuos/{id}/destroy', 'ResiduosController@destroy')->name('residuo_destroy_fs');

    });

    // Rotas para o Controller Gerenciador, que é o controller principal dos tipos de empresa

    //CRUD USUÁRIOS
    Route::post('user/store/','UsuariosController@store')->name('store_user');
    Route::get('user/{id}/show','UsuariosController@edit')->name('get_usuario');
    Route::post('user/update/{id}/{rota}','UsuariosController@update')->name('update_user');
    Route::get('user/destroy/{id}','UsuariosController@destroy')->name('destroy_user');

    // Store perfil
    Route::post('/perfil/store','PermissoesController@store')->name('perfil.store');


    // Edit perfil
    Route::get('/perfil/edit/{id}','PermissoesController@edit')->name('perfil.edit');

    // Update perfil
    Route::post('/perfil/update/{id}','PermissoesController@update')->name('perfil.update');

    // Destroy perfil
    Route::get('/perfil/delete/{id}','PermissoesController@destroy')->name('perfil.delete');

    Route::get('configuracoes', 'Gerenciador\PainelController@configuracoes')->name('configuracoes');
    Route::post('equipamento-salvar', 'Gerenciador\PainelController@equipamentoSalvar');
    Route::get('equipamento-deletar/{id?}', 'Gerenciador\PainelController@equipamentoDeletar');
    Route::post('veiculo-salvar/{id?}', 'Gerenciador\PainelController@veiculoSalvar');
    Route::get('veiculo-deletar/{id?}', 'Gerenciador\PainelController@veiculoDeletar');
    Route::get('vinculo-tr-df-deletar/{id?}', 'Gerenciador\PainelController@vinculoTrDfDeletar');
    Route::get('vinculo-gg-tr-deletar/{id?}', 'Gerenciador\PainelController@vinculoGgTrDeletar');
    Route::get('ctre-balanco-periodo/{dataInicial?}/{dataFinal?}', 'Gerenciador\PainelController@ctreBalancoPeriodo');
    Route::get('vinculos-por-periodo/{periodo?}', 'Gerenciador\PainelController@vinculosPorPeriodo');

});

Route::any('/api/cnpj-cliente', 'ApiController@cnpjClienteGerenciamento');
Route::get('/api/cnpj-empresa/{cnpj}', 'ApiController@cnpjEmpresa');

Route::get('/geolocation', 'GeoLocationController@getEnderecoEmpresas');
Route::get('/downloadJSONFile/{tipo?}', array('as'=> 'downloadJSONFile', 'uses' => 'GeoLocationController@downloadJsonGeolocation'));
Route::get('/readGeolocation/{tipo?}/{filter?}/{termoPesquisado?}', 'GeoLocationController@readJsonGeolocation');
Route::get('/codamlurb', 'GeoLocationController@codAmlurb');

Route::group(['middleware' => 'auth', 'prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});


Route::get('get-location-from-ip',function(){
    $ip = \Request::ip();
    $data = \Location::get($ip);
    pd($data, $ip);
});


/* ====================================================== */
