<?php

// config params para instanciar

return [

    'identificacao_soap' => env('IDENTIFICACAO_SOAP', ''),
    'chave_soap' => env('CHAVE_SOAP', ''),
    'url_wsdl_prefeitura' => env('URL_WSDL_PREFEITURA', ''),
    'url_wsdl_prefeitura_consulta' => env('URL_WSDL_PREFEITURA_CONSULTA', '')

];
