<?php

return [

    'empresas_tipo' => [
        'grande_gerador' => 1,
        'pequeno_gerador' => 2,
        'transportador' => 3,
        'destino_final_reciclado' => 4,
        'cooperativa_residuos' => 5,
        'fiscal' => 6,
        'administrador' => 7,
        'orgao_publico' => 8,
        'servico_saude' => 9,
        'condominio_misto' => 10
    ],

    'empresas_sigla' => [
        'grande_gerador' => 'GG',
        'pequeno_gerador' => 'PG',
        'transportador' => 'TR',
        'destino_final_reciclado' => 'DR',
        'cooperativa_residuos' => 'CO',
        'fiscal' => 'FS',
        'administrador' => 'AD',
        'orgao_publico' => 'OP',
        'servico_saude' => 'SS',
        'condominio_misto' => 'CM',
    ],

    'documentos' => [
        'empresa_cartao_cnpj' => 1,
        'transportador_contrato_social' => 2,
        'transportador_doc_ccm' => 3,
        'transportador_certidao_falencia_concordata' => 4,
        'transportador_balanco_financeiro' => 5,
        'transportador_demonstrativo_contabil' => 6,
        'transportador_certidao_negativa_municipal' => 7,
        'transportador_certidao_negativa_estadual' => 8,
        'transportador_certidao_negativa_federal' => 9,
        'transportador_anotacao_responsabilidade_tecnica' => 10,
        'transportador_rg_responsavel_tecnico' => 11,
        'transportador_rg_num_responsavel_tecnico' => 12,
        'transportador_nome_responsavel_tecnico' => 13,
        'transportador_cpf_responsavel_tecnico' => 14,
        'transportador_cpf_num_responsavel_tecnico' => 15,
        'transportador_certificado_dispensa_licenca' => 16,
        'transportador_licenca_previa' => 17,
        'transportador_licenca_instalacao' => 18,
        'transportador_licenca_operacao' => 19,
        'transportador_alvara_prefeitura' => 20,
        'transportador_ctf_ibama' => 21,
        'transportador_avcb' => 22,
        'condominio_sindico_rg' => 23,
        'condominio_sindico_cpf' => 24,
        'condominio_ata' => 25,
        'condominio_contribuicao' => 26,
        'empresa_iptu' => 27,
        'empresa_num_iptu' => 28,
        'numero_rg_socio_file' => 29,
        'numero_cpf_socio_file' => 30,
        'numero_rg_socio_2_file' => 31,
        'numero_cpf_socio_2_file' => 32,
        'numero_rg_socio_3_file' => 33,
        'numero_cpf_socio_3_file' => 34,
        'condominio_procuracao' => 35,
        'licenka' => 36,
        'transportador_crea_responsavel_tecnico' => 37,
        'transportador_crea_num_responsavel_tecnico' => 38,
        'transportador_crf_caixa' => 39
    ],
    'documentos_alterado' => [
        'empresa_cartao_cnpj_alterado' => 1,
        'transportador_contrato_social_alterado' => 2,
        'transportador_doc_ccm_alterado' => 3,
        'transportador_certidao_falencia_concordata_alterado' => 4,
        'transportador_balanco_financeiro_alterado' => 5,
        'transportador_demonstrativo_contabil_alterado' => 6,
        'transportador_certidao_negativa_municipal_alterado' => 7,
        'transportador_certidao_negativa_estadual_alterado' => 8,
        'transportador_certidao_negativa_federal_alterado' => 9,
        'transportador_anotacao_responsabilidade_tecnica_alterado' => 10,
        'transportador_rg_responsavel_tecnico_alterado' => 11,
        'transportador_rg_num_responsavel_tecnico_alterado' => 12,
        'transportador_nome_responsavel_tecnico_alterado' => 13,
        'transportador_cpf_responsavel_tecnico_alterado' => 14,
        'transportador_cpf_num_responsavel_tecnico_alterado' => 15,
        'transportador_certificado_dispensa_licenca_alterado' => 16,
        'transportador_licenca_previa_alterado' => 17,
        'transportador_licenca_instalacao_alterado' => 18,
        'transportador_licenca_operacao_alterado' => 19,
        'transportador_alvara_prefeitura_alterado' => 20,
        'transportador_ctf_ibama_alterado' => 21,
        'transportador_avcb_alterado' => 22,
        'condominio_sindico_rg_alterado' => 23,
        'condominio_sindico_cpf_alterado' => 24,
        'condominio_ata_alterado' => 25,
        'condominio_contribuicao_alterado' => 26,
        'empresa_iptu_alterado' => 27,
        'empresa_num_iptu_alterado' => 28,
        'numero_rg_socio_file_alterado' => 29,
        'numero_cpf_socio_file_alterado' => 30,
        'numero_rg_socio_2_file_alterado' => 31,
        'numero_cpf_socio_2_file_alterado' => 32,
        'numero_rg_socio_3_file_alterado' => 33,
        'numero_cpf_socio_3_file_alterado' => 34,
        'condominio_procuracao_alterado' => 35,
        'licenka_alterado' => 36,
        'transportador_crea_responsavel_tecnico_alterado' => 37,
        'transportador_crea_num_responsavel_tecnico_alterado' => 38,
        'transportador_crf_caixa_alterado' => 39
    ],


    'status' => [
        'ativo' => 1,
        'inativo' => 2,
        'excluido' => 3, // veiculo / equipamento
        'em_aberto' => 4, // ctre
        'expirado' => 5, // ctre
        'finalizado' => 6, // ctre
        'pago' => 7, // boleto
        'pagamento_pendente' => 8, // boleto / veiculo / equipamento
        'cancelado' => 9,
        'autodeclarado' => 10, // empresa
        'aguardando_revisao' => 11,
        'vencido' => 12, // boleto
        'expirado_finalizado' => 13, // ctre
        'invalidado' => 14, // ctre
        'inconsistente' => 15,
        'revisado' => 16,
        'em_analise_amlurb' => 17,
        'vinculos_pendentes' => 18,
        'pre_cadastro' => 19
    ],

    'termos_de_uso' => [
        'geradores' => 1,
        'transportadores' => 2
    ],

    'ramo_atividade' => [
        'industrias' => 1,
        'servicos_comercio' => 2,
        'tratamento_residuos' => 3,
        'cooperativa' => 4,
        'orgao_publico' => 5,
        'destino_final' => 6,
        'condominio_misto' => 7,
        'servico_saude' => 8,
        'transportador' => 9,

    ],

    'tipo_atividade' => [
        'transportador_residuos' => 46,
        'servico_de_administracao_municipal_direta' => 53,
        'servico_de_administracao_municipal_indireta' => 54,
    ],

    'estabelecimento_tipo' => [
        'predio' => 1,
        'shopping' => 2,
        'casa' => 3,
        'galpao' => 4,
        'outros' => 5
    ],

    'equipamento_tipo' => [
        'contentor_plastico' => 1,
        'container_metalico' => 2,
        'caixa_compactadora' => 3,
        'compactainer_estacionario' => 4,
        'caixa_brooks' => 5,
        'cacamba' => 6,
        'big_bag' => 7,
        'tambor_metalico' => 8,
        'bombona_plastica' => 9,
        'iso_container' => 10
    ],

    'tipos_boleto' => [
        'equipamento' => 'equipamento',
        'veiculo' => 'veiculo',
        'qrcode' => 'qrcode'
    ],

    'tipos_qrcode' => [
        'empresa' => 'empresa',
        'equipamento' => 'equipamento',
        'veiculo' => 'veiculo'
    ],

    // boletos PRODAM
    'codigo_tipo_servico' => [
        'gerador' => '9776',
        'transportador' => '9782',
        'transbordo' => '4828',
        'veiculo' => '9785'
    ],

    'responsavel_empreendimento' => [
        'sindico' => 'Síndico',
        'socio' => 'Sócio',
        'socio2' => 'Sócio 3',
        'socio3' => '9785',
    ]



];
