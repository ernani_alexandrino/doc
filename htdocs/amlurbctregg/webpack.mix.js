let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/font-awesome/fonts/', 'public/fonts');

mix.babel([
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/jquery-mask-plugin/src/jquery.mask.js',
    'node_modules/tablesorter/dist/js/jquery.tablesorter.js',
    'node_modules/chart.js/dist/Chart.js',
    'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
    'node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js',
    'resources/assets/js/jquery.dataTables.min.js',
    'resources/assets/js/pipeline.js',

    // 'mapaSimples.js',
    'resources/assets/js/mapaMarkerclusterer.js',
], 'public/js/plugins.js');

mix.babel([
    'resources/assets/js/funcoes.js',
    'resources/assets/js/scripts.js',
    'resources/assets/js/cadastroEmpresa.js',
    'resources/assets/js/ctre.js',
    'resources/assets/js/ctre-datatables.js',
    'resources/assets/js/equipamentos.js',
    'resources/assets/js/clientes-fornecedores.js',
    'resources/assets/js/vinculos.js',
    'resources/assets/js/pagamentos.js',
    'resources/assets/js/faq.js'
], 'public/js/amlurb.js');

mix.js([
    'resources/assets/js/gerador.js',
], 'public/js/gerador.js');

mix.js([
    'resources/assets/js/transportador.js',
], 'public/js/transportador.js');

mix.js([
    'resources/assets/js/destino-final.js',
], 'public/js/destino-final.js');

mix.js([
    'resources/assets/js/moduloFiscal.js',
    'resources/assets/js/rastreabilidade.js'
], 'public/js/fiscal.js');