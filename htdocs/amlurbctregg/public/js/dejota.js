
var callModalEdit = function (url, modal) {

    modal = $(modal);

    modal.empty();

    $.get(url, function (retorno) {
        modal.html(retorno);
        modal.modal('show');
    }).error(function (e) {
        //console.log(e);
    });
};

function editarUsuario(id) {

    $.ajax({
        type: 'get',
        url: '/painel/user/' + id + '/show',
        success: function (data) {

            $('#modalEditUsuario').html(data);
            $('#modalEditUsuario').modal();
            $('.datepicker').datepicker({
                language: "pt-BR",
                dateFormat: 'dd/mm/yy',

            });

            InputMask();
        }
    });
}

function cadastrarUsuario() {
    $('#modalCadastroUsuario').modal();
}

function cadastrarResiduo() {
    $('#modalCadastroResiduo').modal();
}

function editarResiduos(id) {

    $.ajax({
        type: 'get',
        url: '/painel/fiscal/residuos/' + id + '/edit',
        success: function (data) {

            $('#modalEditResiduos').html(data);
            $('#modalEditResiduos').modal();
            $('.datepicker').datepicker({
                language: "pt-BR",
                dateFormat: 'dd/mm/yy',

            });
        }
    });
}
