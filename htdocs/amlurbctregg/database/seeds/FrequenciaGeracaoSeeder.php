<?php

use Amlurb\Models\FrequenciaGeracao;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class FrequenciaGeracaoSeeder extends Seeder
{

    public $table = 'frequencia_geracao';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        FrequenciaGeracao::truncate();

        DB::table($this->table)->insert([
            'nome' => '0 a 200 litros',
            'quantidade' => 1,
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => '2017-09-11 13:21:00'
        ]);

        DB::table($this->table)->insert([
            'nome' => '201 a 500 litros',
            'quantidade' => 201,
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => '2017-09-11 13:21:00'
        ]);

        DB::table($this->table)->insert([
            'nome' => '501 a 1000 litros',
            'quantidade' => 501,
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => '2017-09-11 13:21:00'
        ]);

        DB::table($this->table)->insert([
            'nome' => 'Acima de 1000 litros',
            'quantidade' => 1001,
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => '2017-09-11 13:21:00'
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
