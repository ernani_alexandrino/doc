<?php

use Amlurb\Models\Residuo;
use Illuminate\Database\Seeder;

class ResiduosSeeder extends Seeder
{

    public $table = 'residuos';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Residuo::truncate();

        DB::table($this->table)->insert([
            'codigo' => 'R001',
            'nome' => 'Resíduos Não Recicláveis classe II (orgânicos, de banheiro, cozinha e etc)',
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'codigo' => 'R002',
            'nome' => 'Resíduos Não Recicláveis classe I (contaminados, perigosos e etc)',
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'codigo' => 'R003',
            'nome' => 'Resíduos Recicláveis (papéis, plásticos, vidros e etc)',
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'codigo' => 'R004',
            'nome' => 'Metais Ferrosos (metal, ferro e etc)',
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'codigo' => 'R005',
            'nome' => 'Metais Não Ferrosos (alumínio, cobre e etc)',
            'created_at' => now()
        ]);

        /* -- 
        DB::table($this->table)->insert([
            'codigo' => 'R006',
            'nome' => 'Resíduos de saúde (ambulatorial, clínicas e etc)',
            'created_at' => now()
        ]);
        */

        DB::table($this->table)->insert([
            'codigo' => 'R007',
            'nome' => 'Resíduos orgânicos (resto de comida, poda e etc)',
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'codigo' => 'R008',
            'nome' => 'Resíduos líquidos classe II (efluentes, lavagem e etc)',
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'codigo' => 'R009',
            'nome' => 'Resíduos líquidos classe I (óleos, solventes, tintas e etc)',
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'codigo' => 'R010',
            'nome' => 'Resíduo de Lodo classe I (ETE, ETA e etc))',
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'codigo' => 'R011',
            'nome' => 'Resíduos de Lodo classe II (ETE, ETA e etc)',
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
