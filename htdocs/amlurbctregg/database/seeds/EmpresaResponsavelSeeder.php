<?php

use Amlurb\Models\EmpresaResponsavel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class EmpresaResponsavelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $table = 'empresa_responsavel';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        EmpresaResponsavel::truncate();


        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
