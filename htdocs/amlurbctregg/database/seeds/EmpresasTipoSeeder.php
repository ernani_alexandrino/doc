<?php

use Amlurb\Models\EmpresasTipo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class EmpresasTipoSeeder extends Seeder
{
    public $table = 'empresas_tipo';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        EmpresasTipo::truncate();

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.grande_gerador'),
            'nome' => 'Grande Gerador',
            'sigla' => \config('enums.empresas_sigla.grande_gerador'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.pequeno_gerador'),
            'nome' => 'Pequeno Gerador',
            'sigla' => \config('enums.empresas_sigla.pequeno_gerador'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.transportador'),
            'nome' => 'Transportador',
            'sigla' => \config('enums.empresas_sigla.transportador'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.destino_final_reciclado'),
            'nome' => 'Destino Final',
            'sigla' => \config('enums.empresas_sigla.destino_final_reciclado'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.cooperativa_residuos'),
            'nome' => 'Cooperativa Ambiental',
            'sigla' => \config('enums.empresas_sigla.cooperativa_residuos'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.fiscal'),
            'nome' => 'Fiscal',
            'sigla' => \config('enums.empresas_sigla.fiscal'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.administrador'),
            'nome' => 'Administrador',
            'sigla' => \config('enums.empresas_sigla.administrador'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.orgao_publico'),
            'nome' => 'Órgão Público',
            'sigla' => \config('enums.empresas_sigla.orgao_publico'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.servico_saude'),
            'nome' => 'Serviço de Saúde',
            'sigla' => \config('enums.empresas_sigla.servico_saude'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.empresas_tipo.condominio_misto'),
            'nome' => 'Condomínio Misto',
            'sigla' => \config('enums.empresas_sigla.condominio_misto'),
            'status_id' => \config('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
