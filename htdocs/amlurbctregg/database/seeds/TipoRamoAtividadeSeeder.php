<?php

use Amlurb\Models\TipoRamoAtividade;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class TipoRamoAtividadeSeeder extends Seeder
{
    public $table = 'tipo_ramo_atividade';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        TipoRamoAtividade::truncate();

        // INDUSTRIA

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Minerais não metálicos',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria Metalúrgica',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Material Elétrico e de Comunicação',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Energia',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Equipamentos Elétricos',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Material de Transporte',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Artefatos de Madeira',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Mobiliário',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Artefatos de Papel e Papelão',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Borracha',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Couros, Peles e Produtos similares (não inclui cançado)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria Química',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Produtos Farmacêuticos, Medicinais e Veterinários',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Produtos de Perfumaria, Cosméticos e Limpeza',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Artefatos de Plástico ',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria Têxtil',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Vestuário, Calçados e Artefatos de Tecidos',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Produtos Alimentícios ',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Bebidas',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Fumo',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria Editorial e Gráfica',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Construção Civil',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Utilidade Pública',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústria de Extração e Tratamento de Minerais',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Outros',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        // SERVIÇOS E COMÉRCIOS

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Comércio Atacadista',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Comércio Varejista',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Tecnologia e Informática',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Transporte',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Comunicação e Marketing',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Hotelaria e similares (Hotel, Motel, Pousada, etc.)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Alimentação (bar, restaurantes, lanchonetes, etc.)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Lavanderia',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Reparação e Manutenção (oficinas, consertos em geral)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços Comerciais (escritório)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Entretenimento e Diversão',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Escritório Geral de Administração',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Entidades Financeiras',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Administração de Imóveis',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Cooperativas (não ambientais)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Fundações e Atividades não lucrativas (ONGs, Associações, entidades filantrópicas, etc.)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Ensino (Escolas, Faculdades, Universidades, etc.)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços de Agências Bancárias',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Estacionamento',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Outros',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);


        // TRANSPORTE E TRATAMENTO

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.tratamento_residuos'),
            'nome' => 'Transportador de Resíduos',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.tratamento_residuos'),
            'nome' => 'Comércio de Aparas e Resíduo',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.tratamento_residuos'),
            'nome' => 'Comércio de Sucatas Ferrosas e Não Ferrosas',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.tratamento_residuos'),
            'nome' => 'Depósito de Resíduos',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.tratamento_residuos'),
            'nome' => 'Tratamento de Resíduos (blendagem, transbordo, etc)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.tratamento_residuos'),
            'nome' => 'Outros',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        // COOPERATIVA

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.cooperativa'),
            'nome' => 'Cooperativa Ambiental',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        // ORGAO PÚBLICO

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.orgao_publico'),
            'nome' => 'Serviço de Administração Municipal Direta',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.orgao_publico'),
            'nome' => 'Serviço de Administração Municipal Indireta',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.orgao_publico'),
            'nome' => 'Serviço de Administração Estadual Direta',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.orgao_publico'),
            'nome' => 'Serviço de Administração Estadual Indireta',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.orgao_publico'),
            'nome' => 'Serviço de Administração Federal Direta',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.orgao_publico'),
            'nome' => 'Serviço de Administração Federal Indireta',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.orgao_publico'),
            'nome' => 'Outros',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        // DESTINO FINAL

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Aterro Sanitário',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Aterro Industrial Controlado',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Reciclagem',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Incinerador',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Coprocessamento',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Rerrefino',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Compostagem',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Recuperação',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Tratamento Térmico',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Outros',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        // CONDOMINIO MISTO

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.condominio_misto'),
            'nome' => 'Shopping',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.condominio_misto'),
            'nome' => 'Galeria',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.condominio_misto'),
            'nome' => 'Condomínios Comerciais',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.condominio_misto'),
            'nome' => 'Condomínios Residênciais com Comércio e/ou Serviço',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.condominio_misto'),
            'nome' => 'Outros',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        // SERVIÇOS DE SAÚDE

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servico_saude'),
            'nome' => 'Serviço Comercial de Saúde (farmácia, etc)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servico_saude'),
            'nome' => 'Serviço de Análise de Saúde (laboratório, etc)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servico_saude'),
            'nome' => 'Serviço de Tratamento de Saúde (hospital, pronto-socorro, etc)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servico_saude'),
            'nome' => 'Serviço de Consulta de Saúde (consultórios, clínicas, etc)',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'ramo_atividade_id' => Config::get('enums.ramo_atividade.servico_saude'),
            'nome' => 'Outros',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
