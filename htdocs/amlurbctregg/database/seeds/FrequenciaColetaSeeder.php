<?php

use Amlurb\Models\FrequenciaColeta;
use Illuminate\Database\Seeder;

class FrequenciaColetaSeeder extends Seeder
{

    public $table = 'frequencia_coleta';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        FrequenciaColeta::truncate();

        DB::table($this->table)->insert([
            'nome' => 'Uma vez por semana.',
            'quantidade' => 1,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => 'Duas vezes por semana.',
            'quantidade' => 2,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => 'Três vezes por semana.',
            'quantidade' => 3,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => 'Quatro vezes por semana.',
            'quantidade' => 4,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => 'Cinco ou mais vezes por semana.',
            'quantidade' => 5,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
