<?php

use Amlurb\Models\Equipamento;
use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Config;

class EquipamentosSeeder extends Seeder
{
    public $table = 'equipamentos';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Equipamento::truncate();

        $tipos_equipamentos = [
            Config::get('enums.equipamento_tipo.contentor_plastico') => [
                '200 a 400 litros', '401 a 600 litros', '601 a 800 litros',
                '801 a 1000 litros', '1000 a 1200 litros', '1201 a 1400 litros',
                '1401 a 1600 litros'
            ],
            Config::get('enums.equipamento_tipo.container_metalico') => [
                '0,20 a 0,40 m³', '0,41 a 0,60 m³', '0,61 a 0,80 m³',
                '0,81 a 1,00 m³', '1,01 a 1,20 m³'
            ],
            Config::get('enums.equipamento_tipo.caixa_compactadora') => [
                '7,00 a 10,00 m³', '10,01 a 13,00 m³', '13,01 a 16,00 m³',
                '16,01 a 19,00 m³', '19,01 a 22,00 m³'
            ],
            Config::get('enums.equipamento_tipo.compactainer_estacionario') => [
                '7,00 a 10,00 m³', '10,01 a 13,00 m³', '13,01 a 16,00 m³',
                '16,01 a 19,00 m³', '19,01 a 21,00 m³'
            ],
            Config::get('enums.equipamento_tipo.caixa_brooks') => [
                '5,00 a 8,00 m³', '8,01 a 11,00 m³', '11,01 a 14,00 m³',
                '14,01 a 17,00 m³'
            ],
            Config::get('enums.equipamento_tipo.cacamba') => [
                '7,00 a 13,00 m³', '13,01 a 19,00 m³', '19,01 a 25,00 m³',
                '25,01 a 31,00 m³', '31,01 a 37,00 m³', '37,01 a 43,00 m³'
            ],
            Config::get('enums.equipamento_tipo.big_bag') => [
                '500 a 1000 litros', '1001 a 2000 litros'
            ],
            Config::get('enums.equipamento_tipo.tambor_metalico') => [
                '200 a 400 litros'
            ],
            Config::get('enums.equipamento_tipo.bombona_plastica') => [
                '200 a 400 litros'
            ],
            Config::get('enums.equipamento_tipo.iso_container') => [
                '1000 a 1500 litros'
            ]
        ];

        // acessa cada tipo de equipamento e insere suas varias capacidades
        foreach ($tipos_equipamentos as $id => $equipamento) {

            foreach ($equipamento as $capacidade) {
                DB::table($this->table)->insert([
                    'tipo_equipamento_id' => $id,
                    'status_id' => Config::get('enums.status.ativo'),
                    'capacidade' => $capacidade
                ]);
            }

        }

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
