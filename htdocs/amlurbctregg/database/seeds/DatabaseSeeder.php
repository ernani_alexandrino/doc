<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(StatusSeeder::class);
         $this->call(DocumentosSeeder::class);
         $this->call(TermosDeUsoSeeder::class);
//         $this->call(UsersSeeder::class);
         $this->call(EmpresasTipoSeeder::class);
         $this->call(EmpresasTipoXTermosDeUsoSeeder::class);
         $this->call(FrequenciaColetaSeeder::class);
         $this->call(FrequenciaGeracaoSeeder::class);
         $this->call(ColaboradoresNumeroSeeder::class);
         $this->call(EnergiaConsumoSeeder::class);
         $this->call(RamoAtividadeSeeder::class);
         $this->call(TipoRamoAtividadeSeeder::class);
         $this->call(TipoEquipamentoSeeder::class);
         $this->call(EquipamentosSeeder::class);
         $this->call(ResiduosSeeder::class);
         $this->call(EstabelecimentoTipoSedeer::class);
         $this->call(PrecosSeeder::class);
    }
}
