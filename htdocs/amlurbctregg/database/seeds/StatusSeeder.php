<?php

use Amlurb\Models\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Status::truncate();

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.ativo'),
            'descricao' => 'Ativo',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.inativo'),
            'descricao' => 'Inativo',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.excluido'),
            'descricao' => 'Excluído',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.em_aberto'),
            'descricao' => 'Em Aberto',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.expirado'),
            'descricao' => 'Expirado',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.finalizado'),
            'descricao' => 'Finalizado',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.pago'),
            'descricao' => 'Pago',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.pagamento_pendente'),
            'descricao' => 'Pgto. Pendente',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.cancelado'),
            'descricao' => 'Cancelado',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.autodeclarado'),
            'descricao' => 'Autodeclarado',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => Config::get('enums.status.aguardando_revisao'),
            'descricao' => 'Aguardando Revisão',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => \config('enums.status.vencido'),
            'descricao' => 'Vencido',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => \config('enums.status.expirado_finalizado'),
            'descricao' => 'Expirado/Finalizado',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => \config('enums.status.invalidado'),
            'descricao' => 'Invalidado',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => \config('enums.status.inconsistente'),
            'descricao' => 'Inconsistente',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => \config('enums.status.revisado'),
            'descricao' => 'Revisado',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => \config('enums.status.em_analise_amlurb'),
            'descricao' => 'Em Análise Amlurb',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => \config('enums.status.vinculos_pendentes'),
            'descricao' => 'Vínculos Pendentes',
            'created_at' => now()
        ]);

        DB::table('status')->updateOrInsert([
            'id' => \config('enums.status.pre_cadastro'),
            'descricao' => 'Pré Cadastro',
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
