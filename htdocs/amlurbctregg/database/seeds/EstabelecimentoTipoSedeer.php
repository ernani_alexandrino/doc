<?php

use Amlurb\Models\EstabelecimentoTipo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class EstabelecimentoTipoSedeer extends Seeder
{
    public $table = 'estabelecimento_tipo';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        EstabelecimentoTipo::truncate();

        DB::table($this->table)->insert([
            'id' => Config::get('enums.estabelecimento_tipo.predio'),
            'nome' => 'Prédio Comercial/Misto',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.estabelecimento_tipo.shopping'),
            'nome' => 'Shopping Center',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.estabelecimento_tipo.casa'),
            'nome' => 'Casa',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.estabelecimento_tipo.galpao'),
            'nome' => 'Galpão',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.estabelecimento_tipo.outros'),
            'nome' => 'Outros',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
