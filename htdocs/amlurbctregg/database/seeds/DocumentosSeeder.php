<?php

use Illuminate\Database\Seeder;
use Amlurb\Models\Documento;

class DocumentosSeeder extends Seeder
{
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Documento::truncate();

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.empresa_cartao_cnpj'),
            'nome' => 'empresa_cartao_cnpj',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_contrato_social'),
            'nome' => 'transportador_contrato_social',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_doc_ccm'),
            'nome' => 'transportador_doc_ccm',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_certidao_falencia_concordata'),
            'nome' => 'transportador_certidao_falencia_concordata',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_balanco_financeiro'),
            'nome' => 'transportador_balanco_financeiro',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_demonstrativo_contabil'),
            'nome' => 'transportador_demonstrativo_contabil',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_certidao_negativa_municipal'),
            'nome' => 'transportador_certidao_negativa_municipal',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_certidao_negativa_estadual'),
            'nome' => 'transportador_certidao_negativa_estadual',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_certidao_negativa_federal'),
            'nome' => 'transportador_certidao_negativa_federal',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_anotacao_responsabilidade_tecnica'),
            'nome' => 'transportador_anotacao_responsabilidade_tecnica',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_rg_responsavel_tecnico'),
            'nome' => 'transportador_rg_responsavel_tecnico',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_rg_num_responsavel_tecnico'),
            'nome' => 'transportador_rg_num_responsavel_tecnico',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_nome_responsavel_tecnico'),
            'nome' => 'transportador_nome_responsavel_tecnico',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_cpf_responsavel_tecnico'),
            'nome' => 'transportador_cpf_responsavel_tecnico',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_cpf_num_responsavel_tecnico'),
            'nome' => 'transportador_cpf_num_responsavel_tecnico',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_certificado_dispensa_licenca'),
            'nome' => 'transportador_certificado_dispensa_licenca',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_licenca_previa'),
            'nome' => 'transportador_licenca_previa',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_licenca_instalacao'),
            'nome' => 'transportador_licenca_instalacao',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_licenca_operacao'),
            'nome' => 'transportador_licenca_operacao',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_alvara_prefeitura'),
            'nome' => 'transportador_alvara_prefeitura',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_ctf_ibama'),
            'nome' => 'transportador_ctf_ibama',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_avcb'),
            'nome' => 'transportador_avcb',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.condominio_sindico_rg'),
            'nome' => 'condominio_sindico_rg',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.condominio_sindico_cpf'),
            'nome' => 'condominio_sindico_cpf',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.condominio_ata'),
            'nome' => 'condominio_ata',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.condominio_contribuicao'),
            'nome' => 'condominio_contribuicao',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.empresa_iptu'),
            'nome' => 'empresa_iptu',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.empresa_num_iptu'),
            'nome' => 'empresa_num_iptu',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.numero_rg_socio_file'),
            'nome' => 'numero_rg_socio_file',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.numero_cpf_socio_file'),
            'nome' => 'numero_cpf_socio_file',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.numero_rg_socio_2_file'),
            'nome' => 'numero_rg_socio_2_file',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.numero_cpf_socio_2_file'),
            'nome' => 'numero_cpf_socio_2_file',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.numero_rg_socio_3_file'),
            'nome' => 'numero_rg_socio_3_file',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.numero_cpf_socio_3_file'),
            'nome' => 'numero_cpf_socio_3_file',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.condominio_procuracao'),
            'nome' => 'condominio_procuracao',
            'created_at' => now()
        ]);
        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.licenka'),
            'nome' => 'licenka',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_crea_responsavel_tecnico'),
            'nome' => 'transportador_crea_responsavel_tecnico',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_crea_num_responsavel_tecnico'),
            'nome' => 'transportador_crea_num_responsavel_tecnico',
            'created_at' => now()
        ]);

        DB::table('documentos')->updateOrInsert([
            'id' => Config::get('enums.documentos.transportador_crf_caixa'),
            'nome' => 'transportador_crf_caixa',
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}