<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;
use Amlurb\Models\TermosDeUso;

class TermosDeUsoSeeder extends Seeder
{
    public $table = 'termos_de_uso';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        TermosDeUso::truncate();

        $termos_de_uso_path = base_path() . DIRECTORY_SEPARATOR . 'resources' .
            DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'termos_de_uso' .
            DIRECTORY_SEPARATOR;

        $geradores = File::get($termos_de_uso_path . 'geradores.blade.php');
        $transportadores = File::get($termos_de_uso_path . 'transportadores.blade.php');

        $geradores = str_replace("\n", "<br>", $geradores);
        $transportadores = str_replace("\n", "<br>", $transportadores);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.termos_de_uso.geradores'),
            'termo_uso' => $geradores
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.termos_de_uso.transportadores'),
            'termo_uso' => $transportadores
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
