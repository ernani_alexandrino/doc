<?php

use Amlurb\Models\Preco;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PrecosSeeder extends Seeder
{

    public function run()
    {
        Preco::truncate();

        DB::table('precos')->insert([
            'tipo' => 'grande_gerador',
            'valor' => 208.00,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('precos')->insert([
            'tipo' => 'transportador',
            'valor' => 251.00,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('precos')->insert([
            'tipo' => 'equipamento',
            'valor' => 42.00,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('precos')->insert([
            'tipo' => 'veiculo',
            'valor' => 42.00,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

    }

}