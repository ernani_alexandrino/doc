<?php

use Illuminate\Database\Seeder;
use Amlurb\Models\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(['name' => 'admin', 'email' => 'admin@admin.com.br', 'password' => Hash::make('secret')]);
    }
}
