<?php

use Amlurb\Models\RamoAtividade;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class RamoAtividadeSeeder extends Seeder
{

    public $table = 'ramo_atividade';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        RamoAtividade::truncate();

        DB::table($this->table)->insert([
            'id' => Config::get('enums.ramo_atividade.industrias'),
            'nome' => 'Indústrias',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.ramo_atividade.servicos_comercio'),
            'nome' => 'Serviços e Comércios',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.ramo_atividade.tratamento_residuos'),
            'nome' => 'Transporte, Comércio e Tratamento de Resíduo',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.ramo_atividade.cooperativa'),
            'nome' => 'Cooperativa Ambiental',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.ramo_atividade.orgao_publico'),
            'nome' => 'Órgão Público',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.ramo_atividade.destino_final'),
            'nome' => 'Destino Final de Resíduos',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.ramo_atividade.condominio_misto'),
            'nome' => 'Condomínio Misto',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.ramo_atividade.servico_saude'),
            'nome' => 'Serviço de Saúde',
            'status_id' => Config::get('enums.status.ativo'),
            'created_at' => now()
        ]);


        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
