<?php

use Amlurb\Models\EnergiaConsumo;
use Illuminate\Database\Seeder;

class EnergiaConsumoSeeder extends Seeder
{
    public $table = 'energia_consumo';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        EnergiaConsumo::truncate();

        DB::table($this->table)->insert([
            'nome' => '10 a 50 MWH',
            'quantidade' => 10,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => '51 a 100 MWH',
            'quantidade' => 51,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => '101 a 500 MWH',
            'quantidade' => 101,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => '501 a 1000 MWH',
            'quantidade' => 501,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => 'Acima de 1000 MWH',
            'quantidade' => 1000,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
