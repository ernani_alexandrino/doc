<?php

use Amlurb\Models\TipoEquipamento;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class TipoEquipamentoSeeder extends Seeder
{
    public $table = 'tipo_equipamento';

    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        TipoEquipamento::truncate();

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.contentor_plastico'),
            'nome' => 'Contentor Plástico',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.container_metalico'),
            'nome' => 'Container Metálico',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.caixa_compactadora'),
            'nome' => 'Caixa Compactadora',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.compactainer_estacionario  '),
            'nome' => 'Compactainer Estacionário',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.caixa_brooks'),
            'nome' => 'Caixa Brooks',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.cacamba'),
            'nome' => 'Caçamba Roll-On / Roll-Off',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.big_bag'),
            'nome' => 'Big Bag',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.tambor_metalico'),
            'nome' => 'Tambor Metálico',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.bombona_plastica'),
            'nome' => 'Bombona Plástica',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::table($this->table)->insert([
            'id' => Config::get('enums.equipamento_tipo.iso_container'),
            'nome' => 'ISO Container',
            'status_id' => Config::get('enums.status.ativo'),
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

}