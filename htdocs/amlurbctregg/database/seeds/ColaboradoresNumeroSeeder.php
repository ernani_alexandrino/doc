<?php

use Amlurb\Models\ColaboradoresNumero;
use Illuminate\Database\Seeder;

class ColaboradoresNumeroSeeder extends Seeder
{

    public $table = 'colaboradores_numero';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        ColaboradoresNumero::truncate();

        DB::table($this->table)->insert([
            'nome' => '1 a 50 funcionários.',
            'quantidade' => 50,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => '51 a 200 funcionários.',
            'quantidade' => 200,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => '201 a 500 funcionários.',
            'quantidade' => 500,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => '501 a 1000 funcionários.',
            'quantidade' => 500,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'nome' => 'Acima de 1000 funcionários.',
            'quantidade' => 1001,
            'status_id' => 1,
            'created_at' => now()
        ]);

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
