<?php

use Amlurb\Models\EmpresasTipoXTermosDeUso;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class EmpresasTipoXTermosDeUsoSeeder extends Seeder
{
    public $table = 'empresas_tipo_x_termos_de_uso';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmpresasTipoXTermosDeUso::truncate();

        DB::table($this->table)->insert([
            'termos_id' => Config::get('enums.termos_de_uso.geradores'),
            'empresa_tipo_id' => Config::get('enums.empresas_tipo.grande_gerador'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'termos_id' => Config::get('enums.termos_de_uso.geradores'),
            'empresa_tipo_id' => Config::get('enums.empresas_tipo.pequeno_gerador'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'termos_id' => Config::get('enums.termos_de_uso.transportadores'),
            'empresa_tipo_id' => Config::get('enums.empresas_tipo.transportador'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'termos_id' => Config::get('enums.termos_de_uso.transportadores'),
            'empresa_tipo_id' => Config::get('enums.empresas_tipo.destino_final_reciclado'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'termos_id' => Config::get('enums.termos_de_uso.transportadores'),
            'empresa_tipo_id' => Config::get('enums.empresas_tipo.cooperativa_residuos'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'termos_id' => Config::get('enums.termos_de_uso.geradores'),
            'empresa_tipo_id' => Config::get('enums.empresas_tipo.orgao_publico'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'termos_id' => Config::get('enums.termos_de_uso.geradores'),
            'empresa_tipo_id' => Config::get('enums.empresas_tipo.servico_saude'),
            'created_at' => now()
        ]);

        DB::table($this->table)->insert([
            'termos_id' => Config::get('enums.termos_de_uso.geradores'),
            'empresa_tipo_id' => Config::get('enums.empresas_tipo.condominio_misto'),
            'created_at' => now()
        ]);

    }
}
