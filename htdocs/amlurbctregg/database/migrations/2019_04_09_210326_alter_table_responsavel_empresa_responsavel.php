<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableResponsavelEmpresaResponsavel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::table('empresa_responsavel', function(Blueprint $table)
        {
            $table->dropColumn('responsavel');
        });

        Schema::table('empresa_responsavel', function (Blueprint $table) {
            $table->enum('responsavel', ['sindico', 'socio_principal', 'socio', 'presidente', 'comum'])->after('email')->nullable()->default('comum');
            $table->renameColumn('sindico_rg', 'rg');
            $table->renameColumn('sindico_cpf', 'cpf');
        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
