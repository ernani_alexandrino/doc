<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameEmpresaResiduosToLogisticaResiduos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::rename('empresa_residuos', 'logistica_residuos');

        // associar o transporte de residuos com transportador
        Schema::table('logistica_residuos', function (Blueprint $table) {
            $table->integer('transportador_id')->unsigned()->after('id');
            $table->foreign('transportador_id', 'er_transportador_fk')->references('id')->on('empresas')->onUpdate('cascade')->onDelete('cascade');
        });

        // associar a frequencia de coleta de residuos pelo transportador
        Schema::table('transportador_vinculos', function (Blueprint $table) {
            $table->integer('frequencia_coleta_id')->unsigned()->after('empresa_responsavel_id')->nullable();
            $table->foreign('frequencia_coleta_id', 'tv_frequencia_fk')->references('id')->on('frequencia_coleta')->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('transportador_vinculos', function (Blueprint $table) {
            $table->dropForeign('tv_frequencia_fk');
            $table->dropColumn('frequencia_coleta_id');
        });

        Schema::table('logistica_residuos', function (Blueprint $table) {
            $table->dropForeign('er_transportador_fk');
            $table->dropColumn('transportador_id');
        });

        Schema::rename('logistica_residuos', 'empresa_residuos');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
