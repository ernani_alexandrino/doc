<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvalidacaoCtre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('justificativa_invalidacao_ctre', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('justificativa_id');
            $table->unsignedInteger('ctre_id');
            $table->unsignedInteger('empresa_id');
            $table->foreign('justificativa_id', 'jic_justificativa_fk')->references('id')->on('justificativas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ctre_id', 'jic_ctre_fk')->references('id')->on('ctre')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_id', 'jic_empresa_fk')->references('id')->on('empresas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('justificativa_invalidacao_ctre');
    }
}
