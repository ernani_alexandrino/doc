<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUselessQrcodeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->dropColumn('qrcode');
        });

        Schema::table('empresas_x_equipamentos', function (Blueprint $table) {
            $table->dropColumn('qrcode');
        });

        Schema::dropIfExists('veiculos_qrcode');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::create('veiculos_qrcode', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_veiculo_id')->unsigned();
            $table->string('qrcode');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_veiculo_id')->references('id')->on('empresas_veiculos')->onDelete('no action')->onUpdate('no action');
        });

        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->string('qrcode')->after('codigo_amlurb')->nullable();
        });

        Schema::table('empresas_x_equipamentos', function (Blueprint $table) {
            $table->string('qrcode')->after('codigo_amlurb')->nullable();
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
