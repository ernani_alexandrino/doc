<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJustificativaExclusoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('justificativa_exclusao_veiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('justificativa_id');
            $table->unsignedInteger('veiculo_id');
            $table->unsignedInteger('empresa_id');
            $table->foreign('justificativa_id', 'jev_justificativa_fk')->references('id')->on('justificativas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('veiculo_id', 'jev_veiculo_fk')->references('id')->on('empresas_veiculos')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_id', 'jev_empresa_fk')->references('id')->on('empresas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('justificativa_exclusao_equipamento', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('justificativa_id');
            $table->unsignedInteger('equipamento_id');
            $table->unsignedInteger('empresa_id');
            $table->foreign('justificativa_id', 'jee_justificativa_fk')->references('id')->on('justificativas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('equipamento_id', 'jee_equipamento_fk')->references('id')->on('empresas_x_equipamentos')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_id', 'jee_empresa_fk')->references('id')->on('empresas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('justificativa_exclusao_veiculo');
        Schema::dropIfExists('justificativa_exclusao_equipamento');
    }
}
