<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCtreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctre', function(Blueprint $table) {
            $table->bigInteger('id')->primary();
            $table->integer('empresa_id')->unsigned()->comment("Id da empresa que atualmente está o CTRE. Ex: Se o Gerador emiti para o transportador, o id que estará aqui é o do Transportador.");
            $table->integer('status_id')->unsigned()->default(1);
            $table->integer('ctre_volume_total_id')->unsigned()->nullable();
            $table->char('placa_veiculo', 7);
            $table->string('tipo_veiculo', 100)->nullable();
            $table->timestamp('data_emissao');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
            $table->foreign('ctre_volume_total_id')->references('id')->on('ctre_volume_total')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctre');
    }
}
