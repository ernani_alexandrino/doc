<?php

use Illuminate\Database\Migrations\Migration;

class AlterDataEmissaoCtre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE ctre MODIFY COLUMN data_emissao
          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER deleted_at');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE ctre MODIFY COLUMN data_emissao
          TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER status_id');
    }
}
