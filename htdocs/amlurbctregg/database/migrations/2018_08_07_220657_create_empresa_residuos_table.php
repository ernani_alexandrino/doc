<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaResiduosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_residuos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('residuo_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('empresa_residuos', function (Blueprint $table) {
            $table->foreign('empresa_id', 'er_empresa_fk')->references('id')->on('empresas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('residuo_id', 'er_residuo_fk')->references('id')->on('residuos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_residuos');
    }
}
