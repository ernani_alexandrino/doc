<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaAtivacaoLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas_ativacao_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('empresa_id');
            $table->string('descricao');
            $table->timestamps();

            $table->foreign('user_id',
                'ativacaoLogUser')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_id',
                'ativacaoLogStatus')->references('id')->on('status')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_id',
                'ativacaoLogEmpresa')->references('id')->on('empresas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('empresas_ativacao_logs');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
