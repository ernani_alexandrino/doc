<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResponsibleUserIdToEmpresaAlteracao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('empresa_alteracao', function (Blueprint $table) {
            $table->integer('responsible_user_id')->unsigned()->after('empresa_id');
            $table->foreign('responsible_user_id', 'fk_empresa_alteracao_responsible_users')
                ->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('empresa_alteracao', function (Blueprint $table) {
            $table->dropForeign('fk_empresa_alteracao_responsible_users');
            $table->dropColumn('responsible_user_id');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
