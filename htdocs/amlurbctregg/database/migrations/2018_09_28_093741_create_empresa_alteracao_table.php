<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaAlteracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_alteracao', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('empresa_id');
            $table->unsignedInteger('ramo_atividade_id');
            $table->unsignedInteger('tipo_ramo_atividade_id');
            $table->string('endereco', 255);
            $table->string('numero', 50);
            $table->string('bairro', 100);
            $table->string('cep', 10);
            $table->unsignedInteger('estado_id');
            $table->unsignedInteger('cidade_id');
            $table->string('complemento', 100)->nullable();
            $table->string('ponto_referencia', 255)->nullable();
            $table->string('ie', 15)->nullable();
            $table->string('im', 15)->nullable();
            $table->string('telefone', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id', 'fk_empresa_alteracao_empresas')->references('id')->on('empresas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ramo_atividade_id', 'fk_empresa_alteracao_ramo_atividade')->references('id')->on('ramo_atividade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tipo_ramo_atividade_id', 'fk_empresa_alteracao_tipo_ramo_atividade')->references('id')->on('tipo_ramo_atividade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('estado_id', 'fk_empresa_alteracao_estados')->references('id')->on('estados')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('cidade_id', 'fk_empresa_alteracao_cidades')->references('id')->on('cidades')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('justificativa_empresa_alteracao');
        Schema::dropIfExists('empresa_alteracao');
    }
}
