<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class PopulatePermissions extends Migration
{
    public $table = 'permissions';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::table($this->table)->insert([
            'id' => '1',
            'name' => 'painel-controle',
            'label' => 'Painel de Controle',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '2',
            'name' => 'rastreabilidade',
            'label' => 'Rastrabilidade',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '3',
            'name' => 'cadastro-grandes-geradores',
            'label' => 'Cadastro Grandes Geradores',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '4',
            'name' => 'cadastro-pequenos-geradores',
            'label' => 'Cadastro Pequenos Geradores',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);

        DB::table($this->table)->insert([
            'id' => '5',
            'name' => 'cadastro-transportadores',
            'label' => 'Cadastro Transportadores',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '6',
            'name' => 'cadastro-destino-final',
            'label' => 'Cadastro Destino Final',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '7',
            'name' => 'cadastro-cooperativas',
            'label' => 'Cadastro Coopertativas',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '8',
            'name' => 'cadastro-transbordos',
            'label' => 'Cadastro Transbordos',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '9',
            'name' => 'configuracoes',
            'label' => 'Configuracoes',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '10',
            'name' => 'configuracoes-user',
            'label' => 'Usuarios',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);
        DB::table($this->table)->insert([
            'id' => '11',
            'name' => 'configuracoes-permissions',
            'label' => 'Permissões',
            'parent_id' => 0,
            'level' => 0,
            'type' => 'fiscal',
            
        ]);

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
