<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableJustificativaInconsistenciaVeiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('justificativa_inconsistencia_veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('justificativa_id');
            $table->unsignedInteger('fiscal_id');
            $table->unsignedInteger('empresa_id');
            $table->unsignedInteger('empresas_veiculo_id');
            $table->foreign('justificativa_id', 'jid_justificativa_veiculo_fk')->references('id')->on('justificativas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('fiscal_id', 'jid_fiscal_veiculo_fk')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_id', 'jid_empresa_veiculo_fk')->references('id')->on('empresas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresas_veiculo_id', 'jempresas_veiculo_id_fk')->references('id')->on('empresas_veiculos')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('justificativa_inconsistencia_veiculos');
    }
}
