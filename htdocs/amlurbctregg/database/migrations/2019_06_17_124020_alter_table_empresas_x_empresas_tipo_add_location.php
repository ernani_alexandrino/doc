<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEmpresasXEmpresasTipoAddLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas_x_empresas_tipo', function (Blueprint $table) {
            $table->enum('location', ['cadastro', 'vinculo'])->after('status_id')->default('cadastro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas_x_empresas_tipo', function (Blueprint $table) {
            $table->dropColumn('location');
        });
    }
}
