<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CtreVolumeTotalField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('ctre', function (Blueprint $table) {
            $table->dropColumn('ctre_volume_total_id');
            $table->decimal('volume_total', 10, 3)->nullable()->after('data_validacao_final');
        });

        Schema::dropIfExists('ctre_volume_total');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::create('ctre_volume_total', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->decimal('volume_total', 10, 3);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
        });

        Schema::table('ctre', function (Blueprint $table) {
            $table->dropColumn('volume_total');
            $table->integer('ctre_volume_total_id')->unsigned()->after('status_id')->nullable();
            $table->foreign('ctre_volume_total_id')->references('id')->on('ctre_volume_total')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
