<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsEmpresaResponsavel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::table('empresa_responsavel', function (Blueprint $table){
           $table->enum('responsavel', ['sindico', 'comum', 'outro'])->after('email')->nullable()->default('comum');
           $table->string('sindico_rg', 20)->after('responsavel')->nullable();
           $table->string('sindico_cpf', 20)->after('sindico_rg')->nullable();
            $table->string('nome', 50)->nullable()->change();
        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        Schema::table('empresa_responsavel', function (Blueprint $table) {
            $table->dropColumn('responsavel');
            $table->dropColumn('sindico_rg');
            $table->dropColumn('sindico_cpf');
        });
        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
