<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermosXEmpresasTipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas_tipo_x_termos_de_uso', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('termos_id')->unsigned();
            $table->integer('empresa_tipo_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('termos_id')->references('id')->on('termos_de_uso')->onDelete('no action')->onUpdate('no action');
            $table->foreign('empresa_tipo_id')->references('id')->on('empresas_tipo')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas_tipo_x_termos_de_uso');
    }
}
