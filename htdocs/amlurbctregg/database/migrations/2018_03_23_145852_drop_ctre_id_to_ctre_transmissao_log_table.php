<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropCtreIdToCtreTransmissaoLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $fields =  DB::select("
        select * from INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME='ctre_transmissao_log' AND COLUMN_NAME='ctre_id' 
        ");

        if(!empty($fields)){
            Schema::table('ctre_transmissao_log', function($table)
            {
                $table->dropForeign('ctre_transmissao_log_ibfk_1');
                $table->dropColumn('ctre_id');
            });
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
