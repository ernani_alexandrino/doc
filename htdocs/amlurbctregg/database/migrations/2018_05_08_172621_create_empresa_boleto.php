<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaBoleto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_boleto', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('empresa_id', 0, 1)->indexed();
            $table->string('anoEmissao', 4);
            $table->integer('numeroGuia');
            $table->decimal('valor');
            $table->integer('status_id', 0, 1)->default(8);
            $table->timestamp('dataDeValidade')->nullable();
            $table->timestamp('dataDeVencimento')->nullable();
            $table->timestamp('dataDePagamento')->nullable();
            $table->integer('codTipoServGuia', 0, 1);
            $table->text('arquivoBoleto');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_boleto');
    }
}
