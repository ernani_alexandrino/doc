<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCtreTransmissaoLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::dropIfExists('ctre_transmissao_log');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::create('ctre_transmissao_log', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('ctre_id')->unsigned();
            $table->integer('empresa_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->char('placa_veiculo', 7);
            $table->string('tipo_veiculo', 100)->nullable();
            $table->timestamp('data_emissao');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ctre_id')->references('id')->on('ctre')->onDelete('no action')->onUpdate('no action');
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
