<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportadorVinculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::create('transportador_vinculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transportador_id')->unsigned();
            $table->integer('empresa_vinculada_id')->unsigned();
            $table->integer('empresa_responsavel_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->decimal('volume_residuo', '10', '3')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('transportador_vinculos', function (Blueprint $table) {
            $table->foreign('transportador_id', 'tv_empresa_fk')->references('id')->on('empresas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_vinculada_id', 'tv_empresa_vinculada_fk')->references('id')->on('empresas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_responsavel_id', 'tv_empresa_responsavel_fk')->references('id')->on('empresa_responsavel')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('status_id', 'tv_status_fk')->references('id')->on('status')->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('transportador_vinculos', function (Blueprint $table) {
           $table->dropForeign('tv_status_fk');
           $table->dropForeign('tv_empresa_vinculada_fk');
           $table->dropForeign('tv_empresa_responsavel_fk');
           $table->dropForeign('tv_empresa_fk');
        });

        Schema::dropIfExists('transportador_vinculos');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
