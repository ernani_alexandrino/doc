<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblTiposEmailsXPerfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_email_x_perfil', function (Blueprint $table) {
            $table->integer('tipos_emails_id')->unsigned();
            $table->integer('perfil_id')->unsigned();
            $table->foreign('tipos_emails_id')->references('id')->on('tipos_emails');
            $table->foreign('perfil_id')->references('id')->on('perfis');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_email_x_perfil');
    }
}
