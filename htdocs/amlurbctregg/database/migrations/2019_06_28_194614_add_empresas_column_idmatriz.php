<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresasColumnIdmatriz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('empresas', function ($table) {
            $table->integer('id_matriz')->unsigned()->default(null)->after('id');
            $table->foreign('id_matriz')->references('id')->on('empresas');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('empresas', function ($table) {
            $table->dropColumn('id_matriz');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
