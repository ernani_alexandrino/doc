<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Amlurb\Models\Boleto;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Services\ServiceBoletos;
use Carbon\Carbon;

class UpdateStatusVeiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $boletos = Boleto::where('status_id', \config('enums.status.pago'))
        ->where('tipo', 'veiculo')
        ->get();
        foreach ($boletos as $boleto) {
            $dataValidade = Carbon::parse($boleto->dataDePagamento);
            $veiculo = EmpresasVeiculo::whereId($boleto->boleto_veiculos->veiculo_id)->first();
            if ($veiculo) {
                $veiculo->status_id = 1;
                $veiculo->data_validade = $dataValidade->addYear();
                $veiculo->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
