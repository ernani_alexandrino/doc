<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserPerfilNullables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_perfil', function(Blueprint $table) {
            $table->date('data_nascimento')->nullable()->change();
            $table->string('cargo')->nullable()->change();
            $table->string('telefone')->nullable()->change();
            $table->string('ramal')->nullable()->change();
            $table->string('celular')->nullable()->change();
            $table->string('imagem_perfil')->nullable()->change();
            
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        //
    }
}
