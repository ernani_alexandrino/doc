<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableHistoricoCodigos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historico_codigos', function (Blueprint $table) {
            $table->string('id_base_amlurb')->after('codigo_antigo');
            $table->string('id_limpurb_base_amlurb')->after('id_base_amlurb');
            $table->string('data_cadastro_base_amlurb')->after('id_limpurb_base_amlurb');
            $table->string('data_publicacao_dom_base_amlurb')->after('data_cadastro_base_amlurb');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
