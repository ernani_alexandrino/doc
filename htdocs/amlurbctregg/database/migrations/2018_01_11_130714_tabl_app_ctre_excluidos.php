<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablAppCtreExcluidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coletas_excluidas_app', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->date('data_coleta')->nullable();
            $table->string('destino')->nullable();
            $table->string('gerador')->nullable();
            $table->string('veiculo')->nullable();
            $table->string('motivo')->nullable();
            $table->string('equipamento')->nullable();
            $table->string('qrcode_equipamento')->nullable();
            $table->string('data_hora_coleta_iso')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('no action')->onUpdate('no action');
        

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('coletas_excluidas_app');
    }
}
