<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataValidadeVeiculosEquipamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipamentos', function (Blueprint $table) {
            $table->timestamp('data_validade')->nullable()->after('capacidade');
        });

        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->timestamp('data_validade')->nullable()->after('vencimento_ipva');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipamentos', function (Blueprint $table) {
            $table->dropColumn('data_validade');
        });

        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->dropColumn('data_validade');
        });
    }
}
