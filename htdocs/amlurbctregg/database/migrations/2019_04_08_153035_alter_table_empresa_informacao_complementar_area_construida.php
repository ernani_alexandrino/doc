<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEmpresaInformacaoComplementarAreaConstruida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresa_informacao_complementar', function (Blueprint $table) {
            $table->bigInteger('area_total')->nullable()->change();
            $table->bigInteger('area_construida')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresa_informacao_complementar', function (Blueprint $table) {
            $table->integer('area_total')->nullable()->change();
            $table->integer('area_construida')->nullable()->change();
        });
    }
}
