<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function(Blueprint $table) {
            $table->increments('id');
            $table->text('ip')->nullable();
            $table->text('country_code')->nullable();
            $table->text('region_name')->nullable();
            $table->text('city_name')->nullable();
            $table->text('zip_code')->nullable();
            $table->text('geolocation')->nullable();
            $table->integer('empresa_id')->unsigned();
            $table->string('tabela');
            $table->text('old')->nullable();
            $table->text('new');
            $table->string('action');
            $table->timestamps();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
