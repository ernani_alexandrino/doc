<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropIdToCtreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $fields =  DB::select("
        select * from INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME='ctre' AND COLUMN_NAME='id' 
        ");

        if(!empty($fields)){
            Schema::table('ctre', function($table)
            {
                $table->dropForeign('ctre_ibfk_1');
                $table->dropColumn('id');
            });
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
