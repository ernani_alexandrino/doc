<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJustificativaEmpresaAlteracaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('justificativa_empresa_alteracao', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('justificativa_id');
            $table->unsignedInteger('empresa_alteracao_id');            
            $table->timestamps();
            $table->foreign('justificativa_id', 'fk_justificativa_empresa_alteracao_justificativas')->references('id')->on('justificativas')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_alteracao_id', 'fk_justificativa_empresa_alteracao_empresa_alteracao')->references('id')->on('justificativas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('justificativa_empresa_alteracao');
    }
}
