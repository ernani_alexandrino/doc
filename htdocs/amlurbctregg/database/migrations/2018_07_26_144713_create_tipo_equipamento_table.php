<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoEquipamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::create('tipo_equipamento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 150);
            $table->integer('status_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });

        Schema::table('equipamentos', function (Blueprint $table) {
            $table->dropColumn('nome');
            $table->dropColumn('codigo');
            $table->integer('tipo_equipamento_id')->unsigned()->after('id');
            $table->foreign('tipo_equipamento_id', 'tipo_equipamento_fk')->references('id')->on('tipo_equipamento')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::table('equipamentos', function (Blueprint $table) {
            $table->dropForeign('tipo_equipamento_fk');
            $table->dropColumn('tipo_equipamento_id');
            $table->string('codigo', 50)->nullable()->after('status_id');
            $table->string('nome', 150)->after('codigo');
        });

        Schema::dropIfExists('tipo_equipamento');

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
