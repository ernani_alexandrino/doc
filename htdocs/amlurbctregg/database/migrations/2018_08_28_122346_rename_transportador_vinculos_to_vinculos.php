<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTransportadorVinculosToVinculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('transportador_vinculos', function (Blueprint $table) {
            $table->dropForeign('tv_empresa_fk');
            $table->dropColumn('transportador_id');
        });

        Schema::rename('transportador_vinculos', 'vinculos');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('vinculos', function (Blueprint $table) {
            $table->integer('transportador_id')->unsigned()->after('vinculador_id');
            $table->foreign('transportador_id', 'tv_empresa_fk')->references('id')->on('empresas')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::rename('vinculos', 'transportador_vinculos');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
