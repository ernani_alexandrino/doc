<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblResiduos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET foreign_key_checks=0");
        Schema::table('residuos', function(Blueprint $table) {
            $table->integer('tabela_onu_id')->unsigned()->after('nome');
            $table->foreign('tabela_onu_id')->references('id')->on('tabela_onu')->onDelete('no action')->onUpdate('no action');
            
            
            });
            DB::statement("SET foreign_key_checks=1");	 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
