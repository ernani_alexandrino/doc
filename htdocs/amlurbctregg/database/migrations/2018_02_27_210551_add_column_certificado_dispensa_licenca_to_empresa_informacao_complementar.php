<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCertificadoDispensaLicencaToEmpresaInformacaoComplementar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::table('empresa_informacao_complementar', function(Blueprint $table) {
            $table->string('F')->after('anotacao_responsabilidade_tecnica_vencimento')->nullable();
            $table->date('certificado_dispensa_licenca_emissao')->after('certificado_dispensa_licenca')->nullable();
            $table->date('certificado_dispensa_licenca_vencimento')->after('certificado_dispensa_licenca_emissao')->nullable();
        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
