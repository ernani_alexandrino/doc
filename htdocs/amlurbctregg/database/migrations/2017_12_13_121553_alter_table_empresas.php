<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEmpresas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::table('empresas', function (Blueprint $table) {
            $table->dropUnique('empresas_cnpj_unique');
            $table->string('cnpj', 50)->nullable()->change();
            $table->string('telefone', 50)->nullable()->change();
            $table->string('telefone_responsavel', 50)->nullable()->change();
            $table->string('celular', 50)->nullable()->change();
        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
