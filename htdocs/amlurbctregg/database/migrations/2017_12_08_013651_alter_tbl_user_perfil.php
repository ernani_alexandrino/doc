<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTblUserPerfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_perfil', function(Blueprint $table) {
            $table->date('data_nascimento')->nullable()->after('user_id');
            $table->string('ramal')->nullable()->after('telefone');
            $table->string('celular')->nullable()->after('ramal');
            
            
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
