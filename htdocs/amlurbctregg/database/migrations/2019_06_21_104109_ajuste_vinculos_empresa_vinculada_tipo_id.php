<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\Vinculos;

class AjusteVinculosEmpresaVinculadaTipoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $vinculos = Vinculos::all();
        foreach ($vinculos as $vinculo) {
            $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $vinculo->empresa_vinculada_id)
            ->where('empresa_tipo_id', '!=', 12)
            ->first();
            if ($empresaTipo) {
                $vinculo->empresa_vinculada_tipo_id = $empresaTipo->empresa_tipo_id;
                $vinculo->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
