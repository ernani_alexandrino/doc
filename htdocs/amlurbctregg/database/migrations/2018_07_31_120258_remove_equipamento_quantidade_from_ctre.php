<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEquipamentoQuantidadeFromCtre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ctre_composicao', function (Blueprint $table) {
            $table->dropColumn('equipamento_quantidade');
        });

        Schema::table('ctre_transmissao_composicao_log', function (Blueprint $table) {
            $table->dropColumn('equipamento_quantidade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ctre_composicao', function (Blueprint $table) {
            $table->integer('equipamento_quantidade')->after('equipamento_id');
        });

        Schema::table('ctre_transmissao_composicao_log', function (Blueprint $table) {
            $table->integer('equipamento_quantidade')->after('equipamento_id');
        });
    }
}
