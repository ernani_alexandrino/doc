<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReviewerUserIdToEmpresaAlteracao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('empresa_alteracao', function (Blueprint $table) {
            $table->integer('reviewer_user_id')->unsigned()->nullable()->after('responsible_user_id');
            $table->foreign('reviewer_user_id', 'fk_empresa_alteracao_reviewer_users')
                ->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('empresa_alteracao', function (Blueprint $table) {
            $table->dropForeign('fk_empresa_alteracao_reviewer_users');
            $table->dropColumn('reviewer_user_id');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
