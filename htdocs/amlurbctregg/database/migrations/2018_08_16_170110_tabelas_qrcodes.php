<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelasQrcodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::create('qrcodes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('codigo');
            $table->integer('status_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('status_id', 'qrcode_status_fk')->references('id')->on('status')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::rename('empresa_qrcode', 'qrcode_empresas');
        Schema::table('qrcode_empresas', function (Blueprint $table) {
            $table->integer('qrcode_id')->unsigned()->after('id');
            $table->dropColumn('qrcode');
            $table->foreign('qrcode_id', 'eq_qrcode_fk')->references('id')->on('qrcodes')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('qrcode_equipamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qrcode_id')->unsigned();
            $table->integer('equipamento_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('qrcode_id', 'qe_qrcode_fk')->references('id')->on('qrcodes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('equipamento_id', 'qe_equipamento_fk')->references('id')->on('empresas_x_equipamentos')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('qrcode_veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qrcode_id')->unsigned();
            $table->integer('veiculo_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('qrcode_id', 'qv_qrcode_fk')->references('id')->on('qrcodes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('veiculo_id', 'qv_veiculo_fk')->references('id')->on('empresas_veiculos')->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::dropIfExists('qrcode_veiculos');
        Schema::dropIfExists('qrcode_equipamentos');

        Schema::table('qrcode_empresas', function (Blueprint $table) {
            $table->dropForeign('eq_qrcode_fk');
            $table->string('qrcode')->after('empresa_id');
            $table->dropColumn('qrcode_id');
        });
        Schema::rename('qrcode_empresas', 'empresa_qrcode');

        Schema::dropIfExists('qrcodes');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
