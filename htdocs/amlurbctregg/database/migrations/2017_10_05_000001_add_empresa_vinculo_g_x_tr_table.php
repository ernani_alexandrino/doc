<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaVinculoGXTrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_vinculo_g_x_tr', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('empresa_vinculada_id')->unsigned();
            $table->integer('empresa_responsavel_id')->unsigned();
            $table->integer('frequencia_geracao_id')->unsigned()->nullable();
            $table->integer('frequencia_coleta_id')->unsigned();
            $table->integer('status_id')->unsigned()->default(1);
            $table->text('residuo_array_id');
            $table->text('equipamento_array_id');
            $table->text('equipamento_array_quantidade');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('empresa_vinculada_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('empresa_responsavel_id')->references('id')->on('empresa_responsavel')->onDelete('no action')->onUpdate('no action');
            $table->foreign('frequencia_geracao_id')->references('id')->on('frequencia_geracao')->onDelete('no action')->onUpdate('no action');
            $table->foreign('frequencia_coleta_id')->references('id')->on('frequencia_coleta')->onDelete('no action')->onUpdate('no action');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_vinculo_g_x_tr');
    }
}
