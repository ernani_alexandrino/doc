<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEmpresaAlteracaoAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('empresa_alteracao', function (Blueprint $table) {

           // $table->dropForeign('fk_empresa_alteracao_cidades');
           // $table->dropForeign('fk_empresa_alteracao_estados');
           // $table->dropForeign('fk_empresa_alteracao_ramo_atividade');
           // $table->dropForeign('fk_empresa_alteracao_tipo_ramo_atividade');

            $table->string('razao_social')->after('tipo_ramo_atividade_id')->nullable();
            $table->string('nome_fantasia')->after('razao_social')->nullable();
            $table->string('empresa_cartao_cnpj')->after('telefone')->nullable();
            $table->string('empresa_num_iptu')->after('empresa_cartao_cnpj')->nullable();
            $table->string('empresa_iptu')->after('empresa_num_iptu')->nullable();
            $table->unsignedInteger('ramo_atividade_id')->nullable()->change();
            $table->unsignedInteger('tipo_ramo_atividade_id')->nullable()->change();
            $table->string('endereco')->nullable()->change();
            $table->string('numero')->nullable()->change();
            $table->string('bairro')->nullable()->change();
            $table->string('cep')->nullable()->change();
            $table->unsignedInteger('estado_id')->nullable()->change();
            $table->unsignedInteger('cidade_id')->nullable()->change();
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('empresa_alteracao', function (Blueprint $table) {
            $table->dropColumn('razao_social');
            $table->dropColumn('nome_fantasia');
            $table->dropColumn('empresa_cartao_cnpj');
            $table->dropColumn('empresa_num_iptu');
            $table->dropColumn('empresa_iptu');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
