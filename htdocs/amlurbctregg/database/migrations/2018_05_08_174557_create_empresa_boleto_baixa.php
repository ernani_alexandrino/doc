<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaBoletoBaixa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_boleto_baixa', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('empresa_boleto_id', 0, 1);
            $table->string('anoExerEmiGuia', 4);
            $table->integer('codTipoServGuia', 0, 1);
            $table->integer('codNumGuia', 0, 1);
            $table->timestamp('dataPgtoGuia')->nullable();
            $table->decimal('valPgtoGuia');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_boleto_id')->references('id')->on('empresa_boleto')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_boleto_baixa');
    }
}
