<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnEstabelecimentoTipoIdEmpresaInformacaoComplementar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::table('empresa_informacao_complementar', function(Blueprint $table) {
            $table->integer('estabelecimento_tipo_id')->after('energia_consumo_id')->unsigned()->nullable();
            $table->foreign('estabelecimento_tipo_id')->references('id')->on('estabelecimento_tipo')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
