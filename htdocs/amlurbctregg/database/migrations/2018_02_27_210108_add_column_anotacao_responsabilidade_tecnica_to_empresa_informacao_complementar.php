<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAnotacaoResponsabilidadeTecnicaToEmpresaInformacaoComplementar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::table('empresa_informacao_complementar', function(Blueprint $table) {
            $table->string('anotacao_responsabilidade_tecnica')->after('certidao_negativa_federal')->nullable();
            $table->date('anotacao_responsabilidade_tecnica_emissao')->after('anotacao_responsabilidade_tecnica')->nullable();
            $table->date('anotacao_responsabilidade_tecnica_vencimento')->after('anotacao_responsabilidade_tecnica_emissao')->nullable();
        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
