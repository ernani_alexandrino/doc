<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVeiculosQrcodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculos_qrcode', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_veiculo_id')->unsigned();
            $table->string('qrcode');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_veiculo_id')->references('id')->on('empresas_veiculos')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculos_qrcode');
    }
}
