<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataValidadeEquipamentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas_x_equipamentos', function (Blueprint $table) {
            $table->timestamp('data_validade')->nullable()->after('codigo_amlurb');
        });

        Schema::table('equipamentos', function (Blueprint $table) {
            $table->dropColumn('data_validade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipamentos', function (Blueprint $table) {
            $table->timestamp('data_validade')->nullable()->after('capacidade');
        });

        Schema::table('empresas_x_equipamentos', function (Blueprint $table) {
            $table->dropColumn('data_validade');
        });
    }
}
