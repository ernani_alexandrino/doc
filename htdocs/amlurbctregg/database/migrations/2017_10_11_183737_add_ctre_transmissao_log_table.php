<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCtreTransmissaoLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctre_transmissao_log', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('ctre_id');
            $table->integer('empresa_id')->unsigned()->comment("Id da empresa que fez a iteratividade no CTRE. Diferente do id da table ctre, onde o empresa_id refere-se a empresa que está com o ctre, nesta tabela grava quem realizou a ação.");
            $table->integer('status_id')->unsigned();
            $table->char('placa_veiculo', 7);
            $table->string('tipo_veiculo', 100)->nullable();
            $table->timestamp('data_emissao');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ctre_id')->references('id')->on('ctre')->onDelete('no action')->onUpdate('no action');
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctre_transmissao_log');
    }
}
