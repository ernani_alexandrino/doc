<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAmlurbIdFromEmpresasXEquipamento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $fields =  DB::select("
        select * from INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME='empresas_x_equipamentos' AND COLUMN_NAME='amlurb_id' 
        ");
        if(!empty($fields)){

            Schema::table('empresas_x_equipamentos', function($table)
            {
                
                 $table->dropColumn('amlurb_id');
                
            });

        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
