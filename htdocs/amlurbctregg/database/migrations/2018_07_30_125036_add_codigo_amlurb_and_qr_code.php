<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodigoAmlurbAndQrCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('empresas_x_equipamentos', function (Blueprint $table) {
            $table->dropColumn('quantidade');
            $table->string('codigo_amlurb', 50)->nullable()->after('status_id');
            $table->string('qrcode')->nullable()->after('codigo_amlurb');
        });

        Schema::dropIfExists('equipamentos_qrcode');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('empresas_x_equipamentos', function (Blueprint $table) {
            $table->dropColumn('codigo_amlurb');
            $table->dropColumn('qrcode');
            $table->integer('quantidade')->nullable()->after('status_id');
        });

        Schema::create('equipamentos_qrcode', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('equipamento_id')->unsigned();
            $table->string('qrcode');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('equipamento_id')->references('id')->on('equipamentos')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
