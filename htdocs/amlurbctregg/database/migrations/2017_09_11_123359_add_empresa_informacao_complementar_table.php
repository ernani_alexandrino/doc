<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaInformacaoComplementarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_informacao_complementar', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('frequencia_geracao_id')->unsigned()->nullable();
            $table->integer('frequencia_coleta_id')->unsigned()->nullable();
            $table->integer('ramo_atividade_id')->unsigned()->nullable();
            $table->integer('tipo_ramo_atividade_id')->unsigned()->nullable();
            $table->integer('colaboradores_numero_id')->unsigned()->nullable();
            $table->integer('energia_consumo_id')->unsigned()->nullable();
            $table->integer('area_total')->nullable();
            $table->integer('area_construida')->nullable();
            $table->string('cartao_cnpj')->nullable();
            $table->string('contrato_social')->nullable();
            $table->string('certidao_falencia_concordata')->nullable();
            $table->string('balanco_financeiro')->nullable();
            $table->string('demonstrativo_contabil')->nullable();
            $table->string('certidao_negativa_municipal')->nullable();
            $table->string('certidao_negativa_estadual')->nullable();
            $table->string('certidao_negativa_federal')->nullable();
            $table->string('licenca_previa')->nullable();
            $table->date('licenca_previa_emissao')->nullable();
            $table->date('licenca_previa_vencimento')->nullable();
            $table->string('licenca_instalacao')->nullable();
            $table->date('licenca_instalacao_emissao')->nullable();
            $table->date('licenca_instalacao_vencimento')->nullable();
            $table->string('licenca_operacao')->nullable();
            $table->date('licenca_operacao_emissao')->nullable();
            $table->date('licenca_operacao_vencimento')->nullable();
            $table->string('alvara_prefeitura')->nullable();
            $table->date('alvara_prefeitura_emissao')->nullable();
            $table->date('alvara_prefeitura_vencimento')->nullable();
            $table->string('ctf_ibama')->nullable();
            $table->date('ctf_ibama_emissao')->nullable();
            $table->date('ctf_ibama_vencimento')->nullable();
            $table->string('avcb')->nullable();
            $table->date('avcb_emissao')->nullable();
            $table->date('avcb_vencimento')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('frequencia_geracao_id')->references('id')->on('frequencia_geracao')->onDelete('no action')->onUpdate('no action');
            $table->foreign('frequencia_coleta_id')->references('id')->on('frequencia_coleta')->onDelete('no action')->onUpdate('no action');
            $table->foreign('ramo_atividade_id')->references('id')->on('ramo_atividade')->onDelete('no action')->onUpdate('no action');
            $table->foreign('tipo_ramo_atividade_id')->references('id')->on('tipo_ramo_atividade')->onDelete('no action')->onUpdate('no action');
            $table->foreign('colaboradores_numero_id')->references('id')->on('colaboradores_numero')->onDelete('no action')->onUpdate('no action');
            $table->foreign('energia_consumo_id')->references('id')->on('energia_consumo')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_informacao_complementar');
    }
}
