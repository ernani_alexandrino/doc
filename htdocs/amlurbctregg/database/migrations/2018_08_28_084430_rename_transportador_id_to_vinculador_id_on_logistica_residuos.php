<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTransportadorIdToVinculadorIdOnLogisticaResiduos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logistica_residuos', function (Blueprint $table) {
            $table->renameColumn('transportador_id', 'vinculador_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logistica_residuos', function (Blueprint $table) {
            $table->renameColumn('vinculador_id', 'transportador_id');
        });
    }
}
