<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterValueEmpresasTipoDestinoFinal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('empresas_tipo')
            ->where('id', 4)
            ->update(['nome' => 'Destino Final']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('empresas_tipo')
            ->where('id', 4)
            ->update(['nome' => 'Destino Final Reciclado']);
    }
}
