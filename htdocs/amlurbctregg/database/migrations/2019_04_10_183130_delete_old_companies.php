<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Amlurb\Models\Empresa;
use Amlurb\Models\EmpresaEndereco;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\EmpresasVeiculo;
use Amlurb\Models\Ctre;
use Amlurb\Models\CtreResiduos;
use Amlurb\Models\User;
use Amlurb\Models\UserPerfil;
use Amlurb\Models\QrcodeEmpresas;
use Amlurb\Models\Qrcode;
use Amlurb\Models\Protocolo;

class DeleteOldCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $empresasid = Empresa::where('created_at', '<', '2019-04-10 ')
        ->whereNotIn('id', [18081,18082,18083,18084,18399])->pluck('id');
        $usersId = User::whereIn('empresa_id', $empresasid)->pluck('id');
        $qrcoresid = QrcodeEmpresas::whereIn('empresa_id', $empresasid)->pluck('qrcode_id');
        EmpresaEndereco::whereIn('empresa_id', $empresasid)->delete();
        EmpresasXEmpresasTipo::whereIn('empresa_id', $empresasid)->delete();
        EmpresasVeiculo::whereIn('empresa_id', $empresasid)->delete();
        Ctre::whereIn('empresa_id', $empresasid)->delete();
        Empresa::whereIn('id', $empresasid)->delete();
        User::whereIn('id', $usersId)->delete();
        UserPerfil::whereIn('user_id', $usersId)->delete();
        Protocolo::whereIn('empresa_id', $empresasid)->delete();
        Qrcode::whereIn('id', $qrcoresid)->delete();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
