<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaEnderecoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_endereco', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->integer('cidade_id')->unsigned();
            $table->string('endereco');
            $table->string('cep', 10);
            $table->string('bairro', 100);
            $table->string('complemento', 100)->nullable();
            $table->string('ponto_referencia')->nullable();
            $table->integer('numero');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('cidade_id')->references('id')->on('cidades')->onDelete('no action')->onUpdate('no action');
            $table->foreign('estado_id')->references('id')->on('estados')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('empresa_endereco');
    }
}
