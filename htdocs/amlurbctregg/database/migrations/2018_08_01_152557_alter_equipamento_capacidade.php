<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEquipamentoCapacidade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipamentos', function (Blueprint $table) {
            $table->dropColumn('capacidade');
            $table->dropColumn('peso');
        });

        Schema::table('equipamentos', function (Blueprint $table) {
            $table->string('capacidade')->after('status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipamentos', function (Blueprint $table) {
            $table->dropColumn('capacidade');
        });

        Schema::table('equipamentos', function (Blueprint $table) {
            $table->integer('capacidade')->after('status_id');
            $table->string('peso', 30)->nullable()->after('capacidade');
        });
    }
}
