<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVinculadorIdToTransportadorVinculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('transportador_vinculos', function (Blueprint $table) {
            $table->integer('vinculador_id')->unsigned()->after('id');
            $table->foreign('vinculador_id', 'vinculador_empresa_fk')->references('id')->on('empresas')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('transportador_vinculos', function (Blueprint $table) {
            $table->dropForeign('vinculador_empresa_fk');
            $table->dropColumn('vinculador_id');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
