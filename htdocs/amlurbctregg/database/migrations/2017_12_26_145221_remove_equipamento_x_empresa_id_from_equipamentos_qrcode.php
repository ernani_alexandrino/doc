<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveEquipamentoXEmpresaIdFromEquipamentosQrcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $fields =  DB::select("
        select * from INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME='equipamentos_qrcode' AND COLUMN_NAME='equipamento_x_empresa_id' 
        ");
        if(!empty($fields)){

            Schema::table('equipamentos_qrcode', function($table)
            {

                $table->dropForeign(['equipamento_x_empresa_id']);
                $table->dropColumn('equipamento_x_empresa_id');

            });

        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
