<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVinculosAddEmpresaVinculadaTipoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('vinculos', function (Blueprint $table) {
            $table->integer('empresa_vinculada_tipo_id')->after('empresa_vinculada_id')->unsigned();
            $table->foreign('empresa_vinculada_tipo_id')->references('id')->on('empresas_tipo')->onDelete('no action')->onUpdate('no action');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('vinculos', function (Blueprint $table) {
            $table->dropColumn('empresa_tipo_id');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
