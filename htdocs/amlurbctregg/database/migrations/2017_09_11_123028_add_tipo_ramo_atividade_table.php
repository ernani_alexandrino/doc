<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoRamoAtividadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_ramo_atividade', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('ramo_atividade_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->string('codigo', 25)->nullable();
            $table->string('nome', 150);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ramo_atividade_id')->references('id')->on('ramo_atividade')->onDelete('no action')->onUpdate('no action');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_ramo_atividade');
    }
}
