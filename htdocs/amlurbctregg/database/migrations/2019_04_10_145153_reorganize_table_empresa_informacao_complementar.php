<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReorganizeTableEmpresaInformacaoComplementar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresa_informacao_complementar', function (Blueprint $table) {
            $table->dropColumn('cartao_cnpj');
            $table->dropColumn('contrato_social');
            $table->dropColumn('certidao_falencia_concordata');
            $table->dropColumn('balanco_financeiro');
            $table->dropColumn('demonstrativo_contabil');
            $table->dropColumn('certidao_negativa_municipal');
            $table->dropColumn('certidao_negativa_estadual');
            $table->dropColumn('certidao_negativa_federal');
            $table->dropColumn('licenca_previa');
            $table->dropColumn('licenca_previa_emissao');
            $table->dropColumn('licenca_previa_vencimento');
            $table->dropColumn('licenca_instalacao');
            $table->dropColumn('licenca_instalacao_emissao');
            $table->dropColumn('licenca_instalacao_vencimento');
            $table->dropColumn('licenca_operacao');
            $table->dropColumn('licenca_operacao_emissao');
            $table->dropColumn('licenca_operacao_vencimento');
            $table->dropColumn('alvara_prefeitura');
            $table->dropColumn('alvara_prefeitura_emissao');
            $table->dropColumn('alvara_prefeitura_vencimento');
            $table->dropColumn('ctf_ibama');
            $table->dropColumn('ctf_ibama_emissao');
            $table->dropColumn('ctf_ibama_vencimento');
            $table->dropColumn('avcb');
            $table->dropColumn('avcb_emissao');
            $table->dropColumn('avcb_vencimento');
            $table->dropColumn('anotacao_responsabilidade_tecnica');
            $table->dropColumn('anotacao_responsabilidade_tecnica_emissao');
            $table->dropColumn('anotacao_responsabilidade_tecnica_vencimento');
            $table->dropColumn('certificado_dispensa_licenca');
            $table->dropColumn('certificado_dispensa_licenca_emissao');
            $table->dropColumn('certificado_dispensa_licenca_vencimento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresa_informacao_complementar', function (Blueprint $table) {
            $table->string('cartao_cnpj')->nullable();
            $table->string('contrato_social')->nullable();
            $table->string('certidao_falencia_concordata')->nullable();
            $table->string('balanco_financeiro')->nullable();
            $table->string('demonstrativo_contabil')->nullable();
            $table->string('certidao_negativa_municipal')->nullable();
            $table->string('certidao_negativa_estadual')->nullable();
            $table->string('certidao_negativa_federal')->nullable();
            $table->string('licenca_previa')->nullable();
            $table->date('licenca_previa_emissao')->nullable();
            $table->date('licenca_previa_vencimento')->nullable();
            $table->string('licenca_instalacao')->nullable();
            $table->date('licenca_instalacao_emissao')->nullable();
            $table->date('licenca_instalacao_vencimento')->nullable();
            $table->string('licenca_operacao')->nullable();
            $table->date('licenca_operacao_emissao')->nullable();
            $table->date('licenca_operacao_vencimento')->nullable();
            $table->string('alvara_prefeitura')->nullable();
            $table->date('alvara_prefeitura_emissao')->nullable();
            $table->date('alvara_prefeitura_vencimento')->nullable();
            $table->string('ctf_ibama')->nullable();
            $table->date('ctf_ibama_emissao')->nullable();
            $table->date('ctf_ibama_vencimento')->nullable();
            $table->string('avcb')->nullable();
            $table->date('avcb_emissao')->nullable();
            $table->date('avcb_vencimento')->nullable();
            $table->string('anotacao_responsabilidade_tecnica')->nullable();
            $table->date('anotacao_responsabilidade_tecnica_emissao')->nullable();
            $table->date('anotacao_responsabilidade_tecnica_vencimento')->nullable();
            $table->string('certificado_dispensa_licenca')->nullable();
            $table->date('certificado_dispensa_licenca_emissao')->nullable();
            $table->date('certificado_dispensa_licenca_vencimento')->nullable();
        });
    }
}
