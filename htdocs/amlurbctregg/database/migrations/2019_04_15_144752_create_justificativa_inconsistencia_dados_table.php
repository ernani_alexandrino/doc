<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJustificativaInconsistenciaDadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('justificativa_inconsistencia_dados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('justificativa_id');
            $table->unsignedInteger('fiscal_id');
            $table->unsignedInteger('empresa_id');
            $table->foreign('justificativa_id', 'jid_justificativa_fk')->references('id')->on('justificativas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('fiscal_id', 'jid_fiscal_fk')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_id', 'jid_empresa_fk')->references('id')->on('empresas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('justificativa_inconsistencia_dados');
    }
}
