<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulatePermissionsXActions extends Migration
{
    public $table = 'permission_x_actions';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table($this->table)->insert([
            'id' => '1',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 3,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '2',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 2,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '3',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 4,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '4',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 5,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '5',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 6,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '6',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 7,
         
        ]);

        DB::table($this->table)->insert([
            'id' => '7',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 8,
         
        ]);

         DB::table($this->table)->insert([
            'id' => '8',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 9,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '9',
            'name' => 'edit-perfil',
            'label' => 'Editar Perfil',
            'permission_id' => 9,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '10',
            'name' => 'create-user',
            'label' => 'Cadastrar Usuário',
            'permission_id' => 10,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '11',
            'name' => 'update-user',
            'label' => 'Editar Usuário',
            'permission_id' => 10,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '12',
            'name' => 'destroy-user',
            'label' => 'Deletar Usuário',
            'permission_id' => 10,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '13',
            'name' => 'create-permissions',
            'label' => 'Cadastrar Permissões',
            'permission_id' => 11,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '14',
            'name' => 'edit-permissions',
            'label' => 'Editar Permissões',
            'permission_id' => 11,
         
        ]);
        DB::table($this->table)->insert([
            'id' => '15',
            'name' => 'destroy-permissions',
            'label' => 'Deletar Permissões',
            'permission_id' => 11,
         
        ]);

        DB::table($this->table)->insert([
            'id' => '16',
            'name' => 'show',
            'label' => 'Visualizar',
            'permission_id' => 1,
         
        ]);

  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
