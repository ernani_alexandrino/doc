<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CodigosVeiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->string('codigo_amlurb', 50)->nullable()->after('status_id');
            $table->string('qrcode')->nullable()->after('codigo_amlurb');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->dropColumn('codigo_amlurb');
            $table->dropColumn('qrcode');
        });
    }
}
