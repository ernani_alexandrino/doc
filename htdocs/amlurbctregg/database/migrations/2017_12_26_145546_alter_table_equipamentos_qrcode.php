<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEquipamentosQrcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");

        Schema::table('equipamentos_qrcode', function(Blueprint $table) {
            $table->integer('empresa_x_equipamento_id')->unsigned();
            $table->foreign('empresa_x_equipamento_id')->references('id')->on('empresas_x_equipamentos')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
