<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVeiculoIdToCtre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('ctre', function (Blueprint $table) {
            $table->dropColumn('placa_veiculo');
            $table->integer('veiculo_id')->unsigned()->nullable()->after('destino_id');
            $table->foreign('veiculo_id', 'fk_ctre_veiculo')->references('id')->on('empresas_veiculos')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('ctre', function (Blueprint $table) {
            $table->dropForeign('fk_ctre_veiculo');
            $table->dropColumn('veiculo_id');
            $table->char('placa_veiculo', 7)->after('ctre_volume_total_id');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
