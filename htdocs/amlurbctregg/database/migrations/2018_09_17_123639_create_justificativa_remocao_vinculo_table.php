<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJustificativaRemocaoVinculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('justificativa_remocao_vinculo', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('justificativa_id');
            $table->unsignedInteger('vinculo_id');
            $table->foreign('justificativa_id', 'jrv_justificativa_fk')->references('id')->on('justificativas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('vinculo_id', 'jrv_vinculo_fk')->references('id')->on('vinculos')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('justificativa_remocao_vinculo');
    }
}
