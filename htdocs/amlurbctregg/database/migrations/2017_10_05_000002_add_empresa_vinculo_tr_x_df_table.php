<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresaVinculoTrXDfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_vinculo_tr_x_df', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned();
            $table->integer('empresa_vinculada_id')->unsigned();
            $table->integer('empresa_responsavel_id')->unsigned();
            $table->integer('status_id')->unsigned()->default(1);
            $table->text('residuo_array_id');
            $table->decimal('volume_residuo_mensal', 10, 3);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('empresa_vinculada_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('empresa_responsavel_id')->references('id')->on('empresa_responsavel')->onDelete('no action')->onUpdate('no action');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_vinculo_tr_x_df');
    }
}
