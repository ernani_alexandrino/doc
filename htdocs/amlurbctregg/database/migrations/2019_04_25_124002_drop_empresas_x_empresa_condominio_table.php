<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropEmpresasXEmpresaCondominioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('empresas_x_empresa_condominio');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('empresas_x_empresa_condominio', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('empresa_id')->unsigned()->comments("Referenciando o id do grande gerador que é condomínio.");
            $table->integer('empresa_vinculada_id')->unsigned()->comments("Referenciando o id do grande gerador que está dentro do condomínio.");
            $table->integer('status_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('empresa_vinculada_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });
    }
}
