<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteBoletoQrcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('boleto_qrcodes');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('boleto_qrcodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boleto_id')->unsigned();
            $table->integer('qrcode_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('boleto_id', 'bq_boleto_fk')->references('id')->on('boletos')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('qrcode_id', 'bq_qrcode_fk')->references('id')->on('qrcodes')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
