<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEquipamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipamentos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->unsigned();
            $table->string('codigo', 50)->nullable();
            $table->string('nome', 150);
            $table->integer('capacidade');
            $table->string('peso', 30)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipamentos');
    }
}
