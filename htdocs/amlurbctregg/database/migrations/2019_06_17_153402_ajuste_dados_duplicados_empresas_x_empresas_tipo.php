<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\Empresa;

class AjusteDadosDuplicadosEmpresasXEmpresasTipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $empresas = Empresa::where('id_limpurb', '!=', null)->get();
        foreach ($empresas as $empresa) {
            $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
            ->get();
            
            if (count($empresaTipo) > 1) {
                foreach ($empresaTipo as $tipoEmpresa) {
                    if ($tipoEmpresa->location == 'vinculo') {
                        EmpresasXEmpresasTipo::find($tipoEmpresa->id)
                        ->delete();
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
