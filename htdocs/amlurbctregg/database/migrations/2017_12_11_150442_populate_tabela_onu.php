<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Amlurb\Models\TabelaOnu;

class PopulateTabelaOnu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("SET foreign_key_checks=0");
		TabelaOnu::truncate();
		DB::statement("SET foreign_key_checks=1");	 
        $tabelaOnu = DB::connection('pv_seed')->table('tabela_onu')->get();
		foreach($tabelaOnu as $onu){
			TabelaOnu::create([
				'codigo' => $onu->codigo, 
				'descricao' => $onu->descricao,
			]);  
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
