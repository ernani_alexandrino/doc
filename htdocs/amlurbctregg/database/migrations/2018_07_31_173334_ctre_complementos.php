<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CtreComplementos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('ctre', function (Blueprint $table) {
            $table->dropColumn('tipo_veiculo');
            $table->integer('gerador_id')->unsigned()->nullable()->after('empresa_id');
            $table->integer('transportador_id')->unsigned()->nullable()->after('gerador_id');
            $table->integer('destino_id')->unsigned()->nullable()->after('transportador_id');
        });

        Schema::table('ctre', function (Blueprint $table) {
            $table->foreign('gerador_id', 'fk_gerador_ctre')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('transportador_id', 'fk_transportador_ctre')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            $table->foreign('destino_id', 'fk_destino_ctre')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::table('ctre', function (Blueprint $table) {
            $table->string('tipo_veiculo', 100)->nullable()->after('placa_veiculo');
            $table->dropForeign('fk_gerador_ctre');
            $table->dropForeign('fk_transportador_ctre');
            $table->dropForeign('fk_destino_ctre');
            $table->dropColumn('gerador_id');
            $table->dropColumn('transportador_id');
            $table->dropColumn('destino_id');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
