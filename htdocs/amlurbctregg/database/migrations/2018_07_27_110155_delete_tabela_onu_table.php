<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTabelaOnuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('residuos', function (Blueprint $table) {
            $table->dropForeign('residuos_tabela_onu_id_foreign');
            $table->dropColumn('tabela_onu_id');
        });

        Schema::table('tabela_onu', function (Blueprint $table) {
            $table->dropIfExists();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('tabela_onu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->nullable();
            $table->string('descricao')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('residuos', function (Blueprint $table) {
            $table->integer('tabela_onu_id')->unsigned()->after('nome');
            $table->foreign('tabela_onu_id', 'residuos_tabela_onu_id_foreign')
                ->references('id')->on('tabela_onu')->onDelete('no action')->onUpdate('no action');
        });
    }
}
