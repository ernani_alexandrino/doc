<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QrcodeMediumtext extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qrcodes', function (Blueprint $table) {
            $table->dropColumn('codigo');
        });

        Schema::table('qrcodes', function (Blueprint $table) {
            $table->mediumText('codigo')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qrcodes', function (Blueprint $table) {
            $table->dropColumn('codigo');
        });

        Schema::table('qrcodes', function (Blueprint $table) {
            $table->text('codigo')->after('id');
        });
    }
}
