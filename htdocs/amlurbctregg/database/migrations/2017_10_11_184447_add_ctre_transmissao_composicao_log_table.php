<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCtreTransmissaoComposicaoLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctre_transmissao_composicao_log', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('ctre_id');
            $table->integer('ctre_transmissao_log_id')->unsigned();
            $table->integer('equipamento_id')->unsigned();
            $table->integer('equipamento_quantidade');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ctre_id')->references('id')->on('ctre')->onDelete('no action')->onUpdate('no action');
            $table->foreign('ctre_transmissao_log_id')->references('id')->on('ctre_transmissao_log')->onDelete('no action')->onUpdate('no action');
            $table->foreign('equipamento_id')->references('id')->on('equipamentos')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctre_transmissao_composicao_log');
    }
}
