<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEquipamentoAlocacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::create('equipamento_alocacao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('equipamento_id')->unsigned();
            $table->integer('empresa_id')->unsigned();
            $table->timestamps();
        });

        // index so deixa o equipamento ser alocado a uma empresa por vez
        // equipamento nao pode estar em dois lugares ao mesmo tempo
        Schema::table('equipamento_alocacao', function (Blueprint $table) {
            $table->unique('equipamento_id');
            $table->foreign('equipamento_id', 'alocacao_equipamento_fk')->references('id')->on('empresas_x_equipamentos')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empresa_id', 'alocacao_empresa_fk')->references('id')->on('empresas')->onUpdate('cascade')->onDelete('cascade');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        Schema::dropIfExists('equipamento_alocacao');
        DB::statement("SET FOREIGN_KEY_CHECKS=1");
    }
}
