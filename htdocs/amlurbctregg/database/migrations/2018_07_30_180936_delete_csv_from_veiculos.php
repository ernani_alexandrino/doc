<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCsvFromVeiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas_veiculos', function(Blueprint $table) {
            $table->dropColumn('inmetro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas_veiculos', function(Blueprint $table) {
            $table->string('inmetro')->nullable()->after('vencimento_ipva');
        });
    }
}
