<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEquipamentosAndResiduosToCtrePivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::create('ctre_equipamentos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('ctre_id')->unsigned();
            $table->integer('equipamento_id')->unsigned();
            $table->timestamps();
            $table->foreign('ctre_id', 'fk_ctre_to_equipamentos')->references('id')->on('ctre')->onDelete('no action')->onUpdate('no action');
            $table->foreign('equipamento_id', 'fk_ctre_equipamento')->references('id')->on('empresas_x_equipamentos')->onDelete('no action')->onUpdate('no action');
        });

        Schema::create('ctre_residuos', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('ctre_id')->unsigned();
            $table->integer('residuo_id')->unsigned();
            $table->timestamps();
            $table->foreign('ctre_id', 'fk_ctre_to_residuos')->references('id')->on('ctre')->onDelete('no action')->onUpdate('no action');
            $table->foreign('residuo_id', 'fk_ctre_residuo')->references('id')->on('residuos')->onDelete('no action')->onUpdate('no action');
        });

        Schema::dropIfExists('ctre_composicao');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::dropIfExists('ctre_residuos');
        Schema::dropIfExists('ctre_equipamentos');

        Schema::create('ctre_composicao', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('ctre_id')->unsigned();
            $table->integer('equipamento_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('ctre_id')->references('id')->on('ctre')->onDelete('no action')->onUpdate('no action');
            $table->foreign('equipamento_id')->references('id')->on('empresas_x_equipamentos')->onDelete('no action')->onUpdate('no action');
        });

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
