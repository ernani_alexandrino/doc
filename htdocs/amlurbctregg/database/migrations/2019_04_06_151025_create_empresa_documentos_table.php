<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa_documentos', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('empresa_id');
            $table->unsignedInteger('documento_id');
            $table->string('caminho_arquivo');
            $table->timestamp('data_emissao')->nullable();
            $table->timestamp('data_vencimento')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('empresa_id', 'ed_empresa_fk')->references('id')->on('empresas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('documento_id', 'ed_documento_fk')->references('id')->on('documentos')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa_documentos');
    }
}
