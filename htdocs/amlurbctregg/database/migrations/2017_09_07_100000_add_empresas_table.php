<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('id_limpurb', 15)->unique();
            $table->integer('status_id')->unsigned()->default(1);
            $table->string('cnpj', 14)->unique();
            $table->string('razao_social', 60);
            $table->string('nome_comercial', 60);
            $table->string('ie', 15)->nullable();
            $table->string('im',15)->nullable();
            $table->string('segmento', 150)->nullable();
            $table->string('telefone', 15)->nullable();
            $table->string('nome_responsavel', 50)->nullable();
            $table->string('email', 50)->nullable();
            $table->string('telefone_responsavel', 15)->nullable();
            $table->string('ramal', 15)->nullable();
            $table->string('celular', 15)->nullable();
            $table->string('cargo', 50)->nullable();
            $table->string('id_base_amlurb', 15)->nullable();
            $table->string('id_limpurb_base_amlurb', 20)->nullable();
            $table->timestamp('data_cadastro_base_amlurb')->nullable();
            $table->timestamp('data_publicacao_dom_base_amlurb')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('status_id')->references('id')->on('status')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
