<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Amlurb\Models\JustificativaEmpresaAlteracao;

class AlterTableJustificativaEmpresaAlteracao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        JustificativaEmpresaAlteracao::truncate();
        Schema::table('justificativa_empresa_alteracao', function (Blueprint $table) {
            $table->dropForeign('justificativa_empresa_alteracao_ibfk_1');
            $table->foreign('empresa_alteracao_id')->references('id')->on('empresa_alteracao_json')->onUpdate('cascade')->onDelete('cascade');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
