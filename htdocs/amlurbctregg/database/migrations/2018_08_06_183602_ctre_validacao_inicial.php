<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CtreValidacaoInicial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ctre', function(Blueprint $table) {
            $table->timestamp('data_validacao')->nullable()->after('data_expiracao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ctre', function(Blueprint $table) {
            $table->dropColumn('data_validacao');
        });
    }
}
