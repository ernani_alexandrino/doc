<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToEmpresasVeiculos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->string('anexo_documento_veiculo')->after('data_validade')->nullable();
            $table->string('anexo_documento_inmetro')->after('anexo_documento_veiculo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->dropColumn('anexo_documento_veiculo');
            $table->dropColumn('anexo_documento_inmetro');
        });
    }
}
