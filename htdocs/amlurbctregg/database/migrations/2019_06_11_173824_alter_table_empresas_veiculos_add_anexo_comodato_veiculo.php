<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEmpresasVeiculosAddAnexoComodatoVeiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->string('anexo_comodato_veiculo')->after('anexo_documento_inmetro')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresas_veiculos', function (Blueprint $table) {
            $table->dropColumn('anexo_comodato_veiculo');
        });
    }
}
