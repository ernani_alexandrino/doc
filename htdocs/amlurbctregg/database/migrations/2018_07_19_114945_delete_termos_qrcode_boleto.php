<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTermosQrcodeBoleto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresas_x_termos_de_uso', function (Blueprint $table) {
            $table->dropColumn('termo_boleto');
            $table->dropColumn('termo_qr_code');
        });

        Schema::table('termos_de_uso', function (Blueprint $table) {
            $table->dropColumn('termo_boleto');
            $table->dropColumn('termo_qr_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('termos_de_uso', function (Blueprint $table) {
            $table->addColumn( 'text', 'termo_boleto')->after('id');
            $table->addColumn( 'text', 'termo_qr_code')->after('termo_boleto');
        });

        Schema::table('empresas_x_termos_de_uso', function (Blueprint $table) {
            $table->addColumn('boolean', 'termo_boleto', ['default' => 0])->after('termo_id');
            $table->addColumn('boolean', 'termo_qr_code', ['default' => 0])->after('termo_boleto');
        });
    }
}
