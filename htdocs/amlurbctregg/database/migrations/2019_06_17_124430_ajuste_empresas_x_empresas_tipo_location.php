<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Amlurb\Models\EmpresasXEmpresasTipo;
use Amlurb\Models\Empresa;

class AjusteEmpresasXEmpresasTipoLocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $empresas = Empresa::where('id_limpurb', '!=', null)->get();
        foreach ($empresas as $empresa) {
            if (strpos($empresa->id_limpurb, 'GG') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.grande_gerador'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'PG') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.pequeno_gerador'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'TR') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.transportador'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'DR') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.destino_final_reciclado'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'CO') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.cooperativa_residuos'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'FS') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.fiscal'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'AD') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.administrador'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'OP') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.orgao_publico'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'SS') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.servico_saude'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
            if (strpos($empresa->id_limpurb, 'CM') !== false) {
                $empresaTipo = EmpresasXEmpresasTipo::where('empresa_id', $empresa->id)
                ->where('empresa_tipo_id', '!=', \Illuminate\Support\Facades\Config::get('enums.empresas_tipo.condominio_misto'))
                ->update(
                    [
                        'location' => 'vinculo'
                    ]
                );
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
