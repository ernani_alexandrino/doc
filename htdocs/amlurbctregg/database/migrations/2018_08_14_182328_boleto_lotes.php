<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BoletoLotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('empresa_boleto', 'boletos');

        Schema::create('boleto_equipamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boleto_id')->unsigned();
            $table->integer('equipamento_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('boleto_id', 'be_boleto_fk')->references('id')->on('boletos')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('equipamento_id', 'be_equipamento_fk')->references('id')->on('empresas_x_equipamentos')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('boleto_veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boleto_id')->unsigned();
            $table->integer('veiculo_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('boleto_id', 'bv_boleto_fk')->references('id')->on('boletos')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('veiculo_id', 'be_veiculo_fk')->references('id')->on('empresas_veiculos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boleto_equipamentos');
        Schema::dropIfExists('boleto_veiculos');

        Schema::rename('boletos', 'empresa_boleto');
    }
}
