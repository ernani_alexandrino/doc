<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(!Schema::hasTable('users')){
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->integer('empresa_id')->unsigned();
                $table->string('name');
                $table->string('email', 100)->unique();
                $table->string('password');
                $table->rememberToken();
                $table->timestamps();
                $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('no action')->onUpdate('no action');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
