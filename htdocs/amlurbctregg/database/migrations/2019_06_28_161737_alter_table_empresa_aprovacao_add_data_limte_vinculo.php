<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableEmpresaAprovacaoAddDataLimteVinculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('empresa_aprovacao', function (Blueprint $table) {
            $table->date('data_limte_vinculo')->after('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('empresa_aprovacao', function (Blueprint $table) {
            $table->dropColumn('data_limte_vinculo');
        });
    }
}
