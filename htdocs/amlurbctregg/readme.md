
# CTRE

Projeto para monitoração de coleta, transporte e destinação de resíduos, com enfoque em serviços para prefeituras.


## observações

#### compilar arquivos JS

- npm run production
- npm run dev

#### compilar arquivos CSS/LESS

- gulp --production
- gulp

*Manter versões dos pacotes do Gulp nas versões definidas no arquivo package.json, pois versões posteriores não estão funcionando*

#### gerar qrcodes para impressão

é necessário instalar as seguintes bibliotecas no sistema:  

- sudo apt install libx11-xcb1 libxcomposite1 libxcursor1 libxdamage1 libxi6 libxtst6 libnss3 libxss1 libxrandr2 libasound2 libpangocairo-1.0 libatk1.0-0 libatk-bridge2.0-0 libgtk-3-0 libgtk-3-0

#### desabilitar sistema (manutencao)

- modificar variavel *MANUTENCAO* do arquivo .env para *true*