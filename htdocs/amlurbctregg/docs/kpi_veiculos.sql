SELECT 'veiculo'                                 AS "Tipo", 
       empresas.cnpj                             AS "CNPJ empresa", 
       empresas.razao_social                     AS "Razao Social Empresa", 
       empresas_veiculos.placa                   AS "Plca Veiculo", 
       empresas_veiculos.created_at              AS "Data de Cadastro", 
       empresas_veiculos.updated_at              AS "Data de alteracao", 
       boletos.datadepagamento                   AS 
       "Data de Pagamento do Boleto ", 
       status.descricao, 
       empresas_veiculos.anexo_documento_veiculo AS "Anexo Documento Veiculo", 
       empresas_veiculos.anexo_documento_inmetro AS "Anexo Documento Inmetro" 
FROM   empresas_veiculos 
       LEFT JOIN status 
              ON status.id = empresas_veiculos.status_id 
       LEFT JOIN empresas 
              ON empresas.id = empresas_veiculos.empresa_id 
       LEFT JOIN (SELECT Max(id) max_id, 
                         veiculo_id, 
                         boleto_id 
                  FROM   boleto_veiculos 
                  GROUP  BY veiculo_id) boleto_veiculos 
              ON ( boleto_veiculos.veiculo_id = empresas_veiculos.id ) 
       LEFT JOIN boletos 
              ON boletos.id = boleto_veiculos.boleto_id 