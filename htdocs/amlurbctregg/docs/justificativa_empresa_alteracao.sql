/*
 Navicat Premium Data Transfer

 Source Server         : amlurb
 Source Server Type    : MySQL
 Source Server Version : 50712
 Source Host           : ctre.cluster-cujgae8pujxi.us-east-1.rds.amazonaws.com:3306
 Source Schema         : ctre

 Target Server Type    : MySQL
 Target Server Version : 50712
 File Encoding         : 65001

 Date: 27/05/2019 10:51:53
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for justificativa_empresa_alteracao
-- ----------------------------
DROP TABLE IF EXISTS `justificativa_empresa_alteracao`;
CREATE TABLE `justificativa_empresa_alteracao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `justificativa_id` int(10) unsigned NOT NULL,
  `empresa_alteracao_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_justificativa_empresa_alteracao_justificativas` (`justificativa_id`) USING BTREE,
  KEY `fk_justificativa_empresa_alteracao_empresa_alteracao` (`empresa_alteracao_id`) USING BTREE,
  CONSTRAINT `fk_justificativa_empresa_alteracao_justificativas` FOREIGN KEY (`justificativa_id`) REFERENCES `justificativas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `justificativa_empresa_alteracao_ibfk_1` FOREIGN KEY (`empresa_alteracao_id`) REFERENCES `empresa_alteracao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
