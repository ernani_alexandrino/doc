SELECT empresas_tipo.nome                          AS "Tipo Empresa", 
       id_limpurb as "Codigo Amlurb", 
       cnpj as "CNPJ Empresa", 
       empresa_documentos.caminho_arquivo          AS "Cartao CNPJ", 
       razao_social as "Razao Social Empresa", 
       nome_comercial as "Nome Comercial Empresa", 
       boletos.datadepagamento as "Data de Pagamento do Boleto", 
       validade_cadastro as "Validade do Cadastro", 
       (SELECT Count(*) 
        FROM   vinculos 
        WHERE  vinculador_id = empresas.id 
               AND empresa_vinculada_tipo_id = 1)  AS "Quantidade GG Vinculado", 
       (SELECT Count(*) 
        FROM   vinculos 
        WHERE  vinculador_id = empresas.id 
               AND empresa_vinculada_tipo_id = 3)  AS "Quantidade TR Vinculado", 
       (SELECT Count(*) 
        FROM   vinculos 
        WHERE  vinculador_id = empresas.id 
               AND empresa_vinculada_tipo_id = 10) AS "Quantidade CM Vinculado", 
       (SELECT Count(*) 
        FROM   vinculos 
        WHERE  vinculador_id = empresas.id 
               AND empresa_vinculada_tipo_id = 5)  AS "Quantidade CO Vinculado", 
       (SELECT Count(*) 
        FROM   vinculos 
        WHERE  vinculador_id = empresas.id 
               AND empresa_vinculada_tipo_id = 4)  AS "Quantidade DR Vinculado", 
       status.descricao as "Status", 
       empresas.created_at                         AS "Data de Cadastro", 
       empresas.updated_at                         AS "Data de Atualizacao"
FROM   empresas 
       LEFT JOIN (SELECT Max(id) max_id, 
                         empresa_id, 
                         datadepagamento, 
                         tipo 
                  FROM   boletos 
                  WHERE  boletos.tipo IS NULL 
                  GROUP  BY empresa_id) boletos 
              ON ( boletos.empresa_id = empresas.id ) 
       LEFT JOIN status 
              ON status.id = empresas.status_id 
       LEFT JOIN (SELECT Max(id) max_id, 
                         empresa_tipo_id, 
                         empresa_id 
                  FROM   empresas_x_empresas_tipo 
                  GROUP  BY empresa_id) empresas_x_empresas_tipo 
              ON ( empresas_x_empresas_tipo.empresa_id = empresas.id ) 
       LEFT JOIN empresas_tipo 
              ON empresas_x_empresas_tipo.empresa_tipo_id = empresas_tipo.id 
       LEFT JOIN (SELECT Max(id) max_id, 
                         empresa_id, 
                         documento_id, 
                         caminho_arquivo 
                  FROM   empresa_documentos 
                  WHERE  documento_id = 1 
                  GROUP  BY empresa_id) empresa_documentos 
              ON ( empresa_documentos.empresa_id = empresas.id ) 
ORDER  BY boletos.datadepagamento DESC 